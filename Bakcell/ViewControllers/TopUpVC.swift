//
//  TopUpVC.swift
//  Bakcell
//
//  Created by Saad Riaz on 6/8/17.
//  Copyright © 2017 evampsaanga. All rights reserved.
//

import UIKit
import ObjectMapper

class TopUpVC: BaseVC {
    
    static let identifier = "TopUpVC"
    //MARK: - Properties
    
    var attributedDiscription : NSMutableAttributedString = NSMutableAttributedString()
    var isScratchCardSelected: Bool? = false // true
    
    //MARK: - IBOutlet
    @IBOutlet var scratchCard_btn: UIButton!
    @IBOutlet var plasticCard_btn: UIButton!
    
    @IBOutlet var mobileNumber_txt: UITextField!
    
    @IBOutlet var cardNumber_txt: REFormattedNumberField!
    @IBOutlet var topUpTitle_lbl: UILabel!
    @IBOutlet var topUpintro_lbl: UILabel!
    @IBOutlet var rechargeOption_lbl: UILabel!
    @IBOutlet var scratchCard_lbl: UILabel!
    @IBOutlet var platicCard_lbl: UILabel!
    @IBOutlet var topUp_btn: UIButton!
    
//    @IBOutlet var plasticCardView: UIView!
    @IBOutlet var amount_txt: UITextField!
    @IBOutlet var amount_view: UIView!

    @IBOutlet weak var scratchCardView  : UIStackView!
    @IBOutlet weak var plasticCardView  : UIStackView!

    @IBOutlet weak var cardTypeView  : UIStackView!
    @IBOutlet var cardType_lbl: UILabel!
    @IBOutlet var cardType_image : UIImageView!
    @IBOutlet var cardTypeDropDown: UIDropDown!
    
    @IBOutlet var newCardContinue_btn: UIButton!
    
    @IBOutlet weak var saveCardView: UIStackView!
    @IBOutlet weak var saveCardForPayment_btn : UIButton!
    @IBOutlet weak var saveCardForPaymentDescription_lbl : UILabel!
    
    @IBOutlet weak var selectCardView  : UIStackView!
    @IBOutlet var lblSelectCard: UILabel!
    @IBOutlet var cardsTableView: UITableView!
//    @IBOutlet var savedCardContinue_btn: UIButton!
    
    
    //MARK: - Properties..
    
    //Edit by touseef...
    var cardTypes : [String] = ["Master/Master  Electron","Visa/Visa Electron"]

    var selectedCardType : String = ""
    var selectedSaveCardIndex : Int = -1
    var isSaveCard : Bool? = true
    var selectedCard : PlasticCard?
    
    
    var savedCards : [SavedCards]?
    
    //MARK: - View Controller Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if MBUserSession.shared.predefineData?.plasticCards?.count ?? 0 > 0 {
            cardTypes.removeAll()
            for cardObject in MBUserSession.shared.predefineData?.plasticCards ?? [PlasticCard]() {
                cardTypes.append(cardObject.cardVale ?? "")
            }
        }
        
        mobileNumber_txt.text = MBUserSession.shared.msisdn
        
        setUpDropdown()
        
        cardsTableView.register(UINib(nibName: CardCell.identifier, bundle: nil), forCellReuseIdentifier: CardCell.identifier)
        cardsTableView.register(UINib(nibName: AddCardPaymentCell.ID, bundle: nil), forCellReuseIdentifier: AddCardPaymentCell.ID)
        mobileNumber_txt.delegate = self
        // cardNumber_txt.delegate = self
        amount_txt.delegate  = self
        
        //setting number of views of this screen
        UserDefaults.standard.set(UserDefaults.standard.integer(forKey: "TopUpScreenViews")+1, forKey: "TopUpScreenViews")
        UserDefaults.standard.synchronize()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.interactivePop(true)
        
        DispatchQueue.main.asyncAfter(deadline: .now()+0.1) {
            self.getSavedCards()
        }
        
        loadViewLayout()
    }
    
    //MARK: - FUNCTIONS
    func loadViewLayout(){
        
        cardNumber_txt.format = "XXXXX-XXXXX-XXXXX"
        amount_txt.keyboardType  = .decimalPad
        topUpTitle_lbl.text = Localized("Title_TopUP")
        topUpintro_lbl.text = Localized("TopUp_Description")
        rechargeOption_lbl.text = Localized("Title_RechargeOption")
        scratchCard_lbl.text = Localized("Title_ScratchCard")
        platicCard_lbl.text = Localized("Title_PlaticCard")
        cardType_lbl.text = Localized("Lbl_Cardtype")
        amount_txt.placeholder = Localized("Placeholder_Amount")
        lblSelectCard.text = Localized("Lbl_SelectCard")
        topUp_btn.setTitle(Localized("BtnTitle_Topup"), for: UIControl.State.normal)
        mobileNumber_txt.placeholder = Localized("Placeholder_MobileNumber")
        cardNumber_txt.placeholder = Localized("Placeholder_EnterCodeOfMoneyCard")
        newCardContinue_btn.setTitle(Localized("BtnTitle_Continue"), for: UIControl.State.normal)
        saveCardForPayment_btn.setImage(UIImage(named: "checkedbox"), for: .normal)
        saveCardForPaymentDescription_lbl.text = Localized("Lbl_SavePaymentForFuture")
        isSaveCard = true
        
        if isScratchCardSelected ?? true{
            self.scratchCardPressed(self.scratchCard_btn)
        }else{
            self.plasticCardPressed(self.plasticCard_btn)
        }
        
    }
    
    func setUpDropdown(){
        cardTypeDropDown.optionsTextAlignment = NSTextAlignment.center
        cardTypeDropDown.textAlignment = NSTextAlignment .center
        cardTypeDropDown.placeholder = Localized("DropDown_SelectCard")
        
        self.cardTypeDropDown.rowBackgroundColor = UIColor.MBDimLightGrayColor
        cardTypeDropDown.setFont = UIFont.MBArial(fontSize: 12)
        cardTypeDropDown.tableHeight = 70
        cardTypeDropDown.options = cardTypes
        cardTypeDropDown.placeholder = MBUserSession.shared.predefineData?.plasticCards?.first?.cardVale ?? "master"
        self.selectedCardType = String(MBUserSession.shared.predefineData?.plasticCards?.first?.cardKey  ??  1)
        
        self.cardTypeDropDown.tableWillAppear {
            self.cardType_image.image = UIImage (named: "Drop-down-arrow-state2")
        }
        self.cardTypeDropDown.tableWillDisappear {
            self.cardType_image.image = UIImage (named: "Drop-down-arrow-state1")
        }
        
        cardTypeDropDown.didSelect { (option, index) in
            print("\(option) : ===> \(index)")
            self.selectedCard = MBUserSession.shared.predefineData?.plasticCards?[index]
            self.selectedCardType = String(self.selectedCard?.cardKey ?? 1)
        }
    }
    
}

//MARK: IBACTIONS
extension TopUpVC{
    
    @IBAction func topUpPressed(_ sender: UIButton) {
        
        if isScratchCardSelected ?? true {
            let cardPinNumber = cardNumber_txt.plainText()
            
            if mobileNumber_txt.text == "" && cardPinNumber == "" {
                
                self.showErrorAlertWithMessage(message: Localized("Message_EnterNumber"))
                
            } else if mobileNumber_txt.text != nil && mobileNumber_txt.isValidMSISDN() == true && cardPinNumber == "" {
                
                
                self.showErrorAlertWithMessage(message: Localized("Message_EnterCardNumber"))
                
            }else if mobileNumber_txt.text != nil && cardPinNumber == "" {
                
                
                self.showErrorAlertWithMessage(message: Localized("Message_EnterValidNumber"))
            } else if mobileNumber_txt.text == nil && cardNumber_txt.plainText().isBlank == false {
                
                self.showErrorAlertWithMessage(message: Localized("Message_EnterNumber"))
                
            } else if  mobileNumber_txt.text != nil && cardNumber_txt.plainText().isValidCardNumber() == false {
                
                self.showErrorAlertWithMessage(message: Localized("Message_InvalidCardNumber"))
                
            } else if  ((mobileNumber_txt.isValidMSISDN() == false) && cardNumber_txt.plainText().isBlank == false) || ((mobileNumber_txt.isValidMSISDN() == false) || cardNumber_txt.plainText() == ""){
                
                self.showErrorAlertWithMessage(message: Localized("Message_EnterValidNumber"))
                
            }
                //Sucess case
            else if mobileNumber_txt.isValidMSISDN() && cardNumber_txt.plainText().isValidCardNumber() {
                
                var showBalanceOfUser:Bool = true
                if  MBUserSession.shared.subscriberType() == MBSubscriberType.PostPaid {
                    showBalanceOfUser = false
                } else  if mobileNumber_txt.text != MBUserSession.shared.userInfo?.msisdn {
                    showBalanceOfUser = false
                }
                
                let alert = self.myStoryBoard.instantiateViewController(withIdentifier: "AlertVC") as! AlertVC
                
                self.presentPOPUP(alert, animated: true, completion: nil)
                
                alert.setConfirmationAlertForTopUp(title: Localized("Title_Confirmation"), reciver: mobileNumber_txt.text ?? "", showBalnce: showBalanceOfUser)
                
                alert.setYesButtonCompletionHandler { (aString) in
                    self.getTopUp(SubscriberType: MBUserSession.shared.subscriberTypeLowerCaseStr, CardNumber: cardPinNumber, TopUp: self.mobileNumber_txt.text ?? "")
                }
                
            }
        } else {
            
            self.showAlertWithMessage(message: Localized("Message_SelectScratchCard"))
            
        }
        
    }
    
    @IBAction func scratchCardPressed(_ sender: UIButton) {
        
        scratchCard_btn .setImage(#imageLiteral(resourceName: "check (1)"), for: UIControl.State.normal)
        plasticCard_btn .setImage(#imageLiteral(resourceName: "Checkbox-state2"), for: UIControl.State.normal)
        
        self.isScratchCardSelected = true
        self.plasticCardView.isHidden = true
        self.scratchCardView.isHidden = false
        
    }
    
    @IBAction func plasticCardPressed(_ sender: UIButton) {
        
        
        
        if mobileNumber_txt.isValidMSISDN() {
            
            scratchCard_btn .setImage(#imageLiteral(resourceName: "Checkbox-state2"), for: UIControl.State.normal)
            plasticCard_btn .setImage(#imageLiteral(resourceName: "check (1)"), for: UIControl.State.normal)
            
            self.isScratchCardSelected = false
            self.plasticCardView.isHidden = false
            self.scratchCardView.isHidden = true
            
            
        } else {
            
            self.showErrorAlertWithMessage(message: Localized("Message_EnterValidNumber"))
        }
        
        /*
         if mobileNumber_txt.isValidMSISDN() {
             
             scratchCard_btn .setImage(#imageLiteral(resourceName: "Checkbox-state2"), for: UIControl.State.normal)
             plasticCard_btn .setImage(#imageLiteral(resourceName: "Checkbox-state1"), for: UIControl.State.normal)
             
             self.isScratchCardSelected = false
             if let plasticCardVC = self.myStoryBoard.instantiateViewController(withIdentifier: "PlasticCardViewController") as? PlasticCardViewController {
                 plasticCardVC.topUpToMSISDN = mobileNumber_txt.text ?? ""
                 self.navigationController?.pushViewController(plasticCardVC, animated: true)
             }
             
         } else {
             
             self.showErrorAlertWithMessage(message: Localized("Message_EnterValidNumber"))
         }
        */
    }
    
    @IBAction func continueBtn(_ sender: UIButton){
        if self.mobileNumber_txt.text?.isBlank ?? true{
            showErrorAlertWithMessage(message: Localized("Lbl_EnterNumber"))
        }else if self.amount_txt.text?.isBlank ?? true{
            showErrorAlertWithMessage(message: Localized("Lbl_EnterAmount"))
        }else{
            
            /*if self.selectedCardType.isBlank{
                self.showErrorAlertWithMessage(message: Localized("Lbl_SelectCard"))
                return
            }*/
            if !mobileNumber_txt.isValidMSISDN(){
                self.showErrorAlertWithMessage(message: Localized("Message_EnvalidMobileNumber"))
                return
            }
            
            if selectedSaveCardIndex == -1 {
                self.isSaveCard = false
                showCardTypesAlert()
                return
            } else {
                let alert = self.myStoryBoard.instantiateViewController(withIdentifier: "AlertVC") as! AlertVC
                
                self.presentPOPUP(alert, animated: true, completion: nil)
                let amount = amount_txt.text ?? "0"
                let number = mobileNumber_txt.text ?? "0"
                var messageString = String(format: Localized("Message_LoadConfirmation"), amount, number)
                if MBLanguageManager.userSelectedLanguageShortCode() == "az" {
                    messageString = String(format: Localized("Message_LoadConfirmation"), number, amount)
                }
                
                alert.showtopUpConfirmationAlert(title: Localized("Title_Confirmation"), message: messageString)
                
                alert.setYesButtonCompletionHandler { (aString) in
                    self.makepayment(topUpNumber: self.mobileNumber_txt.text ?? "")
                }
                
                
            }
            
        }
    }
    
    //Checkbox tapped...
    @IBAction  func checkBoxTapped(_ sender: UIButton){
        if sender.currentImage == UIImage(named: "checkedbox") {
            sender.setImage(UIImage(named: "Checkbox-state-2"), for: .normal)
            self.isSaveCard = false
        }else{
            sender.setImage(UIImage(named: "checkedbox"), for: .normal)
            self.isSaveCard = true
        }
    }
    
    
    
    func showPaymentPrompt() {
        let alert = self.myStoryBoard.instantiateViewController(withIdentifier: "AlertVC") as! AlertVC
        
        self.presentPOPUP(alert, animated: true, completion: nil)
        var messageString = String(format: Localized("Message_LoadConfirmation"), self.amount_txt.text ??  "", mobileNumber_txt.text ?? "")
        if MBLanguageManager.userSelectedLanguageShortCode() == "az" {
            messageString = String(format: Localized("Message_LoadConfirmation"), self.amount_txt.text ??  "", mobileNumber_txt.text ?? "")
        }
        
        alert.showtopUpConfirmationAlert(title: Localized("Title_Confirmation"), message: messageString)
        
        alert.setYesButtonCompletionHandler { (aString) in
            self.initiatePayment(topUpNumber: self.mobileNumber_txt.text ?? "")
        }
    }
    
}



//MARK: Textfield delagates
extension TopUpVC : UITextFieldDelegate {
    
    //Set max Limit for textfields
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if let char = string.cString(using: String.Encoding.utf8) {
                let isBackSpace = strcmp(char, "\\b")
                if (isBackSpace == -92) {
                    return true
                }
            }
        
        guard let text = textField.text else { return false }
        let newLength = text.count + string.count - range.length
        
        let allowedCharacters = CharacterSet(charactersIn: Constants.allowedNumbers)
        let characterSet = CharacterSet(charactersIn: string)
        if textField == mobileNumber_txt {
            if newLength <= 9 && allowedCharacters.isSuperset(of: characterSet) {
                return  true
            } else {
                return  false
            }
            
        } else if textField == amount_txt {
            
            
            let newString = (textField.text! as NSString).replacingCharacters(in: range, with: string)
            let arrayOfString = newString.components(separatedBy: ".")
            if amount_txt.text?.length == 0 && string.toInt < 1 {
                return false
            } else if  arrayOfString.count > 1{
                if arrayOfString.last?.count ?? 0 > 2 {
                    return false
                }
            }
            return true

        } else {
            return false // Bool
        }
    }
}


//MARK: - Table delegates & datasource

extension TopUpVC: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (self.savedCards?.count ?? 0) + 1
    }
    
    func tableView(_ tableView: UITableView, titleForDeleteConfirmationButtonForRowAt indexPath: IndexPath) -> String? {
        return Localized("DeleteButtonTitle")
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        //return true
        if self.savedCards?.count ?? 0 > 0 {
            if indexPath.row < self.savedCards?.count ?? 0 {
                return true
            }
        }
        return false
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == self.savedCards?.count ?? 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: AddCardPaymentCell.ID, for: indexPath) as! AddCardPaymentCell
           // cell.lblTitle.text = Localized("Lbl_AddAnotherCard2")
            if self.savedCards?.count == 0 {
                cell.lblTitle.text = Localized("Lbl_AddACard")
            } else {
                cell.lblTitle.text = Localized("Lbl_AddanotherCard")
            }
//            cell.lblTitle.text = Localized("Lbl_AddAnewCard")
            return cell
        }else{
            
            let cell = cardsTableView.dequeueReusableCell(withIdentifier: CardCell.identifier, for: indexPath) as! CardCell
            cell.setUp(withData: self.savedCards?[indexPath.row])
            if indexPath.row == self.selectedSaveCardIndex{
                cell.cardNumber.textColor = .red
                cell.radioSelect.image = #imageLiteral(resourceName: "radio active")
            }else{
                cell.cardNumber.textColor = #colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1)
                cell.radioSelect.image = #imageLiteral(resourceName: "Checkbox-state2")
            }
            return cell
        }
        
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == self.savedCards?.count ?? 0{
            self.isSaveCard = true
            showCardTypesAlert()
            
            return
            
            /*if self.amount_txt.text?.isBlank ?? true{
                self.showErrorAlertWithMessage(message: Localized("Lbl_EnterAmount"))
            }*/
            let plastic = self.myStoryBoard.instantiateViewController(withIdentifier:  CardTypeSelectionVC.identifier) as! CardTypeSelectionVC
            plastic.selectedAmount = self.amount_txt.text ?? "0"
            plastic.selectedMsisdn = mobileNumber_txt.text ?? ""
            self.navigationController?.pushViewController(plastic, animated: true)
            
            
        }else{
            if indexPath.row == self.selectedSaveCardIndex {
                self.selectedSaveCardIndex = -1
            } else {
                self.selectedSaveCardIndex = indexPath.row
            }
            
            
            self.selectedCardType = self.savedCards?[indexPath.row].cardType ?? ""
            self.cardNumber_txt.text = self.savedCards?[indexPath.row].number ?? ""
        }
        self.cardsTableView.reloadData()
    }
    
    
    //Swipe to Delete
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if indexPath.row != self.savedCards?.count ?? 0{
            if  editingStyle == .delete{
                
                self.deleteSavedCard(savedCardId: self.savedCards?[indexPath.row].id ?? "") { (isDeleted) in
                    if isDeleted{
                        self.savedCards?.remove(at: indexPath.row)
                        self.cardsTableView.reloadData()
                        
                    }
                }
            }
        }
        
    }
    
}


//MARK: API Calls...
extension TopUpVC{
    
    
    /// Call 'initiatePayment' API.
    ///
    /// - returns: Void
    func  initiatePayment(topUpNumber: String){
        activityIndicator.showActivityIndicator()
        var topUpamount = "0"
        if isSaveCard == false {
            topUpamount = self.amount_txt.text  ?? "0"
        }
        _ = MBAPIClient.sharedClient.initiatePayment(CardType: self.selectedCardType, Amount: topUpamount, IsSaved: true/*self.isSaveCard ?? false*/, TopUpNumber: topUpNumber){ ( response, resultData, error, isCancelled, status, resultCode, resultDesc) in
            
            self.activityIndicator.hideActivityIndicator()
            if error != nil {
                //EventLog
//                MBAnalyticsManager.logEvent(screenName: "Top up Screen", contentType:"Top up Scratch Card Failure" , status:"Failure" )
                error?.showServerErrorInViewController(self)
                
            } else {
                // handling data from API response.
                if resultCode == Constants.MBAPIStatusCode.succes.rawValue {
                    
//                    //EventLog
//                    MBAnalyticsManager.logEvent(screenName: "Top up Screen", contentType:"Top up Scratch Card Success" , status:"Success" )
//
//                    // Set true to reload Home page data
//                    MBUserSession.shared.shouldReloadDashboardData = true
//
                    //parsing data...
                    
                    if let resp =  Mapper<SavedCards>().map(JSONObject: resultData){
                        if resp.paymentKey.isEmpty || resp.paymentKey == "" {
                            //do nothing
                        } else {
                            if let plasticCardVC = self.myStoryBoard.instantiateViewController(withIdentifier: "PlasticCardViewController") as? PlasticCardViewController {
                                plasticCardVC.topUpToMSISDN = self.mobileNumber_txt.text ?? ""
                                plasticCardVC.redirectionURL = resp.paymentKey
                                if topUpamount.toInt > 0 {
                                    plasticCardVC.isSaveCard = self.isSaveCard ?? false
                                }
                                
                                self.navigationController?.pushViewController(plasticCardVC, animated: true)
                            }
                        }
                    }
                    
                    
                } else {
                    //EventLog
//                    MBAnalyticsManager.logEvent(screenName: "Top up Screen", contentType:"Top up Scratch Card Failure" , status:"Failure" )
                    
                    // Show error alert to user
                    self.showErrorAlertWithMessage(message: resultDesc)
                }
            }
        }
    }
    
    func checkTopupSurvey(message: String) {
        if let survey  = MBUserSession.shared.appSurvays?.surveys?.first(where: { $0.screenName  == "topup" }) {
            if survey.surveyCount?.toInt ?? 0 < survey.surveyLimit?.toInt ?? 0,
               survey.visitLimit != "-1",
               survey.visitLimit?.toInt ?? 0 <= UserDefaults.standard.integer(forKey: "TopUpScreenViews") {
                if let inAppSurveyVC : InAppSurveyVC = InAppSurveyVC.fromNib() {
                    inAppSurveyVC.currentSurvay = survey
                    inAppSurveyVC.showTopUpStackView = true
                    inAppSurveyVC.toptupTitleText = Localized("Title_Alert")
                    inAppSurveyVC.attributedMessage = NSAttributedString.init(string: message)
                    self.presentPOPUP(inAppSurveyVC, animated: true, completion: nil)
                }
            } else {
                showSuccessAlertWithMessage(message: message) { (message) in
                    self.navigationController?.popViewController(animated: true)
                }
            }
        }
    }
    
    
    /// Call 'makePayment' API.
    ///
    /// - returns: Void
    func  makepayment(topUpNumber: String){
        activityIndicator.showActivityIndicator()
        _ = MBAPIClient.sharedClient.makepayment(PaymentKey: self.savedCards?[selectedSaveCardIndex].paymentKey ?? "", Amount: amount_txt.text ?? "", TopupNumber: topUpNumber){ ( response, resultData, error, isCancelled, status, resultCode, resultDesc) in
            
            self.activityIndicator.hideActivityIndicator()
            if error != nil {
                //EventLog
//                MBAnalyticsManager.logEvent(screenName: "Top up Screen", contentType:"Top up Scratch Card Failure" , status:"Failure" )
                error?.showServerErrorInViewController(self)
                
            } else {
                // handling data from API response.
                if resultCode == Constants.MBAPIStatusCode.succes.rawValue {
                    
//                    //EventLog
//                    MBAnalyticsManager.logEvent(screenName: "Top up Screen", contentType:"Top up Scratch Card Success" , status:"Success" )
//
//                    // Set true to reload Home page data
//                    MBUserSession.shared.shouldReloadDashboardData = true
//
                    //parsing data...
                    MBUserSession.shared.shouldReloadDashboardData = true
                    
                    
                    DispatchQueue.main.asyncAfter(deadline: .now()+0.25) {
                        self.checkTopupSurvey(message: resultDesc ?? "")
                    }
                    /*self.showSuccessAlertWithMessage(message: resultDesc) { (message) in
                        self.navigationController?.popViewController(animated: true)
                    }*/
                    
                    
                } else {
                    //EventLog
//                    MBAnalyticsManager.logEvent(screenName: "Top up Screen", contentType:"Top up Scratch Card Failure" , status:"Failure" )
                    
                    // Show error alert to user
                    self.showErrorAlertWithMessage(message: resultDesc)
                }
            }
        }
    }
    
    /// Call 'deletesavedcard' API.
    ///
    /// - returns: Void
    func  deleteSavedCard(savedCardId: String, onCompletion : @escaping (Bool) -> ()){
        activityIndicator.showActivityIndicator()
        _ = MBAPIClient.sharedClient.deleteSavedcard(savedCardId: savedCardId){ ( response, resultData, error, isCancelled, status, resultCode, resultDesc) in
            self.activityIndicator.hideActivityIndicator()
            if error != nil {
                //EventLog
//                MBAnalyticsManager.logEvent(screenName: "Top up Screen", contentType:"Top up Scratch Card Failure" , status:"Failure" )
                
                error?.showServerErrorInViewController(self)
                
            } else {
                // handling data from API response.
                if resultCode == Constants.MBAPIStatusCode.succes.rawValue {
                    
//                    //EventLog
//                    MBAnalyticsManager.logEvent(screenName: "Top up Screen", contentType:"Top up Scratch Card Success" , status:"Success" )
//
//                    // Set true to reload Home page data
//                    MBUserSession.shared.shouldReloadDashboardData = true
//
                    //parsing data...
                    onCompletion(true)
                    
                    
                } else {
                    //EventLog
//                    MBAnalyticsManager.logEvent(screenName: "Top up Screen", contentType:"Top up Scratch Card Failure" , status:"Failure" )
                    
                    // Show error alert to user
                    self.showErrorAlertWithMessage(message: resultDesc)
                }
            }
        }
    }
    
    
    /// Call 'getsavedcards' API.
    ///
    /// - returns: Void
    func  getSavedCards(){
        activityIndicator.showActivityIndicator()
        _ = MBAPIClient.sharedClient.getSavedCards({ ( response, resultData, error, isCancelled, status, resultCode, resultDesc) in
            self.activityIndicator.hideActivityIndicator()
            if error != nil {
                //EventLog
//                MBAnalyticsManager.logEvent(screenName: "Top up Screen", contentType:"Top up Scratch Card Failure" , status:"Failure" )
                
                error?.showServerErrorInViewController(self)
                
            } else {
                // handling data from API response.
                if resultCode == Constants.MBAPIStatusCode.succes.rawValue {
                    
//                    //EventLog
//                    MBAnalyticsManager.logEvent(screenName: "Top up Screen", contentType:"Top up Scratch Card Success" , status:"Success" )
//
//                    // Set true to reload Home page data
//                    MBUserSession.shared.shouldReloadDashboardData = true
//
                    //parsing data...
                    
                    if let resp =  Mapper<SavedCardsObj>().map(JSONObject:resultData){
                        self.savedCards = resp.cardDetails
                        self.amount_txt.text = resp.lastAmount ?? ""
                        
                        //FIXME: - Change the check to ">" for  new/first time case check... otherwise use "<="
                        if self.savedCards?.count ?? 0 <= 0 {
                            self.selectedSaveCardIndex = -1
                            self.cardTypeView.isHidden = true
                            self.selectCardView.isHidden = false
                            self.saveCardView.isHidden = true
//                            self.amount_view.isHidden = true
                            self.lblSelectCard.isHidden = true
                        }else{
                            self.selectedSaveCardIndex = 0
                            self.cardTypeView.isHidden = true
                            self.selectCardView.isHidden = false
                            self.saveCardView.isHidden = true
//                            self.amount_view.isHidden = false
                            self.cardTypeView.isHidden = true
                            self.lblSelectCard.isHidden = false
                        }
                        self.cardsTableView.reloadData()
                    }
                    
                    
                } else {
                    //EventLog
//                    MBAnalyticsManager.logEvent(screenName: "Top up Screen", contentType:"Top up Scratch Card Failure" , status:"Failure" )
                    
                    // Show error alert to user
                    self.showErrorAlertWithMessage(message: resultDesc)
                }
            }
        })
    }
    
    
    
    //MARK: - API Call
    /// Call 'TopUp' API.
    ///
    /// - returns: Void
    
    func getTopUp( SubscriberType subscriberType: String, CardNumber cardPinNumber : String, TopUp topUpNum : String) {
        activityIndicator.showActivityIndicator()
        
        _ = MBAPIClient.sharedClient.getTopUp(SubscriberType: subscriberType, CardPin: cardPinNumber , TopupNumber: topUpNum , { ( response, resultData, error, isCancelled, status, resultCode, resultDesc) in
            
            self.activityIndicator.hideActivityIndicator()
            if error != nil {
                //EventLog
                MBAnalyticsManager.logEvent(screenName: "Top up Screen", contentType:"Top up Scratch Card Failure" , status:"Failure" )
                
                error?.showServerErrorInViewController(self)
                
            } else {
                // handling data from API response.
                if resultCode == Constants.MBAPIStatusCode.succes.rawValue {
                    
                    //EventLog
                    MBAnalyticsManager.logEvent(screenName: "Top up Screen", contentType:"Top up Scratch Card Success" , status:"Success" )
                    
                    // Set true to reload Home page data
                    MBUserSession.shared.shouldReloadDashboardData = true
                    
                    
                    // Parssing response data
                    if let topUpResponse = Mapper<TopUp>().map(JSONObject:resultData) {
                        
                        var showBalanceOfUser:Bool = true
                        if  MBUserSession.shared.subscriberType() == MBSubscriberType.PostPaid {
                            showBalanceOfUser = false
                        } else {
                            
                            if topUpNum != MBUserSession.shared.userInfo?.msisdn {
                                showBalanceOfUser = false
                            } else {
                                MBUserSession.shared.balance?.amount = topUpResponse.newBalance
                            }
                        }
                        
                        let alert = self.myStoryBoard.instantiateViewController(withIdentifier: "AlertVC") as! AlertVC
                        
                        alert.setSuccessAlertForTopUp(title: Localized("Title_successful"), reciver: topUpNum, showBalnce: showBalanceOfUser)
                        
                        self.presentPOPUP(alert, animated: true, completion: nil)
                        
                        self.cardNumber_txt.text = ""
                    }
                } else {
                    //EventLog
                    MBAnalyticsManager.logEvent(screenName: "Top up Screen", contentType:"Top up Scratch Card Failure" , status:"Failure" )
                    
                    // Show error alert to user
                    self.showErrorAlertWithMessage(message: resultDesc)
                }
            }
        })
    }
    
}


extension TopUpVC {
    func showCardTypesAlert() {
        if MBUserSession.shared.predefineData?.plasticCards?.count ?? 0 > 0 {
            if let alert = self.myStoryBoard2.instantiateViewController(withIdentifier: "CardTypeAlertVC") as? CardTypeAlertVC {
                alert.setAlertWith { (stringToCheck) in
                    self.selectedCardType = stringToCheck
                    self.initiatePayment(topUpNumber: self.mobileNumber_txt.text ?? "")
                }
                self.presentPOPUP(alert, animated: true, modalTransitionStyle:.crossDissolve, completion: nil)
            }
        }
    }
    
    func openHisabPortal() {
        if let plasticCardVC = self.myStoryBoard.instantiateViewController(withIdentifier: "PlasticCardViewController") as? PlasticCardViewController {
            plasticCardVC.topUpToMSISDN = self.mobileNumber_txt.text ?? ""
            plasticCardVC.redirectionURL = "https://hesab.az/unregistered/#/mobile/bakcell/parameters?lang=\(MBLanguageManager.userSelectedLanguageShortCode())"
            self.navigationController?.pushViewController(plasticCardVC, animated: true)
        }
    }
}
