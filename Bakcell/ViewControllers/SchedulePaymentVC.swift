//
//  SchedulePaymentVC.swift
//  Bakcell
//
//  Created by Touseef Sarwar on 04/03/2021.
//  Copyright © 2021 evampsaanga. All rights reserved.
//

import UIKit
import UPCarouselFlowLayout
import ObjectMapper

enum Schedules {
    case daily
    case weekly
    case monthly
}


class SchedulePaymentVC: BaseVC {
    
    
    //View Controller Identifier.
    static let identifier = "SchedulePaymentVC"
    
    
    
    // MARK: - IBOutlet
    
    //Navigation title...
    @IBOutlet weak var schedulePaymentTitle_lbl: UILabel!
    
    
    //Tabs button...
    @IBOutlet weak var dailyBtn : UIButton!
    @IBOutlet weak var weeklyBtn : UIButton!
    @IBOutlet weak var monthlyBtn : UIButton!
    
    
    //Select Card
    @IBOutlet weak var selectCardLabel : UILabel!
    @IBOutlet weak var tableView: UITableView!
    
    //Enter Amount
    @IBOutlet weak var amountStackView : UIStackView!
    @IBOutlet weak var enterAmountLabel : UILabel!
    @IBOutlet weak var amountTF : UITextField!
    
    @IBOutlet weak var parentView: UIStackView!
    
    //Recure every week
    @IBOutlet weak var weekDayView : UIStackView!
    @IBOutlet weak var recureStackView : UIStackView!
    @IBOutlet weak var recureWeekLabel : UILabel!
    @IBOutlet weak var weekOrMonth : UILabel!
    @IBOutlet weak var noOfWeekOrMonthTF : UITextField!
    @IBOutlet weak var weekdaysCollectionView: UICollectionView!
    
    
    //Start date
    @IBOutlet weak var startDayLabel : UILabel!
    @IBOutlet weak var dateLabel : UILabel!
    @IBOutlet weak var startDateBtn : UIButton!
    @IBOutlet weak var dropdownImage : UIImageView!
    let datePicker = UIDatePicker()
    let myView = UIView()
    
    //Number of recurence
    @IBOutlet weak var numberOfRecureLabel : UILabel!
    @IBOutlet weak var numberOfRecureTF : UITextField!
    //cancel & schedule Button..
    @IBOutlet weak var cancelBtn : UIButton!
    @IBOutlet weak var scheduleBtn : UIButton!
    
    
    //Auto renew checkbox & label
    @IBOutlet weak var autoCheckView: UIStackView!
    @IBOutlet weak var autoCheckBtn: UIButton!
    @IBOutlet weak var autoCheckLabel: UILabel!
    
    
    //MARK: - Properties..
    var selectedSchdule : Schedules = .daily
    var selectedCardIndex : Int = 0
    
    var weekDayIndex : Int? = Calendar.current.component(.weekday, from: Date().todayDate()) - 1
 
    var savedCards : [SavedCards]?
    
    
    var weekDays = [
        Localized("Lbl_Mo"),
        Localized("Lbl_Tu"),
        Localized("Lbl_We"),
        Localized("Lbl_Th"),
        Localized("Lbl_Fr"),
        Localized("Lbl_Sa"),
        Localized("Lbl_Su")
    ]
   
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        weekdaysCollectionView.delegate = self
        weekdaysCollectionView.dataSource = self
        tableView.register(UINib(nibName: CardCell.identifier, bundle: nil), forCellReuseIdentifier: CardCell.identifier)
        tableView.tableHeaderView =  UIView()
        amountTF.delegate = self
        amountTF.placeholder = Localized("Placeholder_Amount")
        self.numberOfRecureTF.delegate = self
        self.getSavedCards()
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.interactivePop(true)
        
        loadViewLayout()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }
    
    //MARK: - FUNCTIONS
    func loadViewLayout(){
//        schedulePaymentTitle_lbl.font = UIFont.MBArial(fontSize: 20)
        schedulePaymentTitle_lbl.text = Localized("schedulePayment_Title")
        
        selectCardLabel.text = Localized("Lbl_SelectCard")
        enterAmountLabel.text = Localized("Lbl_EnterAmount")
        recureWeekLabel.text = Localized("Lbl_RecureWeek")
        weekOrMonth.text = selectedSchdule == .weekly ? Localized("Lbl_Week") : Localized("Lbl_Month")
        startDayLabel.text = Localized("Lbl_StartDate")
        numberOfRecureLabel.text = Localized("Lbl_NoOfReccurrence")
        dateLabel.text = MBUserSession.shared.dateFormatter.createString(from: Date().todayDate(), dateFormate: Constants.kDisplayFormat)
        autoCheckLabel.text = Localized("Label_AutoRenew")
        self.autoCheckBtn.setImage(UIImage(named: "Checkbox-state-2"), for: .normal)
        self.autoCheckView.isHidden =  true
//        self.numberOfRecureTF.text = "14"
        //MARK: -Tab Buttons Layout
        
        //daily Button
        self.setGrayButton(dailyBtn, withTitle: Localized("BtnTitle_Daily"), isGrayBackground: true)
        
        //weekly button
        self.setGrayButton(weeklyBtn, withTitle: Localized("BtnTitle_Weekly"), isGrayBackground: false)
        
        //Monthly button
        self.setGrayButton(monthlyBtn, withTitle: Localized("BtnTitle_Monthly"), isGrayBackground: false)
        
        //MARK: - Setting  layout for cancel & schedule button
        //cancel
        self.setGrayButton(cancelBtn, withTitle: Localized("BtnTitle_CANCEL"), isGrayBackground: false)
        

        //schedule
        self.setGrayButton(scheduleBtn, withTitle: Localized("BtnTitle_Schedule"), isGrayBackground: true)
        
        
        switch selectedSchdule {
        case .daily:
            self.weekDayView.isHidden = true
            self.recureStackView.isHidden = true
        case .weekly:
            self.weekDayView.isHidden = false
            self.weekdaysCollectionView.isHidden = false
            self.recureStackView.isHidden = true
        case .monthly:
            self.weekDayView.isHidden = false
            self.weekdaysCollectionView.isHidden = true
            self.recureStackView.isHidden = false
        }
        
    }
    /*
    //MARK: - Setup Dropdown..
    func setDropdown(){
        dateDropdown.textAlignment = NSTextAlignment .center
        dateDropdown.optionsTextAlignment = NSTextAlignment.center
//        dateDropdown.placeholder = Localized("DropDown_SelectAmount")
        dateDropdown.placeholder = "Select Date"
        self.dateDropdown.rowBackgroundColor = UIColor.MBDimLightGrayColor
        dateDropdown.setFont = UIFont.MBArial(fontSize: 12)
        
//        dateDropdown.
        
        self.dateDropdown.tableWillAppear {
            self.dropdownImage.image = UIImage (named: "Drop-down-arrow-state2")
        }
        
        self.dateDropdown.tableWillDisappear {
            self.dropdownImage.image = UIImage (named: "Drop-down-arrow-state1")
        }
        
        dateDropdown.didSelect { (option, index) in
            // self.dropDownLbl.text = " \(option)"
            print("You just select: \(option) at index: \(index)")
//            self.selectedValue = option
            //self.selectedValue = self.selectedValue?.substring(to: (self.selectedValue?.index(before: (self.selectedValue?.endIndex)!))!)
//            print(self.selectedValue ?? "")
            
        }
    }
     */
    
}

// MARK: IBActions
extension SchedulePaymentVC {
    
    //Daily, weekly & Monthly Functionality
    @IBAction func tabButtonActions(_ sender: UIButton){
        
        sender.backgroundColor = UIColor .lightGray
        sender.backgroundColor = UIColor.MBButtonBackgroundGrayColor
        sender.setTitleColor(UIColor .MBTextBlack, for: UIControl.State.normal)
        switch sender.tag {
        case 0:
            self.weekDayView.isHidden = true
            selectedSchdule = .daily
            setGrayButton(weeklyBtn, withTitle: weeklyBtn.currentTitle, isGrayBackground: false)
            setGrayButton(monthlyBtn, withTitle: monthlyBtn.currentTitle, isGrayBackground: false)
            self.autoCheckView.isHidden =  true
            self.amountStackView.isHidden = false
        case 1:
            self.weekDayView.isHidden = false
            self.weekdaysCollectionView.isHidden = false
            selectedSchdule = .weekly
            weekOrMonth.text = Localized("Lbl_Week")
            setGrayButton(dailyBtn, withTitle: dailyBtn.currentTitle, isGrayBackground: false)
            setGrayButton(monthlyBtn, withTitle: monthlyBtn.currentTitle, isGrayBackground: false)
            self.autoCheckView.isHidden =  true
            self.amountStackView.isHidden = false
        case 2:
            self.weekDayView.isHidden = false
            self.weekdaysCollectionView.isHidden = true
            
            selectedSchdule = .monthly
            weekOrMonth.text = Localized("Lbl_Month")
            if MBUserSession.shared.subscriberType() == MBSubscriberType.PostPaid {
                self.autoCheckView.isHidden =  false
                self.amountStackView.isHidden = true
            }
            setGrayButton(dailyBtn, withTitle: dailyBtn.currentTitle, isGrayBackground: false)
            setGrayButton(weeklyBtn, withTitle: weeklyBtn.currentTitle, isGrayBackground: false)
        default:
            break
        }
    }
    
    //Start date
    @IBAction func startDateBtnAction(_ sender: UIButton) {
        let todayDate = MBUtilities.todayDate()
        MBUserSession.shared.dateFormatter.dateFormat = Constants.kDisplayFormat
        
        let threeMonthsAgos = MBUtilities.todayDate().getMonth(WithAdditionalValue: -3)?.startOfMonth() ?? todayDate
        
        //can use predefined date instead of todayDate...
        self.showDatePicker(minDate: threeMonthsAgos, maxDate: todayDate)
    }
    
    //Cancel
    @IBAction func cancelButtonAction(_ sender: UIButton){}
    
    //Schedule
    @IBAction func scheduleButtonAction(_ sender: UIButton){
        if  self.savedCards?.count ?? 0 > 0 && self.numberOfRecureTF.text?.toInt ?? 0 > 0 {
            
            
            if selectedSchdule == .monthly {
                if MBUserSession.shared.subscriberType() == MBSubscriberType.PostPaid {
                    if self.autoCheckBtn.currentImage == UIImage(named: "Checkbox-state-2") {
                        self.showAlertWithMessage(message: Localized("label_selectCheckBox"))
                        return
                    } else {
                        self.amountTF.text? = ""
                    }
                } else {
                    if self.amountTF.text?.toInt ?? 0 < 1 {
                        showErrorAlertWithMessage(message: Localized("Lbl_EnterAmount"))
                        return
                    }
                }
            } else {
                if self.amountTF.text?.toInt ?? 0 < 1 {
                    showErrorAlertWithMessage(message: Localized("Lbl_EnterAmount"))
                    return
                }
            }
            
            
            //Call api
            
            let formatedDate = MBUserSession.shared.dateFormatter.createString(from:   MBUserSession.shared.dateFormatter2.date(from: self.dateLabel.text ?? "\(Date().todayDate())") ?? Date().todayDate(), dateFormate: Constants.kAPIFormat)
            
                        self.addPaymentScheduler(
                amount: self.amountTF.text ?? "0",
                billingCycle: (selectedSchdule == .daily) ? "1" : (selectedSchdule == .weekly) ? "2" : "3",
                startDate: formatedDate,
                            recurrenceNumber: self.numberOfRecureTF.text ?? self.numberOfRecureTF.placeholder ?? "1",
                savedCardId: self.savedCards?[selectedCardIndex].id ?? "1")
        }else{
            if self.savedCards?.count ?? 0 <= 0 {
                self.showAlertWithMessage(message: Localized("Lbl_AddNewCard"))
            } else if self.numberOfRecureTF.text?.toInt ?? 0 <= 0 {
                self.showAlertWithMessage(message: Localized("Lbl_Correctruccrannce"))
            }
            
        }
    }
    
    //Auto renew check button
    @IBAction func autoCheckAction(_ sender: UIButton){
        
        if self.autoCheckBtn.currentImage == UIImage(named: "Checkbox-state-1") {
            self.autoCheckBtn.setImage(UIImage(named: "Checkbox-state-2"), for: .normal)
//            self.numberOfRecureTF.isEnabled = true
//            self.startDateBtn.isEnabled = true
            
//            self.noOfWeekOrMonthTF.isEnabled = false
            //self.noOfWeekOrMonthTF.text = "1"
//            self.parentView.backgroundColor = .clear
            
//            self.numberOfRecureTF.placeholder = "0"
            
//            self.weekdaysCollectionView.backgroundColor = .clear
        } else {
            self.autoCheckBtn.setImage(UIImage(named: "Checkbox-state-1"), for: .normal)
            
//            self.numberOfRecureTF.text = "0"
//            self.numberOfRecureTF.isEnabled = false
            
//            self.startDateBtn.isEnabled = false
            
//            self.noOfWeekOrMonthTF.isEnabled = false
//            self.noOfWeekOrMonthTF.placeholder = "0"
//            self.noOfWeekOrMonthTF.text = "0"
            
            //self.weekdaysCollectionView.backgroundColor = #colorLiteral(red: 0.9253990054, green: 0.9255540371, blue: 0.925378561, alpha: 1)
            //self.parentView.backgroundColor = #colorLiteral(red: 0.9253990054, green: 0.9255540371, blue: 0.925378561, alpha: 1)
        }
        
    }
    
}

//MARK: - DatePicker Methods
extension SchedulePaymentVC{
    
    func showDatePicker(minDate : Date , maxDate : Date){
        //Formate Date
        datePicker.datePickerMode = .date
        
        //ToolBar
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        
        //done button & cancel button
        let doneButton = UIBarButtonItem(title: Localized("Btn_title_Done"), style: UIBarButtonItem.Style.done, target: self, action: #selector(UsageSummaryMainVC.doneDatePicker))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: Localized("Btn_title_Cancel"), style: UIBarButtonItem.Style.plain, target: self, action: #selector(UsageSummaryMainVC.cancelDatePicker))
        toolbar.setItems([cancelButton,spaceButton,doneButton], animated: false)
        
        var  currentLanguageLocale = Locale(identifier:"en")
        switch MBLanguageManager.userSelectedLanguage() {
            
        case .Russian:
            currentLanguageLocale = Locale(identifier:"ru")
        case .English:
            currentLanguageLocale = Locale(identifier:"en")
        case .Azeri:
            currentLanguageLocale = Locale(identifier:"az_AZ")
        }
        
        self.datePicker.calendar.locale = currentLanguageLocale
        self.datePicker.locale = currentLanguageLocale
        
        datePicker.minimumDate = minDate
//        datePicker.maximumDate = maxDate
        datePicker.timeZone =  TimeZone.current
        
        datePicker.backgroundColor = UIColor.lightGray
        
        if #available(iOS 13.4, *) {
            datePicker.preferredDatePickerStyle = .wheels
            toolbar.frame = CGRect.init(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 40)
        }
        
        datePicker.frame = CGRect(x: 0, y: 40, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height * 0.4)
        myView.frame = CGRect(x: 0, y:((UIScreen.main.bounds.height * 0.6) - 40), width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height * 0.4)
        
        if #available(iOS 13.4, *) {
            datePicker.alignmentRect(forFrame: myView.frame)
        }
        
        myView .addSubview(toolbar)
        myView .addSubview(datePicker)
        myView.backgroundColor = UIColor.white
        
        self.view.addSubview(myView)
        
    }
    
    @objc func doneDatePicker() {
        //For date formate
//        let todayDate = MBUtilities.todayDate(dateFormate: Constants.kDisplayFormat)
        
        MBUserSession.shared.dateFormatter.dateFormat = Constants.kDisplayFormat
        MBUserSession.shared.dateFormatter.timeZone = TimeZone.current
        
        let selectedDate = MBUserSession.shared.dateFormatter.string(from: datePicker.date)

//        let startFieldDate = MBUserSession.shared.dateFormatter.date(from: dateLabel.text ?? "")
//        let endFieldDate = MBUserSession.shared.dateFormatter.date(from: endDateLb.text ?? "")
        
        //dismiss date picker dialog
        self.view.endEditing(true)
        myView .removeFromSuperview()
        
        MBUserSession.shared.dateFormatter.dateFormat = Constants.kAPIFormat
        dateLabel.text = selectedDate
        
        //setting day for week days from date
        let weekday = Calendar.current.component(.weekday, from: MBUserSession.shared.dateFormatter.createDate(from: selectedDate) ?? Date().todayDate())
        self.weekDayIndex = weekday - 1
        self.weekdaysCollectionView.reloadData()
    
        
        /*if isStartDate ?? true {
            startDateLb.text = selectedDate
            self.startDateLb.textColor = UIColor.MBBarOrange
            
            self.getUsageHistoryAPICall(StartDate: MBUserSession.shared.dateFormatter.string(from: datePicker.date), EndDate: MBUserSession.shared.dateFormatter.string(from:endFieldDate ?? todayDate))
            
        } else {
            
            endDateLb.text = selectedDate
            self.endDateLb.textColor = UIColor.MBBarOrange
            
            self.getUsageHistoryAPICall(StartDate: MBUserSession.shared.dateFormatter.string(from: datePicker.date), EndDate: MBUserSession.shared.dateFormatter.string(from:endFieldDate ?? todayDate))
        } */
    }
    
    @objc func cancelDatePicker(){
        //cancel button dismiss datepicker dialog
        self.view.endEditing(true)
        myView .removeFromSuperview()
        print("cancel")
    }
}

//MARK: - Textfeild delegate
extension SchedulePaymentVC: UITextFieldDelegate{
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if  textField == numberOfRecureTF{
            if  !(self.numberOfRecureTF.text?.isBlank ?? false){
                self.noOfWeekOrMonthTF.text = numberOfRecureTF.text
            }
            
        }
    }
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if let char = string.cString(using: String.Encoding.utf8) {
                let isBackSpace = strcmp(char, "\\b")
                if (isBackSpace == -92) {
                    return true
                }
            }
        
        if textField == amountTF {
            
            
            let newString = (textField.text! as NSString).replacingCharacters(in: range, with: string)
            let arrayOfString = newString.components(separatedBy: ".")
            if amountTF.text?.length == 0 && string.toInt < 1 {
                return false
            } else if  arrayOfString.count > 1{
                if arrayOfString.last?.count ?? 0 > 2 {
                    return false
                }
            }
            return true
        } else {
            if textField == numberOfRecureTF {
                if numberOfRecureTF.text?.length == 0 && string.toInt < 1 {
                    return false
                }
                return true
            }
        }
        return true
    }
    
}




// MARK: Table Delegates & Datasource..
extension SchedulePaymentVC : UITableViewDataSource, UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.savedCards?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, titleForDeleteConfirmationButtonForRowAt indexPath: IndexPath) -> String? {
        return Localized("DeleteButtonTitle")
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: CardCell.identifier, for: indexPath) as! CardCell
        cell.setUp(withData: self.savedCards?[indexPath.row])
        if indexPath.row == self.selectedCardIndex{
            cell.cardNumber.textColor = .red
            cell.radioSelect.image = #imageLiteral(resourceName: "radio active")
        }else{
            cell.cardNumber.textColor = #colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1)
            cell.radioSelect.image = #imageLiteral(resourceName: "Checkbox-state2")
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.selectedCardIndex = indexPath.row
        self.tableView.reloadData()
        
    }
    
    func setGrayButton(_ sender : UIButton, withTitle title : String?, isGrayBackground  gray: Bool) {
        sender.setTitle(title, for: UIControl.State.normal)
        sender.titleLabel?.font = UIFont.MBArial(fontSize: 16)
        sender.backgroundColor =  gray ?  UIColor.MBButtonBackgroundGrayColor : UIColor.white
        sender.layer.borderColor = UIColor.MBBorderGrayColor.cgColor
        sender .setTitleColor(gray ? UIColor .MBTextBlack : UIColor .MBDarkGrayColor, for: UIControl.State.normal)
    }
    
    
}

// MARK: CollectionView Delegates & Datasource..
extension SchedulePaymentVC: UICollectionViewDelegate,UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.weekDays.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = weekdaysCollectionView.dequeueReusableCell(withReuseIdentifier: DayCell.identifier, for: indexPath) as! DayCell
        cell.dayLabel.text = self.weekDays[indexPath.row]
        if indexPath.row == weekDayIndex{
            cell.backgroundColor = .red
            cell.dayLabel.textColor = .white
        }else{
            cell.dayLabel.textColor = #colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1)
            cell.backgroundColor = #colorLiteral(red: 0.9253990054, green: 0.9255540371, blue: 0.925378561, alpha: 1)
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if self.autoCheckBtn.currentImage == UIImage(named: "Checkbox-state-2") {
//            self.weekDayIndex =  indexPath.row
            self.weekdaysCollectionView.reloadData()
        }
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let width = (weekdaysCollectionView.frame.size.width - 1) / 7
        
        return CGSize(width: width, height: 30.0)
    }
}


//MARK: API Calls...

extension SchedulePaymentVC{
    
    
    /// Call 'addpaymentscheduler' API.
    ///
    /// - returns: Void
    
    
    func  addPaymentScheduler(amount: String, billingCycle: String, startDate: String, recurrenceNumber: String, savedCardId: String){
        activityIndicator.showActivityIndicator()
        _ = MBAPIClient.sharedClient.addPaymentScheduler(amount: amount, billingCycle: billingCycle, startDate: startDate, recurrenceNumber: recurrenceNumber, savedCardId: savedCardId){ ( response, resultData, error, isCancelled, status, resultCode, resultDesc) in
            self.activityIndicator.hideActivityIndicator()
            if error != nil {
                //EventLog
                MBAnalyticsManager.logEvent(screenName: "Top up Screen", contentType:"Top up Scratch Card Failure" , status:"Failure" )
                
                error?.showServerErrorInViewController(self)
                
            } else {
                // handling data from API response.
                if resultCode == Constants.MBAPIStatusCode.succes.rawValue {
                    
//                    //EventLog
//                    MBAnalyticsManager.logEvent(screenName: "Top up Screen", contentType:"Top up Scratch Card Success" , status:"Success" )
//
//                    // Set true to reload Home page data
//                    MBUserSession.shared.shouldReloadDashboardData = true
//
                    self.showSuccessAlertWithMessage(message: resultDesc) { (str) in
                        self.navigationController?.popViewController(animated: true)
                    }
                    
                   
                    
                } else {
                    //EventLog
//                    MBAnalyticsManager.logEvent(screenName: "Top up Screen", contentType:"Top up Scratch Card Failure" , status:"Failure" )
                    
                    // Show error alert to user
                    self.showErrorAlertWithMessage(message: resultDesc)
                }
            }
        }
    }
    
    
    
    
    /// Call 'getsavedcards' API.
    ///
    /// - returns: Void
    func  getSavedCards(){
        activityIndicator.showActivityIndicator()
        _ = MBAPIClient.sharedClient.getSavedCards({ ( response, resultData, error, isCancelled, status, resultCode, resultDesc) in
            self.activityIndicator.hideActivityIndicator()
            if error != nil {
                //EventLog
//                MBAnalyticsManager.logEvent(screenName: "Top up Screen", contentType:"Top up Scratch Card Failure" , status:"Failure" )
                
//                error?.showServerErrorInViewController(self)
                
            } else {
                // handling data from API response.
                if resultCode == Constants.MBAPIStatusCode.succes.rawValue {
                    
//                    //EventLog
//                    MBAnalyticsManager.logEvent(screenName: "Top up Screen", contentType:"Top up Scratch Card Success" , status:"Success" )
//
//                    // Set true to reload Home page data
//                    MBUserSession.shared.shouldReloadDashboardData = true
//
                    //parsing data...
                    
                    if let resp =  Mapper<SavedCardsObj>().map(JSONObject:resultData){
                        self.savedCards = resp.cardDetails
                        
                        //FIXME: - Change the check to ">" for  new/first time case check... otherwise use "<="
                        if self.savedCards?.count ?? 0 <= 0{
                           
                        }else{
                           
                        }
                        self.tableView.reloadData()
                    }
                    
                    
                } else {
                    //EventLog
//                    MBAnalyticsManager.logEvent(screenName: "Top up Screen", contentType:"Top up Scratch Card Failure" , status:"Failure" )
                    
                    // Show error alert to user
                    self.showErrorAlertWithMessage(message: resultDesc)
                }
            }
        })
    }
}
