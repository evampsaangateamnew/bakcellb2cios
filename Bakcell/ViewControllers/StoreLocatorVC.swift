//
//  StoreLocatorVC.swift
//  Bakcell
//
//  Created by Saad Riaz on 6/7/17.
//  Copyright © 2017 evampsaanga. All rights reserved.
//

import UIKit
import ObjectMapper

class StoreLocatorVC: BaseVC {

    enum MBStoreLocatorType : String {
        case StoreLocator = "Store locator"
        case StoreList = "Store list"
    }
    
    var storeLocatorData : StoreLocatorHandler?
    var selectedType : MBStoreLocatorType = MBStoreLocatorType.StoreLocator

    var storeListVC :  StoreListVC?
    var mapVC :  MapVC?

    var shouldFocuseToSelectedLocation : Bool = false
    var selectedLocationForFocuse : [String: String]? = [:]

    
    @IBOutlet var title_lbl: UILabel!
    @IBOutlet var mainView: UIView!
    @IBOutlet var storeLocator_btn: UIButton!
    @IBOutlet var storeList_btn: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        layoutViewController()

        storeLocator_btn.layer.cornerRadius = 17
        storeLocator_btn.layer.borderWidth = 1
        storeLocator_btn.layer.borderColor = UIColor .MBBorderGrayColor.cgColor
        
        storeList_btn.layer.cornerRadius = 17
        storeList_btn.layer.borderWidth = 1
        storeList_btn.layer.borderColor = UIColor .MBBorderGrayColor.cgColor
        
        
        if selectedType == MBStoreLocatorType.StoreLocator {
            self.StoreLocatorPressed(storeLocator_btn)
        } else {
            self.StoreListPressed(storeList_btn ?? UIButton())
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.interactivePop(true)
        
        //EventLog
        MBAnalyticsManager.logEvent(screenName: "Store Locator", contentType:"Store Locator Map" , status:"Store Locator Map" )
    }
    
    //MARK: - Functions
    
    func layoutViewController(){
        title_lbl.text = Localized("Title_StoreLocator")
        storeLocator_btn .setTitle(Localized("StoreLocator_buttonTitle"), for: UIControl.State.normal)
        storeList_btn .setTitle(Localized("StoreList_buttonTitle"), for: UIControl.State.normal)
        
    }

    func focusUserToSelectedLocation(selectedLocationDetail : [String:String]?) {


        // Set True to focuse to specifice location
        self.shouldFocuseToSelectedLocation = true
        self.selectedLocationForFocuse = selectedLocationDetail

        // Load MapView
        StoreLocatorPressed(storeLocator_btn)
    }


    func reDirectUserToSelectedScreen() {
        
        if storeLocatorData != nil {

            if selectedType == MBStoreLocatorType.StoreLocator {

                let children = self.children
                for vc in children {
                    if vc is StoreListVC {
                        vc.view.removeFromSuperview()
                        vc.removeFromParent()
                    }
                }

                if (mapVC == nil) {
                    mapVC = self.myStoryBoard.instantiateViewController(withIdentifier: "MapVC") as? MapVC
                }
                mapVC?.storeLocator = storeLocatorData

                mapVC?.shouldFocuseToSelectedLocation = self.shouldFocuseToSelectedLocation
                mapVC?.selectedLocationForFocuse = self.selectedLocationForFocuse

                self.addChild(mapVC ?? MapVC())
                mainView.addSubview(mapVC?.view ?? UIView())
                mapVC?.didMove(toParent: self)

                mapVC?.view.snp.makeConstraints { make in
                    make.top.equalTo(mainView.snp.top).offset(0.0)
                    make.right.equalTo(mainView.snp.right).offset(0.0)
                    make.left.equalTo(mainView.snp.left).offset(0.0)
                    make.bottom.equalTo(mainView.snp.bottom).offset(0.0)
                }

                // Clear selected location once map loaded.
                self.shouldFocuseToSelectedLocation = false
                self.selectedLocationForFocuse = [:]


            } else {

                let children = self.children
                for vc in children {
                    if vc is MapVC {
                        vc.view.removeFromSuperview()
                        vc.removeFromParent()
                        //break
                    }
                }

                if storeListVC == nil {
                    storeListVC = self.myStoryBoard.instantiateViewController(withIdentifier: "StoreListVC") as? StoreListVC
                }

                storeListVC?.storeLocatorData = storeLocatorData

                self.addChild(storeListVC ?? StoreListVC())
                mainView.addSubview(storeListVC?.view ?? UIView())
                storeListVC?.didMove(toParent: self)
                storeListVC?.view.snp.makeConstraints { make in
                    make.top.equalTo(mainView.snp.top).offset(0.0)
                    make.right.equalTo(mainView.snp.right).offset(0.0)
                    make.left.equalTo(mainView.snp.left).offset(0.0)
                    make.bottom.equalTo(mainView.snp.bottom).offset(0.0)
                }
            }
        } else {
            getStoresLocationDetails()
        }
        
    }
    
    //MARK: IBACTIONS
    @IBAction func StoreLocatorPressed(_ sender: UIButton) {

        selectedType = MBStoreLocatorType.StoreLocator

        storeLocator_btn.backgroundColor = UIColor.MBButtonBackgroundGrayColor
        storeList_btn.backgroundColor = UIColor.white

        self.view.backgroundColor = UIColor.MBStoreGrayColor
        reDirectUserToSelectedScreen()
    }

    @IBAction func StoreListPressed(_ sender: Any) {

        selectedType = MBStoreLocatorType.StoreList

        storeLocator_btn.backgroundColor = UIColor .white
        storeList_btn.backgroundColor = UIColor.MBButtonBackgroundGrayColor

        self.view.backgroundColor = UIColor.white
        reDirectUserToSelectedScreen()
    }
    
    
    //MARK: - APIs call
    
    /// Call 'storeDetails' API.
    ///
    /// - returns: Void
    func getStoresLocationDetails (){

        /*Loading initial data from UserDefaults*/
        if let storeLocatorResponse : StoreLocatorHandler = StoreLocatorHandler.loadFromUserDefaults(key: APIsType.storeLocator.selectedLocalizedAPIKey()) {
            
            self.storeLocatorData = storeLocatorResponse
            self.reDirectUserToSelectedScreen()
            
        } else {
            activityIndicator.showActivityIndicator()
        }
        
        _ = MBAPIClient.sharedClient.storeDetails({ ( response, resultData, error, isCancelled, status, resultCode, resultDesc) in
            
            self.activityIndicator.hideActivityIndicator()
            if error != nil {
                //EventLog
                MBAnalyticsManager.logEvent(screenName: "Store Locator", contentType:"Store Locator Map" , status:"Failure" )
                
                error?.showServerErrorInViewController(self)
                
            } else {
                // handling data from API response.
                if resultCode == Constants.MBAPIStatusCode.succes.rawValue {
                    
                    //EventLog
                    MBAnalyticsManager.logEvent(screenName: "Store Locator", contentType:"Store Locator Map" , status:"Success" )

                    // Parssing response data
                    if let storeLocatorResponse = Mapper<StoreLocatorHandler>().map(JSONObject:resultData) {

                        self.storeLocatorData = storeLocatorResponse

                        /*Save data into user defaults*/
                        storeLocatorResponse.saveInUserDefaults(key: APIsType.storeLocator.selectedLocalizedAPIKey())

                        self.reDirectUserToSelectedScreen()
                    }
                    
                } else {
                    //EventLog
                    MBAnalyticsManager.logEvent(screenName: "Store Locator", contentType:"Store Locator Map" , status:"Failure" )
                    
                    // Show error alert to user
                    self.showErrorAlertWithMessage(message: resultDesc)
                }
            }
        })
    }

}
