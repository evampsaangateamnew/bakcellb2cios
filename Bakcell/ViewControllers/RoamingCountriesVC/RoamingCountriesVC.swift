//
//  RoamingCountriesVC.swift
//  Bakcell
//
//  Created by AbdulRehman Warraich on 1/2/19.
//  Copyright © 2019 evampsaanga. All rights reserved.
//

import UIKit
import ObjectMapper

class RoamingCountriesVC: BaseVC {
    
    //MARK: - Properties
    var showSearch : Bool = true
    var countriesList : [CountryDetailList]?
    var filteredCountriesList : [CountryDetailList]?
    
    //MARK: - IBOutlet
    @IBOutlet var search_txt: UITextField!
    @IBOutlet var search_btn: UIButton!
    @IBOutlet var searchView: UIView!
    @IBOutlet var title_lbl: UILabel!
    @IBOutlet var tableView: UITableView!
    
    //MARK: - ViewContoller methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        loadViewLayout()
        
        search_txt.delegate = self
        searchView.isHidden = true
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 44
        
        tableView.register(UINib(nibName: "CountriesCell", bundle: nil), forCellReuseIdentifier: "CountriesCell")
        
        // reload data first
        reloadTableViewData()
        
        // Load Countries data
        loadRoamingsData()
    }
    
    //MARK: - IBActions
    
    @IBAction func searchPressed(_ sender: UIButton) {
        
        if showSearch == false {
            search_btn.setImage(UIImage (imageLiteralResourceName: "actionbar_search") , for: UIControl.State.normal)
            searchView.isHidden = true
            title_lbl.isHidden = false
            showSearch = true
            
            search_txt.text = ""
            search_txt.resignFirstResponder()
            
            searchCountryByName(searchString: "")
            
        } else {
            search_btn.setImage(UIImage (imageLiteralResourceName: "Cross") , for: UIControl.State.normal)
            title_lbl.isHidden = true
            searchView.isHidden = false
            showSearch = false
            
            search_txt.becomeFirstResponder()
            
        }
    }
    
    //MARK: - FUNCTIONS
    func loadViewLayout(){
        title_lbl.text = Localized("Title_RoamingPricePackages")// Localized("Title_RoamingCountries")
        
        search_txt.attributedPlaceholder = NSAttributedString(string: Localized("Title_Search"),
                                                              attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
    }
    
    
    func reloadTableViewData() {
        
        if (filteredCountriesList?.count ?? 0) <= 0 {
            self.tableView.showDescriptionViewWithImage(description: Localized("Message_NoDataAvalible"))
            
        } else {
            self.tableView.hideDescriptionView()
        }
        
        tableView.reloadData()
    }
    
    func loadRoamingsData() {
        // Load data from UserDefaults
        if let responseData : RoamingCountriesHandler = RoamingCountriesHandler.loadFromUserDefaults(key: APIsType.roamingCountries.selectedLocalizedAPIKey()) {
            
            self.countriesList = responseData.detailList?.copy()
            self.filteredCountriesList = responseData.detailList?.copy()
            
            self.reloadTableViewData()
            
            roamingCountriesAPICall(true)
            
        } else {
            roamingCountriesAPICall()
        }
    }
    
    //MARK: - API Calls
    func roamingCountriesAPICall(_ showIndicator: Bool = true) {
        
        if showIndicator {
            activityIndicator.showActivityIndicator()
        }
        
        _ = MBAPIClient.sharedClient.getRoamingCountries({ ( response, resultData, error, isCancelled, status, resultCode, resultDesc) in
            
            if showIndicator {
                self.activityIndicator.hideActivityIndicator()
            }
            
            if error != nil {
                error?.showServerErrorInViewController(self)
                
            } else {
                // handling data from API response.
                if resultCode == Constants.MBAPIStatusCode.succes.rawValue ,
                    let responseHandler = Mapper<RoamingCountriesHandler>().map(JSONObject:resultData) {
                        
                        // Saving Roaming Countries data in user defaults
                    responseHandler.saveInUserDefaults(key: APIsType.roamingCountries.selectedLocalizedAPIKey())
                       
                        //getting countries list
                        self.countriesList = responseHandler.detailList?.copy()
                        self.filteredCountriesList = responseHandler.detailList?.copy()
                    
                } else {
                    // Show error alert to user
                    self.showErrorAlertWithMessage(message: resultDesc)
                }
            }
            
            // Reload table View data
            self.reloadTableViewData()
        })
    }
}

//MARK: - Table View Delegates

extension RoamingCountriesVC : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return filteredCountriesList?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let cell = tableView.dequeueReusableCell(withIdentifier: "CountriesCell", for: indexPath) as? CountriesCell {
            
            let tempCountryObj = filteredCountriesList?[indexPath.row]
            cell.title_lbl.text = tempCountryObj?.countryName ?? ""
            
            return cell
        }
        
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        // load Roaming Countries viewController
        if let operatoriesVC = self.myStoryBoard.instantiateViewController(withIdentifier: "RoamingOperatorsVC") as? RoamingOperatorsVC{
            operatoriesVC.countryDetail = filteredCountriesList?[indexPath.row]
            self.navigationController?.pushViewController(operatoriesVC, animated: true)
        }
    }
}

extension RoamingCountriesVC : UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        guard let text = textField.text else { return false }
        
        if textField == search_txt {
            
            let newString = (text as NSString).replacingCharacters(in: range, with: string)
            self.searchCountryByName(searchString: newString)
        }
        
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return false
    }
    
    func searchCountryByName(searchString: String?) {
        
        if searchString?.isBlank ?? true {
            
            filteredCountriesList = self.countriesList
        } else {
            
            filteredCountriesList = self.countriesList?.filter {
                
                return $0.countryName?.containsSubString(subString: searchString) ?? false
            }
        }
        
        self.reloadTableViewData()
    }
    
}

