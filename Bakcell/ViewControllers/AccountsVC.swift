//
//  AccountsVC.swift
//  Bakcell
//
//  Created by Saad Riaz on 6/8/17.
//  Copyright © 2017 evampsaanga. All rights reserved.
//

import UIKit

class AccountsVC: BaseVC,UITableViewDelegate,UITableViewDataSource {

    //MARK: - Properties
    var titleArr: [String] = []
    var images: [String] = ["My-subscriptions","Usage-History","Operations-history","Installments"]
    
    //MARK: - IBOutlet
    @IBOutlet var tableView: UITableView!
    @IBOutlet var accountTitle_lbl: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.interactivePop(true)
        
        titleArr = [Localized("Op1_MySubscriptions"),Localized("Op2_UsageHistory"),Localized("Op3_OperationsHistory"),Localized("Op4_MyInstallments")]
        loadViewLayout()
    }

    //MARK: - FUNCTIONS
    func loadViewLayout(){
        accountTitle_lbl.font = UIFont.MBArial(fontSize: 20)
        accountTitle_lbl.text = Localized("Title_Accounts")
        tableView.reloadData()
        
    }
    
    //MARK: - TABLE VIEW METHODS
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return titleArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellID", for: indexPath) as! AccountsCell
        cell.title_lbl.font = UIFont.MBArial(fontSize: 14)
        cell.title_lbl.text = titleArr[indexPath.row]
        cell.img.image = UIImage (named: images[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.row {
        case 0:
            let subscriptions = self.myStoryBoard.instantiateViewController(withIdentifier: "MySubscribeVC") as! MySubscribeVC
            self.navigationController?.pushViewController(subscriptions, animated: true)
        case 1:
            let usage = self.myStoryBoard.instantiateViewController(withIdentifier: "UsageSummaryVC") as! UsageSummaryVC
            self.navigationController?.pushViewController(usage, animated: true)
        case 2:
            let operations = self.myStoryBoard.instantiateViewController(withIdentifier: "OperationsHistoryVC") as! OperationsHistoryVC
            self.navigationController?.pushViewController(operations, animated: true)
        case 3:
            let installments = self.myStoryBoard.instantiateViewController(withIdentifier: "MyInstallmentsVC") as! MyInstallmentsVC
            self.navigationController?.pushViewController(installments, animated: true)

        default:
            break
        }
        tableView.deselectRow(at: indexPath, animated: true)
    }
}
