//
//  SignupVC.swift
//  Bakcell
//
//  Created by Shujat on 16/05/2017.
//  Copyright © 2017 evampsaanga. All rights reserved.
//

import UIKit
import ObjectMapper

class SignupVC: BaseVC {

    //MARK:- Properties
    var registrationType :Constants.RegistrationType = .registration
    var isForgotPassword : Bool = false

    //MARK:- IBOutlet
    @IBOutlet var mobileNumber_txt: UITextField!
    @IBOutlet var signInView: UIView!
    @IBOutlet var accountLbl: UILabel!
    @IBOutlet var lineView: UIView!

    @IBOutlet var nextButtonView: UIView!
    @IBOutlet var sentPin_lbl: UILabel!
    @IBOutlet var next_lbl: UILabel!
    @IBOutlet var signUp_lbl: UILabel!
    @IBOutlet var backcelIcon_img: UIImageView!

    //MARK:- ViewController methods
    override func viewDidLoad() {
        super.viewDidLoad()
        mobileNumber_txt.delegate = self

        // Disabling
        setNextButtonSelection(buttonView: nextButtonView, isEnabled: false)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.interactivePop(true)
        
        loadViewLayout()
        
        if registrationType == .addAccount {
            self.backButton?.isHidden = false
        } else {
            self.backButton?.isHidden = true
        }
    }

    //MARK: IBACTIONS
    @IBAction func nextPressed(_ sender: Any) {

        var msisdn : String = ""
        // MSISDN validation
        if let mobileNumberText = mobileNumber_txt.text,
            mobileNumberText.count == 9 {

            if registrationType == .addAccount,
                MBUserSession.shared.loggedInUsers?.users?.contains(where: {($0.userInfo?.msisdn ?? "").isEqualToStringIgnoreCase(otherString: mobileNumberText)}) ?? false {
                self.showErrorAlertWithMessage(message: Localized("Error_AddNumber"))
                return
            } else {
                msisdn = mobileNumberText
            }
        } else {
            self.showErrorAlertWithMessage(message: Localized("Message_EnterValidNumber"))
            return
        }

        // TextField resignFirstResponder
        mobileNumber_txt.resignFirstResponder()

        //API Call
        if isForgotPassword {
            signUpUserAPICall(MSISDN: msisdn,IsSignUpRequest: false)
        } else {
            signUpUserAPICall(MSISDN: msisdn,IsSignUpRequest: true)
        }
    }

    @IBAction func signInPressed(_ sender: UIButton) {

        self.navigationController?.popViewController(animated: true)

        /* Uncomment below code to chage trasition from rightToLeft for popViewController */

        //  let login = self.myStoryboard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
        /* let transition = CATransition()
         transition.duration = 0.5
         transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
         transition.type = kCATransitionPush
         transition.subtype = kCATransitionFromRight
         self.navigationController?.view.layer.add(transition, forKey: nil)
         _ = self.navigationController?.popViewController(animated: true) */

    }

    //MARK: - FUNCTIONS

    /**
     Set localized text in viewController
     */
    func loadViewLayout(){
        sentPin_lbl.font = UIFont.MBArial(fontSize: 14)
        signUp_lbl.font = UIFont.MBArialBold(fontSize: 16)
        next_lbl.font = UIFont.MBArialBold(fontSize: 16)
        accountLbl.font = UIFont.MBArial(fontSize: 14)
        
        sentPin_lbl.text = Localized("Info_PinSend")
        signUp_lbl.text = Localized("Title_SignIn")
        next_lbl.text = Localized("BtnTitle_NEXT")
        accountLbl.text = Localized("Info_AlreadyHaveAcount")
        mobileNumber_txt.placeholder = Localized("Placeholder_MobileNumber")

        backcelIcon_img.image = UIImage(named: Localized("Img_BakcelLogo"))
    }


    //MARK: - API Calls

    /**
     Call 'signup' API with the specified `MSISDN`.

     - parameter MSISDN: MSISDN of current user.

     - returns: void
     */
    func signUpUserAPICall(MSISDN msisdn : String, IsSignUpRequest isSignUpRequest:Bool ) {

        activityIndicator.showActivityIndicator()
        _ = MBAPIClient.sharedClient.signUp(MSISDN: msisdn, requestForSignUp: isSignUpRequest,{ (response, resultData, error, isCancelled, status, resultCode, resultDesc) in
            self.activityIndicator.hideActivityIndicator()
            if error != nil {
                error?.showServerErrorInViewController(self)
                if self.isForgotPassword == false {
                    //EventLog
                    MBAnalyticsManager.logEvent(screenName: "Signup Screen Step1", contentType:"Number Verification Failure" , status:"Failure" )
                } else {
                    MBAnalyticsManager.logEvent(screenName: "Forgotpassword Screen Step1", contentType:"Number Verification Failure" , status:"Failure" )
                }

            } else {
                // handling data from API response.
                if resultCode == Constants.MBAPIStatusCode.succes.rawValue {
                    if self.isForgotPassword == false {
                        //EventLog
                        MBAnalyticsManager.logEvent(screenName: "Signup Screen Step1", contentType:"Number Verification Success" , status:"Success" )
                    } else {
                        MBAnalyticsManager.logEvent(screenName: "Forgotpassword Screen Step1", contentType:"Number Verification Success" , status:"Success" )
                    }

                    // Parssing response data
                    if let userSignUpHandler = Mapper<SignUp>().map(JSONObject:resultData) {

                        // Redirecting user to Pin verification screen.
                        if let pinVerify = self.myStoryBoard.instantiateViewController(withIdentifier: "SignUpPINVerifyVC") as? SignUpPINVerifyVC {
                            pinVerify.msisdn = msisdn
                            pinVerify.isForgotPassword = self.isForgotPassword
                            pinVerify.registrationType = self.registrationType
                            pinVerify.signUpUserInfo = userSignUpHandler
                            self.navigationController?.pushViewController(pinVerify, animated: true)
                        }
                    }
                } else {
                    // Show error alert to user
                    self.showErrorAlertWithMessage(message: resultDesc)
                    
                    if self.isForgotPassword == false{
                        //EventLog
                        MBAnalyticsManager.logEvent(screenName: "Signup Screen Step1", contentType:"Number Verification Failure" , status:"Failure" )
                    } else {
                        MBAnalyticsManager.logEvent(screenName: "Forgotpassword Screen Step1", contentType:"Number Verification Failure" , status:"Failure" )
                    }
                }
            }
        })
    }
}

//MARK: - Textfield delagates

extension SignupVC : UITextFieldDelegate {
    //Set max Limit for textfields
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let text = textField.text else { return false } // Check that if text is nill then return false

        let newLength = text.count + string.count - range.length

        if textField == mobileNumber_txt {

            let allowedCharacters = CharacterSet(charactersIn: Constants.allowedNumbers)
            let characterSet = CharacterSet(charactersIn: string)
            if newLength <= 9 && allowedCharacters.isSuperset(of: characterSet) {

                // Next button enable disable
                if newLength == 9 {
                    setNextButtonSelection(buttonView: nextButtonView, isEnabled: true)
                } else {
                    setNextButtonSelection(buttonView: nextButtonView, isEnabled: false)
                }
                // Returnimg number input
                return  true
            } else {
                return  false
            }
            
        } else  {
            return newLength <= 15 // Bool
        }
    }
}

