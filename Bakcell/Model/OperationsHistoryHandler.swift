//
//  OperationsHistoryHandler.swift
//  Bakcell
//
//  Created by Saad Riaz on 9/26/17.
//  Copyright © 2017 evampsaanga. All rights reserved.
//

import Foundation
import ObjectMapper

class OperationsHistoryHandler : Mappable{
    
    var records : [OperationsRecord]?
    
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        
        records        <- map["records"]
    }
}
class OperationsRecord : Mappable{

    var primaryIdentity : String = ""
    var date : String = ""
    var transactionType : String = ""
    var description : String = ""
    var amount : String = ""
    var endingBalance : String = ""
    var clarification : String = ""
    var currency : String = ""


    required init?(map: Map) {
    }
    
    func mapping(map: Map) {

        primaryIdentity         <- map["primaryIdentity"]
        date                    <- map["date"]
        transactionType         <- map["transactionType"]
        description             <- map["description"]
        amount                  <- map["amount"]
        endingBalance           <- map["endingBalance"]
        clarification           <- map["clarification"]
        currency                <- map["currency"]
    }
}
