//
//  TabbarController.swift
//  Bakcell
//
//  Created by Saad Riaz on 5/17/17.
//  Copyright © 2017 evampsaanga. All rights reserved.
//

import UIKit
import ObjectMapper
import Alamofire
import FirebaseMessaging

class TabbarController: UITabBarController {

    //MARK:- Properties
    let activityIndicator : MBActivityIndicator = MBActivityIndicator()
    var isLoggedInNow : Bool = false
    var isAppResumeAPICallInProcess :Bool = false

    //MARK:- ViewController Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tabBar.backgroundColor = UIColor.white
        
        /* Check if user just loggedIn then don't call AppResume api */
        if isLoggedInNow {
            
            /* Calling AppResume */
            loadCustomerInfo(callResumeAPI: false)
            /* reset check */
            isLoggedInNow = false
            
        } else { /* Check if user is not loggedIn now then call AppResume api */
            
            /* Calling AppMenu in case user just loggedIn/ Forgot/ SignUp */
            loadCustomerInfo(callResumeAPI: true)
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        self.viewControllers?.forEach({ (aController) in
            
            if aController.isKind(of: DashboardVC().classForCoder) {
                aController.tabBarItem.title = Localized("Title_DashBoard")
            } else if aController.isKind(of: TariffsMainVC().classForCoder)  {
                aController.tabBarItem.title =  Localized("Title_Tariffs")
            }else if aController.isKind(of: ServicesVC().classForCoder) {
                aController.tabBarItem.title = Localized("Services_Title")
            }else if aController.isKind(of: RechargeVC().classForCoder) {
                aController.tabBarItem.title = Localized("Title_TopUP")
            }else if aController.isKind(of: RoamingMainVC().classForCoder) {
                aController.tabBarItem.title = Localized("Title_Roaming")
            }else if aController.isKind(of: AccountsVC().classForCoder) {
                aController.tabBarItem.title = Localized("Title_Accounts")
            }
        })
        
        /* shouldCallAndUpdateAppResumeData will be true in case use change his current language or migrate tariff */
        if MBUserSession.shared.shouldCallAndUpdateAppResumeData == true {
            
            /* Calling AppResume and load information of new selected language */
            getAppResume()
            
            /* Reset changed language check*/
            MBUserSession.shared.shouldCallAndUpdateAppResumeData = false
        }
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)

        activityIndicator.hideActivityIndicator()
    }

    //MARK:- Functios

    /**
     Sort AppMenu data and update user session and load tabbar options.
     
     - parameter appMenuData: information of AppMenu.
     
     - returns: void.
     */
    func sortAndLoadAppMenu(_ appMenuData: AppMenusModel?, shouldLoadDashboardInfo :Bool) {
        
        /* Setting appMenu information */
        MBUserSession.shared.appMenu = appMenuData
        
        /* Sort AppMenus */
        MBUserSession.shared.appMenu?.menuHorizontal?.sort { $0.sortOrder.toInt < $1.sortOrder.toInt }
        MBUserSession.shared.appMenu?.menuVertical?.sort { $0.sortOrder.toInt < $1.sortOrder.toInt }
        
        /* Load options accourding to response */
        self.setTabBarMenuLayout()
        
        /* Call Dashboard information loading function. */
        if shouldLoadDashboardInfo,
            let dashboardObject = self.getObjectOfViewControllerOfType(Identifier: "dashboard", createNewObject: false) as? DashboardVC {
            
            let hasInfo = dashboardObject.loadHomePageInformation()
            dashboardObject.getHomePageInformation(sender: nil,
                                                   shouldShowLoader: (hasInfo ? false : true),
                                                   callOtherAPIS: true)
        }
    }
    
    /**
     update Tabbar layout setting accourding to new Data.

     - returns: void
     */
    func setTabBarMenuLayout() {

        var viewControllers: [UIViewController] = []

        // Loop throught each item in new tabbar data
        MBUserSession.shared.appMenu?.menuHorizontal?.forEach { (aMenuListItem) in

            // Getting object for View controller in tabBar against identifier
            if let aViewControllerObject = self.getObjectOfViewControllerOfType(Identifier: aMenuListItem.identifier) {

                viewControllers.append(aViewControllerObject)
            }
        }

        /* IN CASE there is no information exist create Dashboard object. */
        if viewControllers.count <= 0 {

            // Get instance first add into tabbar
            if let aViewController : UIViewController = getObjectOfViewControllerOfType(Identifier: "dashboard", createNewObject: true) {

                viewControllers.append(aViewController)
            } else {

                // if no instance found then create and add Dashboard object
                if let aViewController : DashboardVC = self.myStoryBoard.instantiateViewController(withIdentifier: "DashboardVC") as? DashboardVC {
                    aViewController.tabBarItem = UITabBarItem(title: Localized("Title_DashBoard"), image: UIImage(named: "1active"), selectedImage: UIImage(named: "1inactive"))
                    // Add EdgeInsets to align image.
                    // aViewController.tabBarItem.imageInsets = UIEdgeInsets.init(top: 6, left: 0, bottom: -6, right: 0)
                    viewControllers.append(aViewController)
                }
            }
        }
        // Replace tabbar controllers with new controller.
        self.setViewControllers(viewControllers, animated: true)
    }

    /**
     Get or Create object against identifier and return UIViewController object.

     - parameter identifier: ViewController identifier.

     - returns: UIViewController object againt identifier.
     */
    func getObjectOfViewControllerOfType(Identifier identifier: String, createNewObject :Bool = true) -> UIViewController? {

        var aViewController : UIViewController?

        // Find viewController against identifier from tabbar and
        self.viewControllers?.forEach({ (aController) in

            if aController.isKind(of: DashboardVC().classForCoder) && identifier == "dashboard" {
                aViewController = aController
                return
            } else if aController.isKind(of: TariffsMainVC().classForCoder) && identifier == "tariffs" {
                aViewController = aController
                return
            }else if aController.isKind(of: ServicesVC().classForCoder) && identifier == "services" {
                aViewController = aController
                return
            }else if aController.isKind(of: RechargeVC().classForCoder) && identifier == "topup" {
                aViewController = aController
                return
            }else if aController.isKind(of: RoamingMainVC().classForCoder) && identifier == "roaming" {
                aViewController = aController
                return
            }else if aController.isKind(of: AccountsVC().classForCoder) && identifier == "myaccount" {
                aViewController = aController
                return
            }
        })

        // If not able to find a viewController then create a new ViewController object and add
        if aViewController == nil &&
            createNewObject {
            
            aViewController = self.createInstanceOfViewControllerFor(Identifier: identifier)
        }
        //  aViewController?.tabBarItem.imageInsets = UIEdgeInsets.init(top: 6, left: 0, bottom: -6, right: 0)

        return aViewController
    }

    /**
     Create object against identifier and return UIViewController object.

     - parameter identifier: ViewController identifier.

     - returns: create UIViewController object againt identifier.
     */
    func createInstanceOfViewControllerFor(Identifier identifier : String) -> UIViewController? {

        if identifier == "myaccount" {

            if let aViewController : AccountsVC = self.myStoryBoard.instantiateViewController(withIdentifier: "AccountsVC") as? AccountsVC {
                aViewController.tabBarItem = UITabBarItem(title: Localized("Title_Accounts"), image: UIImage(named: "5active"), selectedImage: UIImage(named: "5inactive"))
                return aViewController
            }
        } else if identifier == "tariffs" {

            if let aViewController : TariffsMainVC = self.myStoryBoard.instantiateViewController(withIdentifier: "TariffsMainVC") as? TariffsMainVC {
                aViewController.tabBarItem = UITabBarItem(title:  Localized("Title_Tariffs"), image: UIImage(named: "2active"), selectedImage: UIImage(named: "2inactive"))
                return aViewController
            }
        } else if identifier == "services" {

            if let aViewController : ServicesVC = self.myStoryBoard.instantiateViewController(withIdentifier: "ServicesVC") as? ServicesVC {
                aViewController.tabBarItem = UITabBarItem(title: Localized("Services_Title"), image: UIImage(named: "3active"), selectedImage: UIImage(named: "3inactive"))
                return aViewController
            }
        } else if identifier == "topup" {
            
            if let aViewController : RechargeVC = self.myStoryBoard.instantiateViewController(withIdentifier: "RechargeVC") as? RechargeVC {
                aViewController.tabBarItem = UITabBarItem(title: Localized("Title_TopUP"), image: UIImage(named: "4active"), selectedImage: UIImage(named: "4inactive"))
                return aViewController
            }
        }  else if identifier == "roaming" {

            if let aViewController : RoamingMainVC = self.myStoryBoard.instantiateViewController(withIdentifier: "RoamingMainVC") as? RoamingMainVC {
                aViewController.tabBarItem = UITabBarItem(title: Localized("Title_Roaming"), image: UIImage(named: "roamingTemp"), selectedImage: UIImage(named: "roaming_menu"))
                return aViewController
            }
        } else if identifier == "dashboard" {
            
            if let aViewController : DashboardVC = self.myStoryBoard.instantiateViewController(withIdentifier: "DashboardVC") as? DashboardVC {
                aViewController.tabBarItem = UITabBarItem(title: Localized("Title_DashBoard"), image: UIImage(named: "1active"), selectedImage: UIImage(named: "1inactive"))
                return aViewController
            }
        }

        return nil
    }

    
    func loadCustomerInfo(callResumeAPI :Bool) {
        
        if callResumeAPI {
            /* Calling AppResume */
            getAppResume()
            
        } else {
            /* Calling AppMenu in case user just loggedIn/ Forgot/ SignUp */
            self.getAppMenu()
        }
    }
    
   
    //MARK: - API Calls

    /**
     call to get appResume data.

     - returns: void.
     */
    func getAppResume() {

        self.isAppResumeAPICallInProcess = true
        self.activityIndicator.showActivityIndicator()
        
        _ = MBAPIClient.sharedClient.appResume({ (response, resultData, error, isCancelled, status, resultCode, resultDesc) in

            self.isAppResumeAPICallInProcess = false
            self.activityIndicator.hideActivityIndicator()

            if error != nil {

                if MBUserSession.shared.loadUserInfomation() {
                    self.getAppMenu()
                } else {
                    error?.showServerErrorInViewController(self)
                }

            } else {
                if resultCode == Constants.MBAPIStatusCode.succes.rawValue {

                    // Parssing response data
                    if let authenticateUserHandler = Mapper<SaveCustomerModel>().map(JSONObject:resultData) {
                        
                        /* Update AppResume information in User Defaults */
                        MBUserInfoUtilities.updateLoggedInUserInfo(loggedInUserMSISDN: MBUserSession.shared.msisdn,
                                                                   loginInfo: authenticateUserHandler,
                                                                   appConfig: MBUserSession.shared.appConfig)
                        MBUserSession.shared.notificationPopupConfig = authenticateUserHandler.predefinedData?.notificationPopupConfig
                        
                    }
                    // Load App Menu Data
                    self.getAppMenu()
                    self.addFCMId()

                } else if resultCode != Constants.MBAPIStatusCode.sessionExpired.rawValue {
                    // Load App Menu Data
                    self.getAppMenu()
                    
                    self.addFCMId()
                }
            }
        })
    }

    /**
     call to get AppMenu data.
     
     - returns: void.
     */
    func getAppMenu() {
        
        var foundAppMenuData : Bool = false
        
        /* Loading initial data of menu from UserDefaults */
        if let appMenuHandler :AppMenusResponseModel = AppMenusResponseModel.loadFromUserDefaults(key: APIsType.appMenu.selectedAPIKey()),
            let currentMenu = appMenuHandler.getMenuForCurrentLanguage(MBLanguageManager.userSelectedLanguage()){
            
            /* If found information in userDefaults then hide loader and call in background and load. */
            if (currentMenu.menuVertical?.count ?? 0) <= 0 &&
                (currentMenu.menuHorizontal?.count ?? 0) <= 0 {
                
                foundAppMenuData = false
            } else {
                foundAppMenuData = true
                
                /* load appMenu information */
                self.sortAndLoadAppMenu(currentMenu, shouldLoadDashboardInfo: true)
            }
        }
        
        /* If found info then don't show loader for menu API*/
        if foundAppMenuData == false {
            self.activityIndicator.showActivityIndicator()
        }
        
        /* API call to get Menu data */
        _ = MBAPIClient.sharedClient.getAppMenu({ (response, resultData, error, isCancelled, status, resultCode, resultDesc) in
            
            if foundAppMenuData != true {
                self.activityIndicator.hideActivityIndicator()
            }
            
            if error != nil  {
                /* If error occur Durring AppMenu API call then show error */
                error?.showServerErrorInViewController(self)
                
                if foundAppMenuData == false {
                    /* clear App Menu and load dashboard info */
                    self.sortAndLoadAppMenu(nil, shouldLoadDashboardInfo: true)
                }
                
            } else if resultCode == Constants.MBAPIStatusCode.succes.rawValue,
                let appMenuHandler = Mapper<AppMenusResponseModel>().map(JSONObject: resultData) ,
                let currentMenu = appMenuHandler.getMenuForCurrentLanguage(MBLanguageManager.userSelectedLanguage()) { /* handling data from API response. */
                
                /*Save data into user defaults*/
                appMenuHandler.saveInUserDefaults(key: APIsType.appMenu.selectedAPIKey())
                
                /* load appMenu information and call Load Dashboard, if Menu data loaded from defaults then don't call loadDashboard API again */
                self.sortAndLoadAppMenu(currentMenu, shouldLoadDashboardInfo: foundAppMenuData ? false : true)
                
            } else { /* If error occur Durring AppMenu API call then show error. */
                self.showErrorAlertWithMessage(message: resultDesc)
                
                if foundAppMenuData == false {
                    /* clear App Menu and load dashboard info */
                    self.sortAndLoadAppMenu(nil, shouldLoadDashboardInfo: true)
                }
            }
            self.getSurveys()
        })
    }
    
    func addFCMId() {
        
        if let fcmToken = Messaging.messaging().fcmToken {
            
            /* Call add FCMId API*/
            _ = MBAPIClient.sharedClient.addFCMId(fcmKey: fcmToken, ringingStatus: .Tone, isEnable: true, isFromLogin: true, { (response, resultData, error, isCancelled, status, resultCode, resultDesc) in
                
                if resultCode == Constants.MBAPIStatusCode.succes.rawValue ,
                    let addFCMResponseHandler = Mapper<AddFCM>().map(JSONObject:resultData) {
                    
                    // Save Notification configration
                    addFCMResponseHandler.saveInUserDefaults(key: APIsType.notificationConfigurations.selectedLocalizedAPIKey())
                    
                }
            })
        }
    }
    
    
    //MARK: Get survey====  put this service call somewhere else where needed
    
    func getSurveys() {
        
        activityIndicator.showActivityIndicator()
        _ = MBAPIClient.sharedClient.getSurveys({ (response, resultData, error, isCancelled, status, resultCode, resultDesc) in
            
            self.activityIndicator.hideActivityIndicator()
            if error != nil {
                error?.showServerErrorInViewController(self)
                
            } else {
                // handling data from API response.
                if resultCode == Constants.MBAPIStatusCode.succes.rawValue ,
                    let homePageResponse = Mapper<InAppSurveyMapper>().map(JSONObject:resultData) {
                    MBUserSession.shared.appSurvays = homePageResponse
                } else {
                    //EventLog
                    MBAnalyticsManager.logEvent(screenName: "Login Screen", contentType:"User failed to Login" , status:"Failed" )
                    // Show error alert to user
                    self.showErrorAlertWithMessage(message: resultDesc)
                }
            }
        })
    }
}
