//
//  FreeResourceValidityCell.swift
//  Bakcell
//
//  Created by AbdulRehman Warraich on 8/30/17.
//  Copyright © 2017 evampsaanga. All rights reserved.
//

import UIKit

class FreeResourceValidityCell: UITableViewCell {

    static let identifier : String = "FreeResourceValidityCell"
    @IBOutlet var title_lable: UILabel!
    @IBOutlet var titleValue_lbl: UILabel!
    @IBOutlet var subTitle_lbl: UILabel!
    @IBOutlet var subTilteValue_lbl: UILabel!
    @IBOutlet var description_lbl: UILabel!
    @IBOutlet var descriptionLabelHeightConstraint: NSLayoutConstraint!

    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func setFreeResourceValidityValues(freeResourceValidity : FreeResourceValidity?) {

        if let freeResourceValidity = freeResourceValidity {
            title_lable.text = freeResourceValidity.title ?? ""
            titleValue_lbl.text = freeResourceValidity.titleValue ?? ""
            subTitle_lbl.text = freeResourceValidity.subTitle ?? ""
            subTilteValue_lbl.text = freeResourceValidity.subTitleValue ?? ""

            if let description = freeResourceValidity.description {
                description_lbl.loadHTMLString(htmlString: description)
                descriptionLabelHeightConstraint.constant = description_lbl.heightToFitContent()
            } else {
                description_lbl.text = ""
                descriptionLabelHeightConstraint.constant = 0
            }

        }
    }
    
}
