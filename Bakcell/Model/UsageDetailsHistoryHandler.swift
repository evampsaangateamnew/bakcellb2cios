//
//  UsageDetailsHistoryHandler.swift
//  Bakcell
//
//  Created by Awais Shehzad on 25/09/2017.
//  Copyright © 2017 evampsaanga. All rights reserved.
//

import Foundation
import ObjectMapper

protocol Copyable {
    init(instance: Self)
}

extension Copyable {
    func copy() -> Self {
        return Self.init(instance: self)
    }
}

class UsageDetailsHistoryHandler: Mappable, Copyable {


    var records : [DetailsHistory]?

    init() {
    }
    required init(instance: UsageDetailsHistoryHandler) {
        self.records = instance.records
    }

    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        records          <- map["records"]
    }
}

//DetailsHistory : [...]
class DetailsHistory: Mappable {
    var chargedAmount : String = ""
    var destination : String = ""
    var startDateTime : String = ""
    var endDateTime : String = ""
    var number : String = ""
    var period : String = ""
    var service : String = ""
    var type : String?
    var usage : String = ""
    var unit : String = ""
    var zone : String = ""
    
    required init?(map : Map) {
        
    }
   
    func mapping(map: Map) {
        
        chargedAmount       <- map["chargedAmount"]
        destination         <- map["destination"]
        startDateTime       <- map["startDateTime"]
        endDateTime         <- map["endDateTime"]
        number              <- map["number"]
        period              <- map["period"]
        service             <- map["service"]
        type                <- map["type"]
        usage               <- map["usage"]
        unit                <- map["unit"]
        zone                <- map["zone"]
    }
}
