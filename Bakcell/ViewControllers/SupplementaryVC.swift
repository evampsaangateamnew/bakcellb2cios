//
//  SupplementaryVC.swift
//  Bakcell
//
//  Created by Saad Riaz on 5/24/17.
//  Copyright © 2017 evampsaanga. All rights reserved.
//

import UIKit

import ObjectMapper

// Supplementary and Subscription Offer type
enum MBOfferTabType: String {
    case Call           = "Call"
    case Internet       = "Internet"
    case SMS            = "SMS"
    case Hybrid         = "Hybrid"
    case Campaign       = "Campaign"
    case TM             = "TM"
    case Roaming        = "Roaming"
    case AllInclusive   = "All Inclusive"
    
    func localizedString() -> String {
        return Localized(self.rawValue)
    }
    
    static func getTitleFor(title:MBOfferTabType) -> String {
        return title.localizedString()
    }
}
class SupplementaryVC: BaseVC,UICollectionViewDelegate,UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    //MARK: - Properties
    var showSearch : Bool = true
    var showCardView : Bool = true
    var prepareVCForRoaming : Bool = false
    var redirectedFromNotification : Bool = false
    var offeringIdFromNotification : String = ""
    var tabMenuArray: [MBOfferTabType] = []
    var selectedTabType : MBOfferTabType = MBOfferTabType.Internet
    
    var supplementaryOffers : SupplementaryOfferings?
    var offers : [Offers] = []
    var filters : Filters?
    var selectedFilterOptions : [MBFilterType : [KeyValue]] = [:]
    var countryTitle : String = ""
    var countryImage : String = ""
    
    //MARK: - IBOutlet
    @IBOutlet var search_txt: UITextField!
    @IBOutlet var search_btn: UIButton!
    @IBOutlet var switch_btn: UIButton!
    @IBOutlet var filter_btn: UIButton!
    @IBOutlet var back_btn: UIButton!
    @IBOutlet var menu_btn: UIButton!
    @IBOutlet var title_lbl: UILabel!
    @IBOutlet var mainView: UIView!
    @IBOutlet var topOptionsView: UIView!
    
    @IBOutlet var country_lbl: UILabel!
    @IBOutlet var countryIcon_img: UIImageView!
    
    @IBOutlet var collectionView: UICollectionView!
    @IBOutlet var tabBarCollactionViewHeight: NSLayoutConstraint!
    
    //MARK: - ViewContoller methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        search_txt.delegate = self
        search_txt.attributedPlaceholder = NSAttributedString(string: Localized("PlaceHolder_Search"),
                                                              attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        
        if prepareVCForRoaming == true {
            
            // Selected Country information
            country_lbl.isHidden = false
            countryIcon_img.isHidden = false
            self.filter_btn.isHidden = false
            topOptionsView.isHidden = false
            
            tabBarCollactionViewHeight.constant = 0
            // Setting top country information and flag.
            country_lbl.text = countryTitle
            if countryImage.isBlank == true {
                countryIcon_img.image = UIImage(named: "dummy_flag")
            } else {
                countryIcon_img.image = UIImage(named: countryImage)
            }
            
            tabMenuArray = []
            
            // Load information from selected offers
            loadRoamingOffersScreen(offers: offers)
            
        } else {
            
            setSwitchLayoutButtonImage()
            // Hiding Country information view
            country_lbl.isHidden = true
            countryIcon_img.isHidden = true
            
            tabBarCollactionViewHeight.constant = 50
            tabMenuArray = [MBOfferTabType.Call,
                            MBOfferTabType.Internet,
                            MBOfferTabType.SMS,
                            MBOfferTabType.TM,
                            MBOfferTabType.Hybrid,
                            MBOfferTabType.Roaming
                /* MBOfferTabType.Campaign,*/]
            
            let index = tabMenuArray.firstIndex(of: selectedTabType)
            collectionView .selectItem(at: IndexPath(item:index ?? 0, section:0) , animated: true, scrollPosition: UICollectionView.ScrollPosition.left)
            
            //load subscriptions data
            getMySubscriptions()
        }
        
        // Switch Layout button image
        self.setSwitchLayoutButtonImage()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.interactivePop(true)
        
        // Layout view lables
        loadViewLayout()
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if prepareVCForRoaming == false {
            let index = tabMenuArray.firstIndex(of: selectedTabType)
            collectionView.scrollToItem(at: IndexPath(item:index ?? 0, section:0), at: UICollectionView.ScrollPosition.centeredHorizontally, animated: true)
        }
    }
    
    //MARK: - COLLECTION VIEW Delegates
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let str: String = tabMenuArray[indexPath.item].localizedString()
        
        return CGSize(width: ((str as NSString).size(withAttributes: [NSAttributedString.Key.font: UIFont.MBArial(fontSize: 14)]).width + 30), height: 35)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.tabMenuArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellID", for: indexPath) as! SupplementaryCollectionCell
        
        // Tab Button boader color
        cell.call_lbl.font = UIFont.MBArial(fontSize: 14)
        cell.call_lbl.text = tabMenuArray[indexPath.item].localizedString()
        cell.layer.borderColor = UIColor.MBBorderGrayColor.cgColor
        cell.call_lbl.highlightedTextColor = UIColor.black
        
        let bgColorView = UIView()
        bgColorView.backgroundColor = UIColor.MBButtonBackgroundGrayColor
        cell.selectedBackgroundView = bgColorView
        
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if supplementaryOffers != nil {
            
            // Check if user select same selected tab again if true then do nothing.
            if selectedTabType == tabMenuArray[indexPath.item] {
                return
            }
            
            // load selected tab information
            selectedTabType = tabMenuArray[indexPath.item]
            
            // Resetting selected filter
            selectedFilterOptions = [:]
            
            // Reset search
            search_btn.setImage(UIImage (imageLiteralResourceName: "actionbar_search") , for: UIControl.State.normal)
            search_txt.isHidden = true
            title_lbl.isHidden = false
            showSearch = true
            
            search_txt.text = ""
            search_txt.resignFirstResponder()
            
            
        } else {
            // get all information
            selectedTabType = tabMenuArray[indexPath.item]
            self.loadSupplementaryOfferingsData()
        }
        
        // Redirect user to selected screen
        reDirectUserToSelectedScreens(selectedItem: &selectedTabType)
    }
    
    //MARK: - IBActions
    @IBAction func switchView(_ sender: Any) {
        
        if showCardView == false {
            showCardView = true
        } else {
            showCardView = false
        }
        self.setSwitchLayoutButtonImage()
        
        //        searchPressed(search_btn)
        
        // Resetting selected filter
        selectedFilterOptions = [:]
        
        // Reset search 
        search_btn.setImage(UIImage (imageLiteralResourceName: "actionbar_search") , for: UIControl.State.normal)
        search_txt.isHidden = true
        title_lbl.isHidden = false
        showSearch = true
        
        search_txt.text = ""
        search_txt.resignFirstResponder()
        
        NotificationCenter.default.post(name: NSNotification.Name("switchHorizontalPaging"), object: nil, userInfo: nil)
    }
    
    @IBAction func searchPressed(_ sender: UIButton) {
        
        if showSearch == false {
            search_btn.setImage(UIImage (imageLiteralResourceName: "actionbar_search") , for: UIControl.State.normal)
            search_txt.isHidden = true
            title_lbl.isHidden = false
            showSearch = true
            
            search_txt.text = ""
            search_txt.resignFirstResponder()
            searchOffersByName(errorMessage: Localized("Message_NoDataAvalible"))
            
        } else {
            search_btn.setImage(UIImage (imageLiteralResourceName: "Cross") , for: UIControl.State.normal)
            title_lbl.isHidden = true
            search_txt.isHidden = false
            showSearch = false
            
            search_txt.becomeFirstResponder()
            
        }
    }
    
    @IBAction func filterPressed(_ sender: UIButton) {
        
        if let filters = self.filters {
            
            let filterPopUpVC : FilterPopUpVC = self.myStoryBoard.instantiateViewController(withIdentifier: "FilterPopUpVC") as! FilterPopUpVC
            
            filterPopUpVC.filters = filters
            filterPopUpVC.offers = offers
            filterPopUpVC.selectedFilterOptions = selectedFilterOptions
            filterPopUpVC.isRoamingFilter = prepareVCForRoaming
            
            filterPopUpVC.setCompletionHandler(completionBlock: { (filteredOffers,filterSelectedOption) in
                
                self.reDirectUserToSelectedScreenWithFilterdOffers(filteredOffers: filteredOffers, selectedItem: self.selectedTabType, erroMessage: Localized("Message_NothingFoundFilter"))
                
                self.selectedFilterOptions = filterSelectedOption
            })
            
            self.presentPOPUP(filterPopUpVC, animated: true, completion: nil)
            
        } else {
            self.showAlertWithMessage(message: Localized("Message_NothingFoundFilter"))
        }
    }
    
    //MARK: - Functions
    
    func loadViewLayout(){
        
        title_lbl.font = UIFont.MBArial(fontSize: 20)
        search_txt.font = UIFont.MBArial(fontSize: 16)
        
        if prepareVCForRoaming == true {
            
            title_lbl.text = Localized("Title_Roaming");
        } else {
            title_lbl.text = Localized("Title_SupplementaryOffer");
            
        }
    }
    
    func setSwitchLayoutButtonImage() {
        
        if showCardView == false {
            
            // CardView image
            switch_btn.setImage(UIImage (named: "cardview"), for: UIControl.State.normal)
            
        } else {
            // List image
            switch_btn.setImage(UIImage (named: "actionbar_sort"), for: UIControl.State.normal)
            
        }
    }
    
    func reDirectUserToSelectedScreens( selectedItem : inout MBOfferTabType) {
        
        var offerIndex : Int = 0
        
        // Check if user is redirected from notification
        if redirectedFromNotification {
            // Filter and find offer by offering Id
            let (type, index) = typeAndIndexOfOfferingId(offeringId: offeringIdFromNotification, allOffersData: supplementaryOffers)
            
            // Set selected type and index
            selectedItem = type
            offerIndex = index
            
            //  Set to false so user redirected to offer only once
            redirectedFromNotification = false
        }
        
        // Higlight selected collectionView cell color.
        let index = tabMenuArray.firstIndex(of: selectedItem)
        collectionView .selectItem(at: IndexPath(item:index ?? 0, section:0) , animated: true, scrollPosition: UICollectionView.ScrollPosition.centeredHorizontally)
        
        // Removing subview from main view then adding updated again
        let children = self.children
        for vc in children {
            if vc is SuplementaryOffersVC || vc is RoamingVC {
                vc.view.removeFromSuperview()
                vc.removeFromParent()
            }
        }
        
        // Check if user selected offer type roaming
        if selectedItem == MBOfferTabType.Roaming {
            
            topOptionsView.isHidden = true
            
            let roaming = self.myStoryBoard.instantiateViewController(withIdentifier: "RoamingVC") as! RoamingVC
            roaming.isCardView = self.showCardView
            
            if let MySupplementaryOffers = supplementaryOffers {
                
                if let offers = MySupplementaryOffers.roaming?.offers {
                    roaming.offers = offers
                }
                
                if let countries = MySupplementaryOffers.roaming?.countriesFlags {
                    roaming.countries = [String]()
                    for country in countries {
                        roaming.countries?.append(country.name ?? "")
                    }
                }
                
                roaming.filters = MySupplementaryOffers.roaming?.filters
            }
            
            self.addChild(roaming)
            mainView.addSubview(roaming.view)
            roaming.didMove(toParent: self)
            addConstraintsToViewController(viewController: roaming)
            
        } else {
            
            topOptionsView.isHidden = false
            self.filter_btn.isHidden = true
            
            let supp = self.myStoryBoard.instantiateViewController(withIdentifier: "SuplementaryOffersVC") as! SuplementaryOffersVC
            
            supp.isCardView = self.showCardView
            supp.selectedOffersType = selectedItem
            supp.selectedOfferIndex = offerIndex
            
            
            if let MySupplementaryOffers = supplementaryOffers {
                
                switch selectedItem {
                    
                case MBOfferTabType.Call:
                    if let offers = MySupplementaryOffers.call?.offers {
                        supp.offers = offers
                    }
                    self.filters = MySupplementaryOffers.call?.filters
                    
                case MBOfferTabType.Internet:
                    
                    self.filter_btn.isHidden = false
                    if let offers = MySupplementaryOffers.internet?.offers {
                        supp.offers = offers
                    }
                    self.filters = MySupplementaryOffers.internet?.filters
                    
                case MBOfferTabType.SMS:
                    if let offers = MySupplementaryOffers.sms?.offers {
                        supp.offers = offers
                    }
                    self.filters = MySupplementaryOffers.sms?.filters
                    
                case MBOfferTabType.Campaign
                    :
                    if let offers = MySupplementaryOffers.campaign?.offers {
                        supp.offers = offers
                    }
                    self.filters = MySupplementaryOffers.campaign?.filters
                    
                case MBOfferTabType.TM:
                    if let offers = MySupplementaryOffers.tm?.offers {
                        supp.offers = offers
                    }
                    self.filters = MySupplementaryOffers.tm?.filters
                    
                case MBOfferTabType.Hybrid:
                    if let offers = MySupplementaryOffers.hybrid?.offers {
                        supp.offers = offers
                    }
                    self.filters = MySupplementaryOffers.hybrid?.filters
                    
                default:
                    break
                }
                
                self.offers = supp.offers
                
            }
            
            self.addChild(supp)
            mainView.addSubview(supp.view)
            supp.didMove(toParent: self)
            addConstraintsToViewController(viewController: supp)
            
        }
    }
    
    func reDirectUserToSelectedScreenWithFilterdOffers(filteredOffers : [Offers], selectedItem : MBOfferTabType, erroMessage: String) {
        
        // Removing subview from main view then adding updated again
        let children = self.children
        for vc in children {
            if vc is SuplementaryOffersVC || vc is RoamingVC {
                vc.view.removeFromSuperview()
                vc.removeFromParent()
            }
        }
        
        if selectedItem == MBOfferTabType.Roaming  && prepareVCForRoaming {
            
            loadRoamingOffersScreen(offers: filteredOffers)
            
        } else {
            
            let supp = self.myStoryBoard.instantiateViewController(withIdentifier: "SuplementaryOffersVC") as! SuplementaryOffersVC
            supp.offers = filteredOffers
            supp.isCardView = self.showCardView
            supp.errorMessage = erroMessage
            supp.selectedOffersType = self.selectedTabType
            
            if let MySupplementaryOffers = supplementaryOffers {
                
                
                switch selectedItem {
                    
                case MBOfferTabType.Call:
                    
                    self.filters = MySupplementaryOffers.call?.filters
                    
                case MBOfferTabType.Internet:
                    
                    self.filters = MySupplementaryOffers.internet?.filters
                    
                case MBOfferTabType.SMS:
                    
                    self.filters = MySupplementaryOffers.sms?.filters
                    
                case MBOfferTabType.Campaign:
                    
                    self.filters = MySupplementaryOffers.campaign?.filters
                    
                case MBOfferTabType.TM:
                    
                    self.filters = MySupplementaryOffers.tm?.filters
                    
                case MBOfferTabType.Hybrid:
                    
                    self.filters = MySupplementaryOffers.hybrid?.filters
                    
                default:
                    break
                }
            }
            self.addChild(supp)
            mainView.addSubview(supp.view)
            supp.didMove(toParent: self)
            addConstraintsToViewController(viewController: supp)
        }
    }
    
    func loadRoamingOffersScreen (offers : [Offers]) {
        
        let supp = self.myStoryBoard.instantiateViewController(withIdentifier: "SuplementaryOffersVC") as! SuplementaryOffersVC
        supp.offers = offers
        supp.isCardView = self.showCardView
        supp.selectedOffersType = self.selectedTabType
        
        self.addChild(supp)
        mainView.addSubview(supp.view)
        supp.didMove(toParent: self)
        addConstraintsToViewController(viewController: supp)
    }
    
    func addConstraintsToViewController(viewController : UIViewController) {
        viewController.view.snp.makeConstraints { make in
            make.top.equalTo(mainView.snp.top).offset(0.0)
            make.right.equalTo(mainView.snp.right).offset(0.0)
            make.left.equalTo(mainView.snp.left).offset(0.0)
            make.bottom.equalTo(mainView.snp.bottom).offset(0.0)
        }
    }
    
    
    func typeAndIndexOfOfferingId(offeringId : String?, allOffersData : SupplementaryOfferings?) -> (MBOfferTabType, Int) {
        
        // check if found nil information
        if allOffersData == nil {
            return (.Call,0)
        }
        
        /* Filter offers of Call section */
        var filterResponse = containsOfferByOfferingId(offeringId: offeringId, offerArray: allOffersData?.call?.offers)
        
        if filterResponse.isFoundValue {
            return (.Call,filterResponse.index)
        }
        
        /* Filter offers of Internet section */
        filterResponse = containsOfferByOfferingId(offeringId: offeringId, offerArray: allOffersData?.internet?.offers)
        
        if filterResponse.isFoundValue {
            return (.Internet,filterResponse.index)
        }
        
        /* Filter offers of SMS section */
        filterResponse = containsOfferByOfferingId(offeringId: offeringId, offerArray: allOffersData?.sms?.offers)
        
        if filterResponse.isFoundValue {
            return (.SMS,filterResponse.index)
        }
        
        /* Filter offers of Campaign section */
        filterResponse = containsOfferByOfferingId(offeringId: offeringId, offerArray: allOffersData?.campaign?.offers)
        
        //        if filterResponse.isFoundValue {
        //            return (.Campaign,filterResponse.index)
        //        }
        //
        /* Filter offers of TM section */
        filterResponse = containsOfferByOfferingId(offeringId: offeringId, offerArray: allOffersData?.tm?.offers)

        if filterResponse.isFoundValue {
            return (.TM,filterResponse.index)
        }
        
        /* Filter offers of Hybrid section */
        filterResponse = containsOfferByOfferingId(offeringId: offeringId, offerArray: allOffersData?.hybrid?.offers)
        
        if filterResponse.isFoundValue {
            return (.Hybrid,filterResponse.index)
        }
        
        /* Filter offers of Roaming section */
        filterResponse = containsOfferByOfferingId(offeringId: offeringId, offerArray: allOffersData?.roaming?.offers)
        
        if filterResponse.isFoundValue {
            return (.Roaming,filterResponse.index)
        }
        
        // If no information is found then retrun call as default
        return (.Internet,0)
    }
    
    func containsOfferByOfferingId(offeringId : String? , offerArray : [Offers]? ) -> (isFoundValue : Bool, index : Int) {
        
        var foundOffer = false
        
        // return 0 if offeringId is blank
        if offeringId?.isBlank != false {
            return (foundOffer, 0)
        }
        // return 0 if offerArray is nil
        if offerArray == nil {
            return (foundOffer, 0)
        }
        
        // Filtering offeringId index from array
        let indexOfOffer = offerArray?.firstIndex() {
            
            if let aOfferingId = ($0 as Offers).header?.offeringId {
                
                let isFound = aOfferingId.isEqualToStringIgnoreCase(otherString: offeringId)
                
                if isFound {
                    foundOffer = true
                    return true
                } else {
                    return false
                }
                
            } else {
                return false
            }
        }
        
        return (foundOffer, (indexOfOffer ?? 0))
    }
    
    func loadSupplementaryOfferingsData() {
        
        /* Load data from UserDefaults in case not connected to internet */
        if Connectivity.isConnectedToInternet == false,
            let supplementaryOffersHandler : SupplementaryOfferings = SupplementaryOfferings.loadFromUserDefaults(key: APIsType.supplementaryOffer.selectedLocalizedAPIKey()){
            
            // Set information in local object
            self.supplementaryOffers = supplementaryOffersHandler
            // Redirect user to selected screen
            self.reDirectUserToSelectedScreens(selectedItem: &self.selectedTabType)
            
        } else {
            self.getSupplementaryOfferings()
        }
        self.activityIndicator.hideActivityIndicator()
    }
    
    //MARK: - API Calls
    
    /// Call 'getSubscriptions' API .
    ///
    /// - returns: Void
    func getMySubscriptions() {
        
        activityIndicator.showActivityIndicator()
        
        _ = MBAPIClient.sharedClient.getSubscriptions({ (response, resultData, error, isCancelled, status, resultCode, resultDesc)  in
            
            self.activityIndicator.hideActivityIndicator()
            if error != nil {
                // loading supplementary offers
                self.loadSupplementaryOfferingsData()
                
            } else {
                // handling data from API response.
                if resultCode == Constants.MBAPIStatusCode.succes.rawValue {
                    
                    // Parssing response data
                    if let mySubscriptionOffersHandler = Mapper<MySubscription>().map(JSONObject: resultData){
                        
                        // set data in user session
                        MBUserSession.shared.mySubscriptionOffers = mySubscriptionOffersHandler
                        
                    }
                }
                
                // loading supplementary offers
                self.loadSupplementaryOfferingsData()
            }
        })
    }
    
    
    /// Call 'getSupplementaryOffering' API .
    ///
    /// - returns: Void
    func getSupplementaryOfferings() {
        
        activityIndicator.showActivityIndicator()
        
        _ = MBAPIClient.sharedClient.getSupplementaryOfferings({ (response, resultData, error, isCancelled, status, resultCode, resultDesc) in
            
            self.activityIndicator.hideActivityIndicator()
            
            if error != nil {
                error?.showServerErrorInViewController(self)
                
            } else {
                // handling data from API response.
                if resultCode == Constants.MBAPIStatusCode.succes.rawValue {
                    
                    // Parssing response data
                    if let supplementaryOffersHandler = Mapper<SupplementaryOfferings>().map(JSONObject: resultData){
                        
                        // Saving Supplementary Offers data in user defaults
                        supplementaryOffersHandler.saveInUserDefaults(key: APIsType.supplementaryOffer.selectedLocalizedAPIKey())
                        
                        // Set information in local object
                        self.supplementaryOffers = supplementaryOffersHandler
                    }
                } else {
                    // Show error alert to user
                    self.showErrorAlertWithMessage(message: resultDesc)
                }
            }
            // Redirect user to selected screen
            self.reDirectUserToSelectedScreens(selectedItem: &self.selectedTabType)
        })
    }
    
    
    
}

extension SupplementaryVC : UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        guard let text = textField.text else { return false }
        
        if textField == search_txt {
            // New text of string after change
            let newString = (text as NSString).replacingCharacters(in: range, with: string)
            // Filter offers with new name
            self.searchOffersByName(searchString: newString)
            
        }
        
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return false
    }
    
    // Filter offers with offer name
    func searchOffersByName(searchString: String = "", errorMessage : String = Localized("Message_NothingFoundSearch")) {
        
        // find offers where name matches
        let filterdOffers = filterOfferBySearchString(searchString: searchString, offerArray: self.offers)
        
        // Redirect user to screen accourdingly
        self.reDirectUserToSelectedScreenWithFilterdOffers(filteredOffers: filterdOffers, selectedItem: selectedTabType, erroMessage: errorMessage)
        
    }
    
    func filterOfferBySearchString(searchString : String? , offerArray : [Offers]? ) -> [Offers] {
        
        // return complete array if string is empty
        if let searchString = searchString {
            
            if searchString == "" {
                
                if let offers = offerArray {
                    return offers
                }
            }
        }
        
        // Filtering offers array
        var filteredOffers : [Offers] = []
        if let offers = offerArray {
            
            filteredOffers = offers.filter() {
                
                if let offerName = ($0 as Offers).header?.offerName {
                    
                    return offerName.containsSubString(subString: searchString)
                    
                } else {
                    return false
                }
            }
        }
        
        return filteredOffers
    }
}



