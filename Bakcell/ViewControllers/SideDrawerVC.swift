//
//  SideDrawerVC.swift
//  Bakcell
//
//  Created by Saad Riaz on 5/17/17.
//  Copyright © 2017 evampsaanga. All rights reserved.
//

import UIKit
import KYDrawerController
import LiveChat


class SideDrawerVC: BaseVC {
    
    //MARK:- IBOutlet
    @IBOutlet var profile_img: UIImageView!
    @IBOutlet var userPerfileName_lbl: UILabel!
    @IBOutlet var userMSISDN_lbl: UILabel!
    @IBOutlet var packageIcon_img: UIImageView!
    
    @IBOutlet var tableView: UITableView!
    
    //MARK:- ViewController Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Setting TableView delegate and dataSource to self
        self.tableView.delegate = self
        self.tableView.dataSource = self
        
        // Setting Profile images layout
        profile_img.contentMode = .scaleAspectFill
        profile_img.clipsToBounds = true
        profile_img.layer.cornerRadius = profile_img.frame.height / 2
        
        //livechat delegate
        LiveChat.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.interactivePop(false)
        
        MBAPIClient.sharedClient.request(MBUserSession.shared.userInfo?.imageURL ?? "").responseData { response in
            
            if let imageDate = response.result.value,
                let profileImage = UIImage(data: imageDate){
                
                self.profile_img.image = profileImage
            } else {
                self.profile_img.image = UIImage(named: "profile")
            }
        }
        
        /* Menu is empty then show logout option */
        if MBUserSession.shared.appMenu?.menuVertical == nil ||
            (MBUserSession.shared.appMenu?.menuVertical?.count ?? 0) <= 0 {
            MBUserSession.shared.appMenu = AppMenusModel(menuHorizontal: nil, menuVertical: [MenuVertical(identifier: "logout", title: Localized("Title_Logout"), sortOrder: "0", iconName: "")])
        }
        
        // Reload TableView Data
        tableView.reloadData()
        
        // Setting User name and MSISDN
        userPerfileName_lbl.text = MBUserSession.shared.userName()
        userMSISDN_lbl.text = MBUserSession.shared.msisdn
        
        // Setting User current package logo
        var iconName = ""
        if MBUserSession.shared.userInfo?.brandName?.lowercased() == "cin" {
            iconName = "title_cin"
            
        } else if MBUserSession.shared.userInfo?.brandName?.lowercased() == "klass" {
            iconName = "title_klass"
            
        } else if MBUserSession.shared.userInfo?.brandName?.lowercased() == "business" {
            iconName = "title_business"
        }
        
        packageIcon_img.image = UIImage(named: iconName)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
        self.closeSideMenu()
    }
    
    //MARK:- IBACTIONS
    @IBAction func myProfile_btn(_ sender: UIButton) {
        
        // Initialize and push to Profile controller
        if let myProfile = self.myStoryBoard.instantiateViewController(withIdentifier: "MyProfileVC") as? MyProfileVC {
            self.navigationController?.pushViewController(myProfile, animated: true)
        }
    }
    
    @IBAction func manageAccount_btn(_ sender: UIButton) {
        if let manageAccountsPopUpVC :ManageAccountsPopUpVC =  ManageAccountsPopUpVC.instantiateViewControllerFromStoryboard() {
            
            manageAccountsPopUpVC.delegate = self
            //getting completion block on ManageAccounts button tap
            manageAccountsPopUpVC.navigateToManageAccountsScreen { _ in
                
                //navigating to ManageAccountsVC to add/delete accounts
                if let manageAccountsVC :ManageAccountsVC = ManageAccountsVC.instantiateViewControllerFromStoryboard() {
                    // manageAccountsVC.delegate = self
                    self.navigationController?.pushViewController(manageAccountsVC, animated: true)
                }
            }
            self.presentPOPUP(manageAccountsPopUpVC, animated: true)
        }
    }
    
    //MARK: - Functions
    /**
     close side menu.
     
     - returns: void
     */
    
    func closeSideMenu(shouldSelectDashboard:Bool = true, animated :Bool = false) {
        if let drawer = self.parent as? KYDrawerController,
            drawer.drawerState == .opened {
            drawer.setDrawerState(.closed, animated: animated)
            
            if shouldSelectDashboard {
                self.setSelectTabbarIndex(0)
            }
        }
    }
    
    /**
     Set tabbar selected index.
     
     - parameter selectedIndex: Index which need to be set as selected.
     
     - returns: void.
     */
    func setSelectTabbarIndex(_ selectedIndex: Int) {
        if let drawer = self.parent as? KYDrawerController ,
            let tabBarCtr = drawer.mainViewController as? UITabBarController {
            tabBarCtr.selectedIndex = selectedIndex
        }
    }
    
    
    /**
     assing and sort sideMenu data and store sorted list in UserDefault.
     
     - parameter menuList: new side menu list.
     
     - returns: void
     */
    /* class func sortSideMenuListDataAndStoreInUserDefault(menuList : [MenuList]) {
     
     // Set icon name of all menu options
     for i in 0 ..< menuList.count {
     menuList[i].iconName = imageNameFor(Identifier: menuList[i].identifier)
     }
     // Assign and sort menu options
     MBUserSession.shared.drawerMenuList = menuList
     MBUserSession.shared.drawerMenuList.sort { $0.sortOrder < $1.sortOrder }
     
     // Save menu options in userDefault
     MBUtilities.saveMenuListInUserDefaults(menuList: MBUserSession.shared.drawerMenuList, forKey: Localized("API_DrawerMenuList"))
     }*/
    
    
    /**
     Redirect user accourding to provided identifier.
     
     - parameter identifier: key for selected controller
     
     - returns: void
     */
    func redirectUserToSelectedController(Identifier identifier : String) {
        
        switch identifier {
            
        case "notifications":
            // Load Notifications viewController
            if let notifications = self.myStoryBoard.instantiateViewController(withIdentifier: "NotificationsVC") as? NotificationsVC {
                self.navigationController?.pushViewController(notifications, animated: true)
            }
            
        case "store_locator":
            // load StoreLocator viewController
            if let store = self.myStoryBoard.instantiateViewController(withIdentifier: "StoreLocatorVC") as? StoreLocatorVC {
                self.navigationController?.pushViewController(store, animated: true)
            }
            
        case "faq":
            // load FAQs viewController
            if let faqs = self.myStoryBoard.instantiateViewController(withIdentifier: "FAQsVC") as? FAQsVC {
                self.navigationController?.pushViewController(faqs, animated: true)
            }
            
        case "tutorial_and_faqs":
            // load Tutorial viewController
            if let tuts = self.myStoryBoard.instantiateViewController(withIdentifier: "TutsVC") as? TutsVC {
                self.navigationController?.pushViewController(tuts, animated: true)
            }
            
        case "live_chat":
            /*if MBUserSession.shared.liveChatObject == nil {
                if let myWebView = self.myStoryBoard.instantiateViewController(withIdentifier: "LiveChatWebViewVC") as? LiveChatWebViewVC {
                    MBUserSession.shared.liveChatObject = myWebView
                }
                
            }
            
            if (self.navigationController?.topViewController is LiveChatWebViewVC) == false {
                self.navigationController?.pushViewController(MBUserSession.shared.liveChatObject!, animated: true)
            }*/
            openLiveChatNow()
            
            // Log Event of Live Chat redirection
            MBAnalyticsManager.logEvent(screenName: "Live Chat Screen", contentType:"Live Chat Screen" , status:"Live Chat Screen" )
            
        case "contact_us":
            // load ContactUs viewController
            if let contactUs = self.myStoryBoard.instantiateViewController(withIdentifier: "ContactUsVC") as? ContactUsVC {
                self.navigationController?.pushViewController(contactUs, animated: true)
            }
            
        case "terms_and_conditions":
            // load TermsAndConditions viewController
            if let terms = self.myStoryBoard.instantiateViewController(withIdentifier: "Terms_ConditionsVC") as? Terms_ConditionsVC {
                self.navigationController?.pushViewController(terms, animated: true)
            }
            
        case "settings":
            // load Settings viewController
            if let settingsVC = self.myStoryBoard.instantiateViewController(withIdentifier: "SettingsVC") as? SettingsVC {
                self.navigationController?.pushViewController(settingsVC, animated: true)
            }
            
        case "ulduzum":
            if let ulduzumVC = self.myStoryBoard.instantiateViewController(withIdentifier: "UlduzumVC") as? UlduzumVC {
                self.navigationController?.pushViewController(ulduzumVC, animated: true)
            }
            
        case "roaming":
            // load Roaming Countries viewController
            if let roamingVC = self.myStoryBoard.instantiateViewController(withIdentifier: "RoamingCountriesVC") as? RoamingCountriesVC {
                self.navigationController?.pushViewController(roamingVC, animated: true)
            }
            break
            
        case "logout":
            /* Close side menu */
            closeSideMenu(shouldSelectDashboard: false)
            
            if (MBUserSession.shared.loggedInUsers?.users?.count ?? 0) > 1 {
                // Show logout confirmation alert to User
                if let logoutAlertVC = self.myStoryBoard.instantiateViewController(withIdentifier: "LogoutAlertVC") as? LogoutAlertVC {
                    self.presentPOPUP(logoutAlertVC, animated: true, completion: nil)
                    
                    // Set confirmation alert layout for logout
                    logoutAlertVC.setAlertForLogOut()
                    
                    // Action if user confirm to logout
                    logoutAlertVC.setYesButtonCompletionHandler(completionBlock: { (aString) in
                        // Log event of user logout action
                        MBAnalyticsManager.logEvent(screenName: "Logout Dialog", contentType:"User LoggedOut" , status:"Logout Dialog" )
                        
                        // Logout user from current session.
                        self.logOut()
                    })
                    
                    logoutAlertVC.setManageAccountButtonCompletionHandler { _ in
                        if let roamingVC = self.myStoryBoard.instantiateViewController(withIdentifier: "ManageAccountsVC") as? ManageAccountsVC {
                            self.navigationController?.pushViewController(roamingVC, animated: true)
                        }
                    }
                }
                
            } else {
                // Show logout confirmation alert to User
                if let alert = self.myStoryBoard.instantiateViewController(withIdentifier: "AlertVC") as? AlertVC {
                    self.presentPOPUP(alert, animated: true, completion: nil)
                    
                    // Set confirmation alert layout for logout
                    alert.setAlertForLogOut()
                    
                    // Action if user confirm to logout
                    alert.setYesButtonCompletionHandler(completionBlock: { (aString) in
                        // Log event of user logout action
                        MBAnalyticsManager.logEvent(screenName: "Logout Dialog", contentType:"User LoggedOut" , status:"Logout Dialog" )
                        
                        // Logout user from current session.
                        self.logOut()
                    })
                }
            }
            
            
        default:
            break
        }
    }
    
    /**
     Provide image name against identifiers. which are mapped.
     
     - parameter identifier: key for mapped image.
     
     - returns: image name for mapped identifier.
     */
    func imageNameFor(Identifier identifier : String) -> String {
        
        switch identifier {
            
        case "notifications":
            return "menu1"
            
        case "store_locator":
            return "menu2"
            
        case "faq":
            return "menu3"
            
        case "tutorial_and_faqs":
            return "tutorials"
            
        case "live_chat":
            return "menu5"
            
        case "contact_us":
            return "menu6"
            
        case "terms_and_conditions":
            return "menu7"
            
        case "ulduzum":
            return "ulduzum_menu"
            
        case "roaming":
            return "roaming_menu"
            
        case "settings":
            return "menu8"
            
        case "logout":
            return "menu9"
            
        default:
            return ""
            
        }
    }
    
    //MARK: - API Calls
    /**
     Logout user from current session.
     
     - returns: void
     */
    func logOut() {
        
        /* API call to Logout user */
        self.activityIndicator.showActivityIndicator()
        
        _ = MBAPIClient.sharedClient.logOut({ (response, resultData, error, isCancelled, status, resultCode, resultDesc) in
            
            self.activityIndicator.hideActivityIndicator()
            if error != nil {
                error?.showServerErrorInViewController(self)
                
            } else {
                // handling data from API response.
                if resultCode == Constants.MBAPIStatusCode.succes.rawValue {
                    
                    // Clear user data
                    BaseVC.logout()
                    
                } else {
                    // Show error alert to user
                    self.showErrorAlertWithMessage(message: resultDesc)
                }
            }
        })
    }
}

// MARK:- UITableViewDelegate and UITableViewDataSource
extension SideDrawerVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return MBUserSession.shared.appMenu?.menuVertical?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let cell = tableView.dequeueReusableCell(withIdentifier: "cellID", for: indexPath) as? SideBarCell {
            
            // Set cell data and return
            cell.sideBarLbl.text = MBUserSession.shared.appMenu?.menuVertical?[indexPath.row].title
            
            cell.sideBarImgView.image = UIImage(named:self.imageNameFor(Identifier: MBUserSession.shared.appMenu?.menuVertical?[indexPath.row].identifier ?? ""))
            return cell
            
        } else {
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        // Redirect user to selected screen
        redirectUserToSelectedController(Identifier: MBUserSession.shared.appMenu?.menuVertical?[indexPath.row].identifier ?? "")
        
        // Deselect user selected row
        tableView.deselectRow(at: indexPath, animated: true)
    }
}

//MARK:- UsersSwitchDelegate
extension SideDrawerVC: UsersSwitchDelegate {
    func userDidSelect(_ newUser: UserDataModel?) {
        
        if let newUserInfo = newUser {
            
            if (newUserInfo.userInfo?.msisdn?.isEqualToStringIgnoreCase(otherString: MBUserSession.shared.msisdn) ?? false) == true {
                self.showErrorAlertWithMessage(message: Localized("Message_AlreadySelected"))
                
            } else if let drawer = self.parent as? KYDrawerController,
                let tabbarVC = drawer.mainViewController as? TabbarController {
                
                /* Clear user session objects befor switch */
                MBUserSession.shared.clearCurrentUserSessionObjects()
                
                /* Switch user to new user */
                MBUserInfoUtilities.switchToUser(newMSISDN: newUserInfo.userInfo?.msisdn ?? "", selectedUserInfo: newUserInfo)
                
                /* Cancel all API requests */
                MBAPIClient.sharedClient.cancelAllRequests()
                
                /* Load user information and close side menu */
                tabbarVC.loadCustomerInfo(callResumeAPI: true)
                self.closeSideMenu(shouldSelectDashboard: true, animated: true)
            } 
        }
    }
}

// MARK: LiveChatDelegate
extension SideDrawerVC: LiveChatDelegate {
    
    
    func openLiveChatNow() {
        LiveChat.licenseId = "12830742"
        if MBLanguageManager.userSelectedLanguage() == .English {
            LiveChat.groupId = "0"
        } else if MBLanguageManager.userSelectedLanguage() == .Azeri {
            LiveChat.groupId = "1"
        } else if MBLanguageManager.userSelectedLanguage() == .Russian {
            LiveChat.groupId = "2"
        }
        
        
        LiveChat.name = MBUserSession.shared.userInfo?.firstName // User name and email can be provided LiveChat.groupId = "1"if known
        LiveChat.email = MBUserSession.shared.userInfo?.email
        LiveChat.setVariable(withKey:"Phone number", value:MBUserSession.shared.msisdn)
//        LiveChat.setVariable(withKey:"language", value:"az")
        LiveChat.presentChat()
    }

    func received(message: LiveChatMessage) {
        if (!LiveChat.isChatPresented) {
            // Notifying user
            let alert = UIAlertController(title: Localized("Title_Attention"), message: message.text, preferredStyle: .alert)
            let chatAction = UIAlertAction(title: "Go to Chat", style: .default) { alert in
                if !LiveChat.isChatPresented {
                    LiveChat.presentChat()
                }
            }
            alert.addAction(chatAction)

            let cancelAction = UIAlertAction(title: Localized("BtnTitle_CANCEL"), style: .cancel)
            alert.addAction(cancelAction)
            self.present(alert, animated: true, completion: nil)
        }
    }
}
