//
//  MBAnalyticsManager.swift
//  Bakcell
//
//  Created by AbdulRehman Warraich on 11/16/17.
//  Copyright © 2017 evampsaanga. All rights reserved.
//

import UIKit
import Firebase

class MBAnalyticsManager: NSObject {

    public class func logEvent(screenName: String, contentType: String, status: String) {

        let aScreenName = (screenName.lowercased()).replaceSpaceWithHyphen

        Analytics.logEvent(aScreenName, parameters: [
            AnalyticsParameterContentType: contentType as NSObject,
            AnalyticsParameterItemName: status as NSObject
            ])
        
    }

}
