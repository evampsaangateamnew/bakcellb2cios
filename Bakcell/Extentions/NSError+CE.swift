//
//  NSError+MB.swift
//  Bakcell
//
//  Created by AbdulRehman Warraich on 8/16/17.
//  Copyright © 2017 evampsaanga. All rights reserved.
//
import UIKit

let MBErrorDomain = "com.errordomain.bakcell"
let MBFormErrorCode = 40001
typealias MBAlertCompletionHandler = (_ retry: Bool, _ cancel: Bool) -> Void

extension NSError {

    func alertControllerWithTitle(_ title: String) -> UIAlertController {
        return UIAlertController(title: title, message: localizedDescription, preferredStyle: .alert)
    }

    func showServerErrorInViewController(_ viewController: UIViewController, _ actionBlock : @escaping MBButtonCompletionHandler = { _ in }) {

        if (viewController.isViewLoaded && (viewController.view.window != nil)){

            if let alert = viewController.myStoryBoard.instantiateViewController(withIdentifier: "AlertMessageVC") as? AlertMessageVC {

                alert.setAlertWith(title: Localized("Title_Error"), description: localizedDescription, okBtnClickedBlock: actionBlock)
                viewController.presentPOPUP(alert, animated: true, modalTransitionStyle:.crossDissolve, completion: nil)
                
            } else {
                let alertViewController = UIAlertController(title: Localized("Title_Error"), message: localizedDescription, preferredStyle: .alert)

                let okAction = UIAlertAction(title: Localized("BtnTitle_OK"), style: .default) { (action) in
                    actionBlock("")
                }

                alertViewController.addAction(okAction)
                viewController.present(alertViewController, animated: true, completion: nil)
            }
        }
    }
}
