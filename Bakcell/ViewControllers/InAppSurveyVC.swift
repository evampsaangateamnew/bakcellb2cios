//
//  InAppSurveyVC.swift
//  Bakcell
//
//  Created by Muhammad Irfan Awan on 13/08/2021.
//  Copyright © 2021 evampsaanga. All rights reserved.
//

import UIKit
import AlamofireObjectMapper
import ObjectMapper

enum SurveyScreenName: String{
    case dashboard = "dashboard"
    case topup = "topup"
    case tariff = "tariff"
    case bundle = "bundle"
    case getLoan = "get_loan"
    case transferMoney = "transfer_money"
    case fnf = "fnf"
}

class InAppSurveyVC: BaseVC {
    
    //IBOUtlets
    
    @IBOutlet var mainView: MBView!
    
    @IBOutlet var simpleTitleStackView: UIStackView!
    @IBOutlet var lblTitle1: UILabel!
    
    @IBOutlet var thanksStackView: UIStackView!
    @IBOutlet var mainRatingStackView: UIStackView!
    @IBOutlet var topupStackView: UIStackView!
    @IBOutlet var balanceStackView: UIStackView!
    
    @IBOutlet var lbltopupTitle: UILabel!
    @IBOutlet var lblTopupMessage: UILabel!
    @IBOutlet var lbltopupBalance: UILabel!
    @IBOutlet var lblSurvayQuestion: UILabel!
    @IBOutlet var lblThanks: UILabel!
    
    @IBOutlet var AnswerStackView: UIStackView!
    @IBOutlet var btn1: UIButton!
    @IBOutlet var btn2: UIButton!
    @IBOutlet var btn3: UIButton!
    @IBOutlet var btn4: UIButton!
    @IBOutlet var btn5: UIButton!
    @IBOutlet var lblScore: UILabel!
    
    @IBOutlet var commentsView:UIView!
    @IBOutlet var txtComments: UITextField!
    @IBOutlet var btnSubmit: MBMarqueeButton!
    
    var callback: (() -> Void)?
    
    var surveyScreen = SurveyScreenName.dashboard.rawValue
    
    var showTopUpStackView: Bool = false
    var hideBalanceStackView: Bool = true
    var isSavedSurvey: Bool = false
    var attributedMessage: NSAttributedString?
    var attributedBalance: NSAttributedString?
    var confirmationText: String?
    var toptupTitleText: String?
    
    var currentSurvay   : InAppSurvey?
    var survayQuestion  : SurvayQuestions?
    var selectedAnswer  : SurvayAnswer?
    var survayComment   : String?
    
    
    //offeringId
    var offeringId: String? = nil
    var offeringType: String? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        mainRatingStackView.isHidden = false
        thanksStackView.isHidden = true
        
        
//        currentSurvay = MBUserSession.shared.appSurvays?.surveys?.first(where: { $0.screenName == "bundle" })
        
        survayQuestion = currentSurvay?.questions?[0]
        AnswerStackView.isHidden = true
        setLayout()
        if let answer = survayQuestion?.answers?.firstIndex(where: { $0.answerId ==  selectedAnswer?.answerId }) {
            updateStarsFor(selectedIndex: answer)
            self.txtComments.text = survayComment ?? ""
        }
        
        
        
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        mainView.layer.cornerRadius = 10.0
    }
    
    func setRating(){
        
        if offeringId != nil{
            if  let  userSurvey = MBUserSession.shared.appSurvays?.userSurveys?.first(where: { $0.offeringId == offeringId ?? ""}){
                if let survey  = MBUserSession.shared.appSurvays?.surveys?.first(where: { $0.survayId  == userSurvey.surveryId }){
                    if let  question =  survey.questions?.first(where: { $0.questionId == userSurvey.questionId }){
                        if let answer = question.answers?.firstIndex(where: { $0.answerId ==  userSurvey.answerId }) {
                            switch answer {
                            case 0:
                                print(answer)
                                btn1.setImage(UIImage.init(named: "RedFilledStar"), for: .normal)
                                btn2.setImage(UIImage.init(named: "RedLineStar"), for: .normal)
                                btn3.setImage(UIImage.init(named: "RedLineStar"), for: .normal)
                                btn4.setImage(UIImage.init(named: "RedLineStar"), for: .normal)
                                btn5.setImage(UIImage.init(named: "RedLineStar"), for: .normal)
                            case 1:
                                print(answer)
                                btn1.setImage(UIImage.init(named: "RedFilledStar"), for: .normal)
                                btn2.setImage(UIImage.init(named: "RedFilledStar"), for: .normal)
                                btn3.setImage(UIImage.init(named: "RedLineStar"), for: .normal)
                                btn4.setImage(UIImage.init(named: "RedLineStar"), for: .normal)
                                btn5.setImage(UIImage.init(named: "RedLineStar"), for: .normal)
                            case 2:
                                print(answer)
                                btn1.setImage(UIImage.init(named: "RedFilledStar"), for: .normal)
                                btn2.setImage(UIImage.init(named: "RedFilledStar"), for: .normal)
                                btn3.setImage(UIImage.init(named: "RedFilledStar"), for: .normal)
                                btn5.setImage(UIImage.init(named: "RedLineStar"), for: .normal)
                            case 3:
                                print(answer)
                                btn1.setImage(UIImage.init(named: "RedFilledStar"), for: .normal)
                                btn2.setImage(UIImage.init(named: "RedFilledStar"), for: .normal)
                                btn3.setImage(UIImage.init(named: "RedFilledStar"), for: .normal)
                                btn4.setImage(UIImage.init(named: "RedFilledStar"), for: .normal)
                                btn5.setImage(UIImage.init(named: "RedLineStar"), for: .normal)
                            case 4:
                                btn1.setImage(UIImage.init(named: "RedFilledStar"), for: .normal)
                                btn2.setImage(UIImage.init(named: "RedFilledStar"), for: .normal)
                                btn3.setImage(UIImage.init(named: "RedFilledStar"), for: .normal)
                                btn4.setImage(UIImage.init(named: "RedFilledStar"), for: .normal)
                                btn5.setImage(UIImage.init(named: "RedFilledStar"), for: .normal)
                            default:
                                break
                            }
                            
                        }
                    }
                }
            }else{
                btn1.setImage(UIImage.init(named: "RedLineStar"), for: .normal)
                btn2.setImage(UIImage.init(named: "RedLineStar"), for: .normal)
                btn3.setImage(UIImage.init(named: "RedLineStar"), for: .normal)
                btn4.setImage(UIImage.init(named: "RedLineStar"), for: .normal)
                btn5.setImage(UIImage.init(named: "RedLineStar"), for: .normal)
            }
        }
        
    }
    
    
    
    func setLayout() {
        self.lblThanks.text = Localized("Title_Thanks")
        if self.showTopUpStackView == false {
            simpleTitleStackView.isHidden = false
            topupStackView.isHidden = true
            if MBLanguageManager.userSelectedLanguage() == .English{
                self.lblTitle1.text = survayQuestion?.questionTextEN
            }else if MBLanguageManager.userSelectedLanguage() == .Azeri{
                self.lblTitle1.text = survayQuestion?.questionTextAZ
            }else{
                self.lblTitle1.text = survayQuestion?.questionTextRU
            }
        } else {
            
            simpleTitleStackView.isHidden = true
            topupStackView.isHidden = false
            self.lblTopupMessage.attributedText = attributedMessage
            self.lbltopupTitle.text = toptupTitleText ?? ""
            if !hideBalanceStackView{
                self.lbltopupBalance.attributedText = attributedBalance
            } else {
                balanceStackView.isHidden = true
            }
            
        }
//        updateTextMessages()
        
        if currentSurvay?.commentEnable?.contains("enable") ?? false || currentSurvay?.commentEnable ?? "" == "enable"{
            commentsView.isHidden = false
        }else{
            commentsView.isHidden = true
        }

        btnSubmit.backgroundColor = UIColor.MBRedColor
        btnSubmit.layer.cornerRadius = CGFloat(Constants.kButtonCornerRadius)
        btnSubmit.setTitle(Localized("BtnTitle_Submit"), for: UIControl.State.normal)
        btnSubmit.setTitleColor(UIColor.white, for: UIControl.State.normal)
        if MBLanguageManager.userSelectedLanguage() == .English{
            txtComments.placeholder = currentSurvay?.commentsTitleEn
            lblSurvayQuestion.text = survayQuestion?.questionTextEN
        }else if MBLanguageManager.userSelectedLanguage() == .Azeri{
            txtComments.placeholder = currentSurvay?.commentsTitleAz
            lblSurvayQuestion.text = survayQuestion?.questionTextAZ
        }else{
            txtComments.placeholder = currentSurvay?.commentsTitleRu
            lblSurvayQuestion.text = survayQuestion?.questionTextRU
        }
        
    }
    
    func updateTextMessages() {
        /*if self.showTopUpStackView == false {
            if MBLanguageManager.userSelectedLanguage() == .English {
                lblTitle1.text = survayQuestion?.questionTextEN
            } else if MBLanguageManager.userSelectedLanguage() == .Russian {
                lblTitle1.text = survayQuestion?.questionTextRU
            } else {
                lblTitle1.text = survayQuestion?.questionTextAZ
            }
        } else {
            lbltopupTitle.text = confirmationText ?? ""
            if MBLanguageManager.userSelectedLanguage() == .English {
                lblSurvayQuestion.text = survayQuestion?.questionTextEN
            } else if MBLanguageManager.userSelectedLanguage() == .Russian {
                lblSurvayQuestion.text = survayQuestion?.questionTextRU
            } else {
                lblSurvayQuestion.text = survayQuestion?.questionTextAZ
            }
            lblTopupMessage.attributedText = attributedMessage
            if hideBalanceStackView == true {
                balanceStackView.isHidden = true
            }
            if showBlance == true {
                lbltopupBalance.attributedText = attributedBalance
            }
        }*/
    }
    
    
    func updateStarsFor(selectedIndex: Int) {
        
        if selectedIndex == 0 {
            btn1.setImage(UIImage.init(named: "RedFilledStar"), for: .normal)
            btn2.setImage(UIImage.init(named: "RedLineStar"), for: .normal)
            btn3.setImage(UIImage.init(named: "RedLineStar"), for: .normal)
            btn4.setImage(UIImage.init(named: "RedLineStar"), for: .normal)
            btn5.setImage(UIImage.init(named: "RedLineStar"), for: .normal)
        } else if selectedIndex == 1 {
            btn1.setImage(UIImage.init(named: "RedFilledStar"), for: .normal)
            btn2.setImage(UIImage.init(named: "RedFilledStar"), for: .normal)
            btn3.setImage(UIImage.init(named: "RedLineStar"), for: .normal)
            btn4.setImage(UIImage.init(named: "RedLineStar"), for: .normal)
            btn5.setImage(UIImage.init(named: "RedLineStar"), for: .normal)
        } else if selectedIndex == 2 {
            btn1.setImage(UIImage.init(named: "RedFilledStar"), for: .normal)
            btn2.setImage(UIImage.init(named: "RedFilledStar"), for: .normal)
            btn3.setImage(UIImage.init(named: "RedFilledStar"), for: .normal)
            btn4.setImage(UIImage.init(named: "RedLineStar"), for: .normal)
            btn5.setImage(UIImage.init(named: "RedLineStar"), for: .normal)
        } else if selectedIndex == 3 {
            btn1.setImage(UIImage.init(named: "RedFilledStar"), for: .normal)
            btn2.setImage(UIImage.init(named: "RedFilledStar"), for: .normal)
            btn3.setImage(UIImage.init(named: "RedFilledStar"), for: .normal)
            btn4.setImage(UIImage.init(named: "RedFilledStar"), for: .normal)
            btn5.setImage(UIImage.init(named: "RedLineStar"), for: .normal)
        } else if selectedIndex == 4 {
            btn1.setImage(UIImage.init(named: "RedFilledStar"), for: .normal)
            btn2.setImage(UIImage.init(named: "RedFilledStar"), for: .normal)
            btn3.setImage(UIImage.init(named: "RedFilledStar"), for: .normal)
            btn4.setImage(UIImage.init(named: "RedFilledStar"), for: .normal)
            btn5.setImage(UIImage.init(named: "RedFilledStar"), for: .normal)
        }
    }


    
    // MARK: - IBActions
    
    @IBAction func action_dismissSurvey(sender: UIButton) {
        self.dismiss(animated: true) {
            if self.isSavedSurvey{
                self.callback?()
            }
        }
    }

    @IBAction func action_starTapped(sender: UIButton) {
        
        if selectedAnswer != nil,
           survayQuestion != nil,
           survayQuestion?.answers?[sender.tag].answerId == selectedAnswer?.answerId  {
            return
        }
        if AnswerStackView.isHidden == true {
            UIView.animate(withDuration: 0.25) {
                self.AnswerStackView.isHidden = false
                
            }
        }
        
        selectedAnswer = survayQuestion?.answers?[sender.tag]
        if MBLanguageManager.userSelectedLanguage() == .English {
            lblScore.text = selectedAnswer?.answerTextEN
        } else if MBLanguageManager.userSelectedLanguage() == .Russian {
            lblScore.text = selectedAnswer?.answerTextRU
        } else {
            lblScore.text = selectedAnswer?.answerTextAZ
        }
        updateStarsFor(selectedIndex: sender.tag)
    }
    
    
    @IBAction func submitTapped(_ sender: UIButton){
        self.saveSurveys()
    }
    

}


//MARK: Api Calls

extension InAppSurveyVC{
    
    func saveSurveys() {
     
        activityIndicator.showActivityIndicator()
        _ = MBAPIClient.sharedClient.saveSurvey(
            //Parameters to send
            comment: txtComments.text ?? "",
            answerId: self.selectedAnswer?.answerId  ?? "",
            questionId: self.currentSurvay?.questions?.first?.questionId ?? "",
            offeringIdSurvey: offeringId ?? "",
            offeringTypeSurvey: offeringType  ?? "",
            surveyId: self.currentSurvay?.survayId ?? "",
            
            { (response, resultData, error, isCancelled, status, resultCode, resultDesc) in
            
            self.activityIndicator.hideActivityIndicator()
            if error != nil {
                error?.showServerErrorInViewController(self)
                
            } else {
                // handling data from API response.
                if resultCode == Constants.MBAPIStatusCode.succes.rawValue ,
                    let homePageResponse = Mapper<InAppSurveyMapper>().map(JSONObject:resultData) {
                    MBUserSession.shared.appSurvays = homePageResponse
                   
                    if MBLanguageManager.userSelectedLanguage() == .English {
                        self.lblThanks.text = self.currentSurvay?.onTransactionCompleteEn ?? resultDesc
                    } else if MBLanguageManager.userSelectedLanguage() == .Russian {
                        self.lblThanks.text = self.currentSurvay?.onTransactionCompleteRu ?? resultDesc
                    } else {
                        self.lblThanks.text = self.currentSurvay?.onTransactionCompleteAz ?? resultDesc
                    }
                    UIView.animate(withDuration: 0.25) {
                        self.mainRatingStackView.isHidden = true
                        self.thanksStackView.isHidden = false 
                    }
                  
                    self.isSavedSurvey = true
                    
                    if self.surveyScreen == SurveyScreenName.dashboard.rawValue {
                        UserDefaults.standard.set(0, forKey: "DashboardScreenViews")
                    } else if self.surveyScreen == SurveyScreenName.fnf.rawValue {
                        UserDefaults.standard.set(0, forKey: "FNFScreenViews")
                    } else if self.surveyScreen == SurveyScreenName.getLoan.rawValue {
                        UserDefaults.standard.set(0, forKey: "GetLoanScreenViews")
                    } else if self.surveyScreen == SurveyScreenName.transferMoney.rawValue {
                        UserDefaults.standard.set(0, forKey: "BalanceShareScreenViews")
                    } else if self.surveyScreen == SurveyScreenName.topup.rawValue {
                        UserDefaults.standard.set(0, forKey: "TopUpScreenViews")
                    }
                    UserDefaults.standard.synchronize()
                    
                    
                    
//                    self.showSuccessAlertWithMessage(message: self.currentSurvay?.onTransactionComplete ?? resultDesc) {_ in
//                        self.dismiss(animated: true) {
//                            self.callback?()
//                        }
//                    }
                   
                } else {
                    // Show error alert to user
//                    self.showErrorAlertWithMessage(message: resultDesc)
                    self.showErrorAlertWithMessage(message: resultDesc) {_ in
                        self.dismiss(animated: true)
                    }
                }
            }
        })
    }
    
    
}
