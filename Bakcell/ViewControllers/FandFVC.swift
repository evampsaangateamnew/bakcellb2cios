//
//  FandFVC.swift
//  Bakcell
//
//  Created by Saad Riaz on 6/21/17.
//  Copyright © 2017 evampsaanga. All rights reserved.
//

import UIKit
import ObjectMapper

class FandFVC: BaseVC, UITableViewDataSource, UITableViewDelegate {
    
    // MARK: - Properties
    var fnfList : [FNFList]  = []
    var fnfMaxCount = UserDefaults.standard.integer(forKey: Constants.kFNFMaxCount)
    
    //MARK: - IBOutlet
    @IBOutlet var title_lbl: UILabel!
    @IBOutlet var description_lbl: UILabel!
    @IBOutlet var mobileNumber_txt: UITextField!
    @IBOutlet var add_btn: UIButton!
    @IBOutlet var paging_lbl: UILabel!
    @IBOutlet var tableView: UITableView!
    
    //MARK: - View controller functions
    override func viewDidLoad() {
        super.viewDidLoad()
        
        mobileNumber_txt.delegate = self
        tableView.allowsSelection = false
        
        tableView.allowsSelection = false
        mobileNumber_txt.delegate = self
        
        // Get FNF API call
        getFNFList()
        
        //setting number of views of this screen
        UserDefaults.standard.set(UserDefaults.standard.integer(forKey: "FNFScreenViews")+1, forKey: "FNFScreenViews")
        UserDefaults.standard.synchronize()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.interactivePop(true)
        
        layoutViewController()
    }
    
    //MARK: - TableView Delegates
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return fnfList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellID", for: indexPath) as! FFCell
        
        cell.number_lbl.text = fnfList[indexPath.row].msisdn ?? ""
        cell.date_lbl.text = fnfList[indexPath.row].createdDate ?? ""
        cell.delete_btn.tag = indexPath.row
        cell.delete_btn.addTarget(self, action: #selector(self.deleteNumberPressed(_:)), for: UIControl.Event.touchUpInside)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    //MARK: - @IBActions
    @IBAction func addNumberPressed(_ sender: Any) {
        
        if fnfList.count >= fnfMaxCount  {

            self.showErrorAlertWithMessage(message: Localized("Message_FNFMaxLimit"))
            self.add_btn.isEnabled = false
            return
        } else{
            self.add_btn.isEnabled = true
        }
        
        var msisdn = ""
        
        if let mobileNumberText = mobileNumber_txt.text,
            mobileNumberText.count == 9 {
            
            msisdn = mobileNumberText
            
        } else {
            self.showErrorAlertWithMessage(message: Localized("Message_EnterValidNumber"))
            return
        }
        
        // TextField resignFirstResponder
        mobileNumber_txt.resignFirstResponder()


        let confirmationAlert = self.myStoryBoard.instantiateViewController(withIdentifier: "ActivationConfirmationVC") as! ActivationConfirmationVC
        
        let confirmationMessage = String(format: Localized("Message_addFNFConfirmation"), msisdn)

        confirmationAlert.setConfirmationAlertLayout( message: confirmationMessage)

        confirmationAlert.setYesButtonCompletionHandler { (aString) in

            self.addIntoFNFList(msisdn: msisdn)
        }

        self.presentPOPUP(confirmationAlert, animated: true, completion: nil)

    }
    
    @IBAction func deleteNumberPressed(_ sender: Any) {

        if let aButton = sender as? UIButton {

            let confirmationAlert = self.myStoryBoard.instantiateViewController(withIdentifier: "ActivationConfirmationVC") as! ActivationConfirmationVC
            
            let confirmationMessage =  String(format: Localized("Message_DeleteFNFConfirmation"), self.fnfList[aButton.tag].msisdn ?? "")

            confirmationAlert.setConfirmationAlertLayout( message: confirmationMessage)

            confirmationAlert.setYesButtonCompletionHandler { (aString) in

                // Delete user fnf number
                let aFNFItem = self.fnfList[aButton.tag]
                self.deleteFNFList(msisdn: aFNFItem.msisdn ?? "")
            }
            self.presentPOPUP(confirmationAlert, animated: true, completion: nil)

        } else {

            self.showErrorAlertWithMessage(message: Localized("Message_GenralError"))
        }
    }
    
    //MARK: - Functions
    func layoutViewController() {
        /* Setting View controller text*/
        title_lbl.text                  = Localized("Title_fnf")
        description_lbl.text            = Localized("Description_fnf")
        mobileNumber_txt.placeholder    = Localized("Placeholder_FNFNumber")
    }
    
    func reloadTableViewDataAndCount() {
        
        showPageNumber()
        
        if fnfList.count < fnfMaxCount  {
            add_btn.setImage(UIImage(named: "Plus-state-2"), for: UIControl.State.normal)
            add_btn.isEnabled = true
        } else {
            add_btn.setImage(UIImage(named: "Plus-state-1"), for: UIControl.State.normal)
            add_btn.isEnabled = false
        }
        self.tableView.reloadData()
    }
    
    func showInAppSurvey() {
        if let survey  = MBUserSession.shared.appSurvays?.surveys?.first(where: { $0.screenName  == "fnf" }) {
            if survey.surveyCount?.toInt ?? 0 < survey.surveyLimit?.toInt ?? 0,
               survey.visitLimit != "-1",
               survey.visitLimit?.toInt ?? 0 <= UserDefaults.standard.integer(forKey: "FNFScreenViews") {
                if let inAppSurveyVC : InAppSurveyVC = InAppSurveyVC.fromNib() {
                    inAppSurveyVC.currentSurvay = survey
                    inAppSurveyVC.showTopUpStackView = false
                    inAppSurveyVC.hideBalanceStackView = true
                    self.presentPOPUP(inAppSurveyVC, animated: true, completion: nil)
                }
            }
        }
    }
    
    //MARK: - APIs call
    /// Call 'getFNF' API.
    ///
    /// - returns: Void
    func getFNFList() {
        
        activityIndicator.showActivityIndicator()
        
        _ = MBAPIClient.sharedClient.getFNF({ ( response, resultData, error, isCancelled, status, resultCode, resultDesc) in
            
            self.activityIndicator.hideActivityIndicator()
            if error != nil {
                error?.showServerErrorInViewController(self)
                
            } else {
                // handling data from API response.
                if resultCode == Constants.MBAPIStatusCode.succes.rawValue {
                    
                    // Parssing response data
                    if let FNFResponse = Mapper<FAndF>().map(JSONObject:resultData) {
                        
                        // Load new FNF count
                        self.fnfMaxCount = FNFResponse.fnfLimit?.toInt ?? 0
                        
                        /* Saving user fnf max count*/
                        UserDefaults.standard.set(self.fnfMaxCount, forKey: Constants.kFNFMaxCount)
                        
                        self.fnfList = FNFResponse.fnfList ?? []
                    }
                } else {
                    // Show error alert to user
                    self.showErrorAlertWithMessage(message: resultDesc)
                }
            }
            
            self.reloadTableViewDataAndCount()
        })
    }
    
    /// Call 'addIntoFNFList' API.
    ///
    ///msisdn : MSISDN need to be add
    /// - returns: Void
    func addIntoFNFList(msisdn : String) {
        
        
        activityIndicator.showActivityIndicator()
        
        _ = MBAPIClient.sharedClient.addFNF(MSISDN: msisdn,{ ( response, resultData, error, isCancelled, status, resultCode, resultDesc) in
            
            self.activityIndicator.hideActivityIndicator()
            if error != nil {
                //EventLog
                MBAnalyticsManager.logEvent(screenName: "Services Screen", contentType:"Services Friends & Family Screen" , status:"Friends & Family Number Add Failure")
                
                error?.showServerErrorInViewController(self)
                
            } else {
                // handling data from API response.
                if resultCode == Constants.MBAPIStatusCode.succes.rawValue {
                    
                    //EventLog
                    MBAnalyticsManager.logEvent(screenName: "Services Screen", contentType:"Services Friends & Family Screen" , status:"Friends & Family Number Add Success")
                    
                    
                    // Parssing response data
                    if let FNFResponse = Mapper<FAndF>().map(JSONObject:resultData) {
                        
                        self.mobileNumber_txt.text = ""
                        
                        if let fnfList = FNFResponse.fnfList {
                            self.fnfList = fnfList
                        } else {
                            self.fnfList = []
                        }
                        self.showInAppSurvey()
                    }
                    
                } else {
                    //EventLog
                    MBAnalyticsManager.logEvent(screenName: "Services Screen", contentType:"Services Friends & Family Screen" , status:"Friends & Family Number Add Failure")
                    // Show error alert to user
                    self.showErrorAlertWithMessage(message: resultDesc)
                }
            }
            self.reloadTableViewDataAndCount()
        })
    }
    
    /// Call 'deleteFNFList' API.
    ///
    ///msisdn : MSISDN need to be Delete
    /// - returns: Void
    func deleteFNFList(msisdn : String) {

        activityIndicator.showActivityIndicator()
        
        _ = MBAPIClient.sharedClient.deleteFNF(MSISDN: msisdn,{ ( response, resultData, error, isCancelled, status, resultCode, resultDesc) in
            
            self.activityIndicator.hideActivityIndicator()
            if error != nil {
                //EventLog
                MBAnalyticsManager.logEvent(screenName: "Services Screen", contentType:"Services Friends & Family Screen" , status:"Friends & Family Number Delete Failure")
                error?.showServerErrorInViewController(self)
                
            } else {
                // handling data from API response.
                if resultCode == Constants.MBAPIStatusCode.succes.rawValue {
                    
                    //EventLog
                    MBAnalyticsManager.logEvent(screenName: "Services Screen", contentType:"Services Friends & Family Screen" , status:"Friends & Family Number Delete Success")
                    
                    // Parssing response data
                    if let FNFResponse = Mapper<FAndF>().map(JSONObject:resultData) {
                        
                        if let fnfList = FNFResponse.fnfList {
                            self.fnfList = fnfList
                        } else {
                            self.fnfList = []
                        }
                    }
                } else {
                    //EventLog
                    MBAnalyticsManager.logEvent(screenName: "Services Screen", contentType:"Services Friends & Family Screen" , status:"Friends & Family Number Delete Failure")
                    // Show error alert to user
                    self.showErrorAlertWithMessage(message: resultDesc)
                }
            }
            self.reloadTableViewDataAndCount()
        })
    }
}

//MARK: - Textfield delagates

extension FandFVC : UITextFieldDelegate {
    
    //Set max Limit for textfields
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let text = textField.text else { return false } // Check that if text is nill then return false
        
        let newLength = text.count + string.count - range.length
        
        if textField == mobileNumber_txt {
            
            let allowedCharacters = CharacterSet(charactersIn: Constants.allowedNumbers)
            let characterSet = CharacterSet(charactersIn: string)
            if newLength <= 9 && allowedCharacters.isSuperset(of: characterSet) {
                
                // Returnimg number input
                return  true
            } else {
                return  false
            }
            
        } else  {
            return false
        }
    }
}

extension FandFVC:UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        showPageNumber()
    }
    
    func showPageNumber() {
        
        let visibleRect = CGRect(origin: tableView.contentOffset, size: tableView.bounds.size)
        let visiblePoint = CGPoint(x: visibleRect.midX, y: visibleRect.maxY)
        let indexPath = tableView.indexPathForRow(at: visiblePoint)
        
        if indexPath != nil {
            if fnfList.count > 0 {
                
                let index : Int = indexPath?.item ?? 0
                paging_lbl.text = "\(index + 1 )/\(fnfMaxCount)"
            } else {
                paging_lbl.text = "0/\(fnfMaxCount)"
            }
        } else {
            paging_lbl.text = "\(fnfList.count)/\(fnfMaxCount)"
        }
    }
}
