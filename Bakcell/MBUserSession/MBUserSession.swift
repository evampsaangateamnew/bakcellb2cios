//
//  MBUserSession.swift
//  Bakcell
//
//  Created by AbdulRehman Warraich on 8/17/17.
//  Copyright © 2017 evampsaanga. All rights reserved.
//

import UIKit

enum MBSubscriberType : String {
    case PrePaid = "Prepaid"
    case PostPaid = "Postpaid"
    case UnSpecified = ""
}

enum MBCustomerType : String {
    case Individual = "Individual Customer"
    case Corporate = "Corporate Customer"
    case UnSpecified = ""
}

enum MBBrandName : String {
    case CIN = "CIN"
    case Klass = "Klass"
    case Business = "Business"
    case UnSpecified = ""
}

// MARK: - Singleton
final class MBUserSession: NSObject {
    
    // Can't init is singleton
    override private init() {
        super.init()
    }
    // MARK:- Shared Instance
    static let shared = MBUserSession()
    
    // MARK:- Properties
    /* New params */
    var loggedInUsers           : ManageAccountModel?
    var appMenu                 : AppMenusModel?
    var balance                 : MainWallet?
    var mySubscriptionOffers    : MySubscription?
    var liveChatObject          : LiveChatWebViewVC?
    var notificationPopupConfig : NotificationPopupConfig?
    var unreadNotificaitonCount : String = ""
    
    var promoMessage            : String?
    
    var shouldCallAndUpdateAppResumeData    : Bool = false
    var shouldReloadDashboardData           : Bool = false
    var isPushNotificationSelected          : Bool = false
    var isOTPVerified                       : Bool = false
    var changePasswordRequestCount          : Int = 0
    var appSurvays: InAppSurveyMapper?
    var selectedSurveyObject = SelectedSurvey()
    
    let dateFormatter = DateFormatter(withFormat: Constants.kHomeDateFormate, locale: "en_US_POSIX")
    let dateFormatter2 = DateFormatter(withFormat: Constants.kDisplayFormat, locale: "en_US_POSIX")
    
    // MARK:- Computed Properties
    var userInfo : NewCustomerData? {
        return loggedInUsers?.currentUser?.userInfo
    }
    
    var predefineData : PredefinedData? {
        return loggedInUsers?.currentUser?.predefineData
    }
    
    var appConfig : AppConfig? {
        return loggedInUsers?.currentUser?.appConfig
    }
    
    var token : String {
        
        // Load user info if user loggedIn
        _ = self.loadUserInfomation()
        
        if let token = self.userInfo?.token {
            return token
        } else {
            return ""
        }
    }
    
    var msisdn : String {
        
        // Load user info if user loggedIn
        _ = self.loadUserInfomation()
        
        if let msisdn = self.userInfo?.msisdn {
            return msisdn
        } else {
            return ""
        }
    }
    
    var subscriberTypeLowerCaseStr : String {
        
        // Load user info if user loggedIn
        _ = self.loadUserInfomation()
        
        if let subscriberType = self.userInfo?.subscriberType {
            return subscriberType.lowercased()
        } else {
            return ""
        }
    }
    
    var brandNameLowerCaseStr : String {
        // Load user info if user loggedIn
        _ = self.loadUserInfomation()
        
        if let brandName = self.userInfo?.brandName {
            return brandName.lowercased()
        } else {
            return ""
        }
    }
    
    var groupType : String {
        
        // Load user info if user loggedIn
        _ = self.loadUserInfomation()
        
        if let groupType = self.userInfo?.groupType {
            return groupType
        } else {
            return ""
        }
    }
    
    var customerType : String {
        
        // Load user info if user loggedIn
        _ = self.loadUserInfomation()
        
        if let customerType = self.userInfo?.customerType {
            return customerType
        } else {
            return ""
        }
    }
    
    var aSecretKey : String {
        
        let key = "hbnnV6jRcxEUEcdKHs4jMuTbpQyTfEdt+7+Zloen5KU=".aesDecrypt(key: "s@@ng@tibe*&~~~jkm@@st@r")
        return key
        
    }
    
    
    // MARK:- Function
    
    func isLoggedIn() -> Bool {
        // Get Data from UserDefaults
        return UserDefaults.standard.bool(forKey: Constants.kIsUserLoggedInKey)
    }
    
    //Concatinating UserName
    func userName() -> String {
        
        // Load user info if user loggedIn
        _ = self.loadUserInfomation()
        
        var name : String = ""
        
        if let firstName = userInfo?.firstName {
            name = firstName
        }
        
        if let middleName = userInfo?.middleName {
            name = name + " " + middleName
        }
        
        if let lastName = userInfo?.lastName {
            name = name + " " + lastName
        }
        name = name.replacingOccurrences(of: "  ", with: " ")
        
        if name.isBlank == false {
            return name
        } else {
            return ""
        }
    }
    
    func subscriberType() -> MBSubscriberType {
        
        // Load user info if user loggedIn
        _ = self.loadUserInfomation()
        
        if let subscriberType = self.userInfo?.subscriberType {
            
            if subscriberType.isEqualToStringIgnoreCase(otherString: MBSubscriberType.PrePaid.rawValue) {
                return MBSubscriberType.PrePaid
                
            } else if subscriberType.isEqualToStringIgnoreCase(otherString: MBSubscriberType.PostPaid.rawValue){
                
                return MBSubscriberType.PostPaid
            } else {
                return MBSubscriberType.UnSpecified
            }
            
        } else {
            return MBSubscriberType.UnSpecified
        }
    }
    
    func userCustomerType() -> MBCustomerType {
        
        // Load user info if user loggedIn
        _ = self.loadUserInfomation()
        
        if let customerType = self.userInfo?.customerType {
            
            if customerType.isEqualToStringIgnoreCase(otherString: MBCustomerType.Individual.rawValue) {
                return MBCustomerType.Individual
                
            } else if customerType.isEqualToStringIgnoreCase(otherString: MBCustomerType.Corporate.rawValue) {
                
                return MBCustomerType.Corporate
            } else {
                return MBCustomerType.UnSpecified
            }
            
        } else {
            return MBCustomerType.UnSpecified
        }
    }
    
    func brandName() -> MBBrandName {
        
        // Load user info if user loggedIn
        _ = self.loadUserInfomation()
        
        if let brandName = self.userInfo?.brandName {
            
            if brandName.isEqualToStringIgnoreCase(otherString: MBBrandName.CIN.rawValue) {
                return MBBrandName.CIN
                
            } else if brandName.isEqualToStringIgnoreCase(otherString: MBBrandName.Klass.rawValue) {
                
                return MBBrandName.Klass
                
            } else if brandName.isEqualToStringIgnoreCase(otherString: MBBrandName.Business.rawValue) {
                
                return MBBrandName.Business
                
            }else {
                return MBBrandName.UnSpecified
            }
            
        } else {
            return MBBrandName.UnSpecified
        }
    }
    
    func loadUserInfomation() -> Bool {
        
        if self.userInfo == nil {
            
            if self.isLoggedIn() {
                // Load customer information
                let usersInfoObject = MBUserInfoUtilities.loadCustomerDataFromUserDefaults()
                self.loggedInUsers = usersInfoObject.usersInfo
                return usersInfoObject.hasInfo
            } else {
                return false
            }
        }
        return true
    }
    
    func isStatusActive() -> Bool {
        let statusText = MBUserSession.shared.userInfo?.statusDetails ?? ""
        
        if statusText.isEqualToStringIgnoreCase(otherString: "B01") {
            return true
        } else {
            return false
        }
    }
    
    func shouldHideCoreServices() -> Bool {
        
        if let offeringId = MBUserSession.shared.userInfo?.offeringId {
            return self.userInfo?.hideNumberTariffIds?.contains(offeringId.trimmWhiteSpace) ?? false
        }
        
        return false
    }
    
    func selectedBillingLanguageID() -> Constants.MBLanguage {
        
        
        if let language : String = userInfo?.billingLanguage {
            
            let billingLanguage = Constants.MBLanguage(rawValue: language)
            
            if billingLanguage == nil {
                return Constants.MBLanguage.Azeri
            } else {
                return billingLanguage ?? Constants.MBLanguage.Azeri
            }
            
        } else {
            return Constants.MBLanguage.Azeri
        }
    }
    
    func userSelectedBillingLanguageLanguage() -> String {
        
        switch self.selectedBillingLanguageID() {
            
        case .English:
            return "English"
            
        case .Azeri:
            return "Azərbaycan"
            
        case .Russian:
            return "Pу́сский"
        }
    }
    
    func isBiometricEnabled() -> Bool {
        // Get Data from UserDefaults
        return UserDefaults.standard.bool(forKey: Constants.K_IsBiometricEnabled)
    }
    
    func isRoamingEnabled() -> Bool {
        // Get Data from UserDefaults
        return UserDefaults.standard.bool(forKey: Constants.K_IsRoamingEnabled)
    }

    func saveLoggedInUsersCurrentStateInfo() {
        /* Save Users info */
        if let msisdnString = self.userInfo?.msisdn,
            let currentUserInfo = loggedInUsers?.currentUser {
            MBUserInfoUtilities.updateUserInfo(loggedInUserMSISDN: msisdnString, userInfo: currentUserInfo)
        }
    }
    
    func clearUserSession() {
        let userDefaults = UserDefaults.standard
        
        let userSelectedLanguage = MBLanguageManager.userSelectedLanguage()
        
        // Remove all data from UserDefaults
        UserDefaults.standard.removePersistentDomain(forName: Bundle.main.bundleIdentifier!)
        userDefaults.synchronize()
        
        // Set values in user defaults which are changed only once
        MBUtilities.updateLaunchedBeforeStatus()
        MBUtilities.updateTutorialShowenStatus()
        MBLanguageManager.setUserLanguage(selectedLanguage: userSelectedLanguage)
        
        self.loggedInUsers = nil
        
        self.clearCurrentUserSessionObjects()
    }
    
    func clearCurrentUserSessionObjects() {
        
        appMenu               = nil
        balance               = nil
        mySubscriptionOffers  = nil
        liveChatObject        = nil
        
        promoMessage          = nil
        
        shouldCallAndUpdateAppResumeData    = false
        shouldReloadDashboardData           = false
        isPushNotificationSelected          = false
        isOTPVerified                       = false
        changePasswordRequestCount          = 0
        
    }
}
