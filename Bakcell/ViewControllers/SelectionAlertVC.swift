//
//  SelectionAlertVC.swift
//  Bakcell
//
//  Created by Saad Riaz on 8/8/17.
//  Copyright © 2017 evampsaanga. All rights reserved.
//

import UIKit
import ObjectMapper

class SelectionAlertVC: BaseVC {
    
    //MARK: - Properties
    var selectedLanguage : Constants.MBLanguage = .English
    var isBillingLanguage = false
    fileprivate var okBtncompletionHandlerBlock : MBButtonCompletionHandler?
    
    //MARK: - IBOutlet
    @IBOutlet var done_btn: UIButton!
    @IBOutlet var title_lbl: UILabel!
    @IBOutlet var check1_btn: UIButton!
    @IBOutlet var check2_btn: UIButton!
    @IBOutlet var check3_btn: UIButton!
    
    
    //MARK: - View Controller methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title_lbl.text = Localized("PleaseSelect_title")
        done_btn.setTitle(Localized("Btn_title_Done"), for: UIControl.State.normal)
        done_btn.roundTopCorners(radius: CGFloat(Constants.kButtonCornerRadius))
        done_btn.backgroundColor = UIColor.MBButtonBackgroundGrayColor
        
        if isBillingLanguage  {
            selectedLanguage = MBUserSession.shared.selectedBillingLanguageID()
        } else {
            selectedLanguage = MBLanguageManager.userSelectedLanguage()
        }
        
        setlectLanguage(senderType : selectedLanguage)
    }
    
    //MARK: - IBACTIONS
    
    @IBAction func donePressed(_ sender: UIButton) {
        
        if !isBillingLanguage {
            // Setting user language
            MBLanguageManager.setUserLanguage(selectedLanguage: selectedLanguage)
        }
        self.okBtncompletionHandlerBlock?(selectedLanguage.rawValue)
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func check1Pressed(_ sender: Any) {
        setlectLanguage(senderType : Constants.MBLanguage.Azeri)
    }
    
    @IBAction func check2Pressed(_ sender: Any) {
        setlectLanguage(senderType : Constants.MBLanguage.Russian)
    }
    
    @IBAction func check3Pressed(_ sender: Any) {
        setlectLanguage(senderType : Constants.MBLanguage.English)
        
    }
    
    //MARK: - FUNCTIONS
    func setlectLanguage(senderType : Constants.MBLanguage) {
        
        check1_btn .setImage(#imageLiteral(resourceName: "Checkbox-state2"), for: UIControl.State.normal)
        check2_btn .setImage(#imageLiteral(resourceName: "Checkbox-state2"), for: UIControl.State.normal)
        check3_btn .setImage(#imageLiteral(resourceName: "Checkbox-state2"), for: UIControl.State.normal)
        
        switch senderType {
        case .Azeri:
            check1_btn .setImage(#imageLiteral(resourceName: "Checkbox-state1"), for: UIControl.State.normal)
            selectedLanguage = Constants.MBLanguage.Azeri
            
        case .Russian:
            check2_btn .setImage(#imageLiteral(resourceName: "Checkbox-state1"), for: UIControl.State.normal)
            selectedLanguage = Constants.MBLanguage.Russian
            
        case .English:
            
            check3_btn .setImage(#imageLiteral(resourceName: "Checkbox-state1"), for: UIControl.State.normal)
            selectedLanguage = Constants.MBLanguage.English
            
        }
    }
    
    func setOKButtonCompletionHandler(completionBlock : @escaping MBButtonCompletionHandler ) {
        self.okBtncompletionHandlerBlock = completionBlock
    }
}
