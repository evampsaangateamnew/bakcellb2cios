//
//  TopupCardCell.swift
//  Bakcell
//
//  Created by Muhammad Irfan Awan on 03/03/2021.
//  Copyright © 2021 evampsaanga. All rights reserved.
//

import UIKit

class TopupCardCell: UITableViewCell {
    
    @IBOutlet var cardImage: UIImageView!
    @IBOutlet var lblCardNumber: UILabel!
    @IBOutlet var radioButton: UIButton!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
