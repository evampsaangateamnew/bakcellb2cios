//
//  SubTitleValueAndDescView.swift
//  Bakcell
//
//  Created by AbdulRehman Warraich on 8/30/17.
//  Copyright © 2017 evampsaanga. All rights reserved.
//

import UIKit

class SubTitleValueAndDescView: UIView {
    @IBOutlet var subTitle_lbl: UILabel!
    @IBOutlet var subValue_lable: UILabel!
    @IBOutlet var unit_lbl: UILabel!
    @IBOutlet var subDescription_lbl: UILabel!

    @IBOutlet var descriptionLabelHeightConstraint: NSLayoutConstraint!
    @IBOutlet var unitWidthConstranint: NSLayoutConstraint!

    class func instanceFromNib() -> UIView {
        return UINib(nibName: "SubTitleValueAndDescView", bundle: Bundle.main).instantiate(withOwner: nil, options: nil)[0] as! SubTitleValueAndDescView
    }

    func setSubTitleValueAndDescription(aAttribute : AttributeList?) -> CGFloat {

        if let aAttribute = aAttribute {

            subTitle_lbl.text = aAttribute.title ?? ""

            if aAttribute.unit?.isEqualToStringIgnoreCase(otherString: "AZN") ?? false {
                unitWidthConstranint.constant = 10
                subValue_lable.text = aAttribute.value ?? ""

            } else {
                let unitValue = aAttribute.unit?.removeNullValues() ?? ""
                unitWidthConstranint.constant = 0
                subValue_lable.text = "\(aAttribute.value ?? "") \(unitValue)"

            }

            if let description = aAttribute.description {
//                subDescription_lbl.text = description
                subDescription_lbl.loadHTMLString(htmlString: description)
                descriptionLabelHeightConstraint.constant = subDescription_lbl.heightToFitContent()
            } else {
                subDescription_lbl.text = ""
                descriptionLabelHeightConstraint.constant = 0
            }
            return (subDescription_lbl.frame.origin.y + descriptionLabelHeightConstraint.constant + 1.0)
        } else {
            subTitle_lbl.text = ""
            subValue_lable.text = ""
            subDescription_lbl.text = ""
            unitWidthConstranint.constant = 0
        }
        return 0.0
    }

}
