//
//  LoanMainVC.swift
//  Bakcell
//
//  Created by Saad Riaz on 6/14/17.
//  Copyright © 2017 evampsaanga. All rights reserved.
//

import UIKit
import ObjectMapper

class LoanMainVC: BaseVC {
    
    // Properties
    var selectedValue: String = ""
    var amountOptions : [String] = []
    
    // IBOutlet
    @IBOutlet var dropDownImage: UIImageView!
    @IBOutlet var getCredit_btn: UIButton!
    @IBOutlet var amount_lbl: UILabel!
    @IBOutlet var currentAmount_lbl: UILabel!
    @IBOutlet var intro_description: UILabel!
    @IBOutlet var currentCreditAmount_lbl: UILabel!
    @IBOutlet var dropDown: UIDropDown!
    
    //MARK:- ViewController Method
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let homePageResponse :Homepage = Homepage.loadFromUserDefaults(key: APIsType.dashboard.selectedLocalizedAPIKey()),
            let creditTitleValue = homePageResponse.credit?.creditTitleValue,
        creditTitleValue.isBlank == false {
            
            currentCreditAmount_lbl.text = creditTitleValue
        } else {
            currentCreditAmount_lbl.text = "0.0"
        }
        
        
        // Get saved loan information

        if let getLoan = MBUserSession.shared.predefineData?.topup?.getLoan {
            
            //Check for Brand to popuate dropDown
            if MBUserSession.shared.brandName() == MBBrandName.Klass{
                
                if let klassAmountOptions = getLoan.selectAmount?.klass {
                    
                    klassAmountOptions.forEach({ (aSelectAmount) in
                        
                        if let aAmount = aSelectAmount.amount {
                            self.amountOptions.append("\(aAmount)")
                        }
                    })
                }
            } else {
                if let cinAmountOptions = getLoan.selectAmount?.cin {
                    
                    cinAmountOptions.forEach({ (aSelectAmount) in
                        
                        if let aAmount = aSelectAmount.amount {
                            self.amountOptions.append("\(aAmount)")
                            
                        }
                    })
                }
            }
        }
        
        //Drop Down
        dropDown.optionsTextAlignment = NSTextAlignment.center
        dropDown.textAlignment = NSTextAlignment .center
        dropDown.placeholder = Localized("DropDown_SelectAmount")
        self.dropDown.rowBackgroundColor = UIColor.MBDimLightGrayColor
        dropDown.setFont = UIFont.MBArial(fontSize: 12)
        
        if amountOptions.count < 5 {
            dropDown.tableHeight = CGFloat(35 * amountOptions.count)
        } else {
            dropDown.tableHeight = 175
        }
        
        
        // preparing data for options
        var amountOptionsWithSign : [String] = []
        amountOptions.forEach({ (amount) in
            amountOptionsWithSign.append("\(amount)₼")
        })
        
        dropDown.options = amountOptionsWithSign
        
        self.dropDown.tableWillAppear {
            self.dropDownImage.image = UIImage (named: "Drop-down-arrow-state2")
        }
        
        self.dropDown.tableWillDisappear {
            self.dropDownImage.image = UIImage (named: "Drop-down-arrow-state1")
        }
        
        dropDown.didSelect { (option, index) in
            
            self.selectedValue = self.amountOptions[index]
            
        }
        
        //setting number of views of this screen
        UserDefaults.standard.set(UserDefaults.standard.integer(forKey: "GetLoanScreenViews")+1, forKey: "GetLoanScreenViews")
        UserDefaults.standard.synchronize()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        loadViewLayout()
    }
    
    
    //MARK: IBACTIONS
    @IBAction func GetCreditPressed(_ sender: UIButton) {
        
        if self.selectedValue.isBlank == false {
            let alert = self.myStoryBoard.instantiateViewController(withIdentifier: "AlertVC") as! AlertVC
            self.presentPOPUP(alert, animated: true, completion: nil)
            
            alert.setConfirmationAlertLayoutForGetLoan(title: Localized("Title_Confirmation"), amount: self.selectedValue)
            
            alert.setYesButtonCompletionHandler { (aString) in
                self.getCredit(LoanAmount: self.selectedValue)
            }
            
        } else {
            
            self.showErrorAlertWithMessage(message: Localized("Message_NothingSelecterFilter"))
        }
    }
    
    //MARK: - FUNCTIONS
    func loadViewLayout(){
        getCredit_btn.titleLabel?.font = UIFont.MBArial(fontSize: 16)
        intro_description.font = UIFont.MBArial(fontSize: 14)
        currentAmount_lbl.font = UIFont.MBArial(fontSize: 14)
        amount_lbl.font = UIFont.MBArial(fontSize: 14)
        currentCreditAmount_lbl.font = UIFont.MBArial(fontSize: 16)
        
        intro_description.text = Localized("Title_Intro")
        currentAmount_lbl.text = Localized("Title_CurrentAmount")
        amount_lbl.text = Localized("Loan_Amount")
        getCredit_btn.setTitle(Localized("BtnTitle_GetCredit"), for: UIControl.State.normal)
        
    }
    
    //MARK: -  in-App-Survery
    func checkLoanSurvey(amount:String, showBalanceOfUser:Bool) {
        var attributedBalanceTitle = NSMutableAttributedString()
        if  showBalanceOfUser == false {
            attributedBalanceTitle = NSMutableAttributedString()

        } else {
            attributedBalanceTitle = self.setBalanceText(amount: MBUserSession.shared.balance?.amount ?? "0.0", isBalanceColorBlack: false)
        }

        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.alignment = .left

        let messageString = String(format: Localized("Msg_LoanSuccess"), "\(amount) AZN")

        let message = NSMutableAttributedString(string: messageString, attributes: [NSAttributedString.Key.foregroundColor: UIColor.MBTextGrayColor, NSAttributedString.Key.paragraphStyle:paragraphStyle, NSAttributedString.Key.font: UIFont.MBArial(fontSize: 14)])

        let amountTextRange = (messageString as NSString).range(of: "\(amount) AZN")
        message.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.black, range: amountTextRange)
        
        if let survey  = MBUserSession.shared.appSurvays?.surveys?.first(where: { $0.screenName  == "get_loan" }) {
            if survey.surveyCount?.toInt ?? 0 < survey.surveyLimit?.toInt ?? 0,
               survey.visitLimit != "1",
               survey.visitLimit?.toInt ?? 0 <= UserDefaults.standard.integer(forKey: "GetLoanScreenViews") {
                if let inAppSurveyVC : InAppSurveyVC = InAppSurveyVC.fromNib() {
                    inAppSurveyVC.currentSurvay = survey
                    inAppSurveyVC.showTopUpStackView = true
                    inAppSurveyVC.hideBalanceStackView = false
                    inAppSurveyVC.attributedBalance = attributedBalanceTitle
                    inAppSurveyVC.toptupTitleText = Localized("Title_successful")
                    inAppSurveyVC.attributedMessage = message
                    inAppSurveyVC.confirmationText = Localized("Title_successful")
                    self.presentPOPUP(inAppSurveyVC, animated: true, completion: nil)
                }
            } else {
                let alert = self.myStoryBoard.instantiateViewController(withIdentifier: "AlertVC") as! AlertVC
                
                alert.setSuccessAlertForGetLoan(title: Localized("Title_successful"), amount: "\(amount) AZN", showBalnce: showBalanceOfUser)
                
                self.presentPOPUP(alert, animated: true, completion: nil)
            }
        }
    }
    
    
    func setBalanceText(amount: String, isBalanceColorBlack: Bool = true) -> NSMutableAttributedString {

        let balanceText = NSMutableAttributedString(string: Localized("Balance"), attributes: [NSAttributedString.Key.foregroundColor: UIColor.MBTextGrayColor, NSAttributedString.Key.font: UIFont.MBArial(fontSize: 14)])

        let amountAttributedText = NSMutableAttributedString(string: "\(amount) ₼" ,attributes: [ NSAttributedString.Key.font: UIFont.MBArial(fontSize: 14)])

        if isBalanceColorBlack {

            amountAttributedText.addAttribute(NSAttributedString.Key.foregroundColor,
                                              value: UIColor.black,
                                              range: NSRange(location: 0 , length: amountAttributedText.length))
        } else {

            amountAttributedText.addAttribute(NSAttributedString.Key.foregroundColor,
                                              value: UIColor.MBRedColor,
                                              range: NSRange(location: 0 , length: amountAttributedText.length))
        }

        balanceText.append(amountAttributedText)

        balanceText.addAttribute(NSAttributedString.Key.font,
                                 value: UIFont.systemFont(ofSize: 12),
                                 range: NSRange(location: (balanceText.length - 1), length: 1))

        balanceText.addAttribute(NSAttributedString.Key.baselineOffset,
                                 value:2.00,
                                 range: NSRange(location:(balanceText.length - 1),length:1))

        return balanceText
    }
    
    
    //MARK: - API Calls
    /// Call 'getLoan' API.
    ///
    /// - returns: newBalance
    func getCredit(LoanAmount amount : String) {
        activityIndicator.showActivityIndicator()
        
        _ = MBAPIClient.sharedClient.getLoan(LoanAmount: amount, { (response, resultData, error, isCancelled, status, resultCode, resultDesc) in
            
            self.activityIndicator.hideActivityIndicator()
            if error != nil {
                //EventLog
                MBAnalyticsManager.logEvent(screenName: "Top up Screen", contentType:"Loan Screen" , status:"Loan Failure" )
                
                error?.showServerErrorInViewController(self)
                
            } else {
                // handling data from API response.
                if resultCode == Constants.MBAPIStatusCode.succes.rawValue {
                    
                    //EventLog
                    MBAnalyticsManager.logEvent(screenName: "Top up Screen", contentType:"Loan Screen" , status:"Loan Success" )
                    
                    // Set true to reload Home page data
                    MBUserSession.shared.shouldReloadDashboardData = true
                    
                    
                    // Parssing response data
                    if let getCreditResponse = Mapper<NewBalance>().map(JSONObject: resultData) {
                        
                        let showBalanceOfUser:Bool = true
                        
                        // Current balance changed
                        if let newBalance = getCreditResponse.newBalance {
                            MBUserSession.shared.balance?.amount = newBalance
                        }
                        
                        // creditTitleValue changed
                        if let newCredit = getCreditResponse.newCredit {
                            
                            self.currentCreditAmount_lbl.text = newCredit
                            
                            if var homePageResponse :Homepage = Homepage.loadFromUserDefaults(key: APIsType.dashboard.selectedLocalizedAPIKey()) {
                                
                                homePageResponse.credit?.creditTitleValue = newCredit
                                homePageResponse.saveInUserDefaults(key: APIsType.dashboard.selectedLocalizedAPIKey())
                            }
                        }
                        
                        
                        DispatchQueue.main.asyncAfter(deadline: .now()+0.25) {
                            self.checkLoanSurvey(amount: amount, showBalanceOfUser: showBalanceOfUser)
                        }
                        /*let alert = self.myStoryBoard.instantiateViewController(withIdentifier: "AlertVC") as! AlertVC
                        
                        alert.setSuccessAlertForGetLoan(title: Localized("Title_successful"), amount: "\(amount) AZN", showBalnce: showBalanceOfUser)
                        
                        self.presentPOPUP(alert, animated: true, completion: nil)*/
                        
                    }
                    
                }  else {
                    
                    //EventLog
                    MBAnalyticsManager.logEvent(screenName: "Top up Screen", contentType:"Loan Screen" , status:"Loan Failure" )
                    
                    // Show error alert to user
                    self.showErrorAlertWithMessage(message: resultDesc)
                }
            }
        })
    }
}
