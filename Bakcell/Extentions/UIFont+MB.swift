//
//  UIFont+MB.swift
//  Bakcell
//
//  Created by AbdulRehman Warraich on 9/18/17.
//  Copyright © 2017 evampsaanga. All rights reserved.
//

import Foundation
import UIKit

extension UIFont {
    class func MBArial(fontSize: Float) -> UIFont {
        return UIFont(name: "ArialMT", size: CGFloat(fontSize)) ?? UIFont.systemFont(ofSize: CGFloat(fontSize))
    }
    class func MBArialBold(fontSize: Float) -> UIFont {
        return UIFont(name: "Arial-BoldMT", size: CGFloat(fontSize)) ?? UIFont.systemFont(ofSize: CGFloat(fontSize), weight: UIFont.Weight.bold)
    }
}
