//
//  ChangeEmailVCViewController.swift
//  Bakcell
//
//  Created by AbdulRehman Warraich on 9/28/17.
//  Copyright © 2017 evampsaanga. All rights reserved.
//

import UIKit
import ObjectMapper

class ChangeEmailVC: BaseVC {
    
    //MARK:- Properties
    @IBOutlet var description_lbl: UILabel!
    @IBOutlet var email_txt: UITextField!
    @IBOutlet var update_btn: UIButton!
    @IBOutlet var changeEmailTitle_lbl: UILabel!
    
    //MARK:- ViewController Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        email_txt.delegate = self
        update_btn.layer.cornerRadius = CGFloat(Constants.kButtonCornerRadius)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.interactivePop(true)
        
        loadViewLayout()
    }
    
    //MARK:- IBAction
    @IBAction func updatePressed(_ sender:AnyObject) {
        
        if email_txt.text?.isValidEmail() ?? false {
            
            if let alert = self.myStoryBoard.instantiateViewController(withIdentifier: "AlertVC") as? AlertVC {
                self.presentPOPUP(alert, animated: true, completion: nil)
                
                alert.setConfirmationAlertLayoutWithOutBalance(title: Localized("Title_Confirmation"), message: Localized("Message_EmailChangeConfirmation"))
                
                alert.setYesButtonCompletionHandler(completionBlock: { (aString) in
                    // logout user
                    self.updateCustomerEmail(newEmail: self.email_txt.text)
                })
            }
            
        } else {
            
            if email_txt.text?.isBlank ?? false {
                
                self.showErrorAlertWithMessage(message: Localized("Message_EnterEmail"))
                
            } else {
                self.showErrorAlertWithMessage(message: Localized("Message_invalidEmail"))
            }
        }
    }
    
    //MARK: - FUNCTIONS
    func loadViewLayout(){
        changeEmailTitle_lbl.text = Localized("ChangeEmail_Title")
        description_lbl.text = Localized("CM_Info")
        email_txt.placeholder = Localized("PlaceHolder_EnterNewEmail")
        update_btn.setTitle(Localized("CM_UpdateBtn"), for: UIControl.State.normal)
        
    }
    //MARK: - API Calls
    /// Call 'updateCustomerEmail' API .
    ///
    /// - returns: Void
    func updateCustomerEmail(newEmail : String?) {
        
        email_txt.resignFirstResponder()
        
        activityIndicator.showActivityIndicator()
        _ = MBAPIClient.sharedClient.updateCustomerEmail(NewEmail: newEmail ?? "" ,{ (response, resultData, error, isCancelled, status, resultCode, resultDesc) in
            
            self.activityIndicator.hideActivityIndicator()
            
            if error != nil {
                //EventLog
                MBAnalyticsManager.logEvent(screenName: "Profile Screen", contentType:"Change Email Screen" , status:"Email Changed Failure" )
                error?.showServerErrorInViewController(self)
                
            } else {
                // handling data from API response.
                if resultCode == Constants.MBAPIStatusCode.succes.rawValue {
                    //EventLog
                    MBAnalyticsManager.logEvent(screenName: "Profile Screen", contentType:"Change Email Screen" , status:"Email Changed Success" )
                    
                    // Parssing response data
                    if let responseHandler = Mapper<MessageResponse>().map(JSONObject: resultData){
                        
                        //MBUserSession.shared.loggedInUsers?.currentUser?.userInfo?.email = newEmail
                        //MBUserSession.shared.saveLoggedInUsersCurrentStateInfo()
                        
                        self.showSuccessAlertWithMessage(message: responseHandler.message, { (aString) in
                            self.navigationController?.popViewController(animated: true)
                        })
                    }
                    
                }  else {
                    //EventLog
                    MBAnalyticsManager.logEvent(screenName: "Profile Screen", contentType:"Change Email Screen" , status:"Email Changed Failure" )
                    // Show error alert to user
                    self.showErrorAlertWithMessage(message: resultDesc)
                }
            }
        })
    }
}

//MARK: Textfield delagates
extension ChangeEmailVC : UITextFieldDelegate {
    
    //Set max Limit for textfields
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        guard let text = textField.text else { return false }
        let newLength = text.count + string.count - range.length
        
        if textField == email_txt {
            if newLength <= 50 {
                return  true
            } else {
                return  false
            }
            
        }  else {
            return false // Bool
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == email_txt {
            updatePressed(UIButton())
            return false
        } else {
            return true
        }
    }
}
