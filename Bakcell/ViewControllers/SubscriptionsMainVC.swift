//
//  SubscriptionsMainVC.swift
//  Bakcell
//
//  Created by Saad Riaz on 9/7/17.
//  Copyright © 2017 evampsaanga. All rights reserved.
//

import UIKit
import FZAccordionTableView

class SubscriptionsMainVC: BaseVC,UICollectionViewDelegate,UICollectionViewDataSource {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - COLLECTION VIEW DELEGATES
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 6
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SubscriptionCollectionCell", for: indexPath) as! SubscriptionCollectionCell
        
        cell.tableView.register(UINib(nibName: "StoreLocatorExpandCell", bundle: nil), forCellReuseIdentifier: "expandID")
        cell.tableView.register(UINib(nibName: "AccordionHeaderView", bundle: nil), forHeaderFooterViewReuseIdentifier: AccordionHeaderView.kAccordionHeaderViewReuseIdentifier)
        cell.tableView.register(UINib(nibName: "SubscriptionsHeaderView", bundle: nil), forHeaderFooterViewReuseIdentifier: SubscriptionsHeaderView.kAccordionHeaderViewReuseIdentifier)
        cell.tableView.register(UINib(nibName: "SubscriptionHeaderView3", bundle: nil), forHeaderFooterViewReuseIdentifier: SubscriptionHeaderView3.kAccordionHeaderViewReuseIdentifier)
        
        return cell
    }
}

// MARK: - <UITableViewDataSource> / <UITableViewDelegate> -

extension SubscriptionsMainVC : UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3;
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 1{
            return 1
        }
        
        return 0
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 90.0;
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        switch section {
        case 0:
            return 204.0
        case 1:
            return 50.0
        case 2:
            return 70.0
        default:
            return 55.0
        }
        // return nil
        //return AccordionHeaderView.kDefaultAccordionHeaderViewHeight
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "expandID", for: indexPath) as! StoreLocatorExpandCell
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView1 = tableView.dequeueReusableHeaderFooterView(withIdentifier: AccordionHeaderView.kAccordionHeaderViewReuseIdentifier) as! AccordionHeaderView
        let headerView2 = tableView.dequeueReusableHeaderFooterView(withIdentifier: SubscriptionsHeaderView.kAccordionHeaderViewReuseIdentifier) as! SubscriptionsHeaderView
        let headerView3 = tableView.dequeueReusableHeaderFooterView(withIdentifier: SubscriptionHeaderView3.kAccordionHeaderViewReuseIdentifier) as! SubscriptionHeaderView3
        
        headerView2.progressBar1.layer.cornerRadius = 5
        headerView2.progressBar2.layer.cornerRadius = 5
        if section == 0 {
            return headerView2
            
        }
        else if section == 1{
            headerView1.currencyView.isHidden = true
            headerView1.subscribe_btn.isHidden = true
            headerView1.validity_lbl.isHidden = true
            headerView1.offer_lbl.text = "Details"
            return headerView1
        }
        else if section == 2{
            return headerView3
        }
        
        return nil
    }
}

// MARK: - <FZAccordionTableViewDelegate> -

extension SubscriptionsMainVC : FZAccordionTableViewDelegate {
    
    func tableView(_ tableView: FZAccordionTableView, willOpenSection section: Int, withHeader header: UITableViewHeaderFooterView?) {
    }
    
    func tableView(_ tableView: FZAccordionTableView, didOpenSection section: Int, withHeader header: UITableViewHeaderFooterView?) {
        guard tableView.headerView(forSection: section) != nil else {
            return
        }
        let headerView = tableView.headerView(forSection: section) as! AccordionHeaderView
        headerView.Details_btn .setImage(#imageLiteral(resourceName: "minu-sign"), for: UIControlState.normal)
        
    }
    
    func tableView(_ tableView: FZAccordionTableView, willCloseSection section: Int, withHeader header: UITableViewHeaderFooterView?) {
        
    }
    
    func tableView(_ tableView: FZAccordionTableView, didCloseSection section: Int, withHeader header: UITableViewHeaderFooterView?) {
        guard tableView.headerView(forSection: section) != nil else {
            return
        }
        let headerView = tableView.headerView(forSection: section) as! AccordionHeaderView
        headerView.Details_btn .setImage(#imageLiteral(resourceName: "Plus-Sign"), for: UIControlState.normal)
        
    }
    
    func tableView(_ tableView: FZAccordionTableView, canInteractWithHeaderAtSection section: Int) -> Bool {
        return true
    }
}

