//
//  BaseVC.swift
//  Bakcell
//
//  Created by Shujat on 16/05/2017.
//  Copyright © 2017 evampsaanga. All rights reserved
//

import UIKit
import KYDrawerController
import FirebaseMessaging
import ObjectMapper

class BaseVC: UIViewController {
    
    //MARK:- Properties
    let activityIndicator : MBActivityIndicator = MBActivityIndicator()
    var isPopGestureEnabled : Bool = false
    
    //MARK:- @IBOutlet
    @IBOutlet var backButton    : UIButton?
    
    //MARK:- viewController Method
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Setting status bar color
        setStatusBarBackgroundColor(color: UIColor.init(red: 230.0/255.0, green: 40.0/255.0, blue: 71.0/255.0, alpha: 1.0))
        
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        
        /* // Enabling default back Gesture
         self.navigationController?.interactivePopGestureRecognizer?.delegate = self
         self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true */
        
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(navigateToSupplementaryOffers(notification:)), name: NSNotification.Name(Constants.KAppDeepLink), object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        // activityIndicator.hideActivityIndicator()
    }
    
    override open var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    override var prefersStatusBarHidden: Bool {
        return false
    }
    
    @objc func navigateToSupplementaryOffers(notification: NSNotification) {
        let supplementoryOfferVC = self.myStoryBoard.instantiateViewController(withIdentifier: "SupplementaryVC") as! SupplementaryVC
        if notification.userInfo?["offeringId"] != nil {
            if notification.userInfo?["offeringId"] as? String == "DATA" {
                supplementoryOfferVC.selectedTabType = MBOfferTabType.Internet
            } else if notification.userInfo?["offeringId"] as? String == "SMS" {
                supplementoryOfferVC.selectedTabType = MBOfferTabType.SMS
            } else if notification.userInfo?["offeringId"] as? String == "VOICE" {
                supplementoryOfferVC.selectedTabType = MBOfferTabType.Call
            } else if notification.userInfo?["offeringId"] as? String == "SPECIALS" {
                supplementoryOfferVC.selectedTabType = MBOfferTabType.Campaign
            } else if notification.userInfo?["offeringId"] as? String == "INCLUSIVE_RESOURCES" {
                supplementoryOfferVC.selectedTabType = MBOfferTabType.AllInclusive
            } else if notification.userInfo?["offeringId"] as? String == "ROAMING" {
                supplementoryOfferVC.selectedTabType = MBOfferTabType.Roaming
            }
        } else {
            if notification.userInfo?["offeringId"] as? String == "DATA" {
                supplementoryOfferVC.selectedTabType = MBOfferTabType.Internet
            } else if notification.userInfo?["offeringId"] as? String == "SMS" {
                supplementoryOfferVC.selectedTabType = MBOfferTabType.SMS
            } else if notification.userInfo?["offeringId"] as? String == "VOICE" {
                supplementoryOfferVC.selectedTabType = MBOfferTabType.Call
            } else if notification.userInfo?["offeringId"] as? String == "SPECIALS" {
                supplementoryOfferVC.selectedTabType = MBOfferTabType.Campaign
            } else if notification.userInfo?["offeringId"] as? String == "INCLUSIVE_RESOURCES" {
                supplementoryOfferVC.selectedTabType = MBOfferTabType.AllInclusive
            } else if notification.userInfo?["offeringId"] as? String == "ROAMING" {
                supplementoryOfferVC.selectedTabType = MBOfferTabType.Roaming
            }
        }
        self.navigationController?.pushViewController(supplementoryOfferVC, animated: true)
        print("notification.userInfo:\(notification.userInfo!)")
    }
    
    //MARK:- IBACTIONS
    @IBAction func backPressed(_ sender: AnyObject) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func sideMenuBackPressed(_ sender:AnyObject){
        //        tabBarController?.selectedIndex = 0
        self.navigationController?.popViewController(animated: true)
        
    }
    
    @IBAction func menuPressed(_ sender: AnyObject){
        if let drawerController = self.tabBarController?.parent as? KYDrawerController {
            drawerController.setDrawerState(.opened, animated: true)
        }
    }
    
    //MARK:- Functions
    
    /**
     Set color of status bar.
     
     - parameter color: Color which will be set to status bar.
     
     - returns: void
     */
    func setStatusBarBackgroundColor(color: UIColor) {
        let tag = 13254
        if let statusBar = UIApplication.shared.keyWindow?.viewWithTag(tag) {
            statusBar.backgroundColor = color
        } else {
            let statusBarView = UIView(frame: UIApplication.shared.statusBarFrame)
            statusBarView.tag = tag
            UIApplication.shared.keyWindow?.addSubview(statusBarView)
            statusBarView.backgroundColor = color
        }
    }
    
    
    /**
     Enable and disable next button accourding to isEnabled value.
     
     - parameter buttonView: view which have image, arrow and button.
     - parameter isEnabled: set ture for enabling and false for disabling.
     
     - returns: void
     */
    func setNextButtonSelection(buttonView: UIView, isEnabled: Bool) {
        
        // Loop through all subviews of buttonView
        buttonView.subviews.forEach { (aView) in
            
            // Check if subView is UIImageView
            if aView is UIImageView {
                
                // Convert view to UIImageView
                if let imageView = aView as? UIImageView {
                    
                    // Set UIImageView image
                    if isEnabled {
                        imageView.image = UIImage(named: "arrow_sign")
                    } else {
                        imageView.image = UIImage(named: "forward-arrow-disabled")
                    }
                }
            } else if aView is UILabel { // Check if subView is UILabel
                
                // Convert view to UILabel
                if let tiltLable = aView as? UILabel {
                    
                    // Change label textColor
                    if isEnabled {
                        tiltLable.textColor = UIColor.white
                    } else {
                        tiltLable.textColor = UIColor(hexString: "EC4B75")
                    }
                }
            } else if aView is UIButton { // Check if subView is UIButton
                
                // Convert view to UIButton and Enable and Disable
                if let aButton = aView as? UIButton {
                    aButton.isEnabled = isEnabled
                }
            }
        }
    }
    
    /**
     Enable and disable Back button accourding to isEnabled value.
     
     - parameter buttonView: view which have image, arrow and button.
     - parameter isEnabled: set ture for enabling and false for disabling.
     
     - returns: void
     */
    func setBackButtonSelection(buttonView: UIView, isEnabled: Bool) {
        
        // Loop through all subviews of buttonView
        buttonView.subviews.forEach { (aView) in
            
            // Check if subView is UIImageView
            if aView is UIImageView {
                
                // Convert view to UIImageView
                if let imageView = aView as? UIImageView {
                    
                    // Set UIImageView image
                    if isEnabled {
                        imageView.image = UIImage(named: "arrow_sign_back")
                    } else {
                        imageView.image = UIImage(named: "step-2-back-arrow-disabled")
                    }
                }
            } else if aView is UILabel { // Check if subView is UILabel
                
                // Convert view to UILabel
                if let tiltLable = aView as? UILabel {
                    
                    // Change label textColor
                    if isEnabled {
                        tiltLable.textColor = UIColor.white
                    } else {
                        tiltLable.textColor = UIColor(hexString: "EC4B75")
                    }
                }
                
            } else if aView is UIButton { // Check if subView is UIButton
                
                // Convert view to UIButton and Enable and Disable
                if let aButton = aView as? UIButton {
                    aButton.isEnabled = isEnabled
                }
            }
        }
    }
    
    /**
     Get UUID of Vendor and remove "_" from uuidString
     
     - returns: UUID string of current vendor
     */
    static func deviceID() -> String {
        if let UDID : String = UIDevice.current.identifierForVendor?.uuidString {
            return UDID.removeHyphen
        } else {
            return ""
        }
    }
    
    /* CRITERIA
     1. Characters less than or equal to 8
     2. Characters greater than 8
     3. Alphabets A-Z
     4. Alphabets a-z
     5. Digits 0-9
     6. Special Characters
     
     Weak Password:
     All combinations which are out of Normal and Strong.
     
     Normal Password:
     Combination 1, 3, 4, 5, 6
     Combination 2, 5, 6, 3 OR 4
     Combination 2, 3, 4, 5 OR 6
     
     Strong Password:
     Combination 2, 3, 4, 5, 6
     
     Note: Allowed min and max characters (6 - 15).
     Determine password compexcity accourding to predefied roles.
     
     - parameter text: string which need to process to find complexcity.
     
     - returns: Complexcity level of provided string.
     */
    
    func determinePasswordStrength(Text passwordString:String) -> Constants.MBPasswordStrength {
        
        if passwordString.length <= 0 || passwordString.length > 15 || passwordString.length < 6 {
            return Constants.MBPasswordStrength.didNotMatchCriteria
            
        }
            // Strong
        else if passwordString.length > 8 &&
            passwordString.length <= 15 &&
            passwordString.containsSpecialCharacters() &&
            passwordString.constainsSmallAlphabets() && passwordString.constainsCapitalAlphabets() &&
            passwordString.containsDigits() {
            
            return Constants.MBPasswordStrength.Strong
            
        }
            // Medium
        else if passwordString.length >= 6 &&
            passwordString.length <= 8 &&
            passwordString.containsSpecialCharacters() &&
            passwordString.constainsCapitalAlphabets() && passwordString.constainsSmallAlphabets() &&
            passwordString.containsDigits() {
            
            return Constants.MBPasswordStrength.Medium
            
        } else if passwordString.length >= 8 &&
            (passwordString.containsSpecialCharacters() && passwordString.containsAlphbeats() && passwordString.containsDigits()) {
            
            return Constants.MBPasswordStrength.Medium
            
        }
        else if passwordString.length >= 8 && (passwordString.containsDigits() && passwordString.constainsSmallAlphabets() && passwordString.constainsCapitalAlphabets()) || (passwordString.constainsSmallAlphabets() && passwordString.constainsCapitalAlphabets() && passwordString.containsSpecialCharacters()){
            
            return Constants.MBPasswordStrength.Medium
            
        }
            // Weak
        else if passwordString.length >= 6 &&
            passwordString.length < 8 &&
            (passwordString.containsAlphbeats() || passwordString.containsDigits() || passwordString.containsSpecialCharacters()) {
            
            return Constants.MBPasswordStrength.Week
            
        } else if passwordString.length >= 6 {
            
            return Constants.MBPasswordStrength.Week
            
        } else {
            
            return Constants.MBPasswordStrength.didNotMatchCriteria
            
        }
    }
    
    
    //MARK: - API Calls
    
    
    
    
    //MARK: Get survey====  put this service call somewhere else where needed
    
    func getSurveys() {
        
        activityIndicator.showActivityIndicator()
        _ = MBAPIClient.sharedClient.getSurveys({ (response, resultData, error, isCancelled, status, resultCode, resultDesc) in
            
            self.activityIndicator.hideActivityIndicator()
            if error != nil {
                error?.showServerErrorInViewController(self)
                
            } else {
                // handling data from API response.
                if resultCode == Constants.MBAPIStatusCode.succes.rawValue ,
                    let homePageResponse = Mapper<InAppSurveyMapper>().map(JSONObject:resultData) {
                    MBUserSession.shared.appSurvays = homePageResponse
                } else {
                    //EventLog
                    MBAnalyticsManager.logEvent(screenName: "Login Screen", contentType:"User failed to Login" , status:"Failed" )
                    // Show error alert to user
                    self.showErrorAlertWithMessage(message: resultDesc)
                }
            }
        })
    }
    
    
    
    
    
    /**
     Clear user information from userDefaults and pop user to login screen.
     
     - returns: void
     */
    static func logout() {
        
        // try to get rootView controller and pop accordingly
        if let navigationController  = UIApplication.shared.delegate?.window??.rootViewController as? MainNavigationController {
            
            // Clear information in userDefaults
            MBUserSession.shared.clearUserSession()
            navigationController.popToLoginViewController()
        } else {
            UIApplication.shared.delegate?.window??.rootViewController?.navigationController?.popToRootViewController(animated: true)
        }
    }
    
    
    
    /**
     Redirect user to notification screen if he tab on notification.
     
     - returns: void
     */
    class func redirectUserToNotificationScreen() {
        
        // check user if logged in
        if MBUserSession.shared.isLoggedIn() {
            
            // search for Notifications screen from naviagetion
            if let navigationController  = UIApplication.shared.delegate?.window??.rootViewController as? MainNavigationController {
                
                var isFoundNotificationController = false
                
                navigationController.viewControllers.reversed().forEach({ (aViewController) in
                    
                    if aViewController.isKind(of: NotificationsVC().classForCoder) {
                        
                        isFoundNotificationController = true
                        navigationController.popToViewController(aViewController, animated: true)
                    }
                })
                
                // check if Notification screen found in main navigation
                if isFoundNotificationController != true {
                    
                    // Search for DashBoard from current navigation
                    var isFoundController = false
                    navigationController.viewControllers.forEach({ (aViewController) in
                        
                        if aViewController.isKind(of: KYDrawerController(drawerDirection: .left, drawerWidth: (UIScreen.main.bounds.width * 0.85)).classForCoder) {
                            isFoundController = true
                            
                        }
                    })
                    
                    // push notifiaction screen on KYDrawerController controller if found
                    if isFoundController {
                        
                        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                        if let notifications = mainStoryboard.instantiateViewController(withIdentifier: "NotificationsVC") as? NotificationsVC {
                            navigationController.pushViewController(notifications, animated: true)
                        }
                        
                    } else {
                        // Enable check to redirect user to Notification screen
                        MBUserSession.shared.isPushNotificationSelected = true
                    }
                }
            }
        } else {
            // Enable check to redirect user to Notification screen
            MBUserSession.shared.isPushNotificationSelected = true
        }
    }
    
    
   class func redirectUserToDeepLinkScreen(urlString:String) {
       
       // check user if logged in
    if MBUserSession.shared.isLoggedIn() {
        
        // search for Notifications screen from naviagetion
        if let navigationController  = UIApplication.shared.delegate?.window??.rootViewController as? MainNavigationController {
            
            var isFoundNotificationController = false
            
            navigationController.viewControllers.reversed().forEach({ (aViewController) in
                
                if aViewController.isKind(of: SupplementaryVC().classForCoder) {
                    
                    isFoundNotificationController = true
                    navigationController.popToViewController(aViewController, animated: true)
                }
            })
               
               // check if Notification screen found in main navigation
               if isFoundNotificationController != true {
                   
                   // Search for DashBoard from current navigation
                   var isFoundController = false
                   navigationController.viewControllers.forEach({ (aViewController) in
                       
                       if aViewController.isKind(of: KYDrawerController(drawerDirection: .left, drawerWidth: (UIScreen.main.bounds.width * 0.85)).classForCoder) {
                           isFoundController = true
                           
                       }
                   })
                   
                   // push notifiaction screen on KYDrawerController controller if found
                   if isFoundController {
                       
                       let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                       if let supplementoryOfferVC = mainStoryboard.instantiateViewController(withIdentifier: "SupplementaryVC") as? SupplementaryVC {
                        if urlString.containsSubString(subString: "SCREEN_NAME") {
                            supplementoryOfferVC.selectedTabType = MBOfferTabType.Internet
                        } else if urlString.containsSubString(subString: "OFFERING_ID") {
                            supplementoryOfferVC.offeringIdFromNotification = urlString.components(separatedBy: "/").last ?? ""
                            supplementoryOfferVC.redirectedFromNotification = true
                            /*if urlString.containsSubString(subString: "DATA") {
                                supplementoryOfferVC.selectedTabType = MBOfferTabType.Internet
                                supplementoryOfferVC.offeringIdFromNotification = urlString.components(separatedBy: "/").last ?? ""
                            } else if urlString.containsSubString(subString: "SMS") {
                                supplementoryOfferVC.selectedTabType = MBOfferTabType.SMS
                                supplementoryOfferVC.offeringIdFromNotification = urlString.components(separatedBy: "/").last ?? ""
                            } else if urlString.containsSubString(subString: "VOICE") {
                                supplementoryOfferVC.selectedTabType = MBOfferTabType.Call
                                supplementoryOfferVC.offeringIdFromNotification = urlString.components(separatedBy: "/").last ?? ""
                            } else if urlString.containsSubString(subString: "SPECIALS") {
                                supplementoryOfferVC.selectedTabType = MBOfferTabType.TM
                                supplementoryOfferVC.offeringIdFromNotification = urlString.components(separatedBy: "/").last ?? ""
                            } else if urlString.containsSubString(subString: "INCLUSIVE_RESOURCES   ") {
                                supplementoryOfferVC.selectedTabType = MBOfferTabType.AllInclusive
                                supplementoryOfferVC.offeringIdFromNotification = urlString.components(separatedBy: "/").last ?? ""
                            } else if urlString.containsSubString(subString: "ROAMING") {
                                supplementoryOfferVC.selectedTabType = MBOfferTabType.Roaming
                                supplementoryOfferVC.offeringIdFromNotification = urlString.components(separatedBy: "/").last ?? ""
                            } else {
                                supplementoryOfferVC.selectedTabType = MBOfferTabType.Internet
                            }*/
                            supplementoryOfferVC.redirectedFromNotification = true
                        } else if urlString.containsSubString(subString: "TAB_NAME") {
                            if urlString.containsSubString(subString: "DATA") {
                                supplementoryOfferVC.selectedTabType = MBOfferTabType.Internet
                            } else if urlString.containsSubString(subString: "SMS") {
                                supplementoryOfferVC.selectedTabType = MBOfferTabType.SMS
                            } else if urlString.containsSubString(subString: "VOICE") {
                                supplementoryOfferVC.selectedTabType = MBOfferTabType.Call
                            } else if urlString.containsSubString(subString: "SPECIALS") {
                                supplementoryOfferVC.selectedTabType = MBOfferTabType.TM
                            } else if urlString.containsSubString(subString: "INCLUSIVE_RESOURCES   ") {
                                supplementoryOfferVC.selectedTabType = MBOfferTabType.AllInclusive
                            } else if urlString.containsSubString(subString: "ROAMING") {
                                supplementoryOfferVC.selectedTabType = MBOfferTabType.Roaming
                            } else {
                                supplementoryOfferVC.selectedTabType = MBOfferTabType.Internet
                            }
                        } else {
                            
                        }
                           navigationController.pushViewController(supplementoryOfferVC, animated: true)
                       }
                       
                   } else {
                       // Enable check to redirect user to Notification screen
                       MBUserSession.shared.isPushNotificationSelected = true
                   }
               }
           }
       } else {
           // Enable check to redirect user to Notification screen
           MBUserSession.shared.isPushNotificationSelected = true
       }
   }
    
    
    
    
    
    
    // functions to show in-app Surveys
    
    func showSimpleSurvey(survey: InAppSurvey, surveryScreen:SurveyScreenName) {
        if let inAppSurveyVC : InAppSurveyVC = InAppSurveyVC.fromNib() {
            inAppSurveyVC.showTopUpStackView = false
            inAppSurveyVC.currentSurvay = survey
            inAppSurveyVC.surveyScreen = SurveyScreenName.dashboard.rawValue
            self.presentPOPUP(inAppSurveyVC, animated: true, completion: nil)
        }
    }
    
    /*func showTopupSurvey(survey: InAppSurvey) {
        if let inAppSurveyVC : InAppSurveyVC = InAppSurveyVC.fromNib() {
            inAppSurveyVC.currentSurvay = survey
            inAppSurveyVC.showTopUpStackView = true
            self.presentPOPUP(inAppSurveyVC, animated: true, completion: nil)
        }
    }*/
    
}


//MARK:- KYDrawerController extension for status color
extension KYDrawerController {
    override open var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
}

extension BaseVC: UIGestureRecognizerDelegate {
    
    func interactivePop(_ isEnable:Bool = true) {
        self.isPopGestureEnabled = isEnable
        /*if let navigationVC = self.navigationController as? MainNavigationController {
         navigationVC.interactivePop(isEnable)
         }*/
    }
}


extension BaseVC {
    
    
    
    /// Call 'getSupplementaryOffering' API .
    ///
    /// --- 
    @objc func getSupplementaryOfferingsMain() {
        
        _ = MBAPIClient.sharedClient.getSupplementaryOfferings({ (response, resultData, error, isCancelled, status, resultCode, resultDesc) in
            
            self.activityIndicator.hideActivityIndicator()
            
            if error != nil {
                
            } else {
                // handling data from API response.
                if resultCode == Constants.MBAPIStatusCode.succes.rawValue {
                    
                    // Parssing response data
                    if let supplementaryOffersHandler = Mapper<SupplementaryOfferings>().map(JSONObject: resultData){
                        
                        // Saving Supplementary Offers data in user defaults
                        supplementaryOffersHandler.saveInUserDefaults(key: APIsType.supplementaryOffer.selectedLocalizedAPIKey())
                    }
                }
            }
        })
    }
}
