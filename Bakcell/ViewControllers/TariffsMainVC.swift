//
//  TariffsMainVC.swift
//  Bakcell
//
//  Created by Saad Riaz on 7/17/17.
//  Copyright © 2017 evampsaanga. All rights reserved.
//

import UIKit
import ObjectMapper

enum MBTariffScreenType : String {
    case Klass = "Klass"
    case CIN = "CIN"
    case Individual = "Individual"
    case Corporate = "Corporate"
    case KlassPostpaid = "KlassPostpaid"
    case UnSpecified = "UnSpecified"
}

class TariffsMainVC: BaseVC {
    
    var sectionOpen = 0
    var tariffResponse : TariffDetails?
    var selectedType : MBTariffScreenType!
    var redirectedFromNotification : Bool = false

    var offeringIdFromNotification : String = ""
    var isPaid : Bool = false
    var redirectedFromHome : Bool = false

    var currentMSISDNTariffInfo     : String?
    var currentLanguageTariffInfo   : Constants.MBLanguage?
    
    @IBOutlet var buttonViewHeight: NSLayoutConstraint!
    @IBOutlet var back_btn: UIButton!
    @IBOutlet var drawerMenu_btn: UIButton!
    @IBOutlet var tariffTitle_lbl: UILabel!
    @IBOutlet var cin_btn: UIButton!
    @IBOutlet var klass_btn: UIButton!
    @IBOutlet var mainView: UIView!

    //MARK: - ViewController Functions
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.interactivePop(true)
        
        // setting localized strings
        loadViewLayout()

        if redirectedFromNotification {
            drawerMenu_btn.isHidden = true
            back_btn.isHidden = false
        } else {
            drawerMenu_btn.isHidden = false
            back_btn.isHidden = true
        }
        /* Clear user tariff info from current objects if he switch to any other number */
        if (currentMSISDNTariffInfo?.isBlank == true ||
            (currentMSISDNTariffInfo?.isEqualToStringIgnoreCase(otherString: MBUserSession.shared.msisdn) ?? false) == false) ||
            (currentLanguageTariffInfo != MBLanguageManager.userSelectedLanguage()){
            
            tariffResponse = nil
            
            
            currentMSISDNTariffInfo = MBUserSession.shared.msisdn
            currentLanguageTariffInfo = MBLanguageManager.userSelectedLanguage()
        }
        
        self.layoutViewControllerAccourdingToUserType()

    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }

    // MARK: - IBACTIONS
    
    @IBAction func klassPressed(_ sender: Any) {
        cin_btn.backgroundColor = UIColor .white
        klass_btn.backgroundColor = UIColor.MBButtonBackgroundGrayColor
        
        klass_btn .setTitleColor(UIColor .black, for: UIControl.State.normal)
        cin_btn .setTitleColor(UIColor .MBDarkGrayColor, for: UIControl.State.normal)

        selectedType = MBTariffScreenType.Klass
        self.loadSelectedType()
        
    }
    @IBAction func cinPressed(_ sender: Any) {
        cin_btn.backgroundColor = UIColor.MBButtonBackgroundGrayColor
        klass_btn.backgroundColor = UIColor.white
        
        klass_btn .setTitleColor(UIColor .MBDarkGrayColor, for: UIControl.State.normal)
        cin_btn .setTitleColor(UIColor .black, for: UIControl.State.normal)
        selectedType = MBTariffScreenType.CIN

        self.loadSelectedType()

    }
    
    //MARK: - FUNCTIONS
    func loadViewLayout(){
        klass_btn.titleLabel?.font = UIFont.MBArial(fontSize: 16)
        cin_btn.titleLabel?.font = UIFont.MBArial(fontSize: 16)
        tariffTitle_lbl.font = UIFont.MBArial(fontSize: 20)
        tariffTitle_lbl.text = Localized("Title_Tariffs")
        klass_btn.setTitle(Localized("BtnTitle_Klass"), for: UIControl.State.normal)
        cin_btn.setTitle(Localized("BtnTitle_Cin"), for: UIControl.State.normal)
    }

    func layoutViewControllerAccourdingToUserType() {

        // Clear User information
        // tariffResponse = nil

        if MBUserSession.shared.subscriberType() == MBSubscriberType.PrePaid {

            buttonViewHeight.constant = 50
            if MBUserSession.shared.brandName() == MBBrandName.CIN {
                //self.cinPressed(cin_btn)
                self.klassPressed(klass_btn ?? UIButton())
            } else {
                self.klassPressed(klass_btn ?? UIButton())
            }
            //            self.cinPressed(cin_btn)

            cin_btn.layer.borderColor = UIColor.MBBorderGrayColor.cgColor
            klass_btn.layer.borderColor = UIColor.MBBorderGrayColor.cgColor

        } else {

            buttonViewHeight.constant = 0
            cin_btn.isHidden = true
            klass_btn.isHidden = true

            if (MBUserSession.shared.brandName() == MBBrandName.Klass && MBUserSession.shared.subscriberType() == .PostPaid) {

                selectedType = MBTariffScreenType.KlassPostpaid

            } else if MBUserSession.shared.userCustomerType() == MBCustomerType.Corporate {

                selectedType = MBTariffScreenType.Corporate

            } else {
                selectedType = MBTariffScreenType.Individual
            }

            self.loadSelectedType()
        }

    }

    func typeAndIndexOfOfferingId(offeringId : String?, tariffData : TariffDetails?) -> (MBTariffScreenType, Int) {

        // check if found nil information
        if tariffData == nil {

            /* check if subscriberType is prepaid */
            if MBUserSession.shared.subscriberType() == MBSubscriberType.PrePaid {
                return (.Klass, 0)
            } else {

                /* check if customerType is corporate */
                if MBUserSession.shared.userCustomerType() == MBCustomerType.Corporate {
                    return (.Corporate, 0)

                } else if MBUserSession.shared.brandName() == MBBrandName.Klass {
                    return (.KlassPostpaid, 0)

                } else  {
                    return (.Individual, 0)

                }
            }

        } else {

            /* check if subscriberType is prepaid */
            if MBUserSession.shared.subscriberType() == MBSubscriberType.PrePaid {

                /* Filter offers of CIN section */ // 1
                var filterResponse = containsOfferInCINByOfferingId(offeringId: offeringIdFromNotification, offerArray: tariffResponse?.prepaid?.cinTarrifs)

                if filterResponse.isFoundValue {
                    return (.CIN, filterResponse.index)
                }

                /* Filter offers of Klass section */ // 2
                filterResponse = containsOfferInKlassByOfferingId(offeringId: offeringIdFromNotification, offerArray: tariffResponse?.prepaid?.klass)

                if filterResponse.isFoundValue {
                    return (.Klass, filterResponse.index)
                } else {
                    return (.Klass, 0)
                }

            } else {
                /* User Type is postpaid */

                /* check if customerType is corporate */
                if MBUserSession.shared.userCustomerType() == MBCustomerType.Corporate {

                    /* Filter offers of Corporate section */
                    let filterResponse = containsOfferInCorporateByOfferingId(offeringId: offeringIdFromNotification, offerArray: tariffResponse?.postpaid?.corporate)

                    if filterResponse.isFoundValue {
                        return (.Corporate, filterResponse.index)
                    } else {
                        return (.Corporate, 0)
                    }

                } else if MBUserSession.shared.brandName() == MBBrandName.Klass {

                    /* customerType is KlassPostPaid */

                    /* Filter offers of KlassPostPaid section */
                    let filterResponse = containsOfferInKlassPostpaidByOfferingId(offeringId: offeringIdFromNotification, offerArray: tariffResponse?.postpaid?.klassPostpaid)

                    if filterResponse.isFoundValue {
                        return (.KlassPostpaid, filterResponse.index)
                    } else {
                        return (.KlassPostpaid, 0)
                    }
                } else {
                    /* customerType is Individual */

                    /* Filter offers of individual section */
                    let filterResponse = containsOfferInIndividualByOfferingId(offeringId: offeringIdFromNotification, offerArray: tariffResponse?.postpaid?.individual)

                    if filterResponse.isFoundValue {
                        return (.Individual, filterResponse.index)
                    } else {
                        return (.Individual, 0)
                    }
                }
            }
        }
    }

    /* Filter offer id from Cin*/
    func containsOfferInCINByOfferingId(offeringId : String? , offerArray : [NewCinTarrif]? ) -> (isFoundValue : Bool, index : Int) {

        var foundOffer = false

        // return 0 if offeringId is blank
        if offeringId?.isBlank != false {
            return (foundOffer, 0)
        }
        // return 0 if offerArray is nil
        if offerArray == nil {
            return (foundOffer, 0)
        }

        // Filtering offeringId index from array
        let indexOfOffer = offerArray?.firstIndex() {
            
            var aOfferingId : String = ""
            if $0.tariffType == .newCin,
                let aNewCINObject = $0.valueObject as? Klass {
                
                aOfferingId = aNewCINObject.header?.offeringId ?? ""
            
            } else if $0.tariffType == .cin,
                let aCINObject = $0.valueObject as? Cin {
                
                aOfferingId = aCINObject.header?.offeringId ?? ""
            }
            
            if aOfferingId.isEqualToStringIgnoreCase(otherString: offeringId) {
                foundOffer = true
                return true
            } else {
                return false
            }
        }

        return (foundOffer, (indexOfOffer ?? 0))
    }

    /* Filter offer id from Klass*/
    func containsOfferInKlassByOfferingId(offeringId : String? , offerArray : [Klass]? ) -> (isFoundValue : Bool, index : Int) {

        var foundOffer = false

        // return 0 if offeringId is blank
        if offeringId?.isBlank != false {
            return (foundOffer, 0)
        }
        // return 0 if offerArray is nil
        if offerArray == nil {
            return (foundOffer, 0)
        }

        // Filtering offeringId index from array
        let indexOfOffer = offerArray?.firstIndex() {

            if let aOfferingId = ($0 as Klass).header?.offeringId {

                let isFound = aOfferingId.isEqualToStringIgnoreCase(otherString: offeringId)

                if isFound {
                    foundOffer = true
                    return true
                } else {
                    return false
                }

            } else {
                return false
            }
        }

        return (foundOffer, (indexOfOffer ?? 0))
    }

    /* Filter offer id from Corporate*/
    func containsOfferInCorporateByOfferingId(offeringId : String? , offerArray : [Corporate]? ) -> (isFoundValue : Bool, index : Int) {

        var foundOffer = false

        // return 0 if offeringId is blank
        if offeringId?.isBlank != false {
            return (foundOffer, 0)
        }
        // return 0 if offerArray is nil
        if offerArray == nil {
            return (foundOffer, 0)
        }

        // Filtering offeringId index from array
        let indexOfOffer = offerArray?.firstIndex() {

            if let aOfferingId = ($0 as Corporate).header?.offeringId {

                let isFound = aOfferingId.isEqualToStringIgnoreCase(otherString: offeringId)

                if isFound {
                    foundOffer = true
                    return true
                } else {
                    return false
                }

            } else {
                return false
            }
        }

        return (foundOffer, (indexOfOffer ?? 0))
    }

    /* Filter offer id from Individual*/
    func containsOfferInIndividualByOfferingId(offeringId : String? , offerArray : [Individual]? ) -> (isFoundValue : Bool, index : Int) {

        var foundOffer = false

        // return 0 if offeringId is blank
        if offeringId?.isBlank != false {
            return (foundOffer, 0)
        }
        // return 0 if offerArray is nil
        if offerArray == nil {
            return (foundOffer, 0)
        }

        // Filtering offeringId index from array
        let indexOfOffer = offerArray?.firstIndex() {

            if let aOfferingId = ($0 as Individual).header?.offeringId {

                let isFound = aOfferingId.isEqualToStringIgnoreCase(otherString: offeringId)

                if isFound {
                    foundOffer = true
                    return true
                } else {
                    return false
                }

            } else {
                return false
            }
        }

        return (foundOffer, (indexOfOffer ?? 0))
    }


    /* Filter offer id from KlassPostpaid*/
    func containsOfferInKlassPostpaidByOfferingId(offeringId : String? , offerArray : [KlassPostpaid]? ) -> (isFoundValue : Bool, index : Int) {

        var foundOffer = false

        // return 0 if offeringId is blank
        if offeringId?.isBlank != false {
            return (foundOffer, 0)
        }
        // return 0 if offerArray is nil
        if offerArray == nil {
            return (foundOffer, 0)
        }

        // Filtering offeringId index from array
        let indexOfOffer = offerArray?.firstIndex() {

            if let aOfferingId = ($0 as KlassPostpaid).header?.offeringId {

                let isFound = aOfferingId.isEqualToStringIgnoreCase(otherString: offeringId)

                if isFound {
                    foundOffer = true
                    return true
                } else {
                    return false
                }

            } else {
                return false
            }
        }

        return (foundOffer, (indexOfOffer ?? 0))
    }

    //MARK: - API Calls

    /// Call 'getTariffDetails' API .
    ///
    /// - returns: Void
    func loadTariffDetails(subscriberType:String?, customerType:String?, subscribedOfferingName:String?,offeringID: String?, storeId:String?) {

        /*Loading initial data from UserDefaults*/
        if Connectivity.isConnectedToInternet == false ,
            let tariffPrepaid :TariffDetails = TariffDetails.loadFromUserDefaults(key: "\(APIsType.tariffDetails.selectedLocalizedAPIKey())_\(MBUserSession.shared.userCustomerType())") {
            
            self.tariffResponse = tariffPrepaid
            self.loadSelectedType()
            
        } else {
            self.getTariffDetails(subscriberType: subscriberType, customerType: customerType, subscribedOfferingName: subscribedOfferingName, offeringID: offeringID, storeId: storeId)
        }
    }

    func getTariffDetails(subscriberType:String?, customerType:String?, subscribedOfferingName:String?, offeringID: String?, storeId:String?) {

        activityIndicator.showActivityIndicator()

        _ = MBAPIClient.sharedClient.getTariffDetails(SubscriberType: subscriberType ?? "",offeringId: offeringID ?? "", CustomerType: customerType ?? "", SubscribedOfferingName: subscribedOfferingName ?? "", StoreId: storeId ?? "",{ (response, resultData, error, isCancelled, status, resultCode, resultDesc) in

            self.activityIndicator.hideActivityIndicator()
            if error != nil {
                error?.showServerErrorInViewController(self)

            } else {
                // handling data from API response.
                if resultCode == Constants.MBAPIStatusCode.succes.rawValue {

                    // Parssing response data
                    if let tariffPrepaid = Mapper<TariffDetails>().map(JSONObject: resultData){

                        self.tariffResponse = tariffPrepaid
                        self.loadSelectedType()

                        /*Save data into user defaults*/
                        tariffPrepaid.saveInUserDefaults(key: "\(APIsType.tariffDetails.selectedLocalizedAPIKey())_\(MBUserSession.shared.userCustomerType())")

                    }
                } else {
                    // Show error alert to user
                    self.showErrorAlertWithMessage(message: resultDesc)
                }
            }
        })
    }

    func loadSelectedType() {

        // If data found nil
        if tariffResponse == nil {

            // FIXME:  Hardcoded StoreId (allways be fix accourding to discustion with SA)
            loadTariffDetails(subscriberType: MBUserSession.shared.subscriberTypeLowerCaseStr , customerType:  MBUserSession.shared.userInfo?.customerType ?? "", subscribedOfferingName:  MBUserSession.shared.userInfo?.offeringName ?? "",offeringID:  MBUserSession.shared.userInfo?.offeringId ?? "", storeId: "1")
            return

        }

        if redirectedFromNotification || redirectedFromHome {

            let (type, index) = typeAndIndexOfOfferingId(offeringId: offeringIdFromNotification, tariffData: self.tariffResponse)

            self.selectedType = type
            //
            if type == .CIN {

                loadViewControllerForCIN(scrollToIndex: index)

            } else if type == .Klass {

                loadViewControllerForKlass(scrollToIndex: index, isPaid: self.isPaid, isRedirectedFromHome: redirectedFromHome)

            } else if type == .Individual {
                loadViewControllerForIndividual(scrollToIndex: index)
                
            } else if type == .Corporate {
                loadViewControllerForCorporate(scrollToIndex: index)

            }else if selectedType == MBTariffScreenType.KlassPostpaid {

                loadViewControllerForKlassPastpaid(scrollToIndex: index)
            }

            // RESET CHECK
            redirectedFromNotification = false
            redirectedFromHome = false

        } else {

            // If data found then load selceted information
            if selectedType == MBTariffScreenType.CIN {

                loadViewControllerForCIN(scrollToIndex: 0)

            } else  if selectedType == MBTariffScreenType.Individual {

                loadViewControllerForIndividual(scrollToIndex: 0)



            } else if selectedType == MBTariffScreenType.Klass {
                loadViewControllerForKlass(scrollToIndex: 0)

            } else if selectedType == MBTariffScreenType.Corporate {

                loadViewControllerForCorporate(scrollToIndex: 0)
            } else if selectedType == MBTariffScreenType.KlassPostpaid {

                loadViewControllerForKlassPastpaid(scrollToIndex: 0)
            }

        }

    }

    func loadViewControllerForCIN(scrollToIndex : Int) {

        // Setting selected layout
        cin_btn.backgroundColor = UIColor .lightGray
        klass_btn.backgroundColor = UIColor.white

        klass_btn .setTitleColor(UIColor .MBDarkGrayColor, for: UIControl.State.normal)
        cin_btn .setTitleColor(UIColor .black, for: UIControl.State.normal)


        let tarrif = self.myStoryBoard.instantiateViewController(withIdentifier: "TariffsVC")as! TariffsVC
        tarrif.cinData = self.tariffResponse?.prepaid?.cinTarrifs
        tarrif.selectedType = self.selectedType
        tarrif.scrollToIndex = scrollToIndex

        self.addChild(tarrif)
        mainView.addSubview(tarrif.view)
        tarrif.didMove(toParent: self)

        tarrif.view.snp.makeConstraints { make in
            make.top.equalTo(mainView.snp.top).offset(0.0)
            make.right.equalTo(mainView.snp.right).offset(0.0)
            make.left.equalTo(mainView.snp.left).offset(0.0)
            make.bottom.equalTo(mainView.snp.bottom).offset(0.0)
        }
    }

    func loadViewControllerForKlass(scrollToIndex : Int, isPaid: Bool = false, isRedirectedFromHome: Bool = false) {

        // Setting selected layout
        cin_btn.backgroundColor = UIColor .white
        klass_btn.backgroundColor = UIColor.lightGray

        klass_btn .setTitleColor(UIColor .black, for: UIControl.State.normal)
        cin_btn .setTitleColor(UIColor .MBDarkGrayColor, for: UIControl.State.normal)


        let tariffKlass = self.myStoryBoard.instantiateViewController(withIdentifier:"TariffKlassVC") as! TariffKlassVC
        tariffKlass.klassData = tariffResponse?.prepaid?.klass
        tariffKlass.selectedScreenType = self.selectedType
        tariffKlass.scrollToIndex = scrollToIndex
        tariffKlass.isPaid = isPaid
        tariffKlass.isRedirectedFromHome = isRedirectedFromHome

        self.addChild(tariffKlass)
        mainView.addSubview(tariffKlass.view)
        tariffKlass.didMove(toParent: self)

        tariffKlass.view.snp.makeConstraints { make in
            make.top.equalTo(mainView.snp.top).offset(0.0)
            make.right.equalTo(mainView.snp.right).offset(0.0)
            make.left.equalTo(mainView.snp.left).offset(0.0)
            make.bottom.equalTo(mainView.snp.bottom).offset(0.0)
        }

    }

    func loadViewControllerForKlassPastpaid(scrollToIndex : Int) {

        let tarrif = self.myStoryBoard.instantiateViewController(withIdentifier: "TariffsVC")as! TariffsVC
        tarrif.klassPostpaidData = self.tariffResponse?.postpaid?.klassPostpaid
        tarrif.selectedType = self.selectedType
        tarrif.scrollToIndex = scrollToIndex

        self.addChild(tarrif)
        mainView.addSubview(tarrif.view)
        tarrif.didMove(toParent: self)

        tarrif.view.snp.makeConstraints { make in
            make.top.equalTo(mainView.snp.top).offset(0.0)
            make.right.equalTo(mainView.snp.right).offset(0.0)
            make.left.equalTo(mainView.snp.left).offset(0.0)
            make.bottom.equalTo(mainView.snp.bottom).offset(0.0)
        }
    }

    func loadViewControllerForIndividual(scrollToIndex : Int) {

        let tarrif = self.myStoryBoard.instantiateViewController(withIdentifier: "TariffsVC")as! TariffsVC
        tarrif.individualData = self.tariffResponse?.postpaid?.individual
        tarrif.selectedType = self.selectedType
        tarrif.scrollToIndex = scrollToIndex

        self.addChild(tarrif)
        mainView.addSubview(tarrif.view)
        tarrif.didMove(toParent: self)

        tarrif.view.snp.makeConstraints { make in
            make.top.equalTo(mainView.snp.top).offset(0.0)
            make.right.equalTo(mainView.snp.right).offset(0.0)
            make.left.equalTo(mainView.snp.left).offset(0.0)
            make.bottom.equalTo(mainView.snp.bottom).offset(0.0)
        }
    }

    func loadViewControllerForCorporate(scrollToIndex : Int) {

        let tariffCorporate = self.myStoryBoard.instantiateViewController(withIdentifier:"TariffKlassVC") as! TariffKlassVC
        tariffCorporate.corporateData = tariffResponse?.postpaid?.corporate
        tariffCorporate.selectedScreenType = self.selectedType
        tariffCorporate.scrollToIndex = scrollToIndex

        self.addChild(tariffCorporate)
        mainView.addSubview(tariffCorporate.view)
        tariffCorporate.didMove(toParent: self)

        tariffCorporate.view.snp.makeConstraints { make in
            make.top.equalTo(mainView.snp.top).offset(0.0)
            make.right.equalTo(mainView.snp.right).offset(0.0)
            make.left.equalTo(mainView.snp.left).offset(0.0)
            make.bottom.equalTo(mainView.snp.bottom).offset(0.0)
        }
    }
}
