//
//  Terms&ConditionsVC.swift
//  Bakcell
//
//  Created by Saad Riaz on 6/14/17.
//  Copyright © 2017 evampsaanga. All rights reserved.
//

import UIKit

class Terms_ConditionsVC: BaseVC {

    // MARK: - Properties
    var isPopUp : Bool = false

    // MARK: - IBOutlet
    @IBOutlet var ok_btn: UIButton!
    @IBOutlet var terms_textView: UITextView!
    @IBOutlet var terms_title: UILabel!
    @IBOutlet var termsConditionPopUpView: UIView!
    @IBOutlet var termsConditionView: UIView!
    @IBOutlet var terms_title1: UILabel!
    @IBOutlet var terms_textView1: UITextView!
    @IBOutlet var mainView: UIView!

    // MARK: - View Controller Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        mainView.layer.borderColor = UIColor.MBBorderGrayColor.cgColor

        if isPopUp {
            termsConditionPopUpView.isHidden = false
            termsConditionView.isHidden = true
        } else {
            termsConditionView.isHidden = false
            termsConditionPopUpView.isHidden = true
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.interactivePop(true)
        
        loadViewLayout()
        
        //EventLog
        MBAnalyticsManager.logEvent(screenName: "Terms And Conditions Screen", contentType:"Terms And Conditions Screen" , status:"Terms And Conditions Screen" )
    }

    //MARK: - IBACTIONS
    @IBAction func okPressed(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    //MARK: - FUNCTIONS
    func loadViewLayout(){
        terms_title.text = Localized("Terms_Title")
        terms_textView.text = Localized("Terms_Description")
        terms_title1.text = Localized("Terms_Title")
        terms_textView1.text = Localized("Terms_Description")
        ok_btn.setTitle(Localized("BtnTitle_OK"), for: UIControl.State.normal)
    }

}
