//
//  StoreLocatorHandler.swift
//  Bakcell
//
//  Created by Saad Riaz on 10/3/17.
//  Copyright © 2017 evampsaanga. All rights reserved.
//

import Foundation
import ObjectMapper

class StoreLocatorHandler : Mappable, Copyable{

    var city : [String]?
    var stores : [Stores]?
    var type : [String]?

    init() {
    }
    required init?(map: Map) {
    }

    required init(instance: StoreLocatorHandler) {
        self.stores = instance.stores

    }

    func mapping(map: Map) {

        city            <- map["city"]
        stores          <- map["stores"]
        type            <- map["type"]
    }
}

class Stores: Mappable, Copyable {

    var address : String = ""
    var city : String = ""
    var contactNumbers : [String]?
    var id : String = ""
    var latitude : String = ""
    var longitude : String = ""
    var store_name : String = ""
    var timing : [Timings]?
    var type : String = ""
    var distance : Double = 0.0

    init() {

    }
    required init?(map: Map) {
    }

    required init(instance: Stores) {
        self.address = instance.address
        self.city = instance.city
        self.contactNumbers = instance.contactNumbers
        self.id = instance.id
        self.latitude = instance.latitude
        self.longitude = instance.longitude
        self.store_name = instance.store_name
        self.timing = instance.timing
        self.type = instance.type
        self.distance = instance.distance
    }

    func mapping(map: Map) {

        address                     <- map["address"]
        city                        <- map["city"]
        contactNumbers              <- map["contactNumbers"]
        id                          <- map["id"]
        latitude                    <- map["latitude"]
        longitude                   <- map["longitude"]
        store_name                  <- map["store_name"]
        timing                      <- map["timing"]
        type                        <- map["type"]
        distance                    <- map["distance"]

    }
}

class Timings : Mappable{

    var day : String = ""
    var timings : String = ""

    required init?(map: Map) {
    }

    func mapping(map: Map) {

        day                  <- map["day"]
        timings              <- map["timings"]
    }
}

class Values : Mappable{

    var key : String = ""
    var value : String = ""

    required init?(map: Map) {
    }

    func mapping(map: Map) {

        key            <- map["key"]
        value          <- map["value"]
    }
}

