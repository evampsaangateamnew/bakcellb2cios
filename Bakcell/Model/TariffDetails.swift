//
//  TariffDetails.swift
//  Bakcell
//
//  Created by Saad Riaz on 9/14/17.
//  Copyright © 2017 evampsaanga. All rights reserved.
//

import Foundation
import ObjectMapper

struct TariffDetails: Mappable {
    
    var prepaid : PrepaidTariff?
    var postpaid : PostpaidTariff?
    
    init?(map : Map) {}
    
    mutating func mapping(map: Map) {
        
        prepaid    <- map["prepaid"]
        postpaid    <- map["postpaid"]
    }
}

struct PrepaidTariff: Mappable {
    
    var klass   : [Klass]?
    var cin     : [Cin]?
    var newCin  : [Klass]?
    
    var cinTarrifs : [NewCinTarrif] = []
    
    init?(map : Map) {}
    
    mutating func mapping(map: Map) {
        
        klass   <- map["klass"]
        cin     <- map["cin"]
        newCin  <- map["cin_new"]
        
        mapAndSortAllTariffs()
    }
    
    mutating func mapAndSortAllTariffs() {
        cin?.forEach({ (aCinObject) in
            let aTariffObject : NewCinTarrif = NewCinTarrif.init(tariffType: .cin, sortOrder: aCinObject.header?.sortOrder ?? 0, value: aCinObject as AnyObject)
            
            cinTarrifs.append(aTariffObject)
        })
        
        newCin?.forEach({ (aNewCinObject) in
            let aTariffObject : NewCinTarrif = NewCinTarrif.init(tariffType: .newCin, sortOrder: aNewCinObject.header?.sortOrder ?? 0, value: aNewCinObject as AnyObject)
            
            cinTarrifs.append(aTariffObject)
        })
        
        cinTarrifs.sort{ $0.sortOrder < $1.sortOrder }
        
//        cin = nil
//        newCin = nil
    }
}

struct NewCinTarrif {
    var tariffType : Constants.MBTariffType = .unSpecified
    var sortOrder : Int = 0
    var valueObject : AnyObject?
    var openedViewSection : MBSectionType = MBSectionType.Header
    
    init(tariffType :Constants.MBTariffType, sortOrder :Int, value :AnyObject?) {
        self.tariffType = tariffType
        self.sortOrder = sortOrder
        self.valueObject = value
    }
}

struct Klass: Mappable {
    
    var header : TariffHeader?
    var packagePrice: PackagePrice?
    var paygPrice : PaygPrice?
    var details : DetailsWithPriceArray?
    
    init?(map : Map) {}
    
    mutating func mapping(map: Map) {
        
        header          <- map["header"]
        packagePrice    <- map["packagePrice"]
        paygPrice       <- map["paygPrice"]
        details         <- map["details"]
        
    }
}

struct Cin: Mappable {
    
    var header : CinHeader?
    var details: DetailsWithPriceArray?
    var description : TariffDescription?
    
    init?(map : Map) {}
    
    mutating func mapping(map: Map) {
        
        header          <- map["header"]
        details         <- map["details"]
        description     <- map["description"]
        
    }
    
}


struct KlassPostpaid: Mappable {
    
    var header : TariffHeader?
    var packagePrice: PackagePriceKlassPostpaid?
    var details: DetailsWithPriceArray?
    
    
    init?(map : Map) {}
    
    mutating func mapping(map: Map) {
        
        header          <- map["header"]
        details         <- map["details"]
        packagePrice    <- map["packagePrice"]
        
    }
}

struct PackagePriceKlassPostpaid: Mappable {
    
    var packagePriceLabel : String = ""
    var call : CallSMSPriceSection?
    var sms : CallSMSPriceSection?
    var internet : InternetPriceSection?
    init?(map : Map) {}
    
    mutating func mapping(map: Map) {
        
        packagePriceLabel   <- map["packagePriceLabel"]
        call                <- map["call"]
        sms                 <- map["sms"]
        internet            <- map["internet"]
        
    }
    
}


struct DetailsWithPriceArray: Mappable {
    var detailLabel : String?
    var price : [Price]?
    var rounding : Rounding?
    var textWithTitle : TextWithTitle?
    var textWithOutTitle : String?
    var textWithPoints : [String?]?
    var date : DateAndTime?
    var time : DateAndTime?
    var roamingDetails : RoamingDetails?
    var freeResourceValidity : FreeResourceValidity?
    var titleSubTitleValueAndDesc : TitleSubTitleValueAndDesc?
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        detailLabel                 <- map["detailLabel"]
        price                       <- map["price"]
        rounding                    <- map["rounding"]
        textWithTitle               <- map["textWithTitle"]
        textWithOutTitle            <- map["textWithOutTitle.description"]
        textWithPoints              <- map["textWithPoints.pointsList"]
        date                        <- map["date"]
        time                        <- map["time"]
        roamingDetails              <- map["roamingDetails"]
        freeResourceValidity        <- map["freeResourceValidity"]
        titleSubTitleValueAndDesc   <- map["titleSubTitleValueAndDesc"]
        
    }
}

struct TariffHeader: Mappable {
    
    var id: String?
    var offeringId: String?
    var currency: String?
    var name: String?
    var priceLabel: String?
    var priceValue: String?
    var subscribable: String?
    var attributes: [HeaderAttributes]?
    var sortOrder : Int = 0
    
    init?(map : Map) {}
    
    mutating func mapping(map: Map) {
        
        id                           <- map["id"]
        offeringId                   <- map["offeringId"]
        currency                     <- map["currency"]
        name                         <- map["name"]
        priceLabel                   <- map["priceLabel"]
        priceValue                   <- map["priceValue"]
        subscribable                 <- map["subscribable"]
        attributes                   <- map["attributes"]
        sortOrder                    <- map["sortOrder"]
        
    }
}

struct HeaderAttributes: Mappable {
    
    var iconName : String?
    var title : String?
    var value : String?
    var metrics : String?
    
    init?(map : Map) {}
    
    mutating func mapping(map: Map) {
        
        iconName              <- map["iconName"]
        title                 <- map["title"]
        value                 <- map["value"]
        metrics               <- map["metrics"]
        
    }
    
}

struct PackagePrice: Mappable {
    
    var packagePriceLabel : String?
    var call : CallSMSPriceSection?
    var sms : CallSMSPriceSection?
    var internet : InternetPriceSection?
    
    init?(map : Map) {}
    
    mutating func mapping(map: Map) {
        
        packagePriceLabel           <- map["packagePriceLabel"]
        call                        <- map["call"]
        sms                         <- map["sms"]
        internet                    <- map["internet"]
        
    }
}


struct Attributes: Mappable {
    
    var title : String?
    var valueLeft : String?
    var valueRight : String?
    
    init?(map : Map) {}
    
    mutating func mapping(map: Map) {
        title               <- map["title"]
        valueLeft           <- map["valueLeft"]
        valueRight          <- map["valueRight"]
    }
    
}



struct PaygPrice: Mappable {
    var paygPriceLabel : String?
    var call : CallSMSPriceSection?
    var sms : CallSMSPriceSection?
    var internet : InternetPriceSection?
    
    
    init?(map : Map) {}
    
    mutating func mapping(map: Map) {
        
        paygPriceLabel              <- map["paygPriceLabel"]
        call                        <- map["call"]
        sms                         <- map["sms"]
        internet                    <- map["internet"]
    }
    
}


struct CinHeader : Mappable {
    
    var id : String?
    var offeringId : String?
    var bonusDescription : String?
    var bonusIconName : String?
    var bonusTitle : String?
    var name : String?
    var subscribable : String?
    var call : CallSMSPriceSection?
    var sms : CallSMSPriceSection?
    var internet : InternetPriceSection?
    var sortOrder : Int = 0
    
    init?(map : Map) {}
    
    mutating func mapping(map: Map) {
        
        id                  <- map["id"]
        offeringId          <- map["offeringId"]
        bonusDescription    <- map["bonusDescription"]
        bonusIconName       <- map["bonusIconName"]
        bonusTitle          <- map["bonusTitle"]
        name                <- map["name"]
        subscribable        <- map["subscribable"]
        call                <- map["call"]
        internet            <- map["internet"]
        sms                 <- map["sms"]
        sortOrder           <- map["sortOrder"]
    }
}

struct CallSMSPriceSection: Mappable {
    
    var iconName : String?
    var title : String?
    var titleValue : String?
    var titleValueLeft : String?
    var titleValueRight : String?
    var priceTemplate : String?
    var attributes : [CinCallAttributes]?
    //
    init?(map : Map) {}
    
    mutating func mapping(map: Map) {
        
        iconName            <- map["iconName"]
        title               <- map["title"]
        titleValue          <- map["titleValue"]
        titleValueLeft      <- map["titleValueLeft"]
        titleValueRight     <- map["titleValueRight"]
        priceTemplate       <- map["priceTemplate"]
        attributes          <- map["attributes"]
    }
}

struct CinCallAttributes: Mappable {
    
    var title : String?
    var value : String?
    var valueLeft : String?
    var valueRight : String?
    
    init?(map : Map) {}
    
    mutating func mapping(map: Map) {
        
        title           <- map["title"]
        value           <- map["value"]
        valueLeft       <- map["valueLeft"]
        valueRight      <- map["valueRight"]
    }
}

struct InternetPriceSection: Mappable {
    
    var iconName : String?
    var subTitle : String?
    var subTitleValue : String?
    var title : String?
    var titleValue : String?
    var attributes : [CinInternetAttributes]?
    
    init?(map : Map) {}
    
    mutating func mapping(map: Map) {
        
        iconName                           <- map["iconName"]
        subTitle                           <- map["subTitle"]
        subTitleValue                      <- map["subTitleValue"]
        title                              <- map["title"]
        titleValue                         <- map["titleValue"]
    }
}
struct CinInternetAttributes: Mappable {
    
    var title : String?
    var value : String?
    
    init?(map : Map) {}
    
    mutating func mapping(map: Map) {
        
        title           <- map["title"]
        value           <- map["value"]
    }
}


struct CinSMSAttributes: Mappable {
    
    var title : String?
    var value : String?
    
    init?(map : Map) {}
    
    mutating func mapping(map: Map) {
        
        title           <- map["title"]
        value           <- map["value"]
    }
}


struct CinMMSAttributes: Mappable {
    
    var title : String?
    var value : String?
    
    init?(map : Map) {}
    
    mutating func mapping(map: Map) {
        
        title           <- map["title"]
        value           <- map["value"]
    }
}

struct DetailMMSCallDestination: Mappable {
    
    
    var attributes : [CinCallDetailsAttribute]?
    var iconName : String?
    var title : String?
    var titleValue : String?
    
    
    init?(map : Map) {}
    
    mutating func mapping(map: Map) {
        
        attributes           <- map["attributes"]
        iconName             <- map["iconName"]
        title                <- map["title"]
        titleValue           <- map["titleValue"]
        
    }
}

struct CinCallDetailsAttribute: Mappable {
    
    var title : String?
    var value : String?
    
    init?(map : Map) {}
    
    mutating func mapping(map: Map) {
        
        title           <- map["title"]
        value           <- map["value"]
    }
}

struct CinDestinationDetailsAttribute: Mappable {
    
    var title : String?
    var value : String?
    
    init?(map : Map) {}
    
    mutating func mapping(map: Map) {
        
        title           <- map["title"]
        value           <- map["value"]
    }
}

struct TariffDescription: Mappable {
    
    var descriptionTitle : String?
    var advantages : Advantages?
    var classification : Classification?
    
    //Useing Corporate user
    var descLabel : String?
    
    init?(map : Map) {}
    
    mutating func mapping(map: Map) {
        
        descriptionTitle        <- map["descriptionTitle"]
        advantages              <- map["advantages"]
        classification          <- map["classification"]
        descLabel               <- map["descLabel"]
    }
}

struct Advantages: Mappable {
    
    var description : String?
    var title : String?
    
    init?(map : Map) {}
    
    mutating func mapping(map: Map) {
        
        description <- map["description"]
        title       <- map["title"]
    }
}

struct Classification: Mappable {
    
    var description : String?
    var title : String?
    
    init?(map : Map) {}
    
    mutating func mapping(map: Map) {
        
        description <- map["description"]
        title       <- map["title"]
    }
}

struct PostpaidTariff: Mappable {
    
    var corporate : [Corporate]?
    var individual : [Individual]?
    var klassPostpaid : [KlassPostpaid]?
    
    init?(map : Map) {}
    
    mutating func mapping(map: Map) {
        corporate       <- map["corporate"]
        individual      <- map["individual"]
        klassPostpaid   <- map["klassPostpaid"]
    }
}

struct Corporate: Mappable {
    
    var header : CinPostpaidHeader?
    var price : CinPostpaidPrice?
    var details : DetailsWithPriceArray?
    var description : TariffDescription?
    
    init?(map : Map) {}
    
    mutating func mapping(map: Map) {
        
        header          <- map["header"]
        price           <- map["price"]
        details         <- map["details"]
        description     <- map["description"]
    }
}

struct CinPostpaidHeader: Mappable {
    
    var id : String?
    var offeringId : String?
    var name : String?
    var priceLabel : String?
    var priceValue : String?
    var subscribable : String?
    var attributes : [HeaderAttributes]?
    
    init?(map : Map) {}
    
    mutating func mapping(map: Map) {
        
        id                      <- map["id"]
        offeringId              <- map["offeringId"]
        name                    <- map["name"]
        priceLabel              <- map["priceLabel"]
        priceValue              <- map["priceValue"]
        subscribable            <- map["subscribable"]
        attributes              <- map["attributes"]
    }
}

struct CinPostpaidPrice: Mappable {
    
    var priceLabel : String?
    var call : CallSMSPriceSection?
    var sms : CallSMSPriceSection?
    var internet : InternetPriceSection?
    
    init?(map : Map) {}
    
    mutating func mapping(map: Map) {
        
        priceLabel              <- map["priceLabel"]
        call                    <- map["call"]
        sms                     <- map["sms"]
        internet                <- map["internet"]
    }
    
}

struct CinPostpaidDetails: Mappable {
    
    var detailLabel : String?
    var destination : CinPostpaidDestination?
    var internationalOffPeak : CinInternationalOffPeak?
    var internationalOnPeak : CinInternationalOnPeak?
    
    init?(map : Map) {}
    
    mutating func mapping(map: Map) {
        
        detailLabel                             <- map["detailLabel"]
        destination                             <- map["destination"]
        internationalOffPeak                    <- map["internationalOffPeak"]
        internationalOnPeak                     <- map["internationalOnPeak"]
    }
}

struct CinPostpaidDestination: Mappable {
    var iconName : String?
    var title : String?
    var titleValue : String?
    var attributes : [CinDestinationAttribute]?
    
    init?(map : Map) {}
    
    mutating func mapping(map: Map) {
        iconName                 <- map["iconName"]
        title                    <- map["title"]
        titleValue               <- map["titleValue"]
        attributes               <- map["attributes"]
    }
}

struct CinDestinationAttribute: Mappable {
    
    var title : String?
    var value : String?
    
    init?(map : Map) {}
    
    mutating func mapping(map: Map) {
        
        title               <- map["title"]
        value               <- map["value"]
    }
}

struct CinInternationalOffPeak: Mappable {
    
    var description : String?
    var iconName : String?
    var title : String?
    
    init?(map : Map) {}
    
    mutating func mapping(map: Map) {
        
        description                 <- map["description"]
        iconName                    <- map["iconName"]
        title                       <- map["title"]
    }
    
}

struct CinInternationalOnPeak: Mappable {
    
    var description : String?
    var iconName : String?
    var title : String?
    
    init?(map : Map) {}
    
    mutating func mapping(map: Map) {
        
        description                 <- map["description"]
        iconName                    <- map["iconName"]
        title                       <- map["title"]
    }
    
}

struct Individual: Mappable {
    
    var header : IndividualHeader?
    var price : CinPostpaidPrice?
    var details : DetailsWithPriceArray?
    var description : TariffDescription?
    
    init?(map : Map) {}
    
    mutating func mapping(map: Map) {
        header              <- map["header"]
        price           <- map["price"]
        details             <- map["details"]
        description         <- map["description"]
        
    }
}

struct IndividualHeader : Mappable {
    
    var id : String?
    var offeringId : String?
    var name : String?
    var priceLabel : String?
    var priceValue : String?
    var subscribable : String?
    
    var attributes : [HeaderAttributes]?
    
//    var call : CallSMSPriceSection?
//    var sms : CallSMSPriceSection?
//    var internet : InternetPriceSection?
    
    init?(map : Map) {}
    
    mutating func mapping(map: Map) {
        
        id              <- map["id"]
        offeringId      <- map["offeringId"]
        name            <- map["name"]
        priceLabel      <- map["priceLabel"]
        priceValue      <- map["priceValue"]
        subscribable    <- map["subscribable"]
//        call                                    <- map["call"]
//        sms                                     <- map["sms"]
//        internet                                <- map["internet"]
        attributes      <- map["attributes"]
        
    }
}

struct DestinationIndividual : Mappable {
    
    var iconName : String?
    var title : String?
    var titleValue : String?
    var attributes : [Attributes]?
    
    init?(map : Map) {}
    
    mutating func mapping(map: Map) {
        
        iconName        <- map["iconName"]
        title           <- map["title"]
        titleValue      <- map["titleValue"]
        attributes      <- map["attributes"]
        
    }
}

struct International : Mappable {
    
    var iconName : String?
    var title : String?
    var description : String?
    
    init?(map : Map) {}
    
    mutating func mapping(map: Map) {
        
        iconName        <- map["iconName"]
        title           <- map["title"]
        description     <- map["description"]
    }
}



