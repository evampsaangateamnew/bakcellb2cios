//
//  TitleDescriptionCell.swift
//  Bakcell
//
//  Created by AbdulRehman Warraich on 9/20/17.
//  Copyright © 2017 evampsaanga. All rights reserved.
//

import UIKit

class TitleDescriptionCell: UITableViewCell {


    static let identifier : String = "TitleDescriptionCell"
    //    title label
    @IBOutlet var title_lbl: UILabel!

    //    Detail label
    @IBOutlet var detail_lbl: UILabel!

    // image view
    @IBOutlet var icon_img: UIImageView!
    @IBOutlet var iconWidthConstraint: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func setTitleAndDescription(title : String?, description : String? , iconName : String?) {

        title_lbl.text = title ?? ""
        detail_lbl.text = description ?? ""

        icon_img.image = MBUtilities.iconImageFor(key: iconName)
        iconWidthConstraint.constant = 22

    }

    func setTitleAndDescriptionWithOutIcon(title : String?, description : String?) {

        title_lbl.text = title ?? ""
        detail_lbl.text = description ?? ""
        iconWidthConstraint.constant = 0

    }
}
