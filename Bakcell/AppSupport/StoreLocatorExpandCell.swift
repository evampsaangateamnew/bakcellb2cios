//
//  StoreLocatorExpandCell.swift
//  Bakcell
//
//  Created by Saad Riaz on 6/8/17.
//  Copyright © 2017 evampsaanga. All rights reserved.
//

import UIKit

class StoreLocatorExpandCell: UITableViewCell {

    //Height Constraints for views
    @IBOutlet var contactImgHeightConstraint: NSLayoutConstraint!
    @IBOutlet var contacts_img: UIImageView!
    @IBOutlet var view5Height: NSLayoutConstraint!
    @IBOutlet var view4Height: NSLayoutConstraint!
    @IBOutlet var view3Height: NSLayoutConstraint!
    @IBOutlet var view2Height: NSLayoutConstraint!
    @IBOutlet var view1Height: NSLayoutConstraint!
    @IBOutlet var call_btn1: UIButton!
    @IBOutlet var call_btn2: UIButton!
    @IBOutlet var call_btn3: UIButton!
    
    //@IBOutlet var numbers_lbl: UILabel!
    
    @IBOutlet var contactNumbers_View: UIView!
    @IBOutlet var timings_lbl: UILabel!
    @IBOutlet var weeks_lbl: UILabel!
    @IBOutlet var holidaysTimings_lbl: UILabel!
    @IBOutlet var weekendTimings_lbl: UILabel!
    @IBOutlet var weekTimings_lbl: UILabel!
    @IBOutlet var weedaysTimings_lbl: UILabel!
    @IBOutlet var holidays_lbl: UILabel!
    @IBOutlet var weekend_lbl: UILabel!
    @IBOutlet var week_lbl: UILabel!
    @IBOutlet var weekDays_lbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
