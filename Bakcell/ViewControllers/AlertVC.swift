//
//  AlertVC.swift
//  Bakcell
//
//  Created by Saad Riaz on 6/30/17.
//  Copyright © 2017 evampsaanga. All rights reserved.
//

import UIKit
import MarqueeLabel

typealias MBButtonCompletionHandler = (_ gernralString : String) -> Void

class AlertVC: BaseVC {

    // Properties
    var alertTitle = ""
    var attributedBalanceTitle : NSMutableAttributedString = NSMutableAttributedString()
    var attributedDiscription : NSMutableAttributedString = NSMutableAttributedString()

    var yesButtonHolder = UIButton()
    var noButtonHolder = UIButton()

    var dissmisAlerOnButtonAction = true

    var isNoBtnAlignmentCenter = true

    fileprivate var yesBtncompletionHandlerBlock : MBButtonCompletionHandler?
    fileprivate var noBtncompletionHandlerBlock : MBButtonCompletionHandler?

    @IBOutlet var title_lbl: UILabel!
    @IBOutlet var balance_lbl: MarqueeLabel!
    @IBOutlet var description_lbl: UILabel!

    @IBOutlet var yes_btn: MBMarqueeButton!
    @IBOutlet var no_btn: MBMarqueeButton!


    @IBOutlet var noBtnLeftVerticalAlignment: NSLayoutConstraint!
    @IBOutlet var noBtnCenterVerticalAlignment: NSLayoutConstraint!

    @IBOutlet var titleHeightConst: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        yes_btn.setMarqueeButtonLayout()
        no_btn.setMarqueeButtonLayout()
        balance_lbl.setupMarqueeAnimation()

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.interactivePop(false)
        
        title_lbl.text = alertTitle

        balance_lbl.attributedText = attributedBalanceTitle
        description_lbl.attributedText = attributedDiscription

        yes_btn.setTitle(yesButtonHolder.title(for: .normal), for: UIControl.State.normal)
        yes_btn.setTitleColor(yesButtonHolder.titleColor(for: .normal), for: UIControl.State.normal)
        yes_btn.backgroundColor = yesButtonHolder.backgroundColor
        yes_btn.layer.borderColor = yesButtonHolder.layer.borderColor
        yes_btn.layer.borderWidth = yesButtonHolder.layer.borderWidth

        no_btn.setTitle(noButtonHolder.title(for: .normal), for: UIControl.State.normal)
        no_btn.setTitleColor(noButtonHolder.titleColor(for: .normal), for: UIControl.State.normal)
        no_btn.backgroundColor = noButtonHolder.backgroundColor
        no_btn.layer.borderColor = yesButtonHolder.layer.borderColor
        no_btn.layer.borderWidth = yesButtonHolder.layer.borderWidth

        if isNoBtnAlignmentCenter {

            yes_btn.isHidden = true;

            // Setting buttons layout alignment
            noBtnLeftVerticalAlignment.isActive = false
            noBtnCenterVerticalAlignment.isActive = true

        } else {

            yes_btn.isHidden = false;

            // Setting buttons layout alignment
            noBtnLeftVerticalAlignment.isActive = true
            noBtnCenterVerticalAlignment.isActive = false
        }
        no_btn.layoutIfNeeded()

        //        self.view.layoutIfNeeded()
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        if alertTitle.isEmpty == true || alertTitle == "" {
            titleHeightConst?.constant = 1
        }
    }

    //MARK: IBACTIONS
    @IBAction func YesBtnPressed(_ sender: UIButton) {
        if dissmisAlerOnButtonAction {

            self.dismiss(animated: true, completion: {
                self.yesBtncompletionHandlerBlock?("")
            })
        } else {
            self.yesBtncompletionHandlerBlock?("")
        }
    }
    @IBAction func NoBtnPressed(_ sender: UIButton) {

        if dissmisAlerOnButtonAction {

            self.dismiss(animated: true, completion: {
                self.noBtncompletionHandlerBlock?("")
            })
        } else {
            self.noBtncompletionHandlerBlock?("")
        }
    }


    //MARK: - MoneyTransfer Confirmation and success
    func setConfirmationAlertLayoutMoneyTransfer(title :String?, amount : String , reciver: String)  {

        self.initButtonsWithDefaultValues()

        isNoBtnAlignmentCenter = false

        alertTitle = title ?? ""

        attributedBalanceTitle = self.setBalanceText(amount: MBUserSession.shared.balance?.amount ?? "0.0", isBalanceColorBlack: true)

        var  messageString = ""

        if MBLanguageManager.userSelectedLanguage() == .Azeri {
            messageString = String(format: Localized("Message_ConfirmationMoneyTransfer"), reciver, amount)
        } else {
            messageString = String(format: Localized("Message_ConfirmationMoneyTransfer"), amount, reciver)
        }


        let message = NSMutableAttributedString(string: messageString, attributes: [NSAttributedString.Key.foregroundColor: UIColor.MBTextGrayColor, NSAttributedString.Key.font: UIFont.MBArial(fontSize: 14)])

        var amountTextRange = (messageString as NSString).range(of: amount)
        amountTextRange.length = amountTextRange.length + 4 // to include AZN

        message.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.black, range: amountTextRange)


        attributedDiscription = message

    }

    func setSuccessAlertForMoneyTransfer (title : String?, amount : String, receiver : String, showBalnce: Bool) {

        self.initButtonsWithDefaultValues()

        isNoBtnAlignmentCenter = true

        alertTitle = title ?? ""

        noButtonHolder.setTitle(Localized("BtnTitle_OK"), for: UIControl.State.normal)
        noButtonHolder.backgroundColor = UIColor.MBButtonBackgroundGrayColor
        noButtonHolder.setTitleColor(UIColor.black, for: UIControl.State.normal)

        noButtonHolder.layer.borderColor = UIColor.white.cgColor

        if  showBalnce == false {
            attributedBalanceTitle = NSMutableAttributedString()

        } else {
            attributedBalanceTitle = self.setBalanceText(amount: MBUserSession.shared.balance?.amount ?? "0.0", isBalanceColorBlack: false)
        }

        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.alignment = .left

        let successMessage = String(format: Localized("Message_MoneyTransfer_SuccessTransfer"), amount , receiver)

        let message = NSMutableAttributedString(string: successMessage, attributes: [NSAttributedString.Key.foregroundColor: UIColor.MBTextGrayColor, NSAttributedString.Key.paragraphStyle:paragraphStyle, NSAttributedString.Key.font: UIFont.MBArial(fontSize: 14)])

        var amountTextRange = (successMessage as NSString).range(of: amount)
        amountTextRange.length = amountTextRange.length + 4 // to include AZN

        message.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.black, range: amountTextRange)

        attributedDiscription = message

    }

    //MARK: - Top-Up Confirmation and success

    func setConfirmationAlertForTopUp(title :String?, reciver: String, showBalnce: Bool)  {

        self.initButtonsWithDefaultValues()

        isNoBtnAlignmentCenter = false

        alertTitle = title ?? ""


        if  showBalnce == false {

            attributedBalanceTitle = NSMutableAttributedString()
        } else {

            attributedBalanceTitle = self.setBalanceText(amount: MBUserSession.shared.balance?.amount ?? "0.0", isBalanceColorBlack: true)
        }
        let messageString = String(format: Localized("Message_TopUpConfirmation"), reciver)
        let message = NSMutableAttributedString(string: messageString, attributes: [NSAttributedString.Key.foregroundColor: UIColor.MBTextGrayColor])

        attributedDiscription = message

    }
    //MARK: - Top-Up PlasticCard Confirmation and success

    func showtopUpConfirmationAlert(title :String?, message: String)  {

        self.initButtonsWithDefaultValues()

        isNoBtnAlignmentCenter = false

        alertTitle = title ?? ""
        attributedBalanceTitle = NSMutableAttributedString()
        let messageString = String(format: message)
        let message = NSMutableAttributedString(string: messageString, attributes: [NSAttributedString.Key.foregroundColor: UIColor.MBTextGrayColor])

        attributedDiscription = message

    }

    func setSuccessAlertForTopUp (title : String?, reciver : String, showBalnce: Bool) {

        self.initButtonsWithDefaultValues()

        isNoBtnAlignmentCenter = true

        alertTitle = title ?? ""

        noButtonHolder.setTitle(Localized("BtnTitle_OK"), for: UIControl.State.normal)
        noButtonHolder.backgroundColor = UIColor.MBButtonBackgroundGrayColor
        noButtonHolder.setTitleColor(UIColor.black, for: UIControl.State.normal)

        if  showBalnce == false {
            attributedBalanceTitle = NSMutableAttributedString()

        } else {
            attributedBalanceTitle = self.setBalanceText(amount: MBUserSession.shared.balance?.amount ?? "0.0", isBalanceColorBlack: false)
        }

        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.alignment = .left

        let messageString = String(format: Localized("Message_TopUp_Success"), reciver)

        let message = NSMutableAttributedString(string: messageString, attributes: [NSAttributedString.Key.foregroundColor: UIColor.MBTextGrayColor, NSAttributedString.Key.paragraphStyle:paragraphStyle, NSAttributedString.Key.font: UIFont.MBArial(fontSize: 14)])

        attributedDiscription = message

    }

    //MARK: - GetLoan Confirmation and success

    func setConfirmationAlertLayoutForGetLoan(title :String?, amount : String)  {

        self.initButtonsWithDefaultValues()

        isNoBtnAlignmentCenter = false

        alertTitle = title ?? ""

        attributedBalanceTitle = self.setBalanceText(amount: MBUserSession.shared.balance?.amount ?? "0.0", isBalanceColorBlack: true)


        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.alignment = .left


        let messageString = String(format: Localized("Message_GetLoan"), amount)
        let message = NSMutableAttributedString(string: messageString,
                                                attributes: [NSAttributedString.Key.foregroundColor: UIColor.MBTextGrayColor, NSAttributedString.Key.paragraphStyle: paragraphStyle, NSAttributedString.Key.font: UIFont.MBArial(fontSize: 14)])

        var amountTextRange = (messageString as NSString).range(of: amount)
        amountTextRange.length = amountTextRange.length + 4 // to include AZN

        message.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.black, range: amountTextRange)


        attributedDiscription = message

    }

    func setSuccessAlertForGetLoan (title : String?, amount : String, showBalnce: Bool) {

        self.initButtonsWithDefaultValues()

        isNoBtnAlignmentCenter = true

        alertTitle = title ?? ""

        noButtonHolder.setTitle(Localized("BtnTitle_OK"), for: UIControl.State.normal)
        noButtonHolder.backgroundColor = UIColor.MBButtonBackgroundGrayColor
        noButtonHolder.setTitleColor(UIColor.black, for: UIControl.State.normal)

        if  showBalnce == false {
            attributedBalanceTitle = NSMutableAttributedString()

        } else {
            attributedBalanceTitle = self.setBalanceText(amount: MBUserSession.shared.balance?.amount ?? "0.0", isBalanceColorBlack: false)
        }

        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.alignment = .left

        let messageString = String(format: Localized("Msg_LoanSuccess"), amount)

        let message = NSMutableAttributedString(string: messageString, attributes: [NSAttributedString.Key.foregroundColor: UIColor.MBTextGrayColor, NSAttributedString.Key.paragraphStyle:paragraphStyle, NSAttributedString.Key.font: UIFont.MBArial(fontSize: 14)])

        let amountTextRange = (messageString as NSString).range(of: amount)
        message.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.black, range: amountTextRange)

        attributedDiscription = message

    }

    //MARK: - Genral Alert

    func setConfirmationAlertLayoutWithOutBalance(title :String?,message : String, yesBtnTitle : String = Localized("BtnTitle_YES"), noBtnTitle: String = Localized("BtnTitle_NO"))  {

        self.initButtonsWithDefaultValues()

        isNoBtnAlignmentCenter = false

        alertTitle = title ?? ""

        yesButtonHolder.setTitle(yesBtnTitle, for: UIControl.State.normal)
        noButtonHolder.setTitle(noBtnTitle, for: UIControl.State.normal)

        noButtonHolder.backgroundColor = UIColor.white
        noButtonHolder.backgroundColor = UIColor .white
        noButtonHolder.layer.borderWidth = 1
        noButtonHolder.layer.borderColor = UIColor.MBBorderGrayColor.cgColor

        attributedBalanceTitle = NSMutableAttributedString()

        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.alignment = .left

        let message = NSMutableAttributedString(string: message, attributes: [NSAttributedString.Key.foregroundColor: UIColor.MBTextGrayColor, NSAttributedString.Key.paragraphStyle:paragraphStyle, NSAttributedString.Key.font: UIFont.MBArial(fontSize: 14)])


        attributedDiscription = message
        
    }

    
    func setAlertForLogOut() {

        self.initButtonsWithDefaultValues()

        isNoBtnAlignmentCenter = false

        alertTitle = Localized("Title_Logout")

        // set Button Tilte
        yesButtonHolder.setTitle(Localized("BtnTitle_LOGOUT"), for: UIControl.State.normal)
        noButtonHolder.setTitle(Localized("BtnTitle_CANCEL"), for: UIControl.State.normal)

        // Change BackGround color
        yesButtonHolder.backgroundColor = UIColor.MBRedColor
        noButtonHolder.backgroundColor = UIColor.MBButtonBackgroundGrayColor

        // Button Title color
        yesButtonHolder.setTitleColor(UIColor.white, for: UIControl.State.normal)
        noButtonHolder.setTitleColor(UIColor.black, for: UIControl.State.normal)

        yesButtonHolder.layer.borderColor = UIColor.white.cgColor

        attributedBalanceTitle = NSMutableAttributedString()


        // Description label text
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.alignment = .left

        let message = NSMutableAttributedString(string: Localized("Message_LogoutMessage"), attributes: [NSAttributedString.Key.foregroundColor: UIColor.MBTextGrayColor,NSAttributedString.Key.paragraphStyle:paragraphStyle])
        message.addAttribute(NSAttributedString.Key.font,
                             value: UIFont.MBArial(fontSize: 14),
                             range: NSRange(location: 0 , length: message.length))

        attributedDiscription = message

    }

    func setAlertForActivateSuspend(forActive: Bool = false) {

        self.initButtonsWithDefaultValues()

        isNoBtnAlignmentCenter = false

        alertTitle = Localized("Title_Confirmation")

        // set Button Tilte
        no_btn.setTitle(Localized("BtnTitle_CANCEL"), for: UIControl.State.normal)

        // Change BackGround color
        yesButtonHolder.backgroundColor = UIColor.MBRedColor
        noButtonHolder.backgroundColor = UIColor.MBButtonBackgroundGrayColor


        // Button Title color
        yesButtonHolder.setTitleColor(UIColor.white, for: UIControl.State.normal)
        noButtonHolder.setTitleColor(UIColor.black, for: UIControl.State.normal)

        yesButtonHolder.layer.borderColor = UIColor.white.cgColor

        // Description label text
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.alignment = .left

        if forActive {
            yesButtonHolder.setTitle(Localized("BtnTitle_ACTIVE"), for: UIControl.State.normal)

            let message = NSMutableAttributedString(string: Localized("Message_ActiveMessage"), attributes: [NSAttributedString.Key.foregroundColor: UIColor.MBTextGrayColor,NSAttributedString.Key.paragraphStyle:paragraphStyle])

            message.addAttribute(NSAttributedString.Key.font,
                                 value: UIFont.MBArial(fontSize: 14),
                                 range: NSRange(location: 0 , length: message.length))

            attributedDiscription = message

        } else {

            yesButtonHolder.setTitle(Localized("BtnTitle_SUSPEND"), for: UIControl.State.normal)

            let message = NSMutableAttributedString(string: Localized("Message_SuspendMessage"), attributes: [NSAttributedString.Key.foregroundColor: UIColor.MBTextGrayColor,NSAttributedString.Key.paragraphStyle:paragraphStyle])

            message.addAttribute(NSAttributedString.Key.font,
                                 value: UIFont.MBArial(fontSize: 14),
                                 range: NSRange(location: 0 , length: message.length))

            attributedDiscription = message
        }

    }

    func setAlertForSuspendNumber() {

        self.initButtonsWithDefaultValues()

        isNoBtnAlignmentCenter = false

        alertTitle = Localized("Title_Confirmation")

        // set Button Tilte
        yesButtonHolder.setTitle(Localized("BtnTitle_SUSPEND"), for: UIControl.State.normal)
        noButtonHolder.setTitle(Localized("BtnTitle_CANCEL"), for: UIControl.State.normal)

        // Change BackGround color
        yesButtonHolder.backgroundColor = UIColor.MBRedColor
        noButtonHolder.backgroundColor = UIColor.MBButtonBackgroundGrayColor

        // Button Title color
        yesButtonHolder.setTitleColor(UIColor.white, for: UIControl.State.normal)
        noButtonHolder.setTitleColor(UIColor.black, for: UIControl.State.normal)

        yesButtonHolder.layer.borderColor = UIColor.white.cgColor

        // Description label text
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.alignment = .left

        let message = NSMutableAttributedString(string: "\(Localized("Message_SuspendNumberMessage"))", attributes: [NSAttributedString.Key.foregroundColor: UIColor.MBTextGrayColor,NSAttributedString.Key.paragraphStyle:paragraphStyle])
        message.addAttribute(NSAttributedString.Key.font,
                             value: UIFont.MBArial(fontSize: 14),
                             range: NSRange(location: 0 , length: message.length))
        attributedDiscription = message

    }

    func setAlertForUnreadNotifications(title :String?,message : String, yesBtnTitle : String = Localized("BtnTitle_YES"), noBtnTitle: String = Localized("BtnTitle_NO")) {

        self.initButtonsWithDefaultValues()

        isNoBtnAlignmentCenter = false

        alertTitle = ""

        // set Button Tilte
        yesButtonHolder.setTitle(Localized("BtnTitle_CANCEL2"), for: UIControl.State.normal)
        noButtonHolder.setTitle(Localized("BtnTitle_VIEWNOTIFICATIONS"), for: UIControl.State.normal)

        // Change BackGround color
        yesButtonHolder.backgroundColor = UIColor.MBButtonBackgroundGrayColor
        noButtonHolder.backgroundColor = UIColor.MBButtonBackgroundGrayColor

        // Button Title color
        yesButtonHolder.setTitleColor(UIColor.black, for: UIControl.State.normal)
        noButtonHolder.setTitleColor(UIColor.black, for: UIControl.State.normal)

        yesButtonHolder.layer.borderColor = UIColor.white.cgColor

        // Description label text
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.alignment = .left

        let message = NSMutableAttributedString(string: "\(message)", attributes: [NSAttributedString.Key.foregroundColor: UIColor.MBTextGrayColor,NSAttributedString.Key.paragraphStyle:paragraphStyle])
        message.addAttribute(NSAttributedString.Key.font,
                             value: UIFont.MBArial(fontSize: 14),
                             range: NSRange(location: 0 , length: message.length))
        attributedDiscription = message

    }


    // Yes and No button completion handler
    func setYesButtonCompletionHandler(completionBlock : @escaping MBButtonCompletionHandler ) {
        self.yesBtncompletionHandlerBlock = completionBlock
    }
    
    func setNoButtonCompletionHandler(completionBlock : @escaping MBButtonCompletionHandler ) {
        self.noBtncompletionHandlerBlock = completionBlock
    }

    //MARK: - Helping Functions
    func setBalanceText(amount: String, isBalanceColorBlack: Bool = true) -> NSMutableAttributedString {

        let balanceText = NSMutableAttributedString(string: Localized("Balance"), attributes: [NSAttributedString.Key.foregroundColor: UIColor.MBTextGrayColor, NSAttributedString.Key.font: UIFont.MBArial(fontSize: 14)])

        let amountAttributedText = NSMutableAttributedString(string: "\(amount) ₼" ,attributes: [ NSAttributedString.Key.font: UIFont.MBArial(fontSize: 14)])

        if isBalanceColorBlack {

            amountAttributedText.addAttribute(NSAttributedString.Key.foregroundColor,
                                              value: UIColor.black,
                                              range: NSRange(location: 0 , length: amountAttributedText.length))
        } else {

            amountAttributedText.addAttribute(NSAttributedString.Key.foregroundColor,
                                              value: UIColor.MBRedColor,
                                              range: NSRange(location: 0 , length: amountAttributedText.length))
        }

        balanceText.append(amountAttributedText)

        balanceText.addAttribute(NSAttributedString.Key.font,
                                 value: UIFont.systemFont(ofSize: 12),
                                 range: NSRange(location: (balanceText.length - 1), length: 1))

        balanceText.addAttribute(NSAttributedString.Key.baselineOffset,
                                 value:2.00,
                                 range: NSRange(location:(balanceText.length - 1),length:1))

        return balanceText
    }

    func initButtonsWithDefaultValues() {

        yesButtonHolder.setTitle(Localized("BtnTitle_YES"), for: UIControl.State.normal)
        yesButtonHolder.setTitleColor(yesButtonHolder.titleColor(for: .normal), for: UIControl.State.normal)
        yesButtonHolder.backgroundColor = UIColor.MBButtonBackgroundGrayColor
        yesButtonHolder.setTitleColor(UIColor.black, for: UIControl.State.normal)
        yesButtonHolder.layer.borderColor = UIColor.MBBorderGrayColor.cgColor
        yesButtonHolder.layer.borderWidth = 1


        noButtonHolder.setTitle(Localized("BtnTitle_NO"), for: UIControl.State.normal)
        noButtonHolder.setTitleColor(UIColor.black, for: UIControl.State.normal)
        noButtonHolder.backgroundColor = UIColor.white
        noButtonHolder.setTitleColor(UIColor.black, for: UIControl.State.normal)
        noButtonHolder.layer.borderColor = UIColor.MBBorderGrayColor.cgColor
        noButtonHolder.layer.borderWidth = 1
    }
}
