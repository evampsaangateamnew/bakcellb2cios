//
//  RechargeVC.swift
//  Bakcell
//
//  Created by Saad Riaz on 6/8/17.
//  Copyright © 2017 evampsaanga. All rights reserved.
//

import UIKit

class RechargeVC: BaseVC,UITableViewDelegate,UITableViewDataSource {
    
    //Mark: - Properties
 var titleArr: [String] = []
var images: [String] = ["Sub_top_up","MoneyTransfer","loan"]

    //Mark: - IBOutlet
    @IBOutlet var topUpTitle_lbl: UILabel!
    @IBOutlet var tableView: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()

    }

    //Mark: - View Controller Methods
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.interactivePop(true)
        
        if MBUserSession.shared.subscriberType() == MBSubscriberType.PostPaid {
            titleArr = [Localized("Title_TopUP")]
        } else {
            if MBUserSession.shared.brandName() == MBBrandName.Klass{
                titleArr = [Localized("Title_TopUP"),Localized("Op2_MoneyTransfer"),Localized("Loan_TitleKlass")]
            }
            else{
                titleArr = [Localized("Title_TopUP"),Localized("Op2_MoneyTransfer"),Localized("Loan_TitleCin")]
            }
        }

        loadViewLayout()
    }

    //MARK: - FUNCTIONS
    func loadViewLayout(){
        
        topUpTitle_lbl.font = UIFont.MBArial(fontSize: 20)
        topUpTitle_lbl.text = Localized("Title_TopUP")
        tableView.reloadData()
    }
    
    //MARK: TABLE VIEW METHODS
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return titleArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellID", for: indexPath) as! AccountsCell
         cell.title_lbl.font = UIFont.MBArial(fontSize: 14)
        cell.title_lbl.text = titleArr[indexPath.row]
        cell.img.image = UIImage (named: images[indexPath.row])
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.row {
        case 0:
            let topUp = self.myStoryBoard.instantiateViewController(withIdentifier: "TopUpVC") as! TopUpVC
            self.navigationController?.pushViewController(topUp, animated: true)
        case 1:
            let money = self.myStoryBoard.instantiateViewController(withIdentifier: "MoneyTransferVC") as! MoneyTransferVC
            self.navigationController?.pushViewController(money, animated: true)
            
        case 2:
            let loan = self.myStoryBoard.instantiateViewController(withIdentifier: "LoanVC") as! LoanVC
            self.navigationController?.pushViewController(loan, animated: true)
            
        default:
            break
        }
        tableView.deselectRow(at: indexPath, animated: true)
    }


}
