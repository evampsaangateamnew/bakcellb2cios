//
//  SignUpPINVerifyVC.swift
//  Bakcell
//
//  Created by Saad Riaz on 6/5/17.
//  Copyright © 2017 evampsaanga. All rights reserved.
//

import UIKit
import ObjectMapper

class SignUpPINVerifyVC: BaseVC {

    //MARK:- Properties
    var msisdn : String = ""
    var isForgotPassword : Bool = false
    var signUpUserInfo : SignUp = SignUp()
    var registrationType :Constants.RegistrationType = .registration

    //MARK:- IBOutlet
    @IBOutlet var pin_txt: UITextField!

    @IBOutlet var nextButtonView: UIView!
    @IBOutlet var backButtonView: UIView!
    @IBOutlet var resentButtonView: UIView!

    @IBOutlet var info_lbl: UILabel!
    @IBOutlet var back_lbl: UILabel!
    @IBOutlet var next_lbl: UILabel!
    @IBOutlet var resend_lbl: UILabel!
    @IBOutlet var pinNotRecieved_lbl: UILabel!
    @IBOutlet var backcelIcon_img: UIImageView!

    //MARK:- ViewCintroller Methods
    override func viewDidLoad() {
        super.viewDidLoad()

        pin_txt.delegate = self

        //  Disabling Back and Next button
        setBackButtonSelection(buttonView: backButtonView, isEnabled: false)
        setNextButtonSelection(buttonView: nextButtonView, isEnabled: false)

        loadViewLayout()

        self.animatebackButtonEnablingCountDown()
    }

    //MARK:- IBACTIONS
    @IBAction func nextPressed(_ sender: Any) {
        var otp : String = ""
        // OTP validation
        if let otpText = pin_txt.text,
            otpText.lengthWithOutSpace == 4 {

           otp = otpText.trimmWhiteSpace
        } else {
            self.showErrorAlertWithMessage(message: Localized("Message_InvalidOTP"))
            return
        }

        // API call for OTP verification
        if isForgotPassword {
            verifyOTPAPICall(MSISDN: msisdn, OTP: otp, IsSignUpRequest: false)
        } else {
            verifyOTPAPICall(MSISDN: msisdn, OTP: otp, IsSignUpRequest: true)
        }
    }

    @IBAction func reSendPinPressed(_ sender: Any) {
        // API call for resending OTP
        if isForgotPassword {
            reSendPin(MSISDN: msisdn, ofType: .ForgotPassword)
        } else {
            reSendPin(MSISDN: msisdn, ofType: .SignUp)
        }
    }
    
    //MARK: - FUNCTIONS

    /**
     Set localized text in viewController
     */
    func loadViewLayout() {
        pinNotRecieved_lbl.font = UIFont.MBArial(fontSize: 14)
        resend_lbl.font = UIFont.MBArialBold(fontSize: 16)
        next_lbl.font = UIFont.MBArialBold(fontSize: 16)
        back_lbl.font = UIFont.MBArialBold(fontSize: 16)
        
        pinNotRecieved_lbl.text = Localized("Info_PinNotRecieved")
        resend_lbl.text = Localized("BtnTitle_Resend")
        next_lbl.text = Localized("BtnTitle_NEXT")
        back_lbl.text = Localized("BtnTitle_Back")
        info_lbl.text = "\(Localized("VerifyPin_DescFirstPart")) \(25) \(Localized("VerifyPin_DescSecondPart"))"
        pin_txt.placeholder = Localized("EnterPin_PlaceHolder")

        backcelIcon_img.image = UIImage(named: Localized("Img_BakcelLogo"))
    }

    /**
     Animate back button Enabling CountDown.

     - returns: void
     */
    func animatebackButtonEnablingCountDown() {

        let duration: Int = 25//seconds
        DispatchQueue.global().async {
            for i in (0...duration).reversed() {
                DispatchQueue.main.async {

                    self.info_lbl.text = "\(Localized("VerifyPin_DescFirstPart")) \(i) \(Localized("VerifyPin_DescSecondPart"))"

                    if i == 0 {
                        // Enable Back button
                        self.setBackButtonSelection(buttonView: self.backButtonView, isEnabled: true)
                        
                        if self.registrationType == .addAccount {
                            self.backButton?.isHidden = false
                        } else {
                            self.backButton?.isHidden = true
                        }
                    }
                }
                let sleepTime = UInt32(duration/duration * 1000000)
                usleep(sleepTime)
            }
        }
    }

    //MARK: - API Calls

    /**
     Call 'verifyOTP' API with the specified `MSISDN` and `OTP`.

     - parameter MSISDN: MSISDN of current user.
     - parameter OTP:  The One Time PIN user entered.
     - parameter isSignUpRequest:  set true in case of signup and set false in case of forgot.

     - returns: void
     */
    func verifyOTPAPICall(MSISDN msisdn : String , OTP pin : String, IsSignUpRequest isSignUpRequest:Bool) {

        activityIndicator.showActivityIndicator()

        _ = MBAPIClient.sharedClient.verifyOTP(MSISDN: msisdn, OTP: pin, requestForSignUp: isSignUpRequest, { (response, resultData, error, isCancelled, status, resultCode, resultDesc) in

            self.activityIndicator.hideActivityIndicator()
            if error != nil {
                
                if self.isForgotPassword == false{
                    //EventLog
                    MBAnalyticsManager.logEvent(screenName: "Signup Screen Step2", contentType:"PIN Verification Failure" , status:"Failure" )
                } else {
                    MBAnalyticsManager.logEvent(screenName: "Forgotpassword Screen Step2", contentType:"PIN Verification Failure" , status:"Failure" )
                }
                error?.showServerErrorInViewController(self)

            } else {
                // handling data from API response.
                if resultCode == Constants.MBAPIStatusCode.succes.rawValue {
                    if self.isForgotPassword == false{
                        //EventLog
                        MBAnalyticsManager.logEvent(screenName: "Signup Screen Step2", contentType:"PIN Verification Success" , status:"Success" )
                    } else {
                        MBAnalyticsManager.logEvent(screenName: "Forgotpassword Screen Step2", contentType:"PIN Verification Success" , status:"Success" )
                    }

                    // Parssing response data
                    if let _ = Mapper<MessageResponse>().map(JSONObject:resultData) {

                        // Redirecting user to Set Password screen.
                        if let setPassword  = self.myStoryBoard.instantiateViewController(withIdentifier: "SetPasswordVC") as? SetPasswordVC {
                            setPassword.isForgotPassword = self.isForgotPassword
                            setPassword.registrationType = self.registrationType
                            setPassword.msisdn = self.msisdn
                            setPassword.signUpUserInfo = self.signUpUserInfo
                            setPassword.verifiedOTP = pin.aesEncrypt(key: MBUserSession.shared.aSecretKey)
                            self.navigationController?.pushViewController(setPassword, animated: true)
                        }

                    }
                } else {
                    if self.isForgotPassword == false {
                        //EventLog
                        MBAnalyticsManager.logEvent(screenName: "Signup Screen Step2", contentType:"PIN Verification Failure" , status:"Failure" )
                    } else {
                        MBAnalyticsManager.logEvent(screenName: "Forgotpassword Screen Step2", contentType:"PIN Verification Failure" , status:"Failure" )
                    }
                    // Show error alert to user
                    self.showErrorAlertWithMessage(message: resultDesc)
                }
            }
        })
    }



    /**
     Call 'reSendPin' API with the specified `MSISDN`

     - parameter MSISDN: MSISDN of current user.
     - parameter ofType:  type of pin.

     - returns: void
     */

    func reSendPin(MSISDN msisdn : String, ofType : Constants.MBResendType ) {
        activityIndicator.showActivityIndicator()

        _ = MBAPIClient.sharedClient.reSendPin(MSISDN: msisdn, ofType: ofType,  { (response, resultData, error, isCancelled, status, resultCode, resultDesc) in
            self.activityIndicator.hideActivityIndicator()
            if error != nil {
                error?.showServerErrorInViewController(self)

            } else {
                // handling data from API response.
                if resultCode == Constants.MBAPIStatusCode.succes.rawValue {

                    // Parssing response data
                    if let _ = Mapper<ReSendPin>().map(JSONObject:resultData) {

                    } 
                } else {
                    // Show error alert to user
                    self.showErrorAlertWithMessage(message: resultDesc)
                }
            }
        })
    }

}


//MARK: - Textfield delagates

extension SignUpPINVerifyVC : UITextFieldDelegate {

    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let text = textField.text else { return false }

        //Set max Limit for textfields
        let newLength = text.count + string.count - range.length

        //  OTP textfield
        if textField == pin_txt {

            // limiting OTP lenght to 4 digits
            let allowedCharacters = CharacterSet(charactersIn: Constants.allowedNumbers)
            let characterSet = CharacterSet(charactersIn: string)
            
            
            if newLength <= 4 && allowedCharacters.isSuperset(of: characterSet) {
                
                if newLength >= 1 && newLength <= 4 {
                    setNextButtonSelection(buttonView: resentButtonView, isEnabled: false)
                } else {
                    setNextButtonSelection(buttonView: resentButtonView, isEnabled: true)
                }
                
                // Next button enable disable
                if newLength == 4 {
                    setNextButtonSelection(buttonView: nextButtonView, isEnabled: true)
                } else {
                    setNextButtonSelection(buttonView: nextButtonView, isEnabled: false)
                }
                // Returnimg number input
                return  true
            } else {
                return  false
            }
        } else  {
            return false
        }
    }

    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        setNextButtonSelection(buttonView: nextButtonView, isEnabled: false)
        return true
    }
}
