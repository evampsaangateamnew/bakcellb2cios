//
//  VerifyOTPVC.swift
//  Bakcell
//
//  Created by AbdulRehman Warraich on 1/1/19.
//  Copyright © 2019 evampsaanga. All rights reserved.
//

import UIKit
import ObjectMapper

class VerifyOTPVC: BaseVC {
    
    //MARK: - IBOutlet
    @IBOutlet var backgroundView: UIView!
    @IBOutlet var pinField: UITextField!
    @IBOutlet var description_lbl: UILabel!
    @IBOutlet var pinNotRecieve_lbl: UILabel!
    @IBOutlet var submit_btn: UIButton!
    @IBOutlet var resend_lbl: UILabel!
    @IBOutlet var resentButtonView: UIView!
    
    fileprivate var successCompletionHandlerBlock : () -> Void = {}
    fileprivate var failureCompletionHandlerBlock : () -> Void = {}
    
    //MARK: - View Controller Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        loadViewController()
        
        pinField.delegate = self
        
        // API call for resending OTP
        reSendPin(MSISDN: MBUserSession.shared.msisdn, ofType: .MoneyTransfer, isOnStrart: true)
    }
    
    //MARK: IBACTIONS
    @IBAction func submitPressed(_ sender: Any) {
        var otp : String = ""
        // OTP validation
        if let otpText = pinField.text,
            otpText.lengthWithOutSpace == 4 {
            
            otp = otpText.trimmWhiteSpace
            
        } else {
            self.showErrorAlertWithMessage(message: Localized("Message_InvalidOTP"))
            return
        }
        
        // API call for OTP verification
        self.verifyPin(otp)
        
    }
    
    @IBAction func reSendPinPressed(_ sender: Any) {
        // API call for resending OTP
        reSendPin(MSISDN: MBUserSession.shared.msisdn, ofType: .MoneyTransfer)
    }
    
    //MARK: - FUNCTIONS
    func loadViewController(){
        description_lbl.text = Localized("Title_PinDescription")
        pinNotRecieve_lbl.text = Localized("Title_PinNotRecieved")
        resend_lbl.text = Localized("Title_RESEND")
        submit_btn.setTitle(Localized("BtnTitle_Submit"), for: UIControl.State.normal)
        
        pinField.textColor = UIColor.MBTextGrayColor
        pinField.attributedPlaceholder =  NSAttributedString(string: Localized("PlaceHolder_EnterPIN"), attributes: [NSAttributedString.Key.foregroundColor: UIColor.MBTextGrayColor])
    }
    
    /**
     Enable and disable Resent button accourding to isEnabled value.
     
     - parameter buttonView: view which have image, arrow and button.
     - parameter isEnabled: set ture for enabling and false for disabling.
     
     - returns: void
     */
    func setResendButtonSelection(buttonView: UIView, isEnabled: Bool) {
        
        // Loop through all subviews of buttonView
        buttonView.subviews.forEach { (aView) in
            
            // Check if subView is UIImageView
            if aView is UIImageView {
                
                // Convert view to UIImageView
                if let imageView = aView as? UIImageView {
                    
                    // Set UIImageView image
                    if isEnabled {
                        imageView.image = UIImage(named: "arrow_resend")
                    } else {
                        imageView.image = UIImage(named: "disable_arrow_resend")
                    }
                }
            } else if aView is UILabel { // Check if subView is UILabel
                
                // Convert view to UILabel
                if let tiltLable = aView as? UILabel {
                    
                    // Change label textColor
                    if isEnabled {
                        tiltLable.textColor = UIColor.black
                    } else {
                        tiltLable.textColor = UIColor.lightGray
                    }
                }
            } else if aView is UIButton { // Check if subView is UIButton
                
                // Convert view to UIButton and Enable and Disable
                if let aButton = aView as? UIButton {
                    aButton.isEnabled = isEnabled
                }
            }
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        let touch = touches.first!
        if touch.view == backgroundView {
            self.dismiss(animated: true) {
                self.pinField.resignFirstResponder()
                self.failureCompletionHandlerBlock()
            }
        }
        
    }
    
    func setStatusCompletionBlocks(sucessBlock : @escaping () -> Void = {}, failureBlock : @escaping () -> Void = {}) {

        successCompletionHandlerBlock = sucessBlock
        failureCompletionHandlerBlock = failureBlock
    }
    
    //MARK: - API Calls
    /// Call 'verifypin' API .
    ///
    /// - returns:
    func verifyPin(_ pin:String?) {
        
        activityIndicator.showActivityIndicator()
        
        _ = MBAPIClient.sharedClient.verifyPin(Pin: pin?.trimmWhiteSpace ?? "", { (response, resultData, error, isCancelled, status, resultCode, resultDesc) in
            
            self.activityIndicator.hideActivityIndicator()
            if error != nil {
                error?.showServerErrorInViewController(self)
                
            } else {
                // handling data from API response.
                if resultCode == Constants.MBAPIStatusCode.succes.rawValue {
                    
                    
                    if let _ = Mapper<MessageResponse>().map(JSONObject: resultData) {
                        
                        // MBUserSession.shared.isOTPVerified = true
                        
                        self.dismiss(animated: true) {
                            self.pinField.resignFirstResponder()
                            self.successCompletionHandlerBlock()
                        }
                    }
                    
                } else {
                    // Show error alert to user
                    self.showErrorAlertWithMessage(message: resultDesc)
                }
            }
        })
    }
    
    
    /// Call 'reSendPin' API with the specified `MSISDN`
    ///
    /// - parameter MSISDN:     The MSISDN.
    /// - returns: Void
    func reSendPin(MSISDN msisdn : String, ofType : Constants.MBResendType, isOnStrart: Bool = false) {
        activityIndicator.showActivityIndicator()
        _ = MBAPIClient.sharedClient.historyReSendPIN(MSISDN: msisdn, ofType: ofType,  { (response, resultData, error, isCancelled, status, resultCode, resultDesc) in
            self.activityIndicator.hideActivityIndicator()
            if error != nil {
                error?.showServerErrorInViewController(self, { _ in
                    if isOnStrart {
                        self.dismiss(animated: true) {
                            self.pinField.resignFirstResponder()
                            self.failureCompletionHandlerBlock()
                        }
                    }
                })
                
            } else {
                // handling data from API response.
                if resultCode == Constants.MBAPIStatusCode.succes.rawValue {
                    
                    // Parssing response data
                    if let newPinHandler = Mapper<ReSendPin>().map(JSONObject:resultData) {
                        
                        // new pin sent to user
                        self.pinField.text = newPinHandler.pin
                    }
                } else {
                    // Show error alert to user
                    self.showErrorAlertWithMessage(message: resultDesc,{ _ in
                        if isOnStrart {
                            self.dismiss(animated: true) {
                                self.pinField.resignFirstResponder()
                                self.failureCompletionHandlerBlock()
                            }
                        }
                    })
                }
            }
        })
    }
}


//MARK: - Textfield delagates

extension VerifyOTPVC : UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let text = textField.text else { return false }
        
        //Set max Limit for textfields
        let newLength = text.count + string.count - range.length
        
        //  OTP textfield
        if textField == pinField {
            
            // limiting OTP lenght to 4 digits
            let allowedCharacters = CharacterSet(charactersIn: Constants.allowedNumbers)
            let characterSet = CharacterSet(charactersIn: string)
            
            if newLength <= 4 && allowedCharacters.isSuperset(of: characterSet) {
                
                if newLength >= 1 && newLength <= 4 {
                    setResendButtonSelection(buttonView: resentButtonView, isEnabled: false)
                } else {
                    setResendButtonSelection(buttonView: resentButtonView, isEnabled: true)
                }
                // Returnimg number input
                return  true
            } else {
                return  false
            }
        } else  {
            return false
        }
    }
}
