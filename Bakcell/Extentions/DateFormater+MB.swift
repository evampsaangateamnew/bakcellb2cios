//
//  DateFormater+MB.swift
//  Bakcell
//
//  Created by AbdulRehman Warraich on 1/2/19.
//  Copyright © 2019 evampsaanga. All rights reserved.
//

import Foundation
extension DateFormatter {
    
    public func createDate(from dateString:String, dateFormate :String = "dd/MM/yy") -> Date? {
        
        self.timeZone = TimeZone.appTimeZone()
        self.dateFormat = dateFormate
        
        return self.date(from: dateString)
    }
    
    public func createString(from fromDate:Date, dateFormate :String = "dd/MM/yy") -> String {
        
        self.timeZone = TimeZone.appTimeZone()
        self.dateFormat = dateFormate
        
        return self.string(from: fromDate)
    }
}
