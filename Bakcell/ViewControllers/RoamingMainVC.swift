//
//  RoamingMainVC.swift
//  Bakcell
//
//  Created by Muhammad Irfan Awan on 25/11/2020.
//  Copyright © 2020 evampsaanga. All rights reserved.
//

import UIKit
import ObjectMapper

class RoamingMainVC:  BaseVC {
    
    var titleArr: [String] = [Localized("Title_SupplementaryOffers"),
                              Localized("Title_QuickServices")]
    var images: [String] = ["Supplementry-offers",
                            "Quick-services"]
    
    // MARK: - IBOutlet
    @IBOutlet var roamingTitle_lbl: UILabel!
    
    
    @IBOutlet  var viewRoamingService: UIView!
    @IBOutlet  var lblRoamingService: UILabel!
    @IBOutlet  var imgRoamingService: UIImageView!
    @IBOutlet var roamingServiceSwitch: MBSwitch!
    @IBOutlet var roamingServiceTopCost: NSLayoutConstraint!
    
    @IBOutlet  var viewPricesOperators: UIView!
    @IBOutlet  var lblPricesOperators: UILabel!
    @IBOutlet  var imgPricesOperators: UIImageView!
    
    @IBOutlet  var viewPackages: UIView!
    @IBOutlet  var lblPackages: UILabel!
    @IBOutlet  var imgPackages: UIImageView!
    
    // MARK: - View Controllers Method
    override func viewDidLoad() {
        super.viewDidLoad()
        
        roamingServiceSwitch.addTarget(self, action:#selector(roamingSwitchValueChanged) , for: UIControl.Event.valueChanged)
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.interactivePop(false)
        
        loadViewLayout()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if let supplementaryOffersHandler : SupplementaryOfferings = SupplementaryOfferings.loadFromUserDefaults(key: APIsType.supplementaryOffer.selectedLocalizedAPIKey()){
            //print("offer found:\(supplementaryOffersHandler.roaming?.offers ?? [Offers]())")
            if (supplementaryOffersHandler.roaming?.offers?.count ?? 0) == 0 {
                self.activityIndicator.showActivityIndicator()
                getSupplementaryOfferingsMain()
            }
        } else {
            self.activityIndicator.showActivityIndicator()
            getSupplementaryOfferingsMain()
        }
        getMySubscriptions()
    }
    
    //MARK: - FUNCTIONS
    func loadViewLayout(){
        roamingTitle_lbl.font = UIFont.MBArial(fontSize: 20)
        roamingTitle_lbl.text = Localized("Title_Roaming")
        lblRoamingService.text = Localized("Title_RoamingServices")
        lblPricesOperators.text = Localized("Title_RoamingPricePackages")
        lblPackages.text = Localized("Title_RoamingPackages")
        imgRoamingService.image = UIImage(named: "active-roaming-az")
        imgPricesOperators.image = UIImage(named: "roaming-operators-az")
        imgPackages.image = UIImage(named: "roaming-packages-az")
        
        
        /* Reset service menu */
        titleArr = [Localized("Title_SupplementaryOffers"), Localized("Title_QuickServices")]
        images = ["Supplementry-offers", "Quick-services"]
        
        if MBUserSession.shared.shouldHideCoreServices() == false {
            titleArr.append(Localized("Title_CoreServices"))
            images.append("Core-services")
        }
        
        if MBUserSession.shared.predefineData?.fnfAllowed.isStringTrue() == true {
            titleArr.append(Localized("Title_FriendsAndFamily"))
            images.append("FF")
        }
        
        if MBUserSession.shared.predefineData?.roamingVisible == false {
            viewRoamingService.isHidden = true
            roamingServiceTopCost.constant = 0
            self.view.layoutSubviews()
        } else {
            viewRoamingService.isHidden = false
            roamingServiceTopCost.constant = 70
            self.view.layoutSubviews()
        }
        
        self.getRoamingStatus()
        roamingServiceSwitch.isOn = MBUserSession.shared.isRoamingEnabled()
    }
    
    @objc func roamingSwitchValueChanged() {

        if roamingServiceSwitch.isOn {
            enameDisableRoaming(actionType: "2")
        } else {
            enameDisableRoaming(actionType: "1")
        }

    }
    
    
    @IBAction func action_roamingServices(_ sender: UIButton) {
        
    }
    
    @IBAction func action_pricesOperators(_ sender: UIButton) {
        if let roamingVC = self.myStoryBoard.instantiateViewController(withIdentifier: "RoamingCountriesVC") as? RoamingCountriesVC {
            self.navigationController?.pushViewController(roamingVC, animated: true)
        }
    }
    
    
    @IBAction func action_packages(_ sender: UIButton) {
        if let roaming = self.myStoryBoard.instantiateViewController(withIdentifier: "RoamingVC") as? RoamingVC {
            
            if let supplementaryOffersHandler : SupplementaryOfferings = SupplementaryOfferings.loadFromUserDefaults(key: APIsType.supplementaryOffer.selectedLocalizedAPIKey()){
                //print("offer found:\(supplementaryOffersHandler.roaming?.offers ?? [Offers]())")
                if (supplementaryOffersHandler.roaming?.offers?.count ?? 0) == 0 {
                    self.activityIndicator.showActivityIndicator()
                    getSupplementaryOfferingsMain()
                } else {
                    roaming.offers = supplementaryOffersHandler.roaming?.offers ?? [Offers]()
                    roaming.countries = [""]
                    for contryitem in supplementaryOffersHandler.roaming?.countriesFlags ?? [Country]() {
                        roaming.countries?.append(contryitem.name ?? "")
                    }
                    roaming.isCardView = true
                    roaming.showNavBar = true
                    roaming.filters = supplementaryOffersHandler.roaming?.filters
                    self.navigationController?.pushViewController(roaming, animated: true)
                }
            } else {
                self.activityIndicator.showActivityIndicator()
                getSupplementaryOfferingsMain()
            }
        }
    }
    
}


//MARK: - API Calling
extension RoamingMainVC {
    
    
    func enameDisableRoaming(actionType:String) {
        activityIndicator.showActivityIndicator()
        
        _ = MBAPIClient.sharedClient.enableDisableRoaminng(OfferingId: "177943157", ActionType: actionType, { (response, resultData, error, isCancelled, status, resultCode, resultDesc)  in
            
            self.activityIndicator.hideActivityIndicator()
            if error != nil {
                //EventLog
                MBAnalyticsManager.logEvent(screenName: "Roaming Screen", contentType:"Roaming Main Screen" , status:"Roaming Activation/Deactivation Failure" )
                
                error?.showServerErrorInViewController(self)
            } else {
                // handling data from API response.
                if resultCode == Constants.MBAPIStatusCode.succes.rawValue {
                    
                    self.activityIndicator.hideActivityIndicator()
                    //EventLog
                    MBAnalyticsManager.logEvent(screenName: "Roaming Screen", contentType:"Roaming Main Screen" , status:"Roaming Activation/Deactivation Success" )
                    
                    // Parssing response data
                    if let mySubscriptionOffersHandler = Mapper<MySubscription>().map(JSONObject: resultData){
                        
                        // Show succes alert
                        self.showSuccessAlertWithMessage(message: mySubscriptionOffersHandler.message)
                        
                        // reload CollectionView
                        self.updateroamingSwitch(withActionType:actionType)
                        
                    } else {
                        // Show Error alert
                        self.showErrorAlertWithMessage(message: resultDesc)
                        
                    }
                } else {
                    
                    //EventLog
                    MBAnalyticsManager.logEvent(screenName: "Roaming Screen", contentType:"Roaming Main Screen" , status:"Roaming Activation/Deactivation Failure" )
                    
                    // Show Error alert
                    self.showErrorAlertWithMessage(message: resultDesc)
                    
                }
            }
        })
    }
    
    
    func getRoamingStatus() {
//        activityIndicator.showActivityIndicator()
        
        _ = MBAPIClient.sharedClient.userRoamingStatus(OfferingId: "177943157", { (response, resultData, error, isCancelled, status, resultCode, resultDesc)  in
            
            self.activityIndicator.hideActivityIndicator()
            if error != nil {
                //EventLog
                MBAnalyticsManager.logEvent(screenName: "Roaming Screen", contentType:"Roaming Main Screen" , status:"Get Roaming Status Failure" )
                
//                error?.showServerErrorInViewController(self)
            } else {
                // handling data from API response.
                if resultCode == Constants.MBAPIStatusCode.succes.rawValue {
                    
                    self.activityIndicator.hideActivityIndicator()
                    //EventLog
                    MBAnalyticsManager.logEvent(screenName: "Roaming Screen", contentType:"Roaming Main Screen" , status:"Get Roaming Status Success" )
                    
                    // Parssing response data
                    if let responseHandler = Mapper<UserRoamingStatus>().map(JSONObject: resultData){
                        
                        UserDefaults.standard.saveBoolForKey(boolValue: responseHandler.roamingEnabled, forKey: Constants.K_IsRoamingEnabled)
                        self.roamingServiceSwitch.setOn(responseHandler.roamingEnabled, animated: true)
                        
                    } else {
                        // Show Error alert
//                        self.showErrorAlertWithMessage(message: resultDesc)
                        
                    }
                } else {
                    
                    //EventLog
                    MBAnalyticsManager.logEvent(screenName: "Roaming Screen", contentType:"Roaming Main Screen" , status:"Get Roaming Status Failure" )
                    
                    // Show Error alert
//                    self.showErrorAlertWithMessage(message: resultDesc)
                    
                }
            }
        })
    }
    
    func getMySubscriptions() {
        
//        activityIndicator.showActivityIndicator()
        
        _ = MBAPIClient.sharedClient.getSubscriptions({ (response, resultData, error, isCancelled, status, resultCode, resultDesc)  in
            
//            self.activityIndicator.hideActivityIndicator()
            if error != nil {
                
            } else {
                // handling data from API response.
                if resultCode == Constants.MBAPIStatusCode.succes.rawValue {
                    
                    // Parssing response data
                    if let mySubscriptionOffersHandler = Mapper<MySubscription>().map(JSONObject: resultData){
                        
                        // set data in user session
                        MBUserSession.shared.mySubscriptionOffers = mySubscriptionOffersHandler
                        
                    }
                }
            }
        })
    }
    
    func updateroamingSwitch (withActionType: String) {
        if withActionType == "2" {
            self.roamingServiceSwitch.setOn(true, animated: true)
        } else {
            self.roamingServiceSwitch.setOn(false, animated: true)
        }
    }
}
