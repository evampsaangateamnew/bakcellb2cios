//
//  UnusedCodesPopUpVC.swift
//  Bakcell
//
//  Created by Muhammad Waqas on 1/4/19.
//  Copyright © 2019 evampsaanga. All rights reserved.
//

import UIKit

class UnusedCodesPopUpVC: BaseVC {
    
    //MARK:- Properties
    var unusedCodes : [UlduzumUnusedCodes]?
    
    //MARK:- IBOutlet
    @IBOutlet var tableView: UITableView!
    
    @IBOutlet var viewHeader: UIView!
    @IBOutlet var unusedCodeTitleLabel: UILabel!
    @IBOutlet var insertDateTitleLabel: UILabel!
    @IBOutlet var stackViewHeader: UIStackView!
    
    @IBOutlet var BtnOK: UIButton!
   
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //checking if array have data than show header labels else hide them
        if ((self.unusedCodes?.count ?? 0) > 0) {
            self.viewHeader.isHidden = false
        } else {
            self.viewHeader.isHidden = true
        }
        
        unusedCodeTitleLabel.text = Localized("Title_UnusedCode")
        insertDateTitleLabel.text = Localized("Title_UnusedDate")
        BtnOK.setTitle(Localized("BtnTitle_OK"), for: .normal)
        
        //init tableview
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(UINib(nibName: "UnusedcodesTableViewCell", bundle: nil), forCellReuseIdentifier: "unusedCodeCell")
        tableView.rowHeight = UITableView.automaticDimension
        self.tableView.estimatedRowHeight = 32.0;
        
        self.reloadTableViewData()
    }
    
    //MARK: - FUNCTIONS
    
    func reloadTableViewData() {
        if self.unusedCodes?.count == 0 {
            self.tableView.showDescriptionViewWithImage(description: Localized("Message_NoDataAvalible"))
            
        } else {
            self.tableView.hideDescriptionView()
        }
        self.tableView.reloadData()
    }
    
    
    // MARK: - IBActions
    
    @IBAction func okPressed(_ sender: UIButton) {
        
        self.dismiss(animated: true) {
            
        }
    }
}

// MARK: - <UITableViewDataSource> / <UITableViewDelegate>
extension UnusedCodesPopUpVC : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return unusedCodes?.count ?? 0
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableView.automaticDimension
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "unusedCodeCell", for: indexPath) as! UnusedcodesTableViewCell
        
        //getting object and assigning values
        if let dataObj: UlduzumUnusedCodes = unusedCodes?[indexPath.row] {
            cell.lblUnusedCode.text = dataObj.identicalCode
            cell.lblInsertDate.text = dataObj.insertDate
        }
        
        return cell
    }
    
}
