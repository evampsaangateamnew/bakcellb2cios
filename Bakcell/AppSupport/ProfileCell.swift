//
//  ProfileCell.swift
//  Bakcell
//
//  Created by Saad Riaz on 7/3/17.
//  Copyright © 2017 evampsaanga. All rights reserved.
//

import UIKit
import MarqueeLabel

class ProfileCell: UITableViewCell {

    @IBOutlet var title_lbl: MarqueeLabel!
    @IBOutlet var imageView1: UIImageView!
    @IBOutlet var cell2_lbl: UILabel!
    @IBOutlet var description_lbl: UILabel!
    @IBOutlet var arrow_img: UIImageView!
    @IBOutlet var LineView: UIView!
    @IBOutlet var arrowWidthConstraint: NSLayoutConstraint!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        title_lbl.setupMarqueeAnimation()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}
