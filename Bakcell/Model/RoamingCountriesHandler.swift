//
//  RoamingCountriesHandler.swift
//  Bakcell
//
//  Created by Muhammad Waqas on 1/11/19.
//  Copyright © 2019 evampsaanga. All rights reserved.
//

import Foundation
import ObjectMapper

class RoamingCountriesHandler : Mappable, Copyable {
    
    var countryList : [CountryList]?
    var detailList : [CountryDetailList]?
    
    required init(instance: RoamingCountriesHandler) {
        self.countryList = instance.countryList
        self.detailList = instance.detailList
    }
    
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        
        countryList           <- map["countryList"]
        detailList            <- map["detailList"]
    }
}

class CountryList : Mappable, Copyable {
    
    var countryName : String?
    var countryID : String?
    
    required init(instance: CountryList) {
        self.countryName = instance.countryName
        self.countryID = instance.countryID
    }
    
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        
        countryName         <- map["country"]
        countryID           <- map["country_id"]
    }
}

class CountryDetailList : Mappable, Copyable {
    
    var countryName : String?
    var countryID : String?
    var operatorList : [CountryOperator]?
    
    required init(instance: CountryDetailList) {
        self.countryName = instance.countryName
        self.countryID = instance.countryID
        self.operatorList = instance.operatorList
    }
    
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        
        countryName       <- map["country"]
        countryID         <- map["country_id"]
        operatorList      <- map["operator_list"]
    }
}

class CountryOperator : Mappable, Copyable {
    
    var name : String?
    var servicesList : [OperatorService]?
    
    required init(instance: CountryOperator) {
        self.name = instance.name
        self.servicesList = instance.servicesList
    }
    
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        
        name            <- map["operator"]
        servicesList    <- map["service_list"]
    }
}

class OperatorService : Mappable, Copyable {
    
    var name : String?
    var unit : String?
    var types : [OperatorServiceType]?
    
    required init(instance: OperatorService) {
        self.name = instance.name
        self.unit = instance.unit
        self.types = instance.types
    }
    
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        
        name            <- map["service"]
        unit            <- map["service_unit"]
        types           <- map["service_type_list"]
    }
}

class OperatorServiceType : Mappable {
    
    var type : String?
    var value : String?
    var unit : String?
    
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        
        type            <- map["service_type"]
        value           <- map["value"]
        unit            <- map["unit"]
    }
}
