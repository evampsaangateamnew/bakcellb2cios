//
//  UsageSummaryMainVC.swift
//  Bakcell
//
//  Created by Saad Riaz on 6/15/17.
//  Copyright © 2017 evampsaanga. All rights reserved.
//

import UIKit

import ObjectMapper

class UsageSummaryMainVC: BaseVC {
    
    //MARK:- Properties
    var isStartDate : Bool?
    var usageSummaryHistoryResponse : UsageSummaryHistoryHandler?
    
    //Uidate picker
    let datePicker = UIDatePicker()
    let myView = UIView()
    var selectedSectionIndexs: [Int] = []
    
    //MARK:- IBOutlet
    @IBOutlet var dateDropDown: UIDropDown!
    @IBOutlet var usageTableVIew: MBAccordionTableView!
    @IBOutlet var pageCount: UILabel!

    @IBOutlet var spacificBtnView: UIView!
    @IBOutlet var endDateBtn: UIButton!
    @IBOutlet var startDateBtn: UIButton!
    @IBOutlet var startDateLb: UILabel!
    @IBOutlet var endDateLb: UILabel!
    @IBOutlet var usageType_lbl: UILabel!
    @IBOutlet var usage_lbl: UILabel!
    @IBOutlet var charged_lbl: UILabel!
    
    
    
 //MARK: - View Controller Methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        usageTableVIew.allowMultipleSectionsOpen = true
        usageTableVIew.delegate = self
        usageTableVIew.dataSource = self
        reloadTableViewData()
        
        // Hidding counter because there is no counter in design on usage summery
        pageCount.isHidden = true
        
        usageTableVIew.register(UINib(nibName: "UsageSummaryExpandCell", bundle: nil), forCellReuseIdentifier: "UsageSummaryExpandCell")
        usageTableVIew.register(UINib(nibName: "UssageSummaryHeader", bundle: nil), forHeaderFooterViewReuseIdentifier: UssageSummaryHeader.kAccordionHeaderViewReuseIdentifier)
        
        spacificBtnView.isHidden = true
        
        let todayDateString = MBUtilities.todayDateString(dateFormate: Constants.kAPIFormat)
        // Load current day history
        self.getUsageSummaryHistory(startDate: todayDateString, endDate: todayDateString)
        
        //Drop Down
        dateDropDown.optionsTextAlignment = NSTextAlignment.center
        dateDropDown.textAlignment = NSTextAlignment.center
        dateDropDown.placeholder = Localized("DropDown_CurrentDay")
        self.dateDropDown.rowBackgroundColor = UIColor.clear
        self.dateDropDown.borderWidth = 0
        self.dateDropDown.tableHeight = 176
        
        dateDropDown.optionsSize = 14
        dateDropDown.fontSize = 12
        
        dateDropDown.options = [Localized("DropDown_CurrentDay"),Localized("DropDown_Last7Days"), Localized("DropDown_Last30Days"), Localized("DropDown_PreviousMonth"),Localized("DropDown_CustomPeriod")]
        
        
        dateDropDown.didSelect { (option, index) in
            
            
            switch index{
            case 0:
                print("default")
                self.spacificBtnView.isHidden = true
                
                self.getUsageSummaryHistory(startDate: todayDateString, endDate: todayDateString)
                
            case 1:
                print("Last 7 days")
                self.spacificBtnView.isHidden = true
                
                // Get data of previous seven days
                let sevenDaysAgo = MBUtilities.todayDateString(withAdditionalValue: -7)
                self.getUsageSummaryHistory(startDate: sevenDaysAgo, endDate: todayDateString)
                
                
            case 2:
                print("Last 30 days")
                self.spacificBtnView.isHidden = true
                
                // Get data of previous thirty days
                let thirtyDaysAgo = MBUtilities.todayDateString(withAdditionalValue: -30)
                self.getUsageSummaryHistory(startDate: thirtyDaysAgo, endDate: todayDateString)
                
            case 3:
                print("Previous month")
                self.spacificBtnView.isHidden = true
                
                // Getting previous month start and end date
                let startOfPreviousMonth = MBUtilities.todayDate().getPreviousMonth()?.startOfMonth() ?? MBUtilities.todayDate()
                let endOfPreviousMonth = MBUtilities.todayDate().getPreviousMonth()?.endOfMonth() ?? MBUtilities.todayDate()
                
                MBUserSession.shared.dateFormatter.dateFormat = Constants.kAPIFormat
                
                let startDateString = MBUserSession.shared.dateFormatter.string(from: startOfPreviousMonth)
                let endDateString = MBUserSession.shared.dateFormatter.string(from: endOfPreviousMonth)
                
                self.getUsageSummaryHistory(startDate: startDateString, endDate: endDateString )
                
            case 4:
                self.spacificBtnView.isHidden = false
                
                //For date formate
                self.startDateLb.text = MBUtilities.todayDateString(dateFormate: Constants.kDisplayFormat)
                self.endDateLb.text = MBUtilities.todayDateString(dateFormate: Constants.kDisplayFormat)
                
                self.startDateLb.textColor = UIColor.MBDarkGrayColor
                self.endDateLb.textColor = UIColor.MBDarkGrayColor
                
                self.getUsageSummaryHistory(startDate: todayDateString, endDate: todayDateString)
                
                print("this Month Days")
                
            default:
                print("destructive")
                
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.interactivePop(true)
        
        loadViewLayout()
    }
    
    // MARK: - IBAction
    
    @IBAction func startBtnAction(_ sender: UIButton) {
        
        isStartDate = true;
        
        let todayDate = MBUtilities.todayDate()
        MBUserSession.shared.dateFormatter.dateFormat = Constants.kDisplayFormat
         
        let threeMonthsAgos = MBUtilities.todayDate().getMonth(WithAdditionalValue: -3)?.startOfMonth() ?? todayDate
        
        if let endDateString = endDateLb.text,
            endDateString.isBlank == false {
            
            self.showDatePicker(minDate: threeMonthsAgos, maxDate: MBUserSession.shared.dateFormatter.date(from: endDateString) ?? todayDate)
            
        } else {
            self.showDatePicker(minDate: threeMonthsAgos, maxDate: todayDate)
        }
        
    }
    
    @IBAction func endBtnAction(_ sender: UIButton) {
        
        isStartDate = false;
        
        let todayDate = MBUtilities.todayDate(dateFormate: Constants.kDisplayFormat)
        
        MBUserSession.shared.dateFormatter.dateFormat = Constants.kDisplayFormat
        
        self.showDatePicker(minDate: MBUserSession.shared.dateFormatter.date(from: startDateLb.text ?? "") ?? todayDate, maxDate: todayDate)
    }
    
    //MARK: - FUNCTIONS
    func loadViewLayout(){
        usageType_lbl.text = Localized("str_UserType")
        usage_lbl.text = Localized("str_Usage")
        charged_lbl.text = Localized("str_Charged")
    }
    
    func showDatePicker(minDate : Date , maxDate : Date){
        //Formate Date
        datePicker.datePickerMode = .date
        
        //ToolBar
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        
        //done button & cancel button
        let doneButton = UIBarButtonItem(title: Localized("Btn_title_Done"), style: UIBarButtonItem.Style.done, target: self, action: #selector(UsageSummaryMainVC.doneDatePicker))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: Localized("Btn_title_Cancel"), style: UIBarButtonItem.Style.plain, target: self, action: #selector(UsageSummaryMainVC.cancelDatePicker))
        toolbar.setItems([doneButton,spaceButton,cancelButton], animated: false)
        
        
        var  currentLanguageLocale = Locale(identifier:"en")
        switch MBLanguageManager.userSelectedLanguage() {
            
        case .Russian:
            currentLanguageLocale = Locale(identifier:"ru")
        case .English:
            currentLanguageLocale = Locale(identifier:"en")
        case .Azeri:
            currentLanguageLocale = Locale(identifier:"az_AZ")
        }
        
        self.datePicker.calendar.locale = currentLanguageLocale
        self.datePicker.locale = currentLanguageLocale
        
        datePicker.minimumDate = minDate
        datePicker.maximumDate = maxDate
        datePicker.timeZone =  TimeZone.current
        
        datePicker.backgroundColor = UIColor.lightGray
        
        if #available(iOS 13.4, *) {
            datePicker.preferredDatePickerStyle = .wheels
            toolbar.frame = CGRect.init(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 40)
        }
        
        datePicker.frame = CGRect(x: 0, y: 40, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height * 0.4)
        
        myView.frame = CGRect(x: 0, y:((UIScreen.main.bounds.height * 0.6) - 40), width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height * 0.4)
        
        if #available(iOS 13.4, *) {
            datePicker.alignmentRect(forFrame: myView.frame)
        }
        
        myView .addSubview(toolbar)
        myView .addSubview(datePicker)
        myView.backgroundColor = UIColor.white
        
        self.view.addSubview(myView)
        
    }
    
    @objc func doneDatePicker() {
        //For date formate
        let todayDate = MBUtilities.todayDate(dateFormate: Constants.kDisplayFormat)
        
        MBUserSession.shared.dateFormatter.dateFormat = Constants.kDisplayFormat
        MBUserSession.shared.dateFormatter.timeZone = TimeZone.current
        
        let selectedDate = MBUserSession.shared.dateFormatter.string(from: datePicker.date)
        
        let startFieldDate = MBUserSession.shared.dateFormatter.date(from: startDateLb.text ?? "")
        let endFieldDate = MBUserSession.shared.dateFormatter.date(from: endDateLb.text ?? "")
        
        //dismiss date picker dialog
        self.view.endEditing(true)
        myView .removeFromSuperview()
        
        MBUserSession.shared.dateFormatter.dateFormat = Constants.kAPIFormat
        
        if isStartDate ?? true {
            startDateLb.text = selectedDate
            self.startDateLb.textColor = UIColor.MBBarOrange
            
            self.getUsageSummaryHistory(startDate: MBUserSession.shared.dateFormatter.string(from: datePicker.date), endDate: MBUserSession.shared.dateFormatter.string(from:endFieldDate ?? todayDate))
            
        } else {
            
            endDateLb.text = selectedDate
            self.endDateLb.textColor = UIColor.MBBarOrange
            
            self.getUsageSummaryHistory(startDate: MBUserSession.shared.dateFormatter.string(from: startFieldDate ?? todayDate), endDate: MBUserSession.shared.dateFormatter.string(from: datePicker.date))
        }
    }
    
    @objc func cancelDatePicker(){
        //cancel button dismiss datepicker dialog
        self.view.endEditing(true)
        
        myView .removeFromSuperview()
        print("cancel")
    }
    
    
    func reloadTableViewData() {
        if usageSummaryHistoryResponse?.summaryList?.count ?? 0 <= 0 {
            self.usageTableVIew.showDescriptionViewWithImage(description: Localized("Message_NoDataAvalible"))
        } else {
            self.usageTableVIew.hideDescriptionView()

        }
        self.usageTableVIew.reloadData()
        self.showPageNumber()
    }
}

// MARK: - <UITableViewDataSource> / <UITableViewDelegate> -

extension UsageSummaryMainVC : UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return (usageSummaryHistoryResponse?.summaryList?.count) ?? 0
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (usageSummaryHistoryResponse?.summaryList?[section].records?.count)!
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 53.0;
    }
    
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 52.0
        
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: UssageSummaryHeader.kAccordionHeaderViewReuseIdentifier) as! UssageSummaryHeader
        
        if let aSummaryList = usageSummaryHistoryResponse?.summaryList?[section] {
            
            headerView.usageType.text = aSummaryList.name
            headerView.usage.text = aSummaryList.totalUsage
            headerView.charedLb.text = aSummaryList.totalCharge
            
            if aSummaryList.totalCharge.isBlank == false && aSummaryList.totalCharge.isStringAnNumber() == true {
                
                headerView.manatSignLb.isHidden = false
                headerView.manatSignWidthConstraint.constant = 10
            } else {
                headerView.manatSignLb.isHidden = true
                headerView.manatSignWidthConstraint.constant = 0
            }
            // Check if section is expanded then set expaned image else set unexpended image
            if selectedSectionIndexs.contains(section) {
                headerView.plusImage.image  = UIImage.init(named: "circleminus")
            } else {
                headerView.plusImage.image = UIImage.init(named: "pluspressed")
            }
            
        } else {
            
            headerView.usageType.text = ""
            headerView.usage.text = ""
            headerView.charedLb.text = ""
            
            headerView.manatSignLb.isHidden = true
        }
        
        return headerView
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "UsageSummaryExpandCell", for: indexPath) as! UsageSummaryExpandCell

        if let aRecordDetail = usageSummaryHistoryResponse?.summaryList?[indexPath.section].records?[indexPath.row] {

            cell.label1.text = aRecordDetail.service_type
            cell.label2.text = aRecordDetail.total_usage

//            if aRecordDetail.unit.lowercased() == Localized("str_Voice") || aRecordDetail.unit.lowercased() == Localized("str_SMS") {
//                cell.label3.text = aRecordDetail.chargeable_amount.appending(" ₼")
//            } else {
//                cell.label3.text = aRecordDetail.chargeable_amount.appending(" ").appending(aRecordDetail.unit)
//            }

            cell.label3.text = aRecordDetail.chargeable_amount

            return cell

        } else {
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        // tableView.deselectRow(at: indexPath, animated: true)
        
    }
    
    //MARK: - API Calls
    /// Call 'getUsageSummaryHistory' API .
    ///
    /// - returns: SummaryHistory
    func getUsageSummaryHistory(startDate:String?, endDate:String?) {
        
        activityIndicator.showActivityIndicator()
        
        _ = MBAPIClient.sharedClient.getUsageSummaryHistory(StartDate: startDate ?? "", EndDate: endDate ?? "",{ (response, resultData, error, isCancelled, status, resultCode, resultDesc) in
            
            self.activityIndicator.hideActivityIndicator()
            if error != nil {
                //EventLog
                MBAnalyticsManager.logEvent(screenName: "My Account", contentType:"Usage History Screen" , status:"Usage History Summary Screen" )
                
                error?.showServerErrorInViewController(self)
                
            } else {
                // handling data from API response.
                //EventLog
                MBAnalyticsManager.logEvent(screenName: "My Account", contentType:"Usage History Screen" , status:"Usage History Summary Screen" )
                
                if resultCode == Constants.MBAPIStatusCode.succes.rawValue {
                    
                    // Parssing response data
                    if let usageHistoryList = Mapper<UsageSummaryHistoryHandler>().map(JSONObject: resultData){
                        
                        self.usageSummaryHistoryResponse = usageHistoryList
                        self.pageCount.text = "\(self.usageSummaryHistoryResponse?.summaryList?.count ?? 0) / \(self.usageSummaryHistoryResponse?.summaryList?.count ?? 0)"
                    }
                } else {
                    //EventLog
                    MBAnalyticsManager.logEvent(screenName: "My Account", contentType:"Usage History Screen" , status:"Usage History Summary Screen" )
                    
                    // Show error alert to user
                    self.showErrorAlertWithMessage(message: resultDesc)
                }
            }
            
            self.reloadTableViewData()
        })
    }
    
}

// MARK: - <MBAccordionTableViewDelegate>

extension UsageSummaryMainVC : MBAccordionTableViewDelegate {
    
    
    func tableView(_ tableView: MBAccordionTableView, didOpenSection section: Int, withHeader header: UITableViewHeaderFooterView?) {
        
        if let headerView = header as? UssageSummaryHeader {
            
            headerView.plusImage.image  = UIImage(named: "circleminus")
            showPageNumber()
            
            // Adding section from selectedSectionIndexs
            selectedSectionIndexs.append(section)
        }
    }
    
    func tableView(_ tableView: MBAccordionTableView, didCloseSection section: Int, withHeader header: UITableViewHeaderFooterView?) {
        
        
        if let headerView = header as? UssageSummaryHeader {
            headerView.plusImage.image = UIImage(named: "pluspressed")
            
            showPageNumber()
            
            // Removing section from selectedSectionIndexs
            if let sectionValue = selectedSectionIndexs.firstIndex(of: section) {
                selectedSectionIndexs.remove(at: sectionValue)
            }
        }
    }
    
    func tableView(_ tableView: MBAccordionTableView, canInteractWithHeaderAtSection section: Int) -> Bool {
        return true
    }
}

extension UsageSummaryMainVC:UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        showPageNumber()
    }
    
    func showPageNumber() {
        
        pageCount.text = "\(usageTableVIew.lastVisibleSection() )/\(usageSummaryHistoryResponse?.summaryList?.count ?? 0)"
    }
}
