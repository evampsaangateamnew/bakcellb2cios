//
//  UlduzumStoreListVC.swift
//  Bakcell
//
//  Created by AbdulRehman Warraich on 1/2/19.
//  Copyright © 2019 evampsaanga. All rights reserved.
//

import UIKit

class UlduzumStoreListVC: BaseVC {
    
    //MARK:- Properties
    var storeLocatorData : UlduzumStoreLocator?
    var storeLocatorFilter : [UlduzumStoreLocation]?
    var storeLocatorSorted : [[UlduzumStoreLocation]]?
    
    var selectedType : String = Localized("DropDown_All")
    
    //MARK:- IBOutlet
    @IBOutlet var storeCenterDrop: UIDropDown!
    @IBOutlet var tableView: UITableView!
    
    //MARK: - ViewController Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        storeLocatorFilter = storeLocatorData?.storesList?.copy()
        setUPDropBox()
        
        tableView.delegate = self
        tableView.dataSource = self
        
        tableView.register(UINib(nibName: "UlduzumStoreListTableViewCell", bundle: nil), forCellReuseIdentifier: "storeListCellUL")
        tableView.register(UINib(nibName: "UlduzumStoreListHeaderTableViewCell", bundle: nil), forCellReuseIdentifier: "storeListHeaderCellUL")
        
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 58.0;
        
        //reloading table's data
        self.reloadTableViewData()
        
    }
    
    //MARK: - FUNCTIONS
    
    func reloadTableViewData() {
        if self.storeLocatorSorted?.count == 0 {
            self.tableView.showDescriptionViewWithImage(description: Localized("Message_NoDataAvalible"))
            
        } else {
            self.tableView.hideDescriptionView()
        }
        self.tableView.reloadData()
    }
    
    func setUPDropBox() {
        
        // set storeCenterDrop
        storeCenterDrop.layer.borderWidth = 0
        storeCenterDrop.textAlignment = NSTextAlignment .center
        storeCenterDrop.placeholder = Localized("DropDown_All")
        storeCenterDrop.rowBackgroundColor = UIColor.MBDimLightGrayColor
        storeCenterDrop.setFont = UIFont.MBArial(fontSize: 12)
        
        var optionsArray : [String] = [Localized("DropDown_All")]
        
        if let selectedCenter = storeLocatorData?.storesCategories {
            for center in selectedCenter{
                optionsArray.append(center.categoryName)
            }
        }
        
        if optionsArray.count < 10 {
            storeCenterDrop.tableHeight = CGFloat(35 * optionsArray.count)
        } else {
            storeCenterDrop.tableHeight = 325
        }
        
        storeCenterDrop.options = optionsArray
        
        //calling method to first sort data alphabetically
        self.sortFilteredLocations(categoryName: "", categoryID: nil, StoreDetails: self.storeLocatorData?.storesList?.copy())
        
        storeCenterDrop.didSelect { (option, index) in
            
            self.selectedType = option
            
            if let tappedObject : UlduzumStoreCategory = (index == 0) ? nil :
                self.storeLocatorData?.storesCategories?[index-1] {
                
                //calling method to first sort data alphabetically
                self.sortFilteredLocations(categoryName: option, categoryID: tappedObject.categoryID.toInt, StoreDetails: self.storeLocatorData?.storesList?.copy())
            } else {
                //calling method to first sort data alphabetically
                self.sortFilteredLocations(categoryName: "", categoryID: nil, StoreDetails: self.storeLocatorData?.storesList?.copy())
            }
        }
    }
    
    //Filter on id
    func filterStoreBy(categoryName:String?, categoryID:Int?, StoreDetails : [UlduzumStoreLocation]? ) -> [UlduzumStoreLocation]? {
        
        guard categoryID != nil else {
            return StoreDetails
        }
        
        guard let StoreListDetails = StoreDetails else {
            return nil
        }
        
        if(categoryName?.isEqualToStringIgnoreCase(otherString: Localized("DropDown_All")) == true) {
            
            return StoreDetails
            
        } else if (categoryName?.isEqualToStringIgnoreCase(otherString: Localized("DropDown_All")) == false){
            
            let  filterdStoreDetails = StoreListDetails.filter() {
                
                if ($0 as UlduzumStoreLocation).categoryID.toInt == categoryID {
                    return true
                } else {
                    return false
                }
            }
            return filterdStoreDetails
            
        } else {
            return StoreDetails
        }
    }
    
    func sortFilteredLocations(categoryName:String?, categoryID:Int?, StoreDetails : [UlduzumStoreLocation]?) {
        
        //getting filtered data of specific category
        self.storeLocatorFilter? = self.filterStoreBy(categoryName:categoryName, categoryID:categoryID , StoreDetails: self.storeLocatorData?.storesList?.copy())!
        
        //getting filtered array alphabetically
        let sortedLocations = self.storeLocatorFilter?.sorted { $0.name < $1.name }
        self.storeLocatorFilter = sortedLocations?.copy()
        
        var tempArrayL : [[UlduzumStoreLocation]]? = []
        
        for oneChar in Array("0123456789abcdefghijklmnopqrstuvwxyz") {
            let filterdStoreDetails = self.storeLocatorFilter?.filter() {
                
                if (($0 as UlduzumStoreLocation).name.isNumeric) {
                    //alphanumeric char
                    if ($0 as UlduzumStoreLocation).name.hasPrefix(String(oneChar)) {
                        return true
                    }
                    
                } else if ($0 as UlduzumStoreLocation).name.lowercased().hasPrefix(String(oneChar)) {
                    return true
                }
                
                return false
            }
            if (filterdStoreDetails?.count ?? 0) > 0 {
                tempArrayL?.append(filterdStoreDetails ?? [])
            }
        }
        
        self.storeLocatorSorted = tempArrayL
        
        //reloading table's data
        self.reloadTableViewData()
    }
}

// MARK: - <UITableViewDataSource> / <UITableViewDelegate>
extension UlduzumStoreListVC : UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.storeLocatorSorted?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40.0
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        if let headerCell = tableView.dequeueReusableCell(withIdentifier: "storeListHeaderCellUL") as? UlduzumStoreListHeaderTableViewCell {
            
            if let aStoreInfo = self.storeLocatorSorted?[section].first {
                headerCell.lblHeader.text = aStoreInfo.name.firstCharacter.uppercased()
            } else {
                headerCell.lblHeader.text = ""
            }
            return headerCell
        }
        return UITableViewHeaderFooterView()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.storeLocatorSorted?[section].count ?? 0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let cell = tableView.dequeueReusableCell(withIdentifier: "storeListCellUL", for: indexPath) as? UlduzumStoreListTableViewCell {
            
            if let aStoreInfo = self.storeLocatorSorted?[indexPath.section][indexPath.row] {
                
                cell.lblStoreName.text = aStoreInfo.name
                cell.lblStoredesc.text = aStoreInfo.categoryName
                
                if aStoreInfo.loyalitySegment.isNumeric {
                    cell.ivIcon.isHidden = true
                    cell.lblStorePercentage.isHidden = false
                    cell.lblStorePercentage.text = "\(aStoreInfo.loyalitySegment)%"
                } else {
                    cell.ivIcon.isHidden = false
                    cell.lblStorePercentage.isHidden = true
                    cell.lblStorePercentage.text = ""
                }
            } else {
                cell.ivIcon.isHidden = true
                cell.lblStorePercentage.isHidden = true
                cell.lblStorePercentage.text = ""
            }
            return cell
        }
        return UITableViewCell()
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let aStoreInfo = self.storeLocatorSorted?[indexPath.section][indexPath.row] {
            if let ulduzumStoreDetailVC = self.myStoryBoard.instantiateViewController(withIdentifier: "UiduzumStoreDetailVC") as? UiduzumStoreDetailVC {
                ulduzumStoreDetailVC.storeDetail = aStoreInfo
                self.navigationController?.pushViewController(ulduzumStoreDetailVC, animated: true)
            }
        }
    }
    
}
