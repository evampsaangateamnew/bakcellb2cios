//
//  MyInstallmentsCell.swift
//  Bakcell
//
//  Created by Saad Riaz on 6/12/17.
//  Copyright © 2017 evampsaanga. All rights reserved.
//

import UIKit

class MyInstallmentsCell: UITableViewCell {

    //View6
    @IBOutlet var date_lbl: UILabel!
    @IBOutlet var purchase_lbl: UILabel!
    //View5
    @IBOutlet var remainingPeriod_lbl: UILabel!
    @IBOutlet var pro3EndDate_lbl: UILabel!
    @IBOutlet var pro3BeginDate_lbl: UILabel!
    @IBOutlet var pro3RemaingPeriod_lbl: UILabel!
    @IBOutlet var progress3: MBGradientProgressView!
    //View4
    @IBOutlet var remainingAmount_lbl: UILabel!
    @IBOutlet var pro2Date_lbl: UILabel!
    @IBOutlet var pro2RemainingAmount_lbl: UILabel!
    @IBOutlet var progress2: MBGradientProgressView!
    //View3
    @IBOutlet var nextPayment_lbl: UILabel!
    @IBOutlet var pro1Date_lbl: UILabel!
    @IBOutlet var pro1Days_lbl: UILabel!
    @IBOutlet var progress1: MBGradientProgressView!
    //View2
    @IBOutlet var amount_lbl: UILabel!
    @IBOutlet var view2Amount_lbl: UILabel!
    //View1
    @IBOutlet var view1Title_lbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        
        // Configure the view for the selected state
    }
    
    func setUpInstallments(installmentsData : Installment?) {
        
        if let aInstallmentData = installmentsData {
            var  progress = 0.0
            
            //view 1
            view1Title_lbl.text = aInstallmentData.name
            
            //View 2
            amount_lbl.text = aInstallmentData.amountValue
            view2Amount_lbl.text = aInstallmentData.amountLabel
            
            //View 3 (Next paymentView)
            nextPayment_lbl.text = aInstallmentData.nextPaymentLabel

            let computedValuesFromDates = MBUtilities.calculateProgressValuesForInstallment(from: aInstallmentData.nextPaymentInitialDate, to: aInstallmentData.nextPaymentValue)

            pro1Date_lbl.text = computedValuesFromDates.endDate

            // set days left
            pro1Days_lbl.text = computedValuesFromDates.daysLeftDisplayValue
            if computedValuesFromDates.daysLeft < aInstallmentData.installmentFeeLimit.toInt {
                
                pro1Days_lbl.textColor = UIColor.MBRedColor
            } else {
                
                pro1Days_lbl.textColor = UIColor.black
            }

            self.progress1.setProgress(computedValuesFromDates.progressValue, animated: true)

            
            //View 4
            remainingAmount_lbl.text = aInstallmentData.remainingAmountLabel
            pro2Date_lbl.isHidden = true
            pro2RemainingAmount_lbl.text = "\(aInstallmentData.remainingAmountCurrentValue)₼ / \(aInstallmentData.remainingAmountTotalValue)₼"
            
            progress = (aInstallmentData.remainingAmountTotalValue.toDouble - aInstallmentData.remainingAmountCurrentValue.toDouble) / aInstallmentData.remainingAmountTotalValue.toDouble
            progress2.setProgress(Float(progress), animated: true)
            
            //View5

            // Begin Date
            MBUserSession.shared.dateFormatter.dateFormat = Constants.kHomeDateFormate
            if let remainingPeriodStartDate  = MBUserSession.shared.dateFormatter.date(from: aInstallmentData.remainingPeriodBeginDateValue) {

                MBUserSession.shared.dateFormatter.dateFormat = Constants.kDisplayFormat
                pro3BeginDate_lbl.text = "\(aInstallmentData.remainingPeriodBeginDateLabel): \(MBUserSession.shared.dateFormatter.string(from: remainingPeriodStartDate) )"

            } else {
                pro3BeginDate_lbl.text = ""
            }

            // End Date
            MBUserSession.shared.dateFormatter.dateFormat = Constants.kHomeDateFormate
            if let remainingPeriodEndDate  = MBUserSession.shared.dateFormatter.date(from: aInstallmentData.remainingPeriodEndDateValue) {

                MBUserSession.shared.dateFormatter.dateFormat = Constants.kDisplayFormat

                pro3EndDate_lbl.text = "\(aInstallmentData.remainingPeriodEndDateLabel): \(MBUserSession.shared.dateFormatter.string(from: remainingPeriodEndDate) )"
            } else {
                pro3EndDate_lbl.text = ""
            }


            
            remainingPeriod_lbl.text = aInstallmentData.remainingPeriodLabel
            pro3RemaingPeriod_lbl.text = "\(aInstallmentData.remainingCurrentPeriod)/\(aInstallmentData.remainingTotalPeriod)"

            progress = (aInstallmentData.remainingTotalPeriod.toDouble - aInstallmentData.remainingCurrentPeriod.toDouble) / aInstallmentData.remainingTotalPeriod.toDouble
            progress3.setProgress(Float(progress), animated: true)
            
            //view 6
            MBUserSession.shared.dateFormatter.dateFormat = Constants.kHomeDateFormate
            if let purchaseDate = MBUserSession.shared.dateFormatter.date(from: aInstallmentData.purchaseDateValue) {
                MBUserSession.shared.dateFormatter.dateFormat = Constants.kDisplayFormat

                date_lbl.text = "\(MBUserSession.shared.dateFormatter.string(from: purchaseDate) )"
            } else {
                date_lbl.text = ""
            }
            purchase_lbl.text = "\(aInstallmentData.purchaseDateLabel):"
            
        }

        MBUserSession.shared.dateFormatter.dateFormat = Constants.kHomeDateFormate
    }
}
