//
//  VerifyAppVersion.swift
//  Bakcell
//
//  Created by AbdulRehman Warraich on 10/20/17.
//  Copyright © 2017 evampsaanga. All rights reserved.
//

import Foundation
import ObjectMapper

class VerifyAppVersion : Mappable {
    var message :  String       = ""
    var updatedToken :  String = ""
    var timeStamps : [TimeStamps]?

    /*"data": {
    "message": "New version of app is available.",
    "updatedToken": "1v95VnaIw4B7RU6Q5taW28tix49xehOneHKH%2BTy%2BRwixxqI8muzjwT Af1uMO5rvo%2BCwIrR5MoO7E%0D%0Atuf14tr%2FwwW%2F%2FTvOOnL ReEyyZwjd0wyKGvSaO2dF5ovs8ew4bnC06QpjSZgUpqs%3D",
     "timeStamps": [ {}]}*/
    init() {
    }
    required init?(map: Map) {
    }
    func mapping(map: Map) {

        message         <- map["message"]
        updatedToken    <- map["updatedToken"]
        timeStamps      <- map["timeStamps"]
    }
}


class TimeStamps : Mappable {
    var cacheType :  String = ""
    var timeStamp :  String = ""

    //timeStamps": [ {"cacheType":"timeStamp": }]

    init() {
    }
    required init?(map: Map) {
    }
    func mapping(map: Map) {

        cacheType       <- map["cacheType"]
        timeStamp       <- map["timeStamp"]
    }
}
