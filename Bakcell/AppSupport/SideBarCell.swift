//
//  SideBarCell.swift
//  Bakcell
//
//  Created by Saad Riaz on 5/17/17.
//  Copyright © 2017 evampsaanga. All rights reserved.
//

import UIKit

class SideBarCell: UITableViewCell {

    @IBOutlet var sideBarImgView: UIImageView!
    @IBOutlet var sideBarLbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
