//
//  SettingsVC.swift
//  Bakcell
//
//  Created by Saad Riaz on 6/19/17.
//  Copyright © 2017 evampsaanga. All rights reserved.
//

import UIKit
import FirebaseMessaging
import ObjectMapper

class SettingsVC: BaseVC {

    // MARK: - Properties
    var ringingType : Constants.MBNotificationSoundType = .Tone
    var isNotificationEnabled : Bool = false

    // MARK: - IBOutlet
    @IBOutlet var title_lbl: UILabel!
    @IBOutlet var changeLanguageTitle_lbl: UILabel!
    @IBOutlet var language_lbl: UILabel!
    @IBOutlet var notificationTitle_lbl: UILabel!
    @IBOutlet var notificationDiscription_lbl: UILabel!
    @IBOutlet var notificationSettingTitle_lbl: UILabel!
    @IBOutlet var notificationSettingTune_lbl: UILabel!
    @IBOutlet var notificationSettingVibrate_lbl: UILabel!
    @IBOutlet var notificationSettingMute_lbl: UILabel!
    @IBOutlet var changePasswordTitle_lbl: UILabel!
    
    @IBOutlet var appVersionTitle_lbl: UILabel!
    @IBOutlet var appVersion_lbl: UILabel!

    @IBOutlet var notifications_Switch: MBSwitch!
    @IBOutlet var notificationsVibrate_btn: UIButton!
    @IBOutlet var notificationsTune_btn: UIButton!
    @IBOutlet var notificationsMute_btn: UIButton!
    
    @IBOutlet var biometricTitleLabel: UILabel!
    @IBOutlet var biometricSwitch: MBSwitch!

    //MARK: - View Controller Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        notifications_Switch.addTarget(self, action:#selector(notificationSwitchValueChanged) , for: UIControl.Event.valueChanged)
        biometricSwitch.addTarget(self, action:#selector(biometricSwitchValueChanged) , for: UIControl.Event.valueChanged)

        // get Notifications Configurations from server
        loadNotificationsConfigurations()

    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.interactivePop(true)
        
        // Loading intial layout Text
        layoutViewController()
        
        /* Set biometric current status */
        biometricSwitch.isOn = MBUserSession.shared.isBiometricEnabled()
    }

    //MARK: - IBACTIONS

    @IBAction func changeLanguagePressed(_ sender: UIButton) {

        let changelang = self.myStoryBoard.instantiateViewController(withIdentifier: "SelectionAlertVC") as! SelectionAlertVC
        
        changelang.setOKButtonCompletionHandler { (aString) in

            // Setting check to define user has changed language.
            MBUserSession.shared.shouldCallAndUpdateAppResumeData = true

            // Reloading layout Text accourding to seletcted language
            self.layoutViewController()
        }

        self.presentPOPUP(changelang, animated: true, completion:  nil)
    }

    @IBAction func notificationsTonePressed(_ sender: Any) {

        notificationSoundSetting(ringingType: .Tone)
    }

    @IBAction func vibratePressed(_ sender: UIButton) {

        notificationSoundSetting(ringingType: .Vibrate)
    }

    @IBAction func mutePressed(_ sender: UIButton) {

        notificationSoundSetting(ringingType: .Mute)
    }

    @IBAction func changePassword(_ sender: UIButton) {
        let changePassword = self.myStoryBoard.instantiateViewController(withIdentifier: "ChangePasswordVC") as! ChangePasswordVC
        self.navigationController?.pushViewController(changePassword, animated: true)
    }
    

    //MARK: - Functions
    func layoutViewController() {
        title_lbl.text                      = Localized("Title_Settings")
        changeLanguageTitle_lbl.text        = Localized("Title_ChangeLanguage")
        language_lbl.text                   = Localized("Info_Language")
        notificationTitle_lbl.text          = Localized("Title_Notifications")
        notificationDiscription_lbl.text    = Localized("Description_Notifications")
        notificationSettingTitle_lbl.text   = Localized("Title_NotificationsSetting")
        notificationSettingTune_lbl.text    = Localized("Info_NotificationSettingTune")
        notificationSettingVibrate_lbl.text = Localized("Info_NotificationSettingVibrate")
        notificationSettingMute_lbl.text    = Localized("Info_NotificationSettingMute")
        changePasswordTitle_lbl.text        = Localized("Title_ChangePassword")
        appVersionTitle_lbl.text            = Localized("Title_AppVersion")
        biometricTitleLabel.text             = Localized("Title_Biometric")
        appVersion_lbl.text                 = String.appVersion()
    }

    func loadNotificationsConfigurations() {

        // Parssing response data
        if let responseHandler : AddFCM = AddFCM.loadFromUserDefaults(key: APIsType.notificationConfigurations.selectedLocalizedAPIKey()) {

            self.isNotificationEnabled = self.checkIsEnableFromString(string: responseHandler.isEnable)
            self.ringingType = Constants.MBNotificationSoundType(rawValue: responseHandler.ringingStatus) ?? .Tone

            // Set Notification ON/Off setting
            self.changeNotificationSwitchLayout(isEnable: self.isNotificationEnabled)
            // Set  Notification Sound setting
            self.notificationSoundSettingLayout(ringingType: self.ringingType)
        } else {
            // Set Notification ON/Off setting
            self.changeNotificationSwitchLayout(isEnable: false)
            // Set  Notification Sound setting
            self.notificationSoundSettingLayout(ringingType: .Tone)

            self.activityIndicator.showActivityIndicator()
        }

        getNotificationSettings()
    }

    @objc func notificationSwitchValueChanged() {

        changeNotificationState(isEnable: notifications_Switch.isOn, ringingStatus: ringingType)
        changeNotificationSwitchLayout(isEnable: notifications_Switch.isOn)

    }

    func changeNotificationSwitchLayout(isEnable: Bool) {

        if isEnable {

            notifications_Switch.isOn = true
            notificationTitle_lbl.textColor = UIColor.black
            notificationDiscription_lbl.textColor = UIColor.MBDarkGrayColor

            notificationSettingTune_lbl.textColor = UIColor.MBDarkGrayColor
            notificationSettingVibrate_lbl.textColor = UIColor.MBDarkGrayColor
            notificationSettingMute_lbl.textColor = UIColor.MBDarkGrayColor

            notificationsVibrate_btn.isEnabled = true
            notificationsTune_btn.isEnabled = true
            notificationsMute_btn.isEnabled = true

        } else {

            notifications_Switch.isOn = false
            //notificationTitle_lbl.textColor = UIColor.MBLightGrayColor
            //notificationDiscription_lbl.textColor = UIColor.MBDimLightGrayColor

            notificationSettingTune_lbl.textColor = UIColor.MBLightGrayColor
            notificationSettingVibrate_lbl.textColor = UIColor.MBLightGrayColor
            notificationSettingMute_lbl.textColor = UIColor.MBLightGrayColor

            notificationsVibrate_btn.isEnabled = false
            notificationsTune_btn.isEnabled = false
            notificationsMute_btn.isEnabled = false
        }
    }

    func notificationSoundSetting(ringingType: Constants.MBNotificationSoundType) {

        // Check if user can change notification sound type
        if !isNotificationEnabled {
            return
        } else {

            notificationSoundSettingLayout(ringingType: ringingType)
            changeNotificationState(isEnable: isNotificationEnabled, ringingStatus: ringingType)
        }
    }

    func notificationSoundSettingLayout(ringingType: Constants.MBNotificationSoundType) {

        notificationsTune_btn .setImage(#imageLiteral(resourceName: "Checkbox-state2"), for: UIControl.State.normal)
        notificationsVibrate_btn .setImage(#imageLiteral(resourceName: "Checkbox-state2"), for: UIControl.State.normal)
        notificationsMute_btn .setImage(#imageLiteral(resourceName: "Checkbox-state2"), for: UIControl.State.normal)

        switch ringingType {

        case .Tone:
            notificationsTune_btn .setImage(#imageLiteral(resourceName: "Checkbox-state1"), for: UIControl.State.normal)
        case .Vibrate:
            notificationsVibrate_btn .setImage(#imageLiteral(resourceName: "Checkbox-state1"), for: UIControl.State.normal)
        case .Mute:
            notificationsMute_btn .setImage(#imageLiteral(resourceName: "Checkbox-state1"), for: UIControl.State.normal)
        }
    }


    func checkIsEnableFromString(string: String) -> Bool {

        if string == "1" {
            return true
        } else {
            return false
        }
    }

    @objc func biometricSwitchValueChanged() {
        UserDefaults.standard.saveBoolForKey(boolValue: biometricSwitch.isOn, forKey: Constants.K_IsBiometricEnabled)
    }
    

    //MARK: - API Calls
    /// Call 'changeNotificationState' API .
    ///
    /// - returns: Void

    func changeNotificationState(isEnable : Bool, ringingStatus: Constants.MBNotificationSoundType) {

        activityIndicator.showActivityIndicator()
        _ = MBAPIClient.sharedClient.addFCMId(fcmKey: Messaging.messaging().fcmToken ?? "", ringingStatus: ringingStatus ,isEnable: isEnable, isFromLogin: false, { (response, resultData, error, isCancelled, status, resultCode, resultDesc) in

            self.activityIndicator.hideActivityIndicator()
            if error != nil {
                error?.showServerErrorInViewController(self)

            } else {
                // handling data from API response.
                if resultCode == Constants.MBAPIStatusCode.succes.rawValue {

                    //EventLog
                    MBAnalyticsManager.logEvent(screenName: "Settings Screens", contentType:"Notification Enabled" , status:"Success" )

                    // Parssing response data
                    if let addFCMResponseHandler = Mapper<AddFCM>().map(JSONObject:resultData) {

                        // Save Notification configration
                        addFCMResponseHandler.saveInUserDefaults(key: APIsType.notificationConfigurations.selectedLocalizedAPIKey())

                        // Updating selected Values
                        self.isNotificationEnabled = self.checkIsEnableFromString(string: addFCMResponseHandler.isEnable)
                        self.ringingType = Constants.MBNotificationSoundType(rawValue: addFCMResponseHandler.ringingStatus) ?? .Tone

                        // Showing alert

                        self.showSuccessAlertWithMessage(message: resultDesc, { (aString) in

                            // Check if user enable or disabled notification swiftch
                            if self.isNotificationEnabled {
                                // Check for Notification setting
                                self.isUserEnabledNotification(completionHandler: { (isEnabled) in

                                    if isEnabled != true {
                                        self.showAccessAlert(message: Localized("Message_EnableRemoteNotification"))
                                    }
                                })
                            }
                        })
                        
                    } else {
                        self.showErrorAlertWithMessage(message: resultDesc)
                    }
                } else {

                    MBAnalyticsManager.logEvent(screenName: "Settings Screen", contentType:"Notification Disabled" , status:"Failure")

                    self.showErrorAlertWithMessage(message: resultDesc)
                }
            }

            // Will change Notification ON/Off setting in case of failure
            self.changeNotificationSwitchLayout(isEnable: self.isNotificationEnabled)
            // Will change Notification Sound setting in case of failure
            self.notificationSoundSettingLayout(ringingType: self.ringingType)
        })
    }

    func getNotificationSettings() {

        _ = MBAPIClient.sharedClient.addFCMId(fcmKey: Messaging.messaging().fcmToken ?? "", ringingStatus: .Tone, isEnable: true, isFromLogin: true, { (response, resultData, error, isCancelled, status, resultCode, resultDesc) in

            self.activityIndicator.hideActivityIndicator()

            if error != nil {
                error?.showServerErrorInViewController(self)

            } else {
                // handling data from API response.
                if resultCode == Constants.MBAPIStatusCode.succes.rawValue {

                    // Parssing response data
                    let extractedExpr: Mapper<AddFCM> = Mapper<AddFCM>()
                    if let addFCMResponseHandler = extractedExpr.map(JSONObject:resultData) {

                        // Save Notification configration
                        addFCMResponseHandler.saveInUserDefaults(key: APIsType.notificationConfigurations.selectedLocalizedAPIKey())
                        // Updating selected Values
                        self.isNotificationEnabled = self.checkIsEnableFromString(string: addFCMResponseHandler.isEnable)
                        self.ringingType = Constants.MBNotificationSoundType(rawValue: addFCMResponseHandler.ringingStatus) ?? .Tone
                    } else {
                        self.showErrorAlertWithMessage(message: resultDesc)
                    }

                } else {
                    self.showErrorAlertWithMessage(message: resultDesc)
                }
            }
        })
    }
}
