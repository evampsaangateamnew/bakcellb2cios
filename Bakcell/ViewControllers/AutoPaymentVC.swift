//
//  AutoPaymentVC.swift
//  Bakcell
//
//  Created by Touseef Sarwar on 04/03/2021.
//  Copyright © 2021 evampsaanga. All rights reserved.
//

import UIKit
import ObjectMapper

class AutoPaymentVC: BaseVC {

    //View Controller Identifier.
    static let identifier = "AutoPaymentVC"
    
    
    
    // MARK: - IBOutlet
    @IBOutlet var autoPaymentTitle_lbl: UILabel!
    @IBOutlet var tableView: UITableView!
    
    
    //MARK: -Properties
    var autoPayments : [AutoPayments]?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        tableView.register(UINib(nibName: AutoPaymentCell.ID, bundle: nil), forCellReuseIdentifier: AutoPaymentCell.ID)
        tableView.register(UINib(nibName: AddCardPaymentCell.ID, bundle: nil), forCellReuseIdentifier: AddCardPaymentCell.ID)
        tableView.tableFooterView = UIView()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.interactivePop(true)
        loadViewLayout()
        self.getScheduledPayments()
    }
    
    //MARK: - FUNCTIONS
    func loadViewLayout(){
        autoPaymentTitle_lbl.font = UIFont.MBArial(fontSize: 20)
        autoPaymentTitle_lbl.text = Localized("autoPayment_Title")
    }
}


//MARK: TableView Datasource and delegates
extension AutoPaymentVC: UITableViewDataSource, UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (self.autoPayments?.count ?? 0) + 1
    }
    
    func tableView(_ tableView: UITableView, titleForDeleteConfirmationButtonForRowAt indexPath: IndexPath) -> String? {
        return Localized("DeleteButtonTitle")
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == self.autoPayments?.count ?? 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: AddCardPaymentCell.ID, for: indexPath) as! AddCardPaymentCell
            
            cell.lblTitle.text = Localized("Lbl_AddNewAutoPayment")
            
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: AutoPaymentCell.ID, for: indexPath) as! AutoPaymentCell
            cell.setUp(withData: self.autoPayments?[indexPath.row])
            
            return cell
        }
        
        
    }
    
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == self.autoPayments?.count ?? 0{
            let schedulePayment = self.myStoryBoard2.instantiateViewController(withIdentifier: SchedulePaymentVC.identifier) as! SchedulePaymentVC
            self.navigationController?.pushViewController(schedulePayment, animated: true)
        }
        
    }
    
    
    //Swipe to Delete
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        //return true
        if self.autoPayments?.count ?? 0 > 0 {
            if indexPath.row < self.autoPayments?.count ?? 0 {
                return true
            }
        }
        return false
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if indexPath.row != self.autoPayments?.count ?? 0{
            if  editingStyle == .delete{
                
                let id = String(self.autoPayments?[indexPath.row].id ?? 0)
                self.deletePayment(paymentSchedulerId: id) { (isDeleted) in
                    if isDeleted{
                        self.autoPayments?.remove(at: indexPath.row)
                        self.tableView.reloadData()

                    }
                }
            }
        }
    }
    
}


//MARK: - API Calls
extension AutoPaymentVC{
    
    
    /// Call 'getScheduledPayments' API.
    ///
    /// - returns: Void
    func  getScheduledPayments(){
        activityIndicator.showActivityIndicator()
        _ = MBAPIClient.sharedClient.getScheduledPayments({ ( response, resultData, error, isCancelled, status, resultCode, resultDesc) in
            self.activityIndicator.hideActivityIndicator()
            if error != nil {
                //EventLog
                MBAnalyticsManager.logEvent(screenName: "Top up Screen", contentType:"Top up Scratch Card Failure" , status:"Failure" )
                
//                error?.showServerErrorInViewController(self)
                
            } else {
                // handling data from API response.
                if resultCode == Constants.MBAPIStatusCode.succes.rawValue {
                    
//                    //EventLog
//                    MBAnalyticsManager.logEvent(screenName: "Top up Screen", contentType:"Top up Scratch Card Success" , status:"Success" )
//
//                    // Set true to reload Home page data
//                    MBUserSession.shared.shouldReloadDashboardData = true
//
                    //parsing data...
                    
                    if let resp =  Mapper<AutoPaymentsResponse>().map(JSONObject: resultData){
                        self.autoPayments = resp.autoPaymentResponse
                        self.tableView.reloadData()
                    }
                    
                    
                } else {
                    //EventLog
//                    MBAnalyticsManager.logEvent(screenName: "Top up Screen", contentType:"Top up Scratch Card Failure" , status:"Failure" )
                    
                    // Show error alert to user
                    self.showErrorAlertWithMessage(message: resultDesc)
                }
            }
        })
    }
    
    
    /// Call 'deletepayment' API.
    ///
    /// - returns: Void
    
    
    func  deletePayment(paymentSchedulerId: String, onCompletion : @escaping (Bool) -> ()){
        activityIndicator.showActivityIndicator()
        _ = MBAPIClient.sharedClient.deletePaymentScheduler(paymentSchedulerId: paymentSchedulerId){ ( response, resultData, error, isCancelled, status, resultCode, resultDesc) in
            self.activityIndicator.hideActivityIndicator()
            if error != nil {
                //EventLog
                MBAnalyticsManager.logEvent(screenName: "Top up Screen", contentType:"Top up Scratch Card Failure" , status:"Failure" )
                
                error?.showServerErrorInViewController(self)
                
            } else {
                // handling data from API response.
                if resultCode == Constants.MBAPIStatusCode.succes.rawValue {
                    
//                    //EventLog
//                    MBAnalyticsManager.logEvent(screenName: "Top up Screen", contentType:"Top up Scratch Card Success" , status:"Success" )
//
//                    // Set true to reload Home page data
//                    MBUserSession.shared.shouldReloadDashboardData = true
//
                    //parsing data...
                    print(resultData ?? "no data found.")
                    onCompletion(true)
                    
                } else {
                    //EventLog
//                    MBAnalyticsManager.logEvent(screenName: "Top up Screen", contentType:"Top up Scratch Card Failure" , status:"Failure" )
                    
                    // Show error alert to user
                    self.showErrorAlertWithMessage(message: resultDesc)
                }
            }
        }
    }
}


