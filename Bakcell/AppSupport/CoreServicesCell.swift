//
//  CoreServicesCell.swift
//  Bakcell
//
//  Created by Saad Riaz on 6/6/17.
//  Copyright © 2017 evampsaanga. All rights reserved.
//

import UIKit

class CoreServicesCell: UITableViewCell {
    
    @IBOutlet var title_lbl: UILabel!
    @IBOutlet var price_lbl: UILabel!
    @IBOutlet var details_lbl: UILabel!
    @IBOutlet var validity_lbl: UILabel!
    
    @IBOutlet var middleSepratorView: UIView!
    @IBOutlet var setting_btn: MBMarqueeButton!
    @IBOutlet var switchControl: MBSwitch!
    @IBOutlet var switchControlTopConstraint: NSLayoutConstraint!
    
    
    // Title label alignment
    @IBOutlet var titleLableTopConstraint: NSLayoutConstraint!
    @IBOutlet var detailsLableTopConstraint: NSLayoutConstraint!
    @IBOutlet var titleLableCenterAligmentConstraint: NSLayoutConstraint!
    @IBOutlet var titleLabelHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet var descriptionLableTrailingToSwitchBtnConstraint: NSLayoutConstraint!
    @IBOutlet var descriptionLableTrailingToSettingBtnConstraint: NSLayoutConstraint!
    
    @IBOutlet var settingBtnWidthConstraint: NSLayoutConstraint!
    
    // Progress View
    @IBOutlet var progressView: UIView!
    @IBOutlet var renewalTitle_lbl: UILabel!
    @IBOutlet var renewalDaysLeft_lbl: UILabel!
    @IBOutlet var renewalDate_lbl: UILabel!
    @IBOutlet var renewalProgress: MBGradientProgressView!
    
    @IBOutlet var progressViewHeight: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    func setLayoutForForwordSettings(title: String?) {
        
        // setting Title label alignment
        titleLableTopConstraint.priority = UILayoutPriority(rawValue: 990)
        titleLableCenterAligmentConstraint.priority = UILayoutPriority(rawValue: 999)
        titleLableTopConstraint.isActive = false
        titleLableCenterAligmentConstraint.isActive = true
        titleLabelHeightConstraint.constant = 16
        
        middleSepratorView.isHidden = false
        
        progressViewHeight.constant = 0
        progressView.isHidden = true
        
        // setting button width
        //  settingBtnWidthConstraint.constant = 30.0
        
        // Hidding controlls which are not need
        switchControl.isHidden = false
        setting_btn.isHidden = true
        details_lbl.isHidden = true
        details_lbl.text = ""
        price_lbl.isHidden = true
        price_lbl.text = ""
        validity_lbl.isHidden = true
        validity_lbl.text = ""
        
        if title?.isBlank ?? false {
            title_lbl.text = Localized("Info_SettingTitle") + ": " + Localized("Info_None")
            switchControl.isEnabled = false
        } else {
            title_lbl.text = Localized("Info_SettingTitle") + ": " + (title ?? "")
            switchControl.isEnabled = true
        }
        
        
        //  setting detail lable trailing constraint
        descriptionLableTrailingToSwitchBtnConstraint.priority = UILayoutPriority(rawValue: 999)
        descriptionLableTrailingToSettingBtnConstraint.priority = UILayoutPriority(rawValue: 998)
        
    }
    
    func setLayoutForSwitchSettings(_ aCoreService: CoreServicesList?, isEnabled: Bool = true) {
        // setting Title label alignment
        titleLableTopConstraint.priority = UILayoutPriority(rawValue: 999)
        titleLableCenterAligmentConstraint.priority = UILayoutPriority(rawValue: 990)
        titleLableTopConstraint.isActive = true
        titleLableCenterAligmentConstraint.isActive = false
        titleLabelHeightConstraint.constant = 16
        
        middleSepratorView.isHidden = true
        
        // setting detail lable trailing constraint
        descriptionLableTrailingToSwitchBtnConstraint.priority = UILayoutPriority(rawValue: 999)
        descriptionLableTrailingToSettingBtnConstraint.priority = UILayoutPriority(rawValue: 998)
        
        // Hidding controlls which are not need
        details_lbl.isHidden = false
        switchControl.isHidden = false
        setting_btn.isHidden = true
        
        // Setting Enable/Disable
        title_lbl.isEnabled = isEnabled
        details_lbl.isEnabled = isEnabled
        switchControl.isEnabled = isEnabled
        price_lbl.isEnabled = isEnabled
        validity_lbl.isEnabled = isEnabled
        
        if let aCoreServiceObject = aCoreService {
            
            title_lbl.text = aCoreServiceObject.name
            DispatchQueue.main.async {
                if aCoreServiceObject.name.isEmpty || aCoreServiceObject.name == "" {
                    self.titleLableTopConstraint.isActive = true
                    self.titleLabelHeightConstraint.constant = 0.0
                    self.detailsLableTopConstraint.constant = 2.0
                    self.switchControlTopConstraint.constant = 3.0
                } else {
                    self.titleLabelHeightConstraint.constant = 16
                    self.detailsLableTopConstraint.constant = 4.0
                    self.switchControlTopConstraint.constant = 4.0
                }
            }
            details_lbl.text = aCoreServiceObject.description
            details_lbl.textColor = UIColor.MBLightGrayColor
            validity_lbl.textColor = UIColor.MBLightGrayColor
            price_lbl.textColor = UIColor.MBLightGrayColor
            
            // Price setting
            price_lbl.isHidden = false
            
            if (aCoreServiceObject.price.isBlank == true) {
                price_lbl.text = ""
                
            } else if (aCoreServiceObject.price.isEqualToStringIgnoreCase(otherString: "0.0") ||
                        aCoreServiceObject.price.isEqualToStringIgnoreCase(otherString: "0")) {
                
                price_lbl.text = Localized("Header_FREE")
                price_lbl.textColor = .MBRedColor
                
            } else {
                price_lbl.attributedText = MBUtilities.createAttributedTextWithManatSign(aCoreServiceObject.price)
            }
            
            
            // Setting Validity Label
            validity_lbl.isHidden = false
            
            if aCoreServiceObject.validity.isBlank == false {
                
               validity_lbl.text = "\n\(aCoreServiceObject.validityLabel): \(aCoreServiceObject.validity)"
                
            } else {
                validity_lbl.text = ""
            }
            
            //  Setting Progress bar
            if aCoreServiceObject.status.isTextActive() == false {
                
                progressViewHeight.constant = 0
                progressView.isHidden = true
                
              
            } else {
                
                validity_lbl.isHidden = true
                validity_lbl.text = ""
                
                if aCoreServiceObject.effectiveDate.isBlank == false &&
                    aCoreServiceObject.expireDate.isBlank == false {
                    
                    progressViewHeight.constant = 54
                    progressView.isHidden = false
                    
                    let computedValuesFromDates = MBUtilities.calculateProgressValues(from: aCoreServiceObject.effectiveDate, to: aCoreServiceObject.expireDate, AdditionalValueInEndDate: -1)
                    
                    renewalDaysLeft_lbl.text = computedValuesFromDates.daysLeftDisplayValue
                    
                    renewalProgress.setProgress(computedValuesFromDates.progressValue, animated: true)
                    
                    renewalTitle_lbl.text = aCoreServiceObject.progressTitle
                    renewalDate_lbl.text = "\(aCoreServiceObject.progressDateLabel): \(computedValuesFromDates.endDate)"
                    
                } else {
                    progressViewHeight.constant = 0
                    progressView.isHidden = true
                }
                
            }
            
        } else {
            
            title_lbl.text = ""
            details_lbl.text = ""
            price_lbl.text = ""
            validity_lbl.text = ""
        }
        
        self.layoutIfNeeded()
        
    }
    
    func setLayoutForInternetSettings(title: String?, detail: String?) {
        // setting Title label alignment
        titleLableTopConstraint.priority = UILayoutPriority(rawValue: 999)
        titleLableCenterAligmentConstraint.priority = UILayoutPriority(rawValue: 990)
        titleLableTopConstraint.isActive = true
        titleLableCenterAligmentConstraint.isActive = false
        titleLabelHeightConstraint.constant = 16
        
        middleSepratorView.isHidden = true
        
        progressViewHeight.constant = 0
        progressView.isHidden = true
        
        // setting button width
        settingBtnWidthConstraint.constant = 120.0
        
        // Hidding controlls which are not need
        details_lbl.isHidden = false
        setting_btn.isHidden = false
        switchControl.isHidden = true
        price_lbl.isHidden = true
        price_lbl.text = ""
        validity_lbl.isHidden = true
        validity_lbl.text = ""
        
        
        // setting detail lable trailing constraint
        descriptionLableTrailingToSwitchBtnConstraint.priority = UILayoutPriority(rawValue: 998)
        descriptionLableTrailingToSettingBtnConstraint.priority = UILayoutPriority(rawValue: 999)
        
        title_lbl.text = title ?? ""
        details_lbl.text = detail ?? ""
        setting_btn.setBackgroundImage(UIImage(), for: UIControl.State.normal)
        setting_btn.setTitle(Localized("BtnTitle_Send"), for: UIControl.State.normal)
        
        details_lbl.textColor = UIColor.MBLightGrayColor
        setting_btn.backgroundColor = UIColor.MBButtonBackgroundGrayColor
        setting_btn.layer.cornerRadius = CGFloat(Constants.kButtonCornerRadius)
        
        self.layoutIfNeeded()
        
    }
    
    func setLayoutForSuspend(title: String?, detail: String?) {
        // setting Title label alignment
        titleLableTopConstraint.priority = UILayoutPriority(rawValue: 999)
        titleLableCenterAligmentConstraint.priority = UILayoutPriority(rawValue: 990)
        titleLableTopConstraint.isActive = true
        titleLableCenterAligmentConstraint.isActive = false
        titleLabelHeightConstraint.constant = 0
        
        middleSepratorView.isHidden = true
        
        progressViewHeight.constant = 0
        progressView.isHidden = true
        
        // setting button width
        settingBtnWidthConstraint.constant = 120.0
        
        // Hidding controlls which are not need
        details_lbl.isHidden = false
        setting_btn.isHidden = false
        switchControl.isHidden = true
        price_lbl.isHidden = true
        price_lbl.text = ""
        validity_lbl.isHidden = true
        validity_lbl.text = ""
        
        
        // setting detail lable trailing constraint
        descriptionLableTrailingToSettingBtnConstraint.priority = UILayoutPriority(rawValue: 999)
        descriptionLableTrailingToSettingBtnConstraint.isActive = true
        descriptionLableTrailingToSwitchBtnConstraint.priority = UILayoutPriority(rawValue: 990)
        
        title_lbl.text = ""
        details_lbl.text = detail ?? ""
        setting_btn.setBackgroundImage(UIImage(), for: UIControl.State.normal)
        
        
        if MBUserSession.shared.isStatusActive() {
            setting_btn.setTitle(Localized("BtnTitle_SUSPEND"), for: UIControl.State.normal)
            details_lbl.textColor = UIColor.black
            details_lbl.isEnabled = true
        } else {
            setting_btn.setTitle(Localized("BtnTitle_SUSPENDED"), for: UIControl.State.normal)
            details_lbl.isEnabled = false
        }
        
        details_lbl.textColor = UIColor.MBLightGrayColor
        
        setting_btn.backgroundColor = UIColor.MBButtonBackgroundGrayColor
        setting_btn.layer.cornerRadius = CGFloat(Constants.kButtonCornerRadius)
        
        self.layoutIfNeeded()
        
    }
    
}
