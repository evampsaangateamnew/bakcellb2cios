//
//  AutoPayment.swift
//  Bakcell
//
//  Created by Touseef Sarwar on 13/04/2021.
//  Copyright © 2021 evampsaanga. All rights reserved.
//

import Foundation
import ObjectMapper


class AutoPaymentsResponse : Mappable{
    
    var autoPaymentResponse : [AutoPayments]?
    
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        
        autoPaymentResponse      <- map["data"]
        
    }
}

class AutoPayments : Mappable{
    
    var id: Int?
    var amount : String = ""
    var billingCycle : String = ""
    var cardType : String = ""
    var recurrenceNumber : String = ""
    var savedCardId: String = ""
    var startDate: String = ""
    var msisdn: String = ""
    var nextScheduledDate: String  = ""
    var recurrentDay: String = ""
    
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        
        id                  <- map["id"]
        amount              <- map["amount"]
        billingCycle        <- map["billingCycle"]
        cardType            <- map["cardType"]
        recurrenceNumber    <- map["recurrenceNumber"]
        savedCardId         <- map["savedCardId"]
        startDate           <- map["startDate"]
        msisdn              <- map["msisdn"]
        nextScheduledDate   <- map["nextScheduledDate"]
        recurrentDay        <- map["recurrentDay"]
        
    }
}
