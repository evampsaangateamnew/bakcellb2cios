//
//  OperationsHistoryVC.swift
//  Bakcell
//
//  Created by Saad Riaz on 5/19/17.
//  Copyright © 2017 evampsaanga. All rights reserved.
//

import UIKit
import  ObjectMapper

class OperationsHistoryVC: BaseVC {
    
    // MARK: - Properties
    var operationsHistoryData : [OperationsRecord]?
    var filteredOperationsData : [OperationsRecord]?
    var transactionSelectedType : String?
    var isStartDate : Bool?
    
    //UIdate picker
    let datePicker = UIDatePicker()
    let myView = UIView()
    
    
    // MARK: - IBOutlet
    
    @IBOutlet var dateDropDown: UIDropDown!
    @IBOutlet var transactionDropDown: UIDropDown!
    @IBOutlet var dateLbl: UILabel!
    @IBOutlet var tableView: MBAccordionTableView!
    @IBOutlet var pageCount: UILabel!
    @IBOutlet var spacificBtnView: UIView!
    @IBOutlet var endDateBtn: UIButton!
    @IBOutlet var startDateBtn: UIButton!
    @IBOutlet var startDateLb: UILabel!
    @IBOutlet var endDateLb: UILabel!
    @IBOutlet var operationHistoryTitle_lbl: UILabel!
    @IBOutlet var service_lbl: UILabel!
    @IBOutlet var amount_lbl: UILabel!
    @IBOutlet var endingBalanceView: UIView!
    @IBOutlet var endingBalance_lbl: UILabel!
    
    
    //MARK: - ViewController Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        spacificBtnView.isHidden = true
        tableView.allowMultipleSectionsOpen = true
        tableView.delegate = self
        tableView.dataSource = self
        
        //Registering Cells for tableView
        tableView.register(UINib(nibName: "OperationsExpandCell", bundle: nil), forCellReuseIdentifier: "OperationsExpandCell")
        tableView.register(UINib(nibName: "OperationsHeaderView", bundle: nil), forHeaderFooterViewReuseIdentifier: OperationsHeaderView.identifier)
        
        setUpDropDowns()
        
        // Hidding Ending Balance in case of postpaid user
        if MBUserSession.shared.subscriberType() == MBSubscriberType.PostPaid {
            
            endingBalanceView.isHidden = true
        } else {
            endingBalanceView.isHidden = false
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.interactivePop(true)
        
        //EventLog
        MBAnalyticsManager.logEvent(screenName: "My Account", contentType:"Operation History Screen" , status:"Operation History Screen")
        
        loadViewLayout()
    }
    
    // MARK: - IBActions
    
    @IBAction func startBtnAction(_ sender: UIButton) {
        
        isStartDate = true;
        
        let todayDate = MBUtilities.todayDate()
        MBUserSession.shared.dateFormatter.dateFormat = Constants.kDisplayFormat
        
        let threeMonthsAgos = MBUtilities.todayDate().getMonth(WithAdditionalValue: -3)?.startOfMonth() ?? todayDate
        
        if let endDateString = endDateLb.text,
            endDateString.isBlank == false {
            
            self.showDatePicker(minDate: threeMonthsAgos, maxDate: MBUserSession.shared.dateFormatter.date(from: endDateString) ?? todayDate)
            
        } else {
            self.showDatePicker(minDate: threeMonthsAgos, maxDate: todayDate)
        }
        
    }
    
    @IBAction func endBtnAction(_ sender: UIButton) {
        
        isStartDate = false;
        
        let todayDate = MBUtilities.todayDate(dateFormate: Constants.kDisplayFormat)
        
        MBUserSession.shared.dateFormatter.dateFormat = Constants.kDisplayFormat
        
        self.showDatePicker(minDate: MBUserSession.shared.dateFormatter.date(from: startDateLb.text ?? "") ?? todayDate, maxDate: todayDate)
    }
    
    //MARK: - FUNCTIONS
    
    func loadViewLayout(){
        operationHistoryTitle_lbl.text = Localized("OpsHist_Title")
        dateLbl.text = Localized("OpsHist_Date")
        service_lbl.text = Localized("OpsHist_Service")
        amount_lbl.text = Localized("OpsHist_Amount")
        endingBalance_lbl.text = Localized("OpsHist_EndingBalance")
    }
    
    func setUpDropDowns(){
        //Transaction DropDown
        transactionDropDown.optionsTextAlignment = NSTextAlignment.center
        transactionDropDown.textAlignment = NSTextAlignment.center
        transactionDropDown.placeholder = Localized("DropDown_All")
        //transactionDropDown.placeholder = Localized("PlaceHolder_SelectPeriod")
        self.transactionDropDown.rowBackgroundColor = UIColor.clear
        self.transactionDropDown.borderWidth = 0
        self.transactionDropDown.tableHeight = 325
        transactionDropDown.setFont = UIFont.MBArial(fontSize: 14)
        transactionDropDown.optionsSize = 12
        transactionDropDown.options = [Localized("DropDown_All"),Localized("DropDown_DataBundles"), Localized("DropDown_SMSBundles"), Localized("DropDown_Duplicate"), Localized("DropDown_MRC"), Localized("DropDown_Payment"),Localized("DropDown_Adjustment"),Localized("DropDown_Content(CP)"),Localized("DropDown_Installment"),Localized("DropDown_MoneyTransfer"),Localized("DropDown_ChangeOwnership"),Localized("DropDown_ChangeCustomerType"),Localized("DropDown_Others")]
        
        // Setting transfer type to all
        self.transactionSelectedType = Localized("DropDown_All")
        
        transactionDropDown.didSelect { (option, index) in
            
            self.transactionSelectedType = option
            
            self.filteredOperationsData = self.filterHistoryBy(type: self.transactionSelectedType, operationRecords: self.operationsHistoryData, allTypes: self.transactionDropDown.options)
            
            // Reload tableview data
            self.reloadTableViewData()
            
        }
        
        let todayDateString = MBUtilities.todayDateString(dateFormate: Constants.kAPIFormat)
        
        // Load initional data
        self.operationsHistoryApiCall(StartDate: todayDateString, EndDate: todayDateString) { (canShow) in
            
            if canShow == true && DisclaimerMessageVC.canShowDisclaimerOfType(.OperationHistory) == true {
                if let disclaimerMessageVC = self.myStoryBoard.instantiateViewController(withIdentifier: "DisclaimerMessageVC") as? DisclaimerMessageVC {
                    
                    disclaimerMessageVC.setDisclaimerAlertWith(description: Localized("Disclaimer_Description"), okBtnClickedBlock: { (dontShowAgain) in
                        
                        if dontShowAgain == true {
                            DisclaimerMessageVC.setDisclaimerStatusForType(.OperationHistory, canShow: false)
                        }
                    })
                    
                    self.presentPOPUP(disclaimerMessageVC, animated: true, modalTransitionStyle: .crossDissolve, completion: nil)
                }
            }
        }
        
        //Drop Down
        dateDropDown.optionsTextAlignment = NSTextAlignment.center
        dateDropDown.textAlignment = NSTextAlignment.center
        dateDropDown.placeholder = Localized("DropDown_CurrentDay")
        self.dateDropDown.rowBackgroundColor = UIColor.clear
        self.dateDropDown.borderWidth = 0
        self.dateDropDown.tableHeight = 175
        dateDropDown.setFont = UIFont.MBArial(fontSize: 14)
        dateDropDown.optionsSize = 12
        
        dateDropDown.options = [Localized("DropDown_CurrentDay"),Localized("DropDown_Last7Days"), Localized("DropDown_Last30Days"), Localized("DropDown_PreviousMonth"),Localized("DropDown_CustomPeriod")]
        
        dateDropDown.didSelect { (option, index) in
            
            switch index{
            case 0:
                print("default")
                self.spacificBtnView.isHidden = true
                
                self.operationsHistoryApiCall(StartDate: todayDateString, EndDate: todayDateString)
            case 1:
                print("Last 7 days")
                self.spacificBtnView.isHidden = true
                
                // Get data of previous seven days
                let sevenDaysAgo = MBUtilities.todayDateString(withAdditionalValue: -7)
                self.operationsHistoryApiCall(StartDate: sevenDaysAgo, EndDate: todayDateString)
                
            case 2:
                print("Last 30 days")
                self.spacificBtnView.isHidden = true
                
                // Get data of previous thirty days
                let thirtyDaysAgo = MBUtilities.todayDateString(withAdditionalValue: -30)
                self.operationsHistoryApiCall(StartDate: thirtyDaysAgo, EndDate: todayDateString)
                
            case 3:
                print("Previous month")
                self.spacificBtnView.isHidden = true
                
                // Getting previous month start and end date
                let startOfPreviousMonth = MBUtilities.todayDate().getPreviousMonth()?.startOfMonth() ?? MBUtilities.todayDate()
                let endOfPreviousMonth = MBUtilities.todayDate().getPreviousMonth()?.endOfMonth() ?? MBUtilities.todayDate()
                
                MBUserSession.shared.dateFormatter.dateFormat = Constants.kAPIFormat
                
                let startDateString = MBUserSession.shared.dateFormatter.string(from: startOfPreviousMonth)
                let endDateString = MBUserSession.shared.dateFormatter.string(from: endOfPreviousMonth)
                
                self.operationsHistoryApiCall(StartDate: startDateString, EndDate: endDateString)
                
            case 4:
                self.spacificBtnView.isHidden = false
                
                //For date formate
                self.startDateLb.text = MBUtilities.todayDateString(dateFormate: Constants.kDisplayFormat)
                self.endDateLb.text = MBUtilities.todayDateString(dateFormate: Constants.kDisplayFormat)
                
                self.startDateLb.textColor = UIColor.MBDarkGrayColor
                self.endDateLb.textColor = UIColor.MBDarkGrayColor
                
                self.operationsHistoryApiCall(StartDate: todayDateString, EndDate: todayDateString)
                
                print("this Month Days")
                
            default:
                print("destructive")
                
            }
        }
    }
    
    func showDatePicker(minDate : Date , maxDate : Date){
        //Formate Date
        datePicker.datePickerMode = .date
        
        //ToolBar
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        
        //done button & cancel button
        let doneButton = UIBarButtonItem(title: Localized("Btn_title_Done"), style: UIBarButtonItem.Style.done, target: self, action: #selector(OperationsHistoryVC.donedatePicker))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: Localized("Btn_title_Cancel"), style: UIBarButtonItem.Style.plain, target: self, action: #selector(OperationsHistoryVC.cancelDatePicker))
        toolbar.setItems([doneButton,spaceButton,cancelButton], animated: false)
        
        var  currentLanguageLocale = Locale(identifier:"en")
        switch MBLanguageManager.userSelectedLanguage() {
            
        case .Russian:
            currentLanguageLocale = Locale(identifier:"ru")
        case .English:
            currentLanguageLocale = Locale(identifier:"en")
        case .Azeri:
            currentLanguageLocale = Locale(identifier:"az_AZ")
        }
        
        self.datePicker.calendar.locale = currentLanguageLocale
        self.datePicker.locale = currentLanguageLocale
        
        
        datePicker.minimumDate = minDate
        datePicker.maximumDate = maxDate
        datePicker.timeZone =  TimeZone.current
        
        datePicker.backgroundColor = UIColor.lightGray
        
        if #available(iOS 13.4, *) {
            datePicker.preferredDatePickerStyle = .wheels
            toolbar.frame = CGRect.init(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 40)
        }
        
        datePicker.frame = CGRect(x: 0, y: 40, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height * 0.4)
        
        myView.frame = CGRect(x: 0, y:((UIScreen.main.bounds.height * 0.6) - 40), width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height * 0.4)
        
        if #available(iOS 13.4, *) {
            datePicker.alignmentRect(forFrame: myView.frame)
        }
        
        myView .addSubview(toolbar)
        myView .addSubview(datePicker)
        myView.backgroundColor = UIColor.white
        
        self.view.addSubview(myView)
    }
    
    func filterHistoryBy(type: String?, operationRecords: [OperationsRecord]?, allTypes: [String]? ) -> [OperationsRecord]? {
        
        guard let myType = type else {
            return nil
        }
        
        if myType == Localized("DropDown_All"){
            return operationRecords
            
        } else if myType == Localized("DropDown_Others"){
            
            // Get all recodes which does not mach any type
            let  filterdHistoryDetailsWhichDoesNotMatchType = operationRecords?.filter() {
                
                let aRecordType = ($0 as OperationsRecord).transactionType.lowercased()
                
                if (allTypes?.contains(where: { $0.lowercased() == aRecordType })) == false {
                    return true
                } else {
                    return false
                }
            }
            
            // Get Data of selected type "others"
            let  filterdHistoryDetailsForOthers = operationRecords?.filter() {
                
                if ($0 as OperationsRecord).transactionType.lowercased() == myType.lowercased() {
                    
                    return true
                } else {
                    return false
                }
            }
            
            // Combine both filtered data of "others" type and type does not match
            var filteredData = filterdHistoryDetailsForOthers
            filteredData?.append(contentsOf: filterdHistoryDetailsWhichDoesNotMatchType ?? [])
            
            return filteredData
            
        } else {
            
            let  filterdHistoryDetails = operationRecords?.filter() {
                
                if ($0 as OperationsRecord).transactionType.lowercased() == myType.lowercased() {
                    
                    return true
                } else {
                    return false
                }
            }
            return filterdHistoryDetails
        }
        
    }
    
    @objc func donedatePicker() {
        
        let todayDate = MBUtilities.todayDate(dateFormate: Constants.kDisplayFormat)
        
        //For date formate
        MBUserSession.shared.dateFormatter.dateFormat = Constants.kDisplayFormat
        MBUserSession.shared.dateFormatter.timeZone = TimeZone.current
        
        let selectedDate = MBUserSession.shared.dateFormatter.string(from: datePicker.date)
        
        let startFieldDate = MBUserSession.shared.dateFormatter.date(from: startDateLb.text ?? "")
        let endFieldDate = MBUserSession.shared.dateFormatter.date(from: endDateLb.text ?? "")
        
        //dismiss date picker dialog
        self.view.endEditing(true)
        myView .removeFromSuperview()
        
        MBUserSession.shared.dateFormatter.dateFormat = Constants.kAPIFormat
        
        if isStartDate ?? true {
            startDateLb.text = selectedDate
            self.startDateLb.textColor = UIColor.MBBarOrange
            self.operationsHistoryApiCall(StartDate: MBUserSession.shared.dateFormatter.string(from: datePicker.date), EndDate: MBUserSession.shared.dateFormatter.string(from:endFieldDate ?? todayDate))
        } else {
            endDateLb.text = selectedDate
            self.endDateLb.textColor = UIColor.MBBarOrange
            self.operationsHistoryApiCall(StartDate: MBUserSession.shared.dateFormatter.string(from: startFieldDate ?? todayDate), EndDate: MBUserSession.shared.dateFormatter.string(from: datePicker.date))
        }
    }
    
    @objc func cancelDatePicker(){
        //cancel button dismiss datepicker dialog
        self.view.endEditing(true)
        
        myView .removeFromSuperview()
        print("cancel")
    }
    
    func reloadTableViewData() {
        
        if filteredOperationsData?.count ?? 0 <= 0 {
            self.tableView.showDescriptionViewWithImage(description: Localized("Message_NoDataAvalible"))
        } else {
            
            self.tableView.hideDescriptionView()
        }
        self.tableView.reloadData()
        showPageNumber()
    }
    
    //MARK: - API Calls
    /// Call 'operationsHistoryApiCall' API.
    ///
    /// - returns: Void
    func operationsHistoryApiCall(StartDate startDate : String, EndDate endDate: String, loadFromDefault: Bool = false, completionHandler: @escaping (Bool) -> Void = {_ in }) {
        
        //NotE: No Caching is added right now
        
        if loadFromDefault == true,
            let opsHistoryResponse :OperationsHistoryHandler = OperationsHistoryHandler.loadFromUserDefaults(key: APIsType.operationHistory.selectedLocalizedAPIKey()){
            
            self.operationsHistoryData = opsHistoryResponse.records
            self.filteredOperationsData = self.filterHistoryBy(type: self.transactionSelectedType, operationRecords: self.operationsHistoryData, allTypes: transactionDropDown.options)
            
            // Reload tableview data
            self.reloadTableViewData()
            
            completionHandler(true)
            
        } else {
            activityIndicator.showActivityIndicator()
        }
        
        
        /*API call*/
        _ = MBAPIClient.sharedClient.getOperationsHistory(StartDate: startDate, EndDate: endDate, { ( response, resultData, error, isCancelled, status, resultCode, resultDesc) in
            
            self.activityIndicator.hideActivityIndicator()
            if error != nil {
                //EventLog
                MBAnalyticsManager.logEvent(screenName: "My Account", contentType:"Operation History Screen" , status:"Failure")
                error?.showServerErrorInViewController(self)
                completionHandler(false)
                
            } else {
                // handling data from API response.
                if resultCode == Constants.MBAPIStatusCode.succes.rawValue {
                    //EventLog
                    MBAnalyticsManager.logEvent(screenName: "My Account", contentType:"Operation History Screen" , status:"Success")
                    
                    // Parssing response data
                    if let opsHistoryResponse = Mapper<OperationsHistoryHandler>().map(JSONObject:resultData) {
                        
                        /*Save data into user defaults*/
                        opsHistoryResponse.saveInUserDefaults(key: APIsType.operationHistory.selectedLocalizedAPIKey())
                        
                        self.operationsHistoryData = opsHistoryResponse.records
                        self.filteredOperationsData = self.filterHistoryBy(type: self.transactionSelectedType, operationRecords: self.operationsHistoryData, allTypes: self.transactionDropDown.options)
                        
                        completionHandler(true)
                    }
                } else {
                    //EventLog
                    MBAnalyticsManager.logEvent(screenName: "My Account", contentType:"Operation History Screen" , status:"Failure")
                    
                    // Show error alert to user
                    self.showErrorAlertWithMessage(message: resultDesc)
                    completionHandler(false)
                }
            }
            
            // Reload tableview data
            self.reloadTableViewData()
        })
    }
    
}

// MARK: - <UITableViewDataSource> / <UITableViewDelegate>

extension OperationsHistoryVC : UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return filteredOperationsData?.count ?? 0
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 90.0;
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 52.0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "OperationsExpandCell", for: indexPath) as! OperationsExpandCell
        
        cell.title1_lbl.text = Localized("OpHist_Description")
        cell.title2_lbl.text = filteredOperationsData?[indexPath.section].description
        cell.description_lbl.text = Localized("OpHist_Clarification")
        cell.clarification_lbl.text = filteredOperationsData?[indexPath.section].clarification
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: OperationsHeaderView.identifier) as! OperationsHeaderView
        
        if let aHistoryObject = filteredOperationsData?[section] {
            
            headerView.date_lbl.text = aHistoryObject.date
            
            headerView.dataBundle_lbl.text = aHistoryObject.transactionType
            
            if aHistoryObject.currency.isEqualToStringIgnoreCase(otherString: "AZN") == true {
                
                headerView.amount_lbl.attributedText = MBUtilities.createAttributedTextWithManatSign(aHistoryObject.amount)
                
            } else {
                headerView.amount_lbl.text = aHistoryObject.amount
            }
            
            
            if MBUserSession.shared.subscriberType() == MBSubscriberType.PostPaid {
                headerView.endingBalanceView.isHidden = true
            } else {
                headerView.endingBalanceView.isHidden = false
                headerView.endingBalance_lbl.attributedText = MBUtilities.createAttributedTextWithManatSign(aHistoryObject.endingBalance)
            }
            
        }
        
        return headerView
    }
}

// MARK: - <MBAccordionTableViewDelegate>

extension OperationsHistoryVC : MBAccordionTableViewDelegate {
    
    
    func tableView(_ tableView: MBAccordionTableView, didOpenSection section: Int, withHeader header: UITableViewHeaderFooterView?) {
        
        showPageNumber()
    }
    func tableView(_ tableView: MBAccordionTableView, didCloseSection section: Int, withHeader header: UITableViewHeaderFooterView?) {
        
        showPageNumber()
    }
    
    func tableView(_ tableView: MBAccordionTableView, canInteractWithHeaderAtSection section: Int) -> Bool {
        return true
    }
}

// MARK: UIScrollViewDelegate
extension OperationsHistoryVC: UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        showPageNumber()
    }
    
    func showPageNumber() {
        
        pageCount.text = "\(tableView.lastVisibleSection())/\(filteredOperationsData?.count ?? 0)"
        
    }
}

