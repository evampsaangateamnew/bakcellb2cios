//
//  FastPayments.swift
//  Bakcell
//
//  Created by Touseef Sarwar on 08/04/2021.
//  Copyright © 2021 evampsaanga. All rights reserved.
//

import Foundation
import ObjectMapper

class FastPaymentsResponse : Mappable{
    var fastPaymentDetails: [FastPayment]?
    
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        
        fastPaymentDetails     <- map["fastPaymentDetails"]
        
    }
}

class FastPayment : Mappable{
    var topupNumber : String = ""
    var cardType : String = ""
    var amount : String = ""
    var paymentKey: String = ""
    var id: String = ""
    
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        
        topupNumber     <- map["topupNumber"]
        cardType        <- map["cardType"]
        amount          <- map["amount"]
        paymentKey      <- map["paymentKey"]
        id              <- map["id"]
    }
}
