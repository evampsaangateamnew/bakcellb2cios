//
//  ContactUsVC.swift
//  Bakcell
//
//  Created by Saad Riaz on 6/23/17.
//  Copyright © 2017 evampsaanga. All rights reserved.
//

import UIKit
import MessageUI
import ObjectMapper

class ContactUsVC: BaseVC {

    //MARK: - Properties
    var contactUsDetail = ContactUs()

    //MARK: - IBOutlet
    @IBOutlet var title_lbl: UILabel!
    @IBOutlet var headOfficeTitle_lbl: UILabel!
    @IBOutlet var headOfficeAddress_lbl: UILabel!

    @IBOutlet var infoTitle_lbl: UILabel!
    @IBOutlet var helpLine_lbl: UILabel!
    @IBOutlet var phoneNumber_lbl: UILabel!
    @IBOutlet var secondPhoneNumber_lbl: UILabel!
    @IBOutlet var email_lbl: UILabel!
    @IBOutlet var wap_lbl: UILabel!
    @IBOutlet var webSite_lbl: UILabel!
    @IBOutlet var offices_lbl: UILabel!

    @IBOutlet var socialMediaTitle_lbl: UILabel!
    @IBOutlet var inviteFriends_lbl: UILabel!
    @IBOutlet var send_btn: UIButton!

    //MARK: - View controller methods
    override func viewDidLoad() {
        super.viewDidLoad()

        // loading user initial inforamtion
        layoutViewController()

        // API call to get contact us information
        getContactUsDetails()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.interactivePop(true)
        
        //EventLog
        MBAnalyticsManager.logEvent(screenName: "Contact Us Screen", contentType:"Contact Us Screen" , status:"Contact Us Screen" )

    }

    //MARK: - IBAction
    @IBAction func headOfficeLocationBtnPressed(_ sender: UIButton) {

        let lattitude = contactUsDetail.addressLat ?? ""
        let longitude = contactUsDetail.addressLong ?? ""


        
        if lattitude.isBlank == false && longitude.isBlank == false {

            let cordinatesDict = ["lati": lattitude, "longi" : longitude, "title": Localized("Title_HeadOffice"), "address" : contactUsDetail.address ?? "" ]

            let store = self.myStoryBoard.instantiateViewController(withIdentifier: "StoreLocatorVC") as! StoreLocatorVC
            store.selectedType = .StoreLocator

            store.shouldFocuseToSelectedLocation = true
            store.selectedLocationForFocuse = cordinatesDict
            
            self.navigationController?.pushViewController(store, animated: true)

        } else {

            self.showErrorAlertWithMessage(message: Localized("Message_NoDataAvalible"))
        }
    }

    @IBAction func helpLineBtnPressed(_ sender: UIButton) {
        dialNumber(number: contactUsDetail.customerCareNo ?? "")
    }

    @IBAction func phoneBtnPressed(_ sender: UIButton) {
        dialNumber(number: contactUsDetail.phone ?? "")
    }

    @IBAction func phone2BtnPressed(_ sender: UIButton) {
        dialNumber(number: contactUsDetail.fax ?? "")
    }

    @IBAction func emialBtnPressed(_ sender: UIButton) {

        if MFMailComposeViewController.canSendMail() {
            let mail = MFMailComposeViewController()
            mail.mailComposeDelegate = self
            mail.setToRecipients([contactUsDetail.email ?? ""])
            mail.setMessageBody("", isHTML: false)

            present(mail, animated: true)
        } else {
            self.showErrorAlertWithMessage(message: Localized("Message_SetEmail"))
        }
    }
    

    @IBAction func wapBtnPressed(_ sender: UIButton) {
        //openURLInSafari(urlString: contactUsDetail.website ?? "")
    }

    @IBAction func webSiteBtnPressed(_ sender: UIButton) {
        openURLInSafari(urlString: contactUsDetail.website ?? "")
    }

    @IBAction func officesBtnPressed(_ sender: UIButton) {
        let store = self.myStoryBoard.instantiateViewController(withIdentifier: "StoreLocatorVC") as! StoreLocatorVC
        store.selectedType = .StoreLocator
        self.navigationController?.pushViewController(store, animated: true)
    }

    @IBAction func faceBookBtnPressed(_ sender: UIButton) {
        openURLInSafari(urlString: contactUsDetail.facebookLink)
    }

    @IBAction func twitterBtnPressed(_ sender: UIButton) {
        openURLInSafari(urlString: contactUsDetail.twitterLink)
    }

    @IBAction func googleBtnPressed(_ sender: UIButton) {
        openURLInSafari(urlString: contactUsDetail.googleLink)
    }

    @IBAction func youTubeBtnPressed(_ sender: UIButton) {
        openURLInSafari(urlString: contactUsDetail.youtubeLink)
    }

    @IBAction func instagramBtnPressed(_ sender: UIButton) {
        openURLInSafari(urlString: contactUsDetail.instagramLink)
    }
    @IBAction func linkedInBtnPressed(_ sender: UIButton) {
        openURLInSafari(urlString: contactUsDetail.linkedinLink)
    }

    @IBAction func sendPressed(_ sender: UIButton) {

        let sharingItems:[AnyObject?] = ["\(contactUsDetail.inviteFriendText ?? "")" as AnyObject]

        let activityViewController = UIActivityViewController(activityItems: sharingItems.compactMap({$0}), applicationActivities: nil)
        present(activityViewController, animated: true, completion: nil)
    }

    //MARK: - Functions
    func layoutViewController() {

        /* Setting View controller text*/
        title_lbl.text              = Localized("Title_ContactUs")
        headOfficeTitle_lbl.text    = Localized("Title_HeadOffice")
        headOfficeAddress_lbl.text  = Localized("Info_HeadOfficeAddress")
        infoTitle_lbl.text          = Localized("Title_InfoCenter")
        helpLine_lbl.text           = Localized("Info_HelpLine")
        phoneNumber_lbl.text        = Localized("Info_PhoneNumber")
        secondPhoneNumber_lbl.text  = Localized("Info_PhoneNumber2")
        email_lbl.text              = Localized("Info_EmailData")
        wap_lbl.text                = Localized("Info_WAP")
        webSite_lbl.text            = Localized("Info_webSitre")
        offices_lbl.text            = Localized("Info_Offices")
        socialMediaTitle_lbl.text   = Localized("Title_SocialMedia")
        inviteFriends_lbl.text      = Localized("Title_InviteFriends")

        send_btn.setTitle(Localized("BtnTitle_Send"), for: UIControl.State.normal)
    }

    func updateInformationOfLayout() {

        /* Setting View controller text*/
        headOfficeAddress_lbl.text  = contactUsDetail.address ?? ""
        helpLine_lbl.text           = contactUsDetail.customerCareNo ?? ""
        phoneNumber_lbl.text        = contactUsDetail.phone ?? ""
        secondPhoneNumber_lbl.text  = contactUsDetail.fax ?? ""
        email_lbl.text              = contactUsDetail.email ?? ""
        wap_lbl.text                = contactUsDetail.website ?? ""
        webSite_lbl.text            = contactUsDetail.website ?? ""

    }

    //MARK: - APIs call

    /// Call 'getContactUsDetails' API.
    ///
    /// - returns: Void
    func getContactUsDetails() {


        /*Loading initial data from UserDefaults*/
        if let contactUsResponse :ContactUs = ContactUs.loadFromUserDefaults(key: APIsType.contactUs.selectedLocalizedAPIKey()) {
            
            self.contactUsDetail = contactUsResponse
            self.updateInformationOfLayout()
            
        } else {
            activityIndicator.showActivityIndicator()
        }


        _ = MBAPIClient.sharedClient.getContactUsDetails({ ( response, resultData, error, isCancelled, status, resultCode, resultDesc) in

            self.activityIndicator.hideActivityIndicator()
            if error != nil {
                error?.showServerErrorInViewController(self)

            } else {
                // handling data from API response.
                if resultCode == Constants.MBAPIStatusCode.succes.rawValue {

                    // Parssing response data
                    if let contactUsResponse = Mapper<ContactUs>().map(JSONObject:resultData) {

                        /*Save data into user defaults*/
                        contactUsResponse.saveInUserDefaults(key: APIsType.contactUs.selectedLocalizedAPIKey())

                        self.contactUsDetail = contactUsResponse
                        self.updateInformationOfLayout()
                    }
                }  else {
                    // Show error alert to user
                    self.showErrorAlertWithMessage(message: resultDesc)
                }
            }
        })
    }
}

//MARK: - MFMailComposeViewControllerDelegate
extension ContactUsVC : MFMailComposeViewControllerDelegate {

    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true)
    }
}
