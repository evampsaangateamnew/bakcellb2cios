//
//  UserDefault+MB.swift
//  Bakcell
//
//  Created by AbdulRehman Warraich on 10/9/17.
//  Copyright © 2017 evampsaanga. All rights reserved.
//

import Foundation
extension UserDefaults {

    func saveStringForKey(stringValue: String, forKey key: String) {
        UserDefaults.standard.set(stringValue, forKey: key)
        UserDefaults.standard.synchronize()
    }

    func saveBoolForKey(boolValue: Bool, forKey key: String) {
        UserDefaults.standard.set(boolValue, forKey: key)
        UserDefaults.standard.synchronize()
    }
}
