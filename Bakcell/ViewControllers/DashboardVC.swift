//
//  DashboardVC.swift
//  Bakcell
//
//  Created by Shujat on 16/05/2017.
//  Copyright © 2017 evampsaanga. All rights reserved.
//

import UIKit
import CircleProgressView
import UPCarouselFlowLayout
import ObjectMapper
import MarqueeLabel
import KYDrawerController

//MARK: - enum
enum MBFreeResourceType : String {
    case Internet   = "DATA"
    case SMS        = "SMS"
    case Call       = "VOICE"
}

enum MBPostPaidTempletType : String {
    case IndividualCorporate    = "1"
    case TempleteTwo            = "2"
    case FullAndPartialPayment  = "3"
}

class DashboardVC: BaseVC {
    
    //MARK: - Properties
    var isRoamingSelected: Bool = false
    
    var freeResources : FreeResources?
    var homePageInformation : Homepage?
    
    
    //MARK: - IBOutlet
    @IBOutlet var topViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet var middleVIewHeightConstraint: NSLayoutConstraint!
    @IBOutlet var title_lbl: UILabel!
    @IBOutlet var notificationBage_lbl: UILabel!
    @IBOutlet var packageName_lbl: MarqueeLabel!
    @IBOutlet var packageTitle_lbl: UILabel!
    @IBOutlet var active_lbl: UILabel!
    @IBOutlet var roaming_btn: UIButton!
    @IBOutlet var ordinary_btn: UIButton!
    @IBOutlet var roamingTitle_lbl: UILabel!
    @IBOutlet var roamingTitle_lbl: UILabel!
    @IBOutlet var packageRedirection_btn: UIButton!
    
    
    @IBOutlet var mrcCurrency_lbl: UILabel!
    @IBOutlet var middleView: UIView!
    
    @IBOutlet var mainWalletView: UIView!
    @IBOutlet var bonusWalletView: UIView!
    @IBOutlet var countryWideView: UIView!
    
    // Bottom progress Views
    @IBOutlet var creditView: UIView!
    @IBOutlet var creditProgressView: UIView!
    @IBOutlet var mrcView: UIView!
    @IBOutlet var installmemtView: UIView!
    
    @IBOutlet var navigateToInstallments_btn: UIButton!
    @IBOutlet var installmemtView1: UIView!
    @IBOutlet var installmemtView2: UIView!
    
    @IBOutlet var bottomViewTopConstraintWithMiddle: NSLayoutConstraint!
    @IBOutlet var collectionViewHeight: NSLayoutConstraint!
    @IBOutlet var centerAlignmentOfMiddleView: NSLayoutConstraint!
    
    @IBOutlet var paymentDaysLeft_lbl: UILabel!
    @IBOutlet var nextPaymentDate_lbl: UILabel!
    @IBOutlet var mrcDaysLeft_lbl: UILabel!
    
    @IBOutlet var mrcDate_lbl: UILabel!
    @IBOutlet var expiryDaysLeft_lbl: UILabel!
    @IBOutlet var expiryDate_lbl: UILabel!
    
    
    @IBOutlet var countryWide_lbl: MarqueeLabel!
    @IBOutlet var bonusWallet_lbl: MarqueeLabel!
    @IBOutlet var mainWallet_lbl: MarqueeLabel!
    
    @IBOutlet var credit_lbl: MarqueeLabel!
    @IBOutlet var expiration_lbl: MarqueeLabel!
    @IBOutlet var mrc_lbl: MarqueeLabel!
    @IBOutlet var mrcValue_lbl: MarqueeLabel!
    @IBOutlet var installment_lbl: MarqueeLabel!
    @IBOutlet var nextPayment_lbl: MarqueeLabel!
    
    
    @IBOutlet var bonusTime_lbl: UILabel!
    @IBOutlet var countryWidePrice_lbl: UILabel!
    @IBOutlet var walletSign_lbl:UILabel!
    
    // Icons
    @IBOutlet var first_img: UIImageView!
    @IBOutlet var second_img: UIImageView!
    @IBOutlet var third_img: UIImageView!
    
    @IBOutlet var rightArrowConstraint: NSLayoutConstraint!
    @IBOutlet var leftArrowConstraint: NSLayoutConstraint!
    @IBOutlet var walletPrice_lbl:UILabel!
    
    @IBOutlet var installmentFee_lbl: UILabel!
    @IBOutlet var mrcPrice_lbl: UILabel!
    @IBOutlet var creditPrice_lbl: UILabel!
    
    
    @IBOutlet var progressBar3: MBGradientProgressView!
    @IBOutlet var progressBar2: MBGradientProgressView!
    @IBOutlet var progressBar1: MBGradientProgressView!
    @IBOutlet var collectionView: UICollectionView!
    @IBOutlet var scrollView: UIScrollView!
    
    @IBOutlet var mrcCurrencyManatSignWidth: NSLayoutConstraint!
    fileprivate var refreshControl:UIRefreshControl?
    
    @IBOutlet var minAmountView: UIView!
    @IBOutlet var minAmountHeightConstraint: NSLayoutConstraint!
    @IBOutlet var minAmountTitleLabel: MarqueeLabel!
    @IBOutlet var minAmountValueLabel: UILabel!
    
    //MARK: - ViewController Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Set Marquee Animation of labels
        mainWallet_lbl.setupMarqueeAnimation()
        bonusWallet_lbl.setupMarqueeAnimation()
        countryWide_lbl.setupMarqueeAnimation()
        
        credit_lbl.setupMarqueeAnimation()
        expiration_lbl.setupMarqueeAnimation()
        mrc_lbl.setupMarqueeAnimation()
        mrcValue_lbl.setupMarqueeAnimation()
        installment_lbl.setupMarqueeAnimation()
        nextPayment_lbl.setupMarqueeAnimation()
        packageName_lbl.setupMarqueeAnimation()
        
        // Set Label colors
        self.mainWalletView.layer.borderColor = UIColor.MBBorderGrayColor.cgColor
        packageName_lbl.textColor = UIColor.MBTextGrayColor
        packageTitle_lbl.text = ""
        
        // Setting Notification bage number
        notificationBage_lbl.layer.cornerRadius = 10
        notificationBage_lbl.layer.masksToBounds = true
        notificationBage_lbl.textColor = UIColor.MBRedColor
        
        collectionView.delegate = self
        collectionView.dataSource = self
        
        // Set UIRefreshControl
        scrollView.alwaysBounceVertical = true
        refreshControl = UIRefreshControl()
        refreshControl?.tintColor = UIColor.white
        refreshControl?.addTarget(self, action: #selector(didPullToRefresh(sender:)), for: .valueChanged)
        scrollView.addSubview(refreshControl!)
        
        // Setting Top layout
        setTopViewHeightAndCollectionViewLayout()
        
        scrollToIndexOfProgressView(item: 11+200)
        
        
        //setting number of views of this screen
        UserDefaults.standard.set(UserDefaults.standard.integer(forKey: "DashboardScreenViews")+1, forKey: "DashboardScreenViews")
        UserDefaults.standard.synchronize()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.interactivePop(false)
        
        // Setting ViewController localized text
        layoutViewController()
        // Check and call homepage api, when user open and close drawer no API call on Dashboard.
        if MBUserSession.shared.shouldReloadDashboardData == true {
            
            /* Reset check and load dashboard information */
            MBUserSession.shared.shouldReloadDashboardData = false
            
            // API call to reload HomePage Data
            getHomePageInformation(sender: nil,
                                   shouldShowLoader: true,
                                   callOtherAPIS: false)
        } else {
            _ = loadHomePageInformation()
        }
        
        // Redirect user to notification screen if user select screen when application is close
        if MBUserSession.shared.isPushNotificationSelected {
            
            // Reset to false
            MBUserSession.shared.isPushNotificationSelected = false
            
            if let notifications = self.myStoryBoard.instantiateViewController(withIdentifier: "NotificationsVC") as? NotificationsVC {
                MBUserSession.shared.unreadNotificaitonCount = "0"
                self.navigationController?.pushViewController(notifications, animated: true)
            }
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        /* show RateUs alert if user curently on dashboard */
        //self.checkAndLoadRateUsAlert()
        
        
        if let survey = MBUserSession.shared.appSurvays?.surveys?.first(where: { $0.screenName ?? "" == SurveyScreenName.dashboard.rawValue}),
           survey.surveyCount?.toInt ?? 0 < survey.surveyLimit?.toInt ?? 0,
           survey.visitLimit != "-1",
           survey.visitLimit?.toInt ?? 0 <= UserDefaults.standard.integer(forKey: "DashboardScreenViews") {
            self.viewDidLayoutSubviews()
            
            self.showSimpleSurvey(survey: survey, surveryScreen: SurveyScreenName.dashboard)
        }
        
        //setting number of views of this screen
        UserDefaults.standard.set(UserDefaults.standard.integer(forKey: "DashboardScreenViews")+1, forKey: "DashboardScreenViews")
        UserDefaults.standard.synchronize()
    }
    
    //MARK: - Remove observes
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    //MARK: - IBACTIONS
    @IBAction func roamingPressed(_ sender: Any) {
        
        collectionView.flipAnimation()
        
        if isRoamingSelected {
            isRoamingSelected = false
            //EventLog
            MBAnalyticsManager.logEvent(screenName: "Dashboard Screen", contentType:"Switch to Normal Usage" , status:"Success" )
            
        } else {
            isRoamingSelected = true
            //EventLog
            MBAnalyticsManager.logEvent(screenName: "Dashboard Screen", contentType:"Switch to Roaming Usage" , status:"Success" )
        }
        self.collectionView.reloadData()
        loadInitialCircle()
    }
    
    @IBAction func navigateToLoan(_ sender: UIButton) {
        
        if let loan = self.myStoryBoard.instantiateViewController(withIdentifier: "LoanVC") as? LoanVC {
            self.navigationController?.pushViewController(loan, animated: true)
        }
    }
    
    @IBAction func notificationsPressed(_ sender: UIButton) {
        
        if let notifications = self.myStoryBoard.instantiateViewController(withIdentifier: "NotificationsVC") as? NotificationsVC {
            MBUserSession.shared.unreadNotificaitonCount = "0"
            self.navigationController?.pushViewController(notifications, animated: true)
        }
        self.notificationBage_lbl.isHidden = true
    }
    
    @IBAction func navigateToTopUp(_ sender: UIButton) {
        
        if MBUserSession.shared.subscriberType() == .PrePaid {
            if let topUp = self.myStoryBoard.instantiateViewController(withIdentifier: "TopUpVC") as? TopUpVC {
                self.navigationController?.pushViewController(topUp, animated: true)
            }
        } else {
            // get index of tarrif from tabbar.
            let indexForTopUpTab = MBUserSession.shared.appMenu?.menuHorizontal?.firstIndex(where: { $0.identifier == "topup" })
            //  If found then select else select 0 index
            self.tabBarController?.selectedIndex = indexForTopUpTab ?? 0
        }
        
        
    }
    
    @IBAction func packagePressed(_ sender: UIButton) {
        
        // get index of tarrif from tabbar.
        if let indexForTariff = MBUserSession.shared.appMenu?.menuHorizontal?.firstIndex(where: { $0.identifier == "tariffs" }) {
            
            var tabIndexOfTariff = MBUserSession.shared.appMenu?.menuHorizontal?[indexForTariff].sortOrder.toInt ?? 0
            
            // Get index of tariff viewController
            if tabIndexOfTariff > 0 {
                tabIndexOfTariff = tabIndexOfTariff - 1
            }
            
            // Get instance of tariff viewController
            if let tariffVC = self.tabBarController?.viewControllers?[tabIndexOfTariff] as? TariffsMainVC {
                
                tariffVC.redirectedFromHome = true
                
                tariffVC.offeringIdFromNotification = MBUserSession.shared.userInfo?.offeringId ?? ""
                
                if packageTitle_lbl.text == Localized("Title_UnPaid") {
                    tariffVC.isPaid = false
                    
                } else {
                    tariffVC.isPaid = true
                }
            }
            self.tabBarController?.selectedIndex = tabIndexOfTariff
        }
    }
    
    // Used in collection View
    @IBAction func addCallButtonPressed(_ sender: Any) {
        
        redirectUserToSupplementaryController(type: .Call)
    }
    
    @IBAction func addInternetButtonPressed(_ sender: Any) {
        
        redirectUserToSupplementaryController(type: .Internet)
    }
    
    @IBAction func addSMSButtonPressed(_ sender: Any) {
        
        redirectUserToSupplementaryController(type: .SMS)
    }
    
    @IBAction func detailCallButtonPressed(_ sender: Any) {
        
        redirectUserToMySubscribeController(type: .Call)
    }
    
    @IBAction func detailInternetButtonPressed(_ sender: Any) {
        
        redirectUserToMySubscribeController(type: .Internet)
    }
    
    @IBAction func detailSMSButtonPressed(_ sender: Any) {
        
        redirectUserToMySubscribeController(type: .SMS)
    }
    
    @IBAction func navigateToInstallmentsPressed(_ sender: UIButton) {
        
        if let installments = self.myStoryBoard.instantiateViewController(withIdentifier: "MyInstallmentsVC") as? MyInstallmentsVC {
            self.navigationController?.pushViewController(installments, animated: true)
        }
    }
    
    //MARK: - Functions
    
    /**
     Set localized text in viewController
     */
    func layoutViewController() {
        
        title_lbl.font = UIFont.MBArial(fontSize: 20)
        packageTitle_lbl.font = UIFont.MBArial(fontSize: 14)
        title_lbl.text = Localized("Title_DashBoard")
        roamingTitle_lbl.text = Localized("Title_Roaming")
    }
    
    /**
     DashBoard usage circle layout setting
     */
    func setTopViewHeightAndCollectionViewLayout(){
        
        let screenSize = UIScreen.main.bounds.size
        let layout = UPCarouselFlowLayout()
        
        if screenSize.height == 480 {
            //iPhone 4
            topViewHeightConstraint.constant = 175
            centerAlignmentOfMiddleView.constant = 40
            collectionViewHeight.constant = 140
            layout.itemSize = CGSize(width: (screenSize.width - layout.minimumInteritemSpacing)/2.5 , height: 128)
            
        } else {
            centerAlignmentOfMiddleView.constant = 40
            
            if screenSize.height == 568 {
                //iPhone 5
                topViewHeightConstraint.constant = 200
                collectionViewHeight.constant = 152
                layout.itemSize = CGSize(width: (screenSize.width - layout.minimumInteritemSpacing)/2.5 , height: 150)
                
            }  else if screenSize.height == 667 {
                //iPhone 6
                topViewHeightConstraint.constant = 250
                leftArrowConstraint.constant = -70
                rightArrowConstraint.constant = 70
                collectionViewHeight.constant = 182
                layout.itemSize = CGSize(width: (screenSize.width - layout.minimumInteritemSpacing)/2.5 , height: 170)
                
            } else if screenSize.height == 736 {
                //iPhone 6+
                topViewHeightConstraint.constant = 290
                leftArrowConstraint.constant = -85
                rightArrowConstraint.constant = 85
                collectionViewHeight.constant = 212
                layout.itemSize = CGSize(width: (screenSize.width - layout.minimumInteritemSpacing)/2.5 , height: 200)
                
            } else if screenSize.height == 812 {
                //iPhone
                topViewHeightConstraint.constant = 290
                leftArrowConstraint.constant = -85
                rightArrowConstraint.constant = 85
                collectionViewHeight.constant = 212
                layout.itemSize = CGSize(width: (screenSize.width - layout.minimumInteritemSpacing)/2.5 , height: 200)
            } else {
                //iPad
                topViewHeightConstraint.constant = 290
                leftArrowConstraint.constant = -85
                rightArrowConstraint.constant = 85
                collectionViewHeight.constant = 212
                layout.itemSize = CGSize(width: (screenSize.width - layout.minimumInteritemSpacing)/2.5 , height: 200)
            }
        }
        
        layout.scrollDirection = .horizontal
        layout.sideItemAlpha = 1.0
        layout.spacingMode = UPCarouselFlowLayoutSpacingMode.fixed(spacing: 24)
        collectionView.collectionViewLayout = layout
        
    }
    /**
     load HomePageInformation from userDefaults.
     
     - returns: void
     */
    func loadHomePageInformation() -> Bool {
        
        
        var isDataFoundOfHomePage: Bool = false
        
        /*Loading initial data from UserDefaults*/
        
        if let homePageResponse :Homepage = Homepage.loadFromUserDefaults(key: APIsType.dashboard.selectedLocalizedAPIKey()) {
            
            // Set check ture to indicate data loaded from defaults
            isDataFoundOfHomePage = true
            
            if MBUserSession.shared.subscriberType() == .PrePaid {
                
                MBUserSession.shared.balance = homePageResponse.balance?.prepaid?.mainWallet
                
                //EventLog
                if MBUserSession.shared.brandName() == MBBrandName.Klass{
                    MBAnalyticsManager.logEvent(screenName: "Dashboard Screen", contentType:"LogedIn User Type \(MBSubscriberType.PrePaid), \(MBBrandName.Klass)" , status:"Success" )
                }
                else{
                    MBAnalyticsManager.logEvent(screenName: "Dashboard Screen", contentType:"LogedIn User Type \(MBSubscriberType.PrePaid), \(MBBrandName.CIN)" , status:"Success" )
                }
            } else {
                MBUserSession.shared.balance = nil
                
                //EventLog
                if MBUserSession.shared.userCustomerType() == MBCustomerType.Corporate{
                    MBAnalyticsManager.logEvent(screenName: "Dashboard Screen", contentType:"LogedIn User Type \(MBSubscriberType.PostPaid), \(MBCustomerType.Corporate)" , status:"Success" )
                } else {
                    MBAnalyticsManager.logEvent(screenName: "Dashboard Screen", contentType:"LogedIn User Type \(MBSubscriberType.PostPaid), \(MBCustomerType.Individual)" , status:"Success" )
                }
            }
            
            self.setHomeLayoutWithData(homeResponse: homePageResponse)
            self.homePageInformation = homePageResponse
            
        } else {
            // Clear homePAge data and show indicator
            self.setHomeLayoutWithData(homeResponse: Homepage())
        }
        
        
        return isDataFoundOfHomePage
    }
    
    /**
     set HomeLayout using provided data.
     
     - parameter homeResponse: Homepage data object.
     
     - returns: void
     */
    func setHomeLayoutWithData(homeResponse: Homepage, isAPIResponded :Bool = false) {
        
        /* If viewController view is not loaded yet and called after Menu API then don't load information*/
        if self.isViewLoaded  == false {
            return
        }
        
        //setting User Info
        packageName_lbl.text = MBUserSession.shared.userInfo?.offeringNameDisplay ?? ""
        active_lbl.text = MBUserSession.shared.userInfo?.status ?? ""
        
        if MBUserSession.shared.isStatusActive() {
            active_lbl.textColor = UIColor.MBGreen
            active_lbl.text = Localized("Info_Active")
            
        } else {
            active_lbl.text = MBUserSession.shared.userInfo?.status
            active_lbl.textColor = UIColor.MBRedColor
        }
        
        if homeResponse.balance?.minAmountTeBePaid?.isBlank == false {
            self.minAmountView.isHidden = false
            self.minAmountHeightConstraint.constant = 30
            self.minAmountTitleLabel.text = homeResponse.balance?.minAmountLabel
            self.minAmountValueLabel.attributedText = MBUtilities.createAttributedTextWithManatSign(homeResponse.balance?.minAmountTeBePaid, textColor: .black)
        } else {
            self.minAmountView.isHidden = true
            self.minAmountHeightConstraint.constant = 0
        }
        
        
        MBUserSession.shared.dateFormatter.dateFormat = Constants.kHomeDateFormate
        
        // Setting top Progress circles
        self.freeResources = homeResponse.freeResources
        self.collectionView.reloadData()
        loadInitialCircle()
        
        // Setting balance layout
        var credit : Credit?
        var mrc : Mrc?
        var installments: Installments?
        let subViews = self.middleView.subviews
        for subview in subViews{
            if subview.classForCoder == PostpaidSetupView.classForCoder() {
                subview.removeFromSuperview()
            }
        }
        
        if MBUserSession.shared.subscriberType() == .PostPaid {
            
            //All label value will be changed to "" string
            mainWalletView.isHidden = true
            bonusWalletView.isHidden = true
            countryWideView.isHidden = true
            
            //SETTING MiddleView for Postpaid
            if let postPaid = homeResponse.balance?.postpaid {
                
                if let postPaidMiddleView : PostpaidSetupView =  PostpaidSetupView.instanceFromNib() as? PostpaidSetupView {
                    
                    var middleViewHeight : CGFloat = 142
                    //FullAndPartialPayment
                    if postPaid.template == MBPostPaidTempletType.FullAndPartialPayment.rawValue {
                        
                        middleViewHeight = postPaidMiddleView.setupMiddleViewLayout(postPaidData: postPaid, type: .FullAndPartialPayment)
                        
                    } else if postPaid.template == MBPostPaidTempletType.IndividualCorporate.rawValue{
                        //postPaidHidden
                        
                        middleViewHeight = postPaidMiddleView.setupMiddleViewLayout(postPaidData: postPaid, type: .IndividualCorporate)
                        
                    } else if postPaid.template == MBPostPaidTempletType.TempleteTwo.rawValue{
                        //postPaidHidden
                        
                        middleViewHeight = postPaidMiddleView.setupMiddleViewLayout(postPaidData: postPaid, type: .TempleteTwo)
                        
                    }
                    
                    credit = nil
                    mrc = homeResponse.mrc
                    installments = homeResponse.installments
                    
                    middleVIewHeightConstraint.constant = middleViewHeight
                    middleView.setNeedsLayout()
                    // remove allViews first
//                    middleView.removeAllSubViews()
                    //Add new detailView
                    middleView.addSubview(postPaidMiddleView)
                    middleView.updateConstraintsIfNeeded()
                    
                    postPaidMiddleView.snp.makeConstraints { make in
                        make.top.equalTo(middleView.snp.top).offset(0.0)
                        make.right.equalTo(middleView.snp.right).offset(0.0)
                        make.left.equalTo(middleView.snp.left).offset(0.0)
                        make.bottom.equalTo(middleView.snp.bottom).offset(0.0)
                    }
                }
            }
            
        } else {
            
            middleVIewHeightConstraint.constant = 86
            middleView.updateConstraintsIfNeeded()
            //SETTING Prepaid MiddleView
            if let prepaidData = homeResponse.balance?.prepaid  {
                
                credit = homeResponse.credit
                mrc = homeResponse.mrc
                installments = homeResponse.installments
                
                
                // Main Wallet
                if prepaidData.mainWallet?.amount.isBlank ?? true {
                    mainWalletView.isHidden = true
                    
                } else {
                    mainWalletView.isHidden = false
                    self.first_img.image = UIImage(named: "main")
                    self.mainWallet_lbl.text = prepaidData.mainWallet?.balanceTypeName ?? ""
                    
                    let amount = prepaidData.mainWallet?.amount.toDouble ?? 0.0
                    self.walletPrice_lbl.text = prepaidData.mainWallet?.amount
                    
                    if (amount <= prepaidData.mainWallet?.lowerLimit.toDouble ?? 0.0) {
                        self.walletPrice_lbl.textColor = UIColor.MBRedColor
                        self.walletSign_lbl.textColor = UIColor.MBRedColor
                    } else {
                        self.walletPrice_lbl.textColor = UIColor.black
                        self.walletSign_lbl.textColor = UIColor.black
                    }
                }
                
                // Bonus Wallet
                if prepaidData.bounusWallet?.amount.isBlank ?? true {
                    bonusWalletView.isHidden = true
                    
                } else {
                    bonusWalletView.isHidden = false
                    self.second_img.image = UIImage(named: "bonus")
                    self.bonusWallet_lbl.text = prepaidData.bounusWallet?.balanceTypeName ?? ""
                    self.bonusTime_lbl.text = prepaidData.bounusWallet?.amount ?? ""
                    
                }
                
                // Country Wide
                
                if prepaidData.countryWideWallet?.amount.isBlank ?? true {
                    countryWideView.isHidden = true
                } else {
                    countryWideView.isHidden = false
                    self.third_img.image = UIImage(named: "country")
                    self.countryWide_lbl.text = prepaidData.countryWideWallet?.balanceTypeName ?? ""
                    self.countryWidePrice_lbl.text = prepaidData.countryWideWallet?.amount ?? ""
                }
            } else {
                
                //All label value will be changed to "" string
                mainWalletView.isHidden = true
                bonusWalletView.isHidden = true
                countryWideView.isHidden = true
            }
        }
        
        // credit detail progress setting
        if let credit = credit {
            if !credit.creditTitleValue.isBlank{
                creditView.isHidden = false
                
                self.credit_lbl.text = credit.creditTitleLabel
                self.creditPrice_lbl.text = credit.creditTitleValue
                self.expiration_lbl.text = credit.creditDateLabel
                
                let computedValuesFromDates = MBUtilities.calculateProgressValues(from: credit.creditInitialDate, to: credit.creditDate)
                
                
                if computedValuesFromDates.endDate.isBlank == false {
                    
                    creditProgressView.isHidden = false
                    
                    // show end date on credit
                    self.expiryDate_lbl.text = computedValuesFromDates.endDate
                    
                    // show days left in expiring credit
                    //updated by MIA
                    var daysLeftUnit = ""
                    if credit.remainingCreditDays.toInt <= 1 {
                        daysLeftUnit = Localized("Info_day")
                    } else {
                        daysLeftUnit = Localized("Info_days")
                    }
                    let daysLeftDisplayValue = "\(credit.remainingCreditDays) \(daysLeftUnit)"
                    self.expiryDaysLeft_lbl.text = daysLeftDisplayValue//computedValuesFromDates.daysLeftDisplayValue
                    
                    // Highlight days left value
                    if computedValuesFromDates.daysLeft < credit.creditLimit.toInt {
                        self.expiryDaysLeft_lbl.textColor = UIColor.MBRedColor
                    } else {
                        self.expiryDaysLeft_lbl.textColor = UIColor.black
                    }
                    // set progress value
                    //updated by MIA
                    let startValue = Float(credit.progressDays ?? "0") ?? 0.0
                    let maxValue = Float(credit.creditDays ?? "1") ?? 1.0
                    let progressValue = 1.0 - ( maxValue - startValue) / maxValue
                    
                    self.progressBar1.setProgress(Float(progressValue)/*computedValuesFromDates.progressValue*/, animated: true)
                    
                } else {
                    creditProgressView.isHidden = true
                    self.expiryDate_lbl.text = ""
                }
            }
            else{
                creditView.isHidden = true
            }
            
        } else {
            creditView.isHidden = true
        }
        
        // Clear packageTitle
        packageTitle_lbl.text = ""
        
        var mrcStatusString = ""
        
        // Show MRC to specific CIN user
        if MBUserSession.shared.subscriberType() == .PostPaid ||
            (MBUserSession.shared.subscriberType() == .PrePaid &&
                (MBUserSession.shared.brandName() == MBBrandName.Klass ||
                    (MBUserSession.shared.brandName() == MBBrandName.CIN &&
                        (MBUserSession.shared.userInfo?.offeringId == "1770083072" ||
                            MBUserSession.shared.userInfo?.cinTariff?.isEqualToStringIgnoreCase(otherString: "show") ?? false )))) {
            
            // MRC detail progress setting
            if let mrc = mrc {
                
                // setting date value to set packageTitle label value to paid or umpaid
                mrcStatusString = mrc.mrcStatus
                
                if !mrc.mrcTitleLabel.isBlank{
                    mrcView.isHidden = false
                    
                    //check if MRC price is coming, otherwise hide manat aswell
                    if mrc.mrcTitleValue.isBlank == false {
                        
                        self.mrcPrice_lbl.isHidden = false
                        self.mrcPrice_lbl.text = mrc.mrcTitleValue
                        
                        // Check if Value is free then hidden Manat Sign
                        if mrc.mrcTitleValue.isHasFreeText() {
                            mrcCurrencyManatSignWidth.constant = 0
                            self.mrcCurrency_lbl.isHidden = true
                        } else {
                            mrcCurrencyManatSignWidth.constant = 8
                            self.mrcCurrency_lbl.isHidden = false
                        }
                        
                    } else{
                        self.mrcPrice_lbl.isHidden = true
                        self.mrcCurrency_lbl.isHidden = true
                        mrcCurrencyManatSignWidth.constant = 0
                    }
                    
                    self.mrcValue_lbl.text = mrc.mrcTitleLabel
                    self.mrc_lbl.text = mrc.mrcDateLabel
                    
                    //If date or initial date is not coming, show expired in date and show progress bar to Full and days to 0
                    
                    if mrc.mrcDate.isBlank == false && mrc.mrcInitialDate.isBlank == false {
                        
                        let computedValuesFromDates = MBUtilities.calculateProgressValuesForMRC(from: mrc.mrcInitialDate, to: mrc.mrcDate, type: mrc.mrcType)
                        
                        // Set end date
                        self.mrcDate_lbl.text = computedValuesFromDates.endDate
                        
                        // Set days left values
                        self.mrcDaysLeft_lbl.text = computedValuesFromDates.daysLeftDisplayValue
                        
                        // Highlight left days value
                        if computedValuesFromDates.daysLeft < mrc.mrcLimit.toInt {
                            self.mrcDaysLeft_lbl.textColor = UIColor.MBRedColor
                        } else {
                            self.mrcDaysLeft_lbl.textColor = UIColor.black
                        }
                        
                        if mrc.mrcStatus.isEqualToStringIgnoreCase(otherString: "UnPaid") && MBUserSession.shared.subscriberType() == .PrePaid {
                            // set progress value
                            self.progressBar2.setProgress(1, animated: true)
                            self.mrcDaysLeft_lbl.textColor = UIColor.MBRedColor
                            
                            var mrcTypeString = ""
                            if mrc.mrcType.isEqualToStringIgnoreCase(otherString: "daily") {
                                mrcTypeString = "0 \(Localized("title_Hours"))"
                                
                            } else {
                                mrcTypeString = "0 \(Localized("Info_day"))"
                            }
                            
                            self.mrcDaysLeft_lbl.text = mrcTypeString
                            
                        } else {
                            self.progressBar2.setProgress(computedValuesFromDates.progressValue, animated: true)
                        }
                        
                    } else {
                        self.mrcDaysLeft_lbl.textColor = UIColor.MBRedColor
                        self.mrcDate_lbl.text = Localized("Expired_Date")
                        
                        var mrcTypeString = ""
                        if mrc.mrcType.isEqualToStringIgnoreCase(otherString: "daily") {
                            mrcTypeString = "0 \(Localized("title_Hours"))"
                            
                        } else {
                            mrcTypeString = "0 \(Localized("Info_day"))"
                        }
                        
                        self.mrcDaysLeft_lbl.text = mrcTypeString
                        self.progressBar2.setProgress(1, animated: true)
                    }
                    
                } else {
                    mrcView.isHidden = true
                }
                
            } else {
                mrcView.isHidden = true
            }
            
        } else {
            mrcView.isHidden = true
        }
        
        
        // Check whether package is expired or not
        if (MBUserSession.shared.brandName() == MBBrandName.Klass && MBUserSession.shared.subscriberType() == .PrePaid) ||
            (MBUserSession.shared.brandName() == MBBrandName.CIN && MBUserSession.shared.userInfo?.offeringId == "1770083072") {
            
            if mrcStatusString.isBlank == false {
                
                // if date is passsed then show uppaid
                if mrcStatusString.isEqualToStringIgnoreCase(otherString: "Paid") {
                    packageTitle_lbl.text = Localized("Title_Paid")
                    packageTitle_lbl.textColor = UIColor.MBTextGrayColor
                } else {
                    packageTitle_lbl.text = Localized("Title_UnPaid")
                    packageTitle_lbl.textColor = UIColor.MBBarOrange
                }
            } else {
                // if MRC date not found show unpaid
                packageTitle_lbl.text = Localized("Title_UnPaid")
                packageTitle_lbl.textColor = UIColor.MBBarOrange
            }
            
        } else {
            packageTitle_lbl.text = ""
        }
        
        // installments detail progress setting
        if let installments = installments {
            
            self.installment_lbl.text = installments.installmentTitle
            installmentFee_lbl.text = ""
            
            if installments.installments?.count ?? 0 > 0 {
                
                if let aInstallment = installments.installments?[0] {
                    
                    //                    installmemtView2.isHidden = false
                    installmemtView.isHidden = false
                    self.navigateToInstallments_btn.isHidden = false
                    
                    self.installmentFee_lbl.text = aInstallment.amountValue
                    self.nextPayment_lbl.text = aInstallment.nextPaymentLabel
                    
                    
                    let computedValuesFromDates = MBUtilities.calculateProgressValuesForInstallment(from: aInstallment.nextPaymentInitialDate, to: aInstallment.nextPaymentValue)
                    
                    // Set end date
                    self.nextPaymentDate_lbl.text = computedValuesFromDates.endDate
                    
                    // display days left in expiring
                    self.paymentDaysLeft_lbl.text = computedValuesFromDates.daysLeftDisplayValue
                    
                    // Highlight days left
                    if Double(computedValuesFromDates.daysLeft) < aInstallment.installmentFeeLimit.toDouble {
                        self.paymentDaysLeft_lbl.textColor = UIColor.MBRedColor
                    } else {
                        self.paymentDaysLeft_lbl.textColor = UIColor.black
                    }
                    
                    // Set progress value
                    self.progressBar3.setProgress(computedValuesFromDates.progressValue, animated: true)
                    
                } else {
                    // installmemtView2.isHidden = true
                    installmemtView.isHidden = true
                    self.navigateToInstallments_btn.isHidden = true
                }
                
            } else {
                //                installmemtView2.isHidden = true
                installmemtView.isHidden = true
                self.navigateToInstallments_btn.isHidden = true
            }
            
        } else {
            installmemtView.isHidden = true
            self.navigateToInstallments_btn.isHidden = true
        }
        
        
        if isAPIResponded == true &&
            (MBUserSession.shared.promoMessage?.isBlank ?? true) == false {
            
            /* Show promo message to user */
            //self.showAlertWithTitleAndMessage(title: "", message: MBUserSession.shared.promoMessage)
            self.showAlertWithTitleAndMessage(title: "", message: MBUserSession.shared.promoMessage) { (aString) in
                self.checkAndLoadRateUsAlert()
            }
            /* Clear Promo message, Only show it once */
            MBUserSession.shared.promoMessage = nil
        } else {
            //call rateUs API
//            print("checkAndLoadRateUsAlert is called at setHomeLayoutWithData")
//            self.checkAndLoadRateUsAlert()
        }
        
    }
    
    
    func checkAndLoadRateUsAlert() {
        
        /* Check if dashboard visible then check where to show rateus or not. */
        if let kyDrawerController = self.navigationController?.visibleViewController,
            kyDrawerController.isKind(of: KYDrawerController.classForCoder()),
            let selectedVC = self.tabBarController?.selectedViewController,
            selectedVC.isKind(of: self.classForCoder),
            (MBUserSession.shared.userInfo?.rateUsIOS?.isEqualToStringIgnoreCase(otherString: "0") ?? false),
            let parentTabbarController = self.tabBarController as? TabbarController,
            parentTabbarController.isAppResumeAPICallInProcess == false {
            
            var showRateUsAlert :Bool = false
            
            /* Check is Rate Us is presented to user before*/
            if MBUtilities.isRateUsShownBefore() == false {
                
                let timeDiffranceInHours = MBDateUtilities.calculateTotalHoursBetweenTwoDates(startingDate: MBUtilities.getLoginTime(), endingDate: MBUtilities.todayDateString(dateFormate: Constants.kHomeDateFormate), dateFormate: Constants.kHomeDateFormate)
                
                if timeDiffranceInHours >= (MBUserSession.shared.userInfo?.firstPopup?.toInt ?? 0) {
                    showRateUsAlert = true
                }
                
            } else {
                let timeDiffranceInDays = MBDateUtilities.calculateTotalDaysBetween(startingDate: MBUtilities.getRateUsLaterTime(), endingDate: MBUtilities.todayDateString(dateFormate: Constants.kHomeDateFormate), dateFormate: Constants.kHomeDateFormate)
                
                if timeDiffranceInDays >= (MBUserSession.shared.userInfo?.lateOnPopup?.toInt ?? 0) {
                    showRateUsAlert = true
                }
            }
            
            DispatchQueue.main.async {
                /* Show Alert if it matched above conditions */
                if showRateUsAlert ,
                    let rateUsAlert : RateUsAlertVC = RateUsAlertVC.fromNib() {
                    rateUsAlert.setRateUs({_ in
                        
                        self.userDidOpenAppStoreToRateUS()
                        
                    })
                    rateUsAlert.setRateUsLater({_ in
                        self.checkNotificationsPopupConfiguration()
                    })
                    
                    self.presentPOPUP(rateUsAlert, animated: true, completion: nil)
                } else {
                    //as per discussion with Junaid, call getnotificaioncount API only if showRateUsAlert is false.
                    self.checkNotificationsPopupConfiguration()
                }
            }
        } else {
            if (MBUserSession.shared.userInfo?.rateUsIOS?.isEqualToStringIgnoreCase(otherString: "1") ?? false) {
                self.checkNotificationsPopupConfiguration()
            }
        }
        
    }
    
    func checkNotificationsPopupConfiguration () {
        let timeDiffranceForNotificationPopup = MBDateUtilities.calculateTotalHoursBetweenTwoDates(startingDate: MBUtilities.getNotificaitonsPopupTime(), endingDate: MBUtilities.todayDateString(dateFormate: Constants.kHomeDateFormate), dateFormate: Constants.kHomeDateFormate)
//        timeDiffranceForNotificationPopup
        if MBUtilities.getNotificaitonsPopupTime() == "" && MBUserSession.shared.unreadNotificaitonCount.toInt > 0 {
            if MBUserSession.shared.notificationPopupConfig?.popupMessage.isBlank == false {
                DispatchQueue.main.async {
                    if let alert = self.myStoryBoard.instantiateViewController(withIdentifier: "AlertVC") as? AlertVC {
                        self.presentPOPUP(alert, animated: true, completion: nil)
                        
                        alert.setAlertForUnreadNotifications(title: "", message: MBUserSession.shared.notificationPopupConfig?.popupMessage ?? "")
                        MBUtilities.updateNotificaitonsPopupTime()
                        
                        alert.setNoButtonCompletionHandler(completionBlock: { (aString) in
                            // if user clicks on OK
                            //self.updateCustomerEmail(newEmail: self.email_txt.text)
                            MBUserSession.shared.unreadNotificaitonCount = "0"
                            if let notifications = self.myStoryBoard.instantiateViewController(withIdentifier: "NotificationsVC") as? NotificationsVC {
                                self.navigationController?.pushViewController(notifications, animated: true)
                            }
                            self.notificationBage_lbl.isHidden = true
                        })
                        
                    }
                }
            }
        } else if timeDiffranceForNotificationPopup >= (MBUserSession.shared.notificationPopupConfig?.hour.toInt ?? 0) {
            if MBUserSession.shared.notificationPopupConfig?.popupMessage.isBlank == false  && MBUserSession.shared.unreadNotificaitonCount.toInt > 0 {
                DispatchQueue.main.async {
                    if let alert = self.myStoryBoard.instantiateViewController(withIdentifier: "AlertVC") as? AlertVC {
                        self.presentPOPUP(alert, animated: true, completion: nil)
                        
                        alert.setAlertForUnreadNotifications(title: "", message: MBUserSession.shared.notificationPopupConfig?.popupMessage ?? "")
                        MBUtilities.updateNotificaitonsPopupTime()
                        
                        alert.setNoButtonCompletionHandler(completionBlock: { (aString) in
                            // if user clicks on OK
                            //self.updateCustomerEmail(newEmail: self.email_txt.text)
                            MBUserSession.shared.unreadNotificaitonCount = "0"
                            if let notifications = self.myStoryBoard.instantiateViewController(withIdentifier: "NotificationsVC") as? NotificationsVC {
                                self.navigationController?.pushViewController(notifications, animated: true)
                            }
                            self.notificationBage_lbl.isHidden = true
                        })
                    }
                }
            }
        }
    }
    /**
     Called when UIRefreshConrol value changes.
     */
    @objc func didPullToRefresh(sender: UIRefreshControl?) {
        getHomePageInformation(sender: sender, shouldShowLoader: true, callOtherAPIS: true)
    }
    
    //MARK: - API Calls
    
    /**
     call to get HomePage Information.
     
     - parameter BrandId: loggedIn user brandName.
     - parameter SubscriberType: loggedIn user subscriber type.
     
     - returns: void
     */
    func getHomePageInformation(sender: UIRefreshControl?, shouldShowLoader :Bool = true, callOtherAPIS :Bool = true) {
        
        if shouldShowLoader {
            activityIndicator.showActivityIndicator()
        }
        
        /*API call to get data*/
        _ = MBAPIClient.sharedClient.homePage(BrandID: MBUserSession.shared.userInfo?.brandId ?? "", SubscriberType: MBUserSession.shared.subscriberTypeLowerCaseStr, offeringId: MBUserSession.shared.userInfo?.offeringId ?? "", { ( response, resultData, error, isCancelled, status, resultCode, resultDesc) in
            
            sender?.endRefreshing()
            
            if shouldShowLoader {
                self.activityIndicator.hideActivityIndicator()
            }
            
            if error != nil {
                error?.showServerErrorInViewController(self)
                
            } else {
                // handling data from API response.
                if resultCode == Constants.MBAPIStatusCode.succes.rawValue ,
                    let homePageResponse = Mapper<Homepage>().map(JSONObject:resultData) {
                    
                    
                    /*Save data into user defaults*/
                    homePageResponse.saveInUserDefaults(key: APIsType.dashboard.selectedLocalizedAPIKey())
                    
                    if MBUserSession.shared.subscriberType() == .PrePaid {
                        MBUserSession.shared.balance = homePageResponse.balance?.prepaid?.mainWallet
                    } else {
                        MBUserSession.shared.balance = nil
                    }
                    
                    homePageResponse.installments?.saveInUserDefaults(key: APIsType.installments.selectedLocalizedAPIKey())
                    
                    self.setHomeLayoutWithData(homeResponse: homePageResponse, isAPIResponded: true)
                    self.homePageInformation = homePageResponse
                    
                } else {
                    // Show error alert to user
                    self.showErrorAlertWithMessage(message: resultDesc)
                }
            }
            
            if callOtherAPIS {
                /* get number of unread notification counts*/
                /*this API will be called after the rateus*/
                self.getNotificationsCount()
                
//                self.getRateUsConfig()
            } else {
                // it is to check whether to show alert for rate us or not
                self.checkAndLoadRateUsAlert()
                
            }
            
        })
    }
    
    /**
     call to get unread Notifications count Information.
     */
    func getNotificationsCount() {
        
        /*API call to get data*/
        _ = MBAPIClient.sharedClient.getNotificationsCount({ ( response, resultData, error, isCancelled, status, resultCode, resultDesc) in
            
            // handling data from API response.
            if resultCode == Constants.MBAPIStatusCode.succes.rawValue {
                
                // Parssing response data
                if let notificationsCountResponse = Mapper<NotificationsCount>().map(JSONObject:resultData) {
                    
                    // setting notification unread count
                    if (notificationsCountResponse.notificationUnreadCount?.toInt ?? 0) > 0 {
                        MBUserSession.shared.unreadNotificaitonCount = (notificationsCountResponse.notificationUnreadCount ?? "").trimmWhiteSpace
                        self.notificationBage_lbl.isHidden = false
                        self.notificationBage_lbl.text = notificationsCountResponse.notificationUnreadCount?.trimmWhiteSpace
                    } else {
                        //Hide notification unread count label
                        self.notificationBage_lbl.isHidden = true
                        MBUserSession.shared.unreadNotificaitonCount = "0"
                    }
                }
                self.getRateUsConfig()
            }
        })
    }
    
    /**
     call to get RateUS.
     - returns: void
     */
    func getRateUsConfig() {
        
        /*API call to get data*/
        _ = MBAPIClient.sharedClient.getRateUs( { ( response, resultData, error, isCancelled, status, resultCode, resultDesc) in
            
            if resultCode == Constants.MBAPIStatusCode.succes.rawValue,
                let rateUsResponse = Mapper<RateUsModel>().map(JSONObject:resultData) {
                MBUserSession.shared.loggedInUsers?.currentUser?.userInfo?.rateUsIOS = rateUsResponse.rateus_ios
                MBUserSession.shared.saveLoggedInUsersCurrentStateInfo()
                if (MBUserSession.shared.promoMessage?.isBlank ?? true) == false {
                    /* Show promo message to user */
                    //self.showAlertWithTitleAndMessage(title: "", message: MBUserSession.shared.promoMessage)
                    self.showAlertWithTitleAndMessage(title: "", message: MBUserSession.shared.promoMessage) { (aString) in
                        self.checkAndLoadRateUsAlert()
                    }
                    /* Clear Promo message, Only show it once */
                    MBUserSession.shared.promoMessage = nil
                } else {
                    //call rateUs API
                    print("checkAndLoadRateUsAlert is called in getRateUsConfig()")
                    self.checkAndLoadRateUsAlert()
                }
            } else {
                self.checkAndLoadRateUsAlert()
            }
        })
    }
    
    func userDidOpenAppStoreToRateUS() {
        
        self.activityIndicator.showActivityIndicator()
        
        _ = MBAPIClient.sharedClient.rateUs({ (response, resultData, error, isCancelled, status, resultCode, resultDesc)  in
            
            self.activityIndicator.hideActivityIndicator()
            
            if error != nil {
                
                self.dismiss(animated: false, completion:{
                    error?.showServerErrorInViewController(self)
                })
                
            } else {
                /* Updated curent status of rating of application to rated */
                MBUserSession.shared.loggedInUsers?.currentUser?.userInfo?.rateUsIOS = "1"
                MBUserSession.shared.saveLoggedInUsersCurrentStateInfo()
                
                self.redirectUserToAppStore(completion: { (_) in
                    
                })
            }
            
        })
    }
}

// MARK: - CollectionView Delegate & DataSource
extension DashboardVC : UICollectionViewDelegate,UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 1000
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CarouselCollectionViewCell.identifier, for: indexPath) as? CarouselCollectionViewCell {
            
            
            if (indexPath.item % 3 == 0) { /* Call usage */
                
                cell.setLayoutValues(freeResourceData: getFreeResourcesFor(type: .Call), type: .Call, isRoaming: isRoamingSelected)
                
                cell.add_btn.addTarget(self, action: #selector(addCallButtonPressed(_:)), for: UIControl.Event.touchUpInside)
                //  cell.detail_btn.addTarget(self, action: #selector(detailCallButtonPressed(_:)), for: UIControlEvents.touchUpInside)
                
            } else if ((indexPath.item + 1) % 3 == 0) { /* SMS usage */
                
                cell.setLayoutValues(freeResourceData: getFreeResourcesFor(type: .SMS), type: .SMS, isRoaming: isRoamingSelected)
                
                cell.add_btn.addTarget(self, action: #selector(addSMSButtonPressed(_:)), for: UIControl.Event.touchUpInside)
                //  cell.detail_btn.addTarget(self, action: #selector(detailSMSButtonPressed(_:)), for: UIControlEvents.touchUpInside)
                
            } else { /* Internet usage */
                
                cell.setLayoutValues(freeResourceData: getFreeResourcesFor(type: .Internet), type: .Internet, isRoaming: isRoamingSelected)
                
                cell.add_btn.addTarget(self, action: #selector(addInternetButtonPressed(_:)), for: UIControl.Event.touchUpInside)
                //  cell.detail_btn.addTarget(self, action: #selector(detailInternetButtonPressed(_:)), for: UIControlEvents.touchUpInside)
            }
            
            return cell
        } else {
            return UICollectionViewCell()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if let cell : CarouselCollectionViewCell = collectionView.cellForItem(at: indexPath) as? CarouselCollectionViewCell {
            
            switch cell.viewType ?? .Call {
            case MBFreeResourceType.Call:
                redirectUserToMySubscribeController(type: .Call)
            case MBFreeResourceType.Internet:
                redirectUserToMySubscribeController(type: .Internet)
            case MBFreeResourceType.SMS:
                redirectUserToMySubscribeController(type: .SMS)
            }
        }
    }
    
    //MARK: - CollectionView helping method
    /**
     load initial usage circles.
     */
    func loadInitialCircle()  {
        
        var freeResources: [FreeResourceAndRoaming]?
        
        if isRoamingSelected {
            freeResources = self.freeResources?.freeResourcesRoaming
            
            if freeResources?.count ?? 0 <= 0 {
                roaming_btn.setImage(UIImage (imageLiteralResourceName: "roaming_pressed"), for: UIControl.State.normal)
            } else {
                roaming_btn.setImage(UIImage (imageLiteralResourceName: "roamingofferData"), for: UIControl.State.normal)
            }
            
        } else {
            freeResources = self.freeResources?.freeResources
            
            if self.freeResources?.freeResourcesRoaming?.count ?? 0 <= 0 {
                roaming_btn.setImage(UIImage (imageLiteralResourceName: "roaming_normal"), for: UIControl.State.normal)
            } else {
                roaming_btn.setImage(UIImage (imageLiteralResourceName: "noRoamingOfferData"), for: UIControl.State.normal)
            }
        }
        
        let typeOfData = typeOfDataInFreeResource(freeResources: freeResources)
        
        if typeOfData.contains(.Internet) {
            
            scrollToIndexOfProgressView(item: 11+200)
            
        } else if typeOfData.contains(.SMS) {
            
            // setting value 5 to mach design layout of circle at left and right
            scrollToIndexOfProgressView(item: 12+200)
            
        } else if typeOfData.contains(.Call) {
            // setting value 3 to mach design layout of circle at left and right
            scrollToIndexOfProgressView(item: 10 + 200)
        } else {
            scrollToIndexOfProgressView(item: 11+200)
        }
    }
    
    /**
     Help scrolling to IndexOf collectionView
     
     - parameter item: index where you want to scroll.
     
     - returns: void
     */
    func scrollToIndexOfProgressView(item: Int) {
        DispatchQueue.main.async {
            let index = IndexPath(item: item, section: 0)
            self.collectionView .scrollToItem(at: index, at: UICollectionView.ScrollPosition.centeredHorizontally, animated: true)
        }
    }
    
    /**
     Help to find type of data in FreeResourceAndRoaming objects array.
     
     - parameter freeResources: array of FreeResourceAndRoaming.
     
     - returns: type of data in freeResource array as MBFreeResourceType type array.
     */
    func typeOfDataInFreeResource(freeResources : [FreeResourceAndRoaming]?) -> [MBFreeResourceType] {
        
        var typeOfData : [MBFreeResourceType] = []
        
        freeResources?.forEach({ (aResource) in
            
            
            //Both values found Empty
            if aResource.resourceType.lowercased() == MBFreeResourceType.Call.rawValue.lowercased() {
                typeOfData.append(.Call)
                
            } else if aResource.resourceType.lowercased() == MBFreeResourceType.Internet.rawValue.lowercased() {
                
                typeOfData.append(.Internet)
            } else if aResource.resourceType.lowercased() == MBFreeResourceType.SMS.rawValue.lowercased() {
                
                typeOfData.append(.SMS)
            }
        })
        
        return typeOfData
    }
    
    /**
     Help to find and return FreeResourceAndRoaming object accourding to type parameter.
     
     - parameter type: type of data which is required.
     
     - returns: FreeResourceAndRoaming object accourding to type.
     */
    func getFreeResourcesFor(type : MBFreeResourceType) -> FreeResourceAndRoaming? {
        
        var freeResourceData : FreeResourceAndRoaming?
        
        if let freeResources = self.freeResources {
            
            if isRoamingSelected == true {
                
                if let freeResourcesRoaming = freeResources.freeResourcesRoaming {
                    
                    if let i = freeResourcesRoaming.firstIndex(where: { $0.resourceType.lowercased() == type.rawValue.lowercased() }) {
                        
                        freeResourceData = freeResourcesRoaming[i]
                    }
                }
            } else {
                
                if let freeResources = freeResources.freeResources {
                    
                    if let i = freeResources.firstIndex(where: { $0.resourceType.lowercased() == type.rawValue.lowercased() }) {
                        
                        freeResourceData = freeResources[i]
                    }
                }
            }
        }
        
        return freeResourceData
    }
    
    /**
     Redirect user to Supplementary screen and also set selectedTabType of SupplementaryVC
     
     - parameter type: type of usage circle which is selected.
     
     - returns: void.
     */
    func redirectUserToSupplementaryController(type: MBOfferTabType) {
        
        if let supplementoryOfferVC = self.myStoryBoard.instantiateViewController(withIdentifier: "SupplementaryVC") as? SupplementaryVC {
            
            if isRoamingSelected {
                supplementoryOfferVC.selectedTabType = .Roaming
                
            } else {
                supplementoryOfferVC.selectedTabType = type
            }
            self.navigationController?.pushViewController(supplementoryOfferVC, animated: true)
        }
    }
    
    /**
     Redirect user to Subscription screen and also set selectedTabType of MySubscribeVC
     
     - parameter type: type of usage circle add button which is selected.
     
     - returns: void.
     */
    func redirectUserToMySubscribeController(type: MBOfferTabType) {
        
        if let subscriptionVC = self.myStoryBoard.instantiateViewController(withIdentifier: "MySubscribeVC") as? MySubscribeVC {
            
            if isRoamingSelected {
                subscriptionVC.selectedTabType = .Roaming
            } else {
                subscriptionVC.selectedTabType = type
                subscriptionVC.selectedUsageType = type
            }
            subscriptionVC.isRedirectedFromHome = true
            
            self.navigationController?.pushViewController(subscriptionVC, animated: true)
        }
    }
}


