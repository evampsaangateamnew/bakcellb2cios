//
//  PaymentHistoryResponse.swift
//  Bakcell
//
//  Created by Awais Shehzad on 21/09/2017.
//  Copyright © 2017 evampsaanga. All rights reserved.
//

import Foundation
import ObjectMapper

class PaymentHistoryListHandler: Mappable {
    
    var paymentHistory : [PaymentHistory]?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        paymentHistory          <- map["paymentHistory"]
    }
}

//PaymentHistory : [...]
class PaymentHistory: Mappable {
    var loanID : String = ""
    var dateTime : String = ""
    var amount : String = ""
    
    required init?(map : Map) {
        
    }
    
    func mapping(map: Map) {
        
        loanID          <- map["loanID"]
        dateTime        <- map["dateTime"]
        amount          <- map["amount"]
    }
}
