//
//  FreeSMSVC.swift
//  Bakcell
//
//  Created by Saad Riaz on 6/20/17.
//  Copyright © 2017 evampsaanga. All rights reserved.
//

import UIKit
import ObjectMapper

class FreeSMSVC: BaseVC {
    
    // MARK: - IBOutlet
    @IBOutlet var title_lbl: UILabel!
    @IBOutlet var description_lbl: UILabel!
    @IBOutlet var number_txt: UITextField!
    @IBOutlet var symbolsLength_lbl: UILabel!
    @IBOutlet var SMSCount_lbl: UILabel!
    @IBOutlet var SMS_textView: UITextView!
    @IBOutlet var send_btn: UIButton!
    @IBOutlet var OnnetSMSLeftOverTitle_lbl: UILabel!
    @IBOutlet var OffnetSMSLeftOverTitle_lbl: UILabel!
    @IBOutlet var OnnetSMSLeftOverValue_lbl: UILabel!
    @IBOutlet var OffnetSMSLeftOverValue_lbl: UILabel!
    
    
    //MARK: - View controller Method
    override func viewDidLoad() {
        super.viewDidLoad()
        
        number_txt.delegate = self
        SMS_textView.delegate = self
        
        SMS_textView.layer.borderColor = UIColor.MBBorderGrayColor.cgColor
        send_btn.roundTopCorners(radius: CGFloat(Constants.kButtonCornerRadius))
        
        // Get remaining limit
        getFreeSMSStatus()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.interactivePop(true)
        
        // View Controller initial layout text
        layoutViewController()
    }
    //MARK: - IBAction
    @IBAction func sendPressed(_ sender: AnyObject) {
        
        var msisdn : String = ""
        // MSISDN validation
        if let mobileNumberText = number_txt.text,
            mobileNumberText.count == 9 {
            
            msisdn = mobileNumberText
        } else {
            self.showErrorAlertWithMessage(message: Localized("Message_EnterValidNumber"))
            return
        }
        
        if SMS_textView.text.isBlank {
            self.showErrorAlertWithMessage(message: Localized("Message_NoMessage"))
        } else {
            sendFreeSMS(recieverMsisdn: msisdn, textMessage: SMS_textView.text ?? "")
        }
    }
    
    //MARK: - Functions
    func layoutViewController() {
        
        title_lbl.text                  = Localized("Title_QuickServices")
        description_lbl.text            = Localized("Description_QuickServices")
        number_txt.placeholder          = Localized("placeHolder_RecieverNumber")
        SMS_textView.placeholder        = Localized("placeHolder_EnterText")
        symbolsLength_lbl.text          = "\(Localized("Info_SymbolsLength"))\(0)"
        SMSCount_lbl.text               = "\(Localized("Info_SMSCount"))\(0)"
        OnnetSMSLeftOverTitle_lbl.text  = Localized("Info_OnnetSMSLeftOver")
        OffnetSMSLeftOverTitle_lbl.text = Localized("Info_OffnetSMSLeftOver")
        send_btn.setTitle(Localized("BtnTitle_Send"), for: UIControl.State.normal)
        
    }
    
    //MARK: - API Calls
    /// Call 'getFreeSMSStatus' API .
    ///
    /// - returns: Void
    func getFreeSMSStatus() {
        
        /*Loading initial data from UserDefaults*/
        // Parssing response data
        if let freeSMSHandler : FreeSMS = FreeSMS.loadFromUserDefaults(key: APIsType.freeSMSStatus.selectedLocalizedAPIKey()) {
            
            /*Setting limit values*/
            self.OnnetSMSLeftOverValue_lbl.text  = freeSMSHandler.onNetSMS ?? "0"
            self.OffnetSMSLeftOverValue_lbl.text = freeSMSHandler.offNetSMS ?? "0"
            
        } else {
            activityIndicator.showActivityIndicator()
        }
        
        /*API call tpo get data*/
        _ = MBAPIClient.sharedClient.getFreeSMSStatus({ (response, resultData, error, isCancelled, status, resultCode, resultDesc) in
            self.activityIndicator.hideActivityIndicator()
            if error != nil {
                error?.showServerErrorInViewController(self)
                
            } else {
                // handling data from API response.
                if resultCode == Constants.MBAPIStatusCode.succes.rawValue {
                    
                    // Parssing response data
                    if let freeSMSHandler = Mapper<FreeSMS>().map(JSONObject: resultData){
                        
                        /*Save data into user defaults*/
                        freeSMSHandler.saveInUserDefaults(key: APIsType.freeSMSStatus.selectedLocalizedAPIKey())
                        
                        /*Setting limit values*/
                        self.OnnetSMSLeftOverValue_lbl.text  = freeSMSHandler.onNetSMS ?? "0"
                        self.OffnetSMSLeftOverValue_lbl.text = freeSMSHandler.offNetSMS ?? "0"
                    }
                }  else {
                    // Show error alert to user
                    self.showErrorAlertWithMessage(message: resultDesc)
                }
            }
        })
    }
    
    /// Call 'getFreeSMSStatus' API .
    ///
    /// - returns: Void
    func sendFreeSMS(recieverMsisdn: String, textMessage: String) {
        
        activityIndicator.showActivityIndicator()
        
        /*API call tpo get data*/
        _ = MBAPIClient.sharedClient.sendFreeSMS(recieverMsisdn: recieverMsisdn, textMessage: textMessage,{ (response, resultData, error, isCancelled, status, resultCode, resultDesc) in
            self.activityIndicator.hideActivityIndicator()
            if error != nil {
                //EventLog
                MBAnalyticsManager.logEvent(screenName: "Services Screen", contentType:"Services Quik Services Screen" , status:"Message Send Failure")
                
                error?.showServerErrorInViewController(self)
                
            } else {
                // handling data from API response.
                if resultCode == Constants.MBAPIStatusCode.succes.rawValue {
                    //EventLog
                    MBAnalyticsManager.logEvent(screenName: "Services Screen", contentType:"Services Quik Services Screen" , status:"Message Send Success" )
                    
                    
                    // Parssing response data
                    if let freeSMSHandler = Mapper<FreeSMS>().map(JSONObject: resultData){
                        
                        /*Save data into user defaults*/
                        freeSMSHandler.saveInUserDefaults(key: APIsType.freeSMSStatus.selectedLocalizedAPIKey())
                        
                        /*Setting limit values*/
                        self.OnnetSMSLeftOverValue_lbl.text  = freeSMSHandler.onNetSMS ?? "0"
                        self.OffnetSMSLeftOverValue_lbl.text = freeSMSHandler.offNetSMS ?? "0"
                        
                        if freeSMSHandler.smsSent.isStringTrue() {
                            
                            self.symbolsLength_lbl.text = "\(Localized("Info_SymbolsLength"))\(0)"
                            self.SMSCount_lbl.text      = "\(Localized("Info_SMSCount"))\(0)"
                            self.SMS_textView.text = ""
                            self.SMS_textView.resignFirstResponder()
                            
                            self.showSuccessAlertWithMessage(message: Localized("Message_SuccessfulSent"))
                            
                        }
                    }
                }  else {
                    //EventLog
                    MBAnalyticsManager.logEvent(screenName: "Services Screen", contentType:"Services Quik Services Screen" , status:"Message Send Failure")
                    
                    // Show error alert to user
                    self.showErrorAlertWithMessage(message: resultDesc)
                }
            }
        })
    }
    
}

//MARK: - Textfield delagates

extension FreeSMSVC : UITextFieldDelegate {
    
    //Set max Limit for textfields
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let text = textField.text else { return false } // Check that if text is nill then return false
        
        let newLength = text.count + string.count - range.length
        
        if textField == number_txt {
            
            let allowedCharacters = CharacterSet(charactersIn: Constants.allowedNumbers)
            let characterSet = CharacterSet(charactersIn: string)
            if newLength <= 9 && allowedCharacters.isSuperset(of: characterSet) {
                
                // Returnimg number input
                return  true
            } else {
                return  false
            }
            
        } else  {
            return false
        }
    }
}

//MARK: - TextView delagates

extension FreeSMSVC : UITextViewDelegate {
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        
        guard let textViewValue = textView.text else { return false } // Check that if text is nill then return false

        let newTextAfterChange = (textViewValue as NSString).replacingCharacters(in: range, with: text)
        let newTextLength = newTextAfterChange.endIndex.utf16Offset(in: newTextAfterChange)
        
        var smsAllowedTextLength = 0
        if newTextAfterChange.containsOtherThenAllowedCharactersForFreeSMSInEnglish() {
            smsAllowedTextLength = Constants.AzriAndRussianFreeSMSLength
        } else {
            smsAllowedTextLength = Constants.EnglishFreeSMSLength
        }
        
        if newTextLength <= smsAllowedTextLength {
            
            return true
            
        } else if newTextLength <= (smsAllowedTextLength * 2) {
            
            return true
        } else {
            
            if newTextLength > (smsAllowedTextLength * 2) {
                self.showAlertWithMessage(message: String(format: Localized("Message_SMSLimit"), (smsAllowedTextLength * 2)))
            }
            return false
        }
    }
    
    /// When the UITextView did change, show or hide the label based on if the UITextView is empty or not
    ///
    /// - Parameter textView: The UITextView that got updated
    public func textViewDidChange(_ textView: UITextView) {
        
        if textView == self.SMS_textView {
            
            if let placeholderLabel = self.SMS_textView.viewWithTag(100) as? UILabel {
                placeholderLabel.isHidden = self.SMS_textView.text.count > 0
            }
            
            if let newText = textView.text {
                
                var smsAllowedTextLength = 0
                if newText.containsOtherThenAllowedCharactersForFreeSMSInEnglish() {
                    smsAllowedTextLength = Constants.AzriAndRussianFreeSMSLength
                } else {
                    smsAllowedTextLength = Constants.EnglishFreeSMSLength
                }

                let newTextLength = newText.endIndex.utf16Offset(in: newText)
                
                symbolsLength_lbl.text  = "\(Localized("Info_SymbolsLength"))\(newTextLength)"

                if newTextLength <= 0 {
                    SMSCount_lbl.text       = "\(Localized("Info_SMSCount"))\(0)"
                    
                } else if newTextLength <= smsAllowedTextLength {
                    SMSCount_lbl.text       = "\(Localized("Info_SMSCount"))\(1)"
                    
                } else if newTextLength <= (smsAllowedTextLength * 2) {
                    SMSCount_lbl.text       = "\(Localized("Info_SMSCount"))\(2)"
                }
                
                if newTextLength > (smsAllowedTextLength * 2) {
                    self.showAlertWithMessage(message: String(format: Localized("Message_SMSLimit"), (smsAllowedTextLength * 2)))
                }
                
            } else {
                symbolsLength_lbl.text  = "\(Localized("Info_SymbolsLength"))\(0)"
                SMSCount_lbl.text       = "\(Localized("Info_SMSCount"))\(0)"
            }
        }
    }
    
    
//    func textViewShouldEndEditing(_ textView: UITextView) -> Bool {
//        if textView == self.SMS_textView {
//            sendPressed(UIButton())
//        }
//        return true
//    }
}
