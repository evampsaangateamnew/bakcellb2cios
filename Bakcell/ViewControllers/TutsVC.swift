//
//  TutsVC.swift
//  Bakcell
//
//  Created by Saad Riaz on 12/6/17.
//  Copyright © 2017 evampsaanga. All rights reserved.
//

import UIKit
import AVKit
import AVFoundation
import KYDrawerController

class TutsVC: BaseVC {
    
    var isLoggedInNow: Bool = false
    
    var avPlayer: AVPlayer!
    var playerLayer: AVPlayerLayer!
    private var AVPlayerDemoPlaybackViewControllerStatusObservationContext = 0
    
    @IBOutlet var cancel_btn: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(resumePlayer(note:)), name: NSNotification.Name(Constants.KAppDidBecomeActive), object: nil)
        
        // Play Tutorial Video
        self.playTutorailVideo()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.interactivePop(false)
        
        cancel_btn.setImage(UIImage(named: Localized("Img_Skip")), for: UIControl.State.normal)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    @IBAction func cancelBtnPressed(_ sender: AnyObject) {
        
        self.cancelVideoAndRedirect()
    }
    
    func playTutorailVideo() {
        
        //  "https://myapp.bakcell.com/bakcellappserver/demo.mp4"
        
        if Connectivity.isConnectedToInternet {
            
            var videoUrlString = "https://myapp.bakcell.com/bakcellappserver/videos/";
            
            if (MBUserSession.shared.subscriberType() == .PrePaid) {
                
                videoUrlString = videoUrlString + "prepaid";
                
            } else {
                if (MBUserSession.shared.userCustomerType() == .Corporate) {
                    
                    videoUrlString = videoUrlString + "corporate";
                } else {
                    
                    videoUrlString = videoUrlString + "individual";
                }
            }
            
            
            switch (MBLanguageManager.userSelectedLanguage()) {
                
            case .Russian:
                videoUrlString = videoUrlString + "_ru.mp4";
            case .English:
                videoUrlString = videoUrlString + "_en.mp4";
            case .Azeri:
                videoUrlString = videoUrlString + "_az.mp4";
            }
            print("playing url: \(videoUrlString)")
            
            if let videoURL = URL(string:videoUrlString) {
                activityIndicator.showActivityIndicator()
                DispatchQueue.main.async {
                    
                    self.avPlayer = AVPlayer(url: videoURL)
                    
                    self.playerLayer = AVPlayerLayer(player: self.avPlayer)
                    // self.playerLayer.videoGravity = AVLayerVideoGravityResizeAspectFill;
                    self.playerLayer.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
                    self.view.layer.addSublayer(self.playerLayer)
                    self.avPlayer.play()
                    self.view.bringSubviewToFront(self.cancel_btn)
                    
                    
                    self.avPlayer.currentItem?.addObserver(self, forKeyPath: "status", options: NSKeyValueObservingOptions.new, context: &self.AVPlayerDemoPlaybackViewControllerStatusObservationContext)
                    
                    
                    NotificationCenter.default.addObserver(self, selector: #selector(self.playerDidFinishPlaying(note:)),
                                                           name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: self.avPlayer.currentItem)
                }
            } else {
                self.showErrorAlertWithMessage(message: Localized("Message_UnexpectedError"), { (aSting) in
                    self.cancelVideoAndRedirect()
                })
            }
        } else {
            
            self.showErrorAlertWithMessage(message: Localized("Message_NoInternet"), { (aString) in
                self.cancelVideoAndRedirect()
            })
        }
    }
    
    @objc func playerDidFinishPlaying(note: NSNotification) {
        self.cancelVideoAndRedirect()
        
    }
    
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        
        if context == &AVPlayerDemoPlaybackViewControllerStatusObservationContext {
            if let change = change as? [NSKeyValueChangeKey: Int]
            {
                if let status = change[NSKeyValueChangeKey.newKey] {
                    activityIndicator.hideActivityIndicator()
                    switch status {
                    case AVPlayer.Status.unknown.rawValue:
                        print("The status of the player is not yet known because it has not tried to load new media resources for playback")
                        self.showErrorAlertWithMessage(message: Localized("Message_UnexpectedError"), { (aSting) in
                            self.cancelVideoAndRedirect()
                        })
                        
                    case AVPlayer.Status.readyToPlay.rawValue:
                        print("The player is Ready to Play")
                        
                    case AVPlayer.Status.failed.rawValue:
                        print("The player failed to load the video")
                        self.showErrorAlertWithMessage(message: Localized("Message_UnexpectedError"), { (aSting) in
                            self.cancelVideoAndRedirect()
                        })
                        
                    default:
                        print("Other status")
                        self.showErrorAlertWithMessage(message: Localized("Message_UnexpectedError"), { (aSting) in
                            self.cancelVideoAndRedirect()
                        })
                    }
                }
            }
        } else {
            super.observeValue(forKeyPath: keyPath, of: object, change: change, context: context)
        }
    }
    
    @objc func resumePlayer(note: NSNotification) {
        
        if self.avPlayer != nil {
            
            self.avPlayer.play()
        } else {
            
            self.playTutorailVideo()
        }
    }
    
    
    func cancelVideoAndRedirect() {
        
        if self.avPlayer != nil {
            self.avPlayer.currentItem?.removeObserver(self, forKeyPath: "status")
            self.avPlayer.replaceCurrentItem(with: nil)
            self.avPlayer.pause()
            self.playerLayer.removeAllAnimations()
            self.playerLayer.removeFromSuperlayer()
            
            NotificationCenter.default.removeObserver(self, name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: self.avPlayer.currentItem)
            
            self.avPlayer = nil
        }
        
        redirectToDashboardScreen(isLoggedInNow: isLoggedInNow)
    }
    
    func redirectToDashboardScreen(isLoggedInNow :Bool) {
        
        if isLoggedInNow == true,
            let count = self.navigationController?.viewControllers.count,
            count > 1,
            let firstVC = self.navigationController?.viewControllers.first,
            let mainViewController :TabbarController = self.myStoryBoard.instantiateViewController(withIdentifier: "TabbarController") as? TabbarController ,
            let drawerViewController :SideDrawerVC = self.myStoryBoard.instantiateViewController(withIdentifier: "SideDrawerVC") as? SideDrawerVC {
            
            mainViewController.isLoggedInNow = isLoggedInNow
            let drawerController = KYDrawerController(drawerDirection: .left, drawerWidth: (UIScreen.main.bounds.width * 0.85))
            drawerController.mainViewController = mainViewController
            drawerController.drawerViewController = drawerViewController
            
            self.navigationController?.viewControllers = [firstVC, drawerController]
        } else {
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    /*
    func playTutorialVideo() {
        
        if let videoURL = URL(string: "https://myapp.bakcell.com/bakcellappserver/demo.mp4") {
            
            if UIApplication.shared.canOpenURL(videoURL) {
                
                let player = AVPlayer(url: videoURL)
                let playerViewController = AVPlayerViewController()
                playerViewController.player = player
                playerViewController.modalPresentationStyle = .fullScreen
                
                self.present(playerViewController, animated: true) {
                    playerViewController.player?.play()
                }
                
            } else {
                self.showErrorAlertWithMessage(message: Localized("Message_GenralError"))
            }
        } else {
            self.showErrorAlertWithMessage(message: Localized("Message_GenralError"))
        }
    }
 */
}
