//
//  ManageAccountCell.swift
//  Azerfon
//
//  Created by Muhammad Waqas on 3/21/19.
//  Copyright © 2019 Evamp&Saanga. All rights reserved.
//

import UIKit
import MarqueeLabel

class ManageAccountCell: UITableViewCell {

    //MARK: - IBOutlet
    @IBOutlet var selectedIndicatorImageView    : UIImageView!
    @IBOutlet var userNameLabel                 : MarqueeLabel!
    @IBOutlet var userPhoneNumberLabel          : UILabel!
    @IBOutlet var sepratorView                  : UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        userNameLabel.setupMarqueeAnimation()
    }

    func setLayout(name :String?, msisdn :String?, isSelected :Bool) {
        // Initialization code
//        userNameLabel.setupMarqueeAnimation()
//        userPhoneNumberLabel.setupMarqueeAnimation()
        
        userNameLabel.text = name
        userPhoneNumberLabel.text = msisdn
        
        selectedIndicatorImageView.isHidden = !isSelected
    }
}
