//
//  NSTextAttachment+MB.swift
//  Bakcell
//
//  Created by AbdulRehman Warraich on 10/5/17.
//  Copyright © 2017 evampsaanga. All rights reserved.
//

import Foundation
import UIKit

extension NSTextAttachment {
    func setImageSize(size: CGSize) {
        bounds = CGRect(x: bounds.origin.x, y: bounds.origin.y, width: size.width, height: size.height)
    }
}
