//
//  Constants.swift
//  WebservicesWrapper
//
//  Created by Shujat on 08/05/2017.
//  Copyright © 2017 Shujat. All rights reserved.
//

import Foundation


class Constants {
    
    class var kMBAPIClientBaseURL:String {
        //  Development URL
//        return "http://10.220.48.130:8080/bakcellappserver/api/"
        
        //  Staging URL old
//        return "https://myappstg.bakcell.com:16444/bakcellappserverv2/api/"
        
        //  Production URL
        return "https://myapp.bakcell.com/bakcellappserverv2/api/"
    }
    
    class var liveChatURL :String {
        
        /*if let savedLiveChatURL = MBUserSession.shared.predefineData?.liveChat,
            savedLiveChatURL.isEmpty == false {
            
            return savedLiveChatURL.urlEncode
        }*/
        
        return "https://secure-fra.livechatinc.com/licence/12830742/v2/open_chat.cgi?name=(REPLACE_WITH_NAME)&email=(REPLACE_WITH_EMAIL)&params=PhoneNumber%3D(REPLACE_WITH_MSISDN)&___store=az"
    }
    
    
    //Live chat
    class var liveChatUrlRequest : URLRequest? {
        // prepare json data
        let json: [String: String] = ["authenticate": "Ev@mp1iv3Ch@t" ]
        
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: json)
            
            // create post request
            if let url = URL(string: liveChatURL) {
                var request = URLRequest(url: url)
                request.httpMethod = "POST"
                
                // insert json data to the request
                request.httpBody = jsonData
                return request
            }
        } catch let error as NSError {
            print(error.localizedDescription)
        }
        
        return nil
    }
    
    // API Response code
    enum MBPasswordStrength: String {
        case didNotMatchCriteria    = "0"
        case Week                   = "1"
        case Medium                 = "2"
        case Strong                 = "3"
    }
    
    // API Status code
    enum MBAPIStatusCode: String {
        case succes                         = "00"
        case forceUpdate                    = "8"
        case optionalUpdate                 = "9"
        case serverDown                     = "10"
        case sessionExpired                 = "7"
        case wrongAttemptToChangePassword   = "104"
    }
    
    // Language types of Application
    enum MBLanguage: String {
        case Russian = "2"
        case English = "3"
        case Azeri   = "4"
    }
    
    // Resend Type
    enum MBResendType : String {
        case ForgotPassword     = "forgotpassword"
        case SignUp             = "signup"
        case UsageHistory       = "usagehistory-details-view"
        case MoneyTransfer       = "moneytransfer"
    }
    
    // Notification sound setting
    enum MBNotificationSoundType : String {
        case Tone       = "tone"
        case Vibrate    = "vibrate"
        case Mute       = "mute"
    }
    
    enum RegistrationType :String {
        case registration   = "registration"
        case addAccount     = "addAccount"
    }
    
    enum MBTariffType : String {
        case klass = "klass"
        case cin = "Cin"
        case newCin = "new_Cin"
        case unSpecified = "unSpecified"
    }
    
    //Constaint values
    static let kButtonCornerRadius = 16.0
    
    
    
    //Date formates
    static let kDisplayFormat   = "dd/MM/yy"
    static let kAPIFormat       = "yyyy-MM-dd"
    static let kHomeDateFormate = "yyyy-MM-dd HH:mm:ss"
    static let kNumberDateFormate = "dd/MM/yy"
//    static let kAutoPaymentFormate = "E, MM-dd-yyyy"
    
    // MARK: List of Constants
    
    static let SPLASH_DURATION = 1.0
    
    static let kUserInfoKey             = "UserInformation"
    static let kIsUserLoggedInKey       = "isUserLoggedIn"
    //  static let kMoneyInfoKey            = "MoneyInformation"
    //  static let kINstallmentInfoKey      = "InstallmentInformation"
    static let kUserSelectedLanguage    = "UserSelectedLanguage"
    static let kFNFMaxCount             = "FNFMaxCount"
    //static let kIsFCMIdAdded            = "IsFCMIdAdded"
    //static let kIsFCMIdAddedForLogin    = "IsFCMIdAddedForLogin"
    //static let kIsNotificationEnabled   = "IsNotificationEnabled"
    //static let kIsFNFAllowed            = "IsFNFAllowed"
    
    
    class var KAppDidBecomeActive: String {
        return "appDidBecomeActive"
    }
    class var KAppDeepLink: String {
        return "appDeeplink"
    }
    
    static let kSingleValueKey          = "Single value"
    static let kDoubleValueKey          = "Double values"
    static let kDoubleTitlesKey         = "Double titles"
    static let allowedNumbers           = "0123456789"
    static let allowedAlphbeats         = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
    static let allowedSmallAlphabets    = "abcdefghijklmnopqrstuvwxyz"
    static let allowedCapitalAlphabets  = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
    static let allowedSpecialCharacters = "~`!@#$%^&*()_+=-]}[{|'\";:/?.>,<"
    
    static let AzriAndRussianFreeSMSLength = 70
    static let EnglishFreeSMSLength = 160
    
    // API keys
    //static let kRedirectionLinks = "redirectionLinks"
    //static let kTariffMigrationPrices = "tariffMigrationPrices"
    
    class var MaxNumberOfManageAccount: Int {
        return 5
    }
    
    //New_App_Check Keys
    static let kIsLaunchedBefore = "kIsLaunchedBefore"
    static let kIsTutorialShowenBefore = "IsTutorialShowenBefore"
    
    //    class var kLiveChatURLString: String {
    //        return "LiveChatURLString"
    //    }
    class var K_IsBiometricEnabled :String {
        return "IsBiometricEnabled"
    }
    class var K_IsRoamingEnabled :String {
        return "IsRoamingEnabled"
    }
    
    class var K_PayBySUBs :String {
        return "PayBySUBs"
    }
    
    class var K_PayBySUB :String {
        return "PayBySUB"
    }
    
    class var K_RateUsTitle :String {
        return "RateUsTitle"
    }
    
    class var K_RateUsMessage :String {
        return "RateUsMessage"
    }
    
    class var K_AppStoreURL :String {
        return "AppStoreURL"
    }
    
    class var K_UserLoggedInTime :String {
        return "UserLoggedInTime"
    }
    
    class var K_RateUsLaterTime :String {
        return "RateUsLaterTime"
    }
    
    class var K_NotificationPopUpTime :String {
        return "ShowNotificaitonsPopupTime"
    }
    
    class var K_IsRateUsShownBefore :String {
        return "IsRateUsShownBefore"
    }
    
    class var K_CallDivert_Forward :String {
        return "165811270"
    }
    
    class var K_IAmBack :String {
        return "102341307"
    }
    
    class var K_ICalledYou_TurnedOff :String {
        return "984855324"
    }
    
    class var K_ICalledYou_Busy :String {
        return "1479418507"
    }
}

//APIs Key
enum APIsType : String {
    
    case dashboard                  = "Dashboad_Data"
    case notificationConfigurations = "Notification_Configurations"
    case notificationData           = "Notification_Data"
    case appMenu                    = "App_Menu"
    case loggedInUserInfo           = "LoggedIn_UserInfo"
    case installments               = "Installments_Data"
    case freeSMSStatus              = "FreeSMSStatus"
    case operationHistory           = "OperationHistory_Data"
    case supplementaryOffer         = "SupplementaryOffer_Data"
    case mySubscriptionOffers       = "MySubscriptionOffers_Data"
    case CoreServices               = "CoreServices_Data"
    case storeLocator               = "StoreLocator_Data"
    case ulduzumStoreLocator        = "UlduzumStoreLocator_Data"
    case faqs                       = "FAQs_Data"
    case contactUs                  = "ContactUs_Data"
    case roamingCountries           = "RoamingCountries_Data"
    case ulduzumUsage               = "UlduzumUsage_Data"
    case tariffDetails              = "tariffDetails_Data"
    
    func selectedLocalizedAPIKey() -> String {
        return self.rawValue.localizedAPIKey().currentUserKey()
    }
    func selectedAPIKey() -> String {
        return self.rawValue.currentUserKey()
    }
    
    func selectedRawValue() -> String {
        return self.rawValue.currentUserKey()
    }
    
    func simpleRawValue() -> String {
        return self.rawValue
    }
}
