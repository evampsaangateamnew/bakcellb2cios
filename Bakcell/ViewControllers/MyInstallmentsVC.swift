//
//  MyInstallmentsVC.swift
//  Bakcell
//
//  Created by Saad Riaz on 6/12/17.
//  Copyright © 2017 evampsaanga. All rights reserved.
//

import UIKit
import  ObjectMapper

class MyInstallmentsVC: BaseVC, UITableViewDelegate,UITableViewDataSource {
    // MARK: - Properties
    var installmentsData : Installments?
    
    // MARK: - IBOutlet
    @IBOutlet var installmentsDescription_lbl: UILabel!
    @IBOutlet var tableView: UITableView!
    @IBOutlet var myinstallmentsTitle_lbl: UILabel!
    @IBOutlet var intro_lbl: UILabel!
    
    // MARK: - View Controller methods
    override func viewDidLoad() {
        super.viewDidLoad()

        if let installments :Installments = Installments.loadFromUserDefaults(key: APIsType.installments.selectedLocalizedAPIKey()) {
            
            installmentsData = installments

            installmentsDescription_lbl.text = installmentsData?.installmentDescription
        }
        
        if ((installmentsData?.installments ?? []).isEmpty) {
            installmentsDescription_lbl.isHidden = true
            self.tableView.showDescriptionViewWithImage(description: Localized("Message_NoDataAvalible"))
        } else {
            self.tableView.hideDescriptionView()
        }

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.interactivePop(true)
        
        //EventLog
        MBAnalyticsManager.logEvent(screenName: "My Account", contentType:"My Installments Screen" , status:"My Installments Screen")
        
        loadViewLayout()
    }
    //MARK: - FUNCTIONS
    func loadViewLayout(){
        myinstallmentsTitle_lbl.text = Localized("Installmets_Title")
       
    }

    //MARK: - TABLE VIEW DELEGATE METHODS
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return installmentsData?.installments?.count ?? 0
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellID", for: indexPath) as! MyInstallmentsCell
        
        cell.setUpInstallments(installmentsData: installmentsData?.installments?[indexPath.row])
        cell.selectionStyle = UITableViewCell.SelectionStyle.none
        
        return cell
    }
}
