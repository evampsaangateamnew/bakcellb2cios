//
//  AppDelegate.swift
//  Bakcell
//
//  Created by Shujat on 16/05/2017.
//  Copyright © 2017 evampsaanga. All rights reserved.
//

import UIKit
import CoreData
import KYDrawerController
import IQKeyboardManagerSwift
import Fabric
import Crashlytics
import GoogleMaps

import UserNotifications
import Firebase
import FirebaseInstanceID
import FirebaseMessaging
import FirebaseDynamicLinks

@available(iOS 10.0, *)
@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate,UNUserNotificationCenterDelegate, MessagingDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {

        //Tab Bar Selection Color set
        UITabBar.appearance().tintColor = UIColor.MBRedColor

        IQKeyboardManager.shared.enable = true
        Fabric.with([Crashlytics.self])
        window?.backgroundColor = UIColor.MBRedColor


        GMSServices.provideAPIKey("AIzaSyA7C1giVco9UezlExQDJWEt3K4F50AGA7o")
        

        // Use Firebase library to configure APIs
        //FirebaseOptions.defaultOptions()?.deepLinkURLScheme = "mybakcell.page.link"
        FirebaseApp.configure()
        Messaging.messaging().delegate = self

        registerForPushNotifications()

        if (launchOptions != nil) {
            // opened from a push notification when the app is closed
            let userInfo = launchOptions?[UIApplication.LaunchOptionsKey.remoteNotification]
            if (userInfo != nil)  {
                // Redirect user to notification screen
                BaseVC.redirectUserToNotificationScreen()
            }
        }

        return true
    }

    func registerForPushNotifications() {

        // Notification Setting
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self

            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(options: authOptions, completionHandler: { (granted, error) in

                guard granted else { return }

                // register remote notification
                UNUserNotificationCenter.current().getNotificationSettings { (settings) in
                    print("Notification settings: \(settings)")

                    if granted {
                        DispatchQueue.main.async {
                            UIApplication.shared.registerForRemoteNotifications()
                        }
                    } else {
                        // handle the error
                    }
                }

            })

        } else {
            let settings: UIUserNotificationSettings = UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            UIApplication.shared.registerUserNotificationSettings(settings)
            UIApplication.shared.registerForRemoteNotifications()
        }


    }


    func applicationWillResignActive(_ application: UIApplication) {
        
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        MBUserSession.shared.isOTPVerified = false

    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        
    }

    func applicationDidBecomeActive(_ application: UIApplication) {

        // Try to add FCM id if this is not added previosly.
        // BaseVC.addFCMId(isForLogin: UserDefaults.standard.bool(forKey: Constants.kIsFCMIdAddedForLogin))
        
        //Removing Usage History when app comes from background
        NotificationCenter.default.post(name: NSNotification.Name(Constants.KAppDidBecomeActive), object: nil, userInfo: nil)

    }

    func applicationWillTerminate(_ application: UIApplication) {
        
        MBUserSession.shared.isOTPVerified = false
        self.saveContext()
    }

    //MARK: - Notifications delegate

    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
        NSLog("Firebase registration token: \(fcmToken)")
    }

    // The callback to handle data message received via FCM for devices running iOS 10 or above.
    @available(iOS 10.0, *)
    func application(received remoteMessage: MessagingRemoteMessage) {

//        NSLog(remoteMessage.appData)
        NSLog("%@",remoteMessage.appData);

        // Redirect user to notification screen
        BaseVC.redirectUserToNotificationScreen()
    }

    // For iOS 9 and above
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any]) {

        NSLog("%@",userInfo)

        // Redirect user to notification screen
        BaseVC.redirectUserToNotificationScreen()
    }

    // Received notification on background
    /* Note:
     if this is implemented then above application:didReceiveRemoteNotification will not be called
     */
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        NSLog("%@",userInfo)

        // Let FCM know about the message for analytics etc.
        Messaging.messaging().appDidReceiveMessage(userInfo)

        // Redirect user to notification screen
        BaseVC.redirectUserToNotificationScreen()

        completionHandler(UIBackgroundFetchResult.newData)
    }

    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {

        completionHandler([UNNotificationPresentationOptions.alert,UNNotificationPresentationOptions.sound,UNNotificationPresentationOptions.badge])
    }

    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {

        NSLog("%@",response)

        // Redirect user to notification screen
        BaseVC.redirectUserToNotificationScreen()

    }
    
    // MARK:- DeepLinking
    
    func application(_ application: UIApplication, continue userActivity: NSUserActivity,
                     restorationHandler: @escaping ([UIUserActivityRestoring]?) -> Void) -> Bool {
      let handled = DynamicLinks.dynamicLinks().handleUniversalLink(userActivity.webpageURL!) { (dynamiclink, error) in
        
        //handling universal links
        //print("dynammiclink:\(dynamiclink?.description ?? "")")
        DispatchQueue.main.asyncAfter(deadline: .now()+1.0) {
            BaseVC.redirectUserToDeepLinkScreen(urlString: dynamiclink?.url?.absoluteString ?? "notFound")
            //self.navigateUserToServices(urlString:dynamiclink?.url?.absoluteString ?? "notFound")
        }
      }

      return handled
    }
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any]) -> Bool {
      return application(app, open: url,
                         sourceApplication: options[UIApplication.OpenURLOptionsKey.sourceApplication] as? String,
                         annotation: "")
    }

    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
      if let dynamicLink = DynamicLinks.dynamicLinks().dynamicLink(fromCustomSchemeURL: url) {
        // Handle the deep link. For example, show the deep-linked content or
        // apply a promotional offer to the user's account.
        // ...
        print("dynamicLink:\(dynamicLink)")
        return true
      }
      return false
    }
    
    func application(_ application: UIApplication, handleOpen url: URL) -> Bool {
        print("url:\(url)")
        return true
    }
    
    func navigateUserToServices(urlString:String) {
        print("urlString:\(urlString)")
        if MBUserSession.shared.isLoggedIn() && MBUserSession.shared.loadUserInfomation() {
            
            if let navigationController  = UIApplication.shared.delegate?.window??.rootViewController as? MainNavigationController {
                
                
                
                
                for viewControll in navigationController.viewControllers {
                    if viewControll.classForCoder.description() == "KYDrawerController.KYDrawerController" {
                        let supplementoryOfferVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SupplementaryVC") as! SupplementaryVC
                        if urlString.containsSubString(subString: "SCREEN_NAME") {
                            supplementoryOfferVC.selectedTabType = MBOfferTabType.Internet
                        } else if urlString.containsSubString(subString: "OFFERING_ID") {
                            if urlString.containsSubString(subString: "DATA") {
                                supplementoryOfferVC.selectedTabType = MBOfferTabType.Internet
                                supplementoryOfferVC.offeringIdFromNotification = urlString.components(separatedBy: "/").last ?? ""
                            } else if urlString.containsSubString(subString: "SMS") {
                                supplementoryOfferVC.selectedTabType = MBOfferTabType.SMS
                                supplementoryOfferVC.offeringIdFromNotification = urlString.components(separatedBy: "/").last ?? ""
                            } else if urlString.containsSubString(subString: "VOICE") {
                                supplementoryOfferVC.selectedTabType = MBOfferTabType.Call
                                supplementoryOfferVC.offeringIdFromNotification = urlString.components(separatedBy: "/").last ?? ""
                            } else if urlString.containsSubString(subString: "SPECIALS") {
                                supplementoryOfferVC.selectedTabType = MBOfferTabType.Campaign
                                supplementoryOfferVC.offeringIdFromNotification = urlString.components(separatedBy: "/").last ?? ""
                            } else if urlString.containsSubString(subString: "INCLUSIVE_RESOURCES   ") {
                                supplementoryOfferVC.selectedTabType = MBOfferTabType.AllInclusive
                                supplementoryOfferVC.offeringIdFromNotification = urlString.components(separatedBy: "/").last ?? ""
                            } else if urlString.containsSubString(subString: "ROAMING") {
                                supplementoryOfferVC.selectedTabType = MBOfferTabType.Roaming
                                supplementoryOfferVC.offeringIdFromNotification = urlString.components(separatedBy: "/").last ?? ""
                            } else {
                                supplementoryOfferVC.selectedTabType = MBOfferTabType.Internet
                            }
                        } else if urlString.containsSubString(subString: "TAB_NAME") {
                            if urlString.containsSubString(subString: "DATA") {
                                supplementoryOfferVC.selectedTabType = MBOfferTabType.Internet
                            } else if urlString.containsSubString(subString: "SMS") {
                                supplementoryOfferVC.selectedTabType = MBOfferTabType.SMS
                            } else if urlString.containsSubString(subString: "VOICE") {
                                supplementoryOfferVC.selectedTabType = MBOfferTabType.Call
                            } else if urlString.containsSubString(subString: "SPECIALS") {
                                supplementoryOfferVC.selectedTabType = MBOfferTabType.Campaign
                            } else if urlString.containsSubString(subString: "INCLUSIVE_RESOURCES   ") {
                                supplementoryOfferVC.selectedTabType = MBOfferTabType.AllInclusive
                            } else if urlString.containsSubString(subString: "ROAMING") {
                                supplementoryOfferVC.selectedTabType = MBOfferTabType.Roaming
                            } else {
                                supplementoryOfferVC.selectedTabType = MBOfferTabType.Internet
                            }
                        } else {
                            
                        }
                        supplementoryOfferVC.redirectedFromNotification = true
                        
                        viewControll.navigationController?.pushViewController(supplementoryOfferVC, animated: true)
                        break
                    }
                }
            }
            /*let supplementoryOfferVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SupplementaryVC") as! SupplementaryVC
            self.navigationController?.pushViewController(supplementoryOfferVC, animated: true)
            if urlString.containsSubString(subString: "SCREEN_NAME") {
                supplementoryOfferVC.selectedTabType = MBOfferTabType.Internet
            } else if urlString.containsSubString(subString: "TAB_NAME") {
                supplementoryOfferVC.selectedTabType = MBOfferTabType.Internet
            } else if urlString.containsSubString(subString: "OFFERING_ID") {
                <#code#>
            }*/
        }
        
    }


    // MARK: - Core Data stack

    @available(iOS 10.0, *)
    lazy var persistentContainer: NSPersistentContainer = {
        
        let container = NSPersistentContainer(name: "Bakcell")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.

                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()

    // MARK: - Core Data Saving support

    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }

}

