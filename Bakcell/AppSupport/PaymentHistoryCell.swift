//
//  PaymentHistoryCell.swift
//  Bakcell
//
//  Created by Saad Riaz on 6/12/17.
//  Copyright © 2017 evampsaanga. All rights reserved.
//

import UIKit

class PaymentHistoryCell: UITableViewCell {

    @IBOutlet var price_lbl: UILabel!
    @IBOutlet var loanId: UILabel!
    @IBOutlet var dateTime: UILabel!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    

}
