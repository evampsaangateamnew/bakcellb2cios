//
//  MessageResponse.swift
//  Bakcell
//
//  Created by AbdulRehman Warraich on 8/21/17.
//  Copyright © 2017 evampsaanga. All rights reserved.
//

import Foundation
import ObjectMapper

//"data":{"message": "Password change is in progress."}
class MessageResponse: Mappable{
    var message : String = ""

    required init?(map: Map) {

    }

    func mapping(map: Map) {

        message     <- map["message"]
    }
}
