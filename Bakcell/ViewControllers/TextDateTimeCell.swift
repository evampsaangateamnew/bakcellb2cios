//
//  TextDateTimeCell.swift
//  Bakcell
//
//  Created by AbdulRehman Warraich on 8/30/17.
//  Copyright © 2017 evampsaanga. All rights reserved.
//

import UIKit

class TextDateTimeCell: UITableViewCell {

    static let identifier : String = "TextDateTimeCell"
//    title label
    @IBOutlet var title_lbl: UILabel!
    @IBOutlet var titleLabelHeightConstraint: NSLayoutConstraint!

//    Detail label
    @IBOutlet var detail_lbl: UILabel!
    @IBOutlet var detailLabelHeightConstraint: NSLayoutConstraint!

//    Date and Time View
    @IBOutlet var dateTimeView: UIView!
    @IBOutlet var fromTitle_lbl: UILabel!
    @IBOutlet var fromValue_lbl: UILabel!
    @IBOutlet var toTitle_lbl: UILabel!
    @IBOutlet var toValue_lbl: UILabel!
    @IBOutlet var dateTimeViewHeightConstraint: NSLayoutConstraint!

   
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func setTextWithTitleLayoutValues(textWithTitle : TextWithTitle?) {

        titleLabelHeightConstraint.constant = 22
        dateTimeViewHeightConstraint.constant = 0
        self.contentView.backgroundColor = UIColor.white

        if let textWithTitle = textWithTitle {

            title_lbl.text = textWithTitle.title ?? ""

            if let detailText = textWithTitle.text,
                detailText.isBlank == false {
//                detail_lbl.text = detailText
                detail_lbl.loadHTMLString(htmlString: detailText)
//                detailLabelHeightConstraint.constant = detail_lbl.heightToFitContent()
            } else {
                detail_lbl.text = ""
//                detailLabelHeightConstraint.constant = 0
            }
        }
    }

    func setTextWithOutTitleLayoutValues(textWithOutTitle : String?) {

        titleLabelHeightConstraint.constant = 0
        dateTimeViewHeightConstraint.constant = 0
        self.contentView.backgroundColor = UIColor.white

        if let textWithOutTitle = textWithOutTitle {

//                detail_lbl.text = textWithOutTitle
            detail_lbl.loadHTMLString(htmlString: textWithOutTitle)
//                detailLabelHeightConstraint.constant = detail_lbl.heightToFitContent()
            } else {
                detail_lbl.text = ""
//                detailLabelHeightConstraint.constant = 0
        }
    }

    func setDateAndTimeLayoutValues(dateAndTime : DateAndTime?) {

        titleLabelHeightConstraint.constant = 0
        dateTimeViewHeightConstraint.constant = 23

        self.contentView.backgroundColor = UIColor.white
        detail_lbl.backgroundColor = UIColor.clear

        if let dateAndTime = dateAndTime {

            if let description = dateAndTime.description {

//                detail_lbl.text = description
                detail_lbl.loadHTMLString(htmlString: description)
//                detailLabelHeightConstraint.constant = detail_lbl.heightToFitContent()

            } else {

                detail_lbl.text = ""
//                detailLabelHeightConstraint.constant = 0

            }

            if let fromTitle = dateAndTime.fromTitle {
                fromTitle_lbl.text = fromTitle
            } else {
                fromTitle_lbl.text = ""
            }

            if let fromValue = dateAndTime.fromValue {
                fromValue_lbl.text = fromValue
            } else {
                fromValue_lbl.text = ""
            }

            if let toTitle = dateAndTime.toTitle {
                toTitle_lbl.text = toTitle
            } else {
                toTitle_lbl.text = ""
            }

            if let toValue = dateAndTime.toValue {
                toValue_lbl.text = toValue
            } else {
                toValue_lbl.text = ""
            }
        } else {
//            detailLabelHeightConstraint.constant = 0
        }
    }

    func setTextWithPointsLayoutValues(textWithPoint : [String?]?) {

        titleLabelHeightConstraint.constant = 0
        dateTimeViewHeightConstraint.constant = 0
        self.contentView.backgroundColor = UIColor.white

        if let textWithPoint = textWithPoint {

            let attributedText = NSMutableAttributedString()
            textWithPoint.forEach({ (aString) in

                let bulletPoint: String = "\u{2022}"
                let aAttribute = NSAttributedString(string: "\(bulletPoint) \(aString ?? "")\n", attributes: [NSAttributedString.Key.foregroundColor: UIColor.MBTextGrayColor])



                attributedText.append(aAttribute)

            })

            detail_lbl.attributedText  = attributedText
//            detailLabelHeightConstraint.constant = detail_lbl.heightToFitContent()
        } else {
            detail_lbl.text = ""
//            detailLabelHeightConstraint.constant = 0
        }
    }

}
