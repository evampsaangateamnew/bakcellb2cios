//
//  AttributeDetailView.swift
//  Bakcell
//
//  Created by AbdulRehman Warraich on 8/30/17.
//  Copyright © 2017 evampsaanga. All rights reserved.
//

import UIKit
import MarqueeLabel

class AttributeDetailView: UIView {

    //    AttributeListView iner outlets
    //    @IBOutlet var aAttributeView: UIView!
    @IBOutlet var attributeTitle_lbl: MarqueeLabel!
    @IBOutlet var attributeValue_lbl: MarqueeLabel!
    @IBOutlet var attributeManatSign_lbl: UILabel!

    @IBOutlet var attributeLeftValue_lbl: MarqueeLabel!
    @IBOutlet var attributeLeftValueManatSign_lbl: MarqueeLabel!

    @IBOutlet var centerSepratorView: UIView!

    @IBOutlet var aAttributeManatSignWidthConstraint: NSLayoutConstraint!
    @IBOutlet var aAttributeLeftValueManatSignWidthConstraint: NSLayoutConstraint!
    @IBOutlet var aAttributeLeftValueWidthConstraint: NSLayoutConstraint!

    override func awakeFromNib() {
        super.awakeFromNib()

        attributeTitle_lbl.setupMarqueeAnimation()
        attributeValue_lbl.setupMarqueeAnimation()
        attributeLeftValue_lbl.setupMarqueeAnimation()
    }
    class func instanceFromNib() -> UIView {
        return UINib(nibName: "AttributeDetailView", bundle: Bundle.main).instantiate(withOwner: nil, options: nil)[0] as! AttributeDetailView
    }

    // Using in supplementary offers
    func setAttributeValues(showManatSign : Bool , aAttribute : AttributeList?) {

        // Hiding left value
        aAttributeLeftValueManatSignWidthConstraint.constant = 0
        aAttributeLeftValueWidthConstraint.constant = 0
        attributeLeftValueManatSign_lbl.text = ""

        centerSepratorView.isHidden = true

        if let title = aAttribute?.title {
            attributeTitle_lbl.text = title
        } else {
            attributeTitle_lbl.text = ""
        }

        
        if showManatSign == true &&
            aAttribute?.value?.isBlank == false &&
            (aAttribute?.unit?.isEqualToStringIgnoreCase(otherString: "manat") == true ||
                aAttribute?.value?.isStringAnNumber() == true ) {

            attributeValue_lbl.text = aAttribute?.value
            
            attributeManatSign_lbl.text = "₼"
            aAttributeManatSignWidthConstraint.constant = 8
        } else {
            attributeManatSign_lbl.text = ""
            aAttributeManatSignWidthConstraint.constant = 0
            
            attributeValue_lbl.text = "\(aAttribute?.value ?? "") \(aAttribute?.unit ?? "")"
        }
    }


    // Using in tariff Header section for call type
    func setAttributeValuesForCall(showManatSign : Bool , aAttribute : CinCallAttributes?, cellType : MBDataTypes, priceTemplate : String?, isFromKlassPostpaid: Bool = false) {

        centerSepratorView.isHidden = true

        if showManatSign == false {
            attributeManatSign_lbl.text = ""
            aAttributeManatSignWidthConstraint.constant = 0
        } else {
            attributeManatSign_lbl.text = "₼"
            aAttributeManatSignWidthConstraint.constant = 8
        }

        if let title = aAttribute?.title {
            attributeTitle_lbl.text = title
        } else {
            attributeTitle_lbl.text = ""
        }

        var rightValue : String = ""

        //Showing left and right values both in case of call
        if cellType == .Call {
            // Setting left label Value (in middle middle of view)
            if priceTemplate?.isEqualToStringIgnoreCase(otherString: Constants.kDoubleTitlesKey) ?? false ||
                priceTemplate?.isEqualToStringIgnoreCase(otherString: Constants.kDoubleValueKey) ?? false {

                //set width
                aAttributeLeftValueWidthConstraint.constant = 80

                if  aAttribute?.valueLeft?.isBlank == false &&
                    aAttribute?.valueRight?.isBlank == false {

                    centerSepratorView.isHidden = false
                } else {
                    centerSepratorView.isHidden = true
                }


                // get Value
                if let leftValue = aAttribute?.valueLeft {

                    attributeLeftValue_lbl.text = leftValue

                    // Check for free resource value
                    if leftValue.isHasFreeOrUnlimitedText() && isFromKlassPostpaid == false {

                        attributeLeftValue_lbl.textColor = UIColor.MBRedColor

                        attributeLeftValueManatSign_lbl.text = ""
                        aAttributeLeftValueManatSignWidthConstraint.constant = 0

                    } else if leftValue.isBlank == false {

                        attributeLeftValue_lbl.textColor = UIColor.black

                        if leftValue.isStringAnNumber() == true {

                            aAttributeLeftValueManatSignWidthConstraint.constant = 8
                            attributeLeftValueManatSign_lbl.text = "₼"
                        } else {
                            attributeLeftValueManatSign_lbl.text = ""
                            aAttributeLeftValueManatSignWidthConstraint.constant = 0
                        }

                    } else  {
                        attributeLeftValueManatSign_lbl.text = ""
                        aAttributeLeftValueManatSignWidthConstraint.constant = 0
                    }

                } else  {
                    attributeLeftValue_lbl.text = ""
                    attributeLeftValueManatSign_lbl.text = ""

                    aAttributeLeftValueManatSignWidthConstraint.constant = 0
                }

            } else {

                centerSepratorView.isHidden = true
                attributeLeftValue_lbl.text = ""
                attributeLeftValueManatSign_lbl.text = ""

                aAttributeLeftValueManatSignWidthConstraint.constant = 0
                aAttributeLeftValueWidthConstraint.constant = 0
            }

            // Setting right value
            rightValue = aAttribute?.valueRight ?? ""

        } else {
            aAttributeLeftValueManatSignWidthConstraint.constant = 0
            aAttributeLeftValueWidthConstraint.constant = 0

            // Setting right value
            rightValue =  aAttribute?.value ?? ""
        }

        // Setting right value
        if rightValue.isBlank {
            attributeValue_lbl.text = ""
            attributeManatSign_lbl.text = ""
            aAttributeManatSignWidthConstraint.constant = 0

        } else {

            attributeValue_lbl.text = rightValue

            attributeValue_lbl.textColor = UIColor.black
            attributeManatSign_lbl.textColor = UIColor.black

            if rightValue.isHasFreeOrUnlimitedText() {

                attributeValue_lbl.textColor = UIColor.MBRedColor

                attributeManatSign_lbl.text = ""
                aAttributeManatSignWidthConstraint.constant = 0

            } else if rightValue.isStringAnNumber() == true {

                attributeValue_lbl.textColor = UIColor.black

                attributeManatSign_lbl.text = "₼"
                aAttributeManatSignWidthConstraint.constant = 8

            } else {
                attributeManatSign_lbl.text = ""
                aAttributeManatSignWidthConstraint.constant = 0
            }

            // Setting color of right value
            if cellType == .Call &&
                aAttribute?.valueLeft?.isBlank == false &&
                aAttribute?.valueRight?.isBlank == false &&
                (priceTemplate?.isEqualToStringIgnoreCase(otherString: Constants.kDoubleTitlesKey) ?? false || priceTemplate?.isEqualToStringIgnoreCase(otherString: Constants.kDoubleValueKey) ?? false) {

                attributeValue_lbl.textColor = UIColor.MBRedColor
                attributeManatSign_lbl.textColor = UIColor.MBRedColor
            }

        }
    }

    // Using in tariff Header section for Internet type
    func setAttributeValuesForInternet(subTitle : String?, subTitleValue : String?, isFromKlassPostpaid: Bool = false)  {

        centerSepratorView.isHidden = true
        
        // Hiding left value
        aAttributeLeftValueManatSignWidthConstraint.constant = 0
        aAttributeLeftValueWidthConstraint.constant = 0

        if subTitleValue?.isHasFreeOrUnlimitedText() == true && isFromKlassPostpaid == false {

            attributeValue_lbl.textColor = UIColor.MBRedColor

            attributeManatSign_lbl.text = ""
            aAttributeManatSignWidthConstraint.constant = 0

        } else if subTitleValue?.isStringAnNumber() == true {

            attributeValue_lbl.textColor = UIColor.black

            attributeManatSign_lbl.text = "₼"
            aAttributeManatSignWidthConstraint.constant = 8

        } else {
            attributeManatSign_lbl.text = ""
            aAttributeManatSignWidthConstraint.constant = 0
        }

        attributeTitle_lbl.text = subTitle ?? ""
        attributeValue_lbl.text = subTitleValue ?? ""
    }
    
}


