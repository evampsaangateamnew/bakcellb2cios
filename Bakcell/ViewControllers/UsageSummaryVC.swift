//
//  UsageSummaryVC.swift
//  Bakcell
//
//  Created by Saad Riaz on 5/28/17.
//  Copyright © 2017 evampsaanga. All rights reserved.
//

import UIKit
import SnapKit

class UsageSummaryVC: BaseVC {
    
    //MARK: - Properties
    var usageSummaryVC:UsageSummaryMainVC?
    var usageDetailVC:UsageHistoryVC?
    
    //MARK: - IBOutlet
    @IBOutlet var summaryDetail_view: UIView!
    @IBOutlet var MainView: UIView!
    @IBOutlet var details_btn: UIButton!
    @IBOutlet var summary_btn: UIButton!
    @IBOutlet var usageHistoryTitle_lbl: UILabel!
    @IBOutlet var topTabViewHeightConstraint: NSLayoutConstraint!

    //MARK: - View Controller Methods
    override func viewDidLoad() {
        super.viewDidLoad()

        summary_btn.layer.borderColor = UIColor.MBBorderGrayColor.cgColor
        details_btn.layer.borderColor = UIColor.MBBorderGrayColor.cgColor

        self.summaryPressed(summary_btn ?? UIButton())
        
        //hide tabView in case of corporate customer
        if MBUserSession.shared.userCustomerType() == MBCustomerType.Corporate {
            summaryDetail_view.isHidden = true
            topTabViewHeightConstraint.constant = 0
            summary_btn.isEnabled = false
            details_btn.isEnabled = false
        
        } else {
            summaryDetail_view.isHidden = false
            topTabViewHeightConstraint.constant = 50
            
            summary_btn.isEnabled = true
            details_btn.isEnabled = true
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.interactivePop(true)
        
        //Removing usageHistory when app in background
//  NotificationCenter.default.addObserver(self, selector: #selector(detailsPressed(_:)), name: NSNotification.Name(Constants.KAppDidBecomeActive), object: nil)
        
        loadViewLayout()
    }

    deinit {
        NotificationCenter.default.removeObserver(self)
    }

    //MARK: IBACTIONS
    @IBAction func summaryPressed(_ sender: Any) {
        
        details_btn.backgroundColor = UIColor .white
        summary_btn.backgroundColor = UIColor.MBButtonBackgroundGrayColor

        details_btn.setTitleColor(UIColor.darkGray, for: UIControl.State.normal)
        summary_btn.setTitleColor(UIColor.black, for: UIControl.State.normal)

        // Removing subview from main view then adding updated again
        let children = self.children
        for vc in children {
            
            if vc is EnterPassportVC || vc is UsageHistoryVC  || vc is UINavigationController{
                vc.view.removeFromSuperview()
                vc.removeFromParent()
            }
        }

        if usageSummaryVC == nil {
            usageSummaryVC = self.myStoryBoard.instantiateViewController(withIdentifier: "UsageSummaryMainVC") as? UsageSummaryMainVC
        }

        self.addChild(usageSummaryVC!)
        MainView.addSubview(usageSummaryVC!.view)
        usageSummaryVC?.didMove(toParent: self)

        usageSummaryVC?.view.snp.makeConstraints { make in
            make.top.equalTo(MainView.snp.top).offset(0.0)
            make.right.equalTo(MainView.snp.right).offset(0.0)
            make.left.equalTo(MainView.snp.left).offset(0.0)
            make.bottom.equalTo(MainView.snp.bottom).offset(0.0)
        }
    }
    
    @IBAction func detailsPressed(_ sender: Any) {
        details_btn.backgroundColor = UIColor.MBButtonBackgroundGrayColor
        summary_btn.backgroundColor = UIColor .white

        details_btn.setTitleColor(UIColor.black, for: UIControl.State.normal)
        summary_btn.setTitleColor(UIColor.darkGray, for: UIControl.State.normal)
        
        // Removing subview from main view then adding updated again
        let children = self.children
        for vc in children {
            if vc is EnterPassportVC || vc is UsageHistoryVC  || vc is UINavigationController{
                vc.view.removeFromSuperview()
                vc.removeFromParent()
            }
        }
        
        if MBUserSession.shared.isOTPVerified == true {

            if usageDetailVC == nil {
                usageDetailVC = self.myStoryBoard.instantiateViewController(withIdentifier: "UsageHistoryVC") as? UsageHistoryVC
            }


            self.addChild(usageDetailVC!)
            MainView.addSubview(usageDetailVC?.view ?? UIView())
            usageDetailVC?.didMove(toParent: self)

            usageDetailVC?.view.snp.makeConstraints { make in
                make.top.equalTo(MainView.snp.top).offset(0.0)
                make.right.equalTo(MainView.snp.right).offset(0.0)
                make.left.equalTo(MainView.snp.left).offset(0.0)
                make.bottom.equalTo(MainView.snp.bottom).offset(0.0)
            }

        } else {

            let vc = self.myStoryBoard.instantiateViewController(withIdentifier: "EnterPassportVC") as? EnterPassportVC
            let detailsNavCtr: UINavigationController = UINavigationController(rootViewController: vc!)

            detailsNavCtr.setNavigationBarHidden(true, animated: false)
            self.addChild(detailsNavCtr)
            MainView.addSubview(detailsNavCtr.view)
            detailsNavCtr.didMove(toParent: self)

            detailsNavCtr.view.snp.makeConstraints { make in
                make.top.equalTo(MainView.snp.top).offset(0.0)
                make.right.equalTo(MainView.snp.right).offset(0.0)
                make.left.equalTo(MainView.snp.left).offset(0.0)
                make.bottom.equalTo(MainView.snp.bottom).offset(0.0)
            }
        }
    }

    //MARK: - FUNCTIONS
    func loadViewLayout(){
        usageHistoryTitle_lbl.text = Localized("title_UageHistory")
        summary_btn .setTitle(Localized("btn_Summary"), for: UIControl.State.normal)
        details_btn.setTitle(Localized("btn_Details"), for: UIControl.State.normal)
    }
    
}
