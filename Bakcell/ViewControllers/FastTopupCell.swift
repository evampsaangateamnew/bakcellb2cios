//
//  FastTopupCell.swift
//  Bakcell
//
//  Created by Touseef Sarwar on 10/03/2021.
//  Copyright © 2021 evampsaanga. All rights reserved.
//

import UIKit

class FastTopupCell: UITableViewCell {
    
    static let ID = "FastTopupCell"
    
    @IBOutlet weak var cardImage : UIImageView!
    @IBOutlet weak var cardNumber : UILabel!
    @IBOutlet weak var price : UILabel!
    @IBOutlet weak var payBtn : MBMarqueeButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.layoutViews()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
 
    
    func layoutViews(){
        payBtn.setTitle(Localized("BtnTitle_Pay"), for: UIControl.State.normal)
        payBtn.titleLabel?.font = UIFont.MBArial(fontSize: 16)
        payBtn.backgroundColor = UIColor.MBBorderGrayColor
        payBtn.layer.borderColor = UIColor.MBBorderGrayColor.cgColor
        payBtn.setTitleColor(UIColor .MBDarkGrayColor, for: UIControl.State.normal)
        payBtn.setMarqueeButtonLayout()
    }
    
    
    func  setUp(withData data : FastPayment?){
        if let infoo = data{
            self.cardNumber.text = infoo.topupNumber
            self.cardImage.image = infoo.cardType == "master" ? #imageLiteral(resourceName: "masterCard") :  #imageLiteral(resourceName: "visaCard")
            self.price.text = infoo.amount + " ₼"
        }else{return}
    }
    
}
