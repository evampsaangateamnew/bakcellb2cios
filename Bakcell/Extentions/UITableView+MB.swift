//
//  UITableView+MB.swift
//  Bakcell
//
//  Created by AbdulRehman Warraich on 11/29/17.
//  Copyright © 2017 evampsaanga. All rights reserved.
//

import Foundation
import UIKit

extension UITableView {
    
    ///Hides Default Sperator of UITableView
    func hideDefaultSeprator() {
        self.separatorColor = UIColor.clear
        self.separatorStyle = .none
    }

    func visibleSection() -> IndexSet {

        var indexSet: IndexSet = IndexSet()
        let visibleRect: CGRect = self.bounds

        for sectionIndex in 0..<self.numberOfSections {

            let sectionBounds = self.rect(forSection: sectionIndex)

            if sectionBounds.intersects(visibleRect) {

                indexSet.insert(sectionIndex)
            }
        }
        return indexSet
    }

    func lastVisibleSection() -> Int {

        let IndexSet = self.visibleSection()

        if let lastSection = IndexSet.last {

            return lastSection + 1

        } else {
            return 0
        }
    }
    
    func reloadUserData(_ dataArray : [Any?]? = [], description:String = Localized("Message_NoData")) {
        
        if ((dataArray?.count ?? 0) <= 0 ) {
            self.showDescriptionViewWithImage(description: description)
            
        } else {
            self.hideDescriptionView()
        }
        
        self.reloadData()
    }
}
