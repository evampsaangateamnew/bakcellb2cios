//
//  NotificationsCell.swift
//  Bakcell
//
//  Created by Saad Riaz on 6/12/17.
//  Copyright © 2017 evampsaanga. All rights reserved.
//

import UIKit

class NotificationsCell: UITableViewCell {

    @IBOutlet var imgView: UIImageView!
    @IBOutlet var time_lbl: UILabel!
    @IBOutlet var details_lbl: UILabel!
    @IBOutlet var lineView: UIView!
    @IBOutlet var renew_btn: MBMarqueeButton!

    @IBOutlet var lineBottomConstraint: NSLayoutConstraint!
    @IBOutlet var lineHeightConstraint: NSLayoutConstraint!
    @IBOutlet var reNewBtnWidth: NSLayoutConstraint!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
