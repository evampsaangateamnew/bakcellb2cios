//
//  DisclaimerMessageVC.swift
//  Bakcell
//
//  Created by AbdulRehman Warraich on 4/13/18.
//  Copyright © 2018 evampsaanga. All rights reserved.
//

import UIKit

enum MBDisclaimerType: String {
    case OperationHistory
    case UsageHistory
    case Unspecified
}

class DisclaimerMessageVC: UIViewController {
    
    fileprivate var okBtncompletionHandlerBlock : (_ dontShowAgain : Bool) -> Void = { _ in }
    var myTitle : String = ""
    var descriptionText : String = ""
    var okBtnTitle : String = ""
    var isDontShowAgainChecked : Bool = false
    
    //MARK:- IBOutlet
    @IBOutlet var contentView: UIView!
    @IBOutlet var title_lbl: UILabel!
    @IBOutlet var description_lbl: UILabel!
    @IBOutlet var ok_btn: UIButton!
    @IBOutlet var checkBox_btn: UIButton!
    @IBOutlet var dontShow_lbl: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        contentView.layer.cornerRadius = 6
        ok_btn.roundAllCorners(radius: CGFloat(Constants.kButtonCornerRadius))
        
        title_lbl.text = myTitle
        description_lbl.text = descriptionText
        dontShow_lbl.text = Localized("DontShowAgain_Description")
        ok_btn.setTitle(okBtnTitle, for: UIControl.State.normal)
        ok_btn.backgroundColor = UIColor.MBButtonBackgroundGrayColor
        
        if isDontShowAgainChecked {
            checkBox_btn.setImage(UIImage(named: "Checkbox-state-1"), for: UIControl.State.normal)
        } else {
            checkBox_btn.setImage(UIImage(named: "Checkbox-state-2"), for: UIControl.State.normal)
        }
        
    }
    
    //MARK:- IBACTIONS
    @IBAction func YesBtnPressed(_ sender: UIButton) {
        
        self.dismiss(animated: true, completion:nil)
        self.okBtncompletionHandlerBlock(isDontShowAgainChecked)
    }
    
    @IBAction func checkBoxBtnPressed(_ sender: UIButton) {
        
        if isDontShowAgainChecked {
            checkBox_btn.setImage(UIImage(named: "Checkbox-state-2"), for: UIControl.State.normal)
            isDontShowAgainChecked = false
            
        } else {
            checkBox_btn.setImage(UIImage(named: "Checkbox-state-1"), for: UIControl.State.normal)
            isDontShowAgainChecked = true
        }
    }

    //MARK:- Functions
    
    func setDisclaimerAlertWith(title: String = Localized("Title_Disclaimer"), description: String = Localized("Disclaimer_Description"), btnTitle: String = Localized("BtnTitle_OK"), okBtnClickedBlock : @escaping (_ dontShowAgain : Bool) -> Void = { _ in }) {
        
        myTitle = title
        descriptionText = description
        okBtnTitle = btnTitle
        okBtncompletionHandlerBlock = okBtnClickedBlock
    }
    
    class public func canShowDisclaimerOfType(_ alertDisclaimerType: MBDisclaimerType) -> Bool {
        
        switch alertDisclaimerType {
            
        case .OperationHistory:
            return UserDefaults.standard.value(forKey: "OperationHistoryDisclaimerValue") as? Bool ?? true
            
        case .UsageHistory:
            return UserDefaults.standard.value(forKey: "UsageHistoryDisclaimerValue") as? Bool ?? true
            
        default:
            return true
        }
        
    }
    
    class public func setDisclaimerStatusForType(_ alertDisclaimerType: MBDisclaimerType, canShow: Bool) {
        
        
        switch alertDisclaimerType {
            
        case .OperationHistory:
            UserDefaults.standard.set(canShow, forKey: "OperationHistoryDisclaimerValue")
            UserDefaults.standard.synchronize()
            
        case .UsageHistory:
            UserDefaults.standard.set(canShow, forKey: "UsageHistoryDisclaimerValue")
            UserDefaults.standard.synchronize()
            
        default:
            return
        }
    }
    
}
