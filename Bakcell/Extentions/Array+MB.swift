//
//  Array+MB.swift
//  Bakcell
//
//  Created by AbdulRehman Warraich on 12/19/17.
//  Copyright © 2017 evampsaanga. All rights reserved.
//

import Foundation
extension Array where Element: Copyable {
    func copy() -> Array {
        var copiedArray = Array<Element>()
        for element in self {
            copiedArray.append(element.copy())
        }
        return copiedArray
    }
    
    func toJSONString() -> String {
        
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: self, options: [])
            return String(data: jsonData, encoding: .utf8) ?? ""
            
        } catch {
            return ""
        }
        
    }
}
