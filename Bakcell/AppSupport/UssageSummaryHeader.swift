//
//  UssageSummaryHeader.swift
//  Bakcell
//
//  Created by Saad Riaz on 8/9/17.
//  Copyright © 2017 evampsaanga. All rights reserved.
//

import UIKit

import MarqueeLabel

class UssageSummaryHeader: MBAccordionTableViewHeaderView {

    @IBOutlet var plusImage: UIImageView!
    @IBOutlet var charedLb: UILabel!
    @IBOutlet var usageType: UILabel!
    @IBOutlet var usage: MarqueeLabel!

    @IBOutlet var manatSignLb: UILabel!
    @IBOutlet var manatSignWidthConstraint: NSLayoutConstraint!
    
    static let kAccordionHeaderViewReuseIdentifier = "AccordionHeaderViewReuseIdentifieraaa";

    override func awakeFromNib() {
        super.awakeFromNib()

        usage.setupMarqueeAnimation()
    }

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
}
