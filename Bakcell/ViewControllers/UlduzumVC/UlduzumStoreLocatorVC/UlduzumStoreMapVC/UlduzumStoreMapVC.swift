//
//  UlduzumStoreMapVC.swift
//  Bakcell
//
//  Created by AbdulRehman Warraich on 1/2/19.
//  Copyright © 2019 evampsaanga. All rights reserved.
//

import UIKit
import GoogleMaps

class UlduzumStoreMapVC: BaseVC {
    
    var storeLocator : UlduzumStoreLocator?
    var storeLocatorFilter : [UlduzumStoreLocation]?
    
    var selectedType : String = Localized("DropDown_All")
    
    var storeIndex : Int = 0
    
    @IBOutlet var serviceCenterDrop: UIDropDown!
    @IBOutlet weak var googleMapView: GMSMapView!
    @IBOutlet weak var storeInfoView: UIView!
    @IBOutlet weak var storeNameLbl: UILabel!
    @IBOutlet weak var storeRateLbl: UILabel!
    @IBOutlet weak var ivIcon : UIImageView!
    @IBOutlet weak var branchesLbl: UILabel!
    
    //MARK: - ViewController Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.storeLocatorFilter = storeLocator?.storesList?.copy()
        
        googleMapView.delegate = self
        
//        self.storeTableView.register(UINib(nibName: "UlduzumAddressCell", bundle: nil), forCellReuseIdentifier: "UlduzumAddressCell")
        
        setUPDropBox()
        SetUpMarker()
        
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        branchesLbl.text = Localized("Title_Branches")
    }
    
    //MARK: - Functions
    
    func setUPDropBox() {
        
        self.storeInfoView.isHidden = true
        
        // set storeCenterDrop
        serviceCenterDrop.layer.borderWidth = 0
        
        serviceCenterDrop.textAlignment = NSTextAlignment .center
        serviceCenterDrop.placeholder = Localized("DropDown_All")
        serviceCenterDrop.rowBackgroundColor = UIColor.MBDimLightGrayColor
        serviceCenterDrop.setFont = UIFont.MBArial(fontSize: 12)
        
        var optionsArray : [String] = [Localized("DropDown_All")]
        
        if let selectedCenter = storeLocator?.storesCategories {
            for center in selectedCenter{
                optionsArray.append(center.categoryName)
            }
        }
        
        if optionsArray.count < 10 {
            serviceCenterDrop.tableHeight = CGFloat(35 * optionsArray.count)
        } else {
            serviceCenterDrop.tableHeight = 325
        }
        serviceCenterDrop.options = optionsArray
        
        serviceCenterDrop.didSelect { (option, index) in
            
            self.selectedType = option
            
            if let tappedObject : UlduzumStoreCategory = (index == 0) ? nil : self.storeLocator?.storesCategories?[index-1] {
                
                self.storeLocatorFilter = self.filterStoreBy(categoryName:option,
                                                             categoryID:tappedObject.categoryID.toInt,
                                                             StoreDetails: self.storeLocator?.storesList?.copy())
                self.SetUpMarker()
            } else {
                self.storeLocatorFilter? = self.filterStoreBy(categoryName:"", categoryID:nil , StoreDetails: self.storeLocator?.storesList?.copy())!
                self.SetUpMarker()
            }
            
        }
    }
    
    func SetUpMarker() {
        
        DispatchQueue.main.async {
            self.googleMapView.clear()
            
            var bounds = GMSCoordinateBounds()
            var currentIndex = 0
            for aStoreLocation in self.storeLocatorFilter ?? [] {
                
                // Creates a marker in the center of the map.
                if aStoreLocation.coorLat != "" &&   aStoreLocation.coorLng != "" {
                    let aStoreMarker : GMSMarker = self.addMarkerOnLocation(storeLatitude: aStoreLocation.coorLat,
                                                                            storeLongitude: aStoreLocation.coorLng,
                                                                            storeName: aStoreLocation.name,
                                                                            storeAddress: aStoreLocation.address,
                                                                            storeType: "",
                                                                            focusOnLocation: false)
                    aStoreMarker.accessibilityValue = "\(currentIndex)"
                    
//                    print("sent tat:\(aStoreLocation.coorLat), long:\(aStoreLocation.coorLng)")
                    
                    bounds = bounds.includingCoordinate(aStoreMarker.position)
                }
                
                currentIndex = currentIndex + 1
            }
            
            
            //  self.googleMapView.animate(toZoom: 15)
            let updateCamera = GMSCameraUpdate.fit(bounds, withPadding: 20)
            self.googleMapView.animate(with: updateCamera)
            
            if self.storeLocatorFilter?.count ?? 0 <= 1,
                let lastStoreLocation = self.storeLocatorFilter?.last {
                
                let camera = GMSCameraPosition.camera(withLatitude: lastStoreLocation.coorLat.toDouble , longitude: lastStoreLocation.coorLng.toDouble, zoom: 15)
                self.googleMapView.camera = camera
                
            }
        }
    }
    
    
    
    func addMarkerOnLocation(storeLatitude: String?, storeLongitude: String?, storeName: String?, storeAddress: String?, storeType: String?, focusOnLocation: Bool = true) -> GMSMarker {
        
        let latitudeRecived = storeLatitude?.toDouble ?? 0.0
        let longitudeRecieved = storeLongitude?.toDouble ?? 0.0
        
        // Creates a marker in the center of the map.
        let aStoreMarker = GMSMarker()
        aStoreMarker.position = CLLocationCoordinate2D(latitude: latitudeRecived, longitude: longitudeRecieved)
        aStoreMarker.title = storeName
        aStoreMarker.snippet = storeAddress
        aStoreMarker.icon = UIImage.init(named: "ulduzum_maker")
        
        
        aStoreMarker.map = self.googleMapView
        
        if focusOnLocation == true {
            let camera = GMSCameraPosition.camera(withLatitude: latitudeRecived , longitude: longitudeRecieved, zoom: 15)
            self.googleMapView.animate(to: camera)
        }
        
        return aStoreMarker
    }
    
    //Filter on id
    func filterStoreBy(categoryName:String?, categoryID:Int?, StoreDetails : [UlduzumStoreLocation]? ) -> [UlduzumStoreLocation]? {
        
        guard categoryID != nil else {
            return StoreDetails
        }
        
        guard let StoreListDetails = StoreDetails else {
            return nil
        }
        
        if(categoryName?.isEqualToStringIgnoreCase(otherString: Localized("DropDown_All")) == true) {
            
            return StoreDetails
            
        } else if (categoryName?.isEqualToStringIgnoreCase(otherString: Localized("DropDown_All")) == false){
            
            let  filterdStoreDetails = StoreListDetails.filter() {
                
                if ($0 as UlduzumStoreLocation).categoryID.toInt == categoryID {
                    return true
                } else {
                    return false
                }
            }
            return filterdStoreDetails
            
        } else {
            return StoreDetails
        }
        
    }
    
    
    
    @IBAction func StoreLocatorPressed(_ sender: UIButton) {
        if let aStoreInfo = self.storeLocatorFilter?[storeIndex] {
            if let ulduzumStoreDetailVC = self.myStoryBoard.instantiateViewController(withIdentifier: "UiduzumStoreDetailVC") as? UiduzumStoreDetailVC {
                ulduzumStoreDetailVC.storeDetail = aStoreInfo
                self.navigationController?.pushViewController(ulduzumStoreDetailVC, animated: true)
            }
        }
        
        
    }
}

extension UlduzumStoreMapVC: GMSMapViewDelegate {
    
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        print("You tapped : \(marker.accessibilityValue ?? "not found")")
        if marker.accessibilityValue != nil {
            storeIndex = marker.accessibilityValue!.toInt
            showStoreInfo()
        } else {
            storeInfoView.isHidden = true
        }
        return true // or false as needed.
    }
    
    func showStoreInfo () {
        if let aStoreInfo = self.storeLocatorFilter?[storeIndex] {
            storeInfoView.isHidden = false
            storeNameLbl.text = aStoreInfo.name
            storeRateLbl.text = aStoreInfo.categoryName
            if aStoreInfo.loyalitySegment.isNumeric {
                ivIcon.isHidden = true
                storeRateLbl.isHidden = false
                storeRateLbl.text = "\(aStoreInfo.loyalitySegment)%"
            } else {
                ivIcon.isHidden = false
                storeRateLbl.isHidden = true
                storeRateLbl.text = ""
            }
//            self.storeTableView.reloadData()
        }
    }
}


extension UlduzumStoreMapVC: UITableViewDelegate, UITableViewDataSource {
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 1//self.storeLocatorSorted?[section].count ?? 0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 44.0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let cell = tableView.dequeueReusableCell(withIdentifier: "UlduzumAddressCell", for: indexPath) as? UlduzumAddressCell {
            
            cell.lblTitle.text = self.storeLocatorFilter?[storeIndex].address
//            if let aStoreInfo = self.storeLocatorSorted?[indexPath.section][indexPath.row] {
//
//                cell.lblStoreName.text = aStoreInfo.name
//                cell.lblStoredesc.text = aStoreInfo.categoryName
//
//                if aStoreInfo.loyalitySegment.isNumeric {
//                    cell.ivIcon.isHidden = true
//                    cell.lblStorePercentage.isHidden = false
//                    cell.lblStorePercentage.text = "\(aStoreInfo.loyalitySegment)%"
//                } else {
//                    cell.ivIcon.isHidden = false
//                    cell.lblStorePercentage.isHidden = true
//                    cell.lblStorePercentage.text = ""
//                }
//            } else {
//                cell.ivIcon.isHidden = true
//                cell.lblStorePercentage.isHidden = true
//                cell.lblStorePercentage.text = ""
//            }
            return cell
        }
        return UITableViewCell()
        
    }
    
}

