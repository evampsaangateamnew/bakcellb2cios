//
//  FreeSMS.swift
//  Bakcell
//
//  Created by AbdulRehman Warraich on 10/6/17.
//  Copyright © 2017 evampsaanga. All rights reserved.
//

import Foundation
import ObjectMapper

class FreeSMS : Mappable{

    var onNetSMS :  String?
    var offNetSMS : String?
    var smsSent :   String = ""

    init() {
    }
    required init?(map: Map) {
    }
//    {
//    "onNetSMS": "10", "offNetSMS": "8", "smsSent": "true"
//    }
    func mapping(map: Map) {

        onNetSMS            <- map["onNetSMS"]
        offNetSMS           <- map["offNetSMS"]
        smsSent             <- map["smsSent"]
    }
}
