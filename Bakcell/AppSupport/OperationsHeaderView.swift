//
//  OperationsHeaderView.swift
//  Bakcell
//
//  Created by Saad Riaz on 9/26/17.
//  Copyright © 2017 evampsaanga. All rights reserved.
//

import UIKit


class OperationsHeaderView: MBAccordionTableViewHeaderView {

    @IBOutlet var dataBundle_lbl: UILabel!
    @IBOutlet var amount_lbl: UILabel!
    @IBOutlet var date_lbl: UILabel!

    @IBOutlet var endingBalance_lbl: UILabel!
    @IBOutlet var endingBalanceView: UIView!

    static let identifier = "OperationsHeaderView";

}
