//
//  MySubscribeVC.swift
//  Bakcell
//
//  Created by Saad Riaz on 9/8/17.
//  Copyright © 2017 evampsaanga. All rights reserved.
//

import UIKit

import ObjectMapper

class MySubscribeVC: BaseVC, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    //MARK:- Properties
    var mySubscriptionData : MySubscription?
    var selectedTabType : MBOfferTabType = MBOfferTabType.Internet
    var selectedUsageType : MBOfferTabType?
    var isRedirectedFromHome : Bool = false
    
    var tabMenuArray : [MBOfferTabType] = [MBOfferTabType.Call,
                                           MBOfferTabType.Internet,
                                           MBOfferTabType.SMS,
                                           MBOfferTabType.TM,
                                           MBOfferTabType.Hybrid,
                                           MBOfferTabType.Roaming ]
    
    
    //MARK:- IBOutlet
    @IBOutlet var mainView: UIView!
    @IBOutlet var collectionView: UICollectionView!
    @IBOutlet var subscriptionsTitle_lbl: UILabel!
    
    
    //MARK:- ViewController LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Load MySubscription
        getMySubscriptionOfferings()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.interactivePop(true)
        
        loadViewLayout()
        
        self.selectTabOfType(selectedTabType)
    }
    
    //MARK: - FUNCTIONS
    func loadViewLayout() {
        
        subscriptionsTitle_lbl.text = Localized("Subscriptions_Title")
    }
    
    func selectTabOfType(_ userSelectedTabType: MBOfferTabType = .Internet) {
        
        collectionView.reloadData()
        self.collectionView.performBatchUpdates(nil, completion: {
            (isCompleted) in
            
            if isCompleted == true {
                let index = self.tabMenuArray.firstIndex(of: userSelectedTabType)
                
                let selectTabIndexPath = IndexPath(item:index ?? 0, section:0)
                
                self.collectionView.selectItem(at: selectTabIndexPath , animated: true, scrollPosition: UICollectionView.ScrollPosition.centeredHorizontally)
                
                //                self.collectionView.scrollToItem(at: selectTabIndexPath, at: UICollectionViewScrollPosition.centeredHorizontally, animated: true)
                
            }
        })
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let str: String = tabMenuArray[indexPath.item].localizedString()
        
        return CGSize(width: ((str as NSString).size(withAttributes: [NSAttributedString.Key.font: UIFont.MBArial(fontSize: 14)]).width + 30), height: 35)
        
    }
    
    //MARK: - COLLECTION VIEW DELEGATE METHODS
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return tabMenuArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellID", for: indexPath) as! SubscriptionCell
        cell.title_lbl.text = tabMenuArray[indexPath.item].localizedString()
        cell.layer.borderColor = UIColor.MBBorderGrayColor.cgColor
        cell.title_lbl.highlightedTextColor = UIColor.black
        
        
        let bgColorView = UIView()
        bgColorView.backgroundColor = UIColor.MBButtonBackgroundGrayColor
        cell.selectedBackgroundView = bgColorView
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if mySubscriptionData != nil {
            
            // Check if user select same selected tab again if true then do nothing.
            if selectedTabType == tabMenuArray[indexPath.item] {
                return
            }
            
            // load selected tab information
            selectedTabType = tabMenuArray[indexPath.item]
            
            reDirectUserToSelectedScreen(selectedItem: tabMenuArray[indexPath.item])
            
        } else {
            // get all information
            selectedTabType = tabMenuArray[indexPath.item]
            getMySubscriptionOfferings()
        }
        
    }
    
    
    func reDirectUserToSelectedScreen(selectedItem : MBOfferTabType) {
        
        // Removing subview from main view then adding updated again
        let children = self.children
        for vc in children {
            if vc is SubscribeMainVC {
                vc.view.removeFromSuperview()
                vc.removeFromParent()
            }
        }
        
        var mySelectedTabItem : MBOfferTabType = selectedItem
        
        let subscriptionVC = self.myStoryBoard.instantiateViewController(withIdentifier: "SubscribeMainVC") as! SubscribeMainVC
        
        if let mySubscription = mySubscriptionData {
            
            if isRedirectedFromHome
                && ( selectedUsageType == .Call
                    || selectedUsageType == .SMS
                    || selectedUsageType == .Internet) {
                
                mySelectedTabItem = .AllInclusive
            }
            
            
            switch mySelectedTabItem {
                
            case MBOfferTabType.Call:
                if let offers = mySubscription.callOffers {
                    subscriptionVC.offers = offers
                }
                
            case MBOfferTabType.Internet:
                
                if let offers = mySubscription.internetOffers {
                    subscriptionVC.offers = offers
                }
                
            case MBOfferTabType.SMS:
                if let offers = mySubscription.smsOffers {
                    subscriptionVC.offers = offers
                }
                
            case MBOfferTabType.Campaign
                :
                if let offers = mySubscription.campaignOffers {
                    subscriptionVC.offers = offers
                }
                
            case MBOfferTabType.TM:
                if let offers = mySubscription.tmOffers {
                    subscriptionVC.offers = offers
                }
                
            case MBOfferTabType.Hybrid:
                if let offers = mySubscription.hybridOffers {
                    subscriptionVC.offers = offers
                }
            case MBOfferTabType.Roaming:
                if let offers = mySubscription.roamingOffers {
                    subscriptionVC.offers = offers
                }
                
            case MBOfferTabType.AllInclusive:
                
                if selectedUsageType == .Call {
                    
                    if let offers = mySubscription.voiceInclusiveOffers {
                        
                        if offers.count > 0 {
                            subscriptionVC.offers = offers
                            
                        } else if let mainOffers = mySubscription.callOffers {
                            
                            subscriptionVC.offers = mainOffers
                            mySelectedTabItem = .Call
                        }
                        
                    } else if let offers = mySubscription.callOffers {
                        
                        subscriptionVC.offers = offers
                        mySelectedTabItem = .Call
                    }
                    
                } else if selectedUsageType == .SMS {
                    
                    if let offers = mySubscription.smsInclusiveOffers  {
                        
                        if offers.count > 0 {
                            subscriptionVC.offers = offers
                            
                        } else if let mainOffers = mySubscription.smsOffers {
                            
                            subscriptionVC.offers = mainOffers
                            mySelectedTabItem = .SMS
                        }
                    } else if let offers = mySubscription.smsOffers {
                        
                        subscriptionVC.offers = offers
                        mySelectedTabItem = .SMS
                    }
                    
                } else if selectedUsageType == .Internet {
                    
                    if let offers = mySubscription.internetInclusiveOffers {
                        if offers.count > 0 {
                            subscriptionVC.offers = offers
                        } else if let mainOffers = mySubscription.internetOffers {
                            
                            subscriptionVC.offers = mainOffers
                            mySelectedTabItem = .Internet
                        }
                        
                    } else if let offers = mySubscription.internetOffers {
                        
                        subscriptionVC.offers = offers
                        mySelectedTabItem = .Internet
                    }
                } else {
                    subscriptionVC.offers = []
                }
                
                if mySelectedTabItem == .AllInclusive {
                    subscriptionVC.selectedUsageType = self.selectedUsageType
                    
                    tabMenuArray = [MBOfferTabType.Call,
                                    MBOfferTabType.Internet,
                                    MBOfferTabType.SMS,
                                    MBOfferTabType.TM,
                                    MBOfferTabType.Hybrid,
                                    MBOfferTabType.Roaming,
                                    MBOfferTabType.AllInclusive]
                    
                } else {
                    subscriptionVC.selectedUsageType = nil
                    mySelectedTabItem = selectedItem
                    
                    tabMenuArray = [MBOfferTabType.Call,
                                    MBOfferTabType.Internet,
                                    MBOfferTabType.SMS,
                                    MBOfferTabType.TM,
                                    MBOfferTabType.Hybrid,
                                    MBOfferTabType.Roaming]
                }
                
            }
            
        }
        
        /* if selected type is not roaming and user redirected from home and offers count is 0 the load Hybrid section */
        //  if self.selectedTabType != .Roaming && isRedirectedFromHome && subscriptionVC.offers.count <= 0 {
        
        /*  Redirected user to selected type of data comming from home and if offers count is 0 then load Hybrid section */
        if isRedirectedFromHome && subscriptionVC.offers.count <= 0 {
            
            // Set selected type to Hybrid
            mySelectedTabItem = .Hybrid
            
            if let offers = mySubscriptionData?.hybridOffers {
                subscriptionVC.offers = offers
            }
            
        }
        
        //Reset redirection check
        isRedirectedFromHome = false
        
        // setting selected type
        self.selectedTabType = mySelectedTabItem
        subscriptionVC.selectedTabType = mySelectedTabItem
        
        // Higlight selected collectionView cell color.
        self.selectTabOfType(mySelectedTabItem)
        
        self.addChild(subscriptionVC)
        mainView.addSubview(subscriptionVC.view)
        subscriptionVC.didMove(toParent: self)
        
        subscriptionVC.view.snp.makeConstraints { make in
            make.top.equalTo(mainView.snp.top).offset(0.0)
            make.right.equalTo(mainView.snp.right).offset(0.0)
            make.left.equalTo(mainView.snp.left).offset(0.0)
            make.bottom.equalTo(mainView.snp.bottom).offset(0.0)
            
        }
    }
    
    //MARK: - API calls
    
    func getMySubscriptionOfferings() {
        
        /* Load data from UserDefaults if internet is not connected */
        if Connectivity.isConnectedToInternet == false,
            let mySubscriptionOffersHandler :MySubscription = MySubscription.loadFromUserDefaults(key: APIsType.mySubscriptionOffers.selectedLocalizedAPIKey()){
            
            // Set information in local object
            self.mySubscriptionData = mySubscriptionOffersHandler
            
            // Redirect to selected Tab
            self.reDirectUserToSelectedScreen(selectedItem: self.selectedTabType)
            
        } else {
            activityIndicator.showActivityIndicator()
        }
        
        _ = MBAPIClient.sharedClient.getSubscriptions({ (response, resultData, error, isCancelled, status, resultCode, resultDesc)  in
            self.activityIndicator.hideActivityIndicator()
            if error != nil {
                error?.showServerErrorInViewController(self)
                
            } else {
                // handling data from API response.
                if resultCode == Constants.MBAPIStatusCode.succes.rawValue {
                    
                    // Parssing response data
                    if let mySubscriptionOffersHandler = Mapper<MySubscription>().map(JSONObject: resultData){
                        
                        self.mySubscriptionData = mySubscriptionOffersHandler
                        mySubscriptionOffersHandler.saveInUserDefaults(key: APIsType.mySubscriptionOffers.selectedLocalizedAPIKey())
                    }
                    
                } else {
                    // Show error alert to user
                    self.showErrorAlertWithMessage(message: resultDesc)
                }
            }
            
            // Redirect to selected Tab
            self.reDirectUserToSelectedScreen(selectedItem: self.selectedTabType)
        })
    }   
}

