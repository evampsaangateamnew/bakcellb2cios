//
//  SuplementaryOffersVC.swift
//  Bakcell
//
//  Created by Saad Riaz on 8/21/17.
//  Copyright © 2017 evampsaanga. All rights reserved.
//

import UIKit

import UPCarouselFlowLayout
import ObjectMapper
import SnapKit

enum MBSectionType: String {
    case Header         = "Header"
    case PackagePrice   = "PackagePrice"
    case PaygPrice      = "PaygPrice"
    case Price          = "Price"
    case Detail         = "Detail"
    case DetailNewCIN   = "DetailOfNewCIN"
    case Description    = "Description"
    case Subscription   = "Subscription"
    case DeActivate     = "DeActivate"
    case UnSpecified    = ""
}

enum MBOfferType: String {
    case Price                      = "Price"
    case Rounding                   = "Rounding"
    case TextWithTitle              = "TextWithTitle"
    case TextWithOutTitle           = "TextWithOutTitle"
    case TextWithPoints             = "TextWithPoints"
    case Date                       = "Date"
    case Time                       = "Time"
    case RoamingDetails             = "RoamingDetails"
    case FreeResourceValidity       = "FreeResourceValidity"
    case TitleSubTitleValueAndDesc  = "TitleSubTitleValueAndDesc"
    case OfferGroup                 = "OfferGroup"
    case TypeTitle                  = "Type"
    case OfferValidity              = "OfferValidity"
    case OfferActive                = "OfferActive"
    case AttributeList              = "attributeList"
    case Usage                      = "Usage"
}


class SuplementaryOffersVC: BaseVC {
    
    var selectedSectionViewType : MBSectionType = MBSectionType.UnSpecified
    var prepareVCForRoaming : Bool = false
    
    var errorMessage : String?
    let layout = UPCarouselFlowLayout()
    
    // Values sent from supplementaryVC
    var offers : [Offers] = []
    var selectedOffersType : MBOfferTabType = MBOfferTabType.Call
    var isCardView : Bool = true
    var selectedOfferIndex : Int? = 0
    
    // Outlets
    @IBOutlet var collectionView: UICollectionView!
    @IBOutlet var offerNumber_lbl: UILabel!
    
    //MARK: - ViewController Functions
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(switchHorizontalPaging), name: NSNotification.Name("switchHorizontalPaging"), object: nil)
        
        if prepareVCForRoaming {
            layout.itemSize = CGSize(width: UIScreen.main.bounds.width * 0.79, height:  ((UIScreen.main.bounds.height * 0.65) + 50))
        } else {
            
            layout.itemSize = CGSize(width: UIScreen.main.bounds.width * 0.79, height:  UIScreen.main.bounds.height * 0.65)
        }
        layout.sideItemScale = 0.9
        
        // CardView setting
        switchCardView(showCardView: isCardView)
        
        if offers.count == 0 {
            collectionView.isHidden = true
            offerNumber_lbl.text = "0/0"
            
            self.view.showDescriptionViewWithImage(description: errorMessage ?? Localized("Message_NoPackageAvalible"))
            
        } else {
            collectionView.isHidden = false
            offerNumber_lbl.text = "1/\(offers.count)"
            
            self.view.hideDescriptionView()
        }
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if offers.count > 0 && selectedOfferIndex != nil {
            
            if isCardView {
                collectionView.scrollToItem(at: IndexPath(item:selectedOfferIndex ?? 0, section:0), at: .centeredHorizontally, animated: true)
            } else {
                collectionView.scrollToItem(at: IndexPath(item:selectedOfferIndex ?? 0, section:0), at: .centeredVertically, animated: true)
            }
            selectedOfferIndex = nil
        }
    }
    
    // Remove notification
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    @IBAction func SubscribeButtonPressed(_ sender: UIButton) {
        
        let selectedOffer = offers[sender.tag]
        
        if let selectedButtonTitle = (sender as UIButton).title(for: .normal) {
            
            if /* selectedOffersType == .TM && */selectedOffer.header?.isTopUp?.isEqualToStringIgnoreCase(otherString: "1") ?? false {
                
                if let topUp = self.myStoryBoard.instantiateViewController(withIdentifier: "TopUpVC") as? TopUpVC {
                    self.navigationController?.pushViewController(topUp, animated: true)
                }
                
            } else {
                var isTypeRenewValue: Bool = false
                if selectedButtonTitle.trimmWhiteSpace.isEqualToStringIgnoreCase(otherString: Localized("BtnTitle_RENEW")) {
                    
                    isTypeRenewValue = true
                }
                
                if MBUserSession.shared.mySubscriptionOffers == nil {
                    getMySubscriptions(aOffer: selectedOffer, isTypeRenew: isTypeRenewValue)
                } else {
                    showConfirmationPOPUP(aOffer: selectedOffer, isTypeRenew: isTypeRenewValue)
                }
            }
        }
        
    }
    
    func showConfirmationPOPUP(aOffer: Offers, isTypeRenew: Bool) {
        
        let confirmationAlert = self.myStoryBoard.instantiateViewController(withIdentifier: "ActivationConfirmationVC") as! ActivationConfirmationVC
        
        
        // aOffer.header?.offerName ?? ""
        if containsOfferOfOfferLevel(offeringLevel: aOffer.header?.offerLevel,
                                     offeringId: aOffer.header?.offeringId,
                                     mySubscriptions: MBUserSession.shared.mySubscriptionOffers) {
            
            confirmationAlert.setConfirmationAlertLayout(title: Localized("Title_Warning"), message: Localized("Message_SubscribeOfferWarning"))
            
        } else {
            
            var confirmationMessageString = ""
            if isTypeRenew {
                confirmationMessageString = Localized("Message_RenewOfferConfirmationMessage")
            } else {
                confirmationMessageString = Localized("Message_SubscribeOfferConfirmation")
            }
            confirmationAlert.setConfirmationAlertLayout( message: confirmationMessageString )
        }
        
        confirmationAlert.setYesButtonCompletionHandler { (aString) in
            self.changeSupplementaryOffering(selectedOffer: aOffer)
        }
        
        self.presentPOPUP(confirmationAlert, animated: true, completion: nil)
    }
    
    //MARK: - Fuctions
    
    @objc func switchHorizontalPaging() {
        
        if isCardView == false {
            isCardView = true
            
        } else {
            isCardView = false
        }
        
        switchCardView(showCardView: isCardView)
        
        openSelectedSection()
        
    }
    
    func switchCardView(showCardView : Bool) {
        
        if showCardView {
            layout.scrollDirection = .horizontal
            
        } else {
            layout.scrollDirection = .vertical
        }
        
        collectionView.setCollectionViewLayout(layout, animated: true)
    }
    
    //MARK: - API Calls
    
    /// Call 'getSubscriptions' API .
    ///
    /// - returns: Void
    func getMySubscriptions(aOffer: Offers, isTypeRenew: Bool) {
        
        activityIndicator.showActivityIndicator()
        
        _ = MBAPIClient.sharedClient.getSubscriptions({ (response, resultData, error, isCancelled, status, resultCode, resultDesc)  in
            
            self.activityIndicator.hideActivityIndicator()
            if error != nil {
                // Show confirmation alert
                self.showConfirmationPOPUP(aOffer: aOffer, isTypeRenew: isTypeRenew)
            } else {
                // handling data from API response.
                if resultCode == Constants.MBAPIStatusCode.succes.rawValue {
                    
                    // Parssing response data
                    if let mySubscriptionOffersHandler = Mapper<MySubscription>().map(JSONObject: resultData){
                        
                        // set data in user session
                        MBUserSession.shared.mySubscriptionOffers = mySubscriptionOffersHandler
                        
                        // Show confirmation alert
                        self.showConfirmationPOPUP(aOffer: aOffer, isTypeRenew: isTypeRenew)
                        
                    } else {
                        // Show confirmation alert
                        self.showConfirmationPOPUP(aOffer: aOffer, isTypeRenew: isTypeRenew)
                    }
                } else {
                    // Show confirmation alert
                    self.showConfirmationPOPUP(aOffer: aOffer, isTypeRenew: isTypeRenew)
                }
            }
        })
    }
    
    /// Call 'changeSupplementaryOffering' API .
    ///
    /// - returns: Void
    func changeSupplementaryOffering(selectedOffer: Offers) {
        
        activityIndicator.showActivityIndicator()
        
        _ = MBAPIClient.sharedClient.changeSupplementaryOffering(actionType: true, offeringId: selectedOffer.header?.offeringId ?? "", offerName: selectedOffer.header?.offerName ?? "", { (response, resultData, error, isCancelled, status, resultCode, resultDesc)  in
            
            self.activityIndicator.hideActivityIndicator()
            if error != nil {
                //EventLog
                MBAnalyticsManager.logEvent(screenName: "Services Screen", contentType:"Services Supplementary Offers Screen" , status:"Offers Activation Failure" )
                
                error?.showServerErrorInViewController(self)
            } else {
                // handling data from API response.
                if resultCode == Constants.MBAPIStatusCode.succes.rawValue {
                    
                    self.activityIndicator.hideActivityIndicator()
                    //EventLog
                    MBAnalyticsManager.logEvent(screenName: "Services Screen", contentType:"Services Supplementary Offers Screen" , status:"Offers Activation Success" )
                    
                    // Parssing response data
                    if let mySubscriptionOffersHandler = Mapper<MySubscription>().map(JSONObject: resultData){
                        
                        // set data in user session
                        MBUserSession.shared.mySubscriptionOffers = mySubscriptionOffersHandler
                        
                        // Show succes alert
                        self.showSuccessAlertWithMessage(message: mySubscriptionOffersHandler.message)
                        
                        // reload CollectionView
                        self.collectionView.reloadData()
                        
                    } else {
                        // Show Error alert
                        self.showErrorAlertWithMessage(message: resultDesc)
                        
                    }
                } else {
                    
                    //EventLog
                    MBAnalyticsManager.logEvent(screenName: "Services Screen", contentType:"Services Supplementary Offers Screen" , status:"Offers Activation Failure" )
                    
                    // Show Error alert
                    self.showErrorAlertWithMessage(message: resultDesc)
                    
                }
            }
        })
    }
}

//MARK: SupplementaryOffersCellDelegate
extension SuplementaryOffersVC: SupplementaryOffersCellDelegate {
    func starTapped(offerId: String) {
        
        if let inAppSurveyVC : InAppSurveyVC = InAppSurveyVC.fromNib() {
            if  let  userSurvey = MBUserSession.shared.appSurvays?.userSurveys?.first(where: { $0.offeringId == offerId}){
                if let survey  = MBUserSession.shared.appSurvays?.surveys?.first(where: { $0.survayId  == userSurvey.surveryId }){
                    inAppSurveyVC.currentSurvay = survey
                    if let  question =  survey.questions?.first(where: { $0.questionId == userSurvey.questionId }){
                        inAppSurveyVC.survayQuestion = question
                        if let answer = question.answers?.firstIndex(where: { $0.answerId ==  userSurvey.answerId }) {
                            inAppSurveyVC.selectedAnswer = question.answers?[answer]
                        }
                    }
                }
                inAppSurveyVC.survayComment = userSurvey.comments
                inAppSurveyVC.offeringId = offerId
                inAppSurveyVC.offeringType = "2"
            } else {
                if let survey = MBUserSession.shared.appSurvays?.surveys?.first(where: { $0.screenName ?? "" == SurveyScreenName.bundle.rawValue}) {
                    inAppSurveyVC.currentSurvay = survey
                    inAppSurveyVC.offeringId = offerId
                    inAppSurveyVC.offeringType = "2"
                }
            }
            inAppSurveyVC.callback =  {
                DispatchQueue.main.asyncAfter(deadline: .now()+0.25) {
                    self.collectionView.reloadData()
                }
            }
            self.presentPOPUP(inAppSurveyVC, animated: true, completion: nil)
        }
        
    }
    
    
}


// MARK: - <UICollectionViewDataSource> / <UICollectionViewDelegate> -

extension SuplementaryOffersVC : UICollectionViewDelegate,UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return offers.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: SupplementaryOffersCell.identifier, for: indexPath) as? SupplementaryOffersCell {
            if containsOfferOfOfferId(offeringId: offers[indexPath.row].header?.offeringId, mySubscriptions: MBUserSession.shared.mySubscriptionOffers) {
                cell.btnsStackView.isHidden = false
                cell.btnsStackView.snp.updateConstraints { const in
                    const.height.equalTo(20)
                }
            }else{
                cell.btnsStackView.isHidden = true
                cell.btnsStackView.snp.updateConstraints { const in
                    const.height.equalTo(0)
                }
 
            }
            cell.setRating(with: offers[indexPath.row])
            cell.delegate =  self
            cell.tableView.delegate = self
            cell.tableView.dataSource = self
            cell.tableView.allowMultipleSectionsOpen = false
            cell.tableView.keepOneSectionOpen = true;
            cell.tableView.tag = indexPath.item
            cell.tableView.reloadData()
            
            return cell
        } else {
            return UICollectionViewCell()
        }
    }
    
    
    // USED to toggle
    func openSelectedSection() {
        
        let visbleIndexPath = collectionView.indexPathsForVisibleItems
        
        visbleIndexPath.forEach { (aIndexPath) in
            
            if let cell = collectionView.cellForItem(at: aIndexPath) as? SupplementaryOffersCell {
                
                let types = typeOfDataInAOffer(aOffer: offers[cell.tableView.tag])
                
                if (selectedSectionViewType == .Detail || selectedSectionViewType == .Description) && isCardView {
                    
                    if types.contains(selectedSectionViewType) {
                        
                        openSectionAt(index: types.firstIndex(of: selectedSectionViewType) ?? 0, tableView: cell.tableView)
                    } else {
                        openSectionAt(index: 0, tableView: cell.tableView)
                    }
                    
                } else if (isCardView == false) {
                    /* Reset open icon for Detail */
                    if types.contains(.Detail),
                        let sectionIndexValue = types.firstIndex(of: .Detail) {
                        if let headerView = cell.tableView.headerView(forSection: sectionIndexValue) as? SupplementaryOffersHeaderView {
                            headerView.setSectionSelectedIcon(false)
                        }
                    }
                    /* Reset open icon for Description */
                    if types.contains(.Description),
                        let sectionIndexValue = types.firstIndex(of: .Description) {
                        if let headerView = cell.tableView.headerView(forSection: sectionIndexValue) as? SupplementaryOffersHeaderView {
                            headerView.setSectionSelectedIcon(false)
                        }
                    }
                    
                    if types.contains(offers[cell.tableView.tag].openedViewSection) {
                        
                        openSectionAt(index: types.firstIndex(of: offers[cell.tableView.tag].openedViewSection) ?? 0, tableView: cell.tableView)
                    } else {
                        openSectionAt(index: 0, tableView: cell.tableView)
                    }
                } else {
                    openSectionAt(index: 0, tableView: cell.tableView)
                }
            }
        }
    }
    
    
    func openSectionAt(index : Int , tableView : MBAccordionTableView) {
        
        if  tableView.isSectionOpen(index) == false {
            
            tableView.toggleSection(withOutCallingDelegates: index)
        }
    }
}

// MARK: - <UITableViewDataSource> / <UITableViewDelegate> -

extension SuplementaryOffersVC : UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return numberOfSectionAtOfferIndex(index: tableView.tag)
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        if section == 0 {
            
            // For view setting incase of sticker
            if let stickerTitle = offers[tableView.tag].header?.stickerLabel {
                
                if stickerTitle.length > 0 {
                    return 61
                } else {
                    return 45
                }
            } else {
                return 45
            }
            
        } else {
            return 45
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        let types = typeOfDataInAOffer(aOffer: offers[tableView.tag])
        
        switch types[section] {
            
        case MBSectionType.Header:
            return numberOfRowsInHeaderSection(aHeader: offers[tableView.tag].header)
            
        case MBSectionType.Detail:
            return numberOfRowsInDetailOrDescriptionSection(aDetailsOrDescription: offers[tableView.tag].details)
            
        case MBSectionType.Description:
            return numberOfRowsInDetailOrDescriptionSection(aDetailsOrDescription: offers[tableView.tag].description)
            
        default:
            return 0
        }
        
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let types = typeOfDataInAOffer(aOffer: offers[tableView.tag])
        let typeOfDataInOffer = types[section]
        
        switch typeOfDataInOffer {
            
        case MBSectionType.Header:
            let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: SupplementaryOffersHeaderView.identifier) as! SupplementaryOffersHeaderView
            headerView.setViewForOfferNameWithSickerValues(offerName: offers[tableView.tag].header?.offerName, stickerTitle: offers[tableView.tag].header?.stickerLabel, stickerColorCode: offers[tableView.tag].header?.stickerColorCode)
            return headerView
            
        case MBSectionType.Detail:
            let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: SupplementaryOffersHeaderView.identifier) as! SupplementaryOffersHeaderView
            
            if ((selectedSectionViewType == MBSectionType.Detail && isCardView == true) ||
                (offers[tableView.tag].openedViewSection == MBSectionType.Detail && isCardView == false)) {
                
                headerView.setViewWithTitle(title: Localized("Title_Details"), isSectionSelected: true, headerType: MBSectionType.Detail)
            } else {
                headerView.setViewWithTitle(title: Localized("Title_Details"), isSectionSelected: false, headerType: MBSectionType.Detail)
            }
            return headerView
            
        case MBSectionType.Description:
            let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: SupplementaryOffersHeaderView.identifier) as! SupplementaryOffersHeaderView
            
            if ((selectedSectionViewType == MBSectionType.Description && isCardView == true) ||
                (offers[tableView.tag].openedViewSection == MBSectionType.Description && isCardView == false)) {
                
                headerView.setViewWithTitle(title: Localized("Title_Description"), isSectionSelected: true, headerType: MBSectionType.Description)
            } else {
                headerView.setViewWithTitle(title: Localized("Title_Description"), isSectionSelected: false, headerType: MBSectionType.Description)
            }
            return headerView
            
        case MBSectionType.Subscription:
            
            if let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: SubscribeButtonView.identifier) as? SubscribeButtonView {
                headerView.viewType = .Subscription
                
                let aOffer = offers[tableView.tag]
                
                if /* selectedOffersType == .TM && */ aOffer.header?.isTopUp?.isEqualToStringIgnoreCase(otherString: "1") ?? false {
                    
                    headerView.setTopUpButtonLayout(tag: tableView.tag)
                    
                } else if containsOfferOfOfferId(offeringId: aOffer.header?.offeringId, mySubscriptions: MBUserSession.shared.mySubscriptionOffers) {
                    
                    if aOffer.header?.btnRenew?.isButtonEnabled() ?? false {
                        headerView.setRenewAndSubscribedButton(tag: tableView.tag)
                    } else {
                        headerView.setSubscribedButton(tag: tableView.tag)
                    }
                    
                } else {
                    headerView.setSubscribeButtonLayout(tag: tableView.tag)
                }
                
                headerView.activateRenew_btn.addTarget(self, action: #selector(SubscribeButtonPressed(_:)), for: UIControl.Event.touchUpInside)
                return headerView
            } else {
                return UITableViewHeaderFooterView()
            }
            
        default:
            return UITableViewHeaderFooterView()
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let types = typeOfDataInAOffer(aOffer: offers[tableView.tag])
        
        let typeOfDataInOffer = types[indexPath.section]
        
        switch typeOfDataInOffer {
            
        case MBSectionType.Header:
            
            let typeOfData = typeOfDataInHeaderSection(aHeader: offers[tableView.tag].header)
            
            if indexPath.row < typeOfData.count {
                
                let cell : SupplementaryExpandCell = tableView.dequeueReusableCell(withIdentifier: SupplementaryExpandCell.identifier) as! SupplementaryExpandCell
                
                switch typeOfData[indexPath.row] {
                    
                case MBOfferType.TypeTitle:
                    cell.setOfferType(header: offers[tableView.tag].header)
                    break
                    
                case MBOfferType.OfferGroup:
                    cell.setOfferGroup(offerGroup: offers[tableView.tag].header?.offerGroup)
                    break
                    
                case MBOfferType.OfferValidity:
                    cell.setOfferValidity(header: offers[tableView.tag].header)
                    break
                    
                default:
                    break
                }
                
                return cell
            } else {
                let attributedListIndex : Int = indexPath.row - typeOfData.count
                
                let cell : AttributeListCell = tableView.dequeueReusableCell(withIdentifier: AttributeListCell.identifier) as! AttributeListCell
                
                if let aHeader = offers[tableView.tag].header {
                    
                    cell.setAttributeList(attributeList: aHeader.attributeList?[attributedListIndex], offerType: aHeader.type ?? "")
                }
                return cell
            }
            
        case MBSectionType.Detail , MBSectionType.Description :
            
            var aDetailsOrDescription : DetailsAndDescription?
            
            if typeOfDataInOffer == MBSectionType.Detail {
                aDetailsOrDescription = offers[tableView.tag].details
            } else {
                aDetailsOrDescription = offers[tableView.tag].description
            }
            
            let containingDataTypes = typeOfDataInDetailOrDescriptionSection(aDetailsOrDescription: aDetailsOrDescription)
            
            switch containingDataTypes[indexPath.row] {
                
            case MBOfferType.Price:
                
                let cell = tableView.dequeueReusableCell(withIdentifier: PriceAndRoundingCell.identifier) as! PriceAndRoundingCell
                cell.setPriceLayoutValues(price: aDetailsOrDescription?.price, showIcon: false)
                return cell
                
            case MBOfferType.Rounding:
                
                let cell = tableView.dequeueReusableCell(withIdentifier: PriceAndRoundingCell.identifier) as! PriceAndRoundingCell
                cell.setRoundingLayoutValues(rounding: aDetailsOrDescription?.rounding, showIcon: false)
                return cell
                
            case MBOfferType.TextWithTitle:
                
                let cell = tableView.dequeueReusableCell(withIdentifier: TextDateTimeCell.identifier) as! TextDateTimeCell
                cell.setTextWithTitleLayoutValues(textWithTitle: aDetailsOrDescription?.textWithTitle)
                return cell
                
            case MBOfferType.TextWithOutTitle:
                
                let cell = tableView.dequeueReusableCell(withIdentifier: TextDateTimeCell.identifier) as! TextDateTimeCell
                cell.setTextWithOutTitleLayoutValues(textWithOutTitle: aDetailsOrDescription?.textWithOutTitle)
                return cell
                
            case MBOfferType.TextWithPoints:
                
                let cell = tableView.dequeueReusableCell(withIdentifier: TextDateTimeCell.identifier) as! TextDateTimeCell
                cell.setTextWithPointsLayoutValues(textWithPoint: aDetailsOrDescription?.textWithPoints)
                return cell
                
            case MBOfferType.Date:
                
                let cell = tableView.dequeueReusableCell(withIdentifier: TextDateTimeCell.identifier) as! TextDateTimeCell
                cell.setDateAndTimeLayoutValues(dateAndTime: aDetailsOrDescription?.date)
                return cell
                
            case MBOfferType.Time:
                
                let cell = tableView.dequeueReusableCell(withIdentifier: TextDateTimeCell.identifier) as! TextDateTimeCell
                cell.setDateAndTimeLayoutValues(dateAndTime: aDetailsOrDescription?.time)
                return cell
                
            case MBOfferType.RoamingDetails:
                
                let cell = tableView.dequeueReusableCell(withIdentifier: RoamingDetailsCell.identifier) as! RoamingDetailsCell
                cell.setRoamingDetailsLayout(roamingDetails: aDetailsOrDescription?.roamingDetails)
                return cell
                
            case MBOfferType.FreeResourceValidity:
                
                let cell = tableView.dequeueReusableCell(withIdentifier: FreeResourceValidityCell.identifier) as! FreeResourceValidityCell
                cell.setFreeResourceValidityValues(freeResourceValidity: aDetailsOrDescription?.freeResourceValidity)
                return cell
                
            case MBOfferType.TitleSubTitleValueAndDesc:
                
                let cell = tableView.dequeueReusableCell(withIdentifier: TitleSubTitleValueAndDescCell.identifier) as! TitleSubTitleValueAndDescCell
                cell.setTitleSubTitleValueAndDescLayoutValues(titleSubTitleValueAndDesc: aDetailsOrDescription?.titleSubTitleValueAndDesc)
                return cell
                
            default:
                break
            }
            
        default:
            let cell : UITableViewCell = UITableViewCell(style: UITableViewCell.CellStyle.default, reuseIdentifier: "dCell")
            return cell
        }
        
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //tableView.deselectRow(at: indexPath, animated: true)
    }
    
    //MARK: - Helper fuction for data loading
    func numberOfSectionAtOfferIndex(index: Int) -> Int {
        
        var numberOfSections : Int = 2
        
        
        if offers[index].details != nil {
            numberOfSections += 1
        }
        
        if offers[index].description != nil {
            numberOfSections += 1
        }
        
        return numberOfSections
    }
    
    func numberOfRowsInHeaderSection(aHeader : Header?) -> Int {
        
        // Atleast one because there is one with offer name
        var numberOfRows : Int = 1
        
        if let aHeader =  aHeader {
            
            if checkOfferContainsOfferGroup(aHeader: aHeader) {
                numberOfRows = numberOfRows + 1
            }
            
            if checkOfferContainsHeaderType(aHeader: aHeader) {
                numberOfRows = numberOfRows + 1
            }
            
            if let attributeList = aHeader.attributeList {
                numberOfRows = numberOfRows + attributeList.count
            }
        }
        return numberOfRows
    }
    
    func checkOfferContainsOfferGroup(aHeader : Header?) -> Bool {
        
        // Atleast one because there is one with offer name
        var isContainsOfferGroup : Bool = false
        
        if selectedOffersType == MBOfferTabType.Internet || selectedOffersType == MBOfferTabType.Roaming {
            
            if let aHeader =  aHeader {
                
                if aHeader.offerGroup != nil {
                    isContainsOfferGroup = true
                }
            }
        }
        
        return isContainsOfferGroup
    }
    
    func checkOfferContainsHeaderType(aHeader : Header?) -> Bool {
        
        // Atleast one because there is one with offer name
        var isContainsOfferGroup : Bool = false
        
        if selectedOffersType == MBOfferTabType.Roaming {
            
            if let aHeader =  aHeader {
                
                if aHeader.type != nil {
                    isContainsOfferGroup = true
                }
            }
        }
        
        return isContainsOfferGroup
    }
    
    func numberOfRowsInDetailOrDescriptionSection(aDetailsOrDescription : DetailsAndDescription?) -> Int {
        
        // Atleast one because there is one with offer name
        var numberOfRows : Int = 0
        
        if let aDetailsOrDescription =  aDetailsOrDescription {
            
            // 1
            if aDetailsOrDescription.price != nil {
                numberOfRows = numberOfRows + 1
            }
            
            /* if let count = aDetailsOrDescription.price?.count {
             numberOfRows = numberOfRows + count
             }*/
            // 2
            if aDetailsOrDescription.rounding != nil {
                numberOfRows = numberOfRows + 1
            }
            // 3
            if aDetailsOrDescription.textWithTitle != nil {
                numberOfRows = numberOfRows + 1
            }
            // 4
            if aDetailsOrDescription.textWithOutTitle != nil {
                numberOfRows = numberOfRows + 1
            }
            // 5
            if aDetailsOrDescription.textWithPoints != nil {
                numberOfRows = numberOfRows + 1
            }
            // 6
            if aDetailsOrDescription.date != nil {
                numberOfRows = numberOfRows + 1
            }
            // 7
            if aDetailsOrDescription.time != nil {
                numberOfRows = numberOfRows + 1
            }
            // 8
            if aDetailsOrDescription.roamingDetails != nil {
                numberOfRows = numberOfRows + 1
            }
            // 9
            if aDetailsOrDescription.freeResourceValidity != nil {
                numberOfRows = numberOfRows + 1
            }
            // 10
            if aDetailsOrDescription.titleSubTitleValueAndDesc != nil {
                numberOfRows = numberOfRows + 1
            }
            
        }
        return numberOfRows
    }
    
    func containsInfoOfDetailOrDescriptionSection(aDetailsOrDescription : DetailsAndDescription?) -> Bool {
        
        if let aDetailsOrDescription =  aDetailsOrDescription {
            
            // 1
            if aDetailsOrDescription.price != nil {
                return true
            }
            // 2
            if aDetailsOrDescription.rounding != nil {
                return true
            }
            // 3
            if aDetailsOrDescription.textWithTitle != nil {
                return true
            }
            // 4
            if aDetailsOrDescription.textWithOutTitle != nil {
                return true
            }
            // 5
            if aDetailsOrDescription.textWithPoints != nil {
                return true
            }
            // 6
            if aDetailsOrDescription.date != nil {
                return true
            }
            // 7
            if aDetailsOrDescription.time != nil {
                return true
            }
            // 8
            if aDetailsOrDescription.roamingDetails != nil {
                return true
            }
            // 9
            if aDetailsOrDescription.freeResourceValidity != nil {
                return true
            }
            // 10
            if aDetailsOrDescription.titleSubTitleValueAndDesc != nil {
                return true
            }
            
        }
        return false
    }
    
    func typeOfDataInAOffer(aOffer : Offers?) -> [MBSectionType] {
        
        // Atleast one because there is one with offer name
        
        var dataTypes : [MBSectionType] = []
        
        if let aOffer =  aOffer {
            // 1
            if aOffer.header != nil {
                
                dataTypes.append(MBSectionType.Header)
            }
            // 2
            if aOffer.details != nil  {
                
                dataTypes.append(MBSectionType.Detail)
            }
            // 3
            if aOffer.description != nil {
                
                dataTypes.append(MBSectionType.Description)
            }
            
            dataTypes.append(MBSectionType.Subscription)
        }
        return dataTypes
    }
    
    func typeOfDataInHeaderSection(aHeader : Header?) -> [MBOfferType] {
        
        var dataTypeArray : [MBOfferType] = []
        if let aHeader =  aHeader {
            
            
            
            if selectedOffersType == MBOfferTabType.Internet ||
                selectedOffersType == MBOfferTabType.Roaming {
                
                if checkOfferContainsHeaderType(aHeader: aHeader) {
                    dataTypeArray.append(MBOfferType.TypeTitle)
                }
                
                if checkOfferContainsOfferGroup(aHeader: aHeader) {
                    dataTypeArray.append(MBOfferType.OfferGroup)
                }
            }
            dataTypeArray.append(MBOfferType.OfferValidity)
        }
        return dataTypeArray
    }
    
    func typeOfDataInDetailOrDescriptionSection(aDetailsOrDescription : DetailsAndDescription?) -> [MBOfferType] {
        
        var dataTypes : [MBOfferType] = []
        
        if let aDetailsOrDescription =  aDetailsOrDescription {
            // 1
            if aDetailsOrDescription.price != nil {
                
                dataTypes.append(MBOfferType.Price)
            }
            /*
             if let count = aDetailsOrDescription.price?.count {
             
             for _ in 0..<count {
             dataTypes.append(MBOfferType.Price)
             }
             }
             */
            // 2
            if aDetailsOrDescription.rounding != nil  {
                
                dataTypes.append(MBOfferType.Rounding)
            }
            // 3
            if aDetailsOrDescription.textWithTitle != nil {
                
                dataTypes.append(MBOfferType.TextWithTitle)
            }
            // 4
            if aDetailsOrDescription.textWithOutTitle != nil {
                
                dataTypes.append(MBOfferType.TextWithOutTitle)
            }
            // 5
            if aDetailsOrDescription.textWithPoints != nil {
                
                dataTypes.append(MBOfferType.TextWithPoints)
            }
            // 6
            if aDetailsOrDescription.titleSubTitleValueAndDesc != nil {
                
                dataTypes.append(MBOfferType.TitleSubTitleValueAndDesc)
            }
            
            // 7
            if aDetailsOrDescription.date != nil {
                
                dataTypes.append(MBOfferType.Date)
            }
            // 8
            if aDetailsOrDescription.time != nil {
                
                dataTypes.append(MBOfferType.Time)
            }
            // 9
            if aDetailsOrDescription.roamingDetails != nil {
                
                dataTypes.append(MBOfferType.RoamingDetails)
            }
            // 10
            if aDetailsOrDescription.freeResourceValidity != nil {
                
                dataTypes.append(MBOfferType.FreeResourceValidity)
            }
        }
        return dataTypes
    }
    
    func containsOfferOfOfferId(offeringId : String?, mySubscriptions: MySubscription?) -> Bool {
        
        // return complete array if string is empty
        if offeringId?.isBlank == true {
            return false
        } else {
            
            if let mySubscriptions = mySubscriptions {
                
                var containsOffer : Bool = false
                if self.isOffersContainsOfferingId(offeringId: offeringId ?? "", offerArray: mySubscriptions.callOffers) {
                    containsOffer = true
                } else if self.isOffersContainsOfferingId(offeringId: offeringId ?? "", offerArray: mySubscriptions.internetOffers) {
                    containsOffer = true
                } else if self.isOffersContainsOfferingId(offeringId: offeringId ?? "", offerArray: mySubscriptions.smsOffers) {
                    containsOffer = true
                } else if self.isOffersContainsOfferingId(offeringId: offeringId ?? "", offerArray: mySubscriptions.campaignOffers) {
                    containsOffer = true
                } else if self.isOffersContainsOfferingId(offeringId: offeringId ?? "", offerArray: mySubscriptions.tmOffers) {
                    containsOffer = true
                } else if self.isOffersContainsOfferingId(offeringId: offeringId ?? "", offerArray: mySubscriptions.hybridOffers) {
                    containsOffer = true
                } else if self.isOffersContainsOfferingId(offeringId: offeringId ?? "", offerArray: mySubscriptions.roamingOffers) {
                    containsOffer = true
                }
                return containsOffer
                
            } else {
                return false
            }
        }
    }
    
    func isOffersContainsOfferingId(offeringId : String , offerArray : [SubscriptionOffers]? ) -> Bool {
        // Filtering offers array
        let containsOfferingId = offerArray?.contains() { (aOffer) -> Bool in
            
            if let aOfferingId = aOffer.header?.offeringId {
                return aOfferingId.trimmWhiteSpace.containsSubString(subString: offeringId.trimmWhiteSpace)
            } else {
                return false
            }
        }
        
        return containsOfferingId ?? false
    }
    
    func containsOfferOfOfferLevel(offeringLevel : String?, offeringId: String?, mySubscriptions: MySubscription?) -> Bool {
        
        // return complete array if string is empty
        if offeringLevel?.isBlank == true || offeringId?.isBlank == true {
            return false
        } else {
            if let mySubscriptions = mySubscriptions {
                
                var containsOffer : Bool = false
                
                if selectedOffersType == .Call {
                    
                    if self.isOfferLevelMatch(offerLevel: offeringLevel ?? "", offeringId: offeringId ?? "", offerArray: mySubscriptions.callOffers) {
                        containsOffer = true
                    } else {
                        containsOffer = false
                    }
                } else if selectedOffersType == .Internet {
                    
                    if self.isOfferLevelMatch(offerLevel: offeringLevel ?? "", offeringId: offeringId ?? "", offerArray: mySubscriptions.internetOffers) {
                        containsOffer = true
                    } else {
                        containsOffer = false
                    }
                } else if selectedOffersType == .SMS {
                    
                    if self.isOfferLevelMatch(offerLevel: offeringLevel ?? "", offeringId: offeringId ?? "", offerArray: mySubscriptions.smsOffers) {
                        containsOffer = true
                    } else {
                        containsOffer = false
                    }
                } else if selectedOffersType == .Campaign {
                    
                    if self.isOfferLevelMatch(offerLevel: offeringLevel ?? "", offeringId: offeringId ?? "", offerArray: mySubscriptions.campaignOffers) {
                        containsOffer = true
                    } else {
                        containsOffer = false
                    }
                } else if selectedOffersType == .TM {
                    
                    if self.isOfferLevelMatch(offerLevel: offeringLevel ?? "", offeringId: offeringId ?? "", offerArray: mySubscriptions.tmOffers) {
                        containsOffer = true
                    } else {
                        containsOffer = false
                    }
                } else if selectedOffersType == .Hybrid {
                    
                    if self.isOfferLevelMatch(offerLevel: offeringLevel ?? "", offeringId: offeringId ?? "", offerArray: mySubscriptions.hybridOffers) {
                        containsOffer = true
                    } else {
                        containsOffer = false
                    }
                } else if selectedOffersType == .Roaming {
                    
                    if self.isOfferLevelMatch(offerLevel: offeringLevel ?? "", offeringId: offeringId ?? "", offerArray: mySubscriptions.roamingOffers) {
                        containsOffer = true
                    } else {
                        containsOffer = false
                    }
                }
                
                return containsOffer
                
            } else {
                return false
            }
        }
    }
    
    func isOfferLevelMatch(offerLevel : String, offeringId: String, offerArray : [SubscriptionOffers]? ) -> Bool {
        // Filtering offers array
        let offerLevelMatch = offerArray?.contains() { (aOffer) -> Bool in
            
            if let header = aOffer.header {
                
                guard let aOfferLevel = header.offerLevel?.trimmWhiteSpace else {
                    return false
                }
                guard let aOfferingId = header.offeringId?.trimmWhiteSpace else {
                    return false
                }
                
                if aOfferLevel.isEqualToStringIgnoreCase(otherString: offerLevel.trimmWhiteSpace) && aOfferingId.isEqualToStringIgnoreCase(otherString: offeringId.trimmWhiteSpace) != true {
                    
                    return true
                } else {
                    return false
                }
                
            } else {
                return false
            }
        }
        
        return offerLevelMatch ?? false
    }
}

// MARK: - <MBAccordionTableViewDelegate>

extension SuplementaryOffersVC : MBAccordionTableViewDelegate {
    
    func tableView(_ tableView: MBAccordionTableView, willOpenSection section: Int, withHeader header: UITableViewHeaderFooterView?) {
        
        if let headerView = header as? SupplementaryOffersHeaderView {
            headerView.detailSign_btn .setImage(#imageLiteral(resourceName: "minu-sign"), for: UIControl.State.normal)
        }
    }
    
    func tableView(_ tableView: MBAccordionTableView, didOpenSection section: Int, withHeader header: UITableViewHeaderFooterView?) {
        
        if let headerView = header as? SupplementaryOffersHeaderView {
            
            if (selectedSectionViewType != headerView.viewType && isCardView == true) {
                
                selectedSectionViewType = headerView.viewType
                
            } else if (offers[tableView.tag].openedViewSection != headerView.viewType && isCardView == false) {
             
             offers[tableView.tag].openedViewSection = headerView.viewType;
             }
        }
    }
    
    func tableView(_ tableView: MBAccordionTableView, willCloseSection section: Int, withHeader header: UITableViewHeaderFooterView?) {
        
        if let headerView = header as? SupplementaryOffersHeaderView {
            headerView.detailSign_btn .setImage(#imageLiteral(resourceName: "Plus-Sign"), for: UIControl.State.normal)
        }
    }
    
    func tableView(_ tableView: MBAccordionTableView, didCloseSection section: Int, withHeader header: UITableViewHeaderFooterView?) {
        
        if let headerView = header as? SupplementaryOffersHeaderView {
            
            if (selectedSectionViewType == headerView.viewType && isCardView == true) {
                selectedSectionViewType = MBSectionType.Header
                tableView.toggleSection(0)
                
            } else if (offers[tableView.tag].openedViewSection == headerView.viewType && isCardView == false) {
             
             offers[tableView.tag].openedViewSection = MBSectionType.Header;
             tableView.toggleSection(0)
             }
        }
    }
    
    func tableView(_ tableView: MBAccordionTableView, canInteractWithHeaderAtSection section: Int) -> Bool {
        
        if let headerView = tableView.headerView(forSection: section) as? SubscribeButtonView {
            
            if headerView.viewType == MBSectionType.Subscription {
                
                return false
                
            } else {
                
                return true
            }
        } else {
            
            return true
        }
    }
}

// MARK: - Pageing logic
extension SuplementaryOffersVC : UIScrollViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        let visibleRect = CGRect(origin: collectionView.contentOffset, size: collectionView.bounds.size)
        let visiblePoint = CGPoint(x: visibleRect.midX, y: visibleRect.midY)
        let indexPath = collectionView.indexPathForItem(at: visiblePoint)
        
        if indexPath != nil {
            if offers.count > 0 {
                
                let index : Int = indexPath?.item ?? 0
                
                /* Check if user scrolled to new offer and then change count and close all opened section (section closing only working for List view mode.)*/
                if "\(index + 1 )/\(offers.count)" != offerNumber_lbl.text {
                    offers.forEach { (aOfferObject) in
                        aOfferObject.openedViewSection = MBSectionType.Header
                    }
                    offerNumber_lbl.text = "\(index + 1 )/\(offers.count)"
                }
                
            } else {
                offerNumber_lbl.text = "0/\(offers.count)"
            }
        }
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        
        if scrollView == collectionView {
            openSelectedSection()
        }
    }
}
