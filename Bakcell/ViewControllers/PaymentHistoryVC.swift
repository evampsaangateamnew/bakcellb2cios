//
//  PaymentHistoryVC.swift
//  Bakcell
//
//  Created by Saad Riaz on 6/9/17.
//  Copyright © 2017 evampsaanga. All rights reserved.
//

import UIKit
import ObjectMapper

class PaymentHistoryVC: BaseVC, UITableViewDelegate,UITableViewDataSource {
    
    var paymentHistoryResponse : PaymentHistoryListHandler?
    
    @IBOutlet var pageCount: UILabel!
    @IBOutlet var date_lbl: UILabel!
    @IBOutlet var loanId_lbl: UILabel!
    @IBOutlet var amount_lbl: UILabel!
    @IBOutlet var dateDropDown: UIDropDown!
    @IBOutlet var tableView: UITableView!
    
    
    //MARK:- ViewController Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.allowsSelection = false
        tableView.delegate = self
        tableView.dataSource = self
        
        loadTableViewData()
        
        // Load default data
        let todayDate = MBUtilities.todayDateString(dateFormate: Constants.kAPIFormat)
        self.getPaymentHistory(startDate: todayDate, endDate: todayDate)
        
        //Drop Down
        dateDropDown.optionsTextAlignment = NSTextAlignment.center
        dateDropDown.textAlignment = NSTextAlignment.center
        
        dateDropDown.placeholder = Localized("DropDown_CurrentDay")
        self.dateDropDown.rowBackgroundColor = UIColor.clear
        self.dateDropDown.borderWidth = 0
        self.dateDropDown.tableHeight = 140
        dateDropDown.fontSize = 12
        
        dateDropDown.options = [Localized("DropDown_CurrentDay"),Localized("DropDown_Last7Days"), Localized("DropDown_Last30Days"), Localized("DropDown_Last90Days")]
        
        dateDropDown.didSelect { (option, index) in
            
            MBUserSession.shared.dateFormatter.dateFormat = Constants.kAPIFormat
            
            let todayDate = MBUtilities.todayDateString(dateFormate: Constants.kAPIFormat)
            
            switch index {
            case 0:
                
                // Get data of Today
                self.getPaymentHistory(startDate: todayDate, endDate: todayDate)
                
            case 1:
                
                // Get data of previous seven days
                let sevenDaysAgo = MBUtilities.todayDateString(withAdditionalValue: -7)
                self.getPaymentHistory(startDate: sevenDaysAgo, endDate: todayDate)
                
                
            case 2:
                
                // Get data of previous thirty days
                let thirtyDaysAgo = MBUtilities.todayDateString(withAdditionalValue: -30)
                self.getPaymentHistory(startDate: thirtyDaysAgo, endDate: todayDate)
                
            case 3:
                // Get data of previous ninety days
                let ninetyDaysAgo = MBUtilities.todayDateString(withAdditionalValue: -90)
                self.getPaymentHistory(startDate: ninetyDaysAgo, endDate: todayDate)
                
            default:
                print("destructive")
                
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        loadViewLayout()
    }
    
    //MARK: - FUNCTIONS
    func loadViewLayout() {
        
        date_lbl.font = UIFont.MBArial(fontSize: 14)
        loanId_lbl.font = UIFont.MBArial(fontSize: 14)
        amount_lbl.font = UIFont.MBArial(fontSize: 14)
        
        date_lbl.text = Localized("PY_Date")
        loanId_lbl.text = Localized("PY_LoanID")
        amount_lbl.text = Localized("PY_Amount")
        
    }
    
    func loadTableViewData() {
        
        if (paymentHistoryResponse?.paymentHistory?.count ?? 0) <= 0 {
            self.tableView.showDescriptionViewWithImage(description: Localized("Message_NoDataAvalible"))
        } else {
            self.tableView.hideDescriptionView()
        }
        self.tableView.reloadData()
        showPageNumber()
    }
    
    //MARK: - TABLE VIEW METHODS
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return (self.paymentHistoryResponse?.paymentHistory?.count) ?? 0
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 52
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "cellID", for: indexPath) as? PaymentHistoryCell {
            
            cell.dateTime.font = UIFont.MBArial(fontSize: 14)
            cell.loanId.font = UIFont.MBArial(fontSize: 14)
            cell.price_lbl.font = UIFont.MBArial(fontSize: 14)
            
            cell.dateTime.text = paymentHistoryResponse?.paymentHistory?[indexPath.row].dateTime
            cell.loanId.text = paymentHistoryResponse?.paymentHistory?[indexPath.row].loanID
            
            cell.price_lbl.attributedText = MBUtilities.createAttributedTextWithManatSign(paymentHistoryResponse?.paymentHistory?[indexPath.row].amount ?? "", textColor: .black)
            
            return cell
        } else {
            return UITableViewCell()
        }
    }
    
    //MARK: - API Calls
    /// Call 'getPaymentHistory' API .
    ///
    /// - returns: PaymentHistory
    func getPaymentHistory(startDate:String?, endDate:String?) {
        
        activityIndicator.showActivityIndicator()
        
        _ = MBAPIClient.sharedClient.getPaymentHistory(StartDate: startDate ?? "", EndDate: endDate ?? "", { (response, resultData, error, isCancelled, status, resultCode, resultDesc) in
            
            self.activityIndicator.hideActivityIndicator()
            if error != nil {
                //EventLog
                MBAnalyticsManager.logEvent(screenName: "Loan Screen", contentType:"Loan Payment History Screen" , status:"Failure" )
                
                error?.showServerErrorInViewController(self)
                
            } else {
                // handling data from API response.
                if resultCode == Constants.MBAPIStatusCode.succes.rawValue {
                    //EventLog
                    MBAnalyticsManager.logEvent(screenName: "Loan Screen", contentType:"Loan Payment History Screen" , status:"Success" )
                    
                    
                    
                    // Parssing response data
                    if let paymentHistoryList = Mapper<PaymentHistoryListHandler>().map(JSONObject: resultData){
                        
                        self.paymentHistoryResponse = paymentHistoryList
                        
                    }
                    
                } else {
                    //EventLog
                    MBAnalyticsManager.logEvent(screenName: "Loan Screen", contentType:"Loan Payment History Screen" , status:"Failure" )
                    
                    // Show error alert to user
                    self.showErrorAlertWithMessage(message: resultDesc)
                }
            }
            
            // Reload TableView Data
            self.loadTableViewData()
        })
    }
}

// MARK: UIScrollViewDelegate
extension PaymentHistoryVC: UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        showPageNumber()
    }
    
    func showPageNumber() {
        
        //        pageCount.text = "\(tableView.lastVisibleSection())/\(paymentHistoryResponse?.paymentHistory?.count ?? 0)"
        
        let visibleRect = CGRect(origin: tableView.contentOffset, size: tableView.bounds.size)
        let visiblePoint = CGPoint(x: visibleRect.midX, y: visibleRect.maxY)
        let indexPath = tableView.indexPathForRow(at: visiblePoint)
        
        let count = paymentHistoryResponse?.paymentHistory?.count ?? 0
        if indexPath != nil {
            if count > 0 {
                
                let index : Int = indexPath?.item ?? 0
                pageCount.text = "\(index + 1 )/\(count)"
            } else {
                pageCount.text = "0/\(count)"
            }
        } else {
            pageCount.text = "\(count)/\(count)"
        }
        
    }
}

