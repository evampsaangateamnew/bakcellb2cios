//
//  MBButton.swift
//  Bakcell B2B
//
//  Created by AbdulRehman Warraich on 5/23/18.
//  Copyright © 2018 Evamp&Saanga. All rights reserved.
//

import UIKit

//@IBDesignable
class MBButton: UIButton {
    
    enum ButtonLayoutType: Int {
        case grayButton = 1
        case whiteButton = 2
        case centerImageAndBottomTitle = 4
        case redButton = 5
        case borderdButton = 6
        case grayBorderdButton = 7
        case other = 0
    }
    
    var buttonLayoutTypeInfo : ButtonLayoutType = .other
    
    // IB: use the adapter
    @IBInspectable var buttonLayoutType:Int {
        get {
            return self.buttonLayoutTypeInfo.rawValue
            
        } set( labelTypeIndex) {
            
            self.buttonLayoutTypeInfo = ButtonLayoutType(rawValue: labelTypeIndex) ?? .other
            adjustsButtonLayout()
        }
    }
    
    @IBInspectable var roundCorners:Bool = false {
        didSet {
            if self.roundCorners == true {
                self.roundAllCorners(radius: self.bounds.height/2)
                self.contentEdgeInsets = UIEdgeInsets(top: 0, left: 12, bottom: 0, right: 12)
            } else {
                self.contentEdgeInsets = UIEdgeInsets(top: 0, left: 8, bottom: 0, right: 8)
                self.roundAllCorners(radius: 0)
            }
            
        }
    }
    
    @IBInspectable var spacingBWTitleAndImage : CGFloat = 4.0{
        didSet{
            if self.buttonLayoutType == 4{
                 adjustsButtonLayout()
            }
           
        }
    }
    // MARK: - Life Cycle Methods
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        adjustsFontSizeToFitDevice()
    }
    
   
    
    override func setTitle(_ title: String?, for state: UIControl.State) {
        super.setTitle(title, for: state)
        
        self.setNeedsLayout()
        self.layoutIfNeeded()
    }
    
    //MARK: - Functions
    func adjustsFontSizeToFitDevice() {
        
        self.titleLabel?.font = UIFont.MBArial(fontSize: 14)
    }
    
    func adjustsButtonLayout() {
        
        switch buttonLayoutTypeInfo {
        case .grayButton:
            
            self.roundCorners = true
            self.backgroundColor = UIColor.MBButtonBackgroundGrayColor
            self.setTitleColor(UIColor.MBTextBlack, for: UIControl.State.normal)
            
            self.layer.borderColor = UIColor.MBButtonBackgroundGrayColor.cgColor
            self.layer.borderWidth = 1
            
            break
            
        case .whiteButton:
            self.roundCorners = true
            self.backgroundColor = UIColor.white
            self.setTitleColor(UIColor.MBTextGrayColor, for: UIControl.State.normal)
            
            self.layer.borderColor = UIColor.MBBorderGrayColor.cgColor
            self.layer.borderWidth = 1
            break
            
        case .redButton:
            self.roundCorners = true
            self.backgroundColor = UIColor.MBRedColor
            self.setTitleColor(UIColor.white, for: UIControl.State.normal)
            
            self.layer.borderColor = UIColor.MBRedColor.cgColor
            self.layer.borderWidth = 1
            break
            
        case .centerImageAndBottomTitle:

            
            if let imageSize = self.imageView?.frame.size {
                self.titleEdgeInsets = UIEdgeInsets(top: 0, left: -imageSize.width, bottom: -(imageSize.height + spacingBWTitleAndImage), right: 0)
            }
            
            if let titleSize = self.titleLabel?.frame.size {
                self.imageEdgeInsets = UIEdgeInsets(top: -(titleSize.height + spacingBWTitleAndImage), left: 0, bottom: 0, right: -titleSize.width)
                self.setTitleColor(UIColor.MBTextGrayColor, for: UIControl.State.normal)
            }
            
            self.layer.borderColor = UIColor.MBBorderGrayColor.cgColor
            self.layer.borderWidth = 1
            break
        case .borderdButton:
            self.layer.borderColor = UIColor.MBBorderGrayColor.cgColor
            self.layer.borderWidth = 1
            break
        case .grayBorderdButton:
            self.layer.borderColor = UIColor.MBButtonBackgroundGrayColor.cgColor
            self.layer.borderWidth = 1
            break
        default:
            break
        }
    }
    
    func setButtonLayoutType(_ type: ButtonLayoutType?) {
        if let newButtonType = type {
            self.buttonLayoutType = newButtonType.rawValue
        }
    }

    func setState(isEnable : Bool) {
        
        if isEnable {
            self.isEnabled = true
            self.titleLabel?.isEnabled = true
        } else {
            self.isEnabled = false
            self.titleLabel?.isEnabled = false
        }
    }
    
}
