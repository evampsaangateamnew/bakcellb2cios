//
//  UIColor+MB.swift
//  Bakcell
//
//  Created by AbdulRehman Warraich on 8/15/17.
//  Copyright © 2017 evampsaanga. All rights reserved.
//

import Foundation
import UIKit
extension UIColor {
    public class var MBRedColor : UIColor{
        // return UIColor(red: 224/255.0, green: 0/255.0, blue: 52/255.0, alpha: 1.0)
        return UIColor(hexString: "#E00034")
        //return UIColor(hexString: "#E00134")
        
    }//E00134
    
    public class var MBLightGrayColor: UIColor{
        return UIColor(hexString: "#808080")
    }
    public class var MBButtonBackgroundGrayColor: UIColor{
        return UIColor(hexString: "BEBEBE")
    }
    
    public class var MBTextBlack: UIColor{
        return UIColor (hexString: "#363636")
    }
    
    public class var MBStoreGrayColor: UIColor{
        return UIColor(red: 237.0/255.0, green: 237.0/255.0, blue: 237.0/255.0, alpha: 1.0)
    }
    
    public class var MBDimLightGrayColor: UIColor{
        return UIColor(red: 236.0/255.0, green: 236.0/255.0, blue: 236.0/255.0, alpha: 1.0)
    }
    
    public class var MBTextGrayColor: UIColor{
        return UIColor(red: 85.0/255.0, green: 85.0/255.0, blue: 85.0/255.0, alpha: 1.0)
        //        555555
    }
    
    public class var MBDarkGrayColor: UIColor{
        return UIColor.darkGray
    }
    public class var MBLightProgressTrackColor: UIColor{
        return UIColor (red: 229.0/255.0, green: 79.0/255.0, blue: 114.0/255.0, alpha: 1.0)
    }
    
    public class var MBBorderGrayColor: UIColor{
        return UIColor (hexString: "#ECECEC")
    }
    
    public class var MBGreen: UIColor{
        return UIColor(hexString: "#6DBC36")
    }
    
    public class var MBBarRed: UIColor{
        return UIColor(hexString: "#FF3939")
        
    }
    public class var MBBarOrange: UIColor{
        return UIColor(hexString: "#FF8A3B")
    }
    public class var MBBarGreen: UIColor{
        return UIColor(hexString: "#73F473")
    }
    
    public class var MBProgerssDisableTextColor: UIColor{
        return UIColor(hexString: "#EA456B")
    }
    public class var MBProgerssDisablePathColor: UIColor{
        return UIColor(hexString: "#D71441")
    }
    public class var MBProgerssEnablePathColor: UIColor{
        return UIColor(hexString: "#808080")
    }
    public class var MBLightRedColor : UIColor{
        return UIColor (red: 207.0/255.0, green: 81.0/255.0, blue: 104.0/255.0, alpha: 1.0)
    }
    
}

// Based on GradientProgressBar Library
extension UIColor {
    // Create color from RGB
    convenience init(absoluteRed: Int, green: Int, blue: Int) {
        self.init(
            absoluteRed: absoluteRed,
            green: green,
            blue: blue,
            alpha: 1.0
        )
    }
    
    // Create color from RGBA
    convenience init(absoluteRed: Int, green: Int, blue: Int, alpha: CGFloat) {
        let normalizedRed = CGFloat(absoluteRed) / 255
        let normalizedGreen = CGFloat(green) / 255
        let normalizedBlue = CGFloat(blue) / 255
        
        self.init(
            red: normalizedRed,
            green: normalizedGreen,
            blue: normalizedBlue,
            alpha: alpha
        )
    }
    
    // Color from HEX-Value
    // Based on: http://stackoverflow.com/a/24263296
    convenience init(hexValue:Int) {
        self.init(
            absoluteRed: (hexValue >> 16) & 0xff,
            green: (hexValue >> 8) & 0xff,
            blue: hexValue & 0xff
        )
    }
    
    // Color from HEX-String
    // Based on: http://stackoverflow.com/a/27203691
    convenience init(hexString:String) {
        var normalizedHexString = hexString.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (normalizedHexString.hasPrefix("#")) {
            normalizedHexString.remove(at: normalizedHexString.startIndex)
        }
        //       normalizedHexString = normalizedHexString.replacingOccurrences(of: "#", with: "")
        
        // Convert to hexadecimal integer
        var hexValue:UInt32 = 0
        Scanner(string: normalizedHexString).scanHexInt32(&hexValue)
        
        self.init(
            hexValue:Int(hexValue)
        )
    }
}
