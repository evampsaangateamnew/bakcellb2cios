//
//  LoginVC.swift
//  Bakcell
//
//  Created by Shujat on 16/05/2017.
//  Copyright © 2017 evampsaanga. All rights reserved.
//

import UIKit
import KYDrawerController
import ObjectMapper

class LoginVC: BaseVC {
    
    //MARK:- Properties
    var isForgotPassword : Bool = false
    var registrationType :Constants.RegistrationType = .registration
    
    //MARK: - IBOutlet
    @IBOutlet var mobileNumber_txt: UITextField!
    @IBOutlet var password_txt: UITextField!
    @IBOutlet var forgot_btn: UIButton!
    @IBOutlet var login_lbl: UILabel!
    @IBOutlet var haveAccount_lbl: UILabel!
    @IBOutlet var signUp_lbl: UILabel!
    @IBOutlet var backcelIcon_img: UIImageView!
    
    @IBOutlet var languageLable: UILabel!
    
    //MARK:- ViewController Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Setting UITextField delegate to self
        mobileNumber_txt.delegate = self
        password_txt.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.interactivePop(false)
        
        loadViewLayout()
        
        #if DEBUG
        mobileNumber_txt.text   = "556199963"
        password_txt.text       = "123456"
        #else
        mobileNumber_txt.text   = ""
        password_txt.text       = ""
        #endif
        
        activityIndicator.removeAllActivityIndicator()
        
        if registrationType == .addAccount {
            self.backButton?.isHidden = false
        } else {
            self.backButton?.isHidden = true
        }
        
    }
    
    //MARK: - IBACTIONS
    @IBAction func changePressed(_ sender:AnyObject) {
        
        if let changelang = self.myStoryBoard.instantiateViewController(withIdentifier: "SelectionAlertVC") as? SelectionAlertVC {
            
            changelang.setOKButtonCompletionHandler { (aString) in
                
                // Reloading layout Text accourding to seletcted language
                self.loadViewLayout()
            }
            
            self.presentPOPUP(changelang, animated: true, completion:  nil)
        }
    }
    
    @IBAction func forgotPasswordPressed(_ sender:AnyObject) {
        mobileNumber_txt.resignFirstResponder()
        password_txt.resignFirstResponder()
        
        if let signUp = self.myStoryBoard.instantiateViewController(withIdentifier: "SignupVC") as? SignupVC {
            signUp.isForgotPassword = true
            signUp.registrationType = self.registrationType
            self.navigationController?.pushViewController(signUp, animated: true)
        }
    }
    
    @IBAction func signupPressed(_ sender:AnyObject) {
        mobileNumber_txt.resignFirstResponder()
        password_txt.resignFirstResponder()
        
        if let signupVC = self.myStoryBoard.instantiateViewController(withIdentifier: "SignupVC") as? SignupVC {
            signupVC.isForgotPassword = false
            signupVC.registrationType = self.registrationType
            self.navigationController?.pushViewController(signupVC, animated: true)
        }
    }
    
    @IBAction func loginPressed(_ sender: UIButton) {
        
        //***** Need to remove inapp survey function calling*****/
//        showDashbaordSurvey()
//        return
        
        
        
        var msisdn : String = ""
        var password : String = ""
        
        // MSISDN validation
        if let mobileNumberText = mobileNumber_txt.text ,
            mobileNumberText.count == 9 {
            if registrationType == .addAccount,
                MBUserSession.shared.loggedInUsers?.users?.contains(where: {($0.userInfo?.msisdn ?? "").isEqualToStringIgnoreCase(otherString: mobileNumberText)}) ?? false {
                self.showErrorAlertWithMessage(message: Localized("Error_AddNumber"))
                return
            } else {
                msisdn = mobileNumberText
            }
            
        } else {
            self.showErrorAlertWithMessage(message: Localized("Message_EnterValidNumber"))
            return
        }
        
        // Password validation
        if let passwordText = password_txt.text {
            
            let passwordStrength:Constants.MBPasswordStrength = self.determinePasswordStrength(Text: passwordText)
            
            switch passwordStrength {
                
            case Constants.MBPasswordStrength.didNotMatchCriteria:
                
                self.showErrorAlertWithMessage(message: Localized("Message_InvalidPassword"))
                return
            case Constants.MBPasswordStrength.Week, Constants.MBPasswordStrength.Medium, Constants.MBPasswordStrength.Strong:
                password = passwordText
            }
            
        } else {
            self.showErrorAlertWithMessage(message: Localized("Message_InvalidPassword"))
            return
        }
        
        mobileNumber_txt.resignFirstResponder()
        password_txt.resignFirstResponder()
        // API call for user authentication
        authenticateUserAPICall(MSISDN: msisdn, Password: password)
        
    }
    
    //MARK: - FUNCTIONS
    
    /**
     Set localized text in viewController
     */
    func loadViewLayout() {
        forgot_btn.titleLabel?.font = UIFont.MBArial(fontSize: 16)
        forgot_btn.setTitle(Localized("Title_Forgot"), for: UIControl.State.normal)
        login_lbl.font = UIFont.MBArialBold(fontSize: 16)
        login_lbl.text = Localized("Title_Login")
        signUp_lbl.font = UIFont.MBArialBold(fontSize: 16)
        signUp_lbl.text = Localized("Title_SignUp")
        haveAccount_lbl.font = UIFont.MBArial(fontSize: 14)
        haveAccount_lbl.text = Localized("Title_HaveAccount")
        mobileNumber_txt.placeholder = Localized("Placeholder_MobileNumber")
        password_txt.placeholder = Localized("Placeholder_Password")
        
        backcelIcon_img.image = UIImage(named: Localized("Img_BakcelLogo"))
        languageLable.font = UIFont.MBArialBold(fontSize: 16)
        languageLable.text = Localized("Info_Language_Short")
        
    }
    
    func redirectToDashboardScreen(isLoggedInNow :Bool) {
        
        if let count = self.navigationController?.viewControllers.count,
            count > 1,
            let firstVC = self.navigationController?.viewControllers.first,
            let mainViewController :TabbarController = self.myStoryBoard.instantiateViewController(withIdentifier: "TabbarController") as? TabbarController ,
            let drawerViewController :SideDrawerVC = self.myStoryBoard.instantiateViewController(withIdentifier: "SideDrawerVC") as? SideDrawerVC {
            
            mainViewController.isLoggedInNow = isLoggedInNow
            let drawerController = KYDrawerController(drawerDirection: .left, drawerWidth: (UIScreen.main.bounds.width * 0.85))
            drawerController.mainViewController = mainViewController
            drawerController.drawerViewController = drawerViewController
            
            self.navigationController?.viewControllers = [firstVC, drawerController]
        }
    }
    
    //MARK: - API Calls
    
    /**
     Call 'authenticateUser' API with the specified `MSISDN` and `Password`.
     
     - parameter MSISDN: MSISDN of current user.
     - parameter pasword: pasword of current user.
     
     - returns: void
     */
    func authenticateUserAPICall(MSISDN msisdn : String , Password pasword : String) {
        
        activityIndicator.showActivityIndicator()
        _ = MBAPIClient.sharedClient.authenticateUser(MSISDN:msisdn , Password:pasword ,{ (response, resultData, error, isCancelled, status, resultCode, resultDesc) in
            
            self.activityIndicator.hideActivityIndicator()
            if error != nil {
                error?.showServerErrorInViewController(self)
                //EventLog
                MBAnalyticsManager.logEvent(screenName: "Login Screen", contentType:"User failed to Login" , status:"Failed" )
                
            } else {
                // handling data from API response.
                if resultCode == Constants.MBAPIStatusCode.succes.rawValue {
                    
                    //EventLog
                    MBAnalyticsManager.logEvent(screenName: "Login Screen", contentType:"User LoggedIn" , status:"Success" )
                    
                    // Parssing response data
                    if let authenticateUserHandler = Mapper<SaveCustomerModel>().map(JSONObject:resultData) {
                        
                        if self.registrationType == .addAccount {
                            /* Save logged in user information */
                            MBUserInfoUtilities.addNewUser(newUserMSISDN: msisdn, loginInfo: authenticateUserHandler)
                        } else {
                            /* Save logged in user information */
                            MBUserInfoUtilities.saveLoginInfo(loggedInUserMSISDN: msisdn, loginInfo: authenticateUserHandler)
                        }
                        // Reset Biometric status
                        // UserDefaults.standard.saveBoolForKey(boolValue: false, forKey: Constants.K_IsBiometricEnabled)
                        
                        if MBUtilities.isTutorialShowenBefore() == false &&
                            self.registrationType == .registration,
                            let tutorialsVC = self.myStoryBoard.instantiateViewController(withIdentifier: "TutsVC") as? TutsVC {
                            
                            // Update Check for tutorial
                            MBUtilities.updateTutorialShowenStatus()
                            
                            tutorialsVC.isLoggedInNow = true
                            self.navigationController?.pushViewController(tutorialsVC, animated: true)
                            
                        } else {
                            self.redirectToDashboardScreen(isLoggedInNow: true)
                        }
                        
                    } else {
                        // Show error alert to user
                        self.showErrorAlertWithMessage(message: resultDesc)
                    }
                } else {
                    //EventLog
                    MBAnalyticsManager.logEvent(screenName: "Login Screen", contentType:"User failed to Login" , status:"Failed" )
                    // Show error alert to user
                    self.showErrorAlertWithMessage(message: resultDesc)
                }
            }
        })
    }
    
    
    
    
}

//MARK: - Textfield delagates
extension LoginVC : UITextFieldDelegate {
    
    //Set max Limit for textfields
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let text = textField.text else { return false } // Check that if text is nil then return false
        
        // Length of text in UITextField
        let newLength = text.count + string.count - range.length
        
        // Check for MobileNumber textField
        if textField == mobileNumber_txt {
            
            // Check for number text
            let allowedCharacters = CharacterSet(charactersIn: Constants.allowedNumbers)
            let characterSet = CharacterSet(charactersIn: string)
            if newLength <= 9 && allowedCharacters.isSuperset(of: characterSet) {
                return  true
            } else {
                return  false
            }
            
        } else if textField == password_txt {
            return newLength <= 15 /* Restriction for password length */
        } else {
            return false
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == mobileNumber_txt {
            password_txt.becomeFirstResponder()
            return false
        } else if textField == password_txt {
            _ = textField.resignFirstResponder()
            loginPressed(UIButton())
            return false
        } else {
            return true
        }
    }
}

