//
//  AppMenu.swift
//  Bakcell
//
//  Created by AbdulRehman Warraich on 8/21/17.
//  Copyright © 2017 evampsaanga. All rights reserved.
//

import Foundation
import ObjectMapper

struct AppMenusResponseModel : Mappable {
    var MenuEN : AppMenusModel?
    var MenuAZ : AppMenusModel?
    var MenuRU : AppMenusModel?
    
    init?(map: Map) {
    }
    
    mutating func mapping(map: Map) {
        
        MenuEN <- map["MenuEN"]
        MenuAZ <- map["MenuAZ"]
        MenuRU <- map["MenuRU"]
    }
    
    func getMenuForCurrentLanguage(_ lang :Constants.MBLanguage) -> AppMenusModel? {
        switch lang {
            
        case .Russian:
            return self.MenuRU
        case .English:
            return self.MenuEN
        case .Azeri:
            return self.MenuAZ
        }
    }
    
}

struct AppMenusModel : Mappable {
    var menuHorizontal : [MenuHorizontal]?
    var menuVertical : [MenuVertical]?
    
    init?(map: Map) {
        
    }
    init(menuHorizontal :[MenuHorizontal]?, menuVertical: [MenuVertical]?) {
        self.menuHorizontal = menuHorizontal
        self.menuVertical = menuVertical
    }
    mutating func mapping(map: Map) {
        
        menuHorizontal <- map["menuHorizontal"]
        menuVertical <- map["menuVertical"]
    }
    
}


struct MenuVertical : Mappable {
    var identifier : String = ""
    var title : String = ""
    var sortOrder : String = ""
    var dataV2s : String = ""
    var items : String = ""
    var iconName : String = ""
    
    init?(map: Map) {
        
    }
    
    init(identifier:String, title:String, sortOrder:String, iconName:String ) {
        self.identifier = identifier
        self.title = title
        self.sortOrder = sortOrder
        self.iconName = iconName
    }
    
    mutating func mapping(map: Map) {
        
        identifier <- map["identifier"]
        title <- map["title"]
        sortOrder <- map["sortOrder"]
        dataV2s <- map["dataV2s"]
        items <- map["items"]
    }
    
}


struct MenuHorizontal : Mappable {
    var identifier : String = ""
    var title : String = ""
    var sortOrder : String = ""
    var dataV2s : [DataV2s]?
    var items : [Items]?
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        
        identifier <- map["identifier"]
        title <- map["title"]
        sortOrder <- map["sortOrder"]
        dataV2s <- map["dataV2s"]
        items <- map["items"]
    }
    
}

struct DataV2s : Mappable {
    var identifier : String = ""
    var title : String = ""
    var sortOrder : String = ""
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        
        identifier <- map["identifier"]
        title <- map["title"]
        sortOrder <- map["sortOrder"]
    }
    
}


struct Items : Mappable {
    var identifier : String = ""
    var title : String = ""
    var sortOrder : String = ""
    var iconName : String = ""
    
    init?(map: Map) {
        
    }
    
    init(identifier:String, title:String, sortOrder:String, iconName:String ) {
        self.identifier = identifier
        self.title = title
        self.sortOrder = sortOrder
        self.iconName = iconName
    }
    
    mutating func mapping(map: Map) {
        
        identifier <- map["identifier"]
        title <- map["title"]
        sortOrder <- map["sortOrder"]
    }
    
}
