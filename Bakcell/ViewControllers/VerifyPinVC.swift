//
//  VerifyPinVC.swift
//  Bakcell
//
//  Created by Saad Riaz on 5/29/17.
//  Copyright © 2017 evampsaanga. All rights reserved.
//

import UIKit
import ObjectMapper

class VerifyPinVC: BaseVC {

     //MARK: - IBOutlet
    @IBOutlet var catDropDown: UIDropDown!
    @IBOutlet var dateDropDown: UIDropDown!
    @IBOutlet var pinField: UITextField!
    @IBOutlet var description_lbl: UILabel!
    @IBOutlet var pinNotRecieve_lbl: UILabel!
    @IBOutlet var submit_btn: UIButton!
    @IBOutlet var resend_lbl: UILabel!
    @IBOutlet var resentButtonView: UIView!
    
    var numberText : String?
    var pinText : String?

     //MARK: - View Controller Methods
    override func viewDidLoad() {
        super.viewDidLoad()

        loadViewController()
        
        pinField.delegate = self
        
        //Drop Down
        catDropDown.optionsTextAlignment = NSTextAlignment.center
        catDropDown.textAlignment = NSTextAlignment.center
        catDropDown.placeholder = Localized("DropDown_All")
        self.catDropDown.rowBackgroundColor = UIColor.clear
        self.catDropDown.borderWidth = 0
        self.catDropDown.tableHeight = 140
        catDropDown.optionsSize = 14
        catDropDown.fontSize = 12
        catDropDown.options = [Localized("DropDown_All"),Localized("DropDown_Internet"), Localized("DropDown_Voice"), Localized("DropDown_SMS"),Localized("DropDown_Others")]


        //Drop Down
        dateDropDown.optionsTextAlignment = NSTextAlignment.center
        dateDropDown.textAlignment = NSTextAlignment.center
        dateDropDown.placeholder = Localized("DropDown_CurrentDay")
        self.dateDropDown.rowBackgroundColor = UIColor.clear
        self.dateDropDown.borderWidth = 0
        self.dateDropDown.tableHeight = 176
        dateDropDown.optionsSize = 14
        dateDropDown.fontSize = 12
        dateDropDown.options = [Localized("DropDown_CurrentDay"),Localized("DropDown_Last7Days"), Localized("DropDown_Last30Days"), Localized("DropDown_PreviousMonth"),Localized("DropDown_CustomPeriod")]

    }

    //MARK: IBACTIONS
    @IBAction func submitPressed(_ sender: Any) {        
        var otp : String = ""
        // OTP validation
        if let otpText = pinField.text,
            otpText.lengthWithOutSpace == 4 {
            
            otp = otpText.trimmWhiteSpace
            
        } else {
            self.showErrorAlertWithMessage(message: Localized("Message_InvalidOTP"))
            return
        }
        
        // API call for OTP verification
        self.verifyPin(accountId: numberText, pin: otp)

    }

    @IBAction func reSendPinPressed(_ sender: Any) {
        // API call for resending OTP
      reSendPin(MSISDN: MBUserSession.shared.msisdn, ofType: .UsageHistory)
    }
    
    //MARK: - FUNCTIONS
    func loadViewController(){
        description_lbl.text = Localized("Title_PinDescription")
        pinNotRecieve_lbl.text = Localized("Title_PinNotRecieved")
        resend_lbl.text = Localized("Title_RESEND")
        submit_btn.setTitle(Localized("BtnTitle_Submit"), for: UIControl.State.normal)

        pinField.textColor = UIColor.MBTextGrayColor
        pinField.attributedPlaceholder =  NSAttributedString(string: Localized("PlaceHolder_EnterPIN"), attributes: [NSAttributedString.Key.foregroundColor: UIColor.MBTextGrayColor])
    }
    
    /**
     Enable and disable Resent button accourding to isEnabled value.
     
     - parameter buttonView: view which have image, arrow and button.
     - parameter isEnabled: set ture for enabling and false for disabling.
     
     - returns: void
     */
    func setResendButtonSelection(buttonView: UIView, isEnabled: Bool) {
        
        // Loop through all subviews of buttonView
        buttonView.subviews.forEach { (aView) in
            
            // Check if subView is UIImageView
            if aView is UIImageView {
                
                // Convert view to UIImageView
                if let imageView = aView as? UIImageView {
                    
                    // Set UIImageView image
                    if isEnabled {
                        imageView.image = UIImage(named: "arrow_resend")
                    } else {
                        imageView.image = UIImage(named: "disable_arrow_resend")
                    }
                }
            } else if aView is UILabel { // Check if subView is UILabel
                
                // Convert view to UILabel
                if let tiltLable = aView as? UILabel {
                    
                    // Change label textColor
                    if isEnabled {
                        tiltLable.textColor = UIColor.black
                    } else {
                        tiltLable.textColor = UIColor.lightGray
                    }
                }
            } else if aView is UIButton { // Check if subView is UIButton
                
                // Convert view to UIButton and Enable and Disable
                if let aButton = aView as? UIButton {
                    aButton.isEnabled = isEnabled
                }
            }
        }
    }
    
    //MARK: - API Calls
    /// Call 'verifypin' API .
    ///
    /// - returns: Pin
    func verifyPin(accountId:String?, pin:String?) {

        activityIndicator.showActivityIndicator()
        
        _ = MBAPIClient.sharedClient.verifyPin(Pin: pin?.trimmWhiteSpace ?? "", { (response, resultData, error, isCancelled, status, resultCode, resultDesc) in
            
            self.activityIndicator.hideActivityIndicator()
            if error != nil {
                //EventLog
                MBAnalyticsManager.logEvent(screenName: "My Account", contentType:"Usage History Detail Screen" , status:"Usage Detail PIN Verification Failure")
                
                error?.showServerErrorInViewController(self)
                
            } else {
                // handling data from API response.
                if resultCode == Constants.MBAPIStatusCode.succes.rawValue {
                //EventLog
                MBAnalyticsManager.logEvent(screenName: "My Account", contentType:"Usage History Detail Screen" , status:"Usage Detail PIN Verification Success")
                
                
                    if let _ = Mapper<MessageResponse>().map(JSONObject: resultData) {

                        MBUserSession.shared.isOTPVerified = true
                        let usage = self.myStoryBoard.instantiateViewController(withIdentifier: "UsageHistoryVC")as! UsageHistoryVC
                        self.navigationController?.pushViewController(usage, animated: true)

                    }

                } else {
                    //EventLog
                    MBAnalyticsManager.logEvent(screenName: "My Account", contentType:"Usage History Detail Screen" , status:"Usage Detail PIN Verification Failure")
                    
                    // Show error alert to user
                    self.showErrorAlertWithMessage(message: resultDesc)
                }
            }
        })
    }

   
    /// Call 'reSendPin' API with the specified `MSISDN`
    ///
    /// - parameter MSISDN:     The MSISDN.
    /// - returns: Void
    func reSendPin(MSISDN msisdn : String, ofType : Constants.MBResendType ) {
        activityIndicator.showActivityIndicator()
        _ = MBAPIClient.sharedClient.historyReSendPIN(MSISDN: msisdn, ofType: ofType,  { (response, resultData, error, isCancelled, status, resultCode, resultDesc) in
            self.activityIndicator.hideActivityIndicator()
            if error != nil {
                error?.showServerErrorInViewController(self)

            } else {
                // handling data from API response.
                if resultCode == Constants.MBAPIStatusCode.succes.rawValue {

                    // Parssing response data
                    if let newPinHandler = Mapper<ReSendPin>().map(JSONObject:resultData) {

                        // new pin sent to user
                        self.pinField.text = newPinHandler.pin
                    }
                } else {
                    // Show error alert to user
                    self.showErrorAlertWithMessage(message: resultDesc)
                }
            }
        })
    }
}


//MARK: - Textfield delagates

extension VerifyPinVC : UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let text = textField.text else { return false }
        
        //Set max Limit for textfields
        let newLength = text.count + string.count - range.length
        
        //  OTP textfield
        if textField == pinField {
            
            // limiting OTP lenght to 4 digits
            let allowedCharacters = CharacterSet(charactersIn: Constants.allowedNumbers)
            let characterSet = CharacterSet(charactersIn: string)
            
            if newLength <= 4 && allowedCharacters.isSuperset(of: characterSet) {
                
                if newLength >= 1 && newLength <= 4 {
                    setResendButtonSelection(buttonView: resentButtonView, isEnabled: false)
                } else {
                    setResendButtonSelection(buttonView: resentButtonView, isEnabled: true)
                }
                
//                // Next button enable disable
//                if newLength == 4 {
//                    setNextButtonSelection(buttonView: nextButtonView, isEnabled: true)
//                } else {
//                    setNextButtonSelection(buttonView: nextButtonView, isEnabled: false)
//                }
                // Returnimg number input
                return  true
            } else {
                return  false
            }
        } else  {
            return false
        }
    }
}
