//
//  UlduzumStoreListHeaderTableViewCell.swift
//  Bakcell
//
//  Created by Muhammad Waqas on 1/7/19.
//  Copyright © 2019 evampsaanga. All rights reserved.
//

import UIKit

class UlduzumStoreListHeaderTableViewCell: UITableViewCell {

    //MARK:- IBOutlet
    @IBOutlet var lblHeader : UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
