//
//  MyProfileVC.swift
//  Bakcell
//
//  Created by Saad Riaz on 6/20/17.
//  Copyright © 2017 evampsaanga. All rights reserved.
//

import UIKit
import ObjectMapper
import AVFoundation
import Photos

class MyProfileVC: BaseVC,UINavigationControllerDelegate,UIImagePickerControllerDelegate {

    public enum ResourceAccessType {
        case camera
        case gallery
    }

    //MARK: - Variable
    var imagePicker: UIImagePickerController =  UIImagePickerController()
    var titles: [String] = [Localized("Info_LoyalityIndicator"), Localized("Info_BillingLanguage"),Localized("Info_SIM"),Localized("Info_PIN"),Localized("Info_PUK"),Localized("Info_ActivationDate"),Localized("Info_Email")]
    var images: [String] = ["Loyalty","Billing","sim","PIN","PUK","Activation-code","Email"]
    var descriptionArr : [String] = []
    var updateImageArr: [String] = [Localized("Info_Camera"),Localized("Info_Gallery"),Localized("Info_RemovePhoto")]

    //MARK: - IBOutlet
    @IBOutlet var title_lbl: UILabel!
    @IBOutlet var profile_img: UIImageView!
    @IBOutlet var setting_btn: UIButton!
    @IBOutlet var userNumber_lbl: UILabel!
    @IBOutlet var userName_lbl: UILabel!
    @IBOutlet var tableView: UITableView!
    @IBOutlet var bgView: UIView!
    @IBOutlet var imageUpdateTableView: UITableView!


    //MARK: - View Controller Methods
    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(UINib(nibName: "ProfileCell", bundle: nil), forCellReuseIdentifier: "cellID")

        imageUpdateTableView.delegate = self
        imageUpdateTableView.dataSource = self
        imageUpdateTableView.register(UINib(nibName: "ProfileCell", bundle: nil), forCellReuseIdentifier: "cellID")
        imageUpdateTableView.layer.cornerRadius = 8
        bgView.isHidden = true
        imageUpdateTableView.isHidden = true


        profile_img.contentMode = .scaleAspectFill
        profile_img.clipsToBounds = true
        profile_img.layer.cornerRadius = profile_img.frame.height/2
        setting_btn.layer.cornerRadius = setting_btn.frame.height/2
        //        setting_btn.backgroundColor = UIColor.MBLightProgressTrackColor

        userName_lbl.text = MBUserSession.shared.userName()
        userNumber_lbl.text = MBUserSession.shared.msisdn

        // Load profile image of user

        MBAPIClient.sharedClient.request(MBUserSession.shared.userInfo?.imageURL ?? "").responseData { response in

            if let imageDate = response.result.value {
                print("image downloaded: \(imageDate)")

                let image = UIImage(data: imageDate)

                if image != nil {
                    self.profile_img.image = image
                }
            }
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.interactivePop(true)
        
        // Title localization
        title_lbl.text = Localized("Title_Profile")
        reloadTableViewData()
    }

    //MARK: IBACTIONS
    @IBAction func imageUpdatePressed(_ sender: UIButton) {

        if imageUpdateTableView.isHidden == true {
            imageUpdateTableView.isHidden = false
            bgView.isHidden = false
        } else {
            imageUpdateTableView.isHidden = true
            bgView.isHidden = true
        }
    }


    //MARK: - Functions
    func reloadTableViewData() {
        self.descriptionArr.removeAll()
        self.descriptionArr.append(MBUserSession.shared.userInfo?.loyaltySegment ?? "")
        self.descriptionArr.append(MBUserSession.shared.userSelectedBillingLanguageLanguage())
        self.descriptionArr.append(MBUserSession.shared.userInfo?.sim ?? "")
        self.descriptionArr.append(MBUserSession.shared.userInfo?.pin ?? "")
        self.descriptionArr.append(MBUserSession.shared.userInfo?.puk ?? "")
        self.descriptionArr.append(MBUtilities.displayFormatedDateString(dateString: MBUserSession.shared.userInfo?.effectiveDate ?? ""))
        self.descriptionArr.append(MBUserSession.shared.userInfo?.email ?? "")

        self.tableView.reloadData()
    }

    func loadImagePickerControllerFor(type: ResourceAccessType) {

        if type == .camera {

            // Get current access status for camera
            let cameraAuthorizationStatus = AVCaptureDevice.authorizationStatus(for: AVMediaType.video)

            switch cameraAuthorizationStatus {

            case .authorized:
                DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(1)) {
                    self.loadImagePickerControllerForCamera()
                }
                break

            case .notDetermined:
                // Prompting user for the permission to use the camera.
                AVCaptureDevice.requestAccess(for: AVMediaType.video) { granted in
                    if granted {

                        // User enable camera access to app
                        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(1)) {
                            self.loadImagePickerControllerForCamera()
                        }
                    } else {
                        // Acces denied
                        self.showAccessAlert(message: Localized("Message_EnableCameraAccess"))
                    }
                }
                break

            case .denied, .restricted:
                // Access denied
                self.showAccessAlert(message: Localized("Message_EnableCameraAccess"))
                break

            default:
                break
            }

        } else {

            // Get the current authorization state of Photos
            let status = PHPhotoLibrary.authorizationStatus()

            if (status == PHAuthorizationStatus.authorized) {
                // Access has been granted.
                DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(1)) {
                    self.loadImagePickerControllerForGallery()
                }

            } else if (status == PHAuthorizationStatus.notDetermined) {

                // Access has not been determined.
                PHPhotoLibrary.requestAuthorization({ (newStatus) in

                    // Get Access
                    if (newStatus == PHAuthorizationStatus.authorized) {

                        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(1)) {
                            self.loadImagePickerControllerForGallery()
                        }

                    } else {
                        // Access denied and show user access message
                        self.showAccessAlert(message: Localized("Message_EnablePhotosAccess"))
                    }
                })

            } else if (status == PHAuthorizationStatus.denied) || (status == PHAuthorizationStatus.restricted) {
                // Access has been denied.
                self.showAccessAlert(message: Localized("Message_EnablePhotosAccess"))

            } else {
                self.showErrorAlertWithMessage(message: Localized("Message_UnexpectedError"))
            }
        }
    }

    func loadImagePickerControllerForGallery() {

        if UIImagePickerController.isSourceTypeAvailable(.savedPhotosAlbum) {

            imagePicker.delegate = self
            imagePicker.sourceType = .savedPhotosAlbum;
            imagePicker.allowsEditing = true

            self.present(imagePicker, animated: true, completion: nil)
        } else {
             self.showErrorAlertWithMessage(message: Localized("Message_UnexpectedError"))
        }

    }
    func loadImagePickerControllerForCamera() {

        if UIImagePickerController.isSourceTypeAvailable(.camera) {

            imagePicker.delegate = self
            imagePicker.sourceType = .camera
            imagePicker.allowsEditing = true
            present(imagePicker, animated: true, completion: nil)

        } else {
            DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(1)) {
                self.showErrorAlertWithMessage(message: Localized("Message_UnexpectedError"))
            }
        }

    }


    //MARK: - API Calls
    /// Call 'billingLanguageChangeApiCall' API.
    ///
    /// - returns: Void
    func billingLanguageChange(language lang : String) {
        
        activityIndicator.showActivityIndicator()
        
        _ = MBAPIClient.sharedClient.changeBillingLanguage(language: lang, {(response, resultData, error, isCancelled, status, resultCode, resultDesc) in
            
            self.activityIndicator.hideActivityIndicator()
            if error != nil {
                //EventLog
                MBAnalyticsManager.logEvent(screenName: "Profile Screen", contentType:"Billing Language Changed To Success" , status:"Failure")
                error?.showServerErrorInViewController(self)
                
            } else {
                // handling data from API response.
                if resultCode == Constants.MBAPIStatusCode.succes.rawValue {
                    
                    //EventLog
                    MBAnalyticsManager.logEvent(screenName: "Profile Screen", contentType:"Billing Language Changed To Success" , status:"Success")
                    
                    // Parssing response data
                    if let changeLangResponse = Mapper<ChangeBillingLanguage>().map(JSONObject:resultData) {

                        self.showSuccessAlertWithMessage(message: changeLangResponse.message)

                        MBUserSession.shared.loggedInUsers?.currentUser?.userInfo?.billingLanguage = lang
                        MBUserSession.shared.saveLoggedInUsersCurrentStateInfo()
                        

                        self.reloadTableViewData()
                        
                    }
                } else {
                    //EventLog
                    MBAnalyticsManager.logEvent(screenName: "Profile Screen", contentType:"Billing Language Changed To Success" , status:"Failure")
                    // Show error alert to user
                    self.showErrorAlertWithMessage(message: resultDesc)
                }
            }
        })
    }

    //MARK: - Done image capture here
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {

        imagePicker.dismiss(animated: true, completion: nil)

        if let image = info[UIImagePickerController.InfoKey.editedImage] as? UIImage {

            self.activityIndicator.showActivityIndicator()

            _ = MBAPIClient.sharedClient.uploadImage(image: image, { (response, resultData, error, isCancelled, status, resultCode, resultDesc) in

                self.activityIndicator.hideActivityIndicator()
                if error != nil {
                    //EventLog
                    MBAnalyticsManager.logEvent(screenName: "Profile Screen", contentType:"Add Image Failure" , status:"Failure" )
                    
                    error?.showServerErrorInViewController(self)

                } else {
                    // handling data from API response.
                    if resultCode == Constants.MBAPIStatusCode.succes.rawValue {
                    //EventLog
                    MBAnalyticsManager.logEvent(screenName: "Profile Screen", contentType:"Add Image Success" , status:"Success" )
                    

                        // Parssing response data
                        if let uploadImageResponse = Mapper<UploadImage>().map(JSONObject: resultData) {

                            self.showSuccessAlertWithMessage(message: resultDesc)

                            
                            MBUserSession.shared.loggedInUsers?.currentUser?.userInfo?.imageURL = uploadImageResponse.imageURL
                            MBUserSession.shared.saveLoggedInUsersCurrentStateInfo()

                            // Load profile image of user
                            MBAPIClient.sharedClient.request(MBUserSession.shared.userInfo?.imageURL ?? "").responseData { response in

                                if let imageDate = response.result.value {
                                    print("image downloaded: \(imageDate)")

                                    let image = UIImage(data: imageDate)

                                    if image != nil {
                                        self.profile_img.image = image
                                    }
                                }
                            }
                        } else {
                            self.showAlertWithMessage(message: resultDesc)
                        }
                    } else {
                        
                        //EventLog
                        MBAnalyticsManager.logEvent(screenName: "Profile Screen", contentType:"Add Image Failure" , status:"Failure" )
                        
                        // Show error alert to user
                        self.showErrorAlertWithMessage(message: resultDesc)
                    }
                }
            })

            if picker.sourceType == .camera {

                UIImageWriteToSavedPhotosAlbum(image, self, #selector(image(_:didFinishSavingWithError:contextInfo:)), nil)
            }

        }
    }

    //MARK: - Add image to Library
    @objc func image(_ image: UIImage, didFinishSavingWithError error: Error?, contextInfo: UnsafeRawPointer) {

        //self.showErrorAlertWithMessage(message: error?.localizedDescription)
    }

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        let touch = touches.first!
        if touch.view == bgView {
            bgView.isHidden = true
            imageUpdateTableView.isHidden = true
        }
        
    }

    // Remove profile image
    func removeProfileImage() {
        self.activityIndicator.showActivityIndicator()

        _ = MBAPIClient.sharedClient.uploadImage(image: nil ,isUploadImage: false, { (response, resultData, error, isCancelled, status, resultCode, resultDesc) in

            self.activityIndicator.hideActivityIndicator()
            if error != nil {
                //EventLog
                MBAnalyticsManager.logEvent(screenName: "Profile Screen", contentType:"Image Remove Success" , status:"Failure" )
                error?.showServerErrorInViewController(self)

            } else {
                // handling data from API response.
                if resultCode == Constants.MBAPIStatusCode.succes.rawValue {
                //EventLog
                MBAnalyticsManager.logEvent(screenName: "Profile Screen", contentType:"Image Remove Success" , status:"Success" )
                
                    self.profile_img.image = UIImage(named: "profile")
                    
                    MBUserSession.shared.loggedInUsers?.currentUser?.userInfo?.imageURL = ""
                    MBUserSession.shared.saveLoggedInUsersCurrentStateInfo()
                    
                    self.showAlertWithMessage(message: resultDesc ?? "")
                } else {
                    //EventLog
                    MBAnalyticsManager.logEvent(screenName: "Profile Screen", contentType:"Image Remove Success" , status:"Failure" )
                    // Show error alert to user
                    self.showErrorAlertWithMessage(message: resultDesc)
                }
            }
        })

    }
}

//MARK: - TABLE VIEW DELEGATE METHODS
extension MyProfileVC : UITableViewDelegate,UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        if tableView == imageUpdateTableView {
            return updateImageArr.count
        } else {
            return descriptionArr.count
        }
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let cell = tableView.dequeueReusableCell(withIdentifier: "cellID", for: indexPath) as! ProfileCell

        if tableView == self.tableView {

            cell.cell2_lbl.isHidden = true
            cell.arrow_img.isHidden = true
            //cell.imageView1.isHidden = false
            cell.title_lbl.isHidden = false
            cell.description_lbl.isHidden = false
            cell.LineView.isHidden = false


            //Aligning the arrow with the text
            if indexPath.row == 1 || indexPath.row == 6 {
                cell.arrow_img.isHidden = false
                cell.arrowWidthConstraint.constant = 22

            } else {
                cell.arrowWidthConstraint.constant = 0
            }
            //cell.imageView1.isHidden = true
            //cell.imageView1.image = UIImage (named: images[indexPath.row])
            cell.title_lbl.text = titles[indexPath.row]
            cell.description_lbl.text = descriptionArr[indexPath.row]

//            if titles[indexPath.row].isEqualToStringIgnoreCase(otherString: Localized("Info_SIM")) {
//                cell.description_lbl.startMarqueeAnimation()
//            } else {
//                cell.description_lbl.stopMarqueeAnimation()
//            }

        } else if tableView == imageUpdateTableView {


            //cell.imageView1.isHidden = true
            cell.title_lbl.isHidden = true
            cell.description_lbl.isHidden = true
            cell.arrow_img.isHidden = true
            cell.LineView.isHidden = true
            cell.cell2_lbl.isHidden = false

            cell.cell2_lbl.text = updateImageArr[indexPath.row]

            if indexPath.row == updateImageArr.count-1{
                cell.cell2_lbl.textColor = UIColor.MBRedColor
            } else {
                cell.cell2_lbl.textColor = UIColor.MBDarkGrayColor
            }
        }
        cell.selectionStyle = .none
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        if tableView == imageUpdateTableView {

            if indexPath.row == 0 {

               self.loadImagePickerControllerFor(type: .camera)

            } else if indexPath.row == 1 {

                self.loadImagePickerControllerFor(type: .gallery)

            } else if indexPath.row == 2 {

                self.removeProfileImage()
            }

            // Hide option TableView
            bgView.isHidden = true
            imageUpdateTableView.isHidden = true
        
        } else if tableView == tableView{

            if indexPath.row == 1 {
                let changelang = self.myStoryBoard.instantiateViewController(withIdentifier: "SelectionAlertVC") as! SelectionAlertVC
                changelang.isBillingLanguage = true
                changelang.setOKButtonCompletionHandler { (aString) in
                   
                    self.billingLanguageChange(language: aString)

                    // Reloading layout Text accourding to seletcted language
                }
                
                self.presentPOPUP(changelang, animated: true, completion:  nil)

            } else if indexPath.row == descriptionArr.count - 1 {

                let changeEmailVC = self.myStoryBoard.instantiateViewController(withIdentifier: "ChangeEmailVC") as! ChangeEmailVC
                self.navigationController?.pushViewController(changeEmailVC, animated: true)
            }
        }
        
        
    }
}
