//
//  RequestHistoryExpandCell.swift
//  Bakcell
//
//  Created by Saad Riaz on 6/10/17.
//  Copyright © 2017 evampsaanga. All rights reserved.
//

import UIKit

class RequestHistoryExpandCell: UITableViewCell {

    @IBOutlet var amountValue_lbl: UILabel!
    @IBOutlet var amount_lbl: UILabel!
    @IBOutlet var label1: UILabel!
    @IBOutlet var label2: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
