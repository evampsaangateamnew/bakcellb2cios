//
//  TitleSubTitleValueAndDescCell.swift
//  Bakcell
//
//  Created by AbdulRehman Warraich on 8/30/17.
//  Copyright © 2017 evampsaanga. All rights reserved.
//

import UIKit

class TitleSubTitleValueAndDescCell: UITableViewCell {

    static let identifier : String = "TitleSubTitleValueAndDescCell"

    @IBOutlet var mainTitle_lbl: UILabel!
    @IBOutlet var attributedView: UIView!
    @IBOutlet var attributedViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet var mainTitleLabelHeightConstraint: NSLayoutConstraint!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func setTitleSubTitleValueAndDescLayoutValues (titleSubTitleValueAndDesc : TitleSubTitleValueAndDesc?) {


        if let titleSubTitleValueAndDesc = titleSubTitleValueAndDesc {
            attributedViewHeightConstraint.constant = 35

            mainTitle_lbl.text = titleSubTitleValueAndDesc.title ?? ""
            if mainTitle_lbl.text == "" {
                mainTitleLabelHeightConstraint.constant = 0
            } else {
                mainTitleLabelHeightConstraint.constant = 15
            }

            attributedView.subviews.forEach({ (aView) in

                if aView is SubTitleValueAndDescView {
                    aView.removeFromSuperview()
                }
            })

            var lastView : UIView?
            var subViewHeights : CGFloat = 0.0

            titleSubTitleValueAndDesc.attributeList?.forEach({ (aAttribute) in

                let aAttributesView : SubTitleValueAndDescView = SubTitleValueAndDescView.instanceFromNib() as! SubTitleValueAndDescView
                aAttributesView.translatesAutoresizingMaskIntoConstraints = false

                let aSubViewHeight : CGFloat = aAttributesView.setSubTitleValueAndDescription(aAttribute: aAttribute)
                subViewHeights = subViewHeights + aSubViewHeight

                attributedView.addSubview(aAttributesView)

                let leading = NSLayoutConstraint(item: aAttributesView, attribute: NSLayoutConstraint.Attribute.leading, relatedBy: NSLayoutConstraint.Relation.equal, toItem: attributedView, attribute: NSLayoutConstraint.Attribute.leading, multiplier: 1, constant: 0)
                let trailing = NSLayoutConstraint(item: aAttributesView, attribute: NSLayoutConstraint.Attribute.trailing, relatedBy: NSLayoutConstraint.Relation.equal, toItem: attributedView, attribute: NSLayoutConstraint.Attribute.trailing, multiplier: 1, constant: 0)

                var top : NSLayoutConstraint = NSLayoutConstraint()

                if lastView == nil{

                    top = NSLayoutConstraint(item: aAttributesView, attribute: NSLayoutConstraint.Attribute.top, relatedBy: NSLayoutConstraint.Relation.equal, toItem: attributedView, attribute: NSLayoutConstraint.Attribute.top, multiplier: 1, constant: 0)

                } else {
                    top = NSLayoutConstraint(item: aAttributesView, attribute: NSLayoutConstraint.Attribute.top, relatedBy: NSLayoutConstraint.Relation.equal, toItem: lastView, attribute: NSLayoutConstraint.Attribute.bottom, multiplier: 1, constant: 0)
                }

                let height = NSLayoutConstraint(item: aAttributesView, attribute: NSLayoutConstraint.Attribute.height, relatedBy: NSLayoutConstraint.Relation.equal, toItem: nil, attribute: NSLayoutConstraint.Attribute.notAnAttribute, multiplier: 1, constant: aSubViewHeight)

                attributedView.addConstraints([leading, trailing, top, height])

                lastView = aAttributesView
                
            })
            
            if let _ = titleSubTitleValueAndDesc.attributeList?.count{
                attributedViewHeightConstraint.constant = (subViewHeights)
            } else {
                attributedViewHeightConstraint.constant = 0.0
            }

        } else {
            mainTitleLabelHeightConstraint.constant = 0
            attributedViewHeightConstraint.constant = 0
        }
    }

    
}
