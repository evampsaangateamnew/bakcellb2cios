//
//  UsageHistoryVC.swift
//  Bakcell
//
//  Created by Saad Riaz on 5/28/17.
//  Copyright © 2017 evampsaanga. All rights reserved.
//

import UIKit
import ObjectMapper


class UsageHistoryVC: BaseVC {
    
    //MARK:- Properties
    var usageDetailsHistoryCompleteResponse : UsageDetailsHistoryHandler = UsageDetailsHistoryHandler()
    var usageDetailsHistoryFilteredData : UsageDetailsHistoryHandler = UsageDetailsHistoryHandler()
    var isStartDate : Bool!
    var selectedOption: String = "All"
    
    //Uidate picker
    let datePicker = UIDatePicker()
    let myView = UIView()
    
    //MARK:- IBOutlet
    
    @IBOutlet var pageCount: UILabel!
    
    @IBOutlet var catDropDown: UIDropDown!
    @IBOutlet var dateDropDown: UIDropDown!
    @IBOutlet var tableView: MBAccordionTableView!
    
    @IBOutlet var spacificBtnView: UIView!
    @IBOutlet var endDateBtn: UIButton!
    @IBOutlet var startDateBtn: UIButton!
    @IBOutlet var startDateLb: UILabel!
    @IBOutlet var endDateLb: UILabel!
    @IBOutlet var date_lbl: UILabel!
    @IBOutlet var service_lbl: UILabel!
    @IBOutlet var usage_lbl: UILabel!
    @IBOutlet var charge_lbl: UILabel!
    
    //MARK: - ViewController Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.allowsMultipleSelection = true
        //Registering Cells for tableView
        tableView.register(UINib(nibName: "UsageHistoryExpandCell", bundle: nil), forCellReuseIdentifier: "UsageHistoryExpandCell")
        tableView.register(UINib(nibName: "UsageHistoryHeaderView", bundle: nil), forHeaderFooterViewReuseIdentifier: UsageHistoryHeaderView.UsageHistoryIdentifier)
        
        spacificBtnView.isHidden = true
        //Drop Down
        catDropDown.optionsTextAlignment = NSTextAlignment.center
        catDropDown.textAlignment = NSTextAlignment.center
        catDropDown.placeholder = Localized("PlaceHolder_SelectCategory")
        self.catDropDown.rowBackgroundColor = UIColor.clear
        self.catDropDown.borderWidth = 0
        self.catDropDown.tableHeight = 176
        catDropDown.optionsSize = 14
        catDropDown.fontSize = 12
        catDropDown.options = [Localized("DropDown_All"),Localized("DropDown_Internet"), Localized("DropDown_Voice"), Localized("DropDown_SMS"),Localized("DropDown_Others")]
        
        catDropDown.didSelect { (option, index) in
            
            print("You just select: \(option) at index: \(index)")
            
            switch option {
            case Localized("DropDown_All"):
                self.selectedOption = "All"
                
            case Localized("DropDown_Internet"):
                self.selectedOption = "data"
                
            case Localized("DropDown_Voice"):
                self.selectedOption = "voice"
                
            case Localized("DropDown_SMS"):
                self.selectedOption = "sms"
                
            case Localized("DropDown_Others"):
                self.selectedOption = "others"
                
            default:
                break
            }
            
            let records : [DetailsHistory]? = self.filterUsageDetailBy(type: self.selectedOption, usageDetails: self.usageDetailsHistoryCompleteResponse.records)
            
            self.usageDetailsHistoryFilteredData.records = records
            
            
            self.reloadTableViewData()
        }
        
        //Drop Down
        dateDropDown.optionsTextAlignment = NSTextAlignment.center
        dateDropDown.textAlignment = NSTextAlignment.center
        dateDropDown.placeholder = Localized("PlaceHolder_SelectPeriod")
        self.dateDropDown.rowBackgroundColor = UIColor.clear
        self.dateDropDown.borderWidth = 0
        self.dateDropDown.tableHeight = 176
        dateDropDown.optionsSize = 14
        dateDropDown.fontSize = 12
        
        dateDropDown.options = [Localized("DropDown_CurrentDay"),Localized("DropDown_Last7Days"), Localized("DropDown_Last30Days"), Localized("DropDown_PreviousMonth"),Localized("DropDown_CustomPeriod")]
        dateDropDown.didSelect { (option, index) in
            // self.dropDownLbl.text = " \(option)"
            print("You just select: \(option) at index: \(index)")
            
            let todayDateString = MBUtilities.todayDateString(dateFormate: Constants.kAPIFormat)
            MBUserSession.shared.dateFormatter.dateFormat = Constants.kAPIFormat
            
            switch index{
            case 0:
                print("default")
                self.spacificBtnView.isHidden = true
                
                self.getUsageDetailsHistory(startDate: todayDateString, endDate: todayDateString)
                
            case 1:
                print("Last 7 days")
                self.spacificBtnView.isHidden = true
                
                // Get data of previous seven days
                let sevenDaysAgo = MBUtilities.todayDateString(withAdditionalValue: -7)
                self.getUsageDetailsHistory(startDate: sevenDaysAgo, endDate: todayDateString)
                
                
            case 2:
                print("Last 30 days")
                self.spacificBtnView.isHidden = true
                
                // Get data of previous thirty days
                let thirtyDaysAgo = MBUtilities.todayDateString(withAdditionalValue: -30)
                self.getUsageDetailsHistory(startDate: thirtyDaysAgo, endDate: todayDateString)
                
            case 3:
                print("Previous month")
                self.spacificBtnView.isHidden = true
                
                // Getting previous month start and end date
                let startOfPreviousMonth = MBUtilities.todayDate().getPreviousMonth()?.startOfMonth() ?? MBUtilities.todayDate()
                let endOfPreviousMonth = MBUtilities.todayDate().getPreviousMonth()?.endOfMonth() ?? MBUtilities.todayDate()
                
                MBUserSession.shared.dateFormatter.dateFormat = Constants.kAPIFormat
                
                let startDateString = MBUserSession.shared.dateFormatter.string(from: startOfPreviousMonth)
                let endDateString = MBUserSession.shared.dateFormatter.string(from: endOfPreviousMonth)
                
                self.getUsageDetailsHistory(startDate: startDateString, endDate: endDateString)
                
            case 4:
                self.spacificBtnView.isHidden = false
                
                // For specific period of time
                
                self.startDateLb.text = MBUtilities.todayDateString(dateFormate: Constants.kDisplayFormat)
                self.endDateLb.text = MBUtilities.todayDateString(dateFormate: Constants.kDisplayFormat)
                
                self.startDateLb.textColor = UIColor.MBDarkGrayColor
                self.endDateLb.textColor = UIColor.MBDarkGrayColor
                
                self.getUsageDetailsHistory(startDate: todayDateString, endDate: todayDateString)
                
                
            default:
                print("destructive")
                
            }
        }
        
        loadViewController()
        
        // Load Initial data
        let todayDateString = MBUtilities.todayDateString(dateFormate: Constants.kAPIFormat)
        self.getUsageDetailsHistory(startDate: todayDateString, endDate: todayDateString) { (canShowDisclaimer) in
            
            if canShowDisclaimer == true && DisclaimerMessageVC.canShowDisclaimerOfType(.UsageHistory) == true {
                if let disclaimerMessageVC = self.myStoryBoard.instantiateViewController(withIdentifier: "DisclaimerMessageVC") as? DisclaimerMessageVC {
                    
                    disclaimerMessageVC.setDisclaimerAlertWith(description: Localized("Disclaimer_Description"), okBtnClickedBlock: { (dontShowAgain) in
                        
                        if dontShowAgain == true {
                            DisclaimerMessageVC.setDisclaimerStatusForType(.UsageHistory, canShow: false)
                        }
                    })
                    
                    if self.navigationController?.parent != nil {
                        self.navigationController?.parent?.presentPOPUP(disclaimerMessageVC, animated: true, modalTransitionStyle: .crossDissolve, completion: nil)
                    } else {
                        self.presentPOPUP(disclaimerMessageVC, animated: true, modalTransitionStyle: .crossDissolve, completion: nil)
                    }
                }
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.interactivePop(true)
    }
    
    // MARK: - IBActions
    
    @IBAction func startBtnAction(_ sender: UIButton) {
        
        isStartDate = true;
        let todayDate = MBUtilities.todayDate()
        MBUserSession.shared.dateFormatter.dateFormat = Constants.kDisplayFormat
        
        let threeMonthsAgos = MBUtilities.todayDate().getMonth(WithAdditionalValue: -3)?.startOfMonth() ?? todayDate
        
        if let endDateString = endDateLb.text,
            endDateString.isBlank == false {
            
            self.showDatePicker(minDate: threeMonthsAgos, maxDate: MBUserSession.shared.dateFormatter.date(from: endDateString) ?? todayDate)
            
        } else {
            
            self.showDatePicker(minDate: threeMonthsAgos, maxDate: todayDate)
        }
        
    }
    
    @IBAction func endBtnAction(_ sender: UIButton) {
        
        isStartDate = false;
        let todayDate = MBUtilities.todayDate(dateFormate: Constants.kDisplayFormat)
        
        MBUserSession.shared.dateFormatter.dateFormat = Constants.kDisplayFormat
        
        self.showDatePicker(minDate: MBUserSession.shared.dateFormatter.date(from: startDateLb.text ?? "") ?? todayDate, maxDate: todayDate)
    }
    
    //MARK: - FUNCTIONS
    func loadViewController(){
        
        date_lbl.font = UIFont.MBArial(fontSize: 14)
        service_lbl.font = UIFont.MBArial(fontSize: 14)
        usage_lbl.font = UIFont.MBArial(fontSize: 14)
        charge_lbl.font = UIFont.MBArial(fontSize: 14)
        
        date_lbl.text = Localized("Title_Date")
        service_lbl.text = Localized("Title_Service")
        usage_lbl.text = Localized("Title_Usage")
        charge_lbl.text = Localized("Title_Charge")
        
    }
    
    func showDatePicker(minDate : Date , maxDate : Date){
        //Formate Date
        datePicker.datePickerMode = .date
        
        //ToolBar
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        
        //done button & cancel button
        let doneButton = UIBarButtonItem(title: Localized("Btn_title_Done"), style: UIBarButtonItem.Style.done, target: self, action: #selector(UsageHistoryVC.doneDatePicker))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: Localized("Btn_title_Cancel"), style: UIBarButtonItem.Style.plain, target: self, action: #selector(UsageHistoryVC.cancelDatePicker))
        toolbar.setItems([doneButton,spaceButton,cancelButton], animated: false)
        
        var  currentLanguageLocale = Locale(identifier:"en")
        switch MBLanguageManager.userSelectedLanguage() {
            
        case .Russian:
            currentLanguageLocale = Locale(identifier:"ru")
        case .English:
            currentLanguageLocale = Locale(identifier:"en")
        case .Azeri:
            currentLanguageLocale = Locale(identifier:"az_AZ")
        }
        
        self.datePicker.calendar.locale = currentLanguageLocale
        self.datePicker.locale = currentLanguageLocale
        
        
        datePicker.minimumDate = minDate
        datePicker.maximumDate = maxDate
        datePicker.timeZone =  TimeZone.current
        
        datePicker.backgroundColor = UIColor.lightGray
        
        if #available(iOS 13.4, *) {
            datePicker.preferredDatePickerStyle = .wheels
            toolbar.frame = CGRect.init(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 40)
        }
        
        datePicker.frame = CGRect(x: 0, y: 40, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height * 0.4)
        
        myView.frame = CGRect(x: 0, y:((UIScreen.main.bounds.height * 0.6) - 40), width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height * 0.4)
        
        if #available(iOS 13.4, *) {
            datePicker.alignmentRect(forFrame: myView.frame)
        }
        myView.backgroundColor = UIColor.white
        myView .addSubview(toolbar)
        myView .addSubview(datePicker)
        
        self.view.addSubview(myView)
        
    }
    
    @objc func doneDatePicker() {
        
        let todayDate = MBUtilities.todayDate(dateFormate: Constants.kDisplayFormat)
        
        //For date formate
        MBUserSession.shared.dateFormatter.dateFormat = Constants.kDisplayFormat
        MBUserSession.shared.dateFormatter.timeZone = TimeZone.current
        
        let selectedDate = MBUserSession.shared.dateFormatter.string(from: datePicker.date)
        
        let startFieldDate = MBUserSession.shared.dateFormatter.date(from: startDateLb.text ?? "")
        let endFieldDate = MBUserSession.shared.dateFormatter.date(from: endDateLb.text ?? "")
        
        //dismiss date picker dialog
        self.view.endEditing(true)
        myView .removeFromSuperview()
        
        MBUserSession.shared.dateFormatter.dateFormat = Constants.kAPIFormat
        
        if isStartDate ?? true {
            startDateLb.text = selectedDate
            self.startDateLb.textColor = UIColor.MBBarOrange
            
            self.getUsageDetailsHistory(startDate: MBUserSession.shared.dateFormatter.string(from: datePicker.date), endDate: MBUserSession.shared.dateFormatter.string(from:endFieldDate ?? todayDate))
            
        } else {
            
            endDateLb.text = selectedDate
            self.endDateLb.textColor = UIColor.MBBarOrange
            
            self.getUsageDetailsHistory(startDate: MBUserSession.shared.dateFormatter.string(from: startFieldDate ?? todayDate), endDate: MBUserSession.shared.dateFormatter.string(from: datePicker.date))
        }
    }
    
    @objc func cancelDatePicker(){
        //cancel button dismiss datepicker dialog
        self.view.endEditing(true)
        
        myView .removeFromSuperview()
        print("cancel")
    }
    
    func datePickerValueChanged(sender:UIDatePicker) {
        
        MBUserSession.shared.dateFormatter.dateStyle = DateFormatter.Style.medium
        MBUserSession.shared.dateFormatter.timeStyle = DateFormatter.Style.none
    }
    
    func reloadTableViewData() {
        
        if usageDetailsHistoryFilteredData.records?.count ?? 0 <= 0 {
            self.tableView.showDescriptionViewWithImage(description: Localized("Message_NoDataAvalible"))
        } else {
            self.tableView.hideDescriptionView()
        }
        self.tableView.reloadData()
        
        self.showPageNumber()
        
    }
    
    func filterUsageDetailBy(type:String?, usageDetails : [DetailsHistory]? ) -> [DetailsHistory]? {
        
        guard let myType = type else {
            return []
        }
        
        if myType.isEqualToStringIgnoreCase(otherString: "All") {
            return usageDetails
        }
        
        // Filtering offers array
        var filterdUsageDetails : [DetailsHistory] = []
        
        if let offers = usageDetails {
            
            for aDetailHistory in offers {
                
                if let historyDataType = aDetailHistory.type {
                    
                    if historyDataType.lowercased() == myType.lowercased() {
                        
                        filterdUsageDetails.append(aDetailHistory)
                    }
                }
            }
            
        } else {
            return []
        }
        
        return filterdUsageDetails
    }
    
    
    
    
    //MARK: - API Calls
    /// Call 'getUsageDetailsHistory' API .
    ///
    /// - returns: records
    func getUsageDetailsHistory(startDate:String?, endDate:String?, completionHandler: @escaping (Bool) -> Void = {_ in }) {
        
        activityIndicator.showActivityIndicator()
        
        _ = MBAPIClient.sharedClient.getUsageDetailsHistory(StartDate: startDate ?? "", EndDate: endDate ?? "", { (response, resultData, error, isCancelled, status, resultCode, resultDesc) in
            
            self.activityIndicator.hideActivityIndicator()
            if error != nil {
                error?.showServerErrorInViewController(self)
                completionHandler(false)
                
            } else {
                // handling data from API response.
                if resultCode == Constants.MBAPIStatusCode.succes.rawValue {
                    
                    // Parssing response data
                    if let usageHistoryList = Mapper<UsageDetailsHistoryHandler>().map(JSONObject: resultData){
                        
                        self.usageDetailsHistoryCompleteResponse = usageHistoryList.copy()
                        
                        self.usageDetailsHistoryFilteredData.records = self.filterUsageDetailBy(type: self.selectedOption, usageDetails: usageHistoryList.records)
                        
                        completionHandler(true)
                    }
                    
                } else {
                    // Show error alert to user
                    self.showErrorAlertWithMessage(message: resultDesc)
                }
            }
            
            self.reloadTableViewData()
        })
    }
}

// MARK: - <UITableViewDataSource> / <UITableViewDelegate>

extension UsageHistoryVC : UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return (usageDetailsHistoryFilteredData.records?.count) ?? 0
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 180
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 52.0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "UsageHistoryExpandCell", for: indexPath) as! UsageHistoryExpandCell
        cell.peak_lbl.font = UIFont.MBArial(fontSize: 14)
        cell.zone_lbl.font = UIFont.MBArial(fontSize: 14)
        cell.destination_lbl.font = UIFont.MBArial(fontSize: 14)
        cell.type_lbl.font = UIFont.MBArial(fontSize: 14)
        cell.period_lbl.font = UIFont.MBArial(fontSize: 14)
        cell.destDetail_lbl.font = UIFont.MBArial(fontSize: 14)
        cell.to_lbl.font = UIFont.MBArial(fontSize: 14)
        cell.number_lbl.font = UIFont.MBArial(fontSize: 14)
        
        cell.to_lbl.text = Localized("Title_Number")
        cell.peak_lbl.text = Localized("Expand_PeakOffPeak")
        cell.zone_lbl.text = Localized("Expand_Zone")
        cell.destination_lbl.text = Localized("Expand_Destination")
        
        cell.number_lbl.text = usageDetailsHistoryFilteredData.records?[indexPath.section].number
        cell.type_lbl.text = usageDetailsHistoryFilteredData.records?[indexPath.section].period
        cell.period_lbl.text = usageDetailsHistoryFilteredData.records?[indexPath.section].zone
        cell.destDetail_lbl.text = usageDetailsHistoryFilteredData.records?[indexPath.section].destination
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: "UsageHistory") as! UsageHistoryHeaderView
        
        headerView.dateTimeLb.font = UIFont.MBArial(fontSize: 14)
        headerView.statusLb.font = UIFont.MBArial(fontSize: 14)
        headerView.paidLb.font = UIFont.MBArial(fontSize: 14)
        headerView.remainingLb.font = UIFont.MBArial(fontSize: 14)
        
        if let aRecord = usageDetailsHistoryFilteredData.records?[section] {
            
            headerView.dateTimeLb.text = aRecord.startDateTime
            headerView.statusLb.text = aRecord.service
            headerView.paidLb.text = aRecord.usage
            headerView.remainingLb.text = aRecord.chargedAmount
            
            if aRecord.chargedAmount.isBlank == false && aRecord.chargedAmount.isStringAnNumber() == true {
                
                headerView.manatSign.isHidden = false
                headerView.manatSignWidthConstraint.constant = 8
            } else {
                headerView.manatSign.isHidden = true
                headerView.manatSignWidthConstraint.constant = 0
            }
        } else {
            
            headerView.dateTimeLb.text = ""
            headerView.statusLb.text = ""
            headerView.paidLb.text = ""
            headerView.remainingLb.text = ""
            
            headerView.manatSign.isHidden = true
        }
        
        
        return headerView
    }
}

// MARK: - <MBAccordionTableViewDelegate>

extension UsageHistoryVC : MBAccordionTableViewDelegate {
    
    func tableView(_ tableView: MBAccordionTableView, didOpenSection section: Int, withHeader header: UITableViewHeaderFooterView?) {
        
        showPageNumber()
    }
    
    func tableView(_ tableView: MBAccordionTableView, didCloseSection section: Int, withHeader header: UITableViewHeaderFooterView?) {
        
        showPageNumber()
    }
    
    func tableView(_ tableView: MBAccordionTableView, canInteractWithHeaderAtSection section: Int) -> Bool {
        return true
    }
}

extension UsageHistoryVC:UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        showPageNumber()
    }
    
    func showPageNumber() {
        
        pageCount.text = "\(tableView.lastVisibleSection())/\(usageDetailsHistoryFilteredData.records?.count ?? 0)"
    }
}
