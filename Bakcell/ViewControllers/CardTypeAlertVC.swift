//
//  CardTypeAlertVC.swift
//  Bakcell
//
//  Created by Muhammad Irfan Awan on 06/05/2021.
//  Copyright © 2021 evampsaanga. All rights reserved.
//

import UIKit

class CardTypeAlertVC: UIViewController {
    
    fileprivate var okBtncompletionHandlerBlock : MBButtonCompletionHandler?

    @IBOutlet var contentView: UIView!
    @IBOutlet var titleLbl:UILabel!
    @IBOutlet var tableView: UITableView!
    @IBOutlet var ok_btn: UIButton!
    
    var selectedSaveCardIndex = 0
    
    
    var cardTypes = [PlasticCard]()
    override func viewDidLoad() {
        super.viewDidLoad()
        cardTypes = MBUserSession.shared.predefineData?.plasticCards ?? [PlasticCard]()
        tableView.register(UINib(nibName: CardTypesCell.identifier, bundle: nil), forCellReuseIdentifier: CardTypesCell.identifier)
        ok_btn.setTitle(Localized("BtnTitle_OK"), for: .normal)
        titleLbl.text = Localized("Lbl_Cardtype")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tableView.reloadData()
    }
    
    //MARK:- Functions
    func setAlertWith(okBtnClickedBlock : @escaping MBButtonCompletionHandler = { _ in }) {
        
        okBtncompletionHandlerBlock = okBtnClickedBlock
        
    }
    

    // MARK: - IBActions
    
    @IBAction func okayBtnPressed(_ sender: UIButton) {
        self.dismiss(animated: true, completion:nil)
        self.okBtncompletionHandlerBlock?(String(cardTypes[selectedSaveCardIndex].cardKey ?? 1))
    }
    
 

}

extension CardTypeAlertVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cardTypes.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: CardTypesCell.identifier, for: indexPath) as! CardTypesCell
        cell.lblTitle.text = cardTypes[indexPath.row].cardVale
        if indexPath.row == self.selectedSaveCardIndex{
            cell.radioBtn.setImage(#imageLiteral(resourceName: "radio active"), for: .normal)
        }else{
            cell.radioBtn.setImage(#imageLiteral(resourceName: "Checkbox-state2"), for: .normal)
        }
        cell.radioBtn.tag = indexPath.row
        cell.radioBtn.addTarget(self, action: #selector(selectTapedCard(_sender:)), for: .touchUpInside)
        return cell
        
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedSaveCardIndex = indexPath.row
        self.tableView.reloadData()
    }
    
    @objc func selectTapedCard(_sender: UIButton) {
        selectedSaveCardIndex = _sender.tag
        self.tableView.reloadData()
    }
    
    
}
