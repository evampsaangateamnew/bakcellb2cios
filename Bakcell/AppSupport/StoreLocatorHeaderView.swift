//
//  StoreLocatorHeaderView.swift
//  Bakcell
//
//  Created by Saad Riaz on 6/8/17.
//  Copyright © 2017 evampsaanga. All rights reserved.
//

import UIKit


class StoreLocatorHeaderView: MBAccordionTableViewHeaderView {

    static let kAccordionHeaderViewReuseIdentifier = "AccordionHeaderViewReuseIdentifier"

    @IBOutlet var location_btn: UIButton!
    @IBOutlet var storeAddress_lbl: UILabel!
    @IBOutlet var storeName_lbl: UILabel!
    @IBOutlet var expandIcon_img: UIImageView!

    var isViewSelected = false

}
