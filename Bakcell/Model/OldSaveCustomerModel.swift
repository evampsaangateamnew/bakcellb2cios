//
//  OldSaveCustomerModel.swift
//  Bakcell
//
//  Created by AbdulRehman Warraich on 8/1/19.
//  Copyright © 2019 evampsaanga. All rights reserved.
//

import Foundation
import ObjectMapper

class CustomerData : NSObject,Mappable,NSCoding {
    
    var title : String?
    var firstName : String?
    var middleName : String?
    var lastName : String?
    var customerType : String?
    var gender : String?
    var dob : String?
    var accountId : String?
    var language : String?
    var effectiveDate : String?
    var expiryDate : String?
    var subscriberType : String?
    var status : String?
    var statusDetails : String?
    var brandId : String?
    var brandName : String?
    var loyaltySegment : String?
    var offeringId : String?
    var msisdn : String?
    var token : String?
    var imageURL : String?
    var billingLanguage : String?
    var customerId: String?
    var entityId : String?
    var offeringName : String?
    var offeringNameDisplay : String?
    var groupType : String = ""
    var sim : String?
    var pin : String?
    var puk : String?
    var email : String?
    
    var rateUsIOS : String?
    var firstPopup : String?
    var lateOnPopup : String?
    var rateUsPopupTitle : String?
    var rateUsPopupContent : String?
    
    var hideNumberTariffIds : [String]?
    
    
    override init() {
        super.init()
    }
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        title               <- map["title"]
        firstName           <- map["firstName"]
        middleName          <- map["middleName"]
        lastName            <- map["lastName"]
        customerType        <- map["customerType"]
        gender              <- map["gender"]
        dob                 <- map["dob"]
        accountId           <- map["accountId"]
        language            <- map["language"]
        effectiveDate       <- map["effectiveDate"]
        expiryDate          <- map["expiryDate"]
        subscriberType      <- map["subscriberType"]
        status              <- map["status"]
        statusDetails       <- map["statusDetails"]
        brandId             <- map["brandId"]
        brandName           <- map["brandName"]
        loyaltySegment      <- map["loyaltySegment"]
        offeringId          <- map["offeringId"]
        msisdn              <- map["msisdn"]
        token               <- map["token"]
        imageURL            <- map["imageURL"]
        billingLanguage     <- map["billingLanguage"]
        offeringName        <- map["offeringName"]
        offeringNameDisplay <- map["offeringNameDisplay"]
        entityId            <- map["entityId"]
        customerId          <- map["customerId"]
        groupType           <- map["groupType"]
        sim                 <- map["sim"]
        pin                 <- map["pin"]
        puk                 <- map["puk"]
        email               <- map["email"]
        
        rateUsIOS           <- map["rateus_ios"]
        firstPopup          <- map["firstPopup"]
        lateOnPopup         <- map["lateOnPopup"]
        rateUsPopupTitle    <- map["popupTitle"]
        rateUsPopupContent  <- map["popupContent"]
        
        hideNumberTariffIds <- map["hideNumberTariffIds"]
    }
    
    required convenience init?(coder aDecoder: NSCoder) {
        self.init()
        title = aDecoder.decodeObject(forKey: "title") as? String ?? ""
        firstName = aDecoder.decodeObject(forKey: "firstName") as? String ?? ""
        middleName = aDecoder.decodeObject(forKey: "middleName") as? String ?? ""
        lastName = aDecoder.decodeObject(forKey: "lastName") as? String ?? ""
        customerType = aDecoder.decodeObject(forKey: "customerType") as? String ?? ""
        gender = aDecoder.decodeObject(forKey: "gender") as? String ?? ""
        dob = aDecoder.decodeObject(forKey: "dob") as? String ?? ""
        accountId = aDecoder.decodeObject(forKey: "accountId") as? String ?? ""
        language = aDecoder.decodeObject(forKey: "language") as? String ?? ""
        effectiveDate = aDecoder.decodeObject(forKey: "effectiveDate") as? String ?? ""
        expiryDate = aDecoder.decodeObject(forKey: "expiryDate") as? String ?? ""
        subscriberType = aDecoder.decodeObject(forKey: "subscriberType") as? String ?? ""
        status = aDecoder.decodeObject(forKey: "status") as? String ?? ""
        statusDetails = aDecoder.decodeObject(forKey: "statusDetails") as? String ?? ""
        brandId = aDecoder.decodeObject(forKey: "brandId") as? String ?? ""
        brandName = aDecoder.decodeObject(forKey: "brandName") as? String ?? ""
        loyaltySegment = aDecoder.decodeObject(forKey: "loyaltySegment") as? String ?? ""
        offeringId = aDecoder.decodeObject(forKey: "offeringId") as? String ?? ""
        msisdn = aDecoder.decodeObject(forKey: "msisdn") as? String ?? ""
        token = aDecoder.decodeObject(forKey: "token") as? String ?? ""
        imageURL = aDecoder.decodeObject(forKey: "imageURL") as? String ?? ""
        billingLanguage = aDecoder.decodeObject(forKey: "billingLanguage") as? String ?? ""
        offeringName = aDecoder.decodeObject(forKey: "offeringName") as? String ?? ""
        offeringNameDisplay = aDecoder.decodeObject(forKey: "offeringNameDisplay") as? String ?? ""
        entityId = aDecoder.decodeObject(forKey: "entityId") as? String ?? ""
        customerId = aDecoder.decodeObject(forKey: "customerId") as? String ?? ""
        groupType = aDecoder.decodeObject(forKey: "groupType") as? String ?? ""
        sim = aDecoder.decodeObject(forKey: "sim") as? String ?? ""
        pin = aDecoder.decodeObject(forKey: "pin") as? String ?? ""
        puk = aDecoder.decodeObject(forKey: "puk") as? String ?? ""
        email = aDecoder.decodeObject(forKey: "email") as? String ?? ""
        
        
        rateUsIOS = aDecoder.decodeObject(forKey: "rateus_ios") as? String ?? ""
        firstPopup = aDecoder.decodeObject(forKey: "firstPopup") as? String ?? ""
        lateOnPopup = aDecoder.decodeObject(forKey: "lateOnPopup") as? String ?? ""
        rateUsPopupTitle = aDecoder.decodeObject(forKey: "popupTitle") as? String ?? ""
        rateUsPopupContent = aDecoder.decodeObject(forKey: "popupContent") as? String ?? ""
        
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(title, forKey: "title")
        aCoder.encode(firstName, forKey: "firstName")
        aCoder.encode(middleName, forKey: "middleName")
        aCoder.encode(lastName, forKey: "lastName")
        aCoder.encode(customerType, forKey: "customerType")
        aCoder.encode(gender, forKey: "gender")
        aCoder.encode(dob, forKey: "dob")
        aCoder.encode(accountId, forKey: "accountId")
        aCoder.encode(language, forKey: "language")
        aCoder.encode(effectiveDate, forKey: "effectiveDate")
        aCoder.encode(expiryDate, forKey: "expiryDate")
        aCoder.encode(subscriberType, forKey: "subscriberType")
        aCoder.encode(status, forKey: "status")
        aCoder.encode(statusDetails, forKey: "statusDetails")
        aCoder.encode(brandId, forKey: "brandId")
        aCoder.encode(brandName, forKey: "brandName")
        aCoder.encode(loyaltySegment, forKey: "loyaltySegment")
        aCoder.encode(offeringId, forKey: "offeringId")
        aCoder.encode(msisdn, forKey: "msisdn")
        aCoder.encode(token, forKey: "token")
        aCoder.encode(imageURL, forKey: "imageURL")
        aCoder.encode(billingLanguage, forKey: "billingLanguage")
        aCoder.encode(offeringName, forKey: "offeringName")
        aCoder.encode(offeringNameDisplay, forKey: "offeringNameDisplay")
        aCoder.encode(entityId, forKey: "entityId")
        aCoder.encode(customerId, forKey: "customerId")
        aCoder.encode(groupType, forKey: "groupType")
        aCoder.encode(sim, forKey: "sim")
        aCoder.encode(pin, forKey: "pin")
        aCoder.encode(puk, forKey: "puk")
        aCoder.encode(email, forKey: "email")
        
        aCoder.encode(rateUsIOS, forKey: "rateus_ios")
        aCoder.encode(firstPopup, forKey: "firstPopup")
        aCoder.encode(lateOnPopup, forKey: "lateOnPopup")
        aCoder.encode(rateUsPopupTitle, forKey: "popupTitle")
        aCoder.encode(rateUsPopupContent, forKey: "popupContent")
    }
    
}
