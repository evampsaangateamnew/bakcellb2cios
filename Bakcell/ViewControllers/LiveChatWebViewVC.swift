//
//  LiveChatWebViewVC.swift
//  Bakcell
//
//  Created by AbdulRehman Warraich on 7/27/18.
//  Copyright © 2018 evampsaanga. All rights reserved.
//

import UIKit
import WebKit

class LiveChatWebViewVC: BaseVC {
    //MARK:- Properties
    
    //MARK:- IBOutlet
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var webContrinerView: UIView!
    var myWebView: WKWebView!
    
    var link = URL(string:"google.com")!
    
    //MARK:- View Controller life cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        
        let config = WKWebViewConfiguration.init()
        config.preferences.javaScriptEnabled = true
        //config.preferences.javaEnabled = true
        config.preferences.javaScriptCanOpenWindowsAutomatically = true
        if #available(iOS 11.0, *) {
            config.setURLSchemeHandler(self, forURLScheme: "com.bakcell.carrierapp.portal")
        } else {
            // Fallback on earlier versions
        }
        
        myWebView = WKWebView.init(frame: webContrinerView.frame, configuration:config )
        
        myWebView.backgroundColor = .clear
        myWebView.navigationDelegate = self
        webContrinerView.addSubview(myWebView)
        myWebView.snp.makeConstraints { make in
            make.top.equalTo(webContrinerView.snp.top).offset(0.0)
            make.right.equalTo(webContrinerView.snp.right).offset(0.0)
            make.left.equalTo(webContrinerView.snp.left).offset(0.0)
            make.bottom.equalTo(webContrinerView.snp.bottom).offset(0.0)
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(reloadWebView), name: NSNotification.Name(Constants.KAppDidBecomeActive), object: nil)
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        loadWebViewNow()
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.interactivePop(true)
        
        titleLabel.text = Localized("Title_LiveChat")
        
    }
    
    
    func loadWebViewNow () {
        WKWebsiteDataStore.default().removeData(ofTypes: WKWebsiteDataStore.allWebsiteDataTypes(), modifiedSince: Date(timeIntervalSince1970: 0), completionHandler:{ })
        
        if MBLanguageManager.userSelectedLanguage() == .English {
            link = URL(string:"https://secure-fra.livechatinc.com/licence/12830742/v2/open_chat.cgi?name=\(MBUserSession.shared.userName().urlEncode)&email=\(MBUserSession.shared.userInfo?.email ?? "")&params=PhoneNumber%3D\(MBUserSession.shared.msisdn)&group=0")!
        } else if MBLanguageManager.userSelectedLanguage() == .Azeri {
            link = URL(string:"https://secure-fra.livechatinc.com/licence/12830742/v2/open_chat.cgi?name=\(MBUserSession.shared.userName().urlEncode)&email=\(MBUserSession.shared.userInfo?.email ?? "")&params=PhoneNumber%3D\(MBUserSession.shared.msisdn)&group=1")!
        } else if MBLanguageManager.userSelectedLanguage() == .Russian {
            link = URL(string:"https://secure-fra.livechatinc.com/licence/12830742/v2/open_chat.cgi?name=\(MBUserSession.shared.userName().urlEncode)&email=\(MBUserSession.shared.userInfo?.email ?? "")&params=PhoneNumber%3D\(MBUserSession.shared.msisdn)&group=2")!
        }
        
        let myURLRequest = URLRequest(url: link)
            
        //1. Load web site into my web view
        myWebView.load(myURLRequest)
        
    }
    
    @objc func reloadWebView() {
        
        let myURLRequest = URLRequest(url: link)
            
        //1. Load web site into my web view
        myWebView.load(myURLRequest)
    }
    
    // Remove notification
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
}


extension LiveChatWebViewVC : WKNavigationDelegate, WKUIDelegate, WKURLSchemeHandler {

    
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        if (error as NSError).code !=  -999 {
            
            activityIndicator.hideActivityIndicator()
            self.showErrorAlertWithMessage(message: Localized("Message_FailureLiveChat"))
            MBUserSession.shared.liveChatObject = nil
            self.myWebView.showDescriptionViewWithImage(description: Localized("Message_FailureLiveChat"))
        }
        activityIndicator.hideActivityIndicator()
        print("error found: \(error)")
    }
    func webView(_ webView: WKWebView, didFailProvisionalNavigation navigation: WKNavigation!, withError error: Error) {
        if (error as NSError).code !=  -999 {
            
            activityIndicator.hideActivityIndicator()
            self.showErrorAlertWithMessage(message: Localized("Message_FailureLiveChat"))
            MBUserSession.shared.liveChatObject = nil
            self.myWebView.showDescriptionViewWithImage(description: Localized("Message_FailureLiveChat"))
        }
        activityIndicator.hideActivityIndicator()
        print("error found: \(error)")
    }
    
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        activityIndicator.showActivityIndicator()
    }
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        activityIndicator.hideActivityIndicator()
        //print("Finished navigating to url \(webView.url)")
    }
    
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        
        switch navigationAction.navigationType {
            case .linkActivated:
                if let newURL = navigationAction.request.url,
                UIApplication.shared.canOpenURL(newURL) {
                
                self.openURLInSafari(urlString: newURL.absoluteString)
                decisionHandler(.cancel)
                
            }
            default:
                break
        }

        decisionHandler(.allow)
    }
        
    @available(iOS 11.0, *)
    func webView(_ webView: WKWebView, start urlSchemeTask: WKURLSchemeTask) {
        
    }
    
    @available(iOS 11.0, *)
    func webView(_ webView: WKWebView, stop urlSchemeTask: WKURLSchemeTask) {
        
    }
}
