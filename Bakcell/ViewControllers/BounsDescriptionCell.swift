//
//  BounsDescriptionCell.swift
//  Bakcell
//
//  Created by AbdulRehman Warraich on 1/1/18.
//  Copyright © 2018 evampsaanga. All rights reserved.
//

import UIKit
import MarqueeLabel

class BounsDescriptionCell: UITableViewCell {

    static let identifier : String = "BounsDescriptionCell"

    @IBOutlet var containerView: UIView!
    @IBOutlet var detail_lbl: MarqueeLabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        detail_lbl.setupMarqueeAnimation()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func setTopDescriptionLayoutValues(bonusTitle : String?, bonusIconName : String?, bonusDescription :String? ) {

        self.containerView.backgroundColor = UIColor.MBDimLightGrayColor
        detail_lbl.backgroundColor = UIColor.clear

        // create an NSMutableAttributedString that we'll append everything to
        let fullString = NSMutableAttributedString(string: "")

        //        // create our NSTextAttachment
        //        let iconImage = NSTextAttachment()
        //        iconImage.image = UIImage(named: "Mobile-state-2")
        //        iconImage.setImageSize(size: CGSize(width: 21, height: 21))
        //
        //        // wrap the attachment in its own attributed string so we can append it
        //        let iconImageWraper = NSAttributedString(attachment: iconImage)

        var bonusTitleString = ""

        if bonusTitle?.isBlank == false {
            bonusTitleString = "\( bonusTitle ?? ""): "
        }

        let bonusTitle = NSAttributedString(string:bonusTitleString, attributes: [NSAttributedString.Key.foregroundColor: UIColor.MBRedColor])

        let bonusDescription = NSAttributedString(string: bonusDescription ?? "", attributes: [NSAttributedString.Key.foregroundColor: UIColor.MBDarkGrayColor])

        // add the NSTextAttachment wrapper to our full string, then add some more text.
        //        fullString.append(iconImageWraper)
        fullString.append(bonusTitle)
        fullString.append(bonusDescription)

        if fullString.length > 0 {
            detail_lbl.attributedText  = fullString
        } else {
            detail_lbl.text = ""

        }
    }
    
}
