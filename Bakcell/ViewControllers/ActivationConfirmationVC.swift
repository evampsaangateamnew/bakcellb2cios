//
//  ActivationConfirmationVC.swift
//  Bakcell
//
//  Created by AbdulRehman Warraich on 10/18/17.
//  Copyright © 2017 evampsaanga. All rights reserved.
//

import UIKit

class ActivationConfirmationVC: BaseVC {
    
    //MARK:- Properties
    var alertTitle = ""
    var attributedDiscription : NSMutableAttributedString = NSMutableAttributedString()
    var yesButtonTitle = ""
    var noButtonTitle = ""
    
    fileprivate var yesBtncompletionHandlerBlock : MBButtonCompletionHandler?
    fileprivate var noBtncompletionHandlerBlock : MBButtonCompletionHandler?
    
    //MARK:- IBOutlet
    @IBOutlet var title_lbl: UILabel!
    @IBOutlet var description_lbl: UILabel!
    
    @IBOutlet var yes_btn: UIButton!
    @IBOutlet var no_btn: UIButton!
    
    //MARK:- ViewController method
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.interactivePop(false)
        
        self.yes_btn.isHidden = false;
        self.no_btn.isHidden = false;
        
        yes_btn.setTitle(yesButtonTitle, for: UIControl.State.normal)
        no_btn.setTitle(noButtonTitle, for: UIControl.State.normal)
        
        // Setting buttons layout
        no_btn.backgroundColor = .white
        no_btn.layer.borderWidth = 1
        no_btn.layer.borderColor = UIColor.MBBorderGrayColor.cgColor
        
        yes_btn.backgroundColor = UIColor.MBButtonBackgroundGrayColor
        
        title_lbl.text = alertTitle
        description_lbl.attributedText = attributedDiscription
    }
    
    //MARK:- IBACTIONS
    @IBAction func YesBtnPressed(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
        self.yesBtncompletionHandlerBlock?("")
        
        
    }
    
    @IBAction func NoBtnPressed(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
        self.noBtncompletionHandlerBlock?("")
    }
    
    //MARK:- Functions
    func setConfirmationAlertLayout(title :String = Localized("Title_Confirmation"),message : String, yesBtnTitle : String = Localized("BtnTitle_YES"), noBtnTitle: String = Localized("BtnTitle_NO"))  {
        
        yesButtonTitle = yesBtnTitle
        noButtonTitle = noBtnTitle
        alertTitle = title
        
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.alignment = .left
        paragraphStyle.minimumLineHeight = 22
        
        let message = NSMutableAttributedString(string: message, attributes: [NSAttributedString.Key.foregroundColor: UIColor.MBTextGrayColor, NSAttributedString.Key.paragraphStyle:paragraphStyle])
        
        attributedDiscription = message
        
    }
    
    func setConfirmationAlertLayoutForTariffMigration(title :String = Localized("Title_Confirmation"), message : String, yesBtnTitle : String = Localized("BtnTitle_YES"), noBtnTitle: String = Localized("BtnTitle_NO"))  {
        
        yesButtonTitle = yesBtnTitle
        noButtonTitle = noBtnTitle
        alertTitle = title
        
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.alignment = .left
        paragraphStyle.minimumLineHeight = 22
        
        let attributedMessage = NSMutableAttributedString(string: message, attributes: [NSAttributedString.Key.foregroundColor: UIColor.MBTextGrayColor, NSAttributedString.Key.paragraphStyle:paragraphStyle])
        //
        //        if let amountString = amount {
        //            var amountTextRange = (message as NSString).range(of: amountString)
        //            amountTextRange.length = amountTextRange.length + 4 // to include AZN
        //
        //            attributedMessage.addAttribute(NSForegroundColorAttributeName, value: UIColor.black, range: amountTextRange)
        //        }
        
        attributedDiscription = attributedMessage
        
    }
    
    // Yes and No button completion handler
    func setYesButtonCompletionHandler(completionBlock : @escaping MBButtonCompletionHandler ) {
        self.yesBtncompletionHandlerBlock = completionBlock
    }
    
    func setNoButtonCompletionHandler(completionBlock : @escaping MBButtonCompletionHandler ) {
        self.noBtncompletionHandlerBlock = completionBlock
    }
    
    
}
