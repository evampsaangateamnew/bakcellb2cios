//
//  ChangePasswordVC.swift
//  Bakcell
//
//  Created by Saad Riaz on 6/19/17.
//  Copyright © 2017 evampsaanga. All rights reserved.
//

import UIKit
import ObjectMapper

class ChangePasswordVC: BaseVC {
    
    // MARK: - Properties
    var alertCheck = false
    
    // MARK: - IBOutlet
    @IBOutlet var update_btn: UIButton!
    @IBOutlet var error_lbl: UILabel!
    @IBOutlet var info_lbl: UILabel!
    @IBOutlet var changePassTitle_lbl: UILabel!
    @IBOutlet var passwordStrengthView : UIView!
    @IBOutlet var security_lbl: UILabel!
    @IBOutlet var securityBar : UIProgressView!
    @IBOutlet var alertView: UIView!
    
    @IBOutlet var oldPassword_txt: UITextField!
    @IBOutlet var newPassword_txt: UITextField!
    @IBOutlet var confirmNewPassword_txt: UITextField!
    @IBOutlet var newPasswordTopConstraint: NSLayoutConstraint!
    @IBOutlet var infoViewHeightConstraint: NSLayoutConstraint!
    
    //MARK:- ViewController Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        oldPassword_txt.delegate = self
        newPassword_txt.delegate = self
        confirmNewPassword_txt.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.interactivePop(true)
        
        loadViewLayout()
    }
    
    //MARK:- IBActions
    @IBAction func updatePressed(_ sender: Any) {
        
        if MBUserSession.shared.changePasswordRequestCount < 5 {
            
            var oldPassword : String = ""
            var newPassword : String = ""
            var confirmNewPassword : String = ""
            
            // Current Password validation
            if let oldPasswordText = oldPassword_txt.text,
                oldPasswordText.isEmpty == false {
                
                let passwordStrength:Constants.MBPasswordStrength = self.determinePasswordStrength(Text: oldPasswordText)
                
                switch passwordStrength {
                    
                case Constants.MBPasswordStrength.didNotMatchCriteria :
                    self.showErrorAlertWithMessage(message: Localized("Message_InvalidCurrentPassword"))
                    return
                case Constants.MBPasswordStrength.Week, Constants.MBPasswordStrength.Medium, Constants.MBPasswordStrength.Strong:
                    oldPassword = oldPasswordText
                }
                
            } else {
                self.showErrorAlertWithMessage(message: Localized("Message_InvalidCurrentPassword"))
                return
            }
            
            // Password validation
            if let passwordText = newPassword_txt.text,
                passwordText.isEmpty == false {
                
                let passwordStrength:Constants.MBPasswordStrength = self.determinePasswordStrength(Text: passwordText)
                
                switch passwordStrength {
                    
                case Constants.MBPasswordStrength.didNotMatchCriteria:
                    self.showErrorAlertWithMessage(message: Localized("Message_InvalidPasswordComplexity"))
                    return
                case Constants.MBPasswordStrength.Week, Constants.MBPasswordStrength.Medium, Constants.MBPasswordStrength.Strong:
                    newPassword = passwordText
                }
                
            } else {
                self.showErrorAlertWithMessage(message: Localized("Message_InvalidPasswordComplexity"))
                return
            }
            
            // Confirm Password validation
            if let confirmPasswordText = confirmNewPassword_txt.text,
                confirmPasswordText.isEmpty == false {
                
                if confirmPasswordText == newPassword {
                    confirmNewPassword = confirmPasswordText
                } else {
                    self.showErrorAlertWithMessage(message: Localized("Message_InvalidConfirmPassword"))
                    return
                }
            } else {
                self.showErrorAlertWithMessage(message: Localized("Message_InvalidConfirmPassword"))
                return
            }
            
            // Hiding Key board
            _ = oldPassword_txt.resignFirstResponder()
            _ = newPassword_txt.resignFirstResponder()
            _ = confirmNewPassword_txt.resignFirstResponder()
            
            if MBUserSession.shared.aSecretKey.isBlank == false {
                changePasswordUserAPICall(OldPassword: oldPassword.aesEncrypt(key: MBUserSession.shared.aSecretKey), NewPassword: newPassword.aesEncrypt(key: MBUserSession.shared.aSecretKey), ConfirmNewPassword: confirmNewPassword.aesEncrypt(key: MBUserSession.shared.aSecretKey))
                
            } else {
                self.showErrorAlertWithMessage(message: Localized("Message_GenralError"))
            }
        }
    }
    
    @IBAction func infoPressed(_ sender: UIButton) {
        
        if alertCheck == false{
            alertView.isHidden = false
            alertCheck = true
            newPasswordTopConstraint.constant = infoViewHeightConstraint.constant + 10
            
        } else {
            alertView.isHidden = true
            alertCheck = false
            newPasswordTopConstraint.constant = 24
        }
        
    }
    
    
    //MARK: - FUNCTIONS
    func loadViewLayout(){
        changePassTitle_lbl.text = Localized("Title_ChangePassword")
        error_lbl.text = Localized("Password_ErrorInfo")
        info_lbl.text = Localized("CH_Description")
        update_btn.setTitle(Localized("BtnCHTitle_Update"), for: UIControl.State.normal)
        oldPassword_txt.placeholder = Localized("currentPass_PlaceHolder")
        newPassword_txt.placeholder = Localized("SetPassword_PlaceHolder")
        confirmNewPassword_txt.placeholder = Localized("ConfirmPassword_PlaceHolder")
    }
    
    func setPasswordStrenghtBar(Text text:String) {
        let passwordStrength:Constants.MBPasswordStrength = self.determinePasswordStrength(Text: text)
        
        switch passwordStrength {
        case Constants.MBPasswordStrength.didNotMatchCriteria:
            passwordStrengthView.isHidden = true
            security_lbl.text = ""
            securityBar.setProgress(0, animated: false)
            securityBar.tintColor = UIColor.clear
            break
            
        case Constants.MBPasswordStrength.Week:
            passwordStrengthView.isHidden = false
            security_lbl.isHidden = false
            security_lbl.text = Localized("Pass_Weak")
            securityBar.tintColor = UIColor.MBBarRed
            securityBar.setProgress(0.33, animated: false)
            break
            
        case Constants.MBPasswordStrength.Medium:
            passwordStrengthView.isHidden = false
            security_lbl.isHidden = false
            security_lbl.text = Localized("Pass_Medium")
            securityBar.tintColor = UIColor.MBBarOrange
            securityBar.setProgress(0.66, animated: false)
            break
            
        case Constants.MBPasswordStrength.Strong:
            passwordStrengthView.isHidden = false
            security_lbl.isHidden = false
            security_lbl.text = Localized("Pass_Strong")
            securityBar.tintColor = UIColor.MBBarGreen
            securityBar.setProgress(1, animated: false)
            
        }
    }
    
    func redirectToManageNumber() {
        
        /* navigate to Manage AccountsVC to add/delete accounts */
        if let manageAccountsVC :ManageAccountsVC = ManageAccountsVC.instantiateViewControllerFromStoryboard(), (MBUserSession.shared.loggedInUsers?.users?.count ?? 0) >= 1 {
            manageAccountsVC.canNavigateBack = false
            manageAccountsVC.descriptionMessage = Localized("Message_AccountRemovedDescription")
            self.navigationController?.pushViewController(manageAccountsVC, animated: true)
        } else {
            BaseVC.logout()
        }
    }
    
    //MARK: - API Calls
    /// Call 'changePassword' API with the specified `OldPassword`, 'NewPassword' and `ConfirmNewPassword`.
    ///
    /// - parameter OldPassword:     The OldPassword of user.
    /// - parameter NewPassword:  The User new Password for account.
    /// - parameter ConfirmNewPassword:  The User Confirm new Password for account.
    ///
    /// - returns: Void
    func changePasswordUserAPICall(OldPassword oldPassword : String , NewPassword newPassword : String, ConfirmNewPassword confirmNewPassword : String) {
        
        activityIndicator.showActivityIndicator()
        _ = MBAPIClient.sharedClient.changePassword(OldPassword: oldPassword, NewPassword:newPassword , ConfirmNewPassword: confirmNewPassword,{ (response, resultData, error, isCancelled, status, resultCode, resultDesc) in
            
            self.activityIndicator.hideActivityIndicator()
            
            if resultCode == Constants.MBAPIStatusCode.wrongAttemptToChangePassword.rawValue {
                
                MBUserSession.shared.changePasswordRequestCount = MBUserSession.shared.changePasswordRequestCount + 1
                if MBUserSession.shared.changePasswordRequestCount > 4 {
                    
                    // Reset counter
                    MBUserSession.shared.changePasswordRequestCount = 0
                    // Clear user Data
                    BaseVC.logout()
                    
                }  else {
                    // Show error alert to user
                    self.showErrorAlertWithMessage(message: resultDesc)
                }
            } else if error != nil {
                //EventLog
                MBAnalyticsManager.logEvent(screenName: "Settings Screens", contentType:"Change Password Screen" , status:"Change Password Failure" )
                
                error?.showServerErrorInViewController(self)
                
            } else {
                // handling data from API response.
                if resultCode == Constants.MBAPIStatusCode.succes.rawValue,
                    let changePasswordHandler = Mapper<MessageResponse>().map(JSONObject:resultData) {
                    
                    //EventLog
                    MBAnalyticsManager.logEvent(screenName: "Settings Screens", contentType:"Change Password Screen" , status:"Change Password Success" )
                    
                    /* Remove current user info */
                    MBUserInfoUtilities.removeCustomerDataOf(msisdn: MBUserSession.shared.msisdn)
                    
                    self.showAlertWithTitleAndMessage(title: Localized("Title_successful"), message: changePasswordHandler.message, { (aString) in
                        
                        // Reset counter
                        MBUserSession.shared.changePasswordRequestCount = 0
                        
                        self.redirectToManageNumber()
                    })
                } else {
                    //EventLog
                    MBAnalyticsManager.logEvent(screenName: "Settings Screens", contentType:"Change Password Screen" , status:"Change Password Failure" )
                    // Show error alert to user
                    self.showErrorAlertWithMessage(message: resultDesc)
                }
            }
        })
    }
}

//MARK: Textfield delagates
extension ChangePasswordVC : UITextFieldDelegate {
    
    //Set max Limit for textfields
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        guard let text = textField.text else { return false }
        let newLength = text.count + string.count - range.length
        
        // Allow charactors check
        let allowedCharactors = Constants.allowedAlphbeats + Constants.allowedNumbers + Constants.allowedSpecialCharacters
        let allowedCharacters = CharacterSet(charactersIn: allowedCharactors)
        let characterSet = CharacterSet(charactersIn: string)
        
        if allowedCharacters.isSuperset(of: characterSet) != true {
            
            return false
        }
        
        if textField == newPassword_txt {
            
            let validLimit : Bool = newLength <= 15
            
            if validLimit {
                
                let newString = (text as NSString).replacingCharacters(in: range, with: string)
                setPasswordStrenghtBar(Text: newString)
                
            }
            
            return validLimit // Bool
        } else {
            // For other then setPassword textfield.
            return newLength <= 15 // Bool
        }
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.text = ""
        setPasswordStrenghtBar(Text:"")
    }
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        setPasswordStrenghtBar(Text:"")
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == oldPassword_txt {
            newPassword_txt.becomeFirstResponder()
            return false
        } else if textField == newPassword_txt {
            confirmNewPassword_txt.becomeFirstResponder()
            return false
        } else if textField == confirmNewPassword_txt {
            updatePressed(UIButton())
            return false
        } else {
            return true
        }
    }
}
