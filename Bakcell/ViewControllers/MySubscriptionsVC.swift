//
//  MySubscriptionsVC.swift
//  Bakcell
//
//  Created by Saad Riaz on 6/13/17.
//  Copyright © 2017 evampsaanga. All rights reserved.
//

import UIKit
import  FZAccordionTableView

class MySubscriptionsVC: BaseVC, UICollectionViewDelegate,UICollectionViewDataSource {
    static fileprivate let kTableViewCellReuseIdentifier = "TableViewCellReuseIdentifier"
    
    
    @IBOutlet var mainView: UIView!
    @IBOutlet var collectionView: UICollectionView!
    
    var collectionArr: [String] = ["Calls","SMS","Hybrid","Campaign","TM","Roaming"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        collectionView.selectItem(at:  IndexPath(item: 0, section: 0), animated: false, scrollPosition: UICollectionViewScrollPosition.left)
        let subscription = self.storyboard!.instantiateViewController(withIdentifier: "SubscriptionsMainVC")as! SubscriptionsMainVC
        self.addChildViewController(subscription)
        mainView.addSubview(subscription.view)
        subscription.didMove(toParentViewController: self)
        
        subscription.view.snp.makeConstraints { make in
            make.top.equalTo(mainView.snp.top).offset(0.0)
            make.right.equalTo(mainView.snp.right).offset(0.0)
            make.left.equalTo(mainView.snp.left).offset(0.0)
            make.bottom.equalTo(mainView.snp.bottom).offset(0.0)
        }

    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - COLLECTION VIEW DELEGATE METHODS
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 5
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellID", for: indexPath) as! SubscriptionCell
        cell.layer.borderColor = UIColor.MBDimLightGrayColor.cgColor
        cell.title_lbl.text = collectionArr[indexPath.row]
        
        let bgColorView = UIView()
        bgColorView.backgroundColor = UIColor.lightGray
        cell.selectedBackgroundView = bgColorView
        
        return cell
    }
    
}

