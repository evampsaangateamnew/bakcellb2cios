//
//  ForwardNumberVC.swift
//  Bakcell
//
//  Created by Saad Riaz on 6/7/17.
//  Copyright © 2017 evampsaanga. All rights reserved.
//

import UIKit
import ObjectMapper

class ForwardNumberVC: BaseVC {

    var selectedCoreServiceListItem : CoreServicesList?

    fileprivate var didChangeNumberBlock : MBButtonCompletionHandler?
    
    @IBOutlet var title_lbl: UILabel!
    @IBOutlet var description_lbl: UILabel!
    @IBOutlet var mobileNumber_txt: UITextField!
    @IBOutlet var save_Btn: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        mobileNumber_txt.delegate = self

        if let number = selectedCoreServiceListItem?.forwardNumber {

            if number.isBlank == false {
                mobileNumber_txt.text = number
            } else {
                mobileNumber_txt.text = ""
            }

        } else {
            mobileNumber_txt.text = ""
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.interactivePop(true)
        
        // Layout with localized string
        layoutViewController()
    }
    //MARK: IBACTIONS
    @IBAction func savePressed(_ sender: Any) {

        var msisdn : String = ""
        // MSISDN validation
        if mobileNumber_txt.text?.isBlank == true {
            self.showErrorAlertWithMessage(message: Localized("Message_EnterNumber"))
            return
        }
        if let mobileNumberText = mobileNumber_txt.text,
            mobileNumberText.count == 9 {
            msisdn = mobileNumberText
            
        } else {
            self.showErrorAlertWithMessage(message: Localized("Message_EnterValidNumber"))
            return
        }

        // TextField resignFirstResponder
        mobileNumber_txt.resignFirstResponder()
        // Calling APi to add number
        processCoreServices(forwardNumber: msisdn)
    }


    //MARK: - Functions
    func layoutViewController() {
        title_lbl.text                  = Localized("Title_ForwardNumber")
        description_lbl.text            = Localized("Info_Discription")
        mobileNumber_txt.placeholder    =  Localized("PlaceHolder_FowardNumber")
        save_Btn.setTitle(Localized("BtnTitle_SAVE"), for: UIControl.State.normal)
    }

    // Yes and No button completion handler
    func setCompletionHandler(completionBlock : @escaping MBButtonCompletionHandler ) {
        self.didChangeNumberBlock = completionBlock
    }
    //MARK: - API Calls
    /// Call 'getCoreServices' API .
    ///
    /// - returns: Void
    func processCoreServices(forwardNumber: String) {
        activityIndicator.showActivityIndicator()

        _ = MBAPIClient.sharedClient.processCoreServices(actionType: true, offeringId: selectedCoreServiceListItem?.offeringId ?? "", number: forwardNumber,accountType: MBUserSession.shared.customerType, groupType: MBUserSession.shared.groupType, { (response, resultData, error, isCancelled, status, resultCode, resultDesc) in

            self.activityIndicator.hideActivityIndicator()

            if error != nil {
                //EventLog
                MBAnalyticsManager.logEvent(screenName: "Services Screen", contentType:"Services Core Services Screen" , status:"Farword Number Failure")
                
                error?.showServerErrorInViewController(self)

            } else {
                // handling data from API response.
                if resultCode == Constants.MBAPIStatusCode.succes.rawValue {

                //EventLog
                MBAnalyticsManager.logEvent(screenName: "Services Screen", contentType:"Services Core Services Screen" , status:"Farword Number Success" )
                
                
                    self.showSuccessAlertWithMessage(message: resultDesc, { (aString) in
                        self.didChangeNumberBlock?(forwardNumber)
                        self.navigationController?.popViewController(animated: true)
                    })
                    
                } else {
                    //EventLog
                    MBAnalyticsManager.logEvent(screenName: "Services Screen", contentType:"Services Core Services Screen" , status:"Farword Number Failure")
                    self.showErrorAlertWithMessage(message: resultDesc ?? "")
                }
            }
        })
    }
}

//MARK: - Textfield delagates

extension ForwardNumberVC : UITextFieldDelegate {
    //Set max Limit for textfields
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let text = textField.text else { return false } // Check that if text is nill then return false

        let newLength = text.count + string.count - range.length

        if textField == mobileNumber_txt {

            let allowedCharacters = CharacterSet(charactersIn: Constants.allowedNumbers)
            let characterSet = CharacterSet(charactersIn: string)
            if newLength <= 9 && allowedCharacters.isSuperset(of: characterSet) {
                // Returning number input
                return  true
            } else {
                return  false
            }

        } else {
            return false
        }
    }
}
