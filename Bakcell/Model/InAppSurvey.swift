//
//  InAppSurvey.swift
//  Bakcell
//
//  Created by Muhammad Irfan Awan on 13/08/2021.
//  Copyright © 2021 evampsaanga. All rights reserved.
//



import Foundation
import ObjectMapper


class InAppSurveyMapper : Mappable{
    
    var surveys : [InAppSurvey]?
    var userSurveys:[SelectedSurvey]?
    
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        
        surveys         <- map["surveys"]
        userSurveys     <- map["userSurveys"]
        
    }
}

class InAppSurvey: Mappable {
    
    var survayId: String?
    var screenName: String?
    var timeLimit: String?
    var visitLimit: String?
    var surveyLimit: String?
    var surveyCount: String?
    var onTransactionCompleteEn: String?
    var onTransactionCompleteAz: String?
    var onTransactionCompleteRu: String?
    var commentEnable: String?
    var commentsTitleEn: String?
    var commentsTitleAz: String?
    var commentsTitleRu: String?
    var titleEn: String?
    var titleAz: String?
    var titleRu: String?
    var customerType: String?
    var questions:[SurvayQuestions]?
    
    
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        
        survayId                <- map["id"]
        screenName              <- map["screenName"]
        timeLimit               <- map["timeLimit"]
        visitLimit              <- map["visitLimit"]
        surveyLimit             <- map["surveyLimit"]
        onTransactionCompleteEn   <- map["onTransactionCompleteEn"]
        onTransactionCompleteAz   <- map["onTransactionCompleteAz"]
        onTransactionCompleteRu   <- map["onTransactionCompleteRu"]
        commentEnable           <- map["commentEnable"]
        commentsTitleEn         <- map["commentsTitleEn"]
        commentsTitleAz         <- map["commentsTitleAz"]
        commentsTitleRu         <- map["commentsTitleRu"]
        titleEn                 <- map["titleEn"]
        titleAz                 <- map["titleAz"]
        titleRu                 <- map["titleRu"]
        customerType            <- map["customerType"]
        questions               <- map["questions"]
        
    }
}

class SurvayQuestions: Mappable {
    
    var questionId: String?
    var questionTextEN: String?
    var questionTextAZ: String?
    var questionTextRU: String?
    var answerType: String?
    var answers: [SurvayAnswer]?
    
    required init?(map: Map) {
        
    }
    func mapping(map: Map) {
        
        questionId      <- map["id"]
        questionTextEN  <- map["questionTextEN"]
        questionTextAZ  <- map["questionTextAZ"]
        questionTextRU  <- map["questionTextRU"]
        answerType      <- map["answerType"]
        answers         <- map["answers"]
        
    }
}

class SurvayAnswer: Mappable {
    
    var answerId: String?
    var answerTextEN: String?
    var answerTextAZ: String?
    var answerTextRU: String?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        answerId        <- map["id"]
        answerTextEN    <- map["answerTextEN"]
        answerTextAZ    <- map["answerTextAZ"]
        answerTextRU    <- map["answerTextRU"]
        
    }
}

class SelectedSurvey: Mappable {
    
    var offeringId: String?
    var offeringType: String?
    var surveryId: String?
    var questionId: String?
    var answerId: String?
    var comments: String?
    var answer: String?
    required init?(map: Map) {
        
    }
    
    init() {
        
    }
    
    func mapping(map: Map) {
        
        offeringId      <- map["offeringId"]
        offeringType    <- map["offeringType"]
        surveryId       <- map["surveyId"]
        questionId      <- map["questionId"]
        answerId        <- map["answerId"]
        comments        <- map["comments"]
        answer          <- map["answer"]
    }
    
    
}
