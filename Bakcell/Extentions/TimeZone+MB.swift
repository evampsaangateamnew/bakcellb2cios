//
//  TimeZone+MB.swift
//  Bakcell
//
//  Created by AbdulRehman Warraich on 1/2/19.
//  Copyright © 2019 evampsaanga. All rights reserved.
//

import Foundation
extension TimeZone{
    static func appTimeZone() -> TimeZone {
        return self.init(abbreviation: "UTC") ??  self.current
    }
}
