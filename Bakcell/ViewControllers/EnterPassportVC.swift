//
//  EnterPassportVC.swift
//  Bakcell
//
//  Created by Saad Riaz on 5/29/17.
//  Copyright © 2017 evampsaanga. All rights reserved.
//

import UIKit
import ObjectMapper

class EnterPassportVC: BaseVC {

    //MARK: - IBOutlet
    @IBOutlet var user_img: UIImageView!
    @IBOutlet var dateDropDown: UIDropDown!
    @IBOutlet var catDropDown: UIDropDown!
    @IBOutlet var numberField: UITextField!
    @IBOutlet var description_lbl: UILabel!
    @IBOutlet var submit_btn: UIButton!
    
    
    //MARK: - View Controller Methods
    override func viewDidLoad() {
        super.viewDidLoad()

        numberField.delegate = self
        
        //Drop Down
        catDropDown.optionsTextAlignment = NSTextAlignment.center
        catDropDown.textAlignment = NSTextAlignment.center
        catDropDown.placeholder = Localized("DropDown_All")
        self.catDropDown.rowBackgroundColor = UIColor.clear
        self.catDropDown.borderWidth = 0
        self.catDropDown.tableHeight = 140
        catDropDown.optionsSize = 14
        catDropDown.fontSize = 12
        catDropDown.options = [Localized("DropDown_All"),Localized("DropDown_Internet"), Localized("DropDown_Voice"), Localized("DropDown_SMS"),Localized("DropDown_Others")]
        
        
        //Drop Down
        dateDropDown.optionsTextAlignment = NSTextAlignment.center
        dateDropDown.textAlignment = NSTextAlignment.center
        dateDropDown.placeholder = Localized("DropDown_CurrentDay")
        self.dateDropDown.rowBackgroundColor = UIColor.clear
        self.dateDropDown.borderWidth = 0
        self.dateDropDown.tableHeight = 176
        dateDropDown.optionsSize = 14
        dateDropDown.fontSize = 12
        dateDropDown.options = [Localized("DropDown_CurrentDay"),Localized("DropDown_Last7Days"), Localized("DropDown_Last30Days"), Localized("DropDown_PreviousMonth"),Localized("DropDown_CustomPeriod")]
    
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.interactivePop(true)
        
         loadViewLayout()
        self.user_img.animationImages = [UIImage (named: "redZeros") ?? UIImage(), UIImage (named: "greyZeros") ?? UIImage()]
        self.user_img.animationDuration = 1
        self.user_img.startAnimating()

        // Stop animation
        DispatchQueue.main.asyncAfter(deadline: .now() + 4) {
            self.user_img.stopAnimating()
        }
    }

    //MARK: IBACTIONS
    @IBAction func SubmitPressed(_ sender: Any) {
        
        var number : String = ""
        // MSISDN validation
        if let numberText = numberField.text,
            numberText.count == 7 {
            number = numberText
            
        } else {
            self.showErrorAlertWithMessage(message: Localized("Message_EnterFilter"))
            return
        }
        
        // TextField resignFirstResponder
        numberField.resignFirstResponder()
        
        self.verifyAccountDetails(passportNumber: number)

    }
    
    //MARK: - FUNCTIONS
    func loadViewLayout(){
        description_lbl.text = Localized("str_Description")
        //        numberField.placeholder = Localized("PlaceHolder_Enter7Digit")
        submit_btn.setTitle(Localized("BtnTitle_Submit"), for: UIControl.State.normal)

        numberField.textColor = UIColor.MBTextGrayColor
        numberField.attributedPlaceholder =  NSAttributedString(string: Localized("PlaceHolder_Enter7Digit"), attributes: [NSAttributedString.Key.foregroundColor: UIColor.MBTextGrayColor])

    }
    
    //MARK: - API Calls
    /// Call 'verifyAccountDetails' API .
    ///
    /// - returns: pin
    func verifyAccountDetails(passportNumber:String?) {
        
        activityIndicator.showActivityIndicator()
        
        _ = MBAPIClient.sharedClient.verifyAccountDetails(PassportNumber: passportNumber ?? "", { (response, resultData, error, isCancelled, status, resultCode, resultDesc) in
            
            self.activityIndicator.hideActivityIndicator()
            if error != nil {
                //EventLog
                MBAnalyticsManager.logEvent(screenName: "My Account", contentType:"Usage History Detail Screen" , status:"Usage Detail Passport Verification Failure" )
                
                error?.showServerErrorInViewController(self)
                
            } else {
                // handling data from API response.
                if resultCode == Constants.MBAPIStatusCode.succes.rawValue {
                    
                //EventLog
                MBAnalyticsManager.logEvent(screenName: "My Account", contentType:"Usage History Detail Screen" , status:"Usage Detail Passport Verification Success" )
                
                    
                    // Parssing response data
                    if let getCreditResponse = Mapper<UsagePin>().map(JSONObject: resultData) {
                
                        let verifyPIN = self.myStoryBoard.instantiateViewController(withIdentifier: "VerifyPinVC")as! VerifyPinVC
                        verifyPIN.numberText = passportNumber ?? ""
                        verifyPIN.pinText = getCreditResponse.usagePin ?? ""

                        self.navigationController?.pushViewController(verifyPIN, animated: true)
                    }
                } else {
                    //EventLog
                    MBAnalyticsManager.logEvent(screenName: "My Account", contentType:"Usage History Detail Screen" , status:"Usage Detail Passport Verification Failure" )
                    // Show error alert to user
                    self.showErrorAlertWithMessage(message: resultDesc)
                }
            }
        })
    }
}
//MARK: - Textfield delagates

extension EnterPassportVC : UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let text = textField.text else { return false }
        
        //Set max Limit for textfields
        let newLength = text.count + string.count - range.length
        
        //  OTP textfield
        if textField == numberField {
            
            // limiting Passport lenght to 7 digits
            let allowedCharacters = CharacterSet(charactersIn: "\(Constants.allowedAlphbeats)\(Constants.allowedNumbers)")
            let characterSet = CharacterSet(charactersIn: string)
            
            if newLength <= 7 && allowedCharacters.isSuperset(of: characterSet) {

                // Returning number input
                return  true
            } else {
                return  false
            }
        } else  {
            return false
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == numberField {
            SubmitPressed(UIButton())
            return false
        } else {
            return true
        }
    }
}
