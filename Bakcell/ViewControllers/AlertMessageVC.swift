//
//  AlertMessageVC.swift
//  Bakcell
//
//  Created by AbdulRehman Warraich on 11/15/17.
//  Copyright © 2017 evampsaanga. All rights reserved.
//

import UIKit

class AlertMessageVC: UIViewController {
    
    //MARK:- Properties
    fileprivate var okBtncompletionHandlerBlock : MBButtonCompletionHandler?
    var myTitle : String = ""
    var descriptionText : String = ""
    var okBtnTitle : String = ""
    
    
    //MARK:- IBOutlet
    @IBOutlet var contentView: UIView!
    @IBOutlet var title_lbl: UILabel!
    @IBOutlet var description_lbl: UILabel!
    @IBOutlet var ok_btn: UIButton!
    
    //MARK:- ViewController Methods
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        contentView.layer.cornerRadius = 6
        
        title_lbl.text = myTitle
        description_lbl.text = descriptionText
        ok_btn.setTitle(okBtnTitle, for: UIControl.State.normal)
        ok_btn.backgroundColor = UIColor.MBButtonBackgroundGrayColor
        
    }
    
    //MARK:- IBACTIONS
    @IBAction func YesBtnPressed(_ sender: UIButton) {
        self.dismiss(animated: true, completion:nil)
        self.okBtncompletionHandlerBlock?("")
    }
    
    //MARK:- Functions
    func setAlertWith(title:String = Localized("Title_Attention"), description:String?, btnTitle:String = Localized("BtnTitle_OK"), okBtnClickedBlock : @escaping MBButtonCompletionHandler = { _ in }) {
        
        myTitle = title
        descriptionText = description ?? ""
        okBtnTitle = btnTitle
        
        okBtncompletionHandlerBlock = okBtnClickedBlock
        
    }
}
