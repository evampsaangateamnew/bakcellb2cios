//
//  OperatorsCell.swift
//  Bakcell
//
//  Created by AbdulRehman Warraich on 1/2/19.
//  Copyright © 2019 evampsaanga. All rights reserved.
//

import UIKit

class OperatorsCell: UITableViewCell {

    @IBOutlet var lblName : UILabel!
    @IBOutlet var lblValue : UILabel!
    
    func setTypeLatout(_ aType: OperatorServiceType?) {
       
        if let aTypeObj = aType {
            lblName.text = aTypeObj.type
            
            if aTypeObj.unit?.isEqualToStringIgnoreCase(otherString: "manat") ?? false {
                lblValue.attributedText = MBUtilities.createAttributedTextWithManatSign(aTypeObj.value, textColor: UIColor.MBBarRed)
            } else {
                if aTypeObj.value?.isHasFreeText() ?? false {
                    lblValue.textColor = UIColor.MBBarRed
                } else {
                    lblValue.textColor = UIColor.MBTextGrayColor
                }
                
                lblValue.text = aTypeObj.value
            }
            
            
        } else {
            lblName.text = ""
            lblValue.text = ""
        }
    }

}
