//
//  TutorialVC.swift
//  Bakcell
//
//  Created by Saad Riaz on 8/8/17.
//  Copyright © 2017 evampsaanga. All rights reserved.
//

import UIKit
import KYDrawerController



let TUTS_ICONS:[String] = [Localized("logo_icon"),
                           Localized("1"),
                           Localized("2"),
                           Localized("3"),
                           Localized("4")]

let TUTS_TITLES:[String] = [Localized("Welcome"),
                            Localized("Explore"),
                            Localized("Manage"),
                            Localized("Top_up"),
                            Localized("Get_Started")]

let TUTS_DESC:[String] = [Localized("to_bakcell_app"),
                          Localized("our_latest_services"),
                          Localized("your_account_on"),
                          Localized("your_account_instantly"),
                          Localized("with_My_Bakcell_app")]

class TutorialVC: BaseVC, UIPageViewControllerDelegate, UIPageViewControllerDataSource {

    var pageContainer: UIPageViewController!
    @IBOutlet var pageControl:UIPageControl!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        //EventLog
        MBAnalyticsManager.logEvent(screenName: "Toturial Screens", contentType:"Next 1" , status:"Success" )
    
        // Create the page container
        pageContainer = UIPageViewController(transitionStyle: .scroll, navigationOrientation: .horizontal, options: nil)
        pageContainer.delegate = self
        pageContainer.dataSource = self
        pageContainer.setViewControllers([viewControllerAtIndex(index: 0)], direction: .forward, animated: true, completion: nil)

        // Add it to the view
        view.addSubview(pageContainer.view)

        // Configure our custom pageControl
        view.bringSubviewToFront(pageControl)
        pageControl.numberOfPages = TUTS_ICONS.count
        pageControl.currentPage = 0


    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.interactivePop(false)
        
        //EventLog
        MBAnalyticsManager.logEvent(screenName: "Tutorial Screen", contentType:"Tutorial Screen" , status:"Tutorial Screen" )
    }
    //MARK: - Utililty Methods
    
    private func viewControllerAtIndex(index:Int) -> UIViewController {
        
        let singleScreenVC = self.myStoryBoard.instantiateViewController(withIdentifier: "TutorialSingleScreenVC") as! TutorialSingleScreenVC
        singleScreenVC.delegate = self

        singleScreenVC.pageTitle = TUTS_TITLES[index]
        singleScreenVC.pageDescription = TUTS_DESC[index]
        singleScreenVC.currentPageNumber = index
        singleScreenVC.iconName = TUTS_ICONS[index]
        return singleScreenVC

    }
    

    //MARK: - UIPageViewControllerDatasource
    
    func pageViewController(_ pageViewController: UIPageViewController,
                            viewControllerBefore viewController: UIViewController) -> UIViewController? {
        
        if let vc = viewController as? TutorialSingleScreenVC {

            pageControl.currentPage = vc.currentPageNumber ?? 0

            if vc.currentPageNumber! == 0 {
                return nil
            }
            return viewControllerAtIndex(index:vc.currentPageNumber! - 1)
        }
        pageControl.currentPage = 0
        return viewControllerAtIndex(index: 0)
        
    }
    
    
    func pageViewController(_ pageViewController: UIPageViewController,
                            viewControllerAfter viewController: UIViewController) -> UIViewController? {
        
        if let vc = viewController as? TutorialSingleScreenVC {

            pageControl.currentPage = vc.currentPageNumber ?? 0

            if vc.currentPageNumber! == 4 {
                return nil
            }
            return viewControllerAtIndex(index:vc.currentPageNumber! + 1)
        }
        pageControl.currentPage = 0
        return viewControllerAtIndex(index: 0)
        
    }


    func presentationCount(for pageViewController: UIPageViewController) -> Int {
        return 5
    }

    func presentationIndex(for pageViewController: UIPageViewController) -> Int {
        return 0
    }

    
}


extension TutorialVC : TutorialSingleScreenDelegate {
    func navigatToLoginViewController() {

        if MBUserSession.shared.isLoggedIn() && MBUserSession.shared.loadUserInfomation() {

            let mainViewController   = self.myStoryBoard.instantiateViewController(withIdentifier: "TabbarController") as! TabbarController
            let drawerViewController = self.myStoryBoard.instantiateViewController(withIdentifier: "SideDrawerVC") as! SideDrawerVC
            let drawerController = KYDrawerController(drawerDirection: .left, drawerWidth: (UIScreen.main.bounds.width * 0.85))
            drawerController.mainViewController = mainViewController
            drawerController.drawerViewController = drawerViewController
            self.navigationController? .pushViewController(drawerController, animated: true)

        } else {

            let loginVC = self.myStoryBoard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
            self.navigationController?.pushViewController(loginVC, animated: true)
        }
    }
}
