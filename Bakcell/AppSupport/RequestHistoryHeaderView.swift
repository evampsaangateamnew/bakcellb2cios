//
//  RequestHistoryHeaderView.swift
//  Bakcell
//
//  Created by Saad Riaz on 6/10/17.
//  Copyright © 2017 evampsaanga. All rights reserved.
//

import UIKit


class RequestHistoryHeaderView: MBAccordionTableViewHeaderView {


    @IBOutlet var dateTimeLb: UILabel!
    @IBOutlet var statusLb: UILabel!
    @IBOutlet var paidLb: UILabel!
    @IBOutlet var remainingLb: UILabel!

static let identifier = "AccordionHeaderViewReuseIdentifier";
}
