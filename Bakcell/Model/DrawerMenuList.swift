//
//  DrawerMenuList.swift
//  Bakcell
//
//  Created by AbdulRehman Warraich on 8/24/17.
//  Copyright © 2017 evampsaanga. All rights reserved.
//

import Foundation
import ObjectMapper

class DrawerMenuList: Mappable {
    var identifier : String = ""
    var title : String = ""
    var sortOrder : String = ""
    var iconName : String = ""

    required init?(map: Map) {

    }

    func mapping(map: Map) {

        identifier      <- map["identifier"]
        title           <- map["title"]
        sortOrder       <- map["sortOrder"]
        iconName        <- map["iconName"]
    }
}
