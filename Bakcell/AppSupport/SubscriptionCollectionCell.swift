//
//  SubscriptionCollectionCell.swift
//  Bakcell
//
//  Created by Saad Riaz on 9/7/17.
//  Copyright © 2017 evampsaanga. All rights reserved.
//

import UIKit

class SubscriptionCollectionCell: UICollectionViewCell {
    
    @IBOutlet var tableView: MBAccordionTableView!
    
    //inAppSurvey related
    var delegate : SupplementaryOffersCellDelegate? = nil
    var offer: SubscriptionOffers?
    //Stars outlets....
    @IBOutlet weak var btnsStackView: UIStackView!
    @IBOutlet weak var btn1: UIButton!
    @IBOutlet weak var btn2: UIButton!
    @IBOutlet weak var btn3: UIButton!
    @IBOutlet weak var btn4: UIButton!
    @IBOutlet weak var btn5: UIButton!
    
    override func awakeFromNib() {

        // forCellReuseIdentifier
        tableView.register(UINib(nibName: "SupplementaryExpandCell", bundle: nil), forCellReuseIdentifier: "SupplementaryExpandCell")
        tableView.register(UINib(nibName: "PriceAndRoundingCell", bundle: nil), forCellReuseIdentifier: PriceAndRoundingCell.identifier)
        tableView.register(UINib(nibName: "TextDateTimeCell", bundle: nil), forCellReuseIdentifier: TextDateTimeCell.identifier)
        tableView.register(UINib(nibName: "TitleSubTitleValueAndDescCell", bundle: nil), forCellReuseIdentifier: TitleSubTitleValueAndDescCell.identifier)
        tableView.register(UINib(nibName: "FreeResourceValidityCell", bundle: nil), forCellReuseIdentifier: FreeResourceValidityCell.identifier)
        tableView.register(UINib(nibName: "RoamingDetailsCell", bundle: nil), forCellReuseIdentifier: RoamingDetailsCell.identifier)
        tableView.register(UINib(nibName: "SubscriptionUsageCell", bundle: nil), forCellReuseIdentifier: SubscriptionUsageCell.identifier)
        tableView.register(UINib(nibName: "AttributeListCell", bundle: nil), forCellReuseIdentifier: AttributeListCell.identifier)

        // ForHeaderFooterView
        tableView.register(UINib(nibName: "SupplementaryOffersHeaderView", bundle: nil), forHeaderFooterViewReuseIdentifier: SupplementaryOffersHeaderView.identifier)
        tableView.register(UINib(nibName: "SubscribeButtonView", bundle: nil), forHeaderFooterViewReuseIdentifier: SubscribeButtonView.identifier)

        tableView.separatorColor = UIColor.clear
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 180.0
        tableView.allowsSelection = false

        tableView.initialOpenSections = [0]

    }
    
    
    
    func setRating(with offer : SubscriptionOffers){
        self.offer = offer
        if  let  userSurvey = MBUserSession.shared.appSurvays?.userSurveys?.first(where: { $0.offeringId == offer.header?.offeringId ?? ""}){
            if let survey  = MBUserSession.shared.appSurvays?.surveys?.first(where: { $0.survayId  == userSurvey.surveryId }){
                if let  question =  survey.questions?.first(where: { $0.questionId == userSurvey.questionId }){
                    if let answer = question.answers?.firstIndex(where: { $0.answerId ==  userSurvey.answerId }) {
                        switch answer {
                        case 0:
                            print(answer)
                            btn1.setImage(UIImage.init(named: "RedFilledStar"), for: .normal)
                            btn2.setImage(UIImage.init(named: "RedLineStar"), for: .normal)
                            btn3.setImage(UIImage.init(named: "RedLineStar"), for: .normal)
                            btn4.setImage(UIImage.init(named: "RedLineStar"), for: .normal)
                            btn5.setImage(UIImage.init(named: "RedLineStar"), for: .normal)
                        case 1:
                            print(answer)
                            btn1.setImage(UIImage.init(named: "RedFilledStar"), for: .normal)
                            btn2.setImage(UIImage.init(named: "RedFilledStar"), for: .normal)
                            btn3.setImage(UIImage.init(named: "RedLineStar"), for: .normal)
                            btn4.setImage(UIImage.init(named: "RedLineStar"), for: .normal)
                            btn5.setImage(UIImage.init(named: "RedLineStar"), for: .normal)
                        case 2:
                            print(answer)
                            btn1.setImage(UIImage.init(named: "RedFilledStar"), for: .normal)
                            btn2.setImage(UIImage.init(named: "RedFilledStar"), for: .normal)
                            btn3.setImage(UIImage.init(named: "RedFilledStar"), for: .normal)
                            btn4.setImage(UIImage.init(named: "RedLineStar"), for: .normal)
                            btn5.setImage(UIImage.init(named: "RedLineStar"), for: .normal)
                        case 3:
                            print(answer)
                            btn1.setImage(UIImage.init(named: "RedFilledStar"), for: .normal)
                            btn2.setImage(UIImage.init(named: "RedFilledStar"), for: .normal)
                            btn3.setImage(UIImage.init(named: "RedFilledStar"), for: .normal)
                            btn4.setImage(UIImage.init(named: "RedFilledStar"), for: .normal)
                            btn5.setImage(UIImage.init(named: "RedLineStar"), for: .normal)
                        case 4:
                            print(answer)
                            btn1.setImage(UIImage.init(named: "RedFilledStar"), for: .normal)
                            btn2.setImage(UIImage.init(named: "RedFilledStar"), for: .normal)
                            btn3.setImage(UIImage.init(named: "RedFilledStar"), for: .normal)
                            btn4.setImage(UIImage.init(named: "RedFilledStar"), for: .normal)
                            btn5.setImage(UIImage.init(named: "RedFilledStar"), for: .normal)
                        default:
                            break
                        }
                        
                    }
                }
            }
        }else{
            btn1.setImage(UIImage.init(named: "RedLineStar"), for: .normal)
            btn2.setImage(UIImage.init(named: "RedLineStar"), for: .normal)
            btn3.setImage(UIImage.init(named: "RedLineStar"), for: .normal)
            btn4.setImage(UIImage.init(named: "RedLineStar"), for: .normal)
            btn5.setImage(UIImage.init(named: "RedLineStar"), for: .normal)
        }
        
        
        
        
        
    }
    
    
    @IBAction func tabStar(_ sender: UIButton){
        if  let ofr  = self.offer?.header?.offeringId {
            self.delegate?.starTapped(offerId: ofr)
        }
    }
    
}
