//
//  UITextField+MB.swift
//  Bakcell
//
//  Created by AbdulRehman Warraich on 9/18/17.
//  Copyright © 2017 evampsaanga. All rights reserved.
//

import Foundation
import UIKit

extension UITextField {
    
    func isValidMSISDN() -> Bool {
        
        if let text = self.text,
            text.length == 9,
            text.isNumeric {
            
            return true
        } else {
            return false
        }
    }
    
    func isValidCardNumber() -> Bool {
        
        if let text = self.text,
            text.length == 15,
            text.isNumeric {
            
            return true
        } else {
            return false
        }
    }
    
    func plainText() -> String {
        
        if let text = self.text {
            
            let charsToRemove: Set = Set("-")
            let newNumberCharacters = String( text.filter { !charsToRemove.contains($0) })
            
            if newNumberCharacters.isBlank {
                return ""
            } else {
                return newNumberCharacters
            }
        }
        
        return ""
    }
    
}
