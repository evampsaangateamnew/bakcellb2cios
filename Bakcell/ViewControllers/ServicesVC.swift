//
//  ServicesVC.swift
//  Bakcell
//
//  Created by Saad Riaz on 6/21/17.
//  Copyright © 2017 evampsaanga. All rights reserved.
//

import UIKit

class ServicesVC: BaseVC,UITableViewDataSource,UITableViewDelegate {
    
    var titleArr: [String] = [Localized("Title_SupplementaryOffers"),
                              Localized("Title_QuickServices")]
    var images: [String] = ["Supplementry-offers",
                            "Quick-services"]
    
    // MARK: - IBOutlet
    @IBOutlet var servicesTitle_lbl: UILabel!
    @IBOutlet var tableView: UITableView!
    
    // MARK: - View Controllers Method
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.interactivePop(false)
        
        loadViewLayout()
        
        /* Reset service menu */
        titleArr = [Localized("Title_SupplementaryOffers"), Localized("Title_QuickServices")]
        images = ["Supplementry-offers", "Quick-services"]
        
        if MBUserSession.shared.shouldHideCoreServices() == false {
            titleArr.append(Localized("Title_CoreServices"))
            images.append("Core-services")
        }
        
        if MBUserSession.shared.predefineData?.fnfAllowed.isStringTrue() == true {
            titleArr.append(Localized("Title_FriendsAndFamily"))
            images.append("FF")
        }
        
        if MBUserSession.shared.subscriberType() == MBSubscriberType.PostPaid {
            titleArr.append(Localized("Title_TopUP"))
            titleArr.append(Localized("fastTopup_Title"))
            titleArr.append(Localized("autoPayment_Title"))
//            images.append("Fast Top up-1")
        } else {
            if MBUserSession.shared.brandName() == MBBrandName.Klass{
                titleArr.append(Localized("Title_TopUP"))
                titleArr.append(Localized("fastTopup_Title"))
                titleArr.append(Localized("autoPayment_Title"))
                titleArr.append(Localized("Op2_MoneyTransfer"))
                titleArr.append(Localized("Loan_TitleKlass"))
            }
            else{
                titleArr.append(Localized("Title_TopUP"))
                titleArr.append(Localized("fastTopup_Title"))
                titleArr.append(Localized("autoPayment_Title"))
                titleArr.append(Localized("Op2_MoneyTransfer"))
                titleArr.append(Localized("Loan_TitleCin"))
            }
        }
//        images.append("FF")
        images.append("Top up")
        images.append("Fast Top up")
//        images.append("Sub_top_up")
        images.append("Fast Top up-1")
        images.append("MoneyTransfer")
        images.append("loan")
        
        tableView.reloadData()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if let supplementaryOffersHandler : SupplementaryOfferings = SupplementaryOfferings.loadFromUserDefaults(key: APIsType.supplementaryOffer.selectedLocalizedAPIKey()){
            print("offer found:\(supplementaryOffersHandler.roaming?.offers ?? [Offers]())")
            if supplementaryOffersHandler.roaming?.offers?.count == 0 {
                getSupplementaryOfferingsMain()
            }
        } else {
            getSupplementaryOfferingsMain()
        }
    }
    
    //MARK: - FUNCTIONS
    func loadViewLayout(){
        servicesTitle_lbl.font = UIFont.MBArial(fontSize: 20)
        servicesTitle_lbl.text = Localized("Services_Title")
    }
    
    //MARK: TABLE VIEW METHODS
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return titleArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellID", for: indexPath) as! AccountsCell
        cell.title_lbl.font = UIFont.MBArial(fontSize: 14)
        cell.title_lbl.text = titleArr[indexPath.row]
        cell.img.image = UIImage (named: images[indexPath.row])
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if titleArr[indexPath.row] == (Localized("fastTopup_Title")){
            let topUp = self.myStoryBoard2.instantiateViewController(withIdentifier: "FastTopupVC") as! FastTopupVC
            self.navigationController?.pushViewController(topUp, animated: true)
            return
        } else if titleArr[indexPath.row] == (Localized("Title_TopUP")) {
            let topUp = self.myStoryBoard.instantiateViewController(withIdentifier: "TopUpVC") as! TopUpVC
            self.navigationController?.pushViewController(topUp, animated: true)
            return
        } else if titleArr[indexPath.row] == (Localized("autoPayment_Title")) {
            let autoPayment = self.myStoryBoard2.instantiateViewController(withIdentifier: AutoPaymentVC.identifier) as! AutoPaymentVC
            self.navigationController?.pushViewController(autoPayment, animated: true)
            return
        
            
        }else if titleArr[indexPath.row] == (Localized("Op2_MoneyTransfer")) {
            let money = self.myStoryBoard.instantiateViewController(withIdentifier: "MoneyTransferVC") as! MoneyTransferVC
            self.navigationController?.pushViewController(money, animated: true)
            return
        } else if titleArr[indexPath.row] == (Localized("Loan_TitleKlass")) ||
                    titleArr[indexPath.row] == (Localized("Loan_TitleCin")) {
            let loan = self.myStoryBoard.instantiateViewController(withIdentifier: "LoanVC") as! LoanVC
            self.navigationController?.pushViewController(loan, animated: true)
            return
        }
        switch indexPath.row {
        case 0:
            let subscriptions = self.myStoryBoard.instantiateViewController(withIdentifier: "SupplementaryVC") as! SupplementaryVC
            self.navigationController?.pushViewController(subscriptions, animated: true)
        case 1:
            let usage = self.myStoryBoard.instantiateViewController(withIdentifier: "FreeSMSVC") as! FreeSMSVC
            self.navigationController?.pushViewController(usage, animated: true)
        case 2:
            let operations = self.myStoryBoard.instantiateViewController(withIdentifier: "CoreServicesVC") as! CoreServicesVC
            self.navigationController?.pushViewController(operations, animated: true)
        case 3:
            let ff = self.myStoryBoard.instantiateViewController(withIdentifier: "FandFVC") as! FandFVC
            self.navigationController?.pushViewController(ff, animated: true)
        
        default:
            break
        }
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
}
