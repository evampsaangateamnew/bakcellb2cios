//
//  String+MB.swift
//  Bakcell
//
//  Created by AbdulRehman Warraich on 8/17/17.
//  Copyright © 2017 evampsaanga. All rights reserved.
//

import Foundation
import UIKit

extension String {
    
    static func appVersion() -> String {
        let dictionary = Bundle.main.infoDictionary!
        if let version : String = dictionary["CFBundleShortVersionString"] as? String {
            return version
        } else {
            return ""
        }
    }
    
    var length: Int {
        return self.count
    }
    
    var lengthWithOutSpace: Int {
        let newString = self.trimmingCharacters(in: .whitespacesAndNewlines)
        return newString.count
    }
    
    var trimmWhiteSpace: String {
        return self.trimmingCharacters(in: .whitespacesAndNewlines)
    }
    
    var toInt: Int {
        
        if self != "" {
            
            let newString = self.trimmWhiteSpace
            
            if let cValue = Int(newString) {
                return cValue
            } else {
                return 0
            }
        } else {
            return 0
        }
    }
    
    var toDouble: Double {
        
        if self != "" {
            
            let newString = self.trimmWhiteSpace
            // Try to convert into double
            if let cValue = Double(newString) {
                return cValue
            } else {
                return 0
            }
        } else {
            return 0.0
        }
    }
    
    var floatValue: Float {
        return (self as NSString).floatValue
    }
    
    func isStringAnNumber() -> Bool {
        
        let valueString = self.trimmWhiteSpace
        //First try to convert into Int
        if let _ = Int(valueString) {
            
            return true
            
        } else {
            
            // If cannot convert to Int then try to convert in Double
            if let _ = Double(valueString) {
                return true
                
            } else {
                return false
            }
        }
        
    }
    //To check text field or String is blank or not
    var isBlank: Bool {
        get {
            let trimmed = trimmingCharacters(in: CharacterSet.whitespaces)
            return trimmed.isEmpty
        }
    }
    
    // Check that is string contains only numbers or not
    var isNumeric: Bool {
        return !isEmpty && range(of: "[^0-9]", options: .regularExpression) == nil
    }
    var isAlphanumeric: Bool {
        return !isEmpty && range(of: "[^a-zA-Z0-9]", options: .regularExpression) == nil
    }
    
    func isStringTrue() -> Bool {
        
        if self.isBlank {
            return false
        }
        
        if self.lowercased() == "true" {
            return true
        } else {
            return false
        }
    }
    
    
    func isButtonEnabled() -> Bool {
        
        if self.isBlank {
            return false
        }
        
        if self == "1" {
            return true
        } else {
            return false
        }
    }
    
    
    func removeNullValues() -> String {
        
        if self.isBlank {
            return ""
        }
        
        if self.lowercased() == "null" || self.lowercased() == "nil" {
            return ""
        } else {
            return self
        }
    }
    
    // Check that the string text is a valid email type
    func isValidEmail() -> Bool {
        
        if self.isBlank {
            return false
        }
        let emailFormat = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailPredicate = NSPredicate(format:"SELF MATCHES %@", emailFormat)
        
        let result =  emailPredicate.evaluate(with: self)
        
        return result
        
    }
    
    // Return first character of string
    var firstCharacter : String {
        let inputString : [String] = self.map { String($0) }
        return inputString.first!
    }
    
    
    var urlEncode : String {
        if self.length <= 0 {
            return ""
        }
        let escapedString : String = self.addingPercentEncoding( withAllowedCharacters: NSCharacterSet.urlQueryAllowed)!
        return escapedString
    }
    
    var urlDecode : String {
        if self.length <= 0 {
            return ""
        }
        let decodeString : String = self.removingPercentEncoding!
        return decodeString
    }
    
    var removeHyphen : String {
        return self.replacingOccurrences(of: "-", with: "")
    }
    
    var replaceSpaceWithHyphen : String {
        return self.replacingOccurrences(of: " ", with: "_")
    }
    
    func fromBase64() -> String
    {
        let data = Data(base64Encoded: self, options: NSData.Base64DecodingOptions(rawValue: 0))
        return String(data: data!, encoding: String.Encoding.utf8)!
    }
    
    func toBase64() -> String
    {
        let data = self.data(using: String.Encoding.utf8)
        return data!.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))
    }
    
    func isHasFreeOrUnlimitedText() -> Bool {
        
        if self.isBlank {
            return false
        }
        if self.isHasFreeText() ||
            self.isEqualToStringIgnoreCase(otherString: "Unlimited") ||
            self.isEqualToStringIgnoreCase(otherString: "Безлимитный") ||
            self.isEqualToStringIgnoreCase(otherString: "Limitsiz") {
            
            return true
        } else {
            return false
        }
        
    }
    
    func isHasFreeText() -> Bool {
        
        if self.isBlank {
            return false
        }
        if self.isEqualToStringIgnoreCase(otherString: "Free") ||
            self.isEqualToStringIgnoreCase(otherString: "БЕСПЛАТНО") ||
            self.isEqualToStringIgnoreCase(otherString: "PULSUZ") {
            
            return true
        } else {
            return false
        }
        
    }
    
    //    " Get " Translation in  Russian   "Предоставляется"
    //    " Get " Translation in  Azeri       " Təqdim edilir "
    //
    func isHasGetText() -> Bool {
        
        if self.isBlank {
            return false
        }
        if self.isEqualToStringIgnoreCase(otherString: "Get") ||
            self.isEqualToStringIgnoreCase(otherString: "Предоставляется") ||
            self.isEqualToStringIgnoreCase(otherString: "Təqdim edilir ") {
            
            return true
        } else {
            return false
        }
        
    }
    
    func isTextActive() -> Bool {
        
        if self.isBlank {
            return false
        }
        return self.isEqualToStringIgnoreCase(otherString: "Active")
    }
    
    func isSuspended() -> Bool {
        
        if self.isBlank {
            return false
        }
        return self.isEqualToStringIgnoreCase(otherString: "Suspended")
    }
    
    // convert images into base64 and keep them into string
    
    public func convertBase64ToImage(base64String: String) -> UIImage {
        
        let decodedData = Data(base64Encoded: base64String, options: NSData.Base64DecodingOptions(rawValue: 0))
        
        let decodedimage = UIImage(data: decodedData ?? Data()) ?? UIImage()
        
        return decodedimage
        
    }// end convertBase64ToImage
    
    func charactorAtIndex(IndexPosition indexPosition: Int) -> Character {
        return self[index(startIndex, offsetBy: indexPosition)]
    }
    
    static func checkConsuctiveCharacters(Password password:String?) -> Bool {
        
        if let passwordString = password {
            
            if passwordString.length > 0 {
                var lastChar : Character = passwordString.charactorAtIndex(IndexPosition: 1)
                for aChar in passwordString {
                    if aChar == lastChar{
                        return true
                    }
                    lastChar = aChar
                }
            }
        }
        return false
    }
    
    func containsSubString(subString : String?) -> Bool {
        
        let mainLowerString : String = self.lowercased()
        
        if let subString = subString {
            let subLowerString = subString.lowercased()
            
            return mainLowerString.contains(subLowerString)
            
        } else {
            return false
        }
    }
    
    func isEqualToStringIgnoreCase(otherString : String?) -> Bool {
        
        // Check that if self is Empty
        if self.isBlank{
            return false
        }
        
        // Check that if self is Empty
        if otherString?.isBlank ?? true {
            return false
        }
        
        let mainLowerString : String = self.lowercased()
        
        if let otherString = otherString {
            let otherLowerString = otherString.lowercased()
            
            return mainLowerString == otherLowerString
            
        } else {
            return false
        }
    }
    
    func offSetFirstValue() -> String {
        
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss.SSS"
        //        "2017-10-03 12:00:00.0"
        formatter.calendar = Calendar(identifier: Calendar.Identifier.iso8601)
        //    formatter.locale = Locale(identifier: "PST")
        
        if let date = formatter.date(from: self) {
            
            var calender = Calendar.current
            switch MBLanguageManager.userSelectedLanguage() {
                
            case .Russian:
                calender.locale = Locale(identifier: "ru")
            case .English:
                calender.locale = Locale(identifier: "en")
            case .Azeri:
                calender.locale = Locale(identifier: "az_AZ")
            }
            
            let dateComponentsFormatter = DateComponentsFormatter()
            dateComponentsFormatter.calendar = calender
            dateComponentsFormatter.allowedUnits = [.year,.month,.day,.hour,.minute,.second]
            dateComponentsFormatter.unitsStyle = .short
            dateComponentsFormatter.collapsesLargestUnit = false
            let componetArray : [String] = (dateComponentsFormatter.string(from: date, to: Date()) ?? "").components(separatedBy: ",")
            
            //  print("\n\n\n\(dateComponentsFormatter.string(from: date, to: Date()) ?? "") \n\n")
            //  “9h 41m 30s”
            
            return componetArray.first ?? ""
            
        } else {
            return ""
        }
    }
    
    func containsAlphbeats() -> Bool {
        
        // Check that if self is Empty
        if self.isBlank{
            return false
        }
        
        let allowedCharacters = CharacterSet(charactersIn: Constants.allowedAlphbeats)
        //        let characterSet = CharacterSet(charactersIn: self)
        
        if self.rangeOfCharacter(from: allowedCharacters) != nil {
            return true
        } else {
            return false
        }
    }
    
    func constainsSmallAlphabets() -> Bool{
        // Check that if self is Empty
        if self.isBlank{
            return false
        }
        
        let allowedCharacters = CharacterSet(charactersIn: Constants.allowedSmallAlphabets)
        if self.rangeOfCharacter(from: allowedCharacters) != nil {
            return true
        }else{
            return false
        }
        
    }
    
    func constainsCapitalAlphabets() -> Bool{
        // Check that if self is Empty
        if self.isBlank{
            return false
        }
        let allowedCharacters = CharacterSet(charactersIn: Constants.allowedCapitalAlphabets)
        if  self.rangeOfCharacter(from: allowedCharacters) != nil {
            return true
        }else{
            return false
        }
        
    }
    
    func containsDigits() -> Bool {
        
        // Check that if self is Empty
        if self.isBlank{
            return false
        }
        
        let allowedCharacters = CharacterSet(charactersIn: Constants.allowedNumbers)
        //        let characterSet = CharacterSet(charactersIn: self)
        
        //        if allowedCharacters.isSuperset(of: characterSet) {
        if self.rangeOfCharacter(from: allowedCharacters) != nil {
            return true
        } else {
            return false
        }
    }
    
    func containsSpecialCharacters() -> Bool {
        
        // Check that if self is Empty
        if self.isBlank{
            return false
        }
        
        let disallowedCharacters = CharacterSet(charactersIn: Constants.allowedSpecialCharacters)
        //        let characterSet = CharacterSet(charactersIn: self)
        
        //        if disallowedCharacters.isSuperset(of: characterSet) {
        if self.rangeOfCharacter(from: disallowedCharacters) != nil {
            return true
        } else {
            return false
        }
    }
    
    func containsOtherThenAllowedCharactersForFreeSMSInEnglish() -> Bool {
        
        // Check that if self is Empty
        if self.isBlank{
            return false
        }
        
        // Allow charactors check
        let allowedCharactors = Constants.allowedAlphbeats + Constants.allowedNumbers + Constants.allowedSpecialCharacters + " \n"
        let allowedCharacters = CharacterSet(charactersIn: allowedCharactors)
        let characterSet = CharacterSet(charactersIn: self)
        
        if allowedCharacters.isSuperset(of: characterSet) == true {
            return false
        } else {
            return true
        }
    }
    
    
    //    func containDummyNameValues() -> Bool {
    //
    //        if self.isBlank {
    //            return false
    //        }
    //        if self.isEqualToStringIgnoreCase(otherString: "No") ||
    //            self.isEqualToStringIgnoreCase(otherString: "Name") ||
    //            self.isEqualToStringIgnoreCase(otherString: "No Name") ||
    //            self.isEqualToStringIgnoreCase(otherString: "first") ||
    //            self.isEqualToStringIgnoreCase(otherString: "last") ||
    //            self.isEqualToStringIgnoreCase(otherString: "first name") ||
    //            self.isEqualToStringIgnoreCase(otherString: "last name") ||
    //            self.isEqualToStringIgnoreCase(otherString: "first name last name") ||
    //            self.isEqualToStringIgnoreCase(otherString: "Null") ||
    //            self.isEqualToStringIgnoreCase(otherString: "Any digit") ||
    //            self.isEqualToStringIgnoreCase(otherString: "Russian letters") ||
    //            self.isCyrillic {
    //
    //            return true
    //        } else {
    //            return false
    //        }
    //
    //    }
    //
    //    var isCyrillic: Bool {
    //        let upper = "АБВГДЕЖЗИЙКЛМНОПРСТУФХЦЧШЩЬЮЯ"
    //        let lower = "абвгдежзийклмнопрстуфхцчшщьюя"
    //
    //        for c in self.map({ String($0) }) {
    //            if upper.contains(c) == true ||
    //                lower.contains(c) == true {
    //                return true
    //            }
    //        }
    //        return false
    //    }
    
    func isValidMSISDN() -> Bool {
        
        if self.length == 9 {
            
            if self.isNumeric {
                return true
                
            } else {
                return false
            }
            
        } else {
            return false
        }
    }
    
    func isValidCardNumber() -> Bool {
        
        if self.length == 15 {
            
            if self.isNumeric {
                return true
                
            } else {
                return false
            }
            
        } else {
            return false
        }
    }
    
    /// Check whether a string has 'Free' or 'Unlimited' text.
    ///  - returns: Bool.
    
    func isGroupPayBySUBs() -> Bool {
        
        if self.isBlank {
            return false
        }
        if self.isEqualToStringIgnoreCase(otherString: Constants.K_PayBySUBs) ||
            self.isEqualToStringIgnoreCase(otherString: Constants.K_PayBySUB){
            
            return true
        } else {
            return false
        }
        
    }
    
    func alphabetAtindex(index : Int) -> Character {
        let arrayAlphabets = Array("abcdefghijklmnopqrstuvwxyz")
        return arrayAlphabets[index]

    }
    
    func localizedAPIKey() -> String {
        
        switch MBLanguageManager.userSelectedLanguage() {
        case .English:
            return self + "_Englist"
        case .Azeri:
            return self + "_Azeri"
        case .Russian:
            return self + "_Russian"
        }
    }
    
    func currentUserKey() -> String {
        return "\(self)_\(MBUserSession.shared.msisdn)"
    }
}
