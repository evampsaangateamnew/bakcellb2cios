//
//  AccordionHeaderView1.swift
//  Bakcell
//
//  Created by Saad Riaz on 5/26/17.
//  Copyright © 2017 evampsaanga. All rights reserved.
//

import UIKit


class AccordionHeaderView1: MBAccordionTableViewHeaderView {

    static let identifier = "AccordionHeaderViewReuseIdentifier1";
    static let kDefaultAccordionHeaderViewHeight: CGFloat = 44.0;

    @IBOutlet var lineView: UIView!
    @IBOutlet var plus_img: UIImageView!
    @IBOutlet var question_Lbl: UILabel!

}
