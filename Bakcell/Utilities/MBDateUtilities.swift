//
//  MBDateUtilities.swift
//  Bakcell
//
//  Created by AbdulRehman Warraich on 1/2/19.
//  Copyright © 2019 evampsaanga. All rights reserved.
//

import Foundation

class MBDateUtilities {
    
    /**
     Returns fromated date String.
     
     - parameter dateString: Date String.
     - parameter dateFormate: Date Formate.
     
     - returns: FormattedDate(String)
     */
    class func displayFormatedDateString(dateString: String, dateFormate:String = Constants.kHomeDateFormate, returnDateFormate:String = Constants.kDisplayFormat) -> String {
        
        if let myDate  = MBUserSession.shared.dateFormatter.createDate(from: dateString, dateFormate: dateFormate) {
            
            let myDateString  = MBUserSession.shared.dateFormatter.createString(from: myDate, dateFormate: returnDateFormate)
            
            return myDateString
            
        } else {
            return ""
        }
    }
    
    /**
     Returns fromated date String with addition of days.
     
     - parameter dateString: Date String.
     - parameter dateFormate: Date Formate.
     - parameter additionalValue: Number of days need to be added in date.
     - parameter returnDateFormate: Return date format.
     
     - returns: FormattedDate(String)
     */
    class func displayFormatedDateString(from dateString: String, additionalValue: Int, dateFormate:String = Constants.kHomeDateFormate, returnDateFormate:String = Constants.kDisplayFormat) -> String {
        
        if let myDate  = MBUserSession.shared.dateFormatter.createDate(from: dateString, dateFormate: dateFormate) {
            
            // ADD/Subtract value from date
            if let newDate = Calendar.current.date(byAdding: .day, value: additionalValue, to: myDate) {
                let myDateString  = MBUserSession.shared.dateFormatter.createString(from: newDate, dateFormate: returnDateFormate)
                
                return myDateString
                
            } else {
                return ""
            }
            
        } else {
            return ""
        }
    }
    
    /**
     Returns total hours between two dates.
     
     - parameter startingDate: Start date.
     - parameter endingDate: End date.
     - parameter dateFormate: Date format.
     
     - returns: Hours(Int)
     */
    class func calculateTotalHoursBetweenTwoDates(startingDate: String, endingDate: String, dateFormate: String = Constants.kDisplayFormat ) -> Int {
        
        let startDate  = MBUserSession.shared.dateFormatter.createDate(from: startingDate, dateFormate: dateFormate)
        let expireDate = MBUserSession.shared.dateFormatter.createDate(from: endingDate, dateFormate: dateFormate)
        
        
        if startDate != nil && expireDate != nil {
            
            return calculateHoursBetweenTwoDates(startingDate: startDate!.millisecondsSince1970, endingDate: expireDate!.millisecondsSince1970)
            
        } else {
            return 0
        }
    }
    
    /**
     Returns total hours between two dates.
     
     - parameter startingDate: Start date.
     - parameter endingDate: End date.
     
     - returns: Hours(Int)
     */
    class func calculateHoursBetweenTwoDates(startingDate: Double, endingDate: Double) -> Int {
        
        
        let newStartDate = Date(milliseconds: startingDate)
        let newEndDate = Date(milliseconds: endingDate)
        
        let minute = Calendar.current.dateComponents([.minute], from: newStartDate, to: newEndDate).minute ?? 0
        
        if minute >= 60 {
            return Int((minute / 60))
        }
        
        return 0
    }
    
    /**
     Returns total days between two dates.
     
     - parameter startingDate: Start date.
     - parameter endingDate: End date.
     
     - returns: Days(Int)
     */
    class func calculateDaysBetweenTwoDates(startingDate: Double, endingDate: Double) -> Int {
        
        let newStartDate = Date(milliseconds: startingDate)
        let newEndDate = Date(milliseconds: endingDate)
        
        
        if newStartDate == newEndDate ||
            newStartDate > newEndDate {
            
            return 0
        } else {
            var currentCalendar = Calendar.current
            currentCalendar.timeZone = TimeZone.appTimeZone()
            
            return Calendar.current.dateComponents([.day], from: newStartDate, to: newEndDate).day ?? 0
        }
    }
    
    /**
     Returns total days between two dates.
     
     - parameter startingDate: Start date.
     - parameter endingDate: End date.
     - parameter dateFormate: Date format.
     
     - returns: Days(Int)
     */
    class func calculateTotalDaysBetween(startingDate: String, endingDate: String, dateFormate: String = Constants.kDisplayFormat ) -> Int {
        let startDate  = MBUserSession.shared.dateFormatter.createDate(from: startingDate, dateFormate: dateFormate)
        let expireDate = MBUserSession.shared.dateFormatter.createDate(from: endingDate, dateFormate: dateFormate)
        if startDate != nil && expireDate != nil {
            
            return calculateDaysBetweenTwoDates(startingDate: startDate!.millisecondsSince1970, endingDate: expireDate!.millisecondsSince1970)
            
        } else {
            return 0
        }
    }
    
    /**
     Returns total days between two dates.
     
     - parameter startingDate: Start date.
     - parameter endingDate: End date.
     - parameter dateFormate: Date format.
     
     - returns: Days(Int)
     */
    class func calculateDaysLeft(startingDate: String, endingDate: String, dateFormate: String = Constants.kHomeDateFormate ) -> Int {
        
        let startDate  = MBUserSession.shared.dateFormatter.createDate(from: startingDate, dateFormate: dateFormate)
        let expireDate = MBUserSession.shared.dateFormatter.createDate(from: endingDate, dateFormate: dateFormate)
        
        
        if startDate != nil && expireDate != nil {
            
            return calculateDaysLeft(startingDate: startDate!.millisecondsSince1970, endingDate: expireDate!.millisecondsSince1970)
            
        } else {
            return 0
        }
    }
    
    /**
     Returns total days between two dates.
     
     - parameter startingDate: Start date.
     - parameter endingDate: End date.
     
     - returns: Days(Int)
     */
    class func calculateDaysLeft(startingDate: Double, endingDate: Double) -> Int {
        
        let totalDays =  calculateDaysBetweenTwoDates(startingDate: startingDate, endingDate: endingDate)
        
        let usedDays =  calculateDaysBetweenTwoDates(startingDate: startingDate, endingDate: Date().todayDate().millisecondsSince1970)
        
        let daysLeft : Int = totalDays - usedDays
        
        if daysLeft < 0 {
            return 0
        } else {
            return daysLeft
        }
    }
}
