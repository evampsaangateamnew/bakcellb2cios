//
//  UlduzumUsageHistoryVC.swift
//  Bakcell
//
//  Created by Muhammad Waqas on 1/2/19.
//  Copyright © 2019 evampsaanga. All rights reserved.
//

import UIKit
import ObjectMapper

class UlduzumUsageHistoryVC: BaseVC {
    
    //MARK:- Properties
    var isStartDate : Bool?
    var filteredHistoryData : [UlduzumUsageHistory]?
    
    //Uidate picker
    let datePicker = UIDatePicker()
    let myView = UIView()
    var selectedSectionIndexs: [Int] = []
    
    //MARK:- IBOutlet
    @IBOutlet var titleLable: UILabel!
    @IBOutlet var dateDropDown: UIDropDown!
    @IBOutlet var tableView: UITableView!
    
    @IBOutlet var spacificBtnView: UIView!
    @IBOutlet var endDateBtn: UIButton!
    @IBOutlet var startDateBtn: UIButton!
    @IBOutlet var startDateLb: UILabel!
    @IBOutlet var endDateLb: UILabel!
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        titleLable.text = Localized("Title_UlduzumUsageHistory")
        spacificBtnView.isHidden = true
        
        //init tableview
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(UINib(nibName: "UlduzumUSTbleViewCell", bundle: nil), forCellReuseIdentifier: "UlduzumUHCell")
        tableView.allowsSelection = false
        self.reloadTableViewData()
        
        //init dateDropDown
        let todayDateString = MBUtilities.todayDateString(dateFormate: Constants.kAPIFormat)
        self.initDateDropDown(todayDateString: todayDateString)
        
        //initial/default call to get history call from server on screen load
        getUsageHistoryAPICall(StartDate: todayDateString, EndDate: todayDateString)
    }
    
    
    // MARK: - IBAction
    
    @IBAction func startBtnAction(_ sender: UIButton) {
        
        isStartDate = true;
        
        let todayDate = MBUtilities.todayDate()
        MBUserSession.shared.dateFormatter.dateFormat = Constants.kDisplayFormat
        
        let threeMonthsAgos = MBUtilities.todayDate().getMonth(WithAdditionalValue: -3)?.startOfMonth() ?? todayDate
        
        if let endDateString = endDateLb.text,
            endDateString.isBlank == false {
            
            self.showDatePicker(minDate: threeMonthsAgos, maxDate: MBUserSession.shared.dateFormatter.date(from: endDateString) ?? todayDate)
            
        } else {
            self.showDatePicker(minDate: threeMonthsAgos, maxDate: todayDate)
        }
        
    }
    
    @IBAction func endBtnAction(_ sender: UIButton) {
        
        isStartDate = false;
        
        let todayDate = MBUtilities.todayDate(dateFormate: Constants.kDisplayFormat)
        
        MBUserSession.shared.dateFormatter.dateFormat = Constants.kDisplayFormat
        
        self.showDatePicker(minDate: MBUserSession.shared.dateFormatter.date(from: startDateLb.text ?? "") ?? todayDate, maxDate: todayDate)
    }
    
    //MARK: - FUNCTIONS
    
    func initDateDropDown(todayDateString : String) {
        
        //Drop Down
        dateDropDown.optionsTextAlignment = NSTextAlignment.center
        dateDropDown.textAlignment = NSTextAlignment.center
        dateDropDown.placeholder = Localized("DropDown_CurrentDay")
        self.dateDropDown.rowBackgroundColor = UIColor.clear
        self.dateDropDown.borderWidth = 0
        self.dateDropDown.tableHeight = 176
        
        dateDropDown.optionsSize = 14
        dateDropDown.fontSize = 12
        
        dateDropDown.options = [Localized("DropDown_CurrentDay"),Localized("DropDown_Last7Days"), Localized("DropDown_Last30Days"), Localized("DropDown_PreviousMonth"),Localized("DropDown_CustomPeriod")]
        
        dateDropDown.didSelect { (option, index) in
            
            
            switch index{
            case 0:
                print("default")
                self.spacificBtnView.isHidden = true
                
                self.getUsageHistoryAPICall(StartDate: todayDateString, EndDate: todayDateString)
                
            case 1:
                print("Last 7 days")
                self.spacificBtnView.isHidden = true
                
                // Get data of previous seven days
                let sevenDaysAgo = MBUtilities.todayDateString(withAdditionalValue: -7)
                self.getUsageHistoryAPICall(StartDate: sevenDaysAgo, EndDate: todayDateString)
                
                
            case 2:
                print("Last 30 days")
                self.spacificBtnView.isHidden = true
                
                // Get data of previous thirty days
                let thirtyDaysAgo = MBUtilities.todayDateString(withAdditionalValue: -30)
                self.getUsageHistoryAPICall(StartDate: thirtyDaysAgo, EndDate: todayDateString)
                
            case 3:
                print("Previous month")
                self.spacificBtnView.isHidden = true
                
                // Getting previous month start and end date
                let startOfPreviousMonth = MBUtilities.todayDate().getPreviousMonth()?.startOfMonth() ?? MBUtilities.todayDate()
                let endOfPreviousMonth = MBUtilities.todayDate().getPreviousMonth()?.endOfMonth() ?? MBUtilities.todayDate()
                
                MBUserSession.shared.dateFormatter.dateFormat = Constants.kAPIFormat
                
                let startDateString = MBUserSession.shared.dateFormatter.string(from: startOfPreviousMonth)
                let endDateString = MBUserSession.shared.dateFormatter.string(from: endOfPreviousMonth)
                
                self.getUsageHistoryAPICall(StartDate: startDateString, EndDate: endDateString )
                
            case 4:
                self.spacificBtnView.isHidden = false
                
                //For date formate
                self.startDateLb.text = MBUtilities.todayDateString(dateFormate: Constants.kDisplayFormat)
                self.endDateLb.text = MBUtilities.todayDateString(dateFormate: Constants.kDisplayFormat)
                
                self.startDateLb.textColor = UIColor.MBDarkGrayColor
                self.endDateLb.textColor = UIColor.MBDarkGrayColor
                
                self.getUsageHistoryAPICall(StartDate: todayDateString, EndDate: todayDateString)
                
                print("this Month Days")
                
            default:
                print("destructive")
                
            }
        }
    }
    
    func reloadTableViewData() {
        if self.filteredHistoryData?.count == 0 {
            self.tableView.showDescriptionViewWithImage(description: Localized("Message_NoDataAvalible"))
            
        } else {
            self.tableView.hideDescriptionView()
        }
        self.tableView.reloadData()
    }
    
    //MARK: - DatePicker Methods
    func showDatePicker(minDate : Date , maxDate : Date){
        //Formate Date
        datePicker.datePickerMode = .date
        
        //ToolBar
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        
        //done button & cancel button
        let doneButton = UIBarButtonItem(title: Localized("Btn_title_Done"), style: UIBarButtonItem.Style.done, target: self, action: #selector(UsageSummaryMainVC.doneDatePicker))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: Localized("Btn_title_Cancel"), style: UIBarButtonItem.Style.plain, target: self, action: #selector(UsageSummaryMainVC.cancelDatePicker))
        toolbar.setItems([doneButton,spaceButton,cancelButton], animated: false)
        
        var  currentLanguageLocale = Locale(identifier:"en")
        switch MBLanguageManager.userSelectedLanguage() {
            
        case .Russian:
            currentLanguageLocale = Locale(identifier:"ru")
        case .English:
            currentLanguageLocale = Locale(identifier:"en")
        case .Azeri:
            currentLanguageLocale = Locale(identifier:"az_AZ")
        }
        
        self.datePicker.calendar.locale = currentLanguageLocale
        self.datePicker.locale = currentLanguageLocale
        
        datePicker.minimumDate = minDate
        datePicker.maximumDate = maxDate
        datePicker.timeZone =  TimeZone.current
        
        datePicker.backgroundColor = UIColor.lightGray
        
        if #available(iOS 13.4, *) {
            datePicker.preferredDatePickerStyle = .wheels
            toolbar.frame = CGRect.init(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 40)
        }
        
        datePicker.frame = CGRect(x: 0, y: 40, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height * 0.4)
        myView.frame = CGRect(x: 0, y:((UIScreen.main.bounds.height * 0.6) - 40), width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height * 0.4)
        
        if #available(iOS 13.4, *) {
            datePicker.alignmentRect(forFrame: myView.frame)
        }
        
        myView .addSubview(toolbar)
        myView .addSubview(datePicker)
        myView.backgroundColor = UIColor.white
        
        self.view.addSubview(myView)
        
    }
    
    @objc func doneDatePicker() {
        //For date formate
        let todayDate = MBUtilities.todayDate(dateFormate: Constants.kDisplayFormat)
        
        MBUserSession.shared.dateFormatter.dateFormat = Constants.kDisplayFormat
        MBUserSession.shared.dateFormatter.timeZone = TimeZone.current
        
        let selectedDate = MBUserSession.shared.dateFormatter.string(from: datePicker.date)
        
        //        let startFieldDate = MBUserSession.shared.dateFormatter.date(from: startDateLb.text ?? "")
        let endFieldDate = MBUserSession.shared.dateFormatter.date(from: endDateLb.text ?? "")
        
        //dismiss date picker dialog
        self.view.endEditing(true)
        myView .removeFromSuperview()
        
        MBUserSession.shared.dateFormatter.dateFormat = Constants.kAPIFormat
        
        if isStartDate ?? true {
            startDateLb.text = selectedDate
            self.startDateLb.textColor = UIColor.MBBarOrange
            
            self.getUsageHistoryAPICall(StartDate: MBUserSession.shared.dateFormatter.string(from: datePicker.date), EndDate: MBUserSession.shared.dateFormatter.string(from:endFieldDate ?? todayDate))
            
        } else {
            
            endDateLb.text = selectedDate
            self.endDateLb.textColor = UIColor.MBBarOrange
            
            self.getUsageHistoryAPICall(StartDate: MBUserSession.shared.dateFormatter.string(from: datePicker.date), EndDate: MBUserSession.shared.dateFormatter.string(from:endFieldDate ?? todayDate))
        }
    }
    
    @objc func cancelDatePicker(){
        //cancel button dismiss datepicker dialog
        self.view.endEditing(true)
        
        myView .removeFromSuperview()
        print("cancel")
    }
    
    // MARK: - APICall
    func getUsageHistoryAPICall(StartDate startDate : String , EndDate endDate : String) {
        
        activityIndicator.showActivityIndicator()
        _ = MBAPIClient.sharedClient.getUlduzumUsageHistory(StartDate: startDate, EndDate: endDate, { (response, resultData, error, isCancelled, status, resultCode, resultDesc) in
            self.activityIndicator.hideActivityIndicator()
            
            if error != nil {
                error?.showServerErrorInViewController(self)
                
            } else {
                // handling data from API response.
                if resultCode == Constants.MBAPIStatusCode.succes.rawValue {
                    
                    if let responseHandler = Mapper<UlduzumDataHandler>().map(JSONObject: resultData) {
                        
                        self.filteredHistoryData = responseHandler.usageHistoryList;
                        
                        self.reloadTableViewData()
                        
                        // Success Alert
                        //                        self.showSuccessAlertWithMessage(message: resultDesc)
                        
                    } else {
                        self.showErrorAlertWithMessage(message: resultDesc)
                    }
                    
                    self.reloadTableViewData()
                }
            }
        })
    }
    
}

// MARK: - <UITableViewDataSource> / <UITableViewDelegate>
extension UlduzumUsageHistoryVC : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return filteredHistoryData?.count ?? 0
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 150
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let cell = tableView.dequeueReusableCell(withIdentifier: "UlduzumUHCell", for: indexPath) as? UlduzumUHTbleViewCell {
            
            cell.setUsageCellInfo(filteredHistoryData?[indexPath.row])
            
            if (indexPath.row % 2 == 0) {
                cell.backgroundColor = UIColor.MBDimLightGrayColor
            } else {
                cell.backgroundColor = UIColor.white
            }
            
            return cell
        }
        return UITableViewCell()
    }
    
}
