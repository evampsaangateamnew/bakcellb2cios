//
//  UsageHistoryHeaderView.swift
//  Bakcell
//
//  Created by Saad Riaz on 12/28/17.
//  Copyright © 2017 evampsaanga. All rights reserved.
//

import UIKit


class UsageHistoryHeaderView: MBAccordionTableViewHeaderView {
    
    @IBOutlet var dateTimeLb: UILabel!
    @IBOutlet var statusLb: UILabel!
    @IBOutlet var paidLb: UILabel!
    @IBOutlet var remainingLb: UILabel!
    
    @IBOutlet var manatSign: UILabel!
    @IBOutlet var manatSignWidthConstraint: NSLayoutConstraint!
    
    static let UsageHistoryIdentifier = "UsageHistory";
    
}
