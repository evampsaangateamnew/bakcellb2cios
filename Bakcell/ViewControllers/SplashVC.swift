//
//  SplashVC.swift
//  Bakcell
//
//  Created by Shujat on 16/05/2017.
//  Copyright © 2017 evampsaanga. All rights reserved.
//

import UIKit
import KYDrawerController
import ObjectMapper
import Alamofire
import LocalAuthentication

class SplashVC: BaseVC {
    
    //MARK:- Properties
    var isAppStarted = false
    /// An authentication context stored at class scope so it's available for use during UI updates.
    var context = LAContext()
    
    //MARK:- IBOutlet
    @IBOutlet var backcelIcon_img: UIImageView!
    
    //MARK:- ViewController methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Setting user initial language
        MBLanguageManager.setInitialUserLanguage()
        
        // Verifing current app version
        verifyAppVersion()
        //getSurveys()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.interactivePop(false)
        
        backcelIcon_img.image = UIImage(named: Localized("Img_BakcelLogo"))
        
        //EventLog
        MBAnalyticsManager.logEvent(screenName: "Splash Screen", contentType:"App Launch" , status:"Success" )
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        /* Check if user previously opened application and redirected to Home Page then redirect without calling AppVersion API */
        if isAppStarted == true {
            navigateUserToAppropriateScreen()
        }
    }
    
    //MARK:- Functions
    /**
     Redirect user from splash screen to walkThrough, Login/SignUp and Dashboard ViewController.
     - returns: void
     */
    @objc func navigateUserToAppropriateScreen() {
        
        // Set to true, can verify user redirected first time from splash screen.
        isAppStarted = true
        activityIndicator.hideActivityIndicator()
        
        // Check if WalkThrought show to user before if not then show.
        if MBUtilities.isLaunchedBefore() == false {
            
            // Update value to shown
            MBUtilities.updateLaunchedBeforeStatus()
            
            // Redirect user to WalkThrough screen.
            if let tutorialsVC = self.myStoryBoard.instantiateViewController(withIdentifier: "TutorialVC") as? TutorialVC {
                self.navigationController?.pushViewController(tutorialsVC, animated: true)
            }
            
        } else {
            
            // Check if user loggedIn and Customer information is loaded sucessfully.
            if MBUserSession.shared.isLoggedIn() && MBUserSession.shared.loadUserInfomation() {
                
                if MBUserSession.shared.isBiometricEnabled() {
                    self.authenticateWithTouchID()
                } else {
                    self.redirectToDashboardScreen()
                }
                
            } else {
                let (isLoggedIn, userInfo) = MBUtilities.load_Old_CustomerDataFromUserDefaults()
                
                if isLoggedIn == true,
                    let newCustomerDataObject = Mapper<NewCustomerData>().map(JSONObject:userInfo?.toJSON()),
                    let userMsisdn = newCustomerDataObject.msisdn,
                    userMsisdn.isBlank == false {
                    /* Save logged in user information */
                    MBUserInfoUtilities.saveLoginInfo(loggedInUserMSISDN: userMsisdn, loginInfo: SaveCustomerModel(customerData: newCustomerDataObject))
                    
                    if MBUserSession.shared.isBiometricEnabled() {
                        self.authenticateWithTouchID()
                    } else {
                        self.redirectToDashboardScreen()
                    }
                    
                    /* Clear old user information */
                    MBUserInfoUtilities.clearJSONStringFromUserDefaults(key: Constants.kUserInfoKey)
                    
                } else {
                    // Redirect user to login screen.
                    redirectToLoginScreen()
                }
            }
        }
    }
    
    
    /**
     Redirect user to AppStore
     - returns: void
     */
    func redirectUserToAppStore(byForce: Bool = false) {
        //FIXME: verify itune link of bakcell.
        if let storeURL = URL(string: Localized("appStore_URL")) {
            if UIApplication.shared.canOpenURL(storeURL) {
                
                if #available(iOS 10, *) {
                    UIApplication.shared.open(storeURL, options: convertToUIApplicationOpenExternalURLOptionsKeyDictionary([:]), completionHandler: { (success: Bool) in
                        
                        // If failded to open URL
                        if success == false {
                            UIApplication.shared.openURL(storeURL)
                        }
                        // Close application
                        exit(0)
                    })
                } else {
                    UIApplication.shared.openURL(storeURL)
                    // Close application
                    exit(0)
                }
                
                
            } else {
                
                self.showErrorAlertWithMessage(message: Localized("Message_CannotAbleToRedirect"), { (_) in
                    
                    if byForce == true {
                        // Close application
                        exit(0)
                    } else {
                        self.navigateUserToAppropriateScreen()
                    }
                })
                
                
            }
        } else {
            self.showErrorAlertWithMessage(message: Localized("Message_CannotAbleToRedirect"), { (_) in
                if byForce == true {
                    // Close application
                    exit(0)
                } else {
                    self.navigateUserToAppropriateScreen()
                }
            })
        }
    }
    
    func redirectToDashboardScreen() {
        
        if let mainViewController   = self.myStoryBoard.instantiateViewController(withIdentifier: "TabbarController") as? TabbarController {
            
            if let drawerViewController = self.myStoryBoard.instantiateViewController(withIdentifier: "SideDrawerVC") as? SideDrawerVC {
                let drawerController = KYDrawerController(drawerDirection: .left, drawerWidth: (UIScreen.main.bounds.width * 0.85))
                drawerController.mainViewController = mainViewController
                drawerController.drawerViewController = drawerViewController
                self.navigationController?.pushViewController(drawerController, animated: true)
            }
        }
    }
    
    func redirectToLoginScreen() {
        if let loginVC = self.myStoryBoard.instantiateViewController(withIdentifier: "LoginVC") as? LoginVC {
            self.navigationController?.pushViewController(loginVC, animated: true)
        }
    }
    
    func authenticateWithTouchID() {
        
        // Get a fresh context for each login. If you use the same context on multiple attempts
        //  (by commenting out the next line), then a previously successful authentication
        //  causes the next policy evaluation to succeed without testing biometry again.
        //  That's usually not what you want.
        context = LAContext()
        
        if #available(iOS 10.0, *) {
            //  context.localizedCancelTitle = "Enter Username/Password"
        } else {
            // Fallback on earlier versions
        }
        
        //context.localizedFallbackTitle = "Use Passcode"
        
        // First check if we have the needed hardware support.
        var error: NSError?
        if context.canEvaluatePolicy(.deviceOwnerAuthentication, error: &error) {
            
            let reason = Localized("Biometric_Login")
            context.evaluatePolicy(.deviceOwnerAuthentication, localizedReason: reason ) { success, error in
                
                if success {
                    // Move to the main thread because a state update triggers UI changes.
                    DispatchQueue.main.async {
                        self.redirectToDashboardScreen()
                    }
                    
                } else {
                    // Fall back to a asking for username and password.
                    // ...
                    //TODO: Show appropriate alert if biometry/TouchID/FaceID is lockout or not enrolled
                    let errorDescription = self.evaluateAuthenticationPolicyMessageForLA(errorCode: error?._code)
                    if errorDescription.isUserAction == false {
                        self.showErrorAlertWithMessage(message: errorDescription.message, { (_) in
                            
                            DispatchQueue.main.async {
                                self.redirectToLoginScreen()
                            }
                        })
                    } else {
                        DispatchQueue.main.async {
                            self.redirectToLoginScreen()
                        }
                    }
                }
            }
        } else {
            // Fall back to a asking for username and password.
            // ...
            //TODO: Show appropriate alert if biometry/TouchID/FaceID is lockout or not enrolled
            let errorDescription = self.evaluateAuthenticationPolicyMessageForLA(errorCode: error?._code)
            if errorDescription.isUserAction == false {
                self.showErrorAlertWithMessage(message: errorDescription.message, { (_) in
                    DispatchQueue.main.async {
                        self.redirectToLoginScreen()
                    }
                })
            } else {
                DispatchQueue.main.async {
                    self.redirectToLoginScreen()
                }
            }
        }
    }
    
    func evaluateAuthenticationPolicyMessageForLA(errorCode: Int?) -> (message:String, isUserAction:Bool) {
        
        var message :String?
        var userCanceled :Bool = false
        
        switch errorCode {
        case LAError.authenticationFailed.rawValue:
            message = Localized("Error_FailedCredentials")
            
        case LAError.userCancel.rawValue:
            message = "The user did cancel"
            userCanceled = true
            
        case LAError.userFallback.rawValue:
            message = "The user choose to use the fallback"
            userCanceled = true
            
        case LAError.systemCancel.rawValue:
            message = Localized("Error_SystemCancel")
            
        case LAError.passcodeNotSet.rawValue:
            message = Localized("Error_PasscodeNotSet")
            
        case LAError.appCancel.rawValue:
            message = Localized("Auth_AppCancel")
            
        case LAError.invalidContext.rawValue:
            message = "The context is invalid"
            
        case LAError.notInteractive.rawValue:
            message = Localized("Error_NotInteractive")
            
        default:
            if #available(iOS 11.0, macOS 10.13, *) {
                switch errorCode {
                case LAError.biometryNotAvailable.rawValue:
                    message = Localized("Error_BiometryNotAvailable")
                    
                case LAError.biometryNotEnrolled.rawValue:
                    message = Localized("Error_BiometryNotEnrolled")
                    
                case LAError.biometryLockout.rawValue:
                    message = Localized("Error_BiometricLockout")
                    
                default:
                    break
                }
            } else {
                switch errorCode {
                case LAError.touchIDNotAvailable.rawValue:
                    message = Localized("Error_BiometryNotAvailable")
                    
                case LAError.touchIDNotEnrolled.rawValue:
                    message = Localized("Error_BiometryNotEnrolled")
                    
                case LAError.touchIDLockout.rawValue:
                    message = Localized("Error_BiometricLockout")
                default:
                    break
                }
            }
        }
        
        return ((message ?? Localized("Error_AuthGeneral")) ,userCanceled)
    }
    
    
    //MARK: - API Calls
    /**
     verify AppVersion API call
     
     - returns: void
     */
    func verifyAppVersion() {
        
        // Check if connected to internet then call API
        if Connectivity.isConnectedToInternet == true {
            
            _ = MBAPIClient.sharedClient.verifyAppVersion({ (response, resultData, error, isCancelled, status, resultCode, resultDesc) in
                
                // handling data from API response.
                if resultCode == Constants.MBAPIStatusCode.succes.rawValue { /* Application is updated */
                    
                    /*if let appVersionResponse = Mapper<VerifyAppVersion>().map(JSONObject: resultData) {}*/
                    
                    // Redirect user to appropriate screen
                    self.navigateUserToAppropriateScreen()
                    
                } else if resultCode == Constants.MBAPIStatusCode.forceUpdate.rawValue { /* New version of application is available */
                    
                    //TODO: Need to revert....
                    self.navigateUserToAppropriateScreen()
                    return
                    //EventLog
                    MBAnalyticsManager.logEvent(screenName: "Splash Screen", contentType:"New App Version Available" , status:"Success" )
                    
                    // Show force update alert.
                    self.showAlertWithTitleAndMessage(title: Localized("Title_Update"), message: resultDesc, { (aString) in
                        
                        // Redirect user to appStore in case user select update button
                        self.redirectUserToAppStore(byForce: true)
                    })
                    
                } else if resultCode == Constants.MBAPIStatusCode.optionalUpdate.rawValue {
                    
                    // Log Event
                    MBAnalyticsManager.logEvent(screenName: "Splash Screen", contentType:"New App Version Available" , status:"Splash Screen" )
                    
                    // SHOW Aler and then redirect according to user selection
                    if let alert = self.myStoryBoard.instantiateViewController(withIdentifier: "AlertVC") as? AlertVC {
                        alert.dissmisAlerOnButtonAction = false
                        
                        alert.setConfirmationAlertLayoutWithOutBalance(title: Localized("Title_Update"), message: resultDesc ?? "",yesBtnTitle: Localized("BtnTitle_UPDATE"),noBtnTitle: Localized("BtnTitle_CANCEL"))
                        
                        self.presentPOPUP(alert, animated: true, completion: nil)
                        
                        // Set action for update button
                        alert.setYesButtonCompletionHandler(completionBlock: { (aString) in
                            self.dismiss(animated: true, completion: {
                                
                                //EventLog
                                MBAnalyticsManager.logEvent(screenName: "Splash Screen", contentType:"App Verison Updated" , status:"Success" )
                                self.redirectUserToAppStore(byForce: false)
                            })
                            
                        })
                        
                        // Set action for cancel button
                        alert.setNoButtonCompletionHandler(completionBlock: { (aString) in
                            self.dismiss(animated: true, completion: {
                                // Redirect user to appropriate screen
                                self.navigateUserToAppropriateScreen()
                            })
                        })
                    }
                    
                } else if resultCode == Constants.MBAPIStatusCode.serverDown.rawValue {
                    // Redirect user to appropriate screen
                    self.navigateUserToAppropriateScreen()
                    
                } else {
                    // Redirect user to appropriate screen
                    self.navigateUserToAppropriateScreen()
                }
            })
            
        } else {
            // Redirect user to appropriate screen with delay
            if isAppStarted == false {
                perform(#selector(navigateUserToAppropriateScreen), with: nil, afterDelay: Constants.SPLASH_DURATION)
            } else {
                // Redirect user to appropriate screen
                self.navigateUserToAppropriateScreen()
            }
        }
    }
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertToUIApplicationOpenExternalURLOptionsKeyDictionary(_ input: [String: Any]) -> [UIApplication.OpenExternalURLOptionsKey: Any] {
    return Dictionary(uniqueKeysWithValues: input.map { key, value in (UIApplication.OpenExternalURLOptionsKey(rawValue: key), value)})
}
