//
//  UIViewController+MB.swift
//  Bakcell
//
//  Created by Saad Riaz on 11/6/17.
//  Copyright © 2017 evampsaanga. All rights reserved.
//

import Foundation
import UIKit
import UserNotifications
import KYDrawerController

extension UIViewController {
    open func presentPOPUP(_ viewControllerToPresent: UIViewController, animated flag: Bool, modalTransitionStyle:UIModalTransitionStyle = .coverVertical, completion: (() -> Swift.Void)? = nil) {
        
        viewControllerToPresent.modalPresentationStyle = .overCurrentContext
        viewControllerToPresent.modalTransitionStyle = modalTransitionStyle
        
        self.present(viewControllerToPresent, animated: flag, completion: completion)
        
    }
}

extension UIViewController {
    
    var myStoryBoard: UIStoryboard {
        
//        if let currentStoryBoard = self.storyboard  {
//            return currentStoryBoard
//        } else {
            return UIStoryboard(name: "Main", bundle: nil)
//        }
    }
    var myStoryBoard2: UIStoryboard {
        
//        if let currentStoryBoard = self.storyboard  {
//            return currentStoryBoard
//        } else {
            return UIStoryboard(name: "Main2", bundle: nil)
//        }
    }
    
    /**
     Initialize a nib.
     
     
     - returns: UIViewController.
     */
    public class func fromNib<T>() -> T? where T : UIViewController {
        return fromNib(nibName: nil)
    }
    
    /**
     Initialize a nib.
     
     - parameter nibName: Nib name.
     
     - returns: UIViewController.
     */
    public class func fromNib<T>(nibName: String?) -> T? where T : UIViewController {
        
        let name = nibName ?? String(describing: self)
        return self.init(nibName: name, bundle: Bundle.main) as? T
    }
    
    /**
     Initialize a UIViewController from Storyboard.
     
     
     - returns: UIViewController.
     */
    class func instantiateViewControllerFromStoryboard<T>() -> T? where T : UIViewController {
        return instantiateViewController()
    }
    
    /**
     Initialize a UIViewController from Storyboard.
     
     
     - returns: UIViewController.
     */
    fileprivate class func instantiateViewController<T>() -> T? where T : UIViewController  {
        return UIViewController().myStoryBoard.instantiateViewController(withIdentifier: String(describing: self)) as? T
    }
    
}

extension BaseVC {
    
    func dialNumber(number : String) {
        
        if let url = URL(string: "tel://\(number.trimmWhiteSpace)"), UIApplication.shared.canOpenURL(url) {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url, options: convertToUIApplicationOpenExternalURLOptionsKeyDictionary([:]), completionHandler:nil)
            } else {
                UIApplication.shared.openURL(url)
            }
        } else {
            self.showErrorAlertWithMessage(message: Localized("Message_UnexpectedError"))
        }
    }


    func openURLInSafari(urlString : String?) {

        // Not Valid URL string
        if urlString == nil {
            self.showErrorAlertWithMessage(message: Localized("Message_UnexpectedError"))
        } else {

            var newURLString = urlString
            if (urlString?.containsSubString(subString: "http://") ?? false) == false &&
                (urlString?.containsSubString(subString: "https://") ?? false) == false{

                newURLString = "http://\(urlString ?? "")"
            }

            if let requestUrl = URL(string: newURLString?.trimmWhiteSpace ?? "") {

                if UIApplication.shared.canOpenURL(requestUrl) {
                    if #available(iOS 10, *) {
                        UIApplication.shared.open(requestUrl, options: convertToUIApplicationOpenExternalURLOptionsKeyDictionary([:]), completionHandler:nil)
                    } else {
                        UIApplication.shared.openURL(requestUrl)
                    }
                }  else {
                    self.showErrorAlertWithMessage(message: Localized("Message_UnexpectedError"))
                }

            } else {
                self.showErrorAlertWithMessage(message: Localized("Message_UnexpectedError"))
            }
        }
    }


    func isUserEnabledNotification(completionHandler: @escaping (Bool) -> Void) {


        if #available(iOS 10.0, *) {

            var isEnabled : Bool = false
            let current = UNUserNotificationCenter.current()

            current.getNotificationSettings(completionHandler: { (settings) in

                if settings.authorizationStatus == .authorized {
                    // Notification permission was already granted
                    isEnabled = true

                } else if settings.authorizationStatus == .notDetermined {
                    // Notification permission has not been asked yet, go for it!
                    isEnabled = false

                } else if settings.authorizationStatus == .denied {
                    // Notification permission was previously denied, go to settings & privacy to re-enable
                    isEnabled = false
                    
                }
                completionHandler(isEnabled)

            })

        } else {

            var isEnabled : Bool = false
            // Fallback on earlier versions
            if let notificationType = UIApplication.shared.currentUserNotificationSettings?.types {
                if notificationType == [] {

                    isEnabled = false
                } else {
                    isEnabled = true
                }
            } else {
                isEnabled = false
            }

            completionHandler(isEnabled)
        }
    }
    

    func showAlertWithTitleAndMessage(title:String?, message: String?,_ actionBlock : @escaping MBButtonCompletionHandler = { _ in }) {

        if let alert = self.myStoryBoard.instantiateViewController(withIdentifier: "AlertMessageVC") as? AlertMessageVC {
            
            alert.setAlertWith(title: title ?? "", description: message ?? "", okBtnClickedBlock: actionBlock)
            self.presentPOPUP(alert, animated: true, modalTransitionStyle: .crossDissolve, completion: nil)
            
        } else {
            let alertViewController = UIAlertController(title: title ?? "", message: message ?? "", preferredStyle: .alert)

            let okAction = UIAlertAction(title: Localized("BtnTitle_OK"), style: .default) { (action) in

            }

            alertViewController.addAction(okAction)
            self.present(alertViewController, animated: true, completion: nil)
        }
    }
    
    func showAlertWithMessage(message: String?, actionBlock : @escaping MBButtonCompletionHandler = { _ in }) {

        self.showAlertWithTitleAndMessage(title: Localized("Title_Attention"), message: message, actionBlock)
    }
    
    func showErrorAlertWithMessage(message: String?, _ actionBlock : @escaping MBButtonCompletionHandler = { _ in }) {
        self.showAlertWithTitleAndMessage(title: Localized("Title_Error"), message: message, actionBlock)
    }
    
    func showSuccessAlertWithMessage(message: String?, _ actionBlock : @escaping MBButtonCompletionHandler = { _ in }) {
        self.showAlertWithTitleAndMessage(title: Localized("Title_successful"), message: message, actionBlock)
    }

    func showAccessAlert(message: String?) {

        let alertViewController = UIAlertController(title: Localized("Title_Alert"), message: message ?? "", preferredStyle: .alert)

        let cancelAction = UIAlertAction(title: Localized("BtnTitle_CANCEL"), style: .cancel) { (action) in }

        let settingAction = UIAlertAction(title: Localized("BtnTitle_SETTING"), style: .default) { (action) in

            if let url = URL(string: UIApplication.openSettingsURLString ), UIApplication.shared.canOpenURL(url) {
                if #available(iOS 10, *) {
                    UIApplication.shared.open(url, options: convertToUIApplicationOpenExternalURLOptionsKeyDictionary([:]), completionHandler:nil)
                } else {
                    UIApplication.shared.openURL(url)
                }
            } else {
                self.showErrorAlertWithMessage(message: Localized("Message_UnexpectedError"))
            }
        }

        alertViewController.addAction(cancelAction)
        alertViewController.addAction(settingAction)

        self.present(alertViewController, animated: true, completion: nil)

    }

    func showAccessAlertWith(title:String, message: String?, cancleTitle:String?, okayButtonTitle:String?) {

        let alertViewController = UIAlertController(title: title, message: message ?? "", preferredStyle: .actionSheet)

        let cancelAction = UIAlertAction(title: cancleTitle, style: .cancel) { (action) in }

        let settingAction = UIAlertAction(title: okayButtonTitle, style: .default) { (action) in

            if let url = URL(string: UIApplication.openSettingsURLString ), UIApplication.shared.canOpenURL(url) {
                if let notifications = self.myStoryBoard.instantiateViewController(withIdentifier: "NotificationsVC") as? NotificationsVC {
                    self.navigationController?.pushViewController(notifications, animated: true)
                }
            } else {
                self.showErrorAlertWithMessage(message: Localized("Message_UnexpectedError"))
            }
        }

        alertViewController.addAction(cancelAction)
        alertViewController.addAction(settingAction)

        self.present(alertViewController, animated: true, completion: nil)

    }
    
    
    /**
     Redirect user to AppStore
     - returns: void
     */
    func redirectUserToAppStore(completion: @escaping (_ isRedirected: Bool) -> ()) {
        
        if let storeURL = URL(string: Localized("appStore_URL")),
            UIApplication.shared.canOpenURL(storeURL) {
            
            if #available(iOS 10, *) {
                UIApplication.shared.open(storeURL, options: [:], completionHandler: { (success: Bool) in
                    
                    // If failded to open URL
                    if success == false {
                        UIApplication.shared.openURL(storeURL)
                    }
                    
                    completion(true)
                })
            } else {
                UIApplication.shared.openURL(storeURL)
                completion(true)
            }
            
        } else {
            self.showErrorAlertWithMessage(message: Localized("Message_CannotAbleToRedirect"))
            completion(false)
        }
    }
}


extension UITabBarController {

    func showAlertWithTitleAndMessage(title:String?, message: String?,_ actionBlock : @escaping MBButtonCompletionHandler = { _ in }) {

        if let alert = self.myStoryBoard.instantiateViewController(withIdentifier: "AlertMessageVC") as? AlertMessageVC {

            alert.setAlertWith(title: title ?? "", description: message ?? "", okBtnClickedBlock: actionBlock)
            self.presentPOPUP(alert, animated: true, modalTransitionStyle:.crossDissolve, completion: nil)
        } else {
            let alertViewController = UIAlertController(title: title ?? "", message: message ?? "", preferredStyle: .alert)

            let okAction = UIAlertAction(title: Localized("BtnTitle_OK"), style: .default) { (action) in

            }

            alertViewController.addAction(okAction)
            self.present(alertViewController, animated: true, completion: nil)
        }
    }
    func showAlertWithMessage(message: String?, actionBlock : @escaping MBButtonCompletionHandler = { _ in }) {

        self.showAlertWithTitleAndMessage(title: Localized("Title_Attention"), message: message, actionBlock)
    }
    func showErrorAlertWithMessage(message: String?, _ actionBlock : @escaping MBButtonCompletionHandler = { _ in }) {
        self.showAlertWithTitleAndMessage(title: Localized("Title_Error"), message: message, actionBlock)
    }
    func showSuccessAlertWithMessage(message: String?, _ actionBlock : @escaping MBButtonCompletionHandler = { _ in }) {
        self.showAlertWithTitleAndMessage(title: Localized("Title_successful"), message: message, actionBlock)
    }
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertToUIApplicationOpenExternalURLOptionsKeyDictionary(_ input: [String: Any]) -> [UIApplication.OpenExternalURLOptionsKey: Any] {
	return Dictionary(uniqueKeysWithValues: input.map { key, value in (UIApplication.OpenExternalURLOptionsKey(rawValue: key), value)})
}
