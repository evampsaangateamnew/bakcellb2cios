//
//  MBLanguageManager.swift
//  Bakcell
//
//  Created by AbdulRehman Warraich on 9/28/17.
//  Copyright © 2017 evampsaanga. All rights reserved.
//

import Foundation

class MBLanguageManager: NSObject {

    class func setInitialUserLanguage(){
        _ = MBLanguageManager.userSelectedLanguage()
    }

    class func userSelectedLanguage() -> Constants.MBLanguage {

        let userDefaults = UserDefaults.standard

        if let userSelectedLanguage : String = userDefaults.value(forKey: Constants.kUserSelectedLanguage) as? String {

            switch userSelectedLanguage {

            case "2":
                return Constants.MBLanguage.Russian

            case "3":
                return Constants.MBLanguage.English

            case "4":
                return Constants.MBLanguage.Azeri

            default:
                MBLanguageManager.setUserLanguage(selectedLanguage: .Azeri)
                return Constants.MBLanguage.Azeri
            }

        } else {
            MBLanguageManager.setUserLanguage(selectedLanguage: .Azeri)
            return Constants.MBLanguage.Azeri
        }
    }

    class func setUserLanguage(selectedLanguage : Constants.MBLanguage) {

        // Set true to reload Home page data
        MBUserSession.shared.shouldCallAndUpdateAppResumeData = true

        let userDefaults = UserDefaults.standard

        userDefaults.set(selectedLanguage.rawValue, forKey: Constants.kUserSelectedLanguage)

        userDefaults.synchronize()

        switch selectedLanguage {

        case Constants.MBLanguage.English:
            Localize.setCurrentLanguage("en")
            break

        case Constants.MBLanguage.Russian:
            Localize.setCurrentLanguage("ru")
            break

        case Constants.MBLanguage.Azeri:
            Localize.setCurrentLanguage("az-Cyrl")
            break
            
        }

    }

    class func userSelectedLanguageName() -> String {

        let languageName = MBLanguageManager.userSelectedLanguage()

        switch languageName {

        case Constants.MBLanguage.Russian:
            return "Russian"

        case Constants.MBLanguage.English:
            return "English"

        case Constants.MBLanguage.Azeri:
            return "Azeri"
            
        }
    }
    
    class func userSelectedLanguageShortCode() -> String {
        
        let languageName = MBLanguageManager.userSelectedLanguage()
        
        switch languageName {
            
        case Constants.MBLanguage.Russian:
            return "ru"
            
        case Constants.MBLanguage.English:
            return "en"
            
        case Constants.MBLanguage.Azeri:
            return "az"
            
        }
    }
}
