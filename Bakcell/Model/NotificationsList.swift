//
//  NotificationsList.swift
//  Bakcell
//
//  Created by AbdulRehman Warraich on 10/6/17.
//  Copyright © 2017 evampsaanga. All rights reserved.
//

import Foundation
import ObjectMapper

class Notifications : Mappable {

    var notificationsList :  [NotificationsList]?

    init() {
    }
    required init?(map: Map) {
    }
    func mapping(map: Map) {

        notificationsList       <- map["notificationsList"]
    }
}

class NotificationsList : Mappable {

    //    "message": "Notification 1",
    //    "landingPage": "Home",
    //    "datetime": "2017-10-04 12:00:00",
    //    "icon": "icon 1",
    //    "actionType": "tariff",
    //    "actionID": "2",
    //    "btnTxt": "Renew"

    var message :  String?
    var landingPage :  String?
    var datetime :  String?
    var icon :  String?
    var actionType :  String?
    var actionID :  String?
    var btnTxt :  String?

    required init?(map: Map) {
    }
    func mapping(map: Map) {

        message         <- map["message"]
        landingPage     <- map["landingPage"]
        datetime        <- map["datetime"]
        icon            <- map["icon"]
        actionType      <- map["actionType"]
        actionID        <- map["actionID"]
        btnTxt          <- map["btnTxt"]
    }
}

class NotificationsCount : Mappable {

    //    "notificationUnreadCount": "0"

    var notificationUnreadCount :  String?

    required init?(map: Map) {
    }
    func mapping(map: Map) {

        notificationUnreadCount         <- map["notificationUnreadCount"]
    }
}

