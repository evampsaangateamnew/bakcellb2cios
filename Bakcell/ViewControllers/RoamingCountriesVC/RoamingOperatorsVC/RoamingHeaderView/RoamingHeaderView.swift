//
//  RoamingHeaderView.swift
//  Bakcell
//
//  Created by AbdulRehman Warraich on 1/15/19.
//  Copyright © 2019 evampsaanga. All rights reserved.
//

import UIKit

class RoamingHeaderView: UITableViewHeaderFooterView {
    @IBOutlet var lblName : UILabel!
    @IBOutlet var lblValue : UILabel!

}
