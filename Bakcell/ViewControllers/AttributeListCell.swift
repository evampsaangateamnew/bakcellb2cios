//
//  AttributeListCell.swift
//  Bakcell
//
//  Created by AbdulRehman Warraich on 10/13/17.
//  Copyright © 2017 evampsaanga. All rights reserved.
//

import UIKit
import MarqueeLabel

class AttributeListCell: UITableViewCell {

    static let identifier  = "AttributeListCell"
    @IBOutlet var AttributeListView: UIView!

    // AttributeList view iner outlets
    @IBOutlet var icon_img: UIImageView!
    @IBOutlet var title_lbl: MarqueeLabel!
    @IBOutlet var value_lbl: UILabel!
    @IBOutlet var unit_lbl: UILabel!

    @IBOutlet var onnetView: UIView!
    @IBOutlet var offnetView: UIView!

    @IBOutlet var onnetTitle_lbl: MarqueeLabel!
    @IBOutlet var onnetValue_lbl: UILabel!
    @IBOutlet var offnetTitle_lbl: MarqueeLabel!
    @IBOutlet var offnetValue_lbl: UILabel!

    @IBOutlet var topSeparatorView: UIView!
    @IBOutlet var bottomSeparatorView: UIView!

    @IBOutlet var description_View: UIView!
    @IBOutlet var description_lbl: UILabel!

    @IBOutlet var descriptionViewHeightGreaterRelation: NSLayoutConstraint!
    @IBOutlet var descriptionViewHeightEqualRelation: NSLayoutConstraint!
    @IBOutlet var unitWidthConstranint: NSLayoutConstraint!

    @IBOutlet var onnetViewHeight: NSLayoutConstraint!
    @IBOutlet var offnetViewHeight: NSLayoutConstraint!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.AttributeListView.backgroundColor = UIColor.white
        self.title_lbl.textColor = UIColor.MBTextGrayColor
        self.onnetTitle_lbl.textColor = UIColor.MBTextGrayColor
        self.offnetTitle_lbl.textColor = UIColor.MBTextGrayColor
        self.description_lbl.textColor = UIColor.MBTextGrayColor

        self.value_lbl.textColor = UIColor.MBRedColor
        self.unit_lbl.textColor = UIColor.MBRedColor

        self.title_lbl.setupMarqueeAnimation()
        self.onnetTitle_lbl.setupMarqueeAnimation()
        self.offnetTitle_lbl.setupMarqueeAnimation()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    // Using in supplementary offers
    func setAttributeList(attributeList : AttributeList?, offerType: String) {

        AttributeListView.isHidden = false
        description_View.isHidden = false
        topSeparatorView.isHidden = false

        if let aAttributeList = attributeList {

            icon_img.image = MBUtilities.iconImageFor(key: aAttributeList.iconMap)
            title_lbl.text = aAttributeList.title ?? ""

            if let value = aAttributeList.value {

                if value.isHasFreeOrUnlimitedText() ||
                    offerType.isEqualToStringIgnoreCase(otherString: "Call") == true ||
                    offerType.isEqualToStringIgnoreCase(otherString: "SMS") == true ||
                    offerType.isEqualToStringIgnoreCase(otherString: "Internet") == true ||
                    (offerType.isEqualToStringIgnoreCase(otherString: "TM") == true && aAttributeList.title?.isHasGetText() ?? false) ||
                    offerType.isEqualToStringIgnoreCase(otherString: "Звонок") == true ||
                    offerType.isEqualToStringIgnoreCase(otherString: "Интернет") == true ||
                    (offerType.isEqualToStringIgnoreCase(otherString: "Особенные") == true && aAttributeList.title?.isHasGetText() ?? false) ||
                    offerType.isEqualToStringIgnoreCase(otherString: "Zəng") == true ||
                    offerType.isEqualToStringIgnoreCase(otherString: "İnternet") == true ||
                    (offerType.isEqualToStringIgnoreCase(otherString: "Xüsusilər") == true && aAttributeList.title?.isHasGetText() ?? false) {

                    value_lbl.textColor = UIColor.MBRedColor
                    unit_lbl.textColor = UIColor.MBRedColor

                } else {
                    value_lbl.textColor = UIColor.darkGray
                    unit_lbl.textColor = UIColor.darkGray
                }

                if aAttributeList.unit?.isEqualToStringIgnoreCase(otherString: "AZN") == true {
                    unitWidthConstranint.constant = 10
                    value_lbl.text = value
                    unit_lbl.text = "₼"

                } else {
                    let unitValue = aAttributeList.unit?.removeNullValues() ?? ""
                    unitWidthConstranint.constant = 0
                    value_lbl.text = "\(value) \(unitValue)"

                }
            } else {

                title_lbl.text = ""
                unit_lbl.text = ""
                value_lbl.text = ""
            }



            if aAttributeList.onnetValue?.isBlank == false {
                onnetView.isHidden = false
                onnetViewHeight.constant = 15

                onnetTitle_lbl.text = aAttributeList.onnetLabel
                onnetValue_lbl.text = aAttributeList.onnetValue

            } else {
                onnetView.isHidden = true
                onnetViewHeight.constant = 0
            }

            if aAttributeList.offnetValue?.isBlank == false {
                offnetView.isHidden = false
                offnetViewHeight.constant = 15

                offnetTitle_lbl.text = aAttributeList.offnetLabel
                offnetValue_lbl.text = aAttributeList.offnetValue

            } else {
                offnetView.isHidden = true
                offnetViewHeight.constant = 0
            }


            let description = aAttributeList.description ?? ""
            if description.isBlank == false {
                
                description_View.isHidden = false
                descriptionViewHeightEqualRelation.isActive = false
                descriptionViewHeightGreaterRelation.isActive = true

                descriptionViewHeightEqualRelation.priority = UILayoutPriority(rawValue: 990)
                descriptionViewHeightGreaterRelation.priority = UILayoutPriority(rawValue: 999)

                description_lbl.text = description

            } else {
                description_View.isHidden = true

                descriptionViewHeightGreaterRelation.isActive = false
                descriptionViewHeightEqualRelation.isActive = true


                descriptionViewHeightGreaterRelation.priority = UILayoutPriority(rawValue: 990)
                descriptionViewHeightEqualRelation.priority = UILayoutPriority(rawValue: 999)
                description_lbl.text = nil

            }
        }
    }

    // Using in My Subscription
    func setAttributeListOfMySubscription(attributeList : AttributeList?) {

        AttributeListView.isHidden = false
        description_View.isHidden = false

        onnetView.isHidden = true
        offnetView.isHidden = true
        onnetViewHeight.constant = 0
        offnetViewHeight.constant = 0

        self.contentView.backgroundColor = UIColor.MBDimLightGrayColor
        topSeparatorView.backgroundColor = UIColor.MBDimLightGrayColor
        bottomSeparatorView.backgroundColor = UIColor.MBDimLightGrayColor
        AttributeListView.backgroundColor = UIColor.MBDimLightGrayColor
        description_View.backgroundColor = UIColor.MBDimLightGrayColor

        if let aAttributeList = attributeList {

            title_lbl.text = aAttributeList.title ?? ""

            // Text color
            title_lbl.textColor = UIColor.MBLightGrayColor
            value_lbl.textColor = UIColor.black
            unit_lbl.textColor = UIColor.black
            description_lbl.textColor = UIColor.MBLightGrayColor

            if let value = aAttributeList.value {

                if value.isHasFreeOrUnlimitedText() {

                    value_lbl.textColor = UIColor.MBRedColor
                    unit_lbl.textColor = UIColor.MBRedColor
                } else {
                    value_lbl.textColor = UIColor.black
                    unit_lbl.textColor = UIColor.black
                }


                if aAttributeList.unit?.isEqualToStringIgnoreCase(otherString: "AZN") ?? false {
                    unitWidthConstranint.constant = 10
                    value_lbl.text = value
                    unit_lbl.text = "₼"

                } else {
                    let unitValue = aAttributeList.unit?.removeNullValues() ?? ""
                    unitWidthConstranint.constant = 0
                    value_lbl.text = "\(value) \(unitValue)"

                }
            } else {

                title_lbl.text = ""
                unit_lbl.text = ""
                value_lbl.text = ""
            }

            icon_img.image = MBUtilities.iconImageFor(key: aAttributeList.iconMap)

            let description = aAttributeList.description ?? ""
            if description.isBlank == false {
                description_View.isHidden = false
                descriptionViewHeightEqualRelation.isActive = false
                descriptionViewHeightGreaterRelation.isActive = true

                descriptionViewHeightEqualRelation.priority = UILayoutPriority(rawValue: 990)
                descriptionViewHeightGreaterRelation.priority = UILayoutPriority(rawValue: 999)

                description_lbl.text = description

            } else {
                description_View.isHidden = true

                descriptionViewHeightGreaterRelation.isActive = false
                descriptionViewHeightEqualRelation.isActive = true


                descriptionViewHeightGreaterRelation.priority = UILayoutPriority(rawValue: 990)
                descriptionViewHeightEqualRelation.priority = UILayoutPriority(rawValue: 999)
                description_lbl.text = ""

            }
        }
    }


    // Using in Tariff Postpaid section
    func setAttributeListForTariffPostpaidHeader(attributeList : HeaderAttributes?) {

        AttributeListView.isHidden = false
        description_View.isHidden = true

        descriptionViewHeightGreaterRelation.isActive = false
        descriptionViewHeightEqualRelation.isActive = true


        descriptionViewHeightGreaterRelation.priority = UILayoutPriority(rawValue: 990)
        descriptionViewHeightEqualRelation.priority = UILayoutPriority(rawValue: 999)

        onnetView.isHidden = true
        offnetView.isHidden = true
        onnetViewHeight.constant = 0
        offnetViewHeight.constant = 0

        self.contentView.backgroundColor = UIColor.white
        AttributeListView.backgroundColor = UIColor.white
        topSeparatorView.backgroundColor = UIColor.MBDimLightGrayColor
        bottomSeparatorView.backgroundColor = UIColor.MBDimLightGrayColor



        if let aAttributeList = attributeList {

            unit_lbl.isHidden = true
            unitWidthConstranint.constant = 0

            title_lbl.text = aAttributeList.title ?? ""

            // Text color
            title_lbl.textColor = UIColor.MBLightGrayColor
            value_lbl.textColor = UIColor.black

            description_lbl.textColor = UIColor.MBLightGrayColor

            if aAttributeList.metrics?.isHasFreeOrUnlimitedText() == true {

                value_lbl.textColor = UIColor.MBRedColor

            } else {
                value_lbl.textColor = UIColor.black

            }
            value_lbl.text = "\(aAttributeList.value ?? "") \(aAttributeList.metrics ?? "")"

            icon_img.image = MBUtilities.iconImageFor(key: aAttributeList.iconName)


        } else {
            value_lbl.text = ""
        }
    }
}
