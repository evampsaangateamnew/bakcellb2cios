//
//  CountryView.swift
//  Bakcell
//
//  Created by AbdulRehman Warraich on 8/30/17.
//  Copyright © 2017 evampsaanga. All rights reserved.
//

import UIKit

class CountryView: UIView {

    @IBOutlet var countryTitle_lbl: UILabel!
    @IBOutlet var countryIcon_img: UIImageView!
    @IBOutlet var roamingDetailsCountriesListView: UIView!

    @IBOutlet var roamingDetailsCountriesListHeightConstraint: NSLayoutConstraint!
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

    class func instanceFromNib() -> UIView {
        return UINib(nibName: "CountryView", bundle: Bundle.main).instantiate(withOwner: nil, options: nil)[0] as! CountryView
    }
    
    func setRoamingDetailsCountriesListLayout (aRoamingDetailsCountries : RoamingDetailsCountries?) -> CGFloat {

        if let aRoamingDetailCountry = aRoamingDetailsCountries {

            countryTitle_lbl.text = aRoamingDetailCountry.countryName?.trimmWhiteSpace ?? ""
//            countryTitle_lbl.text = "Hello there"
            countryIcon_img.image = UIImage(named: aRoamingDetailCountry.flag ?? "dummy_flag")
//            countryIcon_img.backgroundColor = UIColor.MBLightGrayColor
            countryIcon_img.layer.cornerRadius = 4

            roamingDetailsCountriesListView.subviews.forEach({ (aLabel) in
                aLabel.removeFromSuperview()
            })


            if let operatorList = aRoamingDetailCountry.operatorList {

                var lastLabel : UILabel?

                operatorList.forEach({ (aOperator) in

                    let aLabel : UILabel = UILabel()
                    aLabel.textColor = countryTitle_lbl.textColor
                    aLabel.font = countryTitle_lbl.font

                    aLabel.translatesAutoresizingMaskIntoConstraints = false

                    aLabel.text = aOperator ?? ""
                    aLabel.textAlignment = NSTextAlignment.right

                    roamingDetailsCountriesListView.addSubview(aLabel)

                    let leading = NSLayoutConstraint(item: aLabel, attribute: NSLayoutConstraint.Attribute.leading, relatedBy: NSLayoutConstraint.Relation.equal, toItem: roamingDetailsCountriesListView, attribute: NSLayoutConstraint.Attribute.leading, multiplier: 1, constant: 0)
                    let trailing = NSLayoutConstraint(item: aLabel, attribute: NSLayoutConstraint.Attribute.trailing, relatedBy: NSLayoutConstraint.Relation.equal, toItem: roamingDetailsCountriesListView, attribute: NSLayoutConstraint.Attribute.trailing, multiplier: 1, constant: 0)

                    var top : NSLayoutConstraint = NSLayoutConstraint()

                    if lastLabel == nil {

                        top = NSLayoutConstraint(item: aLabel, attribute: NSLayoutConstraint.Attribute.top, relatedBy: NSLayoutConstraint.Relation.equal, toItem: roamingDetailsCountriesListView, attribute: NSLayoutConstraint.Attribute.top, multiplier: 1, constant: 0)

                    } else {
                        top = NSLayoutConstraint(item: aLabel, attribute: NSLayoutConstraint.Attribute.top, relatedBy: NSLayoutConstraint.Relation.equal, toItem: lastLabel, attribute: NSLayoutConstraint.Attribute.bottom, multiplier: 1, constant: 0)
                    }



                    let height = NSLayoutConstraint(item: aLabel, attribute: NSLayoutConstraint.Attribute.height, relatedBy: NSLayoutConstraint.Relation.equal, toItem: nil, attribute: NSLayoutConstraint.Attribute.notAnAttribute, multiplier: 1, constant: 15)
                    
                    roamingDetailsCountriesListView.addConstraints([leading, trailing, top, height])
                    
                    lastLabel = aLabel
                })
            }

            var countryDetailListHeight : CGFloat = 0.0

            if let count = aRoamingDetailCountry.operatorList?.count {
                countryDetailListHeight = CGFloat(count * 15)
            }

            //Adding top content height
            countryDetailListHeight += 30
            roamingDetailsCountriesListHeightConstraint.constant = countryDetailListHeight


            return countryDetailListHeight

        }
        return 0.0
    }

}


