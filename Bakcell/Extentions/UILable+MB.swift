//
//  UILable+MB.swift
//  Bakcell
//
//  Created by AbdulRehman Warraich on 8/30/17.
//  Copyright © 2017 evampsaanga. All rights reserved.
//

import Foundation
import UIKit
import MarqueeLabel

extension UILabel {
    
    func heightToFitContent() -> CGFloat {
        
        self.numberOfLines = 0
        self.lineBreakMode = NSLineBreakMode.byWordWrapping
        let maximumLabelSize : CGSize = CGSize(width: (self.frame.size.width - 60 ), height: CGFloat.greatestFiniteMagnitude)
        let expectedLabelSize : CGSize = self.sizeThatFits(maximumLabelSize);
        
        
        self.sizeToFit()
        
        return expectedLabelSize.height
    }
    
    
    func loadHTMLString(htmlString: String) {
        if let htmlData = htmlString.data(using: String.Encoding.unicode) {
            do {
                let attributedString = try NSMutableAttributedString(data: htmlData,
                                                                     options: [.documentType: NSAttributedString.DocumentType.html],
                                                                     documentAttributes: nil)
                
                attributedString.addAttribute(NSAttributedString.Key.font,
                                              value: self.font ?? UIFont.systemFont(ofSize: UIFont.systemFontSize),
                                              range: NSRange(location: 0 , length: attributedString.length))
                
                attributedString.addAttribute(NSAttributedString.Key.foregroundColor,
                                              value: self.textColor ?? UIColor.MBTextGrayColor,
                                              range: NSRange(location: 0 , length: attributedString.length))
                
                self.attributedText = attributedString
                
                
            } catch let e as NSError {
                print("Couldn't parse \(htmlString): \(e.localizedDescription)")
                
                self.text = ""
            }
        }
    }
}

extension MarqueeLabel {
    
    func setupMarqueeAnimation() {
        self.type = .continuous
        self.animationCurve = .easeInOut
        self.speed = .rate(30)
        self.leadingBuffer = 0
        self.trailingBuffer = 0
        self.fadeLength = 4
    }
    
    func startMarqueeAnimation() {
        self.unpauseLabel()
        self.holdScrolling = false
    }
    
    func stopMarqueeAnimation() {
        self.holdScrolling = true
        self.pauseLabel()
        
    }
}

