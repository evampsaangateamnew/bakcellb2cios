//
//  VASVC.swift
//  Bakcell
//
//  Created by Saad Riaz on 6/21/17.
//  Copyright © 2017 evampsaanga. All rights reserved.
//

import UIKit

class VASVC: BaseVC, UITableViewDataSource, UITableViewDelegate {
    
    // MARK: - Properties
    var images: [String] = ["RBT","FF"]
    var titleArr: [String] = ["FRBT","FF"]
    
    // MARK: - IBOutlet
    @IBOutlet var tableView: UITableView!
    @IBOutlet var vasTitle_lbl: UILabel!
    
    // MARK: - View Controller Methosd
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.interactivePop(true)
        
        loadViewLayout()
    }
    
    //MARK: - FUNCTIONS
    func loadViewLayout(){
        vasTitle_lbl.text = Localized("Title_VAS")
    }
    
    //MARK: TABLE VIEW METHODS
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return titleArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellID", for: indexPath) as! AccountsCell
        cell.title_lbl.text = titleArr[indexPath.row]
        cell.img.image = UIImage (named: images[indexPath.row])
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.row {
        case 0:
            break
        case 1:
            let ff = self.myStoryBoard.instantiateViewController(withIdentifier: "FandFVC") as! FandFVC
            self.navigationController?.pushViewController(ff, animated: true)
        default:
            break
        }
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    
}
