//
//  StoreListVC.swift
//  Bakcell
//
//  Created by Saad Riaz on 6/7/17.
//  Copyright © 2017 evampsaanga. All rights reserved.
//

import UIKit


class StoreListVC: BaseVC {
    
    //MARK:- Properties
    var storeLocatorData : StoreLocatorHandler?
    var storeLocatorFilter : StoreLocatorHandler?
    
    var selectedCity : String = Localized("DropDown_All")
    var selectedType : String = Localized("DropDown_All")
    
    var selectedSectionIndexs: [Int] = []
    
    //MARK:- IBOutlet
    @IBOutlet var storeListDrop: UIDropDown!
    @IBOutlet var storeCenterDrop: UIDropDown!
    @IBOutlet var tableView: MBAccordionTableView!
    
    //MARK: - ViewController Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        storeLocatorFilter = storeLocatorData?.copy()
        setUPDropBox()
        
        tableView.allowMultipleSectionsOpen = true
        tableView.allowsSelection = false
        tableView.delegate = self
        tableView.dataSource = self
        
        tableView.register(UINib(nibName: "StoreLocatorExpandCell", bundle: nil), forCellReuseIdentifier: "expandID")
        tableView.register(UINib(nibName: "StoreLocatorHeaderView", bundle: nil), forHeaderFooterViewReuseIdentifier: StoreLocatorHeaderView.kAccordionHeaderViewReuseIdentifier)
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.interactivePop(true)
        
        //EventLog
        MBAnalyticsManager.logEvent(screenName: "Store Locator", contentType:"Store Locator List" , status:"Store Locator List" )
    }
    
    
    //MARK:- IBACTIONS
    
    @objc func showLocationOnMap(sender:UIButton) {
        
        if let aStoreLocation = storeLocatorFilter?.stores?[sender.tag] {
       
            let cordinatesDict = ["lati": aStoreLocation.latitude,
                                  "longi" : aStoreLocation.longitude,
                                  "title": aStoreLocation.store_name,
                                  "address" : aStoreLocation.address,
                                  "type": aStoreLocation.type]
            
            if let parentController = self.parent as? StoreLocatorVC {
                
                parentController.focusUserToSelectedLocation(selectedLocationDetail: cordinatesDict)
                
            } else {
                self.showErrorAlertWithMessage(message: Localized("Message_UnexpectedError"))
            }
            
        } else {
            
            self.showErrorAlertWithMessage(message: Localized("Message_UnexpectedError"))
        }
    }
    
    @objc func initiateCall(sender : UIButton) {
        dialNumber(number: sender.titleLabel?.text ?? "")
    }
    
    
    //MARK: - FUNCTIONS
    
    func setUPDropBox() {
        
        // set storeCenterDrop
        storeCenterDrop.layer.borderWidth = 0
        storeCenterDrop.textAlignment = NSTextAlignment .center
        storeCenterDrop.placeholder = Localized("DropDown_All")
        storeCenterDrop.rowBackgroundColor = UIColor.MBDimLightGrayColor
        storeCenterDrop.setFont = UIFont.MBArial(fontSize: 12)
        
        var optionsArray : [String] = [Localized("DropDown_All")]
        
        if let selectedCenter = storeLocatorData?.type {
            
            for center in selectedCenter{
                
                optionsArray.append(center)
            }
        }
        
        if optionsArray.count < 10 {
            storeCenterDrop.tableHeight = CGFloat(35 * optionsArray.count)
        } else {
            storeCenterDrop.tableHeight = 325
        }
        
        storeCenterDrop.options = optionsArray
        
        storeCenterDrop.didSelect { (option, index) in
            
            self.selectedType = option
            
            self.storeLocatorFilter?.stores = self.filterStoreBy(city: self.selectedCity, type: self.selectedType, StoreDetails: self.storeLocatorData?.stores?.copy())
            
            self.tableView.reloadData()
            
        }
        
        // set storeListDrop
        storeListDrop.layer.borderWidth = 0
        storeListDrop.textAlignment = NSTextAlignment .center
        storeListDrop.placeholder = Localized("DropDown_All")
        storeListDrop.rowBackgroundColor = UIColor.MBDimLightGrayColor
        storeListDrop.setFont = UIFont.MBArial(fontSize: 12)
        
        var optionsArrayCity : [String] = [Localized("DropDown_All")]
        
        
        if let selectedCenter = storeLocatorData?.city {
            
            for center in selectedCenter{
                
                optionsArrayCity.append(center)
            }
        }
        if optionsArrayCity.count < 10 {
            storeListDrop.tableHeight = CGFloat(35 * optionsArrayCity.count)
        } else {
            storeListDrop.tableHeight = 325
        }
        
        storeListDrop.options = optionsArrayCity
        
        storeListDrop.didSelect { (option, index) in
            
            self.selectedCity = option
            
            self.storeLocatorFilter?.stores = self.filterStoreBy(city: self.selectedCity, type: self.selectedType, StoreDetails: self.storeLocatorData?.stores?.copy())
            
            self.tableView.reloadData()
            
        }
    }
    
    func filterStoreBy(city:String?, type:String?, StoreDetails : [Stores]? ) -> [Stores]? {
        
        guard let myType = type else {
            return nil
        }
        
        guard let myCity = city else {
            return nil
        }
        
        guard let StoreListDetails = StoreDetails else {
            return nil
        }
        
        if(myCity.isEqualToStringIgnoreCase(otherString: Localized("DropDown_All")) == true && myType.isEqualToStringIgnoreCase(otherString: Localized("DropDown_All")) == true) {
            
            return StoreDetails
            
        } else if(myCity.isEqualToStringIgnoreCase(otherString: Localized("DropDown_All")) == false && myType.isEqualToStringIgnoreCase(otherString: Localized("DropDown_All")) == false) {
            
            var  filterdStoreDetails : [Stores] = []
            
            for aStoreItem in StoreListDetails {
                
                if aStoreItem.city.lowercased() == myCity.lowercased() {
                    
                    if aStoreItem.type.lowercased() == myType.lowercased() {
                        
                        filterdStoreDetails.append(aStoreItem)
                    }
                }
            }
            return filterdStoreDetails
            
        } else if (myCity.isEqualToStringIgnoreCase(otherString: Localized("DropDown_All")) == false && myType.isEqualToStringIgnoreCase(otherString: Localized("DropDown_All")) == true) {
            let  filterdStoreDetails = StoreListDetails.filter() {
                if ($0 as Stores).city.lowercased() == myCity.lowercased() {
                    return true
                } else {
                    return false
                }
            }
            return filterdStoreDetails
            
        } else if (myCity.isEqualToStringIgnoreCase(otherString: Localized("DropDown_All")) == true && myType.isEqualToStringIgnoreCase(otherString: Localized("DropDown_All")) == false ) {
            let  filterdStoreDetails = StoreListDetails.filter() {
                if ($0 as Stores).type.lowercased() == myType.lowercased() {
                    return true
                } else {
                    return false
                }
                
            }
            return filterdStoreDetails
            
        } else {
            return StoreDetails
        }
        
    }
}

// MARK: - <UITableViewDataSource> / <UITableViewDelegate>
extension StoreListVC : UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return storeLocatorFilter?.stores?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 1
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        var contactsHeight = 0
        
        if let aContact = storeLocatorFilter?.stores?[indexPath.section].contactNumbers{
            if aContact.isEmpty == false {
                contactsHeight = 26
            }
        }
        
        let timingCount = storeLocatorFilter?.stores?[indexPath.section].timing?.count ?? 0
        
        if timingCount == 5{
            //return 115.0
            return CGFloat(89 + contactsHeight)
            
        } else if timingCount == 4{
            //return 95
            return CGFloat(69 + contactsHeight)
            
        } else if timingCount == 3{
            // return 75
            return CGFloat(49 + contactsHeight)
            
        } else if timingCount == 2{
            //return 60
            return CGFloat(39 + contactsHeight)
            
        } else if timingCount == 1{
            //return 50
            return CGFloat(29 + contactsHeight)
            
        } else{
            //return 40
            return CGFloat(10 + contactsHeight)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 70.0
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "expandID", for: indexPath) as! StoreLocatorExpandCell
        
        let aStoreList = storeLocatorFilter?.stores?[indexPath.section]
        
        if  aStoreList?.contactNumbers?.isEmpty == true {
            cell.contacts_img.isHidden = true
            cell.contactNumbers_View.isHidden = true
            cell.contactImgHeightConstraint.constant = 0
        }
        
        if (aStoreList?.timing?.count ?? 0) > 0  {
            cell.weekDays_lbl.text = aStoreList?.timing?[indexPath.row].day ?? ""
            cell.weedaysTimings_lbl.text = aStoreList?.timing?[indexPath.row].timings ?? ""
        }
        else{
            cell.weekDays_lbl.text = nil
            cell.weedaysTimings_lbl.text = nil
        }
        if (aStoreList?.timing?.count ?? 0) > 1  {
            cell.week_lbl.text = aStoreList?.timing?[indexPath.row + 1].day ?? ""
            cell.weekTimings_lbl.text = aStoreList?.timing?[indexPath.row + 1].timings ?? ""
        }
        else{
            cell.week_lbl.text = nil
            cell.weekTimings_lbl.text = nil
        }
        if (aStoreList?.timing?.count ?? 0) > 2 {
            cell.weekend_lbl.text = aStoreList?.timing?[indexPath.row + 2].day ?? ""
            cell.weekendTimings_lbl.text = aStoreList?.timing?[indexPath.row + 2].timings ?? ""
        } else {
            cell.weekend_lbl.text = nil
            cell.weekendTimings_lbl.text = nil
        }
        if (aStoreList?.timing?.count ?? 0) > 3 {
            cell.holidays_lbl.text = aStoreList?.timing?[indexPath.row + 3].day ?? ""
            cell.holidaysTimings_lbl.text = aStoreList?.timing?[indexPath.row + 3].timings ?? ""
        }
        else{
            cell.holidays_lbl.text = nil
            cell.holidaysTimings_lbl.text = nil
        }
        if (aStoreList?.timing?.count ?? 0) > 4 {
            cell.weeks_lbl.text = aStoreList?.timing?[indexPath.row + 4].day ?? ""
            cell.timings_lbl.text = aStoreList?.timing?[indexPath.row + 4].timings ?? ""
        }
        else{
            cell.weeks_lbl.text = nil
            cell.timings_lbl.text = nil
        }
        
        if let numbers = aStoreList?.contactNumbers {
            if numbers.isEmpty == false{
                
                cell.contactNumbers_View.isHidden = false
                cell.contacts_img.isHidden = false
                cell.contactImgHeightConstraint.constant = 24
                
                /*
                 let numbersAttributedString: NSMutableAttributedString = NSMutableAttributedString(string: "")
                 numbers.forEach({ (aNumber) in
                 let aAttributedString = NSMutableAttributedString(string: "\(aNumber)  ", attributes: [NSForegroundColorAttributeName: UIColor.MBTextGrayColor])
                 numbersAttributedString.append(aAttributedString)
                 })
                 */
                
                if numbers.count >= 1 {
                    cell.call_btn1.setTitle(numbers[0], for: UIControl.State.normal)
                }
                
                if numbers.count >= 2 {
                    cell.call_btn2.setTitle(numbers[1], for: UIControl.State.normal)
                }
                
                if numbers.count >= 3 {
                    cell.call_btn3.setTitle(numbers[2], for: UIControl.State.normal)
                }
                
                cell.call_btn1.addTarget(self, action: #selector(initiateCall(sender:)), for: UIControl.Event.touchUpInside)
                cell.call_btn2.addTarget(self, action: #selector(initiateCall(sender:)), for: UIControl.Event.touchUpInside)
                cell.call_btn3.addTarget(self, action: #selector(initiateCall(sender:)), for: UIControl.Event.touchUpInside)
                
                
            } else {
                cell.contactNumbers_View.isHidden = true
                cell.contactImgHeightConstraint.constant = 0
                cell.contacts_img.isHidden = true
                cell.contactNumbers_View.isHidden = true
            }
            
        } else{
            cell.contactNumbers_View.isHidden = true
            cell.contactImgHeightConstraint.constant = 0
            cell.contacts_img.isHidden = true
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: StoreLocatorHeaderView.kAccordionHeaderViewReuseIdentifier) as! StoreLocatorHeaderView
        headerView.location_btn.tag = section
        
        if let aStoreLocation = storeLocatorFilter?.stores?[section] {
            
            headerView.storeName_lbl.text = aStoreLocation.store_name
            headerView.storeAddress_lbl.text = aStoreLocation.address
            
            headerView.location_btn.setImage(MBUtilities.markerIconImageFor(key: aStoreLocation.type, inSmallSize: true), for: UIControl.State.normal)
            headerView.location_btn.addTarget(self, action: #selector(showLocationOnMap), for: UIControl.Event.touchUpInside)
            
            
        } else {
            headerView.storeName_lbl.text =  ""
            headerView.storeAddress_lbl.text = ""
            
            headerView.location_btn.setImage(MBUtilities.markerIconImageFor(key: "", inSmallSize: true), for: UIControl.State.normal)
            headerView.location_btn.removeTarget(self, action: #selector(showLocationOnMap), for: UIControl.Event.touchUpInside)
        }

        // Check if section is expanded then set expaned image else set unexpended image
        if selectedSectionIndexs.contains(section) {
            headerView.expandIcon_img.image = #imageLiteral(resourceName: "minu-sign")
        } else {
            headerView.expandIcon_img.image = #imageLiteral(resourceName: "Plus-Sign")
        }
        return headerView
    }
}

// MARK: - <MBAccordionTableViewDelegate>

extension StoreListVC : MBAccordionTableViewDelegate {
    
    func tableView(_ tableView: MBAccordionTableView, didOpenSection section: Int, withHeader header: UITableViewHeaderFooterView?) {
        
        if let headerView = header as? StoreLocatorHeaderView {
            headerView.expandIcon_img.image = #imageLiteral(resourceName: "minu-sign")
            
            // Adding section from selectedSectionIndexs
            selectedSectionIndexs.append(section)
        }
    }
    
    func tableView(_ tableView: MBAccordionTableView, didCloseSection section: Int, withHeader header: UITableViewHeaderFooterView?) {
        
        if let headerView = header as? StoreLocatorHeaderView {
            headerView.expandIcon_img.image = #imageLiteral(resourceName: "Plus-Sign")
            
            // Removing section from selectedSectionIndexs
            if let sectionValue = selectedSectionIndexs.firstIndex(of: section) {
                selectedSectionIndexs.remove(at: sectionValue)
            }
        }
    }
    
    func tableView(_ tableView: MBAccordionTableView, canInteractWithHeaderAtSection section: Int) -> Bool {
        return true
    }
}
