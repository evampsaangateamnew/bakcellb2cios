//
//  RateUsAlertVC.swift
//  Bakcell B2B
//
//  Created by Waqar Ahmed on 7/9/18.
//  Copyright © 2018 Evamp&Saanga. All rights reserved.
//

import UIKit

class RateUsAlertVC: BaseVC {

    //MARK : - Properties
    fileprivate var rateUsButtonCompletionBlock : MBButtonCompletionHandler = {_ in }
    fileprivate var rateUsButtonCompletionBlock2 : MBButtonCompletionHandler = {_ in }
    
    //MARK: - IBOutlets
    @IBOutlet var mainView: MBView!
    @IBOutlet var alertTitleLabel: UILabel!
    @IBOutlet var alertConfirmationsMessageLabel: UILabel!
    @IBOutlet var alertMessageLabel: UILabel!
    
    @IBOutlet var rateNowButton: MBMarqueeButton!
    @IBOutlet var cancelButton: MBMarqueeButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        rateNowButton.backgroundColor = UIColor.MBButtonBackgroundGrayColor
        rateNowButton.setTitleColor(UIColor.MBTextBlack, for: UIControl.State.normal)
        rateNowButton.roundAllCorners(radius: 16)
        rateNowButton.layer.borderColor = UIColor.MBButtonBackgroundGrayColor.cgColor
        rateNowButton.layer.borderWidth = 1
        
        
        cancelButton.backgroundColor = UIColor.white
        cancelButton.setTitleColor(UIColor.MBTextGrayColor, for: UIControl.State.normal)
        cancelButton.roundAllCorners(radius: 16)
        cancelButton.layer.borderColor = UIColor.MBBorderGrayColor.cgColor
        cancelButton.layer.borderWidth = 1

    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.interactivePop(false)
        
        loadViewLayout()
    }
    
    //MARK: - Functions
    func loadViewLayout()  {
        cancelButton.setTitle(Localized("BtnTitle_MaybeLater"), for: .normal)
        rateNowButton.setTitle(Localized("BtnTitle_RateNow"), for: .normal)
        alertTitleLabel.text = Localized("Title_RateUs")
        alertConfirmationsMessageLabel.text =  RateUsAlertVC.getTitle()
        alertMessageLabel.text = RateUsAlertVC.getMessage()
        
    }

    //MARK: - IBActions
    @IBAction func rateNowButtonAction(_ sender: UIButton) {
        
        /* Save that Rate Us screen presented to user */
        MBUtilities.updateRateUsShownStatus(isShow: true)
        
        /* Save User interaction time with rateus screen */
        MBUtilities.updateRateUsLaterTime()
        
        /* Call Rate Us API to update status*/
        // self.callRateUs()
        
        self.dismiss(animated: false, completion:{
            
            self.rateUsButtonCompletionBlock("")
        })
        
    }
    
    @IBAction func laterButtonAction(_ sender: UIButton) {
        
        /* Save that Rate Us screen presented to user */
        MBUtilities.updateRateUsShownStatus(isShow: true)
        
        /* Save User interaction time with rateus screen */
        MBUtilities.updateRateUsLaterTime()
        
        self.dismiss(animated: true, completion:{
            self.rateUsButtonCompletionBlock2("")
        })
    }
    
    //MARK:- Functions
    func setRateUs(_ block : @escaping MBButtonCompletionHandler = {_ in}) {
        rateUsButtonCompletionBlock = block
    }
    func setRateUsLater(_ block : @escaping MBButtonCompletionHandler = {_ in}) {
        rateUsButtonCompletionBlock2 = block
    }
    
    /*func callRateUs() {
        
        _ = MBAPIClient.sharedClient.rateUs({ (response, resultData, error, isCancelled, status, resultCode, resultDesc)  in
            if error != nil {
                error?.showServerErrorInViewController(self)
            } else {
            }
        })
    }
 */
    

}

// Public class methods
extension RateUsAlertVC {
    
    public class func getTitle() -> String {
        if let titleString = MBUserSession.shared.userInfo?.rateUsPopupTitle,
            titleString.isBlank == false {
            return titleString
        }
        return Localized("Message_DoYouLikeOurApp")
    }
    
    public class func getMessage() -> String {
        if let messageString = MBUserSession.shared.userInfo?.rateUsPopupContent,
            messageString.isBlank == false {
            return messageString
        }
        return Localized("Message_RatingMessage")
    }
}
