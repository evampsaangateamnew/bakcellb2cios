//
//  Data+MB.swift
//  Bakcell
//
//  Created by AbdulRehman Warraich on 10/7/17.
//  Copyright © 2017 evampsaanga. All rights reserved.
//

import Foundation

extension Data {
    func format() -> String {

        let array = [UInt8](self)
        let ext: String
        switch (array[0]) {
        case 0xFF:
            ext = ".jpg"
        case 0x89:
            ext = ".png"
        case 0x47:
            ext = ".gif"
        case 0x49, 0x4D :
            ext = ".tiff"
        default:
            ext = "unknown"
        }
        return ext
    }
}
