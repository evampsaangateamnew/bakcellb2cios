//
//  MBUtilities.swift
//  Bakcell
//
//  Created by AbdulRehman Warraich on 8/24/17.
//  Copyright © 2017 evampsaanga. All rights reserved.
//

import Foundation
import UIKit
import ObjectMapper


//MARK: - < User information related Utilities >
class MBUtilities : NSObject {
    
    class func load_Old_CustomerDataFromUserDefaults() -> (isLoggedIn :Bool, oldUserInfo: CustomerData?) {
        if let data = UserDefaults.standard.object(forKey:Constants.kUserInfoKey) as? NSData {
            
            let unarc = NSKeyedUnarchiver(forReadingWith: data as Data)
            let userInformation = unarc.decodeObject(forKey: "root") as? CustomerData
            return (true,userInformation)
        }
        return (false,nil)
    }
    
    public class func updateLoginTime() {
        UserDefaults.standard.set(MBUtilities.todayDateString(dateFormate: Constants.kHomeDateFormate), forKey: Constants.K_UserLoggedInTime)
    }
    
    public class func getLoginTime() -> String {
        if let loginTime = UserDefaults.standard.string(forKey: Constants.K_UserLoggedInTime),
            loginTime.isBlank == false {
            return loginTime
        }
        return ""
    }
    
    public class func updateRateUsLaterTime() {
        UserDefaults.standard.set(MBUtilities.todayDateString(dateFormate: Constants.kHomeDateFormate), forKey: Constants.K_RateUsLaterTime)
    }
    
    public class func getRateUsLaterTime() -> String {
        if let rateUsTime = UserDefaults.standard.string(forKey: Constants.K_RateUsLaterTime),
            rateUsTime.isBlank == false {
            return rateUsTime
        }
        
        return ""
    }
    
    public class func updateNotificaitonsPopupTime() {
        UserDefaults.standard.set(MBUtilities.todayDateString(dateFormate: Constants.kHomeDateFormate), forKey: Constants.K_NotificationPopUpTime)
    }
    
    
    public class func getNotificaitonsPopupTime() -> String {
        if let rateUsTime = UserDefaults.standard.string(forKey: Constants.K_NotificationPopUpTime),
            rateUsTime.isBlank == false {
            return rateUsTime
        }
        
        return ""
    }
    
    public class func updateRateUsShownStatus(isShow :Bool) {
        UserDefaults.standard.set(isShow, forKey: Constants.K_IsRateUsShownBefore)
    }
    
    public class func isRateUsShownBefore() -> Bool {
        return UserDefaults.standard.bool(forKey: Constants.K_IsRateUsShownBefore)
    }
    
    public class func isLaunchedBefore() -> Bool {
        return UserDefaults.standard.bool(forKey: Constants.kIsLaunchedBefore)
    }
    
    public class func updateLaunchedBeforeStatus() {
        UserDefaults.standard.set("YES", forKey:Constants.kIsLaunchedBefore)
        
    }
    
    public class func isTutorialShowenBefore() -> Bool {
        return UserDefaults.standard.bool(forKey: Constants.kIsTutorialShowenBefore)
    }
    
    public class func updateTutorialShowenStatus() {
        UserDefaults.standard.set(true, forKey:Constants.kIsTutorialShowenBefore)
        
    }
    
    public class func iconImageFor(key: String?) -> UIImage{
        
        if key != nil {
            
            let iconKeyString = key?.lowercased() ?? ""
            var iconName = ""
            
            switch (iconKeyString) {
            case "calls":
                iconName = "ic_calls"
                
            case "internet":
                iconName = "ic_internet"
                
            case "whatsapp":
                iconName = "ic_whatsapp"
                
            case "phone":
                iconName = "ic_calls"
                
            case "sms":
                iconName = "ic_sms"
                
                //Tariff
                
            case "discount":
                iconName = ""
                
            case "free":
                iconName = ""
                
            case "tsdcall":
                iconName = "ic_calls"
                
            case "tsdsms":
                iconName = "ic_sms"
                
            case "tsdmms":
                iconName = "ic_mms"
                
            case "tsdinternet":
                iconName = "ic_internet"
                
            case "tsddistination":
                iconName = "ic_destination"
                
            case "tsdwhatsapp":
                iconName = "ic_whatsapp"
                
            case "tsdrounding":
                iconName = "ic_rounding-off"
                
            case "countrywidecall":
                iconName = "ic_calls"
                
            case "internationalcalls":
                iconName = "ic_calls_international"
                
            case "roamingcalls":
                iconName = "ic_calls_roaming"
                
            case "countrywidesms":
                iconName = "ic_sms"
                
            case "countrywideinternet":
                iconName = "ic_internet"
                
            case "roaminginternet":
                iconName = "ic_internet"
                
            case "whatsappusing":
                iconName = "ic_whatsapp"
                
            case "youtube":
                iconName = "ic_youtube"
                
            case "instagram":
                iconName = "ic_insta"
                
            case "facebook":
                iconName = "ic_facebook"
                
            case "mms":
                iconName = "ic_mms"
                
            case "campaign":
                iconName = "ic_campaing"
                
            case "tmrequired":
                iconName = "ic_tmrequired"
                
            case "tmpresented":
                iconName = "ic_tmpresented"
                
            case "campaigndiscountedcalls":
                iconName = "ic_campaigndiscountedcall"
                
            case "campaigndiscountedinternet":
                iconName = "ic_campaigndiscountedinternet"
                
            case "campaigndiscountedsms":
                iconName = "ic_campaigndiscountedsms"
                
            default:
                iconName = "" //Checkbox-state2
            }
            
            if iconName.isBlank == true {
                return UIImage()
            } else {
                return UIImage(named: iconName) ?? UIImage()
            }
            
        } else {
            return UIImage()
        }
    }
    
    public class func markerIconImageFor(key: String?, inSmallSize:Bool = false) -> UIImage{
        
        
        if key != nil {
            
            let iconKeyString = key?.lowercased() ?? ""
            var iconName = ""
            
            switch (iconKeyString) {
            case "Head Office".lowercased(), "Mərkəzi Ofis".lowercased(), "Центральный Офис".lowercased() :
                iconName = "headOfficeMarker"
                
            case "Customer care offices".lowercased(), "Müştəri xidmətləri mərkəzləri".lowercased(), "Офисы обслуживания".lowercased() :
                iconName = "serviceCenterMarker"
                
            case "Bakcell-IM".lowercased(), "Bakcell-IM".lowercased(), "Bakcell-IM".lowercased() :
                iconName = "bakcellimsMarker"
                
            case "Dealer stores".lowercased(), "Diler mağazaları".lowercased(), "Дилерские магазины".lowercased() :
                iconName = "dealerShopMarker"
                
            default:
                iconName = "bakcellimsMarker"
            }
            
            if iconName.isBlank == true {
                return UIImage()
            } else {
                if inSmallSize == true {
                    iconName += "_Small"
                }
                return UIImage(named: iconName) ?? UIImage()
            }
            
        } else {
            return UIImage()
        }
    }
    
    public class func createAttributedTextWithManatSign(_ textString: String?, textColor: UIColor = .MBTextGrayColor) -> NSMutableAttributedString {
        
        // Return Empty string is text is emty
        if textString?.isBlank == true {
            return NSMutableAttributedString(string: "" ,attributes: [ NSAttributedString.Key.font: UIFont.MBArial(fontSize: 14)])
        }
        
        let amountAttributedText = NSMutableAttributedString(string: "\(textString ?? "") ₼" ,attributes: [ NSAttributedString.Key.font: UIFont.MBArial(fontSize: 14), NSAttributedString.Key.foregroundColor: textColor])
        
        amountAttributedText.addAttribute(NSAttributedString.Key.font,
                                          value: UIFont.systemFont(ofSize: 10),
                                          range: NSRange(location: (amountAttributedText.length - 1), length: 1))
        
        amountAttributedText.addAttribute(NSAttributedString.Key.baselineOffset,
                                          value:2.00,
                                          range: NSRange(location:(amountAttributedText.length - 1),length:1))
        
        return amountAttributedText
        
        
    }
    
    public class func getTariffMigrationConfirmationMessage(offeringID :String?) -> String {
        
        var changeMessageString = Localized("Message_MigrationDefaultConfirmationMessage")
        if let selectedTariffMessage = MBUserSession.shared.predefineData?.tariffMigrationPrices.filter({$0.key?.isEqualToStringIgnoreCase(otherString: offeringID) ?? false}).first?.value,
            selectedTariffMessage.isBlank == false {
            
            changeMessageString = selectedTariffMessage
            
        } else if let changeTariffDefaultMessage =
        MBUserSession.shared.predefineData?.tariffMigrationPrices.filter({$0.key?.isEqualToStringIgnoreCase(otherString: "default") ?? false}).first?.value,
            changeTariffDefaultMessage.isBlank == false {
            
            changeMessageString = changeTariffDefaultMessage
            
        }
        
        return changeMessageString
    }
    
}

extension MBUtilities {
    func readJSONStringFromFile() -> String {
        
        if let filePath = Bundle.main.path(forResource: "JSONTextFile", ofType: "txt") {
            let fileURL = URL(fileURLWithPath: filePath)
            
            //reading
            do {
                let text2 = try String(contentsOf: fileURL, encoding: .utf8)
                
                return text2
            }
            catch {/* error handling here */
                return "Error occur during file reading"
            }
        } else {
            return "Error occur during file reading"
        }
    }
}


//MARK: - < Date Formator related Utilities >
extension MBUtilities {
    
    //Current Date
    class func todayDate(dateFormate: String = Constants.kHomeDateFormate ) -> Date {
        //        return Date()
        
        let now = Date()
        
        MBUserSession.shared.dateFormatter.timeZone = TimeZone.current
        
        MBUserSession.shared.dateFormatter.dateFormat = dateFormate
        
        let currentDateString = MBUserSession.shared.dateFormatter.string(from: now)
        
        MBUserSession.shared.dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        
        return MBUserSession.shared.dateFormatter.date(from: currentDateString) ?? now
    }
    
    
    //Current Date String
    class func todayDateString(dateFormate: String = Constants.kHomeDateFormate ) -> String {
        MBUserSession.shared.dateFormatter.dateFormat = dateFormate
        return MBUserSession.shared.dateFormatter.string(from: self.todayDate(dateFormate: dateFormate))
    }
    
    class func todayDateString(withAdditionalValue additionalValue: Int) -> String {
        
        // ADD/Subtract value from date
        if let newDate = Calendar.current.date(byAdding: .day, value: additionalValue, to: MBUtilities.todayDate()) {
            MBUserSession.shared.dateFormatter.dateFormat = Constants.kAPIFormat
            
            let myDateString  = MBUserSession.shared.dateFormatter.string(from: newDate)
            
            return myDateString
            
        } else {
            return ""
        }
    }
    
    class func calculateTotalHours(start: String, end: String, dateFormate: String = Constants.kDisplayFormat ) -> Int {
        
        MBUserSession.shared.dateFormatter.dateFormat = dateFormate
        
        let startDate  = MBUserSession.shared.dateFormatter.date(from: start)
        let expireDate = MBUserSession.shared.dateFormatter.date(from: end)
        
        
        if startDate != nil && expireDate != nil {
            
            return calculateHoursBetweenTwoDates(start: startDate!, end: expireDate!)
            
        } else {
            return 0
        }
    }
    
    class func calculateHoursBetweenTwoDates(start: Date, end: Date, dateFormate: String = Constants.kHomeDateFormate) -> Int {
        
        MBUserSession.shared.dateFormatter.dateFormat = dateFormate
        
        let startDateStr = MBUserSession.shared.dateFormatter.string(from: start)
        let endDateStr = MBUserSession.shared.dateFormatter.string(from: end)
        
        guard let newStartDate = MBUserSession.shared.dateFormatter.date(from: startDateStr) else {
            return 0
        }
        guard let newEndDate = MBUserSession.shared.dateFormatter.date(from: endDateStr) else {
            return 0
        }
        
        //        var hours = Calendar.current.dateComponents([.hour], from: newStartDate, to: newEndDate).hour ?? 0
        
        let minute = Calendar.current.dateComponents([.minute], from: newStartDate, to: newEndDate).minute ?? 0
        
        if minute >= 60 {
            return Int((minute / 60))
        }
        
        return 0
    }
    
    //    class func calculateDaysLeftFrom(start: String, end: String) -> Int {
    //
    //        MBUserSession.shared.dateFormatter.dateFormat = Constants.kHomeDateFormate
    //
    //        let startDate  = MBUserSession.shared.dateFormatter.date(from: start)
    //        let expireDate = MBUserSession.shared.dateFormatter.date(from: end)
    //
    //
    //        if startDate != nil && expireDate != nil {
    //
    //            let totalDaysLeft =  calculateDaysBetweenTwoDates(start: startDate!, end: expireDate!)
    //
    //            if totalDaysLeft < 0 {
    //                return 0
    //            } else {
    //                return totalDaysLeft
    //            }
    //
    //        } else {
    //            return 0
    //        }
    //    }
    
    class func calculateDaysLeft(start: String, end: String, dateFormate: String = Constants.kHomeDateFormate ) -> Int {
        
        MBUserSession.shared.dateFormatter.dateFormat = dateFormate
        
        let startDate  = MBUserSession.shared.dateFormatter.date(from: start)
        let expireDate = MBUserSession.shared.dateFormatter.date(from: end)
        
        
        if startDate != nil && expireDate != nil {
            
            let totalDays =  calculateDaysBetweenTwoDates(start: startDate!, end: expireDate!)
            
            let usedDays =  calculateDaysBetweenTwoDates(start: startDate!, end: self.todayDate())
            
            let daysLeft : Int = totalDays - usedDays
            
            if daysLeft < 0 {
                return 0
            } else {
                return daysLeft
            }
            
        } else {
            return 0
        }
    }
    
    class func calculateTotalDays(start: String, end: String, dateFormate: String = Constants.kDisplayFormat ) -> Int {
        
        MBUserSession.shared.dateFormatter.dateFormat = dateFormate
        
        let startDate  = MBUserSession.shared.dateFormatter.date(from: start)
        let expireDate = MBUserSession.shared.dateFormatter.date(from: end)
        
        
        if startDate != nil && expireDate != nil {
            
            return calculateDaysBetweenTwoDates(start: startDate!, end: expireDate!)
            
        } else {
            return 0
        }
    }
    
    class func calculateDaysBetweenTwoDates(start: Date, end: Date) -> Int {
        
        MBUserSession.shared.dateFormatter.dateFormat = Constants.kAPIFormat
        
        let startDateStr = MBUserSession.shared.dateFormatter.string(from: start)
        let endDateStr = MBUserSession.shared.dateFormatter.string(from: end)
        
        guard let newStartDate = MBUserSession.shared.dateFormatter.date(from: startDateStr) else {
            return 0
        }
        guard let newEndDate = MBUserSession.shared.dateFormatter.date(from: endDateStr) else {
            return 0
        }
        
        
        if newStartDate == newEndDate {
            return 0
        } else {
            var currentCalendar = Calendar.current
            
            if let timeZone = TimeZone(abbreviation: "UTC") {
                currentCalendar.timeZone = timeZone
            } else {
                return 0
            }
            
            guard let startDay = currentCalendar.ordinality(of: .day, in: .era, for: newStartDate) else {
                return 0
            }
            guard let endDay = currentCalendar.ordinality(of: .day, in: .era, for: newEndDate) else {
                return 0
            }
            return endDay - startDay
        }
    }
    
    class func displayFormatedDateString(dateString: String) -> String {
        
        MBUserSession.shared.dateFormatter.dateFormat = Constants.kHomeDateFormate
        if let myDate  = MBUserSession.shared.dateFormatter.date(from: dateString) {
            
            MBUserSession.shared.dateFormatter.dateFormat = Constants.kDisplayFormat
            
            let myDateString  = MBUserSession.shared.dateFormatter.string(from: myDate)
            
            return myDateString
            
        } else {
            return ""
        }
    }
    
    class func displayFormatedDateString(from dateString: String, additionalValue: Int, returnDateFormate:String = Constants.kDisplayFormat) -> String {
        
        MBUserSession.shared.dateFormatter.dateFormat = Constants.kHomeDateFormate
        
        if let myDate  = MBUserSession.shared.dateFormatter.date(from: dateString) {
            
            // ADD/Subtract value from date
            if let newDate = Calendar.current.date(byAdding: .day, value: additionalValue, to: myDate) {
                MBUserSession.shared.dateFormatter.dateFormat = returnDateFormate
                
                let myDateString  = MBUserSession.shared.dateFormatter.string(from: newDate)
                
                return myDateString
                
            } else {
                return ""
            }
            
        } else {
            return ""
        }
    }
    
    class func calculateProgress(totalDays: Int = 0, daysLeft: Int = 0) -> Float {
        
        if totalDays <= 0 || daysLeft <= 0 {
            return 1.0
        } else {
            return Float(totalDays - daysLeft)/Float(totalDays)
        }
    }
    
    class func calculateProgressValues(from startDateString: String, to endDateString: String, AdditionalValueInEndDate additionalValue: Int = 0) -> (startDate: String, endDate: String, daysLeft: Int, daysLeftDisplayValue: String, totalDays: Int, progressValue: Float ) {
        
        MBUserSession.shared.dateFormatter.dateFormat = Constants.kHomeDateFormate
        MBUserSession.shared.dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        
        let startDateDisplayValue = MBUtilities.displayFormatedDateString(dateString: startDateString)
        let endDateDisplayValue = MBUtilities.displayFormatedDateString(dateString: endDateString)
        
        let dayBeforeEndDate =  MBUtilities.displayFormatedDateString(from: endDateString, additionalValue: additionalValue, returnDateFormate: Constants.kHomeDateFormate)
        
        
        let daysLeft = MBUtilities.calculateDaysLeft(start: startDateString, end: dayBeforeEndDate, dateFormate: Constants.kHomeDateFormate)
        
        let totalDays = MBUtilities.calculateTotalDays(start: startDateString, end: dayBeforeEndDate, dateFormate: Constants.kHomeDateFormate)
        
        var daysLeftUnit = ""
        
        if daysLeft <= 1 {
            daysLeftUnit = Localized("Info_day")
        } else {
            daysLeftUnit = Localized("Info_days")
        }
        let daysLeftDisplayValue = "\(daysLeft) \(daysLeftUnit)"
        
        let progress : Float =  MBUtilities.calculateProgress(totalDays: totalDays, daysLeft: daysLeft)
        
        return (startDateDisplayValue,endDateDisplayValue,daysLeft,daysLeftDisplayValue, totalDays,progress)
    }
    
    class func calculateProgressValuesForMRC(from startDateString: String, to endDateString: String, type: String) -> (startDate: String, endDate: String, daysLeft: Int, daysLeftDisplayValue: String, totalDays: Int, progressValue: Float ) {
        
        if type.isEqualToStringIgnoreCase(otherString: "daily") {
            
            MBUserSession.shared.dateFormatter.dateFormat = Constants.kHomeDateFormate
            MBUserSession.shared.dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
            
            let startDateDisplayValue = MBUtilities.displayFormatedDateString(dateString: startDateString)
            let endDateDisplayValue = MBUtilities.displayFormatedDateString(dateString: endDateString)
            
            let totalHours = MBUtilities.calculateTotalHours(start: startDateString, end: endDateString, dateFormate: Constants.kHomeDateFormate)
            
            var passedHours = MBUtilities.calculateTotalHours(start: startDateString, end: MBUtilities.todayDateString(dateFormate: Constants.kHomeDateFormate), dateFormate: Constants.kHomeDateFormate)
            
            if passedHours > 0 {
                passedHours = passedHours + 1
            }
            
            //  let currentHourValue = Date().currentHour()
            var remainingHours = totalHours - passedHours
            
            var hoursLeftUnit = ""
            
            if remainingHours <= 1 {
                hoursLeftUnit = Localized("title_Hours")
                
                if remainingHours <= 0 {
                    remainingHours = 0
                }
                
            } else {
                hoursLeftUnit = Localized("title_Hours")
            }
            let hoursLeftDisplayValue = "\(remainingHours) \(hoursLeftUnit)"
            
            let progress : Float =  MBUtilities.calculateProgress(totalDays: totalHours, daysLeft: remainingHours)
            
            
            return (startDateDisplayValue,endDateDisplayValue,remainingHours,hoursLeftDisplayValue, totalHours,progress)
            
        } else {
            
            return MBUtilities.calculateProgressValues(from: startDateString, to: endDateString, AdditionalValueInEndDate: -1)
        }
    }
    
    class func calculateProgressValuesForInstallment(from startDateString: String, to endDateString: String) -> (startDate: String, endDate: String, daysLeft: Int, daysLeftDisplayValue: String, totalDays: Int, progressValue: Float ) {
        
        
        MBUserSession.shared.dateFormatter.dateFormat = Constants.kHomeDateFormate
        MBUserSession.shared.dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        
        let startDateDisplayValue = MBUtilities.displayFormatedDateString(dateString: startDateString)
        let endDateDisplayValue = MBUtilities.displayFormatedDateString(dateString: endDateString)
        
        MBUserSession.shared.dateFormatter.dateFormat = Constants.kHomeDateFormate
        let startDateOfMonth = self.todayDate().startOfMonth()
        let endDateOfMonth = self.todayDate().endOfMonth()
        
        MBUserSession.shared.dateFormatter.dateFormat = Constants.kHomeDateFormate
        let startDateStringOfMonth = MBUserSession.shared.dateFormatter.string(from: startDateOfMonth)
        let endDateStringOfMonth = MBUserSession.shared.dateFormatter.string(from:  endDateOfMonth)
        
        //  let dayBeforeEndDate =  MBUtilities.displayFormatedDateString(from: endDateString, additionalValue: 0)
        
        MBUserSession.shared.dateFormatter.dateFormat = Constants.kDisplayFormat
        let daysLeft = MBUtilities.calculateDaysLeft(start: startDateStringOfMonth, end: endDateStringOfMonth, dateFormate: Constants.kHomeDateFormate)
        
        let totalDays = MBUtilities.calculateTotalDays(start: startDateStringOfMonth, end: endDateStringOfMonth, dateFormate: Constants.kHomeDateFormate)
        
        var daysLeftUnit = ""
        
        if daysLeft <= 1 {
            daysLeftUnit = Localized("Info_day")
        } else {
            daysLeftUnit = Localized("Info_days")
        }
        let daysLeftDisplayValue = "\(daysLeft) \(daysLeftUnit)"
        
        let progress : Float =  MBUtilities.calculateProgress(totalDays: totalDays, daysLeft: daysLeft)
        
        return (startDateDisplayValue,endDateDisplayValue,daysLeft,daysLeftDisplayValue, totalDays,progress)
    }
    
}

