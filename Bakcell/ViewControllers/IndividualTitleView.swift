//
//  IndividualTitleView.swift
//  Bakcell
//
//  Created by AbdulRehman Warraich on 11/27/17.
//  Copyright © 2017 evampsaanga. All rights reserved.
//

import UIKit

import MarqueeLabel

class IndividualTitleView: MBAccordionTableViewHeaderView {

    static let identifier : String = "IndividualTitleView"
    @IBOutlet var offer_lbl: MarqueeLabel!
    @IBOutlet var detail_lbl: MarqueeLabel!
    @IBOutlet var manatSign_lbl: UILabel!
    @IBOutlet var contenView: UIView!
    @IBOutlet var roundingCoverView: UIView!
    @IBOutlet var manatSignWidthConstrain: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()

        offer_lbl.setupMarqueeAnimation()
        detail_lbl.setupMarqueeAnimation()
    }

    class func instanceFromNib() -> UIView {
        return UINib(nibName: "IndividualTitleView", bundle: Bundle.main).instantiate(withOwner: nil, options: nil)[0] as! IndividualTitleView
    }

    func setViewWith(offerName : String?, priceLabel : String?, priceValue: String?) {

        contenView.backgroundColor = UIColor.MBDimLightGrayColor
        roundingCoverView.backgroundColor = UIColor.MBDimLightGrayColor
        contenView.roundTopCorners(radius: 8)

        detail_lbl.isHidden = false

        offer_lbl.text = offerName

        if priceLabel?.isBlank == true || priceValue?.isBlank == true {
            manatSign_lbl.isHidden = true
            manatSignWidthConstrain.constant = 0
        } else {
            manatSign_lbl.isHidden = false
//            manatSignWidthConstrain.constant = 10
            manatSignWidthConstrain.constant = 0
        }
        
        detail_lbl.attributedText = MBUtilities.createAttributedTextWithManatSign("\(priceLabel ?? ""): \(priceValue ?? "")", textColor: .black)
    }

    func setViewWith(offerName : String?) {

        contenView.backgroundColor = UIColor.MBDimLightGrayColor
        roundingCoverView.backgroundColor = UIColor.MBDimLightGrayColor
        contenView.roundTopCorners(radius: 8)
        detail_lbl.isHidden = true
        manatSign_lbl.isHidden = true

        offer_lbl.text = offerName

    }
}
