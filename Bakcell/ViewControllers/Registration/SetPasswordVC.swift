//
//  SetPasswordVC.swift
//  Bakcell
//
//  Created by Saad Riaz on 6/5/17.
//  Copyright © 2017 evampsaanga. All rights reserved.
//

import UIKit
import KYDrawerController
import ObjectMapper

class SetPasswordVC: BaseVC {

    //MARK:- Properties
    var signUpUserInfo : SignUp = SignUp()
    var termsAndConditionCheck = false
    var alertCheck = false
    var isForgotPassword : Bool = false
    var msisdn : String = ""
    var verifiedOTP : String = ""
    var registrationType :Constants.RegistrationType = .registration

    //MARK:- IBOutlet
    @IBOutlet var confirmPassword_txt: UITextField!
    @IBOutlet var setPassword_txt: UITextField!
    @IBOutlet var alertView: UIView!

    @IBOutlet var passwordStrengthView : UIView!
    @IBOutlet var security_lbl: UILabel!
    @IBOutlet var securityBar : UIProgressView!

    @IBOutlet var signUpView: UIView!
    @IBOutlet var signInUpdateLbl: UILabel!
    @IBOutlet var checkBox: UIButton!
    @IBOutlet var signupButton: UIButton!
    @IBOutlet var signupLabel: UILabel!
    @IBOutlet var termsLbl: UILabel!
    @IBOutlet var terms_btn: UIButton!
    @IBOutlet var error_lbl: UILabel!
    @IBOutlet var backcelIcon_img: UIImageView!


    //MARK:- viewController Methods
    override func viewDidLoad() {
        super.viewDidLoad()

        setPassword_txt.delegate = self
        confirmPassword_txt.delegate = self

        if isForgotPassword == true {
            checkBox.isHidden = true
            terms_btn.isHidden = true
            termsLbl.isHidden = true
        }
        // Disabling SignUP/Update button
        setNextButtonSelection(buttonView: signUpView, isEnabled: false)

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        loadViewLayout()
        
        if registrationType == .addAccount {
            self.backButton?.isHidden = false
            self.interactivePop(true)
        } else {
            self.backButton?.isHidden = true
            self.interactivePop(false)
        }
    }
    
    //MARK:- IBACTIONS
    @IBAction func checkBoxPressed(_ sender: UIButton) {
        if termsAndConditionCheck == false {
            checkBox .setImage(UIImage (named: "checkbox-enabled"), for: UIControl.State.normal)
            termsAndConditionCheck = true

            let passwordStrength:Constants.MBPasswordStrength = self.determinePasswordStrength(Text: setPassword_txt.text ?? "")

            if (passwordStrength == .Week || passwordStrength == .Medium || passwordStrength == .Strong) && termsAndConditionCheck {
                setNextButtonSelection(buttonView: signUpView, isEnabled: true)
            } else {
                setNextButtonSelection(buttonView: signUpView, isEnabled: false)
            }
        } else {
            setNextButtonSelection(buttonView: signUpView, isEnabled: false)

            checkBox .setImage(UIImage (named: "checkbox-disabled"), for: UIControl.State.normal)
            termsAndConditionCheck = false
        }
    }


    @IBAction func signUpPressed(_ sender: Any) {

        var password : String = ""
        var confirmPassword : String = ""

        // Password validation
        if let passwordText = setPassword_txt.text {

            let passwordStrength:Constants.MBPasswordStrength = self.determinePasswordStrength(Text: passwordText)

            switch passwordStrength {

            case Constants.MBPasswordStrength.didNotMatchCriteria :

                self.showErrorAlertWithMessage(message: Localized("Message_InvalidPasswordComplexity"))
                return

            case Constants.MBPasswordStrength.Week, Constants.MBPasswordStrength.Medium, Constants.MBPasswordStrength.Strong :

                password = passwordText
            }

        } else {
            self.showErrorAlertWithMessage(message: Localized("Message_InvalidPassword"))
            return
        }

        // Confirm Password validation
        if let confirmPasswordText = confirmPassword_txt.text,
            confirmPasswordText == password {
            
            confirmPassword = confirmPasswordText
            
        } else {
            self.showErrorAlertWithMessage(message: Localized("Message_InvalidConfirmPassword"))
            return
        }
        
        _ = setPassword_txt.resignFirstResponder()
        _ = confirmPassword_txt.resignFirstResponder()

        if MBUserSession.shared.aSecretKey.isBlank == false {

            if isForgotPassword {
                forgotPasswordAPICall(MSISDN: self.msisdn, Password: password.aesEncrypt(key: MBUserSession.shared.aSecretKey), ConfirmPassword: confirmPassword.aesEncrypt(key: MBUserSession.shared.aSecretKey), OTP: verifiedOTP)
            } else {
                saveCustomerAPICall(MSISDN: self.msisdn, Password: password.aesEncrypt(key: MBUserSession.shared.aSecretKey), ConfirmPassword: confirmPassword.aesEncrypt(key: MBUserSession.shared.aSecretKey), TermsAndConditions: termsAndConditionCheck, OTP: verifiedOTP)
            }

        } else {
            self.showErrorAlertWithMessage(message: Localized("Message_GenralError"))
        }

    }

    @IBAction func infoPressed(_ sender: UIButton) {
        if alertCheck == false{
            alertView.isHidden = false
            alertCheck = true
        }
        else{
            alertView.isHidden = true
            alertCheck = false
        }
    }

    @IBAction func termsAndConditionsPressed(_ sender: UIButton) {
        if let terms = self.myStoryBoard.instantiateViewController(withIdentifier: "Terms_ConditionsVC") as? Terms_ConditionsVC {
            terms.isPopUp = true
            self.presentPOPUP(terms, animated: true, completion: nil)
        }
    }

    //MARK: - Functions

    /**
     Set localized text in viewController
     */
    func loadViewLayout(){

        if isForgotPassword {
            signInUpdateLbl.text = Localized("BtnTitle_Update")
        } else {
            signInUpdateLbl.text = Localized("BtnTitle_SignUP")
        }
        terms_btn.titleLabel?.font = UIFont.MBArialBold(fontSize: 14)
        error_lbl.font = UIFont.MBArial(fontSize: 12)
        signInUpdateLbl.font = UIFont.MBArialBold(fontSize: 16)
        termsLbl.font = UIFont.MBArialBold(fontSize: 14)
        
        error_lbl.text = Localized("Password_ErrorInfo")
        termsLbl.text = Localized("BtnTitle_Terms")
        setPassword_txt.placeholder = Localized("SetPassword_PlaceHolder")
        confirmPassword_txt.placeholder = Localized("ConfirmPassword_PlaceHolder")

        backcelIcon_img.image = UIImage(named: Localized("Img_BakcelLogo"))
        
    }

    /**
     call to set password strenght bar

     - parameter text: text of textfield.

     - returns: void
     */
    func setPasswordStrenghtBar(Text text:String) {
        
        let passwordStrength:Constants.MBPasswordStrength = self.determinePasswordStrength(Text: text)

        switch passwordStrength {
        case Constants.MBPasswordStrength.didNotMatchCriteria:
            passwordStrengthView.isHidden = true
            security_lbl.text = ""
            securityBar.setProgress(0, animated: false)
            securityBar.tintColor = UIColor.clear
            break

        case Constants.MBPasswordStrength.Week:
            passwordStrengthView.isHidden = false
            security_lbl.isHidden = false
            security_lbl.text = Localized("Pass_Weak")

            securityBar.tintColor = UIColor.MBBarRed
            securityBar.setProgress(0.33, animated: false)
            break

        case Constants.MBPasswordStrength.Medium:
            passwordStrengthView.isHidden = false
            security_lbl.isHidden = false
            security_lbl.text = Localized("Pass_Medium")
            securityBar.tintColor = UIColor.MBBarOrange
            securityBar.setProgress(0.66, animated: false)
            break

        case Constants.MBPasswordStrength.Strong:
            passwordStrengthView.isHidden = false
            security_lbl.isHidden = false
            security_lbl.text = Localized("Pass_Strong")
            securityBar.tintColor = UIColor.MBBarGreen
            securityBar.setProgress(1, animated: false)
        }

        self.view.setNeedsUpdateConstraints()
    }

    /**
     Save customerInfo in userDefault.

     - parameter customerInfo: customerInfo which need to save.
     - parameter playVedio:  set true if you want to play tutorial video.

     - returns: void
     */
    func redirectUserToHome(customerInfo: SaveCustomerModel, playVedio: Bool = false) {

        if MBUtilities.isTutorialShowenBefore() == false && playVedio == true {

            // Update Check for tutorial
            MBUtilities.updateTutorialShowenStatus()

            if let tutorialsVC = self.myStoryBoard.instantiateViewController(withIdentifier: "TutsVC") as? TutsVC {
                tutorialsVC.isLoggedInNow = true
                self.navigationController?.pushViewController(tutorialsVC, animated: true)
            }

        } else {

            // Redirecting user to dashboard screen.
            if let mainViewController = self.myStoryBoard.instantiateViewController(withIdentifier: "TabbarController") as? TabbarController {
                mainViewController.isLoggedInNow = true

                if let drawerViewController = self.myStoryBoard.instantiateViewController(withIdentifier: "SideDrawerVC") as? SideDrawerVC {
                    let drawerController = KYDrawerController(drawerDirection: .left, drawerWidth: (UIScreen.main.bounds.width * 0.85))
                    drawerController.mainViewController = mainViewController
                    drawerController.drawerViewController = drawerViewController
                    self.navigationController?.pushViewController(drawerController, animated: true)
                }
            }
        }


    }

    //MARK: - API Calls

    /**
     Call 'savecustomer' API with the specified `MSISDN` and `Password`.

     - parameter MSISDN:     The MSISDN.
     - parameter OTP:  The One Time PIN.

     - returns: void
     */
    func saveCustomerAPICall(MSISDN msisdn: String, Password password : String , ConfirmPassword confirmPassword : String, TermsAndConditions termsAndConditions : Bool, OTP pin: String) {

        activityIndicator.showActivityIndicator()
        _ = MBAPIClient.sharedClient.saveCustomer(MSISDN: msisdn, Password: password, ConfirmPassword: confirmPassword, isAgrredOnTermsAndConditions: termsAndConditions, OTP: pin ,{ (response, resultData, error, isCancelled, status, resultCode, resultDesc) in
            self.activityIndicator.hideActivityIndicator()
            if error != nil {
                
                //EventLog
                MBAnalyticsManager.logEvent(screenName: "Signup Screen Step3", contentType:"Signup Failure" , status:"Failure" )
                
                error?.showServerErrorInViewController(self)

            } else {
                // handling data from API response.
                if resultCode == Constants.MBAPIStatusCode.succes.rawValue {
                    //EventLog
                    MBAnalyticsManager.logEvent(screenName: "Signup Screen Step3", contentType:"Signup Success" , status:"Success" )

                    // Parssing response data
                    if let sucessResponseHandler = Mapper<SaveCustomerModel>().map(JSONObject:resultData) {
                        
                        if self.registrationType == .addAccount {
                            /* Save logged in user information */
                            MBUserInfoUtilities.addNewUser(newUserMSISDN: msisdn, loginInfo: sucessResponseHandler)
                        } else {
                            /* Save logged in user information */
                            MBUserInfoUtilities.saveLoginInfo(loggedInUserMSISDN: msisdn, loginInfo: sucessResponseHandler)
                        }
                        
                        /* Set user Promo message text */
                        MBUserSession.shared.promoMessage = sucessResponseHandler.promoMessage?.removeNullValues()
                        
                        // Saving User information in default and redirecting to Dash board screen
                        self.redirectUserToHome(customerInfo: sucessResponseHandler, playVedio: true)

                        // Calling FCM API to add new user FCM id
                        // UserDefaults.standard.saveBoolForKey(boolValue: false, forKey: Constants.kIsFCMIdAdded)
                        // UserDefaults.standard.saveBoolForKey(boolValue: false, forKey: Constants.kIsFCMIdAddedForLogin)
                        // BaseVC.addFCMId(isForLogin: false)

                        // SAVE wethere FNFAllowed on current package or not
                        // MBUtilities.saveFNFAllowedInToUserDefaults(isFNFAllowed: sucessResponseHandler.predefinedData?.fnfAllowed)

                        //Assigning MonaeyDetails to App user session.
                        //  MBUtilities.saveCustomerMoneyInUserDefaults(moneyData: sucessResponseHandler.predefinedData?.topup?.moneyTransfer)
                        
                        /* Save user loggeding time*/
                        //MBUtilities.updateLoginTime()

                        // Saving Get loan info
                        // MBUtilities.saveJSONStringInToUserDefaults(jsonString: sucessResponseHandler.predefinedData?.topup?.getLoan?.toJSONString(), key: Localized("API_AuthGetLoan"))

                        // Plastic card Redirection Links
                        //MBUtilities.saveJSONStringInToUserDefaults(jsonString: sucessResponseHandler.predefinedData?.redirectionLinks?.toJSONString(), key: Constants.kRedirectionLinks)

                        // TariffMigrationPrices FOR CONFIRMATION ALERT
                        //MBUtilities.saveJSONStringInToUserDefaults(jsonString: sucessResponseHandler.predefinedData?.tariffMigrationPrices.toJSONString(), key: Constants.kTariffMigrationPrices)

                       
                        
                        /* Save live chat URL in UserDefaults */
                        //UserDefaults.standard.saveStringForKey(stringValue: sucessResponseHandler.predefinedData?.liveChat ?? "", forKey: Constants.kLiveChatURLString)
                        
                        /* Reset Biometric status */
                        // UserDefaults.standard.saveBoolForKey(boolValue: false, forKey: Constants.K_IsBiometricEnabled)

                    }

                } else {
                    //EventLog
                    MBAnalyticsManager.logEvent(screenName: "Signup Screen Step3", contentType:"Signup Failure" , status:"Failure" )
                    
                    // Show error alert to user
                    self.showErrorAlertWithMessage(message: resultDesc)
                }
            }
        })
    }


    /**
     Call 'forgotPassword' API with the specified `Password` and `Conform Password`.

     - parameter Password:     The Password.
     - parameter ConformPassword:  The Conform Password.


     - returns: void
     */
    func forgotPasswordAPICall(MSISDN msisdn:String, Password password : String , ConfirmPassword confirmPassword : String, OTP pin: String) {

        activityIndicator.showActivityIndicator()
        _ = MBAPIClient.sharedClient.forgotPassword(MSISDN: msisdn, Password: password, ConfirmPassword: password, OTP: pin ,{ (response, resultData, error, isCancelled, status, resultCode, resultDesc) in
            self.activityIndicator.hideActivityIndicator()
            if error != nil {
                
                //EventLog
                MBAnalyticsManager.logEvent(screenName: "Forgotpassword Screen Step3", contentType:"Forgotpassword Failure" , status:"Failure" )
                
                error?.showServerErrorInViewController(self)

            } else {
                // handling data from API response.
                if resultCode == Constants.MBAPIStatusCode.succes.rawValue {
                    //EventLog
                    MBAnalyticsManager.logEvent(screenName: "Forgotpassword Screen Step3", contentType:"Forgotpassword Success" , status:"Success" )

                    // Parssing response data
                    if let forgotPasswordHandler = Mapper<SaveCustomerModel>().map(JSONObject:resultData) {

                        if self.registrationType == .addAccount {
                            /* Save logged in user information */
                            MBUserInfoUtilities.addNewUser(newUserMSISDN: msisdn, loginInfo: forgotPasswordHandler)
                        } else {
                            /* Save logged in user information */
                            MBUserInfoUtilities.saveLoginInfo(loggedInUserMSISDN: msisdn, loginInfo: forgotPasswordHandler)
                        }
                        
                        // Calling FCM API to add new user FCM id
                        // UserDefaults.standard.saveBoolForKey(boolValue: false, forKey: Constants.kIsFCMIdAdded)
                        // UserDefaults.standard.saveBoolForKey(boolValue: true, forKey: Constants.kIsFCMIdAddedForLogin)
                        // BaseVC.addFCMId(isForLogin: true)

                        // SAVE wethere FNFAllowed on current package or not
                        //  MBUtilities.saveFNFAllowedInToUserDefaults(isFNFAllowed: forgotPasswordHandler.predefinedData?.fnfAllowed)

                        //Assigning MonaeyDetails to App user session.
                        //  MBUtilities.saveCustomerMoneyInUserDefaults(moneyData: forgotPasswordHandler.predefinedData?.topup?.moneyTransfer)
                        
                        /* Save user loggeding time*/
                        //MBUtilities.updateLoginTime()

                        // Saving Get loan info
                        // MBUtilities.saveJSONStringInToUserDefaults(jsonString: forgotPasswordHandler.predefinedData?.topup?.getLoan?.toJSONString(), key: Localized("API_AuthGetLoan"))

                        // Plastic card Redirection Links
                        //MBUtilities.saveJSONStringInToUserDefaults(jsonString: forgotPasswordHandler.predefinedData?.redirectionLinks?.toJSONString(), key: Constants.kRedirectionLinks)

                        // TariffMigrationPrices FOR CONFIRMATION ALERT
                        //MBUtilities.saveJSONStringInToUserDefaults(jsonString: forgotPasswordHandler.predefinedData?.tariffMigrationPrices.toJSONString(), key: Constants.kTariffMigrationPrices)

                        // Saving User information in default and redirecting to Dash board screen
                        self.redirectUserToHome(customerInfo: forgotPasswordHandler)

                        /* Save live chat URL in UserDefaults */
                        //UserDefaults.standard.saveStringForKey(stringValue: forgotPasswordHandler.predefinedData?.liveChat ?? "", forKey: Constants.kLiveChatURLString)
                        
                        /* Reset Biometric status */
                        // UserDefaults.standard.saveBoolForKey(boolValue: false, forKey: Constants.K_IsBiometricEnabled)
                    }
                } else {
                    
                    //EventLog
                    MBAnalyticsManager.logEvent(screenName: "Forgotpassword Screen Step3", contentType:"Forgotpassword Failure" , status:"Failure" )
                    
                    // Show error alert to user
                    self.showErrorAlertWithMessage(message: resultDesc)
                }
            }
        })
    }
}

//MARK:- Textfield delagates
extension SetPasswordVC : UITextFieldDelegate {

    //Set max Limit for textfields
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let text = textField.text else { return false }
        let newLength = text.count + string.count - range.length

        if textField == setPassword_txt {

            // Allow charactors check
            let allowedCharactors = Constants.allowedAlphbeats + Constants.allowedNumbers + Constants.allowedSpecialCharacters
            let allowedCharacters = CharacterSet(charactersIn: allowedCharactors)
            let characterSet = CharacterSet(charactersIn: string)

            if allowedCharacters.isSuperset(of: characterSet) != true {

                return false
            }

            let validLimit : Bool = newLength <= 15

            if validLimit {

                let newString = (text as NSString).replacingCharacters(in: range, with: string)
                setPasswordStrenghtBar(Text: newString)

                let passwordStrength:Constants.MBPasswordStrength = self.determinePasswordStrength(Text: newString)

                if (passwordStrength == .Week || passwordStrength == .Medium || passwordStrength == .Strong)  {

                    if termsAndConditionCheck && isForgotPassword == false {
                        setNextButtonSelection(buttonView: signUpView, isEnabled: true)
                    } else if isForgotPassword == true {
                        setNextButtonSelection(buttonView: signUpView, isEnabled: true)
                    }

                } else {
                    setNextButtonSelection(buttonView: signUpView, isEnabled: false)
                }
            }

            return validLimit // Bool
        } else {
            // For other then setPassword textfield.
            return newLength <= 15 // Bool
        }
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.text = ""
        if textField == setPassword_txt {
            setPasswordStrenghtBar(Text:"")
            setNextButtonSelection(buttonView: signUpView, isEnabled: false)
        }
    }
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool {

        if textField == setPassword_txt {
            setPasswordStrenghtBar(Text:"")
            setNextButtonSelection(buttonView: signUpView, isEnabled: false)
        }
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == setPassword_txt {
            _ = confirmPassword_txt.becomeFirstResponder()
            return false
        }  else if textField == confirmPassword_txt {
            signUpPressed(UIButton())
            return false
        } else {
            return true
        }
    }
}
