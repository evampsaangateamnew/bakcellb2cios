//
//  FAndF.swift
//  Bakcell
//
//  Created by AbdulRehman Warraich on 10/2/17.
//  Copyright © 2017 evampsaanga. All rights reserved.
//

import Foundation

import ObjectMapper

class FAndF : Mappable{

    var fnfList : [FNFList]?
    var fnfLimit : String?
    required init?(map: Map) {
    }

    func mapping(map: Map) {

        fnfList        <- map["fnfList"]
        fnfLimit       <- map["fnfLimit"]
    }
}

class FNFList : Mappable{

//    msisdn : "994555957012"
//    createdDate : "02 Oct 2017"
    var msisdn : String?
    var createdDate : String?

    required init?(map: Map) {
    }

    init() {
    }
    
    func mapping(map: Map) {

        msisdn              <- map["msisdn"]
        createdDate         <- map["createdDate"]
    }
}
