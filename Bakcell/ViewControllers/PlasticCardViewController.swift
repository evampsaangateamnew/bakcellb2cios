//
//  PlasticCardViewController.swift
//  Bakcell
//
//  Created by AbdulRehman Warraich on 11/10/17.
//  Copyright © 2017 evampsaanga. All rights reserved.
//

import UIKit
import ObjectMapper
import WebKit

class PlasticCardViewController: BaseVC {
    
//    static let identifier = "PlasticCardViewControllers"
    
    //MARK: - Properties
    var topUpToMSISDN = ""
    var redirectionURL = ""
    var isSaveCard = false
    
    //MARK: - IBOutlet
    @IBOutlet var title_lbl: UILabel!
    @IBOutlet var webContainerView: UIView!
    var myWebView: WKWebView!
    
    //MARK: - View Controller Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title_lbl.text = Localized("Title_TopUP")
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.interactivePop(true)
        
        
        let config = WKWebViewConfiguration.init()
        config.preferences.javaScriptEnabled = true
        //config.preferences.javaEnabled = true
        config.preferences.javaScriptCanOpenWindowsAutomatically = true
     
            
        
        
        if #available(iOS 11.0, *) {
            config.setURLSchemeHandler(self, forURLScheme: "com.bakcell.carrierapp.portal")
        
        } else {
            // Fallback on earlier versions
        }
        myWebView = WKWebView.init(frame: webContainerView.frame, configuration:config )
//        myWebView.contentScaleFactor = 2.0
        myWebView.backgroundColor = .clear
        myWebView.navigationDelegate = self
//        myWebView.scrollView.zoomScale = 0.5
        webContainerView.addSubview(myWebView)
        myWebView.snp.makeConstraints { make in
            make.top.equalTo(webContainerView.snp.top).offset(0.0)
            make.right.equalTo(webContainerView.snp.right).offset(0.0)
            make.left.equalTo(webContainerView.snp.left).offset(0.0)
            make.bottom.equalTo(webContainerView.snp.bottom).offset(0.0)
        }
        
        WKWebsiteDataStore.default().removeData(ofTypes: WKWebsiteDataStore.allWebsiteDataTypes(), modifiedSince: Date(timeIntervalSince1970: 0), completionHandler:{ })
        
        var failedToOpenURL: Bool = true
        
        /*if let redirectionResponse = MBUserSession.shared.predefineData?.redirectionLinks {
            
            var baseURLString = ""
            
            switch MBLanguageManager.userSelectedLanguage() {
            case .Russian:
                baseURLString = redirectionResponse.onlinePaymentGatewayURL_RU
            case .English:
                baseURLString = redirectionResponse.onlinePaymentGatewayURL_EN
            case .Azeri:
                baseURLString = redirectionResponse.onlinePaymentGatewayURL_AZ
            }
            
            baseURLString.append("&prefix=\(String(topUpToMSISDN.prefix(2)))&number=\(String(topUpToMSISDN.suffix(7)))")
            
            if let url = URL(string: baseURLString) ,
                UIApplication.shared.canOpenURL(url) {
                
                let request = URLRequest(url: url)
                myWebView.load(request)
                failedToOpenURL = false
            }
        }*/
        if let url = URL(string: redirectionURL) ,
            UIApplication.shared.canOpenURL(url) {
            
            let request = URLRequest(url: url)
            myWebView.load(request)
            failedToOpenURL = false
        }
        
        if failedToOpenURL == true {
            
            self.showErrorAlertWithMessage(message: Localized("Message_FailurePlasticCard"), { (aString) in
                self.navigationController?.popViewController(animated: true)
            })
            clearWebView()
        }
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        myWebView.scrollView.zoomScale = 0.5
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        myWebView.scrollView.setZoomScale(0.5, animated: true)
    }
    
    func clearWebView() {
        
        if let clearURL = URL(string: "about:blank") {
            myWebView.load(URLRequest(url: clearURL))
        }
        
        self.showErrorAlertWithMessage(message: Localized("Message_FailurePlasticCard"))
        self.myWebView.showDescriptionViewWithImage(description: Localized("Message_FailurePlasticCard"))
        
    }
    
    //in-App-Survery handling
    func checkTopupSurvey(message: String) {
        if isSaveCard == true {
            self.showSuccessAlertWithMessage(message: message) { (string) in
                self.popTotopupVC()
            }
            return
        }
        if let survey  = MBUserSession.shared.appSurvays?.surveys?.first(where: { $0.screenName  == "topup" }) {
            if survey.surveyCount?.toInt ?? 0 < survey.surveyLimit?.toInt ?? 0,
               survey.visitLimit != "-1",
               survey.visitLimit?.toInt ?? 0 <= UserDefaults.standard.integer(forKey: "TopUpScreenViews") {
                if let inAppSurveyVC : InAppSurveyVC = InAppSurveyVC.fromNib() {
                    inAppSurveyVC.currentSurvay = survey
                    inAppSurveyVC.showTopUpStackView = true
                    inAppSurveyVC.toptupTitleText = Localized("Title_Alert")
                    inAppSurveyVC.attributedMessage = NSAttributedString.init(string: message)
                    self.presentPOPUP(inAppSurveyVC, animated: true, completion: nil)
                    inAppSurveyVC.callback = {
                        self.popTotopupVC()
                    }
                }
            } else {
                self.showSuccessAlertWithMessage(message: message) { (string) in
                    self.popTotopupVC()
                }
            }
        }
    }
    
}

extension PlasticCardViewController :WKNavigationDelegate, WKUIDelegate, WKURLSchemeHandler {
    
    

    
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        print("URL...\(webView.url?.absoluteString ?? "Not foouund")")
        if (error as NSError).code !=  NSURLErrorCancelled {
            
            self.clearWebView()
        }
        self.activityIndicator.removeAllActivityIndicator()
        
    }
    
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        self.activityIndicator.showActivityIndicator()
        print("will load URL\(webView.url?.absoluteString ?? "Not foouund")")
        
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        
        self.activityIndicator.removeAllActivityIndicator()
        
        if let urlString = webView.url?.absoluteString, urlString.containsSubString(subString: "error.jsp") == true {
                // show error
                print("success ")
                self.showErrorAlertWithMessage(message: Localized("Message_GenralError")) { (string) in
                    MBUserSession.shared.shouldReloadDashboardData = true
                    self.popTotopupVC()
                }
            } else if let urlString = webView.url?.absoluteString, urlString.containsSubString(subString: "ok.jsp") == true {
                //show success popup
                print("success ")
                self.checkTopupSurvey(message: Localized("Message_CardAddedSuccessfully"))
            }
        print("did finish loading URL...\(webView.url?.absoluteString ?? "Not foouund")")
        
    }
    
    
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        
        
        print("recieved policy for URL...\(webView.url?.absoluteString ?? "Not foouund")")
        
        if let urlString = webView.url?.absoluteString, urlString.containsSubString(subString: "/user/account/topup?result=false") == true {
                // show error
            self.activityIndicator.removeAllActivityIndicator()
            myWebView.alpha = 0.0
            let messageToShow = getRequiredFailuerMessage(urlString: urlString)
            self.showErrorAlertWithMessage(message: messageToShow) { (string) in
                    self.popTotopupVC()
                }
            } else if let urlString = webView.url?.absoluteString, urlString.containsSubString(subString: "/user/account/topup?result=true") == true {
                //show success popup
                self.activityIndicator.removeAllActivityIndicator()
                myWebView.alpha = 0.0
                let messageToShow = getRequiredSuccessMessage(urlString: urlString)
                
                self.checkTopupSurvey(message: messageToShow)
            }
        
        switch navigationAction.navigationType {
            case .linkActivated:
                if let newURL = navigationAction.request.url,
                UIApplication.shared.canOpenURL(newURL) {
                
                self.openURLInSafari(urlString: newURL.absoluteString)
                decisionHandler(.cancel)
                
            }
            default:
                decisionHandler(.allow)
                break
        }
        
//        func webView(_ webView: WKWebView, didCommit navigation: WKNavigation!) {
////            webView.scrollView.zoomScale = 0.5
//            }
    }
      
    
    func getRequiredSuccessMessage(urlString: String) -> String {
        if urlString.containsSubString(subString: "error=200") {
            if MBLanguageManager.userSelectedLanguageShortCode() == "en" {
                return MBUserSession.shared.predefineData?.goldenPayMessages?.goldenpaymessage200en ?? Localized("Message_CardAddedSuccessfully")
            } else if MBLanguageManager.userSelectedLanguageShortCode() == "az" {
                return MBUserSession.shared.predefineData?.goldenPayMessages?.goldenpaymessage200az ?? Localized("Message_CardAddedSuccessfully")
            } else {
                return MBUserSession.shared.predefineData?.goldenPayMessages?.goldenpaymessage200ru ?? Localized("Message_CardAddedSuccessfully")
            }
        } else if urlString.containsSubString(subString: "error=116") {
            if MBLanguageManager.userSelectedLanguageShortCode() == "en" {
                return MBUserSession.shared.predefineData?.goldenPayMessages?.goldenpaymessage116en ?? Localized("Message_CardAddedSuccessfully")
            } else if MBLanguageManager.userSelectedLanguageShortCode() == "az" {
                return MBUserSession.shared.predefineData?.goldenPayMessages?.goldenpaymessage116az ?? Localized("Message_CardAddedSuccessfully")
            } else {
                return MBUserSession.shared.predefineData?.goldenPayMessages?.goldenpaymessage116ru ?? Localized("Message_CardAddedSuccessfully")
            }
        } else if urlString.containsSubString(subString: "error=500") {
            if MBLanguageManager.userSelectedLanguageShortCode() == "en" {
                return MBUserSession.shared.predefineData?.goldenPayMessages?.goldenpaymessage500en ?? Localized("Message_CardAddedSuccessfully")
            } else if MBLanguageManager.userSelectedLanguageShortCode() == "az" {
                return MBUserSession.shared.predefineData?.goldenPayMessages?.goldenpaymessage500az ?? Localized("Message_CardAddedSuccessfully")
            } else {
                return MBUserSession.shared.predefineData?.goldenPayMessages?.goldenpaymessage500ru ?? Localized("Message_CardAddedSuccessfully")
            }
        }
        return Localized("Message_CardAddedSuccessfully")
    }
    
    
    func getRequiredFailuerMessage(urlString: String) -> String {
        if urlString.containsSubString(subString: "error=200") {
            if MBLanguageManager.userSelectedLanguageShortCode() == "en" {
                return MBUserSession.shared.predefineData?.goldenPayMessages?.goldenpaymessage200en ?? Localized("Message_GenralError")
            } else if MBLanguageManager.userSelectedLanguageShortCode() == "az" {
                return MBUserSession.shared.predefineData?.goldenPayMessages?.goldenpaymessage200az ?? Localized("Message_GenralError")
            } else {
                return MBUserSession.shared.predefineData?.goldenPayMessages?.goldenpaymessage200ru ?? Localized("Message_GenralError")
            }
        } else if urlString.containsSubString(subString: "/user/account/topup?result=false&error=116") {
            if MBLanguageManager.userSelectedLanguageShortCode() == "en" {
                return MBUserSession.shared.predefineData?.goldenPayMessages?.goldenpaymessage116en ?? Localized("Message_GenralError")
            } else if MBLanguageManager.userSelectedLanguageShortCode() == "az" {
                return MBUserSession.shared.predefineData?.goldenPayMessages?.goldenpaymessage116az ?? Localized("Message_GenralError")
            } else {
                return MBUserSession.shared.predefineData?.goldenPayMessages?.goldenpaymessage116ru ?? Localized("Message_GenralError")
            }
        } else if urlString.containsSubString(subString: "/user/account/topup?result=false&error=500") {
            if MBLanguageManager.userSelectedLanguageShortCode() == "en" {
                return MBUserSession.shared.predefineData?.goldenPayMessages?.goldenpaymessage500en ?? Localized("Message_GenralError")
            } else if MBLanguageManager.userSelectedLanguageShortCode() == "az" {
                return MBUserSession.shared.predefineData?.goldenPayMessages?.goldenpaymessage500az ?? Localized("Message_GenralError")
            } else {
                return MBUserSession.shared.predefineData?.goldenPayMessages?.goldenpaymessage500ru ?? Localized("Message_GenralError")
            }
        }
        return Localized("Message_GenralError")
    }
    @available(iOS 11.0, *)
    func webView(_ webView: WKWebView, start urlSchemeTask: WKURLSchemeTask) {
        
    }
    
    @available(iOS 11.0, *)
    func webView(_ webView: WKWebView, stop urlSchemeTask: WKURLSchemeTask) {
        
    }
    
    func popTotopupVC() {
        
        self.navigationController?.viewControllers.forEach({ (aViewController) in
            if aViewController.isKind(of: TopUpVC().classForCoder) {
                self.navigationController?.popToViewController(aViewController, animated: true)
            }

        })
    }
    
    
    
    
}

