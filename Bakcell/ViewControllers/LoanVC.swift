//
//  LoanVC.swift
//  Bakcell
//
//  Created by Saad Riaz on 6/9/17.
//  Copyright © 2017 evampsaanga. All rights reserved.
//

import UIKit

class LoanVC: BaseVC , UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
    var loanArr: [String] = []
    var loanMainVC: LoanMainVC?
    var requestHistoryVC: RequestHistoryVC?
    var paymentHistoryVC: PaymentHistoryVC?
    
    @IBOutlet var MainView: UIView!
    @IBOutlet var collectionView: UICollectionView!
    @IBOutlet var loanTitle_lbl: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        loadViewLayout()
        
        collectionView.selectItem(at:  IndexPath(item: 0, section: 0), animated: false, scrollPosition: UICollectionView.ScrollPosition.left)
        
        // load initial controller
        let loanMain = self.myStoryBoard.instantiateViewController(withIdentifier: "LoanMainVC")as! LoanMainVC
        self.addChild(loanMain)
        MainView.addSubview(loanMain.view)
        loanMain.didMove(toParent: self)
        
        loanMain.view.snp.makeConstraints { make in
            make.top.equalTo(MainView.snp.top).offset(0.0)
            make.right.equalTo(MainView.snp.right).offset(0.0)
            make.left.equalTo(MainView.snp.left).offset(0.0)
            make.bottom.equalTo(MainView.snp.bottom).offset(0.0)
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.interactivePop(true)
        
    }
    
    //MARK:- FUNCTIONS
    func loadViewLayout(){
        loanTitle_lbl.font = UIFont.MBArial(fontSize: 20)
        if MBUserSession.shared.brandName() == MBBrandName.Klass{
            loanArr = [Localized("Loan_TitleKlass"),Localized("Op2_Request"),Localized("Op4_PaymentHistory")]
            loanTitle_lbl.text = Localized("Loan_TitleKlass")
        }
        else{
            loanArr = [Localized("Loan_TitleCin"),Localized("Op2_Request"),Localized("Op4_PaymentHistory")]
            loanTitle_lbl.text = Localized("Loan_TitleCin")
        }
    }
    
    
    //MARK: - COLLECTION VIEW DELEGATE METHODS
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.loanArr.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let testString : String = loanArr[indexPath.row]
        var calCulateSizze : CGSize = testString.size(withAttributes: nil)
        calCulateSizze.width = calCulateSizze.width + 40;
        calCulateSizze.height = 32
        
        return calCulateSizze;
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellID", for: indexPath) as? LoanCell {
            
            cell.layer.borderColor = UIColor.MBBorderGrayColor.cgColor
            cell.title_lbl.highlightedTextColor = UIColor.black
            cell.title_lbl.text = loanArr[indexPath.row]
            
            let bgColorView = UIView()
            bgColorView.backgroundColor = UIColor.lightGray
            cell.selectedBackgroundView = bgColorView
            
            return cell
        } else {
            return UICollectionViewCell()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        collectionView.scrollToItem(at: indexPath, at: UICollectionView.ScrollPosition.centeredHorizontally, animated: true)
        
        self.children.forEach { (aViewController) in
            if aViewController is LoanMainVC ||
                aViewController is RequestHistoryVC ||
                aViewController is PaymentHistoryVC {
                
                aViewController.view.removeFromSuperview()
                aViewController.removeFromParent()
            }
        }
        
        
        switch indexPath.item {
            
        case 0:
            
            if loanMainVC == nil {
                loanMainVC = self.myStoryBoard.instantiateViewController(withIdentifier: "LoanMainVC") as? LoanMainVC
            }
            self.addChild(loanMainVC ?? LoanMainVC())
            MainView.addSubview(loanMainVC?.view ?? UIView())
            loanMainVC?.didMove(toParent: self)
            
            loanMainVC?.view.snp.makeConstraints { make in
                make.top.equalTo(MainView.snp.top).offset(0.0)
                make.right.equalTo(MainView.snp.right).offset(0.0)
                make.left.equalTo(MainView.snp.left).offset(0.0)
                make.bottom.equalTo(MainView.snp.bottom).offset(0.0)
            }
            
        case 1:
            if requestHistoryVC == nil {
                requestHistoryVC = self.myStoryBoard.instantiateViewController(withIdentifier: "RequestHistoryVC") as? RequestHistoryVC
            }
            self.addChild(requestHistoryVC ?? RequestHistoryVC())
            MainView .addSubview(requestHistoryVC?.view ?? UIView())
            requestHistoryVC?.didMove(toParent: self)
            
            requestHistoryVC?.view.snp.makeConstraints { make in
                make.top.equalTo(MainView.snp.top).offset(0.0)
                make.right.equalTo(MainView.snp.right).offset(0.0)
                make.left.equalTo(MainView.snp.left).offset(0.0)
                make.bottom.equalTo(MainView.snp.bottom).offset(0.0)
            }
            break
            
        case 2:
            if paymentHistoryVC == nil {
                paymentHistoryVC = self.myStoryBoard.instantiateViewController(withIdentifier: "PaymentHistoryVC") as? PaymentHistoryVC
            }
            self.addChild(paymentHistoryVC ?? PaymentHistoryVC())
            MainView.addSubview(paymentHistoryVC?.view ?? UIView())
            paymentHistoryVC?.didMove(toParent: self)
            
            paymentHistoryVC?.view.snp.makeConstraints { make in
                make.top.equalTo(MainView.snp.top).offset(0.0)
                make.right.equalTo(MainView.snp.right).offset(0.0)
                make.left.equalTo(MainView.snp.left).offset(0.0)
                make.bottom.equalTo(MainView.snp.bottom).offset(0.0)
            }
        default:
            break
        }
    }
}
