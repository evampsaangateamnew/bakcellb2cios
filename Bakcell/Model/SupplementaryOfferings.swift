//
//  SupplementaryOfferings.swift
//  Bakcell
//
//  Created by AbdulRehman Warraich on 8/29/17.
//  Copyright © 2017 evampsaanga. All rights reserved.
//

import Foundation
import ObjectMapper

class SupplementaryOfferings: Mappable {
    var internet : Internet?
    var campaign : Campaign?
    var sms : SMS?
    var call : Call?
    var tm : TM?
    var hybrid : Hybrid?
    var roaming : Roaming?

    required init?(map: Map) {

    }
    //    internet
    //    campaign
    //    sms
    //    call
    //    tm
    //    hybrid
    //    roaming

    func mapping(map: Map) {

        internet        <- map["internet"]
        campaign        <- map["campaign"]
        sms             <- map["sms"]
        call            <- map["call"]
        tm              <- map["tm"]
        hybrid          <- map["hybrid"]
        roaming         <- map["roaming"]

    }
}

class Internet: Mappable {
    var offers : [Offers]?
    var filters : Filters?

    required init?(map: Map) {

    }
    //    offers
    //    filters

    func mapping(map: Map) {
        offers         <- map["offers"]
        filters        <- map["filters"]
    }
}

class Campaign: Mappable {
    var offers : [Offers]?
    var filters : Filters?

    required init?(map: Map) {

    }
    //    offers
    //    filters

    func mapping(map: Map) {
        offers         <- map["offers"]
        filters        <- map["filters"]
    }
}

class SMS: Mappable {
    var offers : [Offers]?
    var filters : Filters?

    required init?(map: Map) {

    }
    //    offers
    //    filters

    func mapping(map: Map) {
        offers         <- map["offers"]
        filters        <- map["filters"]
    }
}

class Call: Mappable {
    var offers : [Offers]?
    var filters : Filters?

    required init?(map: Map) {

    }
    //    offers
    //    filters

    func mapping(map: Map) {
        offers         <- map["offers"]
        filters        <- map["filters"]
    }
}

class TM: Mappable {
    var offers : [Offers]?
    var filters : Filters?

    required init?(map: Map) {

    }
    //    offers
    //    filters

    func mapping(map: Map) {
        offers         <- map["offers"]
        filters        <- map["filters"]
    }
}

class Hybrid: Mappable {
    var offers : [Offers]?
    var filters : Filters?

    required init?(map: Map) {

    }
    //    offers
    //    filters

    func mapping(map: Map) {
        offers         <- map["offers"]
        filters        <- map["filters"]
    }
}

class Roaming: Mappable {
    var countries       : [String]?
    var offers          : [Offers]?
    var filters         : Filters?
    var countriesFlags  :[Country]?

    required init?(map: Map) {

    }

    //    countries
    //    offers
    //    filters

    func mapping(map: Map) {

        countriesFlags      <- map["countries"]
        offers         <- map["offers"]
        filters        <- map["filters"]
    }
}

class Country: Mappable {
    var name : String?
    var flag : String?

    required init?(map: Map) {

    }

    //    countries
    //    offers
    //    filters

    func mapping(map: Map) {

        name      <- map["name"]
        flag         <- map["flag"]
    }
}

class Offers: Mappable {
    var details : DetailsAndDescription?
    var description : DetailsAndDescription?
    var header : Header?
    
    var openedViewSection : MBSectionType = MBSectionType.Header

    required init?(map: Map) {

    }

    //    details
    //    description
    //    header

    func mapping(map: Map) {

        details             <- map["details"]
        description         <- map["description"]
        header              <- map["header"]
    }
}


class Header: Mappable {

    var id : String?
    var type : String?
    var offerName : String?
    var stickerLabel : String?
    var stickerColorCode : String?
    var validityTitle : String?
    var validityInformation : String?
    var validityValue : String?
    var price : String?
    var appOfferFilter : String?
    var offeringId : String?
    var offerLevel : String?
    var btnDeactivate : String?
    var btnRenew : String?
    var status : String?
    var offerGroup : OfferGroup?
    var attributeList : [AttributeList]?
    var usage : String?
    var isTopUp : String?

    required init?(map: Map) {

    }

    //    id : "32"
    //    type : "Internet"
    //    offerName : "internet 13"
    //    stickerLabel : "New"
    //    stickerColorCode : "#1C46A6"
    //    validityTitle : ""
    //    validityInformation : ""
    //    validityValue : ""
    //    price : "1.00"
    //    appOfferFilter : ""
    //    offeringId : "1673626906"
    //    offerLevel : ""
    //    btnDeactivate : "0"
    //    btnRenew : "0"
    //    status : null
    //    offerGroup : []
    //    attributeList : []
    //    usage : null

    func mapping(map: Map) {

        id                      <- map["id"]
        type                    <- map["type"]
        offerName               <- map["offerName"]
        stickerLabel            <- map["stickerLabel"]
        stickerColorCode        <- map["stickerColorCode"]
        validityTitle           <- map["validityTitle"]
        validityInformation     <- map["validityInformation"]
        validityValue           <- map["validityValue"]
        price                   <- map["price"]
        appOfferFilter          <- map["appOfferFilter"]
        offeringId              <- map["offeringId"]
        offerLevel              <- map["offerLevel"]
        btnDeactivate           <- map["btnDeactivate"]
        btnRenew                <- map["btnRenew"]
        status                  <- map["status"]
        offerGroup              <- map["offerGroup"]
        attributeList           <- map["attributeList"]
        usage                   <- map["usage"]
        isTopUp                 <- map["isTopUp"]
    }
}

class OfferGroup: Mappable {
    var groupName : String?
    var groupValue : String?
    required init?(map: Map) {

    }
    //    groupName : "Offer group name"
    //    groupValue : "Desktop,Tab,Mobile"

    func mapping(map: Map) {

        groupName       <- map["groupName"]
        groupValue      <- map["groupValue"]

    }
}


class DetailsAndDescription: Mappable {
    var price : Price?
    var rounding : Rounding?
    var textWithTitle : TextWithTitle?
    var textWithOutTitle : String?
    var textWithPoints : [String?]?
    var date : DateAndTime?
    var time : DateAndTime?
    var roamingDetails : RoamingDetails?
    var freeResourceValidity : FreeResourceValidity?
    var titleSubTitleValueAndDesc : TitleSubTitleValueAndDesc?

    //   price
    //    rounding
    //    textWithTitle
    //    textWithOutTitle : null
    //    textWithPoints : null
    //    date
    //    time
    //    roamingDetails : null
    //    freeResourceValidity
    //    titleSubTitleValueAndDesc
    required init?(map: Map) {

    }

    func mapping(map: Map) {

        price                       <- map["price"]
        rounding                    <- map["rounding"]
        textWithTitle               <- map["textWithTitle"]
        textWithOutTitle            <- map["textWithOutTitle.description"]
        textWithPoints              <- map["textWithPoints.pointsList"]
        date                        <- map["date"]
        time                        <- map["time"]
        roamingDetails              <- map["roamingDetails"]
        freeResourceValidity        <- map["freeResourceValidity"]
        titleSubTitleValueAndDesc   <- map["titleSubTitleValueAndDesc"]

    }
}

class RoamingDetails: Mappable {
    var descriptionAbove : String?
    var descriptionBelow : String?
    var roamingDetailsCountriesList : [RoamingDetailsCountries]?


    //    roamingDetailsCountriesList
    //    descriptionAbove : ""
    //    descriptionBelow : " Roaming description Roaming description"
    required init?(map: Map) {

    }

    func mapping(map: Map) {

        descriptionAbove                <- map["descriptionAbove"]
        descriptionBelow                <- map["descriptionBelow"]
        roamingDetailsCountriesList     <- map["roamingDetailsCountriesList"]
    }
}

class RoamingDetailsCountries: Mappable {
    var countryName : String?
    var flag : String?
    var operatorList : [String?]?


    //    countryName : "pakistan"
    //    flag : "pak"
    //    operatorList
    required init?(map: Map) {

    }

    func mapping(map: Map) {

        countryName         <- map["countryName"]
        flag                <- map["flag"]
        operatorList        <- map["operatorList"]
    }
}


class TitleSubTitleValueAndDesc: Mappable {
    var title : String?
    var attributeList : [AttributeList]?


    //    title : "Destination"

    required init?(map: Map) {

    }

    func mapping(map: Map) {

        title               <- map["title"]
        attributeList       <- map["attributeList"]
    }
}

class FreeResourceValidity: Mappable {
    var title : String?
    var titleValue : String?
    var subTitle : String?
    var subTitleValue : String?
    var description : String?

    //    title : "Destination"
    //    titleValue : "value"
    //    subTitle : "Free resource"
    //    subTitleValue : " validity"
    //    description : ""
    required init?(map: Map) {

    }

    func mapping(map: Map) {

        title               <- map["title"]
        titleValue          <- map["titleValue"]
        subTitle            <- map["subTitle"]
        subTitleValue       <- map["subTitleValue"]
        description         <- map["description"]
    }
}

class DateAndTime: Mappable {
    var fromTitle : String?
    var fromValue : String?
    var toTitle : String?
    var toValue : String?
    var description : String?

    //    fromTitle : "from date"
    //    fromValue : "2017-08-01 00:00:00"
    //    toTitle : "To date"
    //    toValue : "2017-08-27 00:00:00"
    //    description : "date text"
    required init?(map: Map) {

    }

    func mapping(map: Map) {

        fromTitle           <- map["fromTitle"]
        fromValue           <- map["fromValue"]
        toTitle             <- map["toTitle"]
        toValue             <- map["toValue"]
        description         <- map["description"]
    }
}

class TextWithTitle: Mappable {
    var title : String?
    var isBullet : Bool = false
    var text : String?

    //    title : "Advantages"
    //    isBullet : "false"
    //    text : "bullets<\n>bullets<\n>bullets<\n>bullets"
    required init?(map: Map) {

    }

    func mapping(map: Map) {

        title           <- map["title"]
        isBullet        <- map["isBullet"]
        text            <- map["text"]
    }
}


class Rounding: Mappable {
    var title : String?
    var value : String?
    var iconName : String?
    var description : String?
    var attributeList : [AttributeList]?

    //    title : "Call"
    //    value : "0.08"
    //    iconName : "callIcon"
    //    description : "Call price description..."
    //    attributeList
    required init?(map: Map) {

    }

    func mapping(map: Map) {

        title           <- map["title"]
        value           <- map["value"]
        iconName        <- map["iconName"]
        description     <- map["description"]
        attributeList   <- map["attributeList"]
    }
}

class Price: Mappable {
    var title : String?
    var value : String?
    var iconName : String?
    var description : String?
    var offersCurrency : String?
    var attributeList : [AttributeList]?

    //    title : " Title name (left title) Testing..."
    //    value : ""
    //    iconName : "callIcon"
    //    description : "Call price description..."
    //    offersCurrency : "manat"
    //    attributeList

    required init?(map: Map) {

    }

    func mapping(map: Map) {

        title           <- map["title"]
        value           <- map["value"]
        iconName        <- map["iconName"]
        description     <- map["description"]
        offersCurrency  <- map["offersCurrency"]
        attributeList   <- map["attributeList"]
    }
}


class AttributeList: Mappable {
    var title : String?
    var value : String?
    var iconMap : String?
    var description : String?
    var unit : String?
    var onnetLabel : String?
    var onnetValue : String?
    var offnetLabel : String?
    var offnetValue : String?

    //    title : "Onnet"
    //    value : "0.05"
    //    iconMap : null
    //    description : null
    //    onnetLabel : ""
    //    onnetValue : ""
    //    offnetLabel : ""
    //    offnetValue : ""

    required init?(map: Map) {

    }

    func mapping(map: Map) {

        title           <- map["title"]
        value           <- map["value"]
        iconMap         <- map["iconMap"]
        description     <- map["description"]
        unit            <- map["unit"]
        onnetLabel      <- map["onnetLabel"]
        onnetValue      <- map["onnetValue"]
        offnetLabel     <- map["offnetLabel"]
        offnetValue     <- map["offnetValue"]
    }
}

class Filters: Mappable {
    var app : [KeyValue]?
    var desktop : [KeyValue]?
    var tab : [KeyValue]?
    
    
    //    app
    //    desktop
    //    tab
    init() {
    }
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        app           <- map["app"]
        desktop       <- map["desktop"]
        tab           <- map["tab"]
    }
}

class KeyValue: Mappable, Copyable {
    var key : String?
    var value : String?
    
    required init?(map: Map) {
        
    }

    required init(instance: KeyValue) {
        self.key = instance.key
        self.value = instance.value
    }

    init(key : String , value : String) {
        self.key = key
        self.value = value
    }
    
    func mapping(map: Map) {
        
        key             <- map["key"]
        value           <- map["value"]
    }
}
