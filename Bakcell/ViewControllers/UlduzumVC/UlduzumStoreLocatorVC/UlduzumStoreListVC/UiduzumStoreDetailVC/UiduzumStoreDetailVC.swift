//
//  UiduzumStoreDetailVC.swift
//  Bakcell
//
//  Created by Muhammad Irfan Awan on 11/12/2020.
//  Copyright © 2020 evampsaanga. All rights reserved.
//

import UIKit
import ObjectMapper

class UiduzumStoreDetailVC: BaseVC {
    
    var storeDetail : UlduzumStoreLocation?
    
    var tableViewData : StoreModel?
    
    
    @IBOutlet var title_lbl: UILabel!
    
    @IBOutlet var nameTitle_lbl: UILabel!
    @IBOutlet var name_lbl: UILabel!
    @IBOutlet var discountTitle_lbl: UILabel!
    @IBOutlet var discount_lbl: UILabel!
    @IBOutlet var iv_icon : UIImageView!
    @IBOutlet var branches_lbl: UILabel!
    @IBOutlet var tableView: UITableView!
    @IBOutlet var noDataView: UIView!
    @IBOutlet var noDataLbl: UILabel!
    
    
    //UlduzumStoreLocation
    

    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.register(UINib(nibName: "StoreDetailCell", bundle: nil), forCellReuseIdentifier: "StoreDetailCell")
        noDataView.isHidden = true
        tableView.isHidden = true
        
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
        self.navigationController?.interactivePopGestureRecognizer?.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupDataForTable()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        getStoreDetails()
    }
    
    override func viewWillLayoutSubviews() {
        title_lbl.text = Localized("title_Details")
        nameTitle_lbl.text = Localized("Title_Name")
        discountTitle_lbl.text = Localized("Title_DiscountPercentage").replacingOccurrences(of: "%", with: "")
        branches_lbl.text = Localized("Info_HeadOfficeAddress")//"Title_BranchesN"
        noDataLbl.text = Localized("Title_NoBranchFound")
        name_lbl.text = storeDetail?.name ?? ""
        
        if (storeDetail?.loyalitySegment ?? "").isNumeric {
            iv_icon.isHidden = true
            discount_lbl.isHidden = false
            discount_lbl.text = "\(storeDetail?.loyalitySegment ?? "")%"
        } else {
            iv_icon.isHidden = false
            discount_lbl.isHidden = true
            discount_lbl.text = ""
        }
    }
    

    func setupDataForTable() {
        title_lbl.text = Localized("StoreDetails")
        tableView.reloadData()
    }
    
    func getStoreDetails() {
        activityIndicator.showActivityIndicator()
        
        _ = MBAPIClient.sharedClient.getBranches(merchantId: storeDetail?.id ?? "",{ ( response, resultData, error, isCancelled, status, resultCode, resultDesc) in
            
            self.activityIndicator.hideActivityIndicator()
            if error != nil {
                //EventLog
                //MBAnalyticsManager.logEvent(screenName: "Store Locator", contentType:"Store Locator Map" , status:"Failure" )
                
                error?.showServerErrorInViewController(self)
                
            } else {
                // handling data from API response.
                if resultCode == Constants.MBAPIStatusCode.succes.rawValue {
                    
                    //EventLog
                    //MBAnalyticsManager.logEvent(screenName: "Store Locator", contentType:"Store Locator Map" , status:"Success" )
                    
                    // Parssing response data
                    if let storeLocatorResponse = Mapper<StoreModel>().map(JSONObject:resultData) {
                        
                        self.tableViewData = storeLocatorResponse
                        self.loadBranchesNow()
                    }
                    
                } else {
                    //EventLog
                    //MBAnalyticsManager.logEvent(screenName: "Store Locator", contentType:"Store Locator Map" , status:"Failure" )
                    
                    // Show error alert to user
                    self.showErrorAlertWithMessage(message: resultDesc)
                }
            }
        })
        
    }
    
    func loadBranchesNow() {
        if self.tableViewData?.branches?.count ?? 0 > 0 {
            tableView.isHidden = false
            noDataView.isHidden = true
        } else {
            tableView.isHidden = true
            noDataView.isHidden = false
        }
        tableView.reloadData()
    }

}

extension UiduzumStoreDetailVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tableViewData?.branches?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 44.0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "StoreDetailCell", for: indexPath) as? StoreDetailCell {
            if let aStoreInfo = self.tableViewData?.branches?[indexPath.row] {
//                cell.nameLbl.text = aStoreInfo.name
                cell.addressLbl.text = aStoreInfo.address
            }
            return cell
        }
        return UITableViewCell()
    }
    
    
}
