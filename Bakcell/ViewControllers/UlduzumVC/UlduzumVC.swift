//
//  UlduzumVC.swift
//  Bakcell
//
//  Created by Muhammad Waqas on 1/2/19.
//  Copyright © 2019 evampsaanga. All rights reserved.
//

import UIKit
import ObjectMapper

class UlduzumVC: BaseVC {

    //MARK:- Properties
    var usageTotals : UlduzumUsageTotals?
    var unusedCodes : [UlduzumUnusedCodes]?
    var newCode : UlduzumNewCode?
    
    //MARK:- IBOutlets
    @IBOutlet var lblTopHeader : UILabel!
    @IBOutlet var lblTotalDiscountsTitle : UILabel!
    @IBOutlet var lblTotalDiscountsValue : UILabel!
    @IBOutlet var lblUlduzumTop : UILabel!

    @IBOutlet var lblCode1 : UILabel!
    @IBOutlet var lblCode2 : UILabel!
    @IBOutlet var lblCode3 : UILabel!
    @IBOutlet var lblCode4 : UILabel!
    
    @IBOutlet var lblUsageHistory : UILabel!
    @IBOutlet var lblLocations : UILabel!

    @IBOutlet var lblRemainingCodesTitle : UILabel!
    @IBOutlet var lblRemainingCodesValue : UILabel!
    
    @IBOutlet var btnGetCode : UIButton!
    @IBOutlet var btnUnUsedCodes : UIButton!
    
    @IBOutlet var ivStarCode1 : UIImageView!
    @IBOutlet var ivStarCode2 : UIImageView!
    @IBOutlet var ivStarCode3 : UIImageView!
    @IBOutlet var ivStarCode4 : UIImageView!
    
    @IBOutlet var progressBar : MBGradientProgressView!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        loadViewLayout()
        
        //init ViewController
        self.initViewController()
    }
    
    // MARK: - MixMethods
    
    /**
     Set localized text in viewController
     */
    func loadViewLayout() {
        lblTopHeader.text = Localized("Title_Ulduzum")
        lblTotalDiscountsTitle.text = Localized("Info_TotalSaved")
        lblUlduzumTop.text = MBUserSession.shared.userInfo?.loyaltySegment
        lblTotalDiscountsValue.text = "0.0"
        
        lblRemainingCodesTitle.text = Localized("Info_RemainingCodes")
        lblUsageHistory.text = Localized("Title_ShoppingRecords")
        lblLocations.text = Localized("Title_Branches")
        
        
        btnGetCode.setTitle(Localized("Btn_GetCode"), for: .normal)
        btnUnUsedCodes.setTitle(Localized("Btn_UnusedCode"), for: .normal)
    }
    
    func initViewController() {
        
        self.showCodeInBoxes(newCode: "")
        
        self.refreshData()
        
        self.lblCode1.isHidden = true
        self.lblCode2.isHidden = true
        self.lblCode3.isHidden = true
        self.lblCode4.isHidden = true
        
        self.ivStarCode1.isHidden = false
        self.ivStarCode2.isHidden = false
        self.ivStarCode3.isHidden = false
        self.ivStarCode4.isHidden = false
        
        //initial API call
        self.loadUlduzumUsageData()
    }
    
    func refreshData() {
        
        //checking if usageTotal = nil than simply reset labels otherwise set their values in appropriate controls
        if let usageTotalsObject = usageTotals {
            self.lblTotalDiscountsValue.text = usageTotalsObject.totalDiscount
            self.lblRemainingCodesValue.text = "\(usageTotalsObject.dailyLimitLeft)/\(usageTotalsObject.dailyLimitTotal)"
            
            progressBar.setProgress((usageTotals?.dailyLimitLeft.floatValue ?? 0.0) / (usageTotals?.dailyLimitTotal.floatValue ?? 0.0), animated: true)
            
        } else {
            self.lblTotalDiscountsValue.text = "0.0"
            self.lblRemainingCodesValue.text = "0/0"
            
            progressBar.setProgress(0.0, animated: false)
        }
    }
    
    func showCodeInBoxes(newCode : String) {
        
        if (newCode.length > 0) {
            
            //Hide/show appropriate labels
            self.ivStarCode1.isHidden = true
            self.ivStarCode2.isHidden = true
            self.ivStarCode3.isHidden = true
            self.ivStarCode4.isHidden = true
            
            self.lblCode1.isHidden = false
            self.lblCode2.isHidden = false
            self.lblCode3.isHidden = false
            self.lblCode4.isHidden = false
            
            
            //showing new code into boxes
            let arrayNewCode = Array(newCode)
            if arrayNewCode.count > 0 {
                if (arrayNewCode.count == 4) {
                    self.lblCode1.text = "\(arrayNewCode[0])"
                    self.lblCode2.text = "\(arrayNewCode[1])"
                    self.lblCode3.text = "\(arrayNewCode[2])"
                    self.lblCode4.text = "\(arrayNewCode[3])"
                }
            }
        } else {
            //Hide/show appropriate labels
            self.ivStarCode1.isHidden = false
            self.ivStarCode2.isHidden = false
            self.ivStarCode3.isHidden = false
            self.ivStarCode4.isHidden = false
            
            self.lblCode1.isHidden = true
            self.lblCode2.isHidden = true
            self.lblCode3.isHidden = true
            self.lblCode4.isHidden = true
            
            self.lblCode1.text = ""
            self.lblCode2.text = ""
            self.lblCode3.text = ""
            self.lblCode4.text = ""
        }
    }
    
    
    func loadUlduzumUsageData() {
        // Load data from UserDefaults
        if let responseData : UlduzumUsageTotals = UlduzumUsageTotals.loadFromUserDefaults(key: APIsType.ulduzumUsage.selectedLocalizedAPIKey()) {
            
            self.usageTotals = responseData;
            self.refreshData()
            
            getUsageTotalsAPICall(false)
            
        } else {
            getUsageTotalsAPICall()
        }
    }
    
    // MARK: - IBActions
    
    @IBAction func getCodePressed(_ sender: UIButton) {
        
        if let alert = self.myStoryBoard.instantiateViewController(withIdentifier: "AlertVC") as? AlertVC {
            self.presentPOPUP(alert, animated: true, completion: nil)
            
            alert.setConfirmationAlertLayoutWithOutBalance(title: Localized("Title_Confirmation"), message: Localized("Message_ConformGetCode"))
            
            alert.setYesButtonCompletionHandler(completionBlock: { (aString) in
                self.getCodeAPICall()
            })
        }
        
        
    }
    
    @IBAction func unusedCodesPressed(_ sender: UIButton) {
        
        self.getUnusedCodesAPICall()
    }
    
    @IBAction func usageHistoryPressed(_ sender: UIButton) {
        
        if let ulduzumUsageHistoryObj = self.myStoryBoard.instantiateViewController(withIdentifier: "UlduzumUsageHistoryVC") as? UlduzumUsageHistoryVC {
            self.navigationController?.pushViewController(ulduzumUsageHistoryObj, animated: true)
        }
    }
    
    @IBAction func locatorPressed(_ sender: UIButton) {
        
        if let ulduzumUsageHistoryObj = self.myStoryBoard.instantiateViewController(withIdentifier: "UlduzumStoreLocatorVC") as? UlduzumStoreLocatorVC {
            self.navigationController?.pushViewController(ulduzumUsageHistoryObj, animated: true)
        }
    }
    
    // MARK: - APICall
    func getUsageTotalsAPICall(_ showIndicator: Bool = true) {
        
        if showIndicator {
            activityIndicator.showActivityIndicator()
        }
        
        _ = MBAPIClient.sharedClient.getUlduzumUsageTotals({ (response, resultData, error, isCancelled, status, resultCode, resultDesc) in
            
            if showIndicator {
                self.activityIndicator.hideActivityIndicator()
            }
            
            if error != nil {
                error?.showServerErrorInViewController(self)
                
            } else {
                
                // handling data from API response.
                if resultCode == Constants.MBAPIStatusCode.succes.rawValue {
                    if let responseHandler = Mapper<UlduzumUsageTotals>().map(JSONObject: resultData) {
                        
                        // Saving Ulduzum data in user defaults
                        responseHandler.saveInUserDefaults(key: APIsType.ulduzumUsage.selectedLocalizedAPIKey())
                        
                        self.usageTotals = responseHandler;
                        self.refreshData()
                        
                    } else {
                        self.showErrorAlertWithMessage(message: resultDesc)
                    }
                }
            }
        })
    }
    
    func getCodeAPICall() {
        
        self.activityIndicator.showActivityIndicator()
        
        _ = MBAPIClient.sharedClient.getUlduzumCode({ (response, resultData, error, isCancelled, status, resultCode, resultDesc) in
            
            self.activityIndicator.hideActivityIndicator()
            
            if error != nil {
                error?.showServerErrorInViewController(self)
                
            } else {
                
                // handling data from API response.
                if resultCode == Constants.MBAPIStatusCode.succes.rawValue {
                    if let responseHandler = Mapper<UlduzumNewCode>().map(JSONObject: resultData) {
                        
                        self.newCode = responseHandler;
                        
                        //showing new code into boxes
                        self.showCodeInBoxes(newCode: self.newCode?.code ?? "")
                        
                        self.getUsageTotalsAPICall(false)
                        
                    } else {
                        self.showErrorAlertWithMessage(message: resultDesc)
                    }
                }
            }
        })
    }
    
    func getUnusedCodesAPICall() {
        
        self.activityIndicator.showActivityIndicator()
        
        _ = MBAPIClient.sharedClient.getUlduzumUnusedCodes({ (response, resultData, error, isCancelled, status, resultCode, resultDesc) in
            
            self.activityIndicator.hideActivityIndicator()
            
            if error != nil {
                error?.showServerErrorInViewController(self)
                
            } else {
                
                // handling data from API response.
                if resultCode == Constants.MBAPIStatusCode.succes.rawValue {
                    if let responseHandler = Mapper<UlduzumUnusedCodesHandler>().map(JSONObject: resultData) {

                        self.unusedCodes = responseHandler.unusedCodesList;
                        
                        //show popup with result
                        if let UnusedCodesPopUp = self.myStoryBoard.instantiateViewController(withIdentifier: "UnusedCodesPopUpVC") as? UnusedCodesPopUpVC {
                            UnusedCodesPopUp.unusedCodes = self.unusedCodes
                            self.presentPOPUP(UnusedCodesPopUp, animated: true, completion: nil)
                        }

                    } else {
                        self.showErrorAlertWithMessage(message: resultDesc)
                    }
                }
            }
        })
    }
}
