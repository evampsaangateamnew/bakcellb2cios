//
//  FAQsCell.swift
//  Bakcell
//
//  Created by Saad Riaz on 6/5/17.
//  Copyright © 2017 evampsaanga. All rights reserved.
//

import UIKit

class FAQsCell: UITableViewCell {
    
    static let ID = "cellID"

    @IBOutlet var answer_lbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
