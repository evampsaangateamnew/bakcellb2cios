//
//  TariffsVC.swift
//  Bakcell
//
//  Created by Saad Riaz on 5/30/17.
//  Copyright © 2017 evampsaanga. All rights reserved.
//

import UIKit
import UPCarouselFlowLayout
import ObjectMapper

enum MBDataTypes : String {
    case TopDescription = "TopDescription"
    case Call = "Call"
    case SMS = "SMS"
    case Internet = "Internet"
    case MMS = "MMS"
    case Destination = "Destination"
    case InternationalOnPeak = "InternationalOnPeak"
    case InternationalOffPeak = "InternationalOffPeak"
    case Advantages = "Advantages"
    case Classification = "Classification"
}

class TariffsVC: BaseVC {
    
    //MARK: Properties
    var cinData : [NewCinTarrif]?
    var individualData : [Individual]?
    var klassPostpaidData : [KlassPostpaid]?
    var selectedType : MBTariffScreenType!
    var scrollToIndex = 0
    
    // USED to toggle
    var selectedSectionViewType : MBSectionType = MBSectionType.UnSpecified
    
    
    //MARK: IBOutlet
    @IBOutlet var collectionView: UICollectionView!
    
    //MARK: - ViewControllers
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let layout = UPCarouselFlowLayout()
        layout.itemSize = CGSize(width: UIScreen.main.bounds.width * 0.79, height:  UIScreen.main.bounds.height * 0.70)
        collectionView.collectionViewLayout = layout
        layout.scrollDirection = .horizontal
        
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if selectedType == .CIN {
            //EventLog
            MBAnalyticsManager.logEvent(screenName: "Tariff Screen", contentType:"Tariff CIN Screen" , status: "Tariff CIN Screen" )
            
            if cinData?.count ?? 0 > scrollToIndex {
                collectionView.scrollToItem(at: IndexPath(item:scrollToIndex, section:0), at: .centeredHorizontally, animated: true)
                
            } else if (cinData?.count ?? 0 <= 0) {
                self.view.showDescriptionViewWithImage(description: Localized("Message_NoDataAvalible"))
            }
        } else if selectedType == .Individual {
            //EventLog
            MBAnalyticsManager.logEvent(screenName: "Tariff Screen", contentType:"Tariff Individual Screen" , status: "Tariff Individual Screen" )
            
            if individualData?.count ?? 0 > scrollToIndex {
                collectionView.scrollToItem(at: IndexPath(item:scrollToIndex, section:0), at: .centeredHorizontally, animated: true)
                
            } else if (individualData?.count ?? 0 <= 0) {
                self.view.showDescriptionViewWithImage(description: Localized("Message_NoDataAvalible"))
            }
        } else if selectedType == .KlassPostpaid {
            //EventLog
            MBAnalyticsManager.logEvent(screenName: "Tariff Screen", contentType:"Tariff Klass Postpaid Screen" , status: "Tariff Klass Postpaid Screen" )
            
            if klassPostpaidData?.count ?? 0 > scrollToIndex {
                collectionView.scrollToItem(at: IndexPath(item:scrollToIndex, section:0), at: .centeredHorizontally, animated: true)
                
            } else if (klassPostpaidData?.count ?? 0 <= 0) {
                self.view.showDescriptionViewWithImage(description: Localized("Message_NoDataAvalible"))
            }
            
        } else {
            self.view.showDescriptionViewWithImage(description: Localized("Message_NoDataAvalible"))
        }
    }
    
    //MARK: - IBAction
    
    @IBAction func subscribeButtonPressed(_ sender: UIButton) {
        
        var selectedOfferingId: String = ""
        var selectedOfferName: String = ""
        var selectedOfferSubscribable: String = ""
        
        if selectedType == MBTariffScreenType.CIN {
            
            if let aTariffObject = cinData?[sender.tag]{
                
                if aTariffObject.tariffType == .cin,
                    let aCINObject = aTariffObject.valueObject as? Cin {
                    
                    selectedOfferingId = aCINObject.header?.offeringId ?? ""
                    selectedOfferName = aCINObject.header?.name ?? ""
                    selectedOfferSubscribable = aCINObject.header?.subscribable ?? ""
                    
                } else if aTariffObject.tariffType == .newCin,
                    let aNewCINObject = aTariffObject.valueObject as? Klass {
                    
                    selectedOfferingId = aNewCINObject.header?.offeringId ?? ""
                    selectedOfferName = aNewCINObject.header?.name ?? ""
                    selectedOfferSubscribable = aNewCINObject.header?.subscribable ?? ""
                }
            }
            
        } else if selectedType == .Individual {
            
            selectedOfferingId = individualData?[sender.tag].header?.offeringId ?? ""
            selectedOfferName = individualData?[sender.tag].header?.name ?? ""
            selectedOfferSubscribable = individualData?[sender.tag].header?.subscribable ?? ""
            
        } else {
            
            selectedOfferingId = klassPostpaidData?[sender.tag].header?.offeringId ?? ""
            selectedOfferName = klassPostpaidData?[sender.tag].header?.name ?? ""
            selectedOfferSubscribable = klassPostpaidData?[sender.tag].header?.subscribable ?? ""
        }
        
        showConfirmationPOPUP(offeringId: selectedOfferingId, offerName: selectedOfferName, subscribableValue: selectedOfferSubscribable)
        
    }
    
    //MARK: - Functions
    func showConfirmationPOPUP(offeringId: String?, offerName: String?, subscribableValue : String?) {
        
        let confirmationAlert = self.myStoryBoard.instantiateViewController(withIdentifier: "ActivationConfirmationVC") as! ActivationConfirmationVC
        
        
        let migrationMessage = MBUtilities.getTariffMigrationConfirmationMessage(offeringID: offeringId)
        
        confirmationAlert.setConfirmationAlertLayoutForTariffMigration(message: migrationMessage)
        
        confirmationAlert.setYesButtonCompletionHandler { (aString) in
            self.changetTariff(offeringId: offeringId ?? "", offerName: offerName ?? "", subscribableValue: subscribableValue ?? "")
        }
        
        self.presentPOPUP(confirmationAlert, animated: true, completion: nil)
    }
    
    //MARK: - API Calls
    /// Call 'changetTariff' API .
    ///
    /// - parameter offeringId:     offerId of selected offer
    /// - parameter offerName:     offerName of selected offer
    /// - parameter subscribableValue:     subscribable value of selected offer
    ///
    /// - returns: Void
    func changetTariff(offeringId: String, offerName: String, subscribableValue: String) {
        
        
        activityIndicator.showActivityIndicator()
        
        _ = MBAPIClient.sharedClient.changetTariff(offeringId: offeringId, offerName: offerName,subscribableValue: subscribableValue, { (response, resultData, error, isCancelled, status, resultCode, resultDesc) in
            
            self.activityIndicator.hideActivityIndicator()
            if error != nil {
                error?.showServerErrorInViewController(self)
                
            } else {
                // handling data from API response.
                if resultCode == Constants.MBAPIStatusCode.succes.rawValue {
                    
                    // Parssing response data
                    if let changeTariffResponse = Mapper<MessageResponse>().map(JSONObject: resultData){
                        
                        // Show Error alert
                        self.showSuccessAlertWithMessage(message: changeTariffResponse.message)
                    } else {
                        // Show Error alert
                        self.showErrorAlertWithMessage(message: resultDesc)
                    }
                } else {
                    // Show Error alert
                    self.showErrorAlertWithMessage(message: resultDesc)
                }
            }
        })
    }
    
}

extension TariffsVC: UICollectionViewDelegate,UICollectionViewDataSource {
    //MARK: - COLLECTION VIEW METHODS
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if selectedType == MBTariffScreenType.CIN {
            return cinData?.count ?? 0
            
        } else if selectedType == MBTariffScreenType.Individual{
            
            return individualData?.count ?? 0
        } else if selectedType == MBTariffScreenType.KlassPostpaid{
            
            return klassPostpaidData?.count ?? 0
        }
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CarouselCollectionViewCell", for: indexPath) as! TariffCell
        print("item to show:\(indexPath.item)")
        print("self.shouldOpenSection(index: indexPath.item): \(self.shouldOpenSection(index: indexPath.item))")
        var isSubscribed = ""
        if selectedType == MBTariffScreenType.CIN {
            
            if let aTariffObject = cinData?[indexPath.item] {
                
                if aTariffObject.tariffType == .cin,
                    let aCINObject = aTariffObject.valueObject as? Cin {
                    
                    isSubscribed = aCINObject.header?.subscribable ?? ""
                    
                } else if aTariffObject.tariffType == .newCin,
                    let aNewCINObject = aTariffObject.valueObject as? Klass {
                    
                    isSubscribed = aNewCINObject.header?.subscribable ?? ""
                }
            }
        } else if selectedType == MBTariffScreenType.Individual {
            
            isSubscribed = individualData?[indexPath.item].header?.subscribable ?? ""
            
        } else {
            
            isSubscribed = klassPostpaidData?[indexPath.item].header?.subscribable ?? ""
        }
        if isSubscribed == "2" || isSubscribed == "3" {
            cell.btnsStackView.isHidden = false
            cell.btnsStackView.snp.updateConstraints { const in
                const.height.equalTo(20)
            }
        } else {
            cell.btnsStackView.isHidden = true
            cell.btnsStackView.snp.updateConstraints { const in
                const.height.equalTo(0)
            }
        }
        
        
        cell.cinData = cinData?[indexPath.item]
        cell.individualData = individualData?[indexPath.item]
        cell.klassPostpaidData = klassPostpaidData?[indexPath.item]
        cell.selectedType = selectedType
        cell.delegate = self
        cell.setRating()
        cell.tableView.delegate = self
        cell.tableView.dataSource = self
        cell.tableView.sectionInfos = []
        cell.tableView.allowMultipleSectionsOpen = false
        cell.tableView.keepOneSectionOpen = true;
        cell.tableView.tag = indexPath.item
        cell.tableView.allowsSelection = false
        
        cell.tableView.initialOpenSections = [0]
        
        cell.tableView.reloadData()
        
        return cell
        
    }
    
    // USED to toggle
    func openSelectedSection() {
        
        
        let visbleIndexPath = collectionView.indexPathsForVisibleItems
        
        visbleIndexPath.forEach { (aIndexPath) in
            
            if let cell = collectionView.cellForItem(at: aIndexPath) as? TariffCell {
                
                let type = typeOfDataInSection(index: cell.tableView.tag)
                
//                for i in 0..<type.count {
//                    cell.tableView.markSection(i, open: false)
//                }
//                cell.tableView.closeAllSections(shouldCallDelegate: false)
                
                if selectedType == .CIN {
                    var isNewCINTariff : Bool = false
                    
                    if let aTariffObject = cinData?[cell.tableView.tag] ,
                        aTariffObject.tariffType == .newCin{
                        
                        isNewCINTariff = true
                    }
                    
                    if selectedSectionViewType == MBSectionType.Header && type.contains(MBSectionType.Header) {
                        
                        openSectionAt(index: 0, tableView: cell.tableView)
                        
                    } else if selectedSectionViewType == MBSectionType.Detail {
                        
                        var index :Int? = 0
                        
                        if isNewCINTariff {
                            if type.contains(MBSectionType.PackagePrice) {
                                
                                index = type.firstIndex(of:MBSectionType.PackagePrice)
                                
                            } else if type.contains(MBSectionType.PaygPrice) {
                                
                                index = type.firstIndex(of:MBSectionType.PaygPrice)
                            }
                            
                        } else if type.contains(MBSectionType.Detail) {
                            index = type.firstIndex(of:MBSectionType.Detail)
                        }
                        
                        openSectionAt(index: index ?? 0, tableView: cell.tableView)
                        
                    } else if selectedSectionViewType == MBSectionType.DetailNewCIN {
                        
                        var index :Int? = 0
                        
                        if isNewCINTariff == false,
                            type.contains(MBSectionType.Description) {
                            
                            index = type.firstIndex(of:MBSectionType.Description)
                            
                        } else if type.contains(MBSectionType.DetailNewCIN) {
                            index = type.firstIndex(of:MBSectionType.DetailNewCIN)
                        }
                        
                        openSectionAt(index: index ?? 0, tableView: cell.tableView)
                        
                    } else if selectedSectionViewType == MBSectionType.Description {
                        
                        var index :Int? = 0
                        
                        if isNewCINTariff ,
                            type.contains(MBSectionType.DetailNewCIN) {
                            
                            index = type.firstIndex(of:MBSectionType.DetailNewCIN)
                            
                        } else if type.contains(MBSectionType.Description) {
                            
                            index = type.firstIndex(of:MBSectionType.Description)
                        }
                        
                        openSectionAt(index: index ?? 0, tableView: cell.tableView)
                        
                    } else if selectedSectionViewType == MBSectionType.PackagePrice {
                        
                        var index :Int? = 0
                        
                        if isNewCINTariff == false,
                            type.contains(MBSectionType.Detail) {
                            
                            index = type.firstIndex(of:MBSectionType.Detail)
                            
                        } else if type.contains(MBSectionType.PackagePrice) {
                            index = type.firstIndex(of:MBSectionType.PackagePrice)
                        }
                        
                        openSectionAt(index: index ?? 0, tableView: cell.tableView)
                        
                    } else if selectedSectionViewType == MBSectionType.PaygPrice {
                        
                        var index :Int? = 0
                        
                        if isNewCINTariff == false,
                            type.contains(MBSectionType.Detail) {
                            
                            index = type.firstIndex(of:MBSectionType.Detail)
                            
                        } else if type.contains(MBSectionType.PaygPrice) {
                            index = type.firstIndex(of:MBSectionType.PaygPrice)
                        }
                        
                        openSectionAt(index: index ?? 0, tableView: cell.tableView)
                        
                    } else {
                        openSectionAt(index: 0, tableView: cell.tableView)
                    }
                    
                } else {
                    if selectedSectionViewType == MBSectionType.Header && type.contains(MBSectionType.Header) {
                        
                        openSectionAt(index: 0, tableView: cell.tableView)
                        
                    } else if selectedSectionViewType == MBSectionType.Detail && type.contains(MBSectionType.Detail) {
                        
                        let index = type.firstIndex(of:MBSectionType.Detail)
                        
                        openSectionAt(index: index ?? 0, tableView: cell.tableView)
                        
                    } else if selectedSectionViewType == MBSectionType.Description && type.contains(MBSectionType.Description) {
                        
                        let index = type.firstIndex(of:MBSectionType.Description)
                        
                        openSectionAt(index: index ?? 0, tableView: cell.tableView)
                        
                    } else if selectedSectionViewType == MBSectionType.PackagePrice && type.contains(MBSectionType.PackagePrice) {
                        
                        let index = type.firstIndex(of:MBSectionType.PackagePrice)
                        
                        openSectionAt(index: index ?? 0, tableView: cell.tableView)
                        
                    } else {
                        openSectionAt(index: 0, tableView: cell.tableView)
                    }
                }
            }
        }
    }
    
    func openSectionAt(index : Int , tableView : MBAccordionTableView) {
        
        if tableView.isSectionOpen(index) == false {
            tableView.toggleSection(withOutCallingDelegates: index)
        } 
    }
}

//MARK: SupplementaryOffersCellDelegate
extension TariffsVC: SupplementaryOffersCellDelegate {
    func starTapped(offerId: String) {
        
        if let inAppSurveyVC : InAppSurveyVC = InAppSurveyVC.fromNib() {
            if  let  userSurvey = MBUserSession.shared.appSurvays?.userSurveys?.first(where: { $0.offeringId == offerId}){
                if let survey  = MBUserSession.shared.appSurvays?.surveys?.first(where: { $0.survayId  == userSurvey.surveryId }){
                    inAppSurveyVC.currentSurvay = survey
                    if let  question =  survey.questions?.first(where: { $0.questionId == userSurvey.questionId }){
                        inAppSurveyVC.survayQuestion = question
                        if let answer = question.answers?.firstIndex(where: { $0.answerId ==  userSurvey.answerId }) {
                            inAppSurveyVC.selectedAnswer = question.answers?[answer]
                        }
                    }
                }
                inAppSurveyVC.survayComment = userSurvey.comments
                inAppSurveyVC.offeringId = offerId
                inAppSurveyVC.offeringType = "1"
            } else {
                if let survey = MBUserSession.shared.appSurvays?.surveys?.first(where: { $0.screenName ?? "" == SurveyScreenName.tariff.rawValue}) {
                    inAppSurveyVC.currentSurvay = survey
                    inAppSurveyVC.offeringId = offerId
                    inAppSurveyVC.offeringType = "1"
                }
            }
            inAppSurveyVC.callback =  {
                self.collectionView.reloadData()
            }
            self.presentPOPUP(inAppSurveyVC, animated: true, completion: nil)
        }
        
    }
    
    
}

// MARK: - <UITableViewDataSource> / <UITableViewDelegate> -

extension TariffsVC : UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return typeOfDataInSection(index: tableView.tag).count
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0 {
            return 35
        } else {
            return 45
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let types = typeOfDataInSection(index: tableView.tag)
        
        switch types[section] {
            
        case MBSectionType.Header:
            
            let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: IndividualTitleView.identifier) as! IndividualTitleView
            
            if selectedType == MBTariffScreenType.CIN {
                
                if let aTariffObject = cinData?[tableView.tag] {
                    
                    if aTariffObject.tariffType == .cin,
                        let aCINObject = aTariffObject.valueObject as? Cin {
                        
                        headerView.setViewWith(offerName: aCINObject.header?.name ?? "")
                        
                    } else if aTariffObject.tariffType == .newCin,
                        let aNewCINObject = aTariffObject.valueObject as? Klass {
                        
                        headerView.setViewWith(offerName: aNewCINObject.header?.name ?? "", priceLabel: aNewCINObject.header?.priceLabel, priceValue: aNewCINObject.header?.priceValue)
                    }
                }
                
            } else if selectedType == MBTariffScreenType.Individual {
                
                headerView.setViewWith(offerName: individualData?[tableView.tag].header?.name ?? "", priceLabel: individualData?[tableView.tag].header?.priceLabel ?? "", priceValue: individualData?[tableView.tag].header?.priceValue ?? "")
                
            } else if selectedType == MBTariffScreenType.KlassPostpaid {
                
                headerView.setViewWith(offerName: klassPostpaidData?[tableView.tag].header?.name ?? "", priceLabel: klassPostpaidData?[tableView.tag].header?.priceLabel ?? "", priceValue: klassPostpaidData?[tableView.tag].header?.priceValue ?? "")
                
            }
            return headerView
            
        case MBSectionType.PackagePrice:
            
            let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: SupplementaryOffersHeaderView.identifier) as! SupplementaryOffersHeaderView
            
            var title :String = ""
            if selectedType == MBTariffScreenType.CIN,
                let aTariffObject = cinData?[tableView.tag],
                aTariffObject.tariffType == .newCin,
                let aNewCINObject = aTariffObject.valueObject as? Klass {
                
                title = aNewCINObject.packagePrice?.packagePriceLabel ?? ""
                
            } else {
                title = klassPostpaidData?[tableView.tag].packagePrice?.packagePriceLabel ?? ""
            }
            
            if self.shouldOpenSection(index: tableView.tag) == MBSectionType.PackagePrice {
                headerView.setViewWithTitle(title: title, isSectionSelected: true, headerType: MBSectionType.PackagePrice)
            } else {
                headerView.setViewWithTitle(title: title, isSectionSelected: false, headerType: MBSectionType.PackagePrice)
            }
            
            return headerView
            
        case MBSectionType.PaygPrice:
            
            let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: SupplementaryOffersHeaderView.identifier) as! SupplementaryOffersHeaderView
            
            var title :String = ""
            if let aTariffObject = cinData?[tableView.tag],
                aTariffObject.tariffType == .newCin,
                let aNewCINObject = aTariffObject.valueObject as? Klass {
                title = aNewCINObject.paygPrice?.paygPriceLabel ?? ""
                
            }
            
            if self.shouldOpenSection(index: tableView.tag) == MBSectionType.PaygPrice {
                headerView.setViewWithTitle(title: title, isSectionSelected: true, headerType: MBSectionType.PaygPrice)
            } else {
                headerView.setViewWithTitle(title: title, isSectionSelected: false, headerType: MBSectionType.PaygPrice)
            }
            
            return headerView
            
        case MBSectionType.Price:
            
            let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: SupplementaryOffersHeaderView.identifier) as! SupplementaryOffersHeaderView
            
            let title :String = individualData?[tableView.tag].price?.priceLabel ?? ""
            
            if selectedSectionViewType == MBSectionType.Price {
                headerView.setViewWithTitle(title: title, isSectionSelected: true, headerType: MBSectionType.Price)
            } else {
                headerView.setViewWithTitle(title: title, isSectionSelected: false, headerType: MBSectionType.Price)
            }
            
            return headerView
            
        case MBSectionType.Detail, .DetailNewCIN:
            let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: SupplementaryOffersHeaderView.identifier) as! SupplementaryOffersHeaderView
            
            
            let title :String = Localized("Title_Details")
            
            if let aTariffObject = cinData?[tableView.tag],
                aTariffObject.tariffType == .newCin {
                
                if self.shouldOpenSection(index: tableView.tag) == MBSectionType.DetailNewCIN {
                    headerView.setViewWithTitle(title: title, isSectionSelected: true, headerType: MBSectionType.DetailNewCIN)
                } else {
                    headerView.setViewWithTitle(title: title, isSectionSelected: false, headerType: MBSectionType.DetailNewCIN)
                }
                return headerView
                
            } else {
                if self.shouldOpenSection(index: tableView.tag) == MBSectionType.Detail {
                    headerView.setViewWithTitle(title: title, isSectionSelected: true, headerType: MBSectionType.Detail)
                } else {
                    headerView.setViewWithTitle(title: title, isSectionSelected: false, headerType: MBSectionType.Detail)
                }
                return headerView
            }
            
        case MBSectionType.Description:
            
            let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: SupplementaryOffersHeaderView.identifier) as! SupplementaryOffersHeaderView
            
            let title :String = Localized("Title_Description")
            /*if selectedType == MBTariffScreenType.CIN {
             title = cinData?[tableView.tag].description?.descriptionTitle ?? ""
             
             } else if selectedType == MBTariffScreenType.Individual {
             
             title = individualData?[tableView.tag].description?.descriptionTitle ?? ""
             }*/
            
            if self.shouldOpenSection(index: tableView.tag) == MBSectionType.Description {
                headerView.setViewWithTitle(title: title, isSectionSelected: true, headerType: MBSectionType.Description)
            } else {
                headerView.setViewWithTitle(title: title, isSectionSelected: false, headerType: MBSectionType.Description)
            }
            
            return headerView
            
        case MBSectionType.Subscription:
            
            let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: SubscribeButtonView.identifier) as! SubscribeButtonView
            
            var isSubscribed :String = ""
            
            if selectedType == MBTariffScreenType.CIN {
                
                if let aTariffObject = cinData?[tableView.tag] {
                    
                    if aTariffObject.tariffType == .cin,
                        let aCINObject = aTariffObject.valueObject as? Cin {
                        
                        isSubscribed = aCINObject.header?.subscribable ?? ""
                        
                    } else if aTariffObject.tariffType == .newCin,
                        let aNewCINObject = aTariffObject.valueObject as? Klass {
                        
                        isSubscribed = aNewCINObject.header?.subscribable ?? ""
                    }
                }
            } else if selectedType == MBTariffScreenType.Individual {
                
                isSubscribed = individualData?[tableView.tag].header?.subscribable ?? ""
                
            } else {
                
                isSubscribed = klassPostpaidData?[tableView.tag].header?.subscribable ?? ""
            }
            
            headerView.setViewForSubscribeButtonForTariff(isSubscribed: isSubscribed, tag: tableView.tag)
            
            headerView.subscribed_btn.addTarget(self, action: #selector(subscribeButtonPressed(_:)), for: UIControl.Event.touchUpInside)
            
            return headerView
            
        default:
            break
        }
        
        return UITableViewHeaderFooterView()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        let sectionType = typeOfDataInSection(index: tableView.tag)
        
        switch sectionType[section] {
            
        case MBSectionType.Header:
            return numberOfRowsInHeaderSection(index: tableView.tag)
            
        case MBSectionType.PackagePrice:
            return typeOfDataInPackagePriceSection(index: tableView.tag).count
        
        case MBSectionType.PaygPrice:
            return numberOfRowsInPaygPrice(aPriceObject: (cinData?[tableView.tag].valueObject as? Klass)?.paygPrice)
            
        case MBSectionType.Price:
            return typeOfDataInPrice(aPriceObject: individualData?[tableView.tag].price).count
            
        case MBSectionType.Detail, .DetailNewCIN:
            return numberOfRowsInDetailSection(index: tableView.tag)
            
            
        case MBSectionType.Description:
            return numberOfRowsInDescriptionSection(index: tableView.tag)
            
        default:
            break
        }
        
        return 0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let sectionType = typeOfDataInSection(index: tableView.tag)
        
        switch sectionType[indexPath.section] {
            
        case MBSectionType.Header:
            
            let headerDataType = typeOfDataInHeaderSection(index: tableView.tag)
            
            if selectedType == MBTariffScreenType.CIN {
                
                if let aTariffObject = cinData?[tableView.tag] {
                    
                    if aTariffObject.tariffType == .cin,
                        let aCINObject = aTariffObject.valueObject as? Cin {
                        
                        if let aHeader = aCINObject.header {
                            
                            switch headerDataType[indexPath.row] {
                                
                            case MBDataTypes.TopDescription:
                                
                                let cell = tableView.dequeueReusableCell(withIdentifier: BounsDescriptionCell.identifier) as! BounsDescriptionCell
                                cell.setTopDescriptionLayoutValues(bonusTitle: aHeader.bonusTitle, bonusIconName: aHeader.bonusIconName, bonusDescription: aHeader.bonusDescription)
                                return cell
                                
                            case MBDataTypes.Call:
                                
                                let cell = tableView.dequeueReusableCell(withIdentifier: PriceAndRoundingCell.identifier) as! PriceAndRoundingCell
                                cell.setCallAndSMSLayoutValues(callHeaderType: aHeader.call, cellType:MBDataTypes.Call)
                                
                                return cell
                                
                            case MBDataTypes.SMS:
                                
                                let cell = tableView.dequeueReusableCell(withIdentifier: PriceAndRoundingCell.identifier) as! PriceAndRoundingCell
                                cell.setCallAndSMSLayoutValues(callHeaderType: aHeader.sms, cellType:MBDataTypes.SMS)
                                
                                return cell
                                
                            case MBDataTypes.Internet:
                                
                                let cell = tableView.dequeueReusableCell(withIdentifier: PriceAndRoundingCell.identifier) as! PriceAndRoundingCell
                                cell.setInternetLayoutValues(internetHeader: aHeader.internet)
                                
                                return cell
                            default:
                                break
                            }
                        }
                    } else if aTariffObject.tariffType == .newCin,
                        let aNewCINObject = aTariffObject.valueObject as? Klass {
                        
                        let cell : AttributeListCell = tableView.dequeueReusableCell(withIdentifier: AttributeListCell.identifier) as! AttributeListCell
                        cell.setAttributeListForTariffPostpaidHeader(attributeList: aNewCINObject.header?.attributes?[indexPath.row])
                        
                        return cell
                    }
                }
                
            }
                /*else if selectedType == MBTariffScreenType.Individual  {
                
                if let aHeader = individualData?[tableView.tag].header {
                    
                    
                    switch headerDataType[indexPath.row] {
                        
                    case MBDataTypes.Call:
                        
                        let cell = tableView.dequeueReusableCell(withIdentifier: PriceAndRoundingCell.identifier) as! PriceAndRoundingCell
                        cell.setCallAndSMSLayoutValues(callHeaderType: aHeader.call, cellType:MBDataTypes.Call)
                        
                        return cell
                        
                    case MBDataTypes.SMS:
                        
                        let cell = tableView.dequeueReusableCell(withIdentifier: PriceAndRoundingCell.identifier) as! PriceAndRoundingCell
                        cell.setCallAndSMSLayoutValues(callHeaderType: aHeader.sms, cellType:MBDataTypes.SMS)
                        
                        return cell
                        
                    case MBDataTypes.Internet:
                        
                        let cell = tableView.dequeueReusableCell(withIdentifier: PriceAndRoundingCell.identifier) as! PriceAndRoundingCell
                        cell.setInternetLayoutValues(internetHeader: aHeader.internet)
                        
                        return cell
                        
                    default:
                        break
                    }
                }
                // Klass Postpaid
            }*/
            else {
                let cell : AttributeListCell = tableView.dequeueReusableCell(withIdentifier: AttributeListCell.identifier) as! AttributeListCell
                
                if selectedType == MBTariffScreenType.KlassPostpaid {
                    cell.setAttributeListForTariffPostpaidHeader(attributeList: klassPostpaidData?[tableView.tag].header?.attributes?[indexPath.row])
                
                } else if selectedType == MBTariffScreenType.Individual {
                    
                    cell.setAttributeListForTariffPostpaidHeader(attributeList: individualData?[tableView.tag].header?.attributes?[indexPath.row])
                }
                
                return cell
            }
            
        case MBSectionType.PackagePrice:
            let packagePriceType = typeOfDataInPackagePriceSection(index: tableView.tag)
            
            if selectedType == MBTariffScreenType.CIN ,
                let aTariffObject = cinData?[tableView.tag] ,
                aTariffObject.tariffType == .newCin,
                let aNewCINObject = aTariffObject.valueObject as? Klass,
                let aPackagePrice = aNewCINObject.packagePrice {
                
                switch packagePriceType[indexPath.row] {
                    
                case MBDataTypes.Call:
                    
                    let cell = tableView.dequeueReusableCell(withIdentifier: PriceAndRoundingCell.identifier) as! PriceAndRoundingCell
                    cell.setCallAndSMSLayoutValues(callHeaderType: aPackagePrice.call, cellType:MBDataTypes.Call, setBackgroundColorWhite: true)
                    
                    return cell
                    
                case MBDataTypes.SMS:
                    
                    let cell = tableView.dequeueReusableCell(withIdentifier: PriceAndRoundingCell.identifier) as! PriceAndRoundingCell
                    cell.setCallAndSMSLayoutValues(callHeaderType: aPackagePrice.sms, cellType:MBDataTypes.SMS, setBackgroundColorWhite: true)
                    
                    return cell
                    
                case MBDataTypes.Internet:
                    
                    let cell = tableView.dequeueReusableCell(withIdentifier: PriceAndRoundingCell.identifier) as! PriceAndRoundingCell
                    cell.setInternetLayoutValues(internetHeader: aPackagePrice.internet, setBackgroundColorWhite: true)
                    
                    return cell
                    
                default:
                    break
                }
            } else if let aPackagePrice = klassPostpaidData?[tableView.tag].packagePrice {
                
                switch packagePriceType[indexPath.row] {
                    
                case MBDataTypes.Call:
                    
                    let cell = tableView.dequeueReusableCell(withIdentifier: PriceAndRoundingCell.identifier) as! PriceAndRoundingCell
                    cell.setCallAndSMSLayoutValues(callHeaderType: aPackagePrice.call, cellType:MBDataTypes.Call, setBackgroundColorWhite: true)
                    
                    return cell
                    
                case MBDataTypes.SMS:
                    
                    let cell = tableView.dequeueReusableCell(withIdentifier: PriceAndRoundingCell.identifier) as! PriceAndRoundingCell
                    cell.setCallAndSMSLayoutValues(callHeaderType: aPackagePrice.sms, cellType:MBDataTypes.SMS, setBackgroundColorWhite: true)
                    
                    return cell
                    
                case MBDataTypes.Internet:
                    
                    let cell = tableView.dequeueReusableCell(withIdentifier: PriceAndRoundingCell.identifier) as! PriceAndRoundingCell
                    cell.setInternetLayoutValues(internetHeader: aPackagePrice.internet, setBackgroundColorWhite: true)
                    
                    return cell
                    
                default:
                    break
                }
            }
        case MBSectionType.PaygPrice:
            
            if selectedType == MBTariffScreenType.CIN ,
                let aTariffObject = cinData?[tableView.tag] ,
                aTariffObject.tariffType == .newCin,
                let aNewCINObject = aTariffObject.valueObject as? Klass,
                let aPaygPrice = aNewCINObject.paygPrice {
                
                let cell = tableView.dequeueReusableCell(withIdentifier: PriceAndRoundingCell.identifier) as! PriceAndRoundingCell
                
                let aPaygPriceDataType = typeOfDataInPaygPrice(aPriceObject: aPaygPrice)
                
                switch aPaygPriceDataType[indexPath.row] {
                    
                case MBDataTypes.Call:
                    
                    cell.setCallAndSMSLayoutValues(callHeaderType: aPaygPrice.call, cellType:MBDataTypes.Call, setBackgroundColorWhite: true)
                    
                case MBDataTypes.SMS:
                    
                    cell.setCallAndSMSLayoutValues(callHeaderType: aPaygPrice.sms, cellType:MBDataTypes.SMS, setBackgroundColorWhite: true)
                    
                case MBDataTypes.Internet:
                    
                    cell.setInternetLayoutValues(internetHeader: aPaygPrice.internet, setBackgroundColorWhite: true)
                    
                default:
                    break
                }
                
                return cell
            }
            
        case MBSectionType.Price:
            if let aPriceObject = individualData?[tableView.tag].price {
                let headerDataType = typeOfDataInPrice(aPriceObject: aPriceObject)
                
                switch headerDataType[indexPath.row] {
                    
                case MBDataTypes.Call:
                    
                    let cell = tableView.dequeueReusableCell(withIdentifier: PriceAndRoundingCell.identifier) as! PriceAndRoundingCell
                    cell.setCallAndSMSLayoutValues(callHeaderType: aPriceObject.call, cellType:MBDataTypes.Call, setBackgroundColorWhite: true)
                    
                    return cell
                    
                case MBDataTypes.SMS:
                    
                    let cell = tableView.dequeueReusableCell(withIdentifier: PriceAndRoundingCell.identifier) as! PriceAndRoundingCell
                    cell.setCallAndSMSLayoutValues(callHeaderType: aPriceObject.sms, cellType:MBDataTypes.SMS, setBackgroundColorWhite: true)
                    
                    return cell
                    
                case MBDataTypes.Internet:
                    
                    let cell = tableView.dequeueReusableCell(withIdentifier: PriceAndRoundingCell.identifier) as! PriceAndRoundingCell
                    cell.setInternetLayoutValues(internetHeader: aPriceObject.internet, setBackgroundColorWhite: true)
                    
                    return cell
                    
                default:
                    break
                }
            }
            break
            
        case MBSectionType.Detail, .DetailNewCIN:
            
            var aDetail : DetailsWithPriceArray?
            
            if selectedType == MBTariffScreenType.CIN {
                
                if let aTariffObject = cinData?[tableView.tag] {
                    
                    if aTariffObject.tariffType == .cin,
                        let aCINObject = aTariffObject.valueObject as? Cin {
                        
                        aDetail = aCINObject.details
                        
                    } else if aTariffObject.tariffType == .newCin,
                        let aNewCINObject = aTariffObject.valueObject as? Klass {
                        
                        aDetail = aNewCINObject.details
                    }
                }
            } else if selectedType == MBTariffScreenType.Individual {
                aDetail = individualData?[tableView.tag].details
            } else {
                aDetail = klassPostpaidData?[tableView.tag].details
            }
            
            let detailDataType = typeOfDataInDetailSection(aDetails: aDetail)
            
            switch detailDataType[indexPath.row] {
                
            case MBOfferType.Price:
                
                let cell = tableView.dequeueReusableCell(withIdentifier: PriceAndRoundingCell.identifier) as! PriceAndRoundingCell
                cell.setPriceLayoutValues(price: aDetail?.price?[indexPath.row])
                return cell
                
            case MBOfferType.Rounding:
                
                let cell = tableView.dequeueReusableCell(withIdentifier: PriceAndRoundingCell.identifier) as! PriceAndRoundingCell
                cell.setRoundingLayoutValues(rounding: aDetail?.rounding)
                return cell
                
            case MBOfferType.TextWithTitle:
                
                let cell = tableView.dequeueReusableCell(withIdentifier: TextDateTimeCell.identifier) as! TextDateTimeCell
                cell.setTextWithTitleLayoutValues(textWithTitle: aDetail?.textWithTitle)
                return cell
                
            case MBOfferType.TextWithOutTitle:
                
                let cell = tableView.dequeueReusableCell(withIdentifier: TextDateTimeCell.identifier) as! TextDateTimeCell
                cell.setTextWithOutTitleLayoutValues(textWithOutTitle: aDetail?.textWithOutTitle)
                return cell
                
            case MBOfferType.TextWithPoints:
                
                let cell = tableView.dequeueReusableCell(withIdentifier: TextDateTimeCell.identifier) as! TextDateTimeCell
                cell.setTextWithPointsLayoutValues(textWithPoint: aDetail?.textWithPoints)
                return cell
                
            case MBOfferType.Date:
                
                let cell = tableView.dequeueReusableCell(withIdentifier: TextDateTimeCell.identifier) as! TextDateTimeCell
                cell.setDateAndTimeLayoutValues(dateAndTime: aDetail?.date)
                return cell
                
            case MBOfferType.Time:
                
                let cell = tableView.dequeueReusableCell(withIdentifier: TextDateTimeCell.identifier) as! TextDateTimeCell
                cell.setDateAndTimeLayoutValues(dateAndTime: aDetail?.time)
                return cell
                
            case MBOfferType.RoamingDetails:
                
                let cell = tableView.dequeueReusableCell(withIdentifier: RoamingDetailsCell.identifier) as! RoamingDetailsCell
                cell.setRoamingDetailsLayout(roamingDetails: aDetail?.roamingDetails)
                return cell
                
            case MBOfferType.FreeResourceValidity:
                
                let cell = tableView.dequeueReusableCell(withIdentifier: FreeResourceValidityCell.identifier) as! FreeResourceValidityCell
                cell.setFreeResourceValidityValues(freeResourceValidity: aDetail?.freeResourceValidity)
                return cell
                
            case MBOfferType.TitleSubTitleValueAndDesc:
                
                let cell = tableView.dequeueReusableCell(withIdentifier: TitleSubTitleValueAndDescCell.identifier) as! TitleSubTitleValueAndDescCell
                cell.setTitleSubTitleValueAndDescLayoutValues(titleSubTitleValueAndDesc: aDetail?.titleSubTitleValueAndDesc)
                return cell
                
            default:
                break
            }
            
        // Detail Section is for CIN and Individual user
        case MBSectionType.Description:
            
            let descriptionDataType = typeOfDataInDescriptionSection(index: tableView.tag)
            
            switch descriptionDataType[indexPath.row] {
                
            case MBDataTypes.Advantages:
                let cell = tableView.dequeueReusableCell(withIdentifier: TitleDescriptionCell.identifier) as! TitleDescriptionCell
                
                if selectedType == MBTariffScreenType.CIN ,
                    let aTariffObject = cinData?[tableView.tag] ,
                    aTariffObject.tariffType == .cin,
                    let aCINObject = aTariffObject.valueObject as? Cin {
                    
                    cell.setTitleAndDescriptionWithOutIcon(title: aCINObject.description?.advantages?.title,
                                                           description: aCINObject.description?.advantages?.description)
                    return cell
                    
                } else if selectedType == MBTariffScreenType.Individual {
                    
                    cell.setTitleAndDescriptionWithOutIcon(title: individualData?[tableView.tag].description?.advantages?.title,
                                                           description: individualData?[tableView.tag].description?.advantages?.description)
                    return cell
                }
                
            case MBDataTypes.Classification:
                let cell = tableView.dequeueReusableCell(withIdentifier: TitleDescriptionCell.identifier) as! TitleDescriptionCell
                
                if selectedType == MBTariffScreenType.CIN ,
                    let aTariffObject = cinData?[tableView.tag] ,
                    aTariffObject.tariffType == .cin,
                    let aCINObject = aTariffObject.valueObject as? Cin {
                    
                    cell.setTitleAndDescriptionWithOutIcon(title: aCINObject.description?.classification?.title,
                                                           description: aCINObject.description?.classification?.description)
                    return cell
                    
                } else if selectedType == MBTariffScreenType.Individual {
                    cell.setTitleAndDescriptionWithOutIcon(title: individualData?[tableView.tag].description?.classification?.title,
                                                           description: individualData?[tableView.tag].description?.classification?.description)
                    return cell
                }
                
            default:
                break
            }
            
        default:
            break
        }
        
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    
    //MARK: - Helping functions
    func typeOfDataInSection(index : Int) -> [MBSectionType] {
        
        // Atleast one because there is one with offer name
        
        var dataTypes : [MBSectionType] = []
        
        if selectedType == MBTariffScreenType.CIN {
            if let aTariffObject = cinData?[index] {
                
                if aTariffObject.tariffType == .cin,
                    let aCINObject = aTariffObject.valueObject as? Cin {
                    
                    // 1
                    if aCINObject.header != nil {
                        
                        dataTypes.append(MBSectionType.Header)
                    }
                    // 2
                    if aCINObject.details != nil  {
                        
                        dataTypes.append(MBSectionType.Detail)
                    }
                    // 3
                    if aCINObject.description != nil {
                        
                        dataTypes.append(MBSectionType.Description)
                    }
                    
                    dataTypes.append(MBSectionType.Subscription)
                    
                    
                } else if aTariffObject.tariffType == .newCin,
                    let aNewCINObject = aTariffObject.valueObject as? Klass {
                    
                    // 1
                    if aNewCINObject.header != nil {
                        
                        dataTypes.append(MBSectionType.Header)
                    }
                    // 2
                    if aNewCINObject.packagePrice != nil  {
                        
                        dataTypes.append(MBSectionType.PackagePrice)
                    }
                    // 3
                    if aNewCINObject.paygPrice != nil {
                        
                        dataTypes.append(MBSectionType.PaygPrice)
                    }
                    // 4
                    if aNewCINObject.details != nil {
                        
                        dataTypes.append(MBSectionType.DetailNewCIN)
                    }
                    
                    dataTypes.append(MBSectionType.Subscription)
                }
            }
            
        } else if selectedType == MBTariffScreenType.Individual {
            
            if let aOffer =  individualData?[index] {
                // 1
                if aOffer.header != nil {
                    
                    dataTypes.append(MBSectionType.Header)
                }
                
                // 2
                if aOffer.price != nil {
                    
                    dataTypes.append(MBSectionType.Price)
                }
                //3
                if aOffer.details != nil  {
                    
                    dataTypes.append(MBSectionType.Detail)
                }
                // 4
                if aOffer.description != nil {
                    
                    dataTypes.append(MBSectionType.Description)
                }
                
                dataTypes.append(MBSectionType.Subscription)
            }
        } else if selectedType == MBTariffScreenType.KlassPostpaid {
            
            if let aOffer =  klassPostpaidData?[index] {
                // 1
                if aOffer.header != nil {
                    
                    dataTypes.append(MBSectionType.Header)
                }
                
                // 2
                if aOffer.packagePrice != nil {
                    
                    dataTypes.append(MBSectionType.PackagePrice)
                }
                
                
                // 3
                if aOffer.details != nil  {
                    
                    dataTypes.append(MBSectionType.Detail)
                }
                
                dataTypes.append(MBSectionType.Subscription)
            }
        }
        
        return dataTypes
    }
    
    func typeOfDataInHeaderSection(index : Int) -> [MBDataTypes] {
        
        // Atleast one because there is one with offer name
        
        var dataTypes : [MBDataTypes] = []
        
        if selectedType == MBTariffScreenType.CIN ,
            let aTariffObject = cinData?[index] ,
            aTariffObject.tariffType == .cin,
            let aCINObject = aTariffObject.valueObject as? Cin,
            let aHeader =  aCINObject.header {
            
            dataTypes.append(MBDataTypes.TopDescription)
            
            // 2
            if aHeader.call != nil {
                
                dataTypes.append(MBDataTypes.Call)
            }
            // 3
            if aHeader.sms != nil  {
                
                dataTypes.append(MBDataTypes.SMS)
            }
            // 4
            if aHeader.internet != nil {
                
                dataTypes.append(MBDataTypes.Internet)
            }
        } else if selectedType == MBTariffScreenType.Individual {
            
            if let _ =  individualData?[index].header {
                // 1
//                if aHeader.call != nil {
//
//                    dataTypes.append(MBDataTypes.Call)
//                }
//                // 2
//                if aHeader.sms != nil  {
//
//                    dataTypes.append(MBDataTypes.SMS)
//                }
//                // 3
//                if aHeader.internet != nil {
//
//                    dataTypes.append(MBDataTypes.Internet)
//                }
            }
        }
        
        return dataTypes
    }
    
    func typeOfDataInPackagePriceSection(index : Int) -> [MBDataTypes] {
        
        // Atleast one because there is one with offer name
        
        var dataTypes : [MBDataTypes] = []
        
        if selectedType == MBTariffScreenType.CIN ,
            let aTariffObject = cinData?[index] ,
            aTariffObject.tariffType == .newCin,
            let aNewCINObject = aTariffObject.valueObject as? Klass,
            let aPackagePrice = aNewCINObject.packagePrice {
            
            // 1
            if aPackagePrice.call != nil {
                
                dataTypes.append(MBDataTypes.Call)
            }
            // 2
            if aPackagePrice.sms != nil  {
                
                dataTypes.append(MBDataTypes.SMS)
            }
            // 3
            if aPackagePrice.internet != nil {
                
                dataTypes.append(MBDataTypes.Internet)
            }
        } else if let aPackagePrice =  klassPostpaidData?[index].packagePrice {
            // 1
            if aPackagePrice.call != nil {
                
                dataTypes.append(MBDataTypes.Call)
            }
            // 2
            if aPackagePrice.sms != nil  {
                
                dataTypes.append(MBDataTypes.SMS)
            }
            // 3
            if aPackagePrice.internet != nil {
                
                dataTypes.append(MBDataTypes.Internet)
            }
        }
        
        return dataTypes
    }
    
    func typeOfDataInPaygPrice(aPriceObject: PaygPrice?) -> [MBDataTypes]  {
        
        var dataTypes : [MBDataTypes] = []
        if let aPrice =  aPriceObject {
            
            // 1
            if aPrice.call != nil {
                
                dataTypes.append(MBDataTypes.Call)
            }
            // 2
            if aPrice.sms != nil  {
                
                dataTypes.append(MBDataTypes.SMS)
            }
            // 3
            if aPrice.internet != nil {
                
                dataTypes.append(MBDataTypes.Internet)
            }
        }
        return dataTypes
    }
    
    func typeOfDataInDetailSection(aDetails : DetailsWithPriceArray?) -> [MBOfferType] {
        
        var dataTypes : [MBOfferType] = []
        
        if let aDetail =  aDetails {
            // 1
            //  if aDetail.price != nil {
            //
            //  dataTypes.append(MBOfferType.Price)
            //  }
            if let count = aDetail.price?.count {
                
                for _ in 0..<count {
                    dataTypes.append(MBOfferType.Price)
                }
            }
            // 2
            if aDetail.rounding != nil  {
                
                dataTypes.append(MBOfferType.Rounding)
            }
            // 3
            if aDetail.textWithTitle != nil {
                
                dataTypes.append(MBOfferType.TextWithTitle)
            }
            // 4
            if aDetail.textWithOutTitle != nil {
                
                dataTypes.append(MBOfferType.TextWithOutTitle)
            }
            // 5
            if aDetail.textWithPoints != nil {
                
                dataTypes.append(MBOfferType.TextWithPoints)
            }
            // 6
            if aDetail.titleSubTitleValueAndDesc != nil {
                
                dataTypes.append(MBOfferType.TitleSubTitleValueAndDesc)
            }
            
            // 7
            if aDetail.date != nil {
                
                dataTypes.append(MBOfferType.Date)
            }
            // 8
            if aDetail.time != nil {
                
                dataTypes.append(MBOfferType.Time)
            }
            // 9
            if aDetail.roamingDetails != nil {
                
                dataTypes.append(MBOfferType.RoamingDetails)
            }
            // 10
            if aDetail.freeResourceValidity != nil {
                
                dataTypes.append(MBOfferType.FreeResourceValidity)
            }
        }
        return dataTypes
    }
    
    func typeOfDataInDescriptionSection(index : Int) -> [MBDataTypes] {
        
        var dataTypes : [MBDataTypes] = []
        
        var aDescription : TariffDescription?
        if selectedType == MBTariffScreenType.CIN,
            let aTariffObject = cinData?[index] ,
            aTariffObject.tariffType == .cin,
            let aCINObject = aTariffObject.valueObject as? Cin {
            
            aDescription =  aCINObject.description
            
        } else if selectedType == MBTariffScreenType.Individual {
            aDescription =  individualData?[index].description
        }
        
        if let aDescription =  aDescription {
            
            // 1
            if aDescription.advantages != nil  {
                
                dataTypes.append(MBDataTypes.Advantages)
            }
            
            // 2
            if aDescription.classification != nil {
                
                dataTypes.append(MBDataTypes.Classification)
            }
            //
            //  // 3
            //  if aDescription.destination != nil {
            //
            //  dataTypes.append(MBDataTypes.Destination)
            //  }
        }
        
        return dataTypes
    }
    
    func typeOfDataInPrice(aPriceObject: CinPostpaidPrice?) -> [MBDataTypes]  {
        
        var dataTypes : [MBDataTypes] = []
        if let aPrice =  aPriceObject {
            
            // 1
            if aPrice.call != nil {
                
                dataTypes.append(MBDataTypes.Call)
            }
            // 2
            if aPrice.sms != nil  {
                
                dataTypes.append(MBDataTypes.SMS)
            }
            // 3
            if aPrice.internet != nil {
                
                dataTypes.append(MBDataTypes.Internet)
            }
        }
        return dataTypes
    }
    
    // Will return number of row in section header accourding to
    func numberOfRowsInHeaderSection(index : Int) -> Int {
        
        if selectedType == MBTariffScreenType.CIN {
            if let aTariffObject = cinData?[index] {
                
                if aTariffObject.tariffType == .cin,
                    let aCINObject = aTariffObject.valueObject as? Cin {
                    
                    return numberOfRowsInCINHeaderSection(aCINOffer: aCINObject)
                    
                } else if aTariffObject.tariffType == .newCin,
                    let aNewCINObject = aTariffObject.valueObject as? Klass {
                    
                    return aNewCINObject.header?.attributes?.count ?? 0
                }
            }
            
        } else  if selectedType == MBTariffScreenType.Individual {
            //return numberOfRowsInIndividualHeaderSection(aIndividualOffer: individualData?[index])
            return individualData?[index].header?.attributes?.count ?? 0
            
        } else  if selectedType == MBTariffScreenType.KlassPostpaid {
            return numberOfRowsInKlassPostpaidHeaderSection(aKlassPostpaidOffer: klassPostpaidData?[index])
            
        }
        
        return 0
    }
    
    func numberOfRowsInCINHeaderSection(aCINOffer : Cin?) -> Int {
        
        // Atleast one because there is one with offer name
        var numberOfRows : Int = 1
        
        if let aHeader =  aCINOffer?.header {
            
            if aHeader.call != nil {
                numberOfRows += 1
            }
            
            if aHeader.sms != nil {
                numberOfRows += 1
            }
            
            if aHeader.internet != nil {
                numberOfRows += 1
            }
        }
        return numberOfRows
    }
    
    /*func numberOfRowsInIndividualHeaderSection(aIndividualOffer : Individual?) -> Int {
        
        // Atleast one because there is one with offer name
        var numberOfRows : Int = 0
        
        if let aHeader =  aIndividualOffer?.header {
            
            if aHeader.call != nil {
                numberOfRows += 1
            }
            
            if aHeader.sms != nil {
                numberOfRows += 1
            }
            
            if aHeader.internet != nil {
                numberOfRows += 1
            }
        }
        return numberOfRows
    }*/
    
    func numberOfRowsInKlassPostpaidPackagePriceSection(aKlassPostpaidOffer : PackagePriceKlassPostpaid?) -> Int {
        
        // Atleast one because there is one with offer name
        var numberOfRows : Int = 0
        
        if let aPackagePrice =  aKlassPostpaidOffer {
            
            if aPackagePrice.call != nil {
                numberOfRows += 1
            }
            
            if aPackagePrice.sms != nil {
                numberOfRows += 1
            }
            
            if aPackagePrice.internet != nil {
                numberOfRows += 1
            }
        }
        return numberOfRows
    }
    
    func numberOfRowsInKlassPostpaidHeaderSection(aKlassPostpaidOffer : KlassPostpaid?) -> Int {
        
        // Atleast one because there is one with offer name
        var numberOfRows : Int = 0
        
        if let aHeader =  aKlassPostpaidOffer?.header {
            
            if aHeader.attributes != nil {
                numberOfRows = aHeader.attributes?.count ?? 0
            }
        }
        return numberOfRows
    }
    
    func numberOfRowsInDetailSection(index : Int) -> Int {
        
        if selectedType == MBTariffScreenType.CIN {
            
            if let aTariffObject = cinData?[index] {
                
                if aTariffObject.tariffType == .cin,
                    let aCINObject = aTariffObject.valueObject as? Cin {
                    
                    return numberOfRowsInDetailSection(aDetails: aCINObject.details)
                    
                } else if aTariffObject.tariffType == .newCin,
                    let aNewCINObject = aTariffObject.valueObject as? Klass {
                    
                    return numberOfRowsInDetailSection(aDetails: aNewCINObject.details)
                }
            }
            
        } else  if selectedType == MBTariffScreenType.Individual {
            return numberOfRowsInDetailSection(aDetails:  individualData?[index].details)
            
        } else  if selectedType == MBTariffScreenType.KlassPostpaid {
            return numberOfRowsInDetailSection(aDetails:  klassPostpaidData?[index].details)
            
        }
        return 0
    }
    
    func numberOfRowsInDetailSection(aDetails : DetailsWithPriceArray?) -> Int {
        
        var numberOfRows : Int = 0
        
        if let aDetail =  aDetails {
            
            // 1
            //            if aDetail.price != nil {
            //                numberOfRows = numberOfRows + 1
            //            }
            if let count = aDetail.price?.count {
                numberOfRows = numberOfRows + count
            }
            // 2
            if aDetail.rounding != nil {
                numberOfRows = numberOfRows + 1
            }
            // 3
            if aDetail.textWithTitle != nil {
                numberOfRows = numberOfRows + 1
            }
            // 4
            if aDetail.textWithOutTitle != nil {
                numberOfRows = numberOfRows + 1
            }
            // 5
            if aDetail.textWithPoints != nil {
                numberOfRows = numberOfRows + 1
            }
            // 6
            if aDetail.date != nil {
                numberOfRows = numberOfRows + 1
            }
            // 7
            if aDetail.time != nil {
                numberOfRows = numberOfRows + 1
            }
            // 8
            if aDetail.roamingDetails != nil {
                numberOfRows = numberOfRows + 1
            }
            // 9
            if aDetail.freeResourceValidity != nil {
                numberOfRows = numberOfRows + 1
            }
            // 10
            if aDetail.titleSubTitleValueAndDesc != nil {
                numberOfRows = numberOfRows + 1
            }
            
        }
        return numberOfRows
    }
    
    func numberOfRowsInDescriptionSection(index : Int) -> Int {
        
        var aDescriptionData : TariffDescription?
        
        if selectedType == MBTariffScreenType.CIN,
            let aTariffObject = cinData?[index],
            aTariffObject.tariffType == .cin,
            let aCINObject = aTariffObject.valueObject as? Cin {
            
            aDescriptionData = aCINObject.description
            
        } else  if selectedType == MBTariffScreenType.Individual {
            
            aDescriptionData = individualData?[index].description
        }
        
        var numberOfRows : Int = 0
        
        if let aDescription = aDescriptionData {
            
            if aDescription.advantages != nil {
                numberOfRows += 1
            }
            
            if aDescription.classification != nil {
                numberOfRows += 1
            }
        }
        return numberOfRows
        
    }
    
    func numberOfRowsInpackagePrice(aPackagePriceObject: PackagePriceKlassPostpaid?) -> Int {
        
        // Atleast one because there is one with offer name
        var numberOfRows : Int = 0
        
        if let aPackagePrice =  aPackagePriceObject {
            
            if aPackagePrice.call != nil {
                numberOfRows += 1
            }
            
            if aPackagePrice.sms != nil {
                numberOfRows += 1
            }
            
            if aPackagePrice.internet != nil {
                numberOfRows += 1
            }
        }
        return numberOfRows
    }
    
    func numberOfRowsInPaygPrice(aPriceObject: PaygPrice?) -> Int {
        
        var numberOfRows : Int = 0
        
        if let aPrice =  aPriceObject {
            
            if aPrice.call != nil {
                numberOfRows += 1
            }
            
            if aPrice.sms != nil {
                numberOfRows += 1
            }
            
            if aPrice.internet != nil {
                numberOfRows += 1
            }
        }
        return numberOfRows
    }
    
    func shouldOpenSection(index: Int) -> MBSectionType {
        
        if selectedType == .CIN {
            
            let type = typeOfDataInSection(index: index)
            
            var isNewCINTariff : Bool = false
            
            if let aTariffObject = cinData?[index] ,
                aTariffObject.tariffType == .newCin{
                
                isNewCINTariff = true
            }
            
            if selectedSectionViewType == MBSectionType.Header && type.contains(MBSectionType.Header) {
                
                return MBSectionType.Header
                
            } else if selectedSectionViewType == MBSectionType.Detail {
                
                if isNewCINTariff {
                    if type.contains(MBSectionType.PackagePrice) {
                        
                        return MBSectionType.PackagePrice
                        
                    } else if type.contains(MBSectionType.PaygPrice) {
                        
                        return MBSectionType.PaygPrice
                    }
                    
                } else if type.contains(MBSectionType.Detail) {
                    
                    return MBSectionType.Detail
                }
                
            } else if selectedSectionViewType == MBSectionType.DetailNewCIN {
                
                if isNewCINTariff == false,
                    type.contains(MBSectionType.Description) {
                    
                    return MBSectionType.Description
                    
                } else if type.contains(MBSectionType.DetailNewCIN) {
                    
                    return MBSectionType.DetailNewCIN
                }
                
            } else if selectedSectionViewType == MBSectionType.Description {
                
                if isNewCINTariff ,
                    type.contains(MBSectionType.DetailNewCIN) {
                    
                    return MBSectionType.DetailNewCIN
                    
                } else if type.contains(MBSectionType.Description) {
                    
                    return MBSectionType.Description
                }
                
            } else if selectedSectionViewType == MBSectionType.PackagePrice {
                
                if isNewCINTariff == false,
                    type.contains(MBSectionType.Detail) {
                    
                    return MBSectionType.Detail
                    
                } else if type.contains(MBSectionType.PackagePrice) {
                    return MBSectionType.PackagePrice
                }
                
            } else if selectedSectionViewType == MBSectionType.PaygPrice {
                
                if isNewCINTariff == false,
                    type.contains(MBSectionType.Detail) {
                    return MBSectionType.Detail
                    
                } else if type.contains(MBSectionType.PaygPrice) {
                    return MBSectionType.PaygPrice
                }
            }
            
            return MBSectionType.Header
            
        } else {
            return selectedSectionViewType
        }
    }
}

// MARK: - <MBAccordionTableViewDelegate> -

extension TariffsVC : MBAccordionTableViewDelegate {
    
    func tableView(_ tableView: MBAccordionTableView, willOpenSection section: Int, withHeader header: UITableViewHeaderFooterView?) {
        
        if let headerView = header as? SupplementaryOffersHeaderView {
            headerView.detailSign_btn .setImage(#imageLiteral(resourceName: "minu-sign"), for: UIControl.State.normal)
        }
    }
    
    func tableView(_ tableView: MBAccordionTableView, didOpenSection section: Int, withHeader header: UITableViewHeaderFooterView?) {
        
        if let headerView = header as? SupplementaryOffersHeaderView {
            
            if selectedSectionViewType != headerView.viewType {
                
                selectedSectionViewType = headerView.viewType
            }
        } else if (header as? IndividualTitleView) != nil {
            
            if selectedSectionViewType != .Header {
                
                selectedSectionViewType = .Header
            }
        }
    }
    
    func tableView(_ tableView: MBAccordionTableView, willCloseSection section: Int, withHeader header: UITableViewHeaderFooterView?) {
        
        if let headerView = header as? SupplementaryOffersHeaderView {
            headerView.detailSign_btn.setImage(#imageLiteral(resourceName: "Plus-Sign"), for: UIControl.State.normal)
        }
    }
    func tableView(_ tableView: MBAccordionTableView, didCloseSection section: Int, withHeader header: UITableViewHeaderFooterView?) {
        
        
        if let headerView = header as? SupplementaryOffersHeaderView {
            
            if selectedSectionViewType == headerView.viewType {
                selectedSectionViewType = MBSectionType.Header
                tableView.toggleSection(0)
            
            } else if tableView.isAnySectionOpen() == false {
                
                selectedSectionViewType = MBSectionType.Header
                tableView.toggleSection(0)
            }
        } else if (header as? IndividualTitleView) != nil {
            
            if selectedSectionViewType == .Header {
                selectedSectionViewType = MBSectionType.Header
                tableView.toggleSection(0)
            }
        }
    }
    
    func tableView(_ tableView: MBAccordionTableView, canInteractWithHeaderAtSection section: Int) -> Bool {
        
        if let headerView = tableView.headerView(forSection: section) as? SubscribeButtonView {
            
            if headerView.viewType == MBSectionType.Subscription {
                
                return false
                
            } else {
                
                return true
            }
        } else {
            
            return true
        }
    }
}

extension TariffsVC : UIScrollViewDelegate {
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        
        if scrollView == collectionView {
            openSelectedSection()
        }
    }
}


