//
//  RoamingVC.swift
//  Bakcell
//
//  Created by Saad Riaz on 6/17/17.
//  Copyright © 2017 evampsaanga. All rights reserved.
//

import UIKit


class RoamingVC: BaseVC {

    var offers : [Offers]?
    var filters : Filters?
    var countries : [String]?
    var isCardView = true
    var showNavBar = false

    @IBOutlet var viewBack: UIView!
    @IBOutlet var title_lbl: UILabel!
    @IBOutlet var description_lbl: UILabel!
    @IBOutlet var countries_txt: SearchTextField!
    @IBOutlet var next_btn: UIButton!
    
    @IBOutlet var descriptionLblTopConst: NSLayoutConstraint!
    
    //MARK: - ViewController LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        if let countries = countries {
            countries_txt.filterStrings(countries)
        }
        countries_txt.delegate = self
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.interactivePop(true)
        
        next_btn.titleLabel?.font = UIFont.MBArial(fontSize: 16)
        description_lbl.font = UIFont.MBArial(fontSize: 14)
        countries_txt.font = UIFont.MBArial(fontSize: 14)
        
        description_lbl.text = Localized("Description_Roaming")
        countries_txt.placeholder = Localized("PlaceHolder_EnterYourCountry")
        next_btn.setTitle(Localized("BtnTitle_NEXT"), for: UIControl.State.normal)

        countries_txt.textColor = UIColor.MBTextGrayColor
        countries_txt.theme = SearchTextFieldTheme.MBLightTheme()

        if showNavBar == true {
            viewBack.isHidden = !showNavBar
            descriptionLblTopConst.constant = 65.0
            self.view.layoutIfNeeded()
            self.view.layoutSubviews()
            title_lbl.text = Localized("Title_RoamingPackages")
        }
        
    }

    //MARK: - IBAction
    @IBAction func nextButtonAction(_ sender: Any) {

        if let countryName = countries_txt.text {

            if countryName.isEmpty {

                self.showErrorAlertWithMessage(message: Localized("Message_PleaseEnterCountry"))
            } else {

                let (filterOffers, flagName) = filterOfferByCountryName(countryName: countryName, offerArray: offers)

                if filterOffers.count <= 0 {

                    self.showAlertWithMessage(message: Localized("Message_NoOfferWereFound"))

                } else {
                    let supp = self.myStoryBoard.instantiateViewController(withIdentifier: "SupplementaryVC") as! SupplementaryVC
                    supp.offers = filterOffers
                    supp.filters = filters
                    supp.prepareVCForRoaming = true
                    supp.showCardView = self.isCardView
                    supp.selectedTabType = MBOfferTabType.Roaming
                    supp.countryTitle = countryName
                    supp.countryImage = flagName

                    self.navigationController?.pushViewController(supp, animated: true)
                }
            }
        }
        
        _ = countries_txt.resignFirstResponder()
    }

    //MARK: - Filtering offers data accourding to selectes country.
    func filterOfferByCountryName(countryName : String , offerArray : [Offers]? ) -> ([Offers], String) {
        // Filtering offers array
        var filteredOffers : [Offers] = []
        var imageName : String = ""

        if let offers = offerArray {

            filteredOffers = offers.filter() {

                if let roamingDetailsCountriesList = ($0 as Offers).details?.roamingDetails?.roamingDetailsCountriesList {

                    let  aRoamingDetailsCountry = roamingDetailsCountriesList.filter() {

                        if let aCountryName = ($0 as RoamingDetailsCountries).countryName {


                            if aCountryName.trimmWhiteSpace.lowercased() == countryName.trimmWhiteSpace.lowercased() {

                                if imageName.isBlank == true {
                                    imageName = ($0 as RoamingDetailsCountries).flag ?? ""
                                }
                                return true
                            } else {
                                return false
                            }

                        } else {

                            return false
                        }
                    }

                    if  aRoamingDetailsCountry.count > 0 {

                        return true
                        
                    } else {
                        
                        return false
                    }
                    
                }  else {
                    return false
                }
            }
        }

        return (filteredOffers, imageName)
    }

}

//MARK: Textfield delagates
extension RoamingVC : UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == countries_txt {
            nextButtonAction(UIButton())
            return false
        } else {
            return true
        }
    }
}
