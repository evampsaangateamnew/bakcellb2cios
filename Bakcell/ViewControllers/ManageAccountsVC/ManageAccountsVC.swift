//
//  ManageAccountsVC.swift
//  Bakcell
//
//  Created by AbdulRehman Warraich on 7/22/19.
//  Copyright © 2019 evampsaanga. All rights reserved.
//

import UIKit
import KYDrawerController

class ManageAccountsVC: BaseVC {
    
    //MARK:- Properties
    var canNavigateBack : Bool          = true
    var descriptionMessage : String?    = Localized("Message_manageAccountDescription")
    
    //MARK: - IBOutlet
    @IBOutlet var titleLabel                : UILabel!
    @IBOutlet var descriptionLabel          : UILabel!
    @IBOutlet var addNewAccountButton       : MBButton!
    @IBOutlet var manageAccountsTableView   : UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        manageAccountsTableView.hideDefaultSeprator()
        backButton?.isHidden = canNavigateBack ? false : true
        manageAccountsTableView.register(UINib(nibName: "EditManageAccountCell", bundle: .main), forCellReuseIdentifier: "EditManageAccountCell")
        manageAccountsTableView.delegate = self
        manageAccountsTableView.dataSource = self
        manageAccountsTableView.estimatedRowHeight = UITableView.automaticDimension
        manageAccountsTableView.rowHeight = 58
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.interactivePop(canNavigateBack)
        
        loadViewContent()
    }
    
    //MARK: - IBAction
    @IBAction func AddNewAccountPressed(_ sender: UIButton) {
        
        if (MBUserSession.shared.loggedInUsers?.users?.count ?? 0) < Constants.MaxNumberOfManageAccount {
            if let loginVC :LoginVC = LoginVC.instantiateViewControllerFromStoryboard() {
                loginVC.registrationType = .addAccount
                self.navigationController?.pushViewController(loginVC, animated: true)
            }
        } else {
            self.showErrorAlertWithMessage(message: Localized("Message_MaxNumberOfAccount"))
        }
        
    }
    
    @IBAction func removeAccountPressed(_ sender: UIButton) {
        
        if let selectedUserMSISDN = MBUserSession.shared.loggedInUsers?.users?[sender.tag].userInfo?.msisdn {
            
            if selectedUserMSISDN.isEqualToStringIgnoreCase(otherString: MBUserSession.shared.msisdn) == false  {
                
                if (MBUserSession.shared.loggedInUsers?.users?.count ?? 0) <= 1 {
                    
                    let confirmationAlert = self.myStoryBoard.instantiateViewController(withIdentifier: "ActivationConfirmationVC") as! ActivationConfirmationVC
                    
                    confirmationAlert.setConfirmationAlertLayout(message: Localized("Message_RemoveLastAccount"))
                    
                    
                    confirmationAlert.setYesButtonCompletionHandler { (aString) in
                        
                        BaseVC.logout()
                    }
                    
                    self.presentPOPUP(confirmationAlert, animated: true, completion: nil)
                    
                }  else {
                    MBUserInfoUtilities.removeCustomerDataOf(msisdn: selectedUserMSISDN)
                    manageAccountsTableView.reloadUserData(MBUserSession.shared.loggedInUsers?.users)
                }
            } else {
                self.showErrorAlertWithMessage(message: Localized("Message_ErrorCantRemove"))
            }
        }
    }
    
    //MARK: - Functions
    /**
     Set localized text in viewController
     */
    func loadViewContent() {
        
        titleLabel.text = Localized("Title_ManageAccount")
        descriptionLabel.text = descriptionMessage
        addNewAccountButton.setTitle(Localized("BtnTitle_AddNewAccount"), for: .normal)
    }
    
}

// MARK:- UITableViewDelegate and UITableViewDataSource
extension ManageAccountsVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return MBUserSession.shared.loggedInUsers?.users?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let cell :EditManageAccountCell = tableView.dequeueReusableCell(withIdentifier: "EditManageAccountCell") as? EditManageAccountCell,
            let selectedUser = MBUserSession.shared.loggedInUsers?.users?[indexPath.row] {
            
            var nameString : String = ""
            
            if let firstName = selectedUser.userInfo?.firstName {
                nameString = firstName
            }
            
            if let lastName = selectedUser.userInfo?.lastName {
                nameString = nameString + " " + lastName
            }
            nameString = nameString.replacingOccurrences(of: "  ", with: " ")
            
            cell.setLayout(name: nameString,
                           msisdn: selectedUser.userInfo?.msisdn,
                           isSelected: selectedUser.userInfo?.msisdn?.isEqualToStringIgnoreCase(otherString: MBUserSession.shared.msisdn) ?? false)
            
            cell.sepratorView.isHidden = (indexPath.row < ((MBUserSession.shared.loggedInUsers?.users?.count ?? 0) - 1)) ? false : true
            
            cell.deletButton.tag = indexPath.row
            cell.deletButton.addTarget(self, action: #selector(removeAccountPressed(_:)), for: .touchDown)
            
            return cell
            
        } else {
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if let aUser = MBUserSession.shared.loggedInUsers?.users?[indexPath.row] {
            
            if (aUser.userInfo?.msisdn?.isEqualToStringIgnoreCase(otherString: MBUserSession.shared.msisdn) ?? false) == true {
                self.showErrorAlertWithMessage(message: Localized("Message_AlreadySelected"))
                
            } else {
                /* Clear user session objects befor switch */
                MBUserSession.shared.clearCurrentUserSessionObjects()
                
                /* Switch user to new user */
                MBUserInfoUtilities.switchToUser(newMSISDN: aUser.userInfo?.msisdn ?? "", selectedUserInfo: aUser)
                
                /* Cancel all API requests */
                MBAPIClient.sharedClient.cancelAllRequests()
                
                /* Redirect user to dashboard screen */
                redirectUserToDashboard()
            }
            // Deselect user selected row
            tableView.deselectRow(at: indexPath, animated: true)
        }
        
    }
    
    func redirectUserToDashboard() {
        
        if let navigationStackCount = self.navigationController?.viewControllers.count,
            navigationStackCount > 1,
            let firstVC = self.navigationController?.viewControllers.first,
            let mainViewController :TabbarController = TabbarController.instantiateViewControllerFromStoryboard() ,
            let drawerViewController :SideDrawerVC = SideDrawerVC.instantiateViewControllerFromStoryboard() {
            
            mainViewController.isLoggedInNow = false
            
            let drawerController = KYDrawerController(drawerDirection: .left, drawerWidth: (UIScreen.main.bounds.width * 0.85))
            drawerController.mainViewController = mainViewController
            drawerController.drawerViewController = drawerViewController
            
            self.navigationController?.viewControllers = [firstVC, drawerController]
        }
    }
}

