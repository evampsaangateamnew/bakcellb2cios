//
//  TariffCell.swift
//  Bakcell
//
//  Created by Saad Riaz on 5/30/17.
//  Copyright © 2017 evampsaanga. All rights reserved.
//

import UIKit


class TariffCell: UICollectionViewCell {

    static let identifier = "CarouselCollectionViewCell"

    @IBOutlet var tableView: MBAccordionTableView!
    
    //Stars outlets....
    @IBOutlet weak var btnsStackView: UIStackView!
    @IBOutlet weak var btn1: UIButton!
    @IBOutlet weak var btn2: UIButton!
    @IBOutlet weak var btn3: UIButton!
    @IBOutlet weak var btn4: UIButton!
    @IBOutlet weak var btn5: UIButton!
    
    
    var delegate : SupplementaryOffersCellDelegate? = nil
    var cinData : NewCinTarrif?
    var individualData : Individual?
    var klassPostpaidData : KlassPostpaid?
    var selectedType : MBTariffScreenType!

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    override func awakeFromNib() {
        super.awakeFromNib()

        // Reuseable cells
         tableView.register(UINib(nibName: "PriceAndRoundingCell", bundle: nil), forCellReuseIdentifier: PriceAndRoundingCell.identifier)
         tableView.register(UINib(nibName: "TextDateTimeCell", bundle: nil), forCellReuseIdentifier: TextDateTimeCell.identifier)
        tableView.register(UINib(nibName: "TitleSubTitleValueAndDescCell", bundle: nil), forCellReuseIdentifier: TitleSubTitleValueAndDescCell.identifier)
        tableView.register(UINib(nibName: "TitleDescriptionCell", bundle: nil), forCellReuseIdentifier: TitleDescriptionCell.identifier)
        tableView.register(UINib(nibName: "SupplementaryExpandCell", bundle: nil), forCellReuseIdentifier: "SupplementaryExpandCell")
        tableView.register(UINib(nibName: "PriceAndRoundingCell", bundle: nil), forCellReuseIdentifier: PriceAndRoundingCell.identifier)
        tableView.register(UINib(nibName: "TextDateTimeCell", bundle: nil), forCellReuseIdentifier: TextDateTimeCell.identifier)
        tableView.register(UINib(nibName: "TitleSubTitleValueAndDescCell", bundle: nil), forCellReuseIdentifier: TitleSubTitleValueAndDescCell.identifier)
        tableView.register(UINib(nibName: "FreeResourceValidityCell", bundle: nil), forCellReuseIdentifier: FreeResourceValidityCell.identifier)
        tableView.register(UINib(nibName: "RoamingDetailsCell", bundle: nil), forCellReuseIdentifier: RoamingDetailsCell.identifier)
        tableView.register(UINib(nibName: "AttributeListCell", bundle: nil), forCellReuseIdentifier: AttributeListCell.identifier)
        tableView.register(UINib(nibName: "BounsDescriptionCell", bundle: nil), forCellReuseIdentifier: BounsDescriptionCell.identifier)



        // ForHeaderFooterView
        tableView.register(UINib(nibName: "SupplementaryOffersHeaderView", bundle: nil), forHeaderFooterViewReuseIdentifier: SupplementaryOffersHeaderView.identifier)
        tableView.register(UINib(nibName: "SubscribeButtonView", bundle: nil), forHeaderFooterViewReuseIdentifier: SubscribeButtonView.identifier)
        tableView.register(UINib(nibName: "IndividualTitleView", bundle: nil), forHeaderFooterViewReuseIdentifier: IndividualTitleView.identifier)


        tableView.separatorColor = UIColor.clear
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 180.0
        tableView.allowsSelection = false
        tableView.allowMultipleSectionsOpen = false

        tableView.initialOpenSections = [0]
    }
    
    
    
    func setRating(){
        var selectedOfferingId: String = ""
        var selectedOfferingType: String = ""
        if selectedType == MBTariffScreenType.CIN {
            if cinData?.tariffType == .cin,
                let aCINObject = cinData?.valueObject as? Cin {
                
                selectedOfferingId = aCINObject.header?.offeringId ?? ""
                
            } else if cinData?.tariffType == .newCin,
                let aNewCINObject = cinData?.valueObject as? Klass {
                
                selectedOfferingId = aNewCINObject.header?.offeringId ?? ""
            }
            
        } else if selectedType == .Individual {
            
            selectedOfferingId = individualData?.header?.offeringId ?? ""
            
        } else {
            
            selectedOfferingId = klassPostpaidData?.header?.offeringId ?? ""
        }
        if  let  userSurvey = MBUserSession.shared.appSurvays?.userSurveys?.first(where: { $0.offeringId == selectedOfferingId}){
            if let survey  = MBUserSession.shared.appSurvays?.surveys?.first(where: { $0.survayId  == userSurvey.surveryId }){
                if let  question =  survey.questions?.first(where: { $0.questionId == userSurvey.questionId }){
                    if let answer = question.answers?.firstIndex(where: { $0.answerId ==  userSurvey.answerId }) {
                        switch answer {
                        case 0:
                            print(answer)
                            btn1.setImage(UIImage.init(named: "RedFilledStar"), for: .normal)
                            btn2.setImage(UIImage.init(named: "RedLineStar"), for: .normal)
                            btn3.setImage(UIImage.init(named: "RedLineStar"), for: .normal)
                            btn4.setImage(UIImage.init(named: "RedLineStar"), for: .normal)
                            btn5.setImage(UIImage.init(named: "RedLineStar"), for: .normal)
                        case 1:
                            print(answer)
                            btn1.setImage(UIImage.init(named: "RedFilledStar"), for: .normal)
                            btn2.setImage(UIImage.init(named: "RedFilledStar"), for: .normal)
                            btn3.setImage(UIImage.init(named: "RedLineStar"), for: .normal)
                            btn4.setImage(UIImage.init(named: "RedLineStar"), for: .normal)
                            btn5.setImage(UIImage.init(named: "RedLineStar"), for: .normal)
                        case 2:
                            print(answer)
                            btn1.setImage(UIImage.init(named: "RedFilledStar"), for: .normal)
                            btn2.setImage(UIImage.init(named: "RedFilledStar"), for: .normal)
                            btn3.setImage(UIImage.init(named: "RedFilledStar"), for: .normal)
                            btn4.setImage(UIImage.init(named: "RedLineStar"), for: .normal)
                            btn5.setImage(UIImage.init(named: "RedLineStar"), for: .normal)
                        case 3:
                            print(answer)
                            btn1.setImage(UIImage.init(named: "RedFilledStar"), for: .normal)
                            btn2.setImage(UIImage.init(named: "RedFilledStar"), for: .normal)
                            btn3.setImage(UIImage.init(named: "RedFilledStar"), for: .normal)
                            btn4.setImage(UIImage.init(named: "RedFilledStar"), for: .normal)
                            btn5.setImage(UIImage.init(named: "RedLineStar"), for: .normal)
                        case 4:
                            print(answer)
                            btn1.setImage(UIImage.init(named: "RedFilledStar"), for: .normal)
                            btn2.setImage(UIImage.init(named: "RedFilledStar"), for: .normal)
                            btn3.setImage(UIImage.init(named: "RedFilledStar"), for: .normal)
                            btn4.setImage(UIImage.init(named: "RedFilledStar"), for: .normal)
                            btn5.setImage(UIImage.init(named: "RedFilledStar"), for: .normal)
                        default:
                            break
                        }
                        
                    }
                }
            }
        }else{
            btn1.setImage(UIImage.init(named: "RedLineStar"), for: .normal)
            btn2.setImage(UIImage.init(named: "RedLineStar"), for: .normal)
            btn3.setImage(UIImage.init(named: "RedLineStar"), for: .normal)
            btn4.setImage(UIImage.init(named: "RedLineStar"), for: .normal)
            btn5.setImage(UIImage.init(named: "RedLineStar"), for: .normal)
        }
    }
    
    @IBAction func tabStar(_ sender: UIButton){
        var selectedOfferingId: String = ""
        
        if selectedType == MBTariffScreenType.CIN {
            if cinData?.tariffType == .cin,
                let aCINObject = cinData?.valueObject as? Cin {
                
                selectedOfferingId = aCINObject.header?.offeringId ?? ""
                
            } else if cinData?.tariffType == .newCin,
                let aNewCINObject = cinData?.valueObject as? Klass {
                
                selectedOfferingId = aNewCINObject.header?.offeringId ?? ""
            }
            
        } else if selectedType == .Individual {
            
            selectedOfferingId = individualData?.header?.offeringId ?? ""
            
        } else {
            
            selectedOfferingId = klassPostpaidData?.header?.offeringId ?? ""
        }
        self.delegate?.starTapped(offerId: selectedOfferingId)
    }
   
}
