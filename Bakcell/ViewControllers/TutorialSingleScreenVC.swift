//
//  TutorialSingleScreenVC.swift
//  Bakcell
//
//  Created by Shujat on 25/10/2017.
//  Copyright © 2017 evampsaanga. All rights reserved.
//

import UIKit

// A Protocol
protocol TutorialSingleScreenDelegate: class {
    func navigatToLoginViewController()
}


class TutorialSingleScreenVC: UIViewController {
    
    public var pageTitle:String?
    public var pageDescription:String?
    public var currentPageNumber:Int?
    public var iconName:String?

    weak var delegate: TutorialSingleScreenDelegate?
    
    @IBOutlet var pageTitleLabel:UILabel!
    @IBOutlet var pageDescLabel:UILabel!
    @IBOutlet var pageControl:UIPageControl!
    @IBOutlet var imageView:UIImageView!
    @IBOutlet var skipButton:UIButton!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.pageTitleLabel.text = self.pageTitle!
        self.pageDescLabel.text = self.pageDescription!
        self.pageControl.currentPage = self.currentPageNumber!

        skipButton.setTitle(Localized("BtnTitle_SKIP"), for: .normal)

        if self.currentPageNumber == 0 {
            self.imageView.image = UIImage(named: self.iconName!)

        } else {

            self.imageView.image = UIImage.gif(name: self.iconName!)
            
            if self.currentPageNumber == 4 {
                skipButton.setTitle(Localized("BtnTitle_GetStarted"), for: .normal)
                skipButton.layer.borderColor = UIColor.MBLightRedColor.cgColor
                skipButton.layer.borderWidth = 1.5
                skipButton.layer.cornerRadius = 20.0
            }
        }
        
    }
    
    //MARK: - IBActions
    
    @IBAction func skipPressed(_ sender: UIButton) {

        self.delegate?.navigatToLoginViewController()
        
//        if MBUserSession.shared.isLoggedIn() && MBUserSession.shared.loadUserInfomation() {
//
//            let mainViewController   = self.myStoryboard.instantiateViewController(withIdentifier: "TabbarController") as! TabbarController
//            let drawerViewController = self.myStoryboard.instantiateViewController(withIdentifier: "SideDrawerVC") as! SideDrawerVC
//            let drawerController = KYDrawerController(drawerDirection: .left, drawerWidth: (UIScreen.main.bounds.width * 0.85))
//            drawerController.mainViewController = mainViewController
//            drawerController.drawerViewController = drawerViewController
//            self.navigationController?.pushViewController(drawerController, animated: true)
//
//        } else {
//
//            let loginVC = self.myStoryboard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
//            self.navigationController?.pushViewController(loginVC, animated: true)
//        }
    }
}
