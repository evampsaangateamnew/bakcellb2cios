//
//  LogoutAlertVC.swift
//  Bakcell
//
//  Created by AbdulRehman Warraich on 7/30/19.
//  Copyright © 2019 evampsaanga. All rights reserved.
//

import UIKit

class LogoutAlertVC: BaseVC {
    
    // Properties
    var alertTitle = ""
    var attributedDiscription : NSMutableAttributedString = NSMutableAttributedString()
    
    var yesButtonHolder = UIButton()
    var noButtonHolder = UIButton()
    var manageAccountButtonHolder = UIButton()
    
    fileprivate var yesBtncompletionHandlerBlock : MBButtonCompletionHandler?
    fileprivate var noBtncompletionHandlerBlock : MBButtonCompletionHandler?
    fileprivate var manageAccountBtncompletionHandlerBlock : MBButtonCompletionHandler?
    
    @IBOutlet var title_lbl: UILabel!
    @IBOutlet var description_lbl: UILabel!
    
    @IBOutlet var yes_btn: MBMarqueeButton!
    @IBOutlet var no_btn: MBMarqueeButton!
    @IBOutlet var manageAccount_btn: MBMarqueeButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        yes_btn.setMarqueeButtonLayout()
        no_btn.setMarqueeButtonLayout()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.interactivePop(false)
        
        title_lbl.text = alertTitle
        
        description_lbl.attributedText = attributedDiscription
        
        yes_btn.setTitle(yesButtonHolder.title(for: .normal), for: UIControl.State.normal)
        yes_btn.setTitleColor(yesButtonHolder.titleColor(for: .normal), for: UIControl.State.normal)
        yes_btn.backgroundColor = yesButtonHolder.backgroundColor
        yes_btn.layer.borderColor = yesButtonHolder.layer.borderColor
        yes_btn.layer.borderWidth = yesButtonHolder.layer.borderWidth
        
        no_btn.setTitle(noButtonHolder.title(for: .normal), for: UIControl.State.normal)
        no_btn.setTitleColor(noButtonHolder.titleColor(for: .normal), for: UIControl.State.normal)
        no_btn.backgroundColor = noButtonHolder.backgroundColor
        no_btn.layer.borderColor = yesButtonHolder.layer.borderColor
        no_btn.layer.borderWidth = yesButtonHolder.layer.borderWidth
        no_btn.layoutIfNeeded()
        
        
        manageAccount_btn.setTitle(manageAccountButtonHolder.title(for: .normal), for: UIControl.State.normal)
        manageAccount_btn.setTitleColor(manageAccountButtonHolder.titleColor(for: .normal), for: UIControl.State.normal)
        manageAccount_btn.backgroundColor = manageAccountButtonHolder.backgroundColor
        manageAccount_btn.layer.borderColor = manageAccountButtonHolder.layer.borderColor
        manageAccount_btn.layer.borderWidth = manageAccountButtonHolder.layer.borderWidth
        
    }
    
    //MARK: IBACTIONS
    @IBAction func YesBtnPressed(_ sender: UIButton) {
        self.dismiss(animated: true, completion: {
            self.yesBtncompletionHandlerBlock?("")
        })
    }
    
    @IBAction func NoBtnPressed(_ sender: UIButton) {
        
        self.dismiss(animated: true, completion: {
            self.noBtncompletionHandlerBlock?("")
        })
    }
    
    @IBAction func manageAccountBtnPressed(_ sender: UIButton) {
        
        self.dismiss(animated: true, completion: {
            self.manageAccountBtncompletionHandlerBlock?("")
        })
    }
    
    //MARK: - Genral Alert
    func setAlertForLogOut() {
        
        self.initButtonsWithDefaultValues()
        
        alertTitle = Localized("Title_Logout")
        
        // set Button Tilte
        yesButtonHolder.setTitle(Localized("BtnTitle_LOGOUT"), for: UIControl.State.normal)
        noButtonHolder.setTitle(Localized("BtnTitle_CANCEL"), for: UIControl.State.normal)
        manageAccountButtonHolder.setTitle(Localized("BtnTitle_MANAGEACCOUNT"), for: UIControl.State.normal)
        
        // Change BackGround color
        yesButtonHolder.backgroundColor = UIColor.MBRedColor
        noButtonHolder.backgroundColor = UIColor.MBButtonBackgroundGrayColor
        manageAccountButtonHolder.backgroundColor = UIColor.MBRedColor
        
        // Button Title color
        yesButtonHolder.setTitleColor(UIColor.white, for: UIControl.State.normal)
        noButtonHolder.setTitleColor(UIColor.black, for: UIControl.State.normal)
        manageAccountButtonHolder.setTitleColor(UIColor.white, for: UIControl.State.normal)
        
        yesButtonHolder.layer.borderColor = UIColor.white.cgColor
        manageAccountButtonHolder.layer.borderColor = UIColor.white.cgColor
        
        // Description label text
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.alignment = .left
        
        let message = NSMutableAttributedString(string: Localized("Message_LogoutMessage_MultiAccount"), attributes: [NSAttributedString.Key.foregroundColor: UIColor.MBTextGrayColor,NSAttributedString.Key.paragraphStyle:paragraphStyle])
        message.addAttribute(NSAttributedString.Key.font,
                             value: UIFont.MBArial(fontSize: 14),
                             range: NSRange(location: 0 , length: message.length))
        
        attributedDiscription = message
        
    }
    
    // Yes and No button completion handler
    func setYesButtonCompletionHandler(completionBlock : @escaping MBButtonCompletionHandler ) {
        self.yesBtncompletionHandlerBlock = completionBlock
    }
    
    func setNoButtonCompletionHandler(completionBlock : @escaping MBButtonCompletionHandler ) {
        self.noBtncompletionHandlerBlock = completionBlock
    }
    
    func setManageAccountButtonCompletionHandler(completionBlock : @escaping MBButtonCompletionHandler ) {
        self.manageAccountBtncompletionHandlerBlock = completionBlock
    }
    
    //MARK: - Helping Functions
    
    func initButtonsWithDefaultValues() {
        
        yesButtonHolder.setTitle(Localized("BtnTitle_YES"), for: UIControl.State.normal)
        yesButtonHolder.setTitleColor(yesButtonHolder.titleColor(for: .normal), for: UIControl.State.normal)
        yesButtonHolder.backgroundColor = UIColor.MBButtonBackgroundGrayColor
        yesButtonHolder.setTitleColor(UIColor.black, for: UIControl.State.normal)
        yesButtonHolder.layer.borderColor = UIColor.MBBorderGrayColor.cgColor
        yesButtonHolder.layer.borderWidth = 1
        
        
        noButtonHolder.setTitle(Localized("BtnTitle_NO"), for: UIControl.State.normal)
        noButtonHolder.setTitleColor(UIColor.black, for: UIControl.State.normal)
        noButtonHolder.backgroundColor = UIColor.white
        noButtonHolder.setTitleColor(UIColor.black, for: UIControl.State.normal)
        noButtonHolder.layer.borderColor = UIColor.MBBorderGrayColor.cgColor
        noButtonHolder.layer.borderWidth = 1
        
        manageAccountButtonHolder.setTitle(Localized("BtnTitle_YES"), for: UIControl.State.normal)
        manageAccountButtonHolder.setTitleColor(manageAccountButtonHolder.titleColor(for: .normal), for: UIControl.State.normal)
        manageAccountButtonHolder.backgroundColor = UIColor.MBButtonBackgroundGrayColor
        manageAccountButtonHolder.setTitleColor(UIColor.black, for: UIControl.State.normal)
        manageAccountButtonHolder.layer.borderColor = UIColor.MBBorderGrayColor.cgColor
        manageAccountButtonHolder.layer.borderWidth = 1
    }
}
