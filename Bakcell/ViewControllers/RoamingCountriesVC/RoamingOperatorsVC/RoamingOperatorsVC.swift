//
//  RoamingOperatorsVC.swift
//  Bakcell
//
//  Created by AbdulRehman Warraich on 1/2/19.
//  Copyright © 2019 evampsaanga. All rights reserved.
//

import UIKit
import ObjectMapper

class RoamingOperatorsVC: BaseVC {
    
    //MARK: - Properties
    var countryDetail : CountryDetailList?
    var selectedIndex: Int = 0
    
    var supplementaryOffers : SupplementaryOfferings?
    var offers : [Offers] = []
    
    //MARK: - IBOutlets
    @IBOutlet var title_lbl: UILabel!
    @IBOutlet var collectionView: UICollectionView!
    @IBOutlet var tableView: UITableView!
    @IBOutlet var bundlesView: UIView!
    @IBOutlet var countryIcon_img: UIImageView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        loadViewLayout()
        
        //init CollectionView
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView .selectItem(at: IndexPath(item:0, section:0) , animated: true, scrollPosition: UICollectionView.ScrollPosition.left)
        
        //init TableView
        tableView.delegate = self
        tableView.dataSource = self
        tableView.hideDefaultSeprator()
        tableView.contentInset.top = 0.0
        
        tableView.register(UINib(nibName: "OperatorsCell", bundle: nil), forCellReuseIdentifier: "OperatorsCell")
        tableView.register(UINib(nibName: "RoamingHeaderView", bundle: nil), forHeaderFooterViewReuseIdentifier: "RoamingHeaderView")
        
        //setting first operator as default
        self.selectOperator(atIndex: selectedIndex)
    }
    //MARK: - Functions
    
    func loadViewLayout(){
        
        title_lbl.font = UIFont.MBArial(fontSize: 20)
        title_lbl.text = countryDetail?.countryName ?? ""
        bundlesView.isHidden = true
        setupRomaingOffers()
        setupCountryFlag()
    }
    
    func selectOperator(atIndex index : Int) {
        self.selectedIndex = index
        
        //reloading tableView data
        self.reloadTableViewData()
    }

    func reloadTableViewData() {
        if (self.countryDetail?.operatorList?[selectedIndex].servicesList?.count ?? 0) <= 0 {
            self.tableView.showDescriptionViewWithImage(description: Localized("Message_NoDataAvalible"))

        } else {
            self.tableView.hideDescriptionView()
        }
        self.tableView.reloadData()
    }
    
    
    func setupCountryFlag () {
        
        for countryFlag:Country in self.supplementaryOffers?.roaming?.countriesFlags ?? [Country]() {
            if countryFlag.name?.trimmWhiteSpace.lowercased().prefix(3) == countryDetail?.countryName?.trimmWhiteSpace.lowercased().prefix(3) {
                countryIcon_img.image = UIImage.init(named: countryFlag.flag ?? "")
                break
            }
        }
    }
    
    func setupRomaingOffers() {
        
        /* Load data from UserDefaults in case not connected to internet */
        if let supplementaryOffersHandler : SupplementaryOfferings = SupplementaryOfferings.loadFromUserDefaults(key: APIsType.supplementaryOffer.selectedLocalizedAPIKey()){
            
            // Set information in local object
            self.supplementaryOffers = supplementaryOffersHandler
            // Redirect user to selected screen
            let (filterOffers, flagName) = filterOfferByCountryName(countryName: countryDetail?.countryName ?? "", offerArray: self.supplementaryOffers?.roaming?.offers)

            if filterOffers.count <= 0 {

            } else {
                loadRoamingOffersScreen(offers: filterOffers)
            }
            if flagName.isEmpty {
                countryIcon_img.image = UIImage.init(named:"")
            } else {
                countryIcon_img.image = UIImage.init(named: flagName)
            }
            
        } else {
            self.getSupplementaryOfferings()
        }
        self.activityIndicator.hideActivityIndicator()
    }
}
//MARK: - COLLECTION VIEW Delegates
extension RoamingOperatorsVC: UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if indexPath.item < (countryDetail?.operatorList?.count ?? 0 - 1) {
            if let operatorObj = countryDetail?.operatorList?[indexPath.item] {
                
                return CGSize(width: ((operatorObj.name ?? "").size(withAttributes: [NSAttributedString.Key.font: UIFont.MBArial(fontSize: 15)]).width + 30), height: 35)
            }
        } else {
            return CGSize(width: (Localized("Title_Bunndle").size(withAttributes: [NSAttributedString.Key.font: UIFont.MBArial(fontSize: 15)]).width + 30), height: 35)
        }
        
        return CGSize(width: 20, height: 35)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        let totalRows = countryDetail?.operatorList?.count ?? 0
        if totalRows > 0 {
            return totalRows + 1
        } else {
            return totalRows
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "RoamingCollectionCell", for: indexPath) as? RoamingCollectionCell {
            
            // Tab Button border color
            cell.titleLabel.font = UIFont.MBArial(fontSize: 15)
            cell.layer.borderColor = UIColor.MBBorderGrayColor.cgColor
            cell.titleLabel.highlightedTextColor = UIColor.black
            
            let bgColorView = UIView()
            bgColorView.backgroundColor = UIColor.MBButtonBackgroundGrayColor
            cell.selectedBackgroundView = bgColorView
            
            if indexPath.item < (countryDetail?.operatorList?.count ?? 0 - 1) {
                if let operatorObj = countryDetail?.operatorList?[indexPath.item] {
                    cell.titleLabel.text = operatorObj.name
                }
            }
             else {
                cell.titleLabel.text = Localized("Title_Bunndle")
            }
            
            return cell
        }
        
        return UICollectionViewCell()
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if indexPath.item > 0 && indexPath.item == countryDetail?.operatorList?.count ?? 0 {
            self.loadSupplementaryOfferingsData()
        } else {
            bundlesView.isHidden = true
            self.selectOperator(atIndex: indexPath.item)
        }
    }
    
    
    func loadSupplementaryOfferingsData() {
        
        /* Load data from UserDefaults in case not connected to internet */
        if let supplementaryOffersHandler : SupplementaryOfferings = SupplementaryOfferings.loadFromUserDefaults(key: APIsType.supplementaryOffer.selectedLocalizedAPIKey()){
            
            // Set information in local object
            self.supplementaryOffers = supplementaryOffersHandler
            // Redirect user to selected screen
            let (filterOffers, flagName) = filterOfferByCountryName(countryName: countryDetail?.countryName ?? "", offerArray: self.supplementaryOffers?.roaming?.offers)

            if filterOffers.count <= 0 {

                self.showAlertWithMessage(message: Localized("Message_NoOfferWereFound"))

            } else {
                bundlesView.isHidden = false
                loadRoamingOffersScreen(offers: filterOffers)
            }
            if flagName.isEmpty {
                countryIcon_img.image = UIImage.init(named:"")
            } else {
                countryIcon_img.image = UIImage.init(named: flagName)
            }
            
        } else {
            self.getSupplementaryOfferings()
        }
        self.activityIndicator.hideActivityIndicator()
    }
    
    //MARK: - Filtering offers data accourding to selectes country.
    func filterOfferByCountryName(countryName : String , offerArray : [Offers]? ) -> ([Offers], String) {
        // Filtering offers array
        var filteredOffers : [Offers] = []
        var imageName : String = ""
        print("country to search:\(countryName)")
        if let offers = offerArray {

            filteredOffers = offers.filter() {

                if let roamingDetailsCountriesList = ($0 as Offers).details?.roamingDetails?.roamingDetailsCountriesList {

                    let  aRoamingDetailsCountry = roamingDetailsCountriesList.filter() {

                        if let aCountryName = ($0 as RoamingDetailsCountries).countryName {
                            print("name found:\(aCountryName)")

                            if aCountryName.trimmWhiteSpace.lowercased() == countryName.trimmWhiteSpace.lowercased() || aCountryName.lowercased().contains(countryName.trimmWhiteSpace.lowercased()) {

                                if imageName.isBlank == true {
                                    imageName = ($0 as RoamingDetailsCountries).flag ?? ""
                                }
                                return true
                            } else {
                                return false
                            }

                        } else {

                            return false
                        }
                    }

                    if  aRoamingDetailsCountry.count > 0 {

                        return true
                        
                    } else {
                        
                        return false
                    }
                    
                }  else {
                    return false
                }
            }
        }

        return (filteredOffers, imageName)
    }
    
    func loadRoamingOffersScreen (offers : [Offers]) {
        
        bundlesView.removeAllSubViews()
        
        let supp = self.myStoryBoard.instantiateViewController(withIdentifier: "SuplementaryOffersVC") as! SuplementaryOffersVC
        supp.offers = offers
        supp.isCardView = true
        supp.selectedOffersType = MBOfferTabType.Roaming
        
        self.addChild(supp)
        bundlesView.addSubview(supp.view)
        supp.didMove(toParent: self)
        addConstraintsToViewController(viewController: supp)
    }
    
    func addConstraintsToViewController(viewController : UIViewController) {
        viewController.view.snp.makeConstraints { make in
            make.top.equalTo(bundlesView.snp.top).offset(0.0)
            make.right.equalTo(bundlesView.snp.right).offset(0.0)
            make.left.equalTo(bundlesView.snp.left).offset(0.0)
            make.bottom.equalTo(bundlesView.snp.bottom).offset(0.0)
        }
    }
}

extension RoamingOperatorsVC: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return self.countryDetail?.operatorList?[selectedIndex].servicesList?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 56
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        if let headerCell = tableView.dequeueReusableHeaderFooterView(withIdentifier: "RoamingHeaderView") as? RoamingHeaderView {
            
            if let aService = self.countryDetail?.operatorList?[selectedIndex].servicesList?[section] {
                headerCell.lblName.text = aService.name
                headerCell.lblValue.text = aService.unit
            } else {
                headerCell.lblName.text = ""
                headerCell.lblValue.text = ""
            }
            return headerCell
        }
        return UITableViewHeaderFooterView()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.countryDetail?.operatorList?[selectedIndex].servicesList?[section].types?.count ?? 0
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let cell = tableView.dequeueReusableCell(withIdentifier: "OperatorsCell", for: indexPath) as? OperatorsCell {
            
            cell.setTypeLatout(self.countryDetail?.operatorList?[selectedIndex].servicesList?[indexPath.section].types?[indexPath.row])
            return cell
        }
        
        return UITableViewCell()
    }
  
}


extension RoamingOperatorsVC {
    
    func getSupplementaryOfferings() {
        
        activityIndicator.showActivityIndicator()
        
        _ = MBAPIClient.sharedClient.getSupplementaryOfferings({ (response, resultData, error, isCancelled, status, resultCode, resultDesc) in
            
            self.activityIndicator.hideActivityIndicator()
            
            if error != nil {
                //error?.showServerErrorInViewController(self)
                
            } else {
                // handling data from API response.
                if resultCode == Constants.MBAPIStatusCode.succes.rawValue {
                    
                    // Parssing response data
                    if let supplementaryOffersHandler = Mapper<SupplementaryOfferings>().map(JSONObject: resultData){
                        
                        // Saving Supplementary Offers data in user defaults
                        supplementaryOffersHandler.saveInUserDefaults(key: APIsType.supplementaryOffer.selectedLocalizedAPIKey())
                        
                        // Set information in local object
                        self.supplementaryOffers = supplementaryOffersHandler
                    }
                } else {
                    // Show error alert to user
                    //self.showErrorAlertWithMessage(message: resultDesc)
                }
            }
            // Redirect user to selected screen
            if self.supplementaryOffers?.roaming?.offers?.count ?? 0 > 0 {
                self.loadSupplementaryOfferingsData()
            } else {
                //self.showAlertWithMessage(message: Localized("Message_NoOfferWereFound"))
            }
        })
    }
}
