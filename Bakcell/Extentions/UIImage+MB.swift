//
//  UIImage+MB.swift
//  Bakcell
//
//  Created by AbdulRehman Warraich on 10/7/17.
//  Copyright © 2017 evampsaanga. All rights reserved.
//

import Foundation
import UIKit

// MARK: - UIImage (Base64 Encoding)

extension UIImage {

    public enum ImageFormat {
        case PNG
        case JPEG(CGFloat)
    }

    public func base64(format: ImageFormat) -> (String,String) {
        var imageData: Data

        switch format {

        case .PNG:
            imageData = self.pngData() ?? Data()

        case .JPEG(let compression):

            imageData = self.jpegData(compressionQuality: compression) ?? Data()
        }

        let imageFormate = imageData.format()

        return (imageData.base64EncodedString(), imageFormate)
    }
}


