//
//  FAQsVC.swift
//  Bakcell
//
//  Created by Saad Riaz on 6/5/17.
//  Copyright © 2017 evampsaanga. All rights reserved.
//

import UIKit
import KYDrawerController
import Alamofire
import AlamofireObjectMapper
import ObjectMapper


class FAQsVC: BaseVC {
    
    //MARK: - Properties
    var showSearch : Bool = true
    var faq : [FAQList]?
    var qaList : [QAList]?
    var filterQaList : [QAList]?

    var selectedSectionIndexs: [Int] = []
    
    //MARK: - IBOutlet
    @IBOutlet var search_txt: UITextField!
    @IBOutlet var search_btn: UIButton!
    @IBOutlet var searchView: UIView!
    @IBOutlet var title_lbl: UILabel!
    @IBOutlet var dropDown: UIDropDown!
    @IBOutlet var dropDownImage: UIImageView!
    @IBOutlet var tableView: MBAccordionTableView!
    
    //MARK: - ViewContoller methods
    override func viewDidLoad() {
        super.viewDidLoad()

        loadViewLayout()

        search_txt.delegate = self
        searchView.isHidden = true

        tableView.delegate = self
        tableView.dataSource = self
        tableView.allowMultipleSectionsOpen = true
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 180

        tableView.register(UINib(nibName: "FAQsCell", bundle: nil), forCellReuseIdentifier: "cellID")
        tableView.register(UINib(nibName: "AccordionHeaderView1", bundle: nil), forHeaderFooterViewReuseIdentifier: AccordionHeaderView1.identifier)

        // reload data first
        reloadTableViewData()
        // Load FAQs data
        loadFAQsData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.interactivePop(true)
        
        //EventLog
        MBAnalyticsManager.logEvent(screenName: "FAQ Screen", contentType:"FAQ Screen" , status:"FAQ Screen" )
    }

    //MARK: - IBActions
    
    @IBAction func searchPressed(_ sender: UIButton) {
        
        if showSearch == false {
            search_btn.setImage(UIImage (imageLiteralResourceName: "actionbar_search") , for: UIControl.State.normal)
            searchView.isHidden = true
            title_lbl.isHidden = false
            showSearch = true
            
            search_txt.text = ""
            search_txt.resignFirstResponder()

            searchOffersByName(searchString: "")
            
        } else {
            search_btn.setImage(UIImage (imageLiteralResourceName: "Cross") , for: UIControl.State.normal)
            title_lbl.isHidden = true
            searchView.isHidden = false
            showSearch = false
            
            search_txt.becomeFirstResponder()
            
        }
    }
    
    //MARK: - FUNCTIONS
    func loadViewLayout(){
        title_lbl.text = Localized("Title_FAQ")

        search_txt.attributedPlaceholder = NSAttributedString(string: Localized("Title_Search"),
                                                              attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
    }
    
    func setDropDown(){
        
        //Drop Down
        self.dropDown.center = CGPoint(x: self.view.frame.midX, y: self.view.frame.midY)

        if self.faq?.indices.contains(0) ?? false {
            self.dropDown.placeholder = self.faq?[0].title ?? ""
            self.qaList = self.faq?[0].qaList
            self.filterQaList = self.qaList

            self.reloadTableViewData()

        }

        self.dropDown.rowBackgroundColor = UIColor.MBDimLightGrayColor
        dropDown.setFont = UIFont.MBArial(fontSize: 12)
        
        if let aCount = self.faq?.count {
            
            self.dropDown.tableHeight = CGFloat(35 * aCount)
        } else {
            self.dropDown.tableHeight = 35 * 0
        }
        
        self.dropDown.rowBackgroundColor = UIColor.MBDimLightGrayColor
        
        var titles : [String] = []
        
        faq?.forEach({ (aFAQList) in
            titles.append(aFAQList.title)
        })
        
        self.dropDown.options = titles

        self.dropDown.tableWillAppear {
            self.dropDownImage.image = UIImage (named: "Drop-down-arrow-state2")
        }

        self.dropDown.tableWillDisappear {
            self.dropDownImage.image = UIImage (named: "Drop-down-arrow-state1")
        }

        self.dropDown.didSelect { (option, index) in
            
            self.qaList = self.faq?[index].qaList
            self.filterQaList = self.qaList

            self.reloadTableViewData()

        }
        self.view.addSubview(self.dropDown)
        
    }

    private func collapseAllSections() {
        if let count = self.filterQaList?.count {
            for i in 0..<count {
                if tableView.isSectionOpen(i) {
                    tableView.toggleSection(i)
                }
            }
        }
    }

    func loadFAQsData() {
        // Load data from UserDefaults
        if let faqResponseHandler :FAQListHandler = FAQListHandler.loadFromUserDefaults(key: APIsType.faqs.selectedLocalizedAPIKey()){
            
            self.faq = faqResponseHandler.faqList
            self.qaList = self.faq?[0].qaList
            self.filterQaList = self.qaList
            
            self.reloadTableViewData()
            
            self.setDropDown()
            
        } else {
            FAQsAPICall()
        }
    }

    func reloadTableViewData() {

        if (filterQaList?.count ?? 0) <= 0 {
            self.tableView.showDescriptionViewWithImage(description: Localized("Message_NoDataAvalible"))
        
        } else {
            self.tableView.hideDescriptionView()
        }

        tableView.reloadData()
    }
    
    //MARK: - API Calls
    /// Call 'FAQs' API.
    ///
    /// - returns: Void
    func FAQsAPICall() {
        
        activityIndicator.showActivityIndicator()
        
        _ = MBAPIClient.sharedClient.getFAQs({ ( response, resultData, error, isCancelled, status, resultCode, resultDesc) in
            
            self.activityIndicator.hideActivityIndicator()
            if error != nil {
                error?.showServerErrorInViewController(self)
                
            } else {
                // handling data from API response.
                if resultCode == Constants.MBAPIStatusCode.succes.rawValue {
                    
                    // Parssing response data
                    if let faqResponseHandler = Mapper<FAQListHandler>().map(JSONObject:resultData) {
                        
                        if let faqList = faqResponseHandler.faqList {
                            // Saving Supplementary Offers data in user defaults
                            
                            faqResponseHandler.saveInUserDefaults(key: APIsType.faqs.selectedLocalizedAPIKey())

                            self.faq = faqList
                            self.qaList = self.faq?[0].qaList
                            self.filterQaList = self.qaList

                            self.setDropDown()
                        }
                    }
                } else {
                    // Show error alert to user
                    self.showErrorAlertWithMessage(message: resultDesc)
                }
            }

            // Reload table View data
            self.reloadTableViewData()
        })
    }
}

//MARK: - Table View Delegates

extension FAQsVC : UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if let aQaList = filterQaList {
            //return
            return aQaList.count
        } else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 1
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }

    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50.0
        
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellID", for: indexPath) as! FAQsCell

        cell.answer_lbl.text = "\(filterQaList?[indexPath.section].answer ?? "")"

        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: AccordionHeaderView1.identifier) as! AccordionHeaderView1
        
        headerView.question_Lbl.text = filterQaList?[section].question

        // Check if section is expanded then set expaned image else set unexpended image
        if selectedSectionIndexs.contains(section) {
            headerView.plus_img.image  = #imageLiteral(resourceName: "minu-sign")
        } else {
            headerView.plus_img.image = #imageLiteral(resourceName: "Plus-Sign")
        }

        return headerView
    }
}

// MARK: - <MBAccordionTableViewDelegate>

extension FAQsVC : MBAccordionTableViewDelegate {
    
    func tableView(_ tableView: MBAccordionTableView, willOpenSection section: Int, withHeader header: UITableViewHeaderFooterView?) {

        if let headerView = header as? AccordionHeaderView1 {

            headerView.plus_img.image  = #imageLiteral(resourceName: "minu-sign")
            // Adding section from selectedSectionIndexs
            selectedSectionIndexs.append(section)
        }
        
    }

    func tableView(_ tableView: MBAccordionTableView, didCloseSection section: Int, withHeader header: UITableViewHeaderFooterView?) {

        if let headerView = header as? AccordionHeaderView1 {
            headerView.plus_img.image = #imageLiteral(resourceName: "Plus-Sign")
            // Removing section from selectedSectionIndexs
            if let sectionValue = selectedSectionIndexs.firstIndex(of: section) {
                selectedSectionIndexs.remove(at: sectionValue)
            }
        }
        
    }
    
    func tableView(_ tableView: MBAccordionTableView, canInteractWithHeaderAtSection section: Int) -> Bool {
        return true
    }
}


extension FAQsVC : UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        guard let text = textField.text else { return false }
        
        if textField == search_txt {
            
            let newString = (text as NSString).replacingCharacters(in: range, with: string)
            self.searchOffersByName(searchString: newString)
        }
        
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return false
    }
    
    func searchOffersByName(searchString: String?) {
        
        if searchString?.isBlank ?? true {

            filterQaList = qaList
        } else {

            filterQaList = searchQABy(searchString: searchString ?? "", qaList: qaList)
        }
        
        self.reloadTableViewData()
    }

    func searchQABy(searchString: String, qaList: [QAList]?) -> [QAList]? {

        if (qaList?.count ?? 0) > 0 {

            let filterQAList = qaList?.filter {

                let faqAnswer = ($0 as QAList).answer

                return faqAnswer.containsSubString(subString: searchString)

            }

            if filterQAList != nil {

                return filterQAList
            } else {
                return []
            }

        } else {
            return []
        }
    }


}
