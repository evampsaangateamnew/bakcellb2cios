//
//  StoreDetailCell.swift
//  Bakcell
//
//  Created by Muhammad Irfan Awan on 28/12/2020.
//  Copyright © 2020 evampsaanga. All rights reserved.
//

import UIKit

class StoreDetailCell: UITableViewCell {
    
//    @IBOutlet var nameLbl: UILabel!
    @IBOutlet var addressLbl: UILabel!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
