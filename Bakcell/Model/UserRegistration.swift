//
//  UserRegistration.swift
//  Bakcell
//
//  Created by AbdulRehman Warraich on 8/18/17.
//  Copyright © 2017 evampsaanga. All rights reserved.
//

import Foundation
import ObjectMapper

struct SignUp :Mappable {

    var pin : String = ""

    init() { }
    
    init?(map: Map) {  }

    mutating func mapping(map: Map) {
        pin <- map["pin"]
    }
}

struct SaveCustomerModel :Mappable {

    var customerData : NewCustomerData?
//    var primaryOfferings: PrimaryOffering?
//    var supplementaryOffers : [SupplementaryOfferingList]?
    var predefinedData : PredefinedData?
    var promoMessage : String?
    
    init(customerData : NewCustomerData?) {
        self.customerData = customerData
    }
     init?(map: Map) {}

    mutating func mapping(map: Map) {

        //primaryOfferings        <- map["primaryOfferings"]
        //supplementaryOffers     <- map["supplementaryOfferingList"]
        customerData            <- map["customerData"]
        predefinedData          <- map["predefinedData"]
        promoMessage            <- map["promoMessage"]
    }
}

struct NewCustomerData :Mappable {
    
    var title : String?
    var firstName : String?
    var middleName : String?
    var lastName : String?
    var customerType : String?
    var gender : String?
    var dob : String?
    var accountId : String?
    var language : String?
    var effectiveDate : String?
    var expiryDate : String?
    var subscriberType : String?
    var status : String?
    var statusDetails : String?
    var brandId : String?
    var brandName : String?
    var loyaltySegment : String?
    var offeringId : String?
    var msisdn : String?
    var token : String?
    var imageURL : String?
    var billingLanguage : String?
    var customerId: String?
    var entityId : String?
    var offeringName : String?
    var offeringNameDisplay : String?
    var groupType : String = ""
    var sim : String?
    var pin : String?
    var puk : String?
    var email : String?
    
    var rateUsIOS : String?
    var firstPopup : String?
    var lateOnPopup : String?
    var rateUsPopupTitle : String?
    var rateUsPopupContent : String?
    var cinTariff : String?
    
    var hideNumberTariffIds : [String]?
    init() { }
    
    init?(map: Map) { }
    
    mutating func mapping(map: Map) {
        title               <- map["title"]
        firstName           <- map["firstName"]
        middleName          <- map["middleName"]
        lastName            <- map["lastName"]
        customerType        <- map["customerType"]
        gender              <- map["gender"]
        dob                 <- map["dob"]
        accountId           <- map["accountId"]
        language            <- map["language"]
        effectiveDate       <- map["effectiveDate"]
        expiryDate          <- map["expiryDate"]
        subscriberType      <- map["subscriberType"]
        status              <- map["status"]
        statusDetails       <- map["statusDetails"]
        brandId             <- map["brandId"]
        brandName           <- map["brandName"]
        loyaltySegment      <- map["loyaltySegment"]
        offeringId          <- map["offeringId"]
        msisdn              <- map["msisdn"]
        token               <- map["token"]
        imageURL            <- map["imageURL"]
        billingLanguage     <- map["billingLanguage"]
        offeringName        <- map["offeringName"]
        offeringNameDisplay <- map["offeringNameDisplay"]
        entityId            <- map["entityId"]
        customerId          <- map["customerId"]
        groupType           <- map["groupType"]
        sim                 <- map["sim"]
        pin                 <- map["pin"]
        puk                 <- map["puk"]
        email               <- map["email"]
        
        rateUsIOS           <- map["rateus_ios"]
        firstPopup          <- map["firstPopup"]
        lateOnPopup         <- map["lateOnPopup"]
        rateUsPopupTitle    <- map["popupTitle"]
        rateUsPopupContent  <- map["popupContent"]
        
        hideNumberTariffIds <- map["hideNumberTariffIds"]
        cinTariff           <- map["cinTariff"]
    }
}

struct PrimaryOffering :Mappable {
    
    var offeringId:String?
    var offeringName:String?
    var offeringCode:String?
    var offeringShortName:String?
    var status:String?
    var networkType:String?
    var effectiveTime:String?
    var expiredTime:String?
    
    init(map:Map) {}
    
    mutating func mapping(map: Map) {
        
        offeringId              <- map["OfferingId"]
        offeringName            <- map["OfferingName"]
        offeringCode            <- map["OfferingCode"]
        offeringShortName       <- map["OfferingShortName"]
        status                  <- map["Status"]
        networkType             <- map["NetworkType"]
        effectiveTime           <- map["EffectiveTime"]
        expiredTime             <- map["ExpiredTime"]
    }
}

struct SupplementaryOfferingList :Mappable {
    
    var offeringId:String?
    var offeringName:String?
    var offeringCode:String?
    var offeringShortName:String?
    var status:String?
    var networkType:String?
    var effectiveTime:String?
    var expiredTime:String?
    
    init(map:Map) {
    }
    
    mutating func mapping(map: Map) {
        
        offeringId          <- map["offeringId"]
        offeringName        <- map["offeringName"]
        offeringCode        <- map["offeringCode"]
        offeringShortName   <- map["offeringShortName"]
        status              <- map["status"]
        networkType         <- map["networkType"]
        effectiveTime       <- map["effectiveTime"]
        expiredTime         <- map["expiredTime"]
        
    }
    
}

struct PredefinedData :Mappable {
    
    var topup : TopUpData?
    var fnfAllowed : String = ""
    var redirectionLinks : RedirectionLinks?
    var tariffMigrationPrices: [KeyValue] = [KeyValue]()
    var liveChat : String?
    var notificationPopupConfig: NotificationPopupConfig?
    var roamingVisible: Bool = false
    var plasticCards: [PlasticCard]?
    var goldenPayMessages: GoldenPayMessages?
    
    init(map:Map) {
    }
    
    mutating func mapping(map: Map) {
        
        topup                   <- map["topup"]
        fnfAllowed              <- map["fnf.fnFAllowed"]
        redirectionLinks        <- map["redirectionLinks"]
        tariffMigrationPrices   <- map["tariffMigrationPrices"]
        liveChat                <- map["liveChat"]
        notificationPopupConfig <- map["notificationPopupConfig"]
        roamingVisible          <- map["roamingVisible"]
        plasticCards            <- map["topup.plasticCard.cardTypes"]
        goldenPayMessages       <- map["goldenPayMessages"]
        
    }
}

struct GoldenPayMessages : Mappable {
    var goldenpaymessage500ru : String?
    var goldenpaymessage116ru : String?
    var goldenpaymessage200ru : String?
    var goldenpaymessage116en : String?
    var goldenpaymessage116az : String?
    var goldenpaymessage500en : String?
    var goldenpaymessage500az : String?
    var goldenpaymessage200en : String?
    var goldenpaymessage200az : String?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        goldenpaymessage500ru <- map["goldenpay_message_500_ru"]
        goldenpaymessage116ru <- map["goldenpay_message_116_ru"]
        goldenpaymessage200ru <- map["goldenpay_message_200_ru"]
        goldenpaymessage116en <- map["goldenpay_message_116_en"]
        goldenpaymessage116az <- map["goldenpay_message_116_az"]
        goldenpaymessage500en <- map["goldenpay_message_500_en"]
        goldenpaymessage500az <- map["goldenpay_message_500_az"]
        goldenpaymessage200en <- map["goldenpay_message_200_en"]
        goldenpaymessage200az <- map["goldenpay_message_200_az"]
    }

}

struct TopUpData :Mappable {
    
    var getLoan : GetLoan?
    var moneyTransfer : MoneyTransfer?
    
    
    init(map:Map) {
    }
    
    mutating func mapping(map: Map) {
        
        getLoan             <- map["getLoan"]
        moneyTransfer       <- map["moneyTransfer"]
        
    }
    
}

struct RedirectionLinks :Mappable {
    
    var onlinePaymentGatewayURL_EN : String = ""
    var onlinePaymentGatewayURL_RU : String = ""
    var onlinePaymentGatewayURL_AZ : String = ""
    
    init(map:Map) {
    }
    
    mutating func mapping(map: Map) {
        
        onlinePaymentGatewayURL_EN          <- map["onlinePaymentGatewayURL_EN"]
        onlinePaymentGatewayURL_RU          <- map["onlinePaymentGatewayURL_RU"]
        onlinePaymentGatewayURL_AZ          <- map["onlinePaymentGatewayURL_AZ"]
        
    }
    
}

struct NotificationPopupConfig :Mappable {
    
    var hour            : String = ""
    var popupMessage    : String = ""
    
    init(map:Map) {
    }
    
    mutating func mapping(map: Map) {
        
        hour          <- (map["hour"], StringTransform())
        popupMessage  <- map["popupMessage"]
        
    }
    
    let transform = TransformOf<String, Int>(fromJSON: { (value: Int?) -> String? in
        
        return String(value!)
        
    }, toJSON: { (value: String?) -> Int? in
        
        return Int(value!)
    })
    
}

struct GetLoan :Mappable {
    
    var selectAmount : SelectAmount?
    
    init(map:Map) { }
    
    mutating func mapping(map: Map) {
        
        selectAmount          <- map["selectAmount"]
        
    }
}

struct MoneyTransfer :Mappable {
    
    var selectAmount : SelectAmount?
    var termsAndCondition : TermsSelectAmount?
    
    init(){}
    init(map:Map) {}
    
    mutating func mapping(map: Map) {
        
        selectAmount                <- map["selectAmount"]
        termsAndCondition           <- map["termsAndCondition"]
    }
}

struct SelectAmount :Mappable {
    
    var cin : [SelectDetails]?
    var klass : [SelectDetails]?
    
    init(map:Map) {
    }
    mutating func mapping(map: Map) {
        
        cin          <- map["cin"]
        klass        <- map["klass"]
    }
}

struct TermsSelectAmount :Mappable {
    
    var cin : [TermsSelectDetails]?
    var klass : [TermsSelectDetails]?
    
    init(map:Map) {}
    mutating func mapping(map: Map) {
        
        cin          <- map["cin"]
        klass        <- map["klass"]
    }
}

struct SelectDetails :Mappable {
    
    var amount : String?
    var currency : String?
    
    init(map:Map) {
        
    }
    
    mutating func mapping(map: Map) {
        
        amount          <- map["amount"]
        currency        <- map["currency"]
        
    }
    
}

struct TermsSelectDetails :Mappable {
    
    var amount : String?
    var currency : String?
    var text : String?
    
    init(map:Map) {
    }
    
    mutating func mapping(map: Map) {
        
        amount          <- map["amount"]
        currency        <- map["currency"]
        text            <- map["text"]
        
    }
    
}


struct UserRoamingStatus :Mappable {
    
    var roamingEnabled : Bool = false
    
    init(map:Map) {
    }
    
    mutating func mapping(map: Map) {
        
        roamingEnabled          <- map["active"]
        
    }
    
}


struct PlasticCard:Mappable {
    var cardKey : Int?
    var cardVale: String?
    
    init(map: Map) {
    }
    
    mutating func mapping(map: Map) {
        cardKey    <- map["key"]
        cardVale    <- map["value"]
    }
}


struct StringTransform: TransformType {

  func transformFromJSON(_ value: Any?) -> String? {
    return value.flatMap(String.init(describing:))
  }

  func transformToJSON(_ value: String?) -> Any? {
    return value
  }

}




