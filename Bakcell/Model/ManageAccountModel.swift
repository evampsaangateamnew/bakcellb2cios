//
//  ManageAccountModel.swift
//  Azerfon
//
//  Created by AbdulRehman Warraich on 4/2/19.
//  Copyright © 2019 Evamp&Saanga. All rights reserved.
//

import ObjectMapper

struct ManageAccountModel : Mappable {
    
    var currentUser     : UserDataModel?
    var users           : [UserDataModel]? = []
    
    init?(map: Map) {
    }
    
    init(current: UserDataModel?, users :[UserDataModel]) {
        self.currentUser = current
        self.users = users
    }
    
    mutating func mapping(map: Map) {
        
        currentUser     <- map["currentSelectedUser"]
        users           <- map["loggedInUsers"]
    }
    
}

struct UserDataModel : Mappable {
    
    var userInfo            : NewCustomerData?
    var predefineData       : PredefinedData?
    var appConfig           : AppConfig?
//    var primaryOfferings    : PrimaryOffering?
//    var supplementaryOffers : [SupplementaryOfferingList]?
//    
    init?(map: Map) {}
    
    init(current: NewCustomerData?, predefinedInfo :PredefinedData?, appConfig :AppConfig?) {
        self.userInfo = current
        self.predefineData = predefinedInfo
        self.appConfig = appConfig
    }
    
    mutating func mapping(map: Map) {
        
        userInfo        <- map["userInfo"]
        predefineData   <- map["predefineData"]
        appConfig       <- map["appConfig"]
    }
}

struct AppConfig : Mappable {
    
    var fcmTokenRegisterdForLogin   : Bool = false
    var unreadNotificationCount     : String?
    
    init?(map: Map) {}
    
    init(isTokenRegisterd :Bool, unreadCount :String?) {
        self.fcmTokenRegisterdForLogin = isTokenRegisterd
        self.unreadNotificationCount = unreadCount
    }
    
    mutating func mapping(map: Map) {
        
        fcmTokenRegisterdForLogin       <- map["fcmTokenRegisterdForLogin"]
        unreadNotificationCount         <- map["unreadNotificationCount"]
    }
}




