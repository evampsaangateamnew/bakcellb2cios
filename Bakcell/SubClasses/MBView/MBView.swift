//
//  MBView.swift
//  Bakcell B2B
//
//  Created by Waqar Ahmed on 6/25/18.
//  Copyright © 2018 Evamp&Saanga. All rights reserved.
//

import UIKit

// @IBDesignable
class MBView: UIView {
    @IBInspectable var roundedCornersValue : CGFloat = 0.0 {
        didSet {
            setupLayout()
        }
    }
    @IBInspectable var borderWidthValue : CGFloat = 0.0 {
        didSet{
            setupLayout()
        }
    }
    @IBInspectable var borderColor : UIColor = .MBButtonBackgroundGrayColor
        {
        didSet{
            setupLayout()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        setupLayout()
    }
    
    func setupLayout() {
        self.roundAllCorners(radius: roundedCornersValue)
        self.layer.borderWidth = borderWidthValue
        self.layer.borderColor = borderColor.cgColor
    }
}
