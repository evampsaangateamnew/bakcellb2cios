//
//  SubscribeButtonView.swift
//  Bakcell
//
//  Created by AbdulRehman Warraich on 10/17/17.
//  Copyright © 2017 evampsaanga. All rights reserved.
//

import UIKit

class SubscribeButtonView: MBAccordionTableViewHeaderView {

    static let identifier = "SubscribeButtonView"
    var viewType : MBSectionType = MBSectionType.UnSpecified

    @IBOutlet var containerView: UIView!

    @IBOutlet var activateRenew_btn: MBMarqueeButton!
    @IBOutlet var subscribed_btn: MBMarqueeButton!

    @IBOutlet var activateRenewRightConstraint: NSLayoutConstraint!
    @IBOutlet var activateRenewCenterConstraint: NSLayoutConstraint!

    @IBOutlet var subscribedLeftConstraint: NSLayoutConstraint!
    @IBOutlet var subscribedCenterConstraint: NSLayoutConstraint!

    // Using in Supplementary offer
    func setSubscribeButtonLayout(tag: Int) {

        containerView.backgroundColor = UIColor.MBDimLightGrayColor

        // set layout
        subscribed_btn.isHidden = true
        activateRenew_btn.isHidden = false

        activateRenewCenterConstraint.priority = UILayoutPriority(rawValue: 999)
        activateRenewRightConstraint.priority = UILayoutPriority(rawValue: 998)

        activateRenew_btn.tag = tag

        activateRenew_btn.backgroundColor = UIColor.MBButtonBackgroundGrayColor
        activateRenew_btn.layer.cornerRadius = CGFloat(Constants.kButtonCornerRadius)
        activateRenew_btn.setTitle(Localized("BtnTitle_SUBSCRIBE"), for: UIControl.State.normal)
        activateRenew_btn.setTitleColor(UIColor.black, for: UIControl.State.normal)
    }
    
    func setTopUpButtonLayout(tag: Int) {
        
        containerView.backgroundColor = UIColor.MBDimLightGrayColor
        
        // set layout
        subscribed_btn.isHidden = true
        activateRenew_btn.isHidden = false
        
        activateRenewCenterConstraint.priority = UILayoutPriority(rawValue: 999)
        activateRenewRightConstraint.priority = UILayoutPriority(rawValue: 998)
        
        activateRenew_btn.tag = tag
        
        activateRenew_btn.backgroundColor = UIColor.MBButtonBackgroundGrayColor
        activateRenew_btn.layer.cornerRadius = CGFloat(Constants.kButtonCornerRadius)
        activateRenew_btn.setTitle(Localized("BtnTitle_Topup"), for: UIControl.State.normal)
        activateRenew_btn.setTitleColor(UIColor.black, for: UIControl.State.normal)
    }


    func setSubscribedButton(tag: Int) {

        containerView.backgroundColor = UIColor.MBDimLightGrayColor
        // set layout
        subscribed_btn.isHidden = false
        activateRenew_btn.isHidden = true

        subscribedCenterConstraint.priority = UILayoutPriority(rawValue: 999)
        subscribedLeftConstraint.priority = UILayoutPriority(rawValue: 998)

        subscribed_btn.tag = tag
        
        subscribed_btn.backgroundColor = UIColor.MBButtonBackgroundGrayColor
        subscribed_btn.layer.cornerRadius = CGFloat(Constants.kButtonCornerRadius)
        subscribed_btn.setTitle(Localized("BtnTitle_SUBSCRIBED"), for: UIControl.State.normal)
        subscribed_btn.setTitleColor(UIColor.white, for: UIControl.State.normal)
    }

    func setRenewAndSubscribedButton(tag: Int) {

        containerView.backgroundColor = UIColor.MBDimLightGrayColor
        // set layout
        subscribed_btn.isHidden = false
        activateRenew_btn.isHidden = false

        subscribed_btn.isEnabled = false

        activateRenewRightConstraint.priority = UILayoutPriority(rawValue: 999)
        activateRenewCenterConstraint.priority = UILayoutPriority(rawValue: 998)

        subscribedLeftConstraint.priority = UILayoutPriority(rawValue: 999)
        subscribedCenterConstraint.priority = UILayoutPriority(rawValue: 998)

        activateRenew_btn.tag = tag

        activateRenew_btn.setTitle(Localized("BtnTitle_RENEW"), for: UIControl.State.normal)
        activateRenew_btn.isEnabled = true
        activateRenew_btn.backgroundColor = UIColor.MBButtonBackgroundGrayColor
        activateRenew_btn.layer.cornerRadius = CGFloat(Constants.kButtonCornerRadius)
        activateRenew_btn.setTitleColor(UIColor.MBRedColor, for: UIControl.State.normal)

        subscribed_btn.backgroundColor = UIColor.MBButtonBackgroundGrayColor
        subscribed_btn.layer.cornerRadius = CGFloat(Constants.kButtonCornerRadius)
        subscribed_btn.setTitle(Localized("BtnTitle_SUBSCRIBED"), for: UIControl.State.normal)
        subscribed_btn.setTitleColor(UIColor.white, for: UIControl.State.normal)

    }

    // Using My Subscription
    func setRenewDeActivateButton(tag: Int, renewEnable: Bool = false, deactivateEnable: Bool = false) {

        containerView.backgroundColor = UIColor.MBDimLightGrayColor
        // set layout
        subscribed_btn.isHidden = true
        activateRenew_btn.isHidden = true

        activateRenewRightConstraint.priority = UILayoutPriority(rawValue: 999)
        activateRenewCenterConstraint.priority = UILayoutPriority(rawValue: 998)

        subscribedLeftConstraint.priority = UILayoutPriority(rawValue: 999)
        subscribedCenterConstraint.priority = UILayoutPriority(rawValue: 998)

        activateRenew_btn.tag = tag
        subscribed_btn.tag = tag

        activateRenew_btn.setTitle(Localized("BtnTitle_RENEW"), for: UIControl.State.normal)
        activateRenew_btn.backgroundColor = UIColor.MBButtonBackgroundGrayColor
        activateRenew_btn.isEnabled = renewEnable
        activateRenew_btn.layer.cornerRadius = CGFloat(Constants.kButtonCornerRadius)
        activateRenew_btn.setTitleColor(UIColor.MBRedColor, for: UIControl.State.normal)

        subscribed_btn.setTitle(Localized("BtnTitle_DEACTIVATE"), for: UIControl.State.normal)
        subscribed_btn.backgroundColor = UIColor.clear
        subscribed_btn.isEnabled = deactivateEnable
        subscribed_btn.layer.cornerRadius = CGFloat(Constants.kButtonCornerRadius)
        subscribed_btn.layer.borderColor = UIColor.MBButtonBackgroundGrayColor.cgColor
        subscribed_btn.layer.borderWidth = 1


        if deactivateEnable {
            subscribed_btn.setTitleColor(UIColor.MBTextGrayColor, for: UIControl.State.normal)
            subscribed_btn.isEnabled = true
        } else {
            subscribed_btn.setTitleColor(UIColor.white, for: UIControl.State.normal)
            subscribed_btn.isEnabled = false
        }

        if renewEnable == true && deactivateEnable == false {

            activateRenewCenterConstraint.priority = UILayoutPriority(rawValue: 999)
            activateRenewRightConstraint.priority = UILayoutPriority(rawValue: 998)

            activateRenew_btn.isHidden = false
            subscribed_btn.isHidden = true

        } else if renewEnable == false && deactivateEnable == true {

            subscribedCenterConstraint.priority = UILayoutPriority(rawValue: 999)
            subscribedLeftConstraint.priority = UILayoutPriority(rawValue: 998)

            subscribed_btn.isHidden = false
            activateRenew_btn.isHidden = true
        } else if renewEnable == true && deactivateEnable == true {

            activateRenewRightConstraint.priority = UILayoutPriority(rawValue: 999)
            activateRenewCenterConstraint.priority = UILayoutPriority(rawValue: 998)

            subscribedLeftConstraint.priority = UILayoutPriority(rawValue: 999)
            subscribedCenterConstraint.priority = UILayoutPriority(rawValue: 998)

            subscribed_btn.isHidden = false
            activateRenew_btn.isHidden = false
        }
    }


    func setViewForSubscribeButtonForTariff(isSubscribed : String?, tag: Int = 0) {

        //View Type
        viewType = MBSectionType.Subscription

        containerView.backgroundColor = UIColor.MBDimLightGrayColor

        subscribed_btn.isHidden = false
        activateRenew_btn.isHidden = true

        subscribedCenterConstraint.priority = UILayoutPriority(rawValue: 999)
        subscribedLeftConstraint.priority = UILayoutPriority(rawValue: 998)

        // 0 = Can not Subscribe
        // 1 = Can Subscribe
        // 2 = Already Subscribe/ Subscribed
        // 3 = Renewable

        subscribed_btn.tag = tag

        subscribed_btn.backgroundColor = UIColor.MBButtonBackgroundGrayColor
        subscribed_btn.layer.cornerRadius = CGFloat(Constants.kButtonCornerRadius)

        if isSubscribed == "2" {
            subscribed_btn.isEnabled = false
            subscribed_btn.setTitle(Localized("BtnTitle_SUBSCRIBED"), for: UIControl.State.normal)
            subscribed_btn.setTitleColor(UIColor.white, for: UIControl.State.normal)

        } else if isSubscribed == "3" {
            subscribed_btn.isEnabled = true
            subscribed_btn.setTitle(Localized("BtnTitle_RENEW"), for: UIControl.State.normal)
            subscribed_btn.setTitleColor(UIColor.MBRedColor, for: UIControl.State.normal)

        } else if isSubscribed == "1" {
            subscribed_btn.isEnabled = true
            subscribed_btn.setTitle(Localized("BtnTitle_SUBSCRIBE"), for: UIControl.State.normal)
            subscribed_btn.setTitleColor(UIColor.black, for: UIControl.State.normal)

        } else if isSubscribed == "0" {
            subscribed_btn.isEnabled = false
            subscribed_btn.setTitle(Localized("BtnTitle_SUBSCRIBE"), for: UIControl.State.normal)
            subscribed_btn.setTitleColor(UIColor.black, for: UIControl.State.normal)
        }
    }
}

