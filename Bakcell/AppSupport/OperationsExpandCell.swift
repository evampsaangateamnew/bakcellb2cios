//
//  OperationsExpandCell.swift
//  Bakcell
//
//  Created by Saad Riaz on 9/26/17.
//  Copyright © 2017 evampsaanga. All rights reserved.
//

import UIKit

class OperationsExpandCell: UITableViewCell {

    @IBOutlet var clarification_lbl: UILabel!
    @IBOutlet var description_lbl: UILabel!
    @IBOutlet var title2_lbl: UILabel!
    @IBOutlet var title1_lbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
