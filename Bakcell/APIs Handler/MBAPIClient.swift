//
//  MBAPIClient.swift
//  Bakcell
//
//  Created by AbdulRehman Warraich on 8/16/17.
//  Copyright © 2017 evampsaanga. All rights reserved.
//

import UIKit
import Alamofire
import ObjectMapper

let MBAPIClientDefaultTimeOut = 30.0

let kRequestHeaders : [String : String] = ["deviceID": BaseVC.deviceID(), "Content-Type": "application/json", "UserAgent": "iPhone"]

class Connectivity {
    class var isConnectedToInternet:Bool {
        return NetworkReachabilityManager()?.isReachable ?? false
    }
}

class MBAPIClient: MBAPIClientHandler {


    static var sharedClient: MBAPIClient = {

        let baseURL = URL(string: Constants.kMBAPIClientBaseURL)
        let configuration = URLSessionConfiguration.default
        configuration.timeoutIntervalForRequest = MBAPIClientDefaultTimeOut
        configuration.requestCachePolicy = .reloadIgnoringCacheData
        
        // Configure the trust policy manager
        //With SSL Pining
        /*let serverTrustPolicy = ServerTrustPolicy.pinCertificates(
            certificates: ServerTrustPolicy.certificates(),
            validateCertificateChain: true,
            validateHost: true
        )*/

        //With Public key
        
        let serverTrustPolicy = ServerTrustPolicy.pinPublicKeys(
                publicKeys: ServerTrustPolicy.publicKeys(),
                validateCertificateChain: true,
                validateHost: true)

        // With out SSL Pining
         // let serverTrustPolicy = ServerTrustPolicy.disableEvaluation

        let serverTrustPolicies = [baseURL?.host ?? "": serverTrustPolicy]
        let serverTrustPolicyManager = ServerTrustPolicyManager(policies: serverTrustPolicies)

        // Configure session manager with trust policy
        let instance = MBAPIClient (
            baseURL: baseURL!,
            configuration: configuration,
            serverTrustPolicyManager: serverTrustPolicyManager
        )

        // SessionManager instance.
        return instance
    }()

    // MARK: -  < APIs WITHOUT TOKEN START >

    // MARK: - verifyAppVersion Request

    func verifyAppVersion(_ completionBlock: @escaping MBAPIClientCompletionHandler) -> Request {

        let serviceName = "generalservices/verifyappversion"

//        { "appversion": "0.8.0",
//            "msisdn":"556061015",
//            "cause":"sign up" }

        var headers : [String : String] = kRequestHeaders
        headers.updateValue(MBLanguageManager.userSelectedLanguage().rawValue, forKey: "lang")

        let params = ["appversion": String.appVersion()]

        return sendRequest(serviceName, parameters: params as [String : AnyObject] , isPostRequest: true, headers: headers, completionBlock: completionBlock)
    }

    // MARK: - authenticateUser Request
    func authenticateUser(MSISDN msisdn: String, Password password: String, _ completionBlock: @escaping MBAPIClientCompletionHandler) -> Request {

        let serviceName = "customerservices/authenticateuser"
        let params : [String : String] = ["msisdn": msisdn , "password" : password]

        var headers : [String : String] = kRequestHeaders
        headers.updateValue(MBLanguageManager.userSelectedLanguage().rawValue, forKey: "lang")

        //            ["deviceID": UIDevice.current.identifierForVendor!.uuidString, "Content-Type": "application/json", "lang":"3", "UserAgent": "iPhone"]
        //        { "msisdn":"557514843","password":"12345"}

        return sendRequest(serviceName, parameters: params as [String : AnyObject], isPostRequest: true, headers: headers, completionBlock: completionBlock)
    }

    // MARK: - SignUp Request
    func signUp(MSISDN msisdn: String, requestForSignUp isSignUpRequest: Bool, _ completionBlock: @escaping MBAPIClientCompletionHandler) -> Request {

        let serviceName = "customerservices/signup"

        var params : [String : String] = ["msisdn" : msisdn] //, "cause" : cause

        if isSignUpRequest {
            params.updateValue("signup", forKey: "cause")
        } else {
            params.updateValue("forgotpassword", forKey: "cause")
        }

        var headers : [String : String] = kRequestHeaders
        headers.updateValue(MBLanguageManager.userSelectedLanguage().rawValue, forKey: "lang")

        return sendRequest(serviceName, parameters: params as [String : AnyObject], isPostRequest: true, headers: headers, completionBlock: completionBlock)
    }

    // MARK: - verifyOTP Request
    func verifyOTP(MSISDN msisdn: String, OTP otp: String, requestForSignUp isSignUpRequest: Bool, _ completionBlock: @escaping MBAPIClientCompletionHandler) -> Request {

        let serviceName = "customerservices/verifyotp"

        var params : [String : String] = ["msisdn" : msisdn, "pin" : otp] //, "cause" : cause

        if isSignUpRequest {
            params.updateValue("signup", forKey: "cause")
        } else {
            params.updateValue("forgotpassword", forKey: "cause")
        }

        var headers : [String : String] = kRequestHeaders
        headers.updateValue(MBLanguageManager.userSelectedLanguage().rawValue, forKey: "lang")

        return sendRequest(serviceName, parameters: params as [String : AnyObject], isPostRequest: true, headers: headers, completionBlock: completionBlock)
    }

    // MARK: - saveCustomer Request
    func saveCustomer(MSISDN msisdn : String, Password password: String, ConfirmPassword confirmPassword: String, isAgrredOnTermsAndConditions isAgrred: Bool, OTP pin: String, _ completionBlock: @escaping MBAPIClientCompletionHandler) -> Request {

        let serviceName = "customerservices/savecustomer"

        var params : [String : Any] = ["password" : password, "confirm_password" : confirmPassword, "msisdn": msisdn, "temp": pin]

        if isAgrred {
            params.updateValue("1", forKey: "terms_and_conditions")
        } else {
            params.updateValue("0", forKey: "terms_and_conditions")
        }

        var headers : [String : String] = kRequestHeaders
        headers.updateValue(MBLanguageManager.userSelectedLanguage().rawValue, forKey: "lang")

        return sendRequest(serviceName, parameters: params as [String : AnyObject], isPostRequest: true, headers: headers, completionBlock: completionBlock)
    }

    // MARK: - forgotPassword Request
    func forgotPassword(MSISDN msisdn: String, Password password: String, ConfirmPassword confirmPassword: String, OTP pin: String, _ completionBlock: @escaping MBAPIClientCompletionHandler) -> Request {

        let serviceName = "customerservices/forgotpassword"

        //{"msisdn":"557514843","password":"1234","confirmPassword":"123"}

        let params : [String : String] = ["msisdn":msisdn,"password":password, "confirmPassword":confirmPassword, "temp": pin]

        var headers : [String : String] = kRequestHeaders
        headers.updateValue(MBLanguageManager.userSelectedLanguage().rawValue, forKey: "lang")

        return sendRequest(serviceName, parameters: params as [String : AnyObject], isPostRequest: true, headers: headers, completionBlock: completionBlock)
    }

    // MARK: - ReSendPin Request
    func reSendPin(MSISDN msisdn: String, ofType: Constants.MBResendType, _ completionBlock: @escaping MBAPIClientCompletionHandler) -> Request {

        let serviceName = "customerservices/resendpin"

        var params : [String : String] = ["msisdn" : msisdn] //, "cause" : cause

        params.updateValue(ofType.rawValue, forKey: "cause")

        var headers : [String : String] = kRequestHeaders
        headers.updateValue(MBLanguageManager.userSelectedLanguage().rawValue, forKey: "lang")

        return sendRequest(serviceName, parameters: params as [String : AnyObject], isPostRequest: true, headers: headers, completionBlock: completionBlock)
    }


    // MARK: - HomePage Request
    func homePage(BrandID brandId: String,SubscriberType subscriberType: String, offeringId:String,  _ completionBlock: @escaping MBAPIClientCompletionHandler) -> Request {

        let serviceName = "homepageservices/gethomepage"

        //  {"brandId": "1770078090","customerType":"Individual Customer"}

        let params: [String : String] = ["brandId" : brandId,
                                         "subscriberType" : subscriberType,
                                         "offeringId": offeringId,
                                         "customerType" : subscriberType]

        var headers : [String : String] = kRequestHeaders
        headers.updateValue(MBLanguageManager.userSelectedLanguage().rawValue, forKey: "lang")
        headers.updateValue(MBUserSession.shared.token, forKey: "token")
        headers.updateValue(MBUserSession.shared.msisdn, forKey: "msisdn")
        headers.updateValue(MBUserSession.shared.subscriberTypeLowerCaseStr, forKey: "subscriberType")
        headers.updateValue(MBUserSession.shared.brandNameLowerCaseStr, forKey: "tariffType")

        return sendRequest(serviceName, parameters: params as [String : AnyObject], isPostRequest: true, headers: headers, completionBlock: completionBlock)
    }

    // MARK: - getAppMenu Request
    func getAppMenu(_ completionBlock: @escaping MBAPIClientCompletionHandler) -> Request {

        let serviceName = "menus/getappmenu"

        var headers : [String : String] = kRequestHeaders

        headers.updateValue(MBLanguageManager.userSelectedLanguage().rawValue, forKey: "lang")
        headers.updateValue(MBUserSession.shared.token, forKey: "token")
        headers.updateValue(MBUserSession.shared.msisdn, forKey: "msisdn")
        headers.updateValue(MBUserSession.shared.subscriberTypeLowerCaseStr, forKey: "subscriberType")
        headers.updateValue(MBUserSession.shared.brandNameLowerCaseStr, forKey: "tariffType")

        return sendRequest(serviceName, parameters: nil, isPostRequest: true, headers: headers, completionBlock: completionBlock)
    }

    // MARK: - getFAQs Request
    func getFAQs(_ completionBlock: @escaping MBAPIClientCompletionHandler) -> Request {

        let serviceName = "generalservices/getfaqs"

        var headers : [String : String] = kRequestHeaders
        headers.updateValue(MBLanguageManager.userSelectedLanguage().rawValue, forKey: "lang")
        headers.updateValue(MBUserSession.shared.token, forKey: "token")
        headers.updateValue(MBUserSession.shared.msisdn, forKey: "msisdn")
        headers.updateValue(MBUserSession.shared.subscriberTypeLowerCaseStr, forKey: "subscriberType")
        headers.updateValue(MBUserSession.shared.brandNameLowerCaseStr, forKey: "tariffType")

        return sendRequest(serviceName, parameters: nil, isPostRequest: true, headers: headers, completionBlock: completionBlock)
    }


    // MARK: - ReSendPin Request
    func historyReSendPIN(MSISDN msisdn: String, ofType: Constants.MBResendType, _ completionBlock: @escaping MBAPIClientCompletionHandler) -> Request {

        let serviceName = "history/historyresendpin"

        let params : [String : String] = ["cause" : ofType.rawValue] //, "cause" : cause

        var headers : [String : String] = kRequestHeaders
        headers.updateValue(MBLanguageManager.userSelectedLanguage().rawValue, forKey: "lang")
        headers.updateValue(MBUserSession.shared.token, forKey: "token")
        headers.updateValue(MBUserSession.shared.msisdn, forKey: "msisdn")
        headers.updateValue(MBUserSession.shared.subscriberTypeLowerCaseStr, forKey: "subscriberType")
        headers.updateValue(MBUserSession.shared.brandNameLowerCaseStr, forKey: "tariffType")

        return sendRequest(serviceName, parameters: params as [String : AnyObject], isPostRequest: true, headers: headers, completionBlock: completionBlock)
    }

    // MARK: - changePassword Request
    func changePassword(OldPassword oldPassword: String, NewPassword newPassword: String, ConfirmNewPassword confirmNewPassword: String,_ completionBlock: @escaping MBAPIClientCompletionHandler) -> Request {

        let serviceName = "customerservices/changepassword"

        //            {"oldPassword":"1234", "newPassword":"12345", "confirmNewPassword":"12345" }

        let params : [String : String] = ["oldPassword":oldPassword, "newPassword":newPassword, "confirmNewPassword":confirmNewPassword]

        var headers : [String : String] = kRequestHeaders
        headers.updateValue(MBLanguageManager.userSelectedLanguage().rawValue, forKey: "lang")
        headers.updateValue(MBUserSession.shared.token, forKey: "token")
        headers.updateValue(MBUserSession.shared.msisdn, forKey: "msisdn")
        headers.updateValue(MBUserSession.shared.subscriberTypeLowerCaseStr, forKey: "subscriberType")
        headers.updateValue(MBUserSession.shared.brandNameLowerCaseStr, forKey: "tariffType")

        return sendRequest(serviceName, parameters: params as [String : AnyObject], isPostRequest: true, headers: headers, completionBlock: completionBlock)
    }



    // MARK: - getLoan Request
    func getLoan(LoanAmount loanAmount: String,_ completionBlock: @escaping MBAPIClientCompletionHandler) -> Request {

        let serviceName = "financialservices/getloan"

        //{ "loanAmount":"5"}

        let params : [String : String] = ["loanAmount":loanAmount, "brandId": MBUserSession.shared.userInfo?.brandId ?? ""]

        var headers : [String : String] = kRequestHeaders
        headers.updateValue(MBLanguageManager.userSelectedLanguage().rawValue, forKey: "lang")
        headers.updateValue(MBUserSession.shared.token, forKey: "token")
        headers.updateValue(MBUserSession.shared.msisdn, forKey: "msisdn")
        headers.updateValue(MBUserSession.shared.subscriberTypeLowerCaseStr, forKey: "subscriberType")
        headers.updateValue(MBUserSession.shared.brandNameLowerCaseStr, forKey: "tariffType")

        return sendRequest(serviceName, parameters: params as [String : AnyObject], isPostRequest: true, headers: headers, completionBlock: completionBlock)
    }

    // MARK: - getLoanHistory Request
    func getLoanHistory(StartDate startDate: String, EndDate endDate: String,_ completionBlock: @escaping MBAPIClientCompletionHandler) -> Request {

        let serviceName = "financialservices/getloanhistory"

        //{"startDate":"2015-12-06","endDate":"2017-09-25"}

        let params : [String : String] = ["startDate":startDate, "endDate":endDate]

        var headers : [String : String] = kRequestHeaders
        headers.updateValue(MBLanguageManager.userSelectedLanguage().rawValue, forKey: "lang")
        headers.updateValue(MBUserSession.shared.token, forKey: "token")
        headers.updateValue(MBUserSession.shared.msisdn, forKey: "msisdn")
        headers.updateValue(MBUserSession.shared.subscriberTypeLowerCaseStr, forKey: "subscriberType")
        headers.updateValue(MBUserSession.shared.brandNameLowerCaseStr, forKey: "tariffType")

        return sendRequest(serviceName, parameters: params as [String : AnyObject], isPostRequest: true, headers: headers, completionBlock: completionBlock)
    }

    // MARK: - getPaymentHistory Request
    func getPaymentHistory(StartDate startDate: String, EndDate endDate: String,_ completionBlock: @escaping MBAPIClientCompletionHandler) -> Request {

        let serviceName = "financialservices/getpaymenthistory"

        //{"startDate":"2015-12-06","endDate":"2017-09-25"}

        let params : [String : String] = ["startDate":startDate, "endDate":endDate]

        var headers : [String : String] = kRequestHeaders
        headers.updateValue(MBLanguageManager.userSelectedLanguage().rawValue, forKey: "lang")
        headers.updateValue(MBUserSession.shared.token, forKey: "token")
        headers.updateValue(MBUserSession.shared.msisdn, forKey: "msisdn")
        headers.updateValue(MBUserSession.shared.subscriberTypeLowerCaseStr, forKey: "subscriberType")
        headers.updateValue(MBUserSession.shared.brandNameLowerCaseStr, forKey: "tariffType")

        return sendRequest(serviceName, parameters: params as [String : AnyObject], isPostRequest: true, headers: headers, completionBlock: completionBlock)
    }

    // MARK: - getUsageSummaryHistory Request
    func getUsageSummaryHistory(StartDate startDate: String, EndDate endDate: String,_ completionBlock: @escaping MBAPIClientCompletionHandler) -> Request {

        let serviceName = "history/getusagesummary"

//{"startDate":"2016-12-06","endDate":"2017-08-24", "accountId":"1010000134403", "customerId":"123331231231"}
        let params : [String : String] = ["startDate":startDate, "endDate":endDate,"accountId":MBUserSession.shared.userInfo?.accountId ?? "","customerId":MBUserSession.shared.userInfo?.customerId ?? ""]

        var headers : [String : String] = kRequestHeaders
        headers.updateValue(MBLanguageManager.userSelectedLanguage().rawValue, forKey: "lang")
        headers.updateValue(MBUserSession.shared.token, forKey: "token")
        headers.updateValue(MBUserSession.shared.msisdn, forKey: "msisdn")
        headers.updateValue(MBUserSession.shared.subscriberTypeLowerCaseStr, forKey: "subscriberType")
        headers.updateValue(MBUserSession.shared.brandNameLowerCaseStr, forKey: "tariffType")

        return sendRequest(serviceName, parameters: params as [String : AnyObject], isPostRequest: true, headers: headers, completionBlock: completionBlock)
    }

    // MARK: - getUsageDetailsHistory Request
    func getUsageDetailsHistory(StartDate startDate: String, EndDate endDate: String,_ completionBlock: @escaping MBAPIClientCompletionHandler) -> Request {

        let serviceName = "history/getusagedetails"

        //{"startDate":"2016-12-06","endDate":"2017-08-24", "accountId":"1010000134403", "customerId":"123331231231"}


        let params : [String : String] = ["startDate":startDate, "endDate":endDate, "accountId":MBUserSession.shared.userInfo?.accountId ?? "","customerId":MBUserSession.shared.userInfo?.customerId ?? ""]

        var headers : [String : String] = kRequestHeaders
        headers.updateValue(MBLanguageManager.userSelectedLanguage().rawValue, forKey: "lang")
        headers.updateValue(MBUserSession.shared.token, forKey: "token")
        headers.updateValue(MBUserSession.shared.msisdn, forKey: "msisdn")
        headers.updateValue(MBUserSession.shared.subscriberTypeLowerCaseStr, forKey: "subscriberType")
        headers.updateValue(MBUserSession.shared.brandNameLowerCaseStr, forKey: "tariffType")

        return sendRequest(serviceName, parameters: params as [String : AnyObject], isPostRequest: true, headers: headers, completionBlock: completionBlock)
    }

    // MARK: - verifyAccountDetails Request
    func verifyAccountDetails(PassportNumber passportNumber: String, _ completionBlock: @escaping MBAPIClientCompletionHandler) -> Request {

        let serviceName = "history/verifyaccountdetails"

        //{"accountId":"1010000100572","customerId":"1010000100572","passportNumber":"5464645"}

        let params : [String : String] = ["passportNumber":passportNumber, "accountId":MBUserSession.shared.userInfo?.accountId ?? "", "customerId":MBUserSession.shared.userInfo?.customerId ?? ""]

        var headers : [String : String] = kRequestHeaders
        headers.updateValue(MBLanguageManager.userSelectedLanguage().rawValue, forKey: "lang")
        headers.updateValue(MBUserSession.shared.token, forKey: "token")
        headers.updateValue(MBUserSession.shared.msisdn, forKey: "msisdn")
        headers.updateValue(MBUserSession.shared.subscriberTypeLowerCaseStr, forKey: "subscriberType")
        headers.updateValue(MBUserSession.shared.brandNameLowerCaseStr, forKey: "tariffType")

        return sendRequest(serviceName, parameters: params as [String : AnyObject], isPostRequest: true, headers: headers, completionBlock: completionBlock)
    }

    // MARK: - verifypin Request
    func verifyPin(Pin pin: String,_ completionBlock: @escaping MBAPIClientCompletionHandler) -> Request {

        let serviceName = "history/verifypin"

        //{"accountId":"1010000127078","pin":"6430", "customerId":"123331231231"}


        let params : [String : String] = ["accountId":MBUserSession.shared.userInfo?.accountId ?? "", "pin":pin, "customerId":MBUserSession.shared.userInfo?.customerId ?? ""]

        var headers : [String : String] = kRequestHeaders
        headers.updateValue(MBLanguageManager.userSelectedLanguage().rawValue, forKey: "lang")
        headers.updateValue(MBUserSession.shared.token, forKey: "token")
        headers.updateValue(MBUserSession.shared.msisdn, forKey: "msisdn")
        headers.updateValue(MBUserSession.shared.subscriberTypeLowerCaseStr, forKey: "subscriberType")
        headers.updateValue(MBUserSession.shared.brandNameLowerCaseStr, forKey: "tariffType")

        return sendRequest(serviceName, parameters: params as [String : AnyObject], isPostRequest: true, headers: headers, completionBlock: completionBlock)
    }

    // MARK: - getsupplementaryofferings Request
    func getSupplementaryOfferings(_ completionBlock: @escaping MBAPIClientCompletionHandler) -> Request {

        let serviceName = "supplementaryofferings/getsupplementaryofferings"

        var headers : [String : String] = kRequestHeaders
        headers.updateValue(MBLanguageManager.userSelectedLanguage().rawValue, forKey: "lang")
        headers.updateValue(MBUserSession.shared.token, forKey: "token")
        headers.updateValue(MBUserSession.shared.msisdn, forKey: "msisdn")
        headers.updateValue(MBUserSession.shared.subscriberTypeLowerCaseStr, forKey: "subscriberType")
        headers.updateValue(MBUserSession.shared.brandNameLowerCaseStr, forKey: "tariffType")

        let params = ["offeringName": MBUserSession.shared.userInfo?.offeringName ?? "", "brandName": MBUserSession.shared.userInfo?.brandName ?? ""]
        
        return sendRequest(serviceName, parameters: params as [String : AnyObject]? , isPostRequest: true, headers: headers, completionBlock: completionBlock)
    }

    // MARK: - getTariffDetails Request
    func getTariffDetails(SubscriberType subscriberType :String, offeringId:String, CustomerType customerType: String,  SubscribedOfferingName subscribedOfferingName: String, StoreId storeId: String,_ completionBlock: @escaping MBAPIClientCompletionHandler) -> Request {

        let serviceName = "tariffservices/gettariffdetails"

        var headers : [String : String] = kRequestHeaders
        headers.updateValue(MBLanguageManager.userSelectedLanguage().rawValue, forKey: "lang")
        headers.updateValue(MBUserSession.shared.token, forKey: "token")
        headers.updateValue(MBUserSession.shared.msisdn, forKey: "msisdn")
        headers.updateValue(MBUserSession.shared.subscriberTypeLowerCaseStr, forKey: "subscriberType")
        headers.updateValue(MBUserSession.shared.brandNameLowerCaseStr, forKey: "tariffType")
//customerType
        let params : [String : String] = ["subscriberType":subscriberType,
                                          "offeringId":offeringId,
                                          "customerType":customerType,
                                          "subscribedOfferingName":subscribedOfferingName,
                                          "storeId":storeId]

        return sendRequest(serviceName, parameters: params as [String : AnyObject]? , isPostRequest: true, headers: headers, completionBlock: completionBlock)
    }

    
    
    
    //MARK: -  Fast Payments API Sections
    
    /**
     - "getfastpayments" Api to get AutoPayment scheduler.
     */
    
    func getFastPayments(_ completionBlock: @escaping MBAPIClientCompletionHandler) -> Request {

        let serviceName = "plasticcard/getfastpayments"

        var headers : [String : String] = kRequestHeaders
        headers.updateValue(MBLanguageManager.userSelectedLanguage().rawValue, forKey: "lang")
        headers.updateValue(MBUserSession.shared.token, forKey: "token")
        headers.updateValue(MBUserSession.shared.msisdn, forKey: "msisdn")
        headers.updateValue(MBUserSession.shared.subscriberTypeLowerCaseStr, forKey: "subscriberType")
        headers.updateValue(MBUserSession.shared.brandNameLowerCaseStr, forKey: "tariffType")

        let params : [String : String] = ["":""]

        return sendRequest(serviceName, parameters: params as [String : AnyObject]? , isPostRequest: true, headers: headers, completionBlock: completionBlock)
    }
    
    /**
     "makepayment" Api to get AutoPayment scheduler.
     
     parameters
         - paymentKey
         - amount
         - topupNumber // pphone number
     */
    
    func makepayment(PaymentKey paymentKey : String, Amount amount: String, TopupNumber topupNumber: String ,_ completionBlock: @escaping MBAPIClientCompletionHandler) -> Request {

        let serviceName = "plasticcard/makepayment"

        var headers : [String : String] = kRequestHeaders
        headers.updateValue(MBLanguageManager.userSelectedLanguage().rawValue, forKey: "lang")
        headers.updateValue(MBUserSession.shared.token, forKey: "token")
        headers.updateValue(MBUserSession.shared.msisdn, forKey: "msisdn")
        headers.updateValue(MBUserSession.shared.subscriberTypeLowerCaseStr, forKey: "subscriberType")
        headers.updateValue(MBUserSession.shared.brandNameLowerCaseStr, forKey: "tariffType")

        let params : [String : String] = [
            "paymentKey": paymentKey,
            "amount": amount,
            "topupNumber": topupNumber
            
        ]

        return sendRequest(serviceName, parameters: params as [String : AnyObject]? , isPostRequest: true, headers: headers, completionBlock: completionBlock)
    }
    
    
    /**
     "deletepayment" Api to get AutoPayment scheduler.
     
     parameters
        -fastPaymentId
    
     */
    
    func deletePayment(fastPaymentId: String, _ completionBlock: @escaping MBAPIClientCompletionHandler) -> Request {

        let serviceName = "plasticcard/deletepayment"

        var headers : [String : String] = kRequestHeaders
        headers.updateValue(MBLanguageManager.userSelectedLanguage().rawValue, forKey: "lang")
        headers.updateValue(MBUserSession.shared.token, forKey: "token")
        headers.updateValue(MBUserSession.shared.msisdn, forKey: "msisdn")
        headers.updateValue(MBUserSession.shared.subscriberTypeLowerCaseStr, forKey: "subscriberType")
        headers.updateValue(MBUserSession.shared.brandNameLowerCaseStr, forKey: "tariffType")

        let params : [String : String] = ["id" : fastPaymentId]

        return sendRequest(serviceName, parameters: params as [String : AnyObject]? , isPostRequest: true, headers: headers, completionBlock: completionBlock)
    }
    
    
    
    
    
    
    
    //MARK: -  AutoPayments API Sections
    
    /**
     - "getscheduledpayments" Api to get AutoPayment scheduler.
     */
    
    func getScheduledPayments(_ completionBlock: @escaping MBAPIClientCompletionHandler) -> Request {

        let serviceName = "plasticcard/getscheduledpayments"

        var headers : [String : String] = kRequestHeaders
        headers.updateValue(MBLanguageManager.userSelectedLanguage().rawValue, forKey: "lang")
        headers.updateValue(MBUserSession.shared.token, forKey: "token")
        headers.updateValue(MBUserSession.shared.msisdn, forKey: "msisdn")
        headers.updateValue(MBUserSession.shared.subscriberTypeLowerCaseStr, forKey: "subscriberType")
        headers.updateValue(MBUserSession.shared.brandNameLowerCaseStr, forKey: "tariffType")

        let params : [String : String] = ["":""]

        return sendRequest(serviceName, parameters: params as [String : AnyObject]? , isPostRequest: true, headers: headers, completionBlock: completionBlock)
    }
    
    
    /**
    "addpaymentscheduler" Api to get AutoPayment scheduler.
     parameters
         - amount
         - billingCycle  // 1.daily, 2. weekly, 3. monthly
         - startDate
         - recurrenceNumber
         - savedCardId
     */
    
    func addPaymentScheduler(amount: String, billingCycle: String, startDate: String,  recurrenceNumber: String, savedCardId: String, _ completionBlock: @escaping MBAPIClientCompletionHandler) -> Request {

        let serviceName = "plasticcard/addpaymentscheduler"

        var headers : [String : String] = kRequestHeaders
        headers.updateValue(MBLanguageManager.userSelectedLanguage().rawValue, forKey: "lang")
        headers.updateValue(MBUserSession.shared.token, forKey: "token")
        headers.updateValue(MBUserSession.shared.msisdn, forKey: "msisdn")
        headers.updateValue(MBUserSession.shared.subscriberTypeLowerCaseStr, forKey: "subscriberType")
        headers.updateValue(MBUserSession.shared.brandNameLowerCaseStr, forKey: "tariffType")

        let params : [String : String] = [
            "amount": amount,
            "billingCycle": billingCycle,  // 1.daily, 2. weekly, 3. monthly
            "startDate": startDate,
            "recurrenceNumber": recurrenceNumber,
            "savedCardId": savedCardId,
            "recurrenceFrequency" : "1"
        ]

        return sendRequest(serviceName, parameters: params as [String : AnyObject]? , isPostRequest: true, headers: headers, completionBlock: completionBlock)
    }
    
    
    /**
     - "deletepaymentscheduler" Api to get AutoPayment scheduler.
     parameters
        - paymentSchedulerId
     */
    
    func deletePaymentScheduler(paymentSchedulerId: String, _ completionBlock: @escaping MBAPIClientCompletionHandler) -> Request {

        let serviceName = "plasticcard/deletepaymentscheduler"

        var headers : [String : String] = kRequestHeaders
        headers.updateValue(MBLanguageManager.userSelectedLanguage().rawValue, forKey: "lang")
        headers.updateValue(MBUserSession.shared.token, forKey: "token")
        headers.updateValue(MBUserSession.shared.msisdn, forKey: "msisdn")
        headers.updateValue(MBUserSession.shared.subscriberTypeLowerCaseStr, forKey: "subscriberType")
        headers.updateValue(MBUserSession.shared.brandNameLowerCaseStr, forKey: "tariffType")

        let params : [String : String] = ["id" : paymentSchedulerId]

        return sendRequest(serviceName, parameters: params as [String : AnyObject]? , isPostRequest: true, headers: headers, completionBlock: completionBlock)
    }
    
    
    
    
    //MARK: getSavedCards
    func getSavedCards(_ completionBlock: @escaping MBAPIClientCompletionHandler) -> Request {

        let serviceName = "plasticcard/getsavedcards"

        var headers : [String : String] = kRequestHeaders
        headers.updateValue(MBLanguageManager.userSelectedLanguage().rawValue, forKey: "lang")
        headers.updateValue(MBUserSession.shared.token, forKey: "token")
        headers.updateValue(MBUserSession.shared.msisdn, forKey: "msisdn")
        headers.updateValue(MBUserSession.shared.subscriberTypeLowerCaseStr, forKey: "subscriberType")
        headers.updateValue(MBUserSession.shared.brandNameLowerCaseStr, forKey: "tariffType")

        let params : [String : String] = ["":""]

        return sendRequest(serviceName, parameters: params as [String : AnyObject]? , isPostRequest: true, headers: headers, completionBlock: completionBlock)
    }
    
    
    //MARK: - deleteSavedCard
    func deleteSavedcard(savedCardId: String, _ completionBlock: @escaping MBAPIClientCompletionHandler) -> Request {

        let serviceName = "plasticcard/deletesavedcard"

        var headers : [String : String] = kRequestHeaders
        headers.updateValue(MBLanguageManager.userSelectedLanguage().rawValue, forKey: "lang")
        headers.updateValue(MBUserSession.shared.token, forKey: "token")
        headers.updateValue(MBUserSession.shared.msisdn, forKey: "msisdn")
        headers.updateValue(MBUserSession.shared.subscriberTypeLowerCaseStr, forKey: "subscriberType")
        headers.updateValue(MBUserSession.shared.brandNameLowerCaseStr, forKey: "tariffType")

        let params : [String : String] = ["id" : savedCardId]

        return sendRequest(serviceName, parameters: params as [String : AnyObject]? , isPostRequest: true, headers: headers, completionBlock: completionBlock)
    }
    
    
    
    //MARK: initiatePayment
    func initiatePayment(CardType cardType : String, Amount amount: String,IsSaved isSaved: Bool, TopUpNumber topUpNumber: String ,_ completionBlock: @escaping MBAPIClientCompletionHandler) -> Request {
 
        let serviceName = "plasticcard/initiatepayment"

        var headers : [String : String] = kRequestHeaders
        headers.updateValue(MBLanguageManager.userSelectedLanguage().rawValue, forKey: "lang")
        headers.updateValue(MBUserSession.shared.token, forKey: "token")
        headers.updateValue(MBUserSession.shared.msisdn, forKey: "msisdn")
        headers.updateValue(MBUserSession.shared.subscriberTypeLowerCaseStr, forKey: "subscriberType")
        headers.updateValue(MBUserSession.shared.brandNameLowerCaseStr, forKey: "tariffType")

        let params : [String : String] = [
            "cardType": cardType,
            "amount": amount,
            "saved": String(isSaved) ,
            "topupNumber": topUpNumber,
            
        ]

        return sendRequest(serviceName, parameters: params as [String : AnyObject]? , isPostRequest: true, headers: headers, completionBlock: completionBlock)
    }
    

    // MARK: - getTopUp Request
    func getTopUp(SubscriberType subscriberType: String, CardPin cardPinNumber: String, TopupNumber topupnum : String, _ completionBlock: @escaping MBAPIClientCompletionHandler) -> Request {

        let serviceName = "financialservices/requesttopup"

        var headers : [String : String] = kRequestHeaders
        headers.updateValue(MBLanguageManager.userSelectedLanguage().rawValue, forKey: "lang")
        headers.updateValue(MBUserSession.shared.token, forKey: "token")
        headers.updateValue(MBUserSession.shared.msisdn, forKey: "msisdn")
        headers.updateValue(MBUserSession.shared.subscriberTypeLowerCaseStr, forKey: "subscriberType")
        headers.updateValue(MBUserSession.shared.brandNameLowerCaseStr, forKey: "tariffType")

        let params : [String : String] = ["subscriberType":subscriberType,"cardPinNumber":cardPinNumber, "topupnum": topupnum]


        return sendRequest(serviceName, parameters: params as [String : AnyObject]? , isPostRequest: true, headers: headers, completionBlock: completionBlock)
    }

    // MARK: - moneyTransfer Request
    func moneyTransfer(Transfer transferee: String, Amount amount: String, _ completionBlock: @escaping MBAPIClientCompletionHandler) -> Request {

        let serviceName = "financialservices/requestmoneytransfer"

        var headers : [String : String] = kRequestHeaders
        headers.updateValue(MBLanguageManager.userSelectedLanguage().rawValue, forKey: "lang")
        headers.updateValue(MBUserSession.shared.token, forKey: "token")
        headers.updateValue(MBUserSession.shared.msisdn, forKey: "msisdn")
        headers.updateValue(MBUserSession.shared.subscriberTypeLowerCaseStr, forKey: "subscriberType")
        headers.updateValue(MBUserSession.shared.brandNameLowerCaseStr, forKey: "tariffType")

        let params : [String : String] = ["transferee":transferee,"amount":amount]

        return sendRequest(serviceName, parameters: params as [String : AnyObject]? , isPostRequest: true, headers: headers, completionBlock: completionBlock)
    }

    // MARK: - getOperationsHistory Request
    func getOperationsHistory(StartDate startDate: String, EndDate endDate: String, _ completionBlock: @escaping MBAPIClientCompletionHandler) -> Request {

        let serviceName = "history/getoperationshistory"

        var headers : [String : String] = kRequestHeaders
        headers.updateValue(MBLanguageManager.userSelectedLanguage().rawValue, forKey: "lang")
        headers.updateValue(MBUserSession.shared.token, forKey: "token")
        headers.updateValue(MBUserSession.shared.msisdn, forKey: "msisdn")
        headers.updateValue(MBUserSession.shared.subscriberTypeLowerCaseStr, forKey: "subscriberType")
        headers.updateValue(MBUserSession.shared.brandNameLowerCaseStr, forKey: "tariffType")

//        {"startDate":"2016-12-06","endDate":"2017-08-24", "accountId":"1010000134403", "customerId":"123331231231"}
        let params : [String : String] = ["startDate":startDate,"endDate":endDate, "accountId":MBUserSession.shared.userInfo?.accountId ?? "", "customerId":MBUserSession.shared.userInfo?.customerId ?? ""]

        return sendRequest(serviceName, parameters: params as [String : AnyObject]? , isPostRequest: true, headers: headers, completionBlock: completionBlock)
    }


    // MARK: - updateCustomerEmail Request

    func updateCustomerEmail(NewEmail newEmail: String, _ completionBlock: @escaping MBAPIClientCompletionHandler) -> Request {

        let serviceName = "customerservices/updatecustomeremail"

        var headers : [String : String] = kRequestHeaders
        headers.updateValue(MBLanguageManager.userSelectedLanguage().rawValue, forKey: "lang")
        headers.updateValue(MBUserSession.shared.token, forKey: "token")
        headers.updateValue(MBUserSession.shared.msisdn, forKey: "msisdn")
        headers.updateValue(MBUserSession.shared.subscriberTypeLowerCaseStr, forKey: "subscriberType")
        headers.updateValue(MBUserSession.shared.brandNameLowerCaseStr, forKey: "tariffType")

        let params : [String : String] = ["email":newEmail]

        return sendRequest(serviceName, parameters: params as [String : AnyObject]? , isPostRequest: true, headers: headers, completionBlock: completionBlock)
    }

    // MARK: - updateCustomerEmail Request

    func getSubscriptions(_ completionBlock: @escaping MBAPIClientCompletionHandler) -> Request {

        let serviceName = "mysubscriptions/getsubscriptions"

        var headers : [String : String] = kRequestHeaders
        headers.updateValue(MBLanguageManager.userSelectedLanguage().rawValue, forKey: "lang")
        headers.updateValue(MBUserSession.shared.token, forKey: "token")
        headers.updateValue(MBUserSession.shared.msisdn, forKey: "msisdn")
        headers.updateValue(MBUserSession.shared.subscriberTypeLowerCaseStr, forKey: "subscriberType")
        headers.updateValue(MBUserSession.shared.brandNameLowerCaseStr, forKey: "tariffType")

        let params = ["offeringName": MBUserSession.shared.userInfo?.offeringName ?? "", "brandName": MBUserSession.shared.userInfo?.brandName ?? ""]
        return sendRequest(serviceName, parameters: params as [String : AnyObject] , isPostRequest: true, headers: headers, completionBlock: completionBlock)
    }


    // MARK: - getfnf Request

    func getFNF(_ completionBlock: @escaping MBAPIClientCompletionHandler) -> Request {

        //        subscriberType:prepaid
        //        tariffType: prepaid

        let serviceName = "fnf/getfnf"

        var headers : [String : String] = kRequestHeaders
        headers.updateValue(MBLanguageManager.userSelectedLanguage().rawValue, forKey: "lang")
        headers.updateValue(MBUserSession.shared.token, forKey: "token")
        headers.updateValue(MBUserSession.shared.msisdn, forKey: "msisdn")
        headers.updateValue(MBUserSession.shared.subscriberTypeLowerCaseStr, forKey: "subscriberType")
        headers.updateValue(MBUserSession.shared.brandNameLowerCaseStr, forKey: "tariffType")

        let params = ["offeringId": MBUserSession.shared.userInfo?.offeringId ?? ""]

        return sendRequest(serviceName, parameters: params as [String : AnyObject], isPostRequest: true, headers: headers, completionBlock: completionBlock)
    }

    // MARK: - addFNF Request

    func addFNF(MSISDN msisdn: String,_ completionBlock: @escaping MBAPIClientCompletionHandler) -> Request {

        //        {"addMsisdn":"557514334"}


        let serviceName = "fnf/addfnf"

        var headers : [String : String] = kRequestHeaders
        headers.updateValue(MBLanguageManager.userSelectedLanguage().rawValue, forKey: "lang")
        headers.updateValue(MBUserSession.shared.token, forKey: "token")
        headers.updateValue(MBUserSession.shared.msisdn, forKey: "msisdn")
        headers.updateValue(MBUserSession.shared.subscriberTypeLowerCaseStr, forKey: "subscriberType")
        headers.updateValue(MBUserSession.shared.brandNameLowerCaseStr, forKey: "tariffType")

        let params : [String : String] = ["addMsisdn": msisdn, "offeringId": MBUserSession.shared.userInfo?.offeringId ?? ""]

        return sendRequest(serviceName, parameters: params as [String : AnyObject], isPostRequest: true, headers: headers, completionBlock: completionBlock)
    }

    // MARK: - deleteFNF Request
    func deleteFNF(MSISDN msisdn: String,_ completionBlock: @escaping MBAPIClientCompletionHandler) -> Request {

        //        {"deleteMsisdn":"557514334"}


        let serviceName = "fnf/deletefnf"

        var headers : [String : String] = kRequestHeaders
        headers.updateValue(MBLanguageManager.userSelectedLanguage().rawValue, forKey: "lang")
        headers.updateValue(MBUserSession.shared.token, forKey: "token")
        headers.updateValue(MBUserSession.shared.msisdn, forKey: "msisdn")
        headers.updateValue(MBUserSession.shared.subscriberTypeLowerCaseStr, forKey: "subscriberType")
        headers.updateValue(MBUserSession.shared.brandNameLowerCaseStr, forKey: "tariffType")

        let params : [String : String] = ["deleteMsisdn": msisdn, "offeringId": MBUserSession.shared.userInfo?.offeringId ?? ""]

        return sendRequest(serviceName, parameters: params as [String : AnyObject], isPostRequest: true, headers: headers, completionBlock: completionBlock)
    }

    // MARK: - getContactUsDetails Request
    func getContactUsDetails(_ completionBlock: @escaping MBAPIClientCompletionHandler) -> Request {

        let serviceName = "generalservices/getcontactusdetails"

        var headers : [String : String] = kRequestHeaders
        headers.updateValue(MBLanguageManager.userSelectedLanguage().rawValue, forKey: "lang")
        headers.updateValue(MBUserSession.shared.token, forKey: "token")
        headers.updateValue(MBUserSession.shared.msisdn, forKey: "msisdn")
        headers.updateValue(MBUserSession.shared.subscriberTypeLowerCaseStr, forKey: "subscriberType")
        headers.updateValue(MBUserSession.shared.brandNameLowerCaseStr, forKey: "tariffType")

        return sendRequest(serviceName, parameters: nil, isPostRequest: true, headers: headers, completionBlock: completionBlock)
    }

    // MARK: - storeDetails Request

    func storeDetails(_ completionBlock: @escaping MBAPIClientCompletionHandler) -> Request {

        let serviceName = "generalservices/getstoresdetails"

        var headers : [String : String] = kRequestHeaders
        headers.updateValue(MBLanguageManager.userSelectedLanguage().rawValue, forKey: "lang")
        headers.updateValue(MBUserSession.shared.token, forKey: "token")
        headers.updateValue(MBUserSession.shared.msisdn, forKey: "msisdn")
        headers.updateValue(MBUserSession.shared.subscriberTypeLowerCaseStr, forKey: "subscriberType")
        headers.updateValue(MBUserSession.shared.brandNameLowerCaseStr, forKey: "tariffType")

        return sendRequest(serviceName, parameters: nil, isPostRequest: true, headers: headers, completionBlock: completionBlock)
    }
    
    // MARK: - getMerchants Request
    
    func getMerchants(_ completionBlock: @escaping MBAPIClientCompletionHandler) -> Request {
        
        let serviceName = "ulduzum/getmerchants"
        
        var headers : [String : String] = kRequestHeaders
        headers.updateValue(MBLanguageManager.userSelectedLanguage().rawValue, forKey: "lang")
        headers.updateValue(MBUserSession.shared.token, forKey: "token")
        headers.updateValue(MBUserSession.shared.msisdn, forKey: "msisdn")
        headers.updateValue(MBUserSession.shared.subscriberTypeLowerCaseStr, forKey: "subscriberType")
        headers.updateValue(MBUserSession.shared.brandNameLowerCaseStr, forKey: "tariffType")
        
        let params : [String : String] = ["iP":"127.0.0.1","loyaltySegment":MBUserSession.shared.userInfo?.loyaltySegment ?? ""]
        
        return sendRequest(serviceName, parameters: params as [String : AnyObject], isPostRequest: true, headers: headers, completionBlock: completionBlock)
    }
    
    func getBranches(merchantId: String, _ completionBlock: @escaping MBAPIClientCompletionHandler) -> Request {
        
        let serviceName = "ulduzum/getmerchantdetails"
        
        var headers : [String : String] = kRequestHeaders
        headers.updateValue(MBLanguageManager.userSelectedLanguage().rawValue, forKey: "lang")
        headers.updateValue(MBUserSession.shared.token, forKey: "token")
        headers.updateValue(MBUserSession.shared.msisdn, forKey: "msisdn")
        headers.updateValue(MBUserSession.shared.subscriberTypeLowerCaseStr, forKey: "subscriberType")
        headers.updateValue(MBUserSession.shared.brandNameLowerCaseStr, forKey: "tariffType")
        
        let params : [String : String] = ["merchantid":merchantId]
        
        return sendRequest(serviceName, parameters: params as [String : AnyObject], isPostRequest: true, headers: headers, completionBlock: completionBlock)
    }

    // MARK: - getFreeSMSStatus Request

    func getFreeSMSStatus(_ completionBlock: @escaping MBAPIClientCompletionHandler) -> Request {

        let serviceName = "quickservices/getfreesmsstatus"

        var headers : [String : String] = kRequestHeaders
        headers.updateValue(MBLanguageManager.userSelectedLanguage().rawValue, forKey: "lang")
        headers.updateValue(MBUserSession.shared.token, forKey: "token")
        headers.updateValue(MBUserSession.shared.msisdn, forKey: "msisdn")
        headers.updateValue(MBUserSession.shared.subscriberTypeLowerCaseStr, forKey: "subscriberType")
        headers.updateValue(MBUserSession.shared.brandNameLowerCaseStr, forKey: "tariffType")

        return sendRequest(serviceName, parameters: nil, isPostRequest: true, headers: headers, completionBlock: completionBlock)
    }

    // MARK: - sendFreeSMS Request

    func sendFreeSMS(recieverMsisdn: String, textMessage: String,_ completionBlock: @escaping MBAPIClientCompletionHandler) -> Request {

        let serviceName = "quickservices/sendfreesms"

        var headers : [String : String] = kRequestHeaders
        headers.updateValue(MBLanguageManager.userSelectedLanguage().rawValue, forKey: "lang")
        headers.updateValue(MBUserSession.shared.token, forKey: "token")
        headers.updateValue(MBUserSession.shared.msisdn, forKey: "msisdn")
        headers.updateValue(MBUserSession.shared.subscriberTypeLowerCaseStr, forKey: "subscriberType")
        headers.updateValue(MBUserSession.shared.brandNameLowerCaseStr, forKey: "tariffType")

        var params : [String : String] = ["recieverMsisdn": recieverMsisdn, "textmsg":textMessage.toBase64()]
        
        if textMessage.containsOtherThenAllowedCharactersForFreeSMSInEnglish()  == true {
            params.updateValue("NE", forKey: "msgLang")
        } else {
            params.updateValue("EN", forKey: "msgLang")
        }
        
        return sendRequest(serviceName, parameters: params as [String : AnyObject], isPostRequest: true, headers: headers, completionBlock: completionBlock)
    }

    // MARK: - getNotifications Request

    func getNotifications(_ completionBlock: @escaping MBAPIClientCompletionHandler) -> Request {

        let serviceName = "notifications/getnotifications"

        var headers : [String : String] = kRequestHeaders
        headers.updateValue(MBLanguageManager.userSelectedLanguage().rawValue, forKey: "lang")
        headers.updateValue(MBUserSession.shared.token, forKey: "token")
        headers.updateValue(MBUserSession.shared.msisdn, forKey: "msisdn")
        headers.updateValue(MBUserSession.shared.subscriberTypeLowerCaseStr, forKey: "subscriberType")
        headers.updateValue(MBUserSession.shared.brandNameLowerCaseStr, forKey: "tariffType")

        return sendRequest(serviceName, parameters: nil, isPostRequest: true, headers: headers, completionBlock: completionBlock)
    }

    // MARK: - uploadImage Request

    func uploadImage(image : UIImage?, isUploadImage : Bool = true, _ completionBlock: @escaping MBAPIClientCompletionHandler) -> Request {

        let serviceName = "generalservices/uploadimage"

        var headers : [String : String] = kRequestHeaders
        headers.updateValue(MBLanguageManager.userSelectedLanguage().rawValue, forKey: "lang")
        headers.updateValue(MBUserSession.shared.token, forKey: "token")
        headers.updateValue(MBUserSession.shared.msisdn, forKey: "msisdn")
        headers.updateValue(MBUserSession.shared.subscriberTypeLowerCaseStr, forKey: "subscriberType")
        headers.updateValue(MBUserSession.shared.brandNameLowerCaseStr, forKey: "tariffType")

        //        {"image":"<BASE64 Encoded String","ext":"jpeg"}


        //Use image name from bundle to create NSData
        //        let image : UIImage = UIImage(named: "AvatarIncoming") ?? UIImage()


        var params : [String : String] = [:]

        if isUploadImage {
            params.updateValue("1", forKey: "actionType")

            if let myImage = image {
                let (bs64String,imageFormate) = myImage.base64(format: .JPEG(0.5))

//                let rImage = bs64String.convertBase64ToImage(base64String: bs64String)
                params.updateValue(bs64String, forKey: "image")
                params.updateValue(imageFormate, forKey: "ext")
            } else {
                params.updateValue("", forKey: "image")
                params.updateValue(".jpg", forKey: "ext")
            }


        } else {
            params.updateValue("3", forKey: "actionType")
            params.updateValue("", forKey: "image")
            params.updateValue(".jpg", forKey: "ext")
        }

        return sendRequest(serviceName, parameters: params as [String : AnyObject], isPostRequest: true, headers: headers, completionBlock: completionBlock)
    }

    // MARK: - addFCMId Request

    func addFCMId(fcmKey : String, ringingStatus : Constants.MBNotificationSoundType, isEnable: Bool, isFromLogin: Bool, _ completionBlock: @escaping MBAPIClientCompletionHandler) -> Request {

        let serviceName = "notifications/addfcm"

        var headers : [String : String] = kRequestHeaders
        headers.updateValue(MBLanguageManager.userSelectedLanguage().rawValue, forKey: "lang")
        headers.updateValue(MBUserSession.shared.token, forKey: "token")
        headers.updateValue(MBUserSession.shared.msisdn, forKey: "msisdn")
        headers.updateValue(MBUserSession.shared.subscriberTypeLowerCaseStr, forKey: "subscriberType")
        headers.updateValue(MBUserSession.shared.brandNameLowerCaseStr, forKey: "tariffType")

        //    {"fcmKey":" edLlvMIxTZc:APA91bEy6jjjEBIP2VLEJeDdwoVlvaBvvy3- d6HqyeEryIXkdl-Ia1xSqtQKWcJjKj0M9QgU5CUmstL8nfALZS0- T44HYtXQrdgsBmq7hByIMJOQ_sl5U1pqAOSskFqV9dibYPijILhz ","ringingStatus":"mute","cause":"login","isEnable":"1"}


        var params : [String : String] = ["fcmKey": fcmKey, "ringingStatus":ringingStatus.rawValue]

        // On Of notification
        if isEnable {
            params.updateValue("1", forKey: "isEnable")
        } else {
            params.updateValue("0", forKey: "isEnable")
        }

        // set cause tyoe
        if isFromLogin {
            params.updateValue("login", forKey: "cause")
        } else {
            params.updateValue("settings", forKey: "cause")
        }

        return sendRequest(serviceName, parameters: params as [String : AnyObject], isPostRequest: true, headers: headers, completionBlock: completionBlock)
    }

    // MARK: - logOut Request

    func logOut(_ completionBlock: @escaping MBAPIClientCompletionHandler) -> Request {

        let serviceName = "customerservices/logout"

        var headers : [String : String] = kRequestHeaders
        headers.updateValue(MBLanguageManager.userSelectedLanguage().rawValue, forKey: "lang")
        headers.updateValue(MBUserSession.shared.token, forKey: "token")
        headers.updateValue(MBUserSession.shared.msisdn, forKey: "msisdn")
        headers.updateValue(MBUserSession.shared.subscriberTypeLowerCaseStr, forKey: "subscriberType")
        headers.updateValue(MBUserSession.shared.brandNameLowerCaseStr, forKey: "tariffType")

        return sendRequest(serviceName, parameters: nil, isPostRequest: true, headers: headers, completionBlock: completionBlock)
    }

    
    //MARK: - billingLanguage Request
    func changeBillingLanguage(language : String ,_ completionBlock: @escaping MBAPIClientCompletionHandler) -> Request{
        
         let serviceName = "customerservices/changebillinglanguage"

        var headers : [String : String] = kRequestHeaders
        headers.updateValue(MBLanguageManager.userSelectedLanguage().rawValue, forKey: "lang")
        headers.updateValue(MBUserSession.shared.token, forKey: "token")
        headers.updateValue(MBUserSession.shared.msisdn, forKey: "msisdn")
        headers.updateValue(MBUserSession.shared.subscriberTypeLowerCaseStr, forKey: "subscriberType")
        headers.updateValue(MBUserSession.shared.brandNameLowerCaseStr, forKey: "tariffType")

        let params : [String : String] = ["language": language]
        return sendRequest(serviceName, parameters: params as [String : AnyObject], isPostRequest: true, headers: headers, completionBlock: completionBlock)
    }

    // MARK: - getCoreServices Request

    func getCoreServices(accountType : String, groupType : String, _ completionBlock: @escaping MBAPIClientCompletionHandler) -> Request {

        let serviceName = "coreservices/getcoreservices"

        var headers : [String : String] = kRequestHeaders
        headers.updateValue(MBLanguageManager.userSelectedLanguage().rawValue, forKey: "lang")
        headers.updateValue(MBUserSession.shared.token, forKey: "token")
        headers.updateValue(MBUserSession.shared.msisdn, forKey: "msisdn")
        headers.updateValue(MBUserSession.shared.subscriberTypeLowerCaseStr, forKey: "subscriberType")
        headers.updateValue(MBUserSession.shared.brandNameLowerCaseStr, forKey: "tariffType")

        let params : [String : String] = ["accountType": accountType, "groupType": groupType]
        
        return sendRequest(serviceName, parameters: params as [String : AnyObject], isPostRequest: true, headers: headers, completionBlock: completionBlock)
    }


    // MARK: - processCoreServices Request

    func processCoreServices(actionType:Bool, offeringId:String, number:String, accountType : String, groupType : String, _ completionBlock: @escaping MBAPIClientCompletionHandler) -> Request {

        let serviceName = "coreservices/processcoreservices"

        var headers : [String : String] = kRequestHeaders
        headers.updateValue(MBLanguageManager.userSelectedLanguage().rawValue, forKey: "lang")
        headers.updateValue(MBUserSession.shared.token, forKey: "token")
        headers.updateValue(MBUserSession.shared.msisdn, forKey: "msisdn")
        headers.updateValue(MBUserSession.shared.subscriberTypeLowerCaseStr, forKey: "subscriberType")
        headers.updateValue(MBUserSession.shared.brandNameLowerCaseStr, forKey: "tariffType")

        var params = ["offeringId":offeringId,"number":number, "accountType": accountType, "groupType": groupType]

        if actionType {
            params.updateValue("1", forKey: "actionType")
        } else {
            params.updateValue("3", forKey: "actionType")
        }

        return sendRequest(serviceName, parameters: params as [String : AnyObject], isPostRequest: true, headers: headers, completionBlock: completionBlock)
    }

    // MARK: - sendInternetSettings Request

    func sendInternetSettings(_ completionBlock: @escaping MBAPIClientCompletionHandler) -> Request {

        let serviceName = "generalservices/sendinternetsettings"

        var headers : [String : String] = kRequestHeaders
        headers.updateValue(MBLanguageManager.userSelectedLanguage().rawValue, forKey: "lang")
        headers.updateValue(MBUserSession.shared.token, forKey: "token")
        headers.updateValue(MBUserSession.shared.msisdn, forKey: "msisdn")
        headers.updateValue(MBUserSession.shared.subscriberTypeLowerCaseStr, forKey: "subscriberType")
        headers.updateValue(MBUserSession.shared.brandNameLowerCaseStr, forKey: "tariffType")

        return sendRequest(serviceName, parameters: nil, isPostRequest: true, headers: headers, completionBlock: completionBlock)
    }

    // MARK: - changeSupplementaryOffering Request

    func changeSupplementaryOffering(actionType:Bool, offeringId:String, offerName:String, _ completionBlock: @escaping MBAPIClientCompletionHandler) -> Request {

        let serviceName = "supplementaryofferings/changesupplementaryoffering"

        var headers : [String : String] = kRequestHeaders
        headers.updateValue(MBLanguageManager.userSelectedLanguage().rawValue, forKey: "lang")
        headers.updateValue(MBUserSession.shared.token, forKey: "token")
        headers.updateValue(MBUserSession.shared.msisdn, forKey: "msisdn")
        headers.updateValue(MBUserSession.shared.subscriberTypeLowerCaseStr, forKey: "subscriberType")
        headers.updateValue(MBUserSession.shared.brandNameLowerCaseStr, forKey: "tariffType")

        var params = ["offeringId":offeringId,"offerName":offerName]
        if !offerName.isBlank {

            if actionType {
                params.updateValue("1", forKey: "actionType")
            } else {
                params.updateValue("3", forKey: "actionType")
            }
        }

//            {"actionType":"1","offeringId":"1376368122","offerName":"Internet"}
        return sendRequest(serviceName, parameters: params as [String : AnyObject], isPostRequest: true, headers: headers, completionBlock: completionBlock)
    }

    // MARK: - appResume Request

    func appResume(_ completionBlock: @escaping MBAPIClientCompletionHandler) -> Request {

        let serviceName = "customerservices/appresume"

        var headers : [String : String] = kRequestHeaders
        headers.updateValue(MBLanguageManager.userSelectedLanguage().rawValue, forKey: "lang")
        headers.updateValue(MBUserSession.shared.token, forKey: "token")
        headers.updateValue(MBUserSession.shared.msisdn, forKey: "msisdn")
        headers.updateValue(MBUserSession.shared.subscriberTypeLowerCaseStr, forKey: "subscriberType")
        headers.updateValue(MBUserSession.shared.brandNameLowerCaseStr, forKey: "tariffType")
        
        let params = ["entityId":MBUserSession.shared.userInfo?.entityId ?? "",
                      "customerId":MBUserSession.shared.userInfo?.customerId ?? ""]

        return sendRequest(serviceName, parameters: params as [String : AnyObject], isPostRequest: true, headers: headers, completionBlock: completionBlock)
    }

    // MARK: - changetTariff Request

    func changetTariff(offeringId:String, offerName:String, subscribableValue: String, _ completionBlock: @escaping MBAPIClientCompletionHandler) -> Request {

        let serviceName = "tariffservices/changetariff"

        var headers : [String : String] = kRequestHeaders
        headers.updateValue(MBLanguageManager.userSelectedLanguage().rawValue, forKey: "lang")
        headers.updateValue(MBUserSession.shared.token, forKey: "token")
        headers.updateValue(MBUserSession.shared.msisdn, forKey: "msisdn")
        headers.updateValue(MBUserSession.shared.subscriberTypeLowerCaseStr, forKey: "subscriberType")
        headers.updateValue(MBUserSession.shared.brandNameLowerCaseStr, forKey: "tariffType")

        //        {"offeringId": "12188819921", "tariffName": "CIN"}
//        actionType="Renew"
//        actionType="Subscribe"

        var params = ["offeringId":offeringId,"tariffName":offerName]

        if subscribableValue == "1" {
            params.updateValue("Subscribe", forKey: "actionType")

        } else if subscribableValue == "3" {
            
            params.updateValue("Renew", forKey: "actionType")
        }

        return sendRequest(serviceName, parameters: params as [String : AnyObject], isPostRequest: true, headers: headers, completionBlock: completionBlock)
    }

    // MARK: - reportLostSim Request
    // HARDCODED 1 will be sent in any case.

    func reportLostSim(reasonCode:String = "1", _ completionBlock: @escaping MBAPIClientCompletionHandler) -> Request {

        let serviceName = "generalservices/reportlostsim"

        var headers : [String : String] = kRequestHeaders
        headers.updateValue(MBLanguageManager.userSelectedLanguage().rawValue, forKey: "lang")
        headers.updateValue(MBUserSession.shared.token, forKey: "token")
        headers.updateValue(MBUserSession.shared.msisdn, forKey: "msisdn")
        headers.updateValue(MBUserSession.shared.subscriberTypeLowerCaseStr, forKey: "subscriberType")
        headers.updateValue(MBUserSession.shared.brandNameLowerCaseStr, forKey: "tariffType")

        let params = ["reasonCode":reasonCode]
        //        {"reasonCode":"1"}
        return sendRequest(serviceName, parameters: params as [String : AnyObject], isPostRequest: true, headers: headers, completionBlock: completionBlock)
    }

    // MARK: - getNotificationsCount Request
    func getNotificationsCount(_ completionBlock: @escaping MBAPIClientCompletionHandler) -> Request {

        let serviceName = "notifications/getnotificationscount"

        var headers : [String : String] = kRequestHeaders
        headers.updateValue(MBLanguageManager.userSelectedLanguage().rawValue, forKey: "lang")
        headers.updateValue(MBUserSession.shared.token, forKey: "token")
        headers.updateValue(MBUserSession.shared.msisdn, forKey: "msisdn")
        headers.updateValue(MBUserSession.shared.subscriberTypeLowerCaseStr, forKey: "subscriberType")
        headers.updateValue(MBUserSession.shared.brandNameLowerCaseStr, forKey: "tariffType")

        return sendRequest(serviceName, parameters: nil, isPostRequest: true, headers: headers, completionBlock: completionBlock)
    }
    
    // MARK: - rateIUs Request
    
    func rateUs(_ completionBlock: @escaping MBAPIClientCompletionHandler) -> Request {
        
        let serviceName = "rateV2/rateus"
        
        var headers : [String : String] = kRequestHeaders
        headers.updateValue(MBLanguageManager.userSelectedLanguage().rawValue, forKey: "lang")
        headers.updateValue(MBUserSession.shared.token, forKey: "token")
        headers.updateValue(MBUserSession.shared.msisdn, forKey: "msisdn")
        headers.updateValue(MBUserSession.shared.subscriberTypeLowerCaseStr, forKey: "subscriberType")
        headers.updateValue(MBUserSession.shared.brandNameLowerCaseStr, forKey: "tariffType")
        
        
        return sendRequest(serviceName, parameters: nil, isPostRequest: true, headers: headers, completionBlock: completionBlock)
    }
    
    // MARK: - UlduzumAPI Requests
    
    func getUlduzumUsageTotals(_ completionBlock: @escaping MBAPIClientCompletionHandler) -> Request {
        
        let serviceName = "ulduzum/getusagetotals"
        
        let params = ["iP":"127.0.0.1"]
        
        var headers : [String : String] = kRequestHeaders
        headers.updateValue(MBLanguageManager.userSelectedLanguage().rawValue, forKey: "lang")
        headers.updateValue(MBUserSession.shared.token, forKey: "token")
        headers.updateValue(MBUserSession.shared.msisdn, forKey: "msisdn")
        headers.updateValue(MBUserSession.shared.subscriberTypeLowerCaseStr, forKey: "subscriberType")
        headers.updateValue(MBUserSession.shared.brandNameLowerCaseStr, forKey: "tariffType")
        
        return sendRequest(serviceName, parameters: params as [String : AnyObject], isPostRequest: true, headers: headers, completionBlock: completionBlock)
    }
    
    func getUlduzumCode(_ completionBlock: @escaping MBAPIClientCompletionHandler) -> Request {
        
        let serviceName = "ulduzum/getcodes"
        
        let params = ["iP":"127.0.0.1"]
        
        var headers : [String : String] = kRequestHeaders
        headers.updateValue(MBLanguageManager.userSelectedLanguage().rawValue, forKey: "lang")
        headers.updateValue(MBUserSession.shared.token, forKey: "token")
        headers.updateValue(MBUserSession.shared.msisdn, forKey: "msisdn")
        headers.updateValue(MBUserSession.shared.subscriberTypeLowerCaseStr, forKey: "subscriberType")
        headers.updateValue(MBUserSession.shared.brandNameLowerCaseStr, forKey: "tariffType")
        
        return sendRequest(serviceName, parameters: params as [String : AnyObject], isPostRequest: true, headers: headers, completionBlock: completionBlock)
    }
    
    func getUlduzumUnusedCodes(_ completionBlock: @escaping MBAPIClientCompletionHandler) -> Request {
        
        let serviceName = "ulduzum/getunusedcodes"
        
        let params = ["iP":"127.0.0.1"]
        
        var headers : [String : String] = kRequestHeaders
        headers.updateValue(MBLanguageManager.userSelectedLanguage().rawValue, forKey: "lang")
        headers.updateValue(MBUserSession.shared.token, forKey: "token")
        headers.updateValue(MBUserSession.shared.msisdn, forKey: "msisdn")
        headers.updateValue(MBUserSession.shared.subscriberTypeLowerCaseStr, forKey: "subscriberType")
        headers.updateValue(MBUserSession.shared.brandNameLowerCaseStr, forKey: "tariffType")
        
        return sendRequest(serviceName, parameters: params as [String : AnyObject], isPostRequest: true, headers: headers, completionBlock: completionBlock)
    }
    
    func getUlduzumUsageHistory(StartDate startDate:String, EndDate endDate:String, _ completionBlock: @escaping MBAPIClientCompletionHandler) -> Request {
        
        let serviceName = "ulduzum/getusagehistory"
        
        let params = ["startDate":startDate,"endDate":endDate]
        
        var headers : [String : String] = kRequestHeaders
        headers.updateValue(MBLanguageManager.userSelectedLanguage().rawValue, forKey: "lang")
        headers.updateValue(MBUserSession.shared.token, forKey: "token")
        headers.updateValue(MBUserSession.shared.msisdn, forKey: "msisdn")
        headers.updateValue(MBUserSession.shared.subscriberTypeLowerCaseStr, forKey: "subscriberType")
        headers.updateValue(MBUserSession.shared.brandNameLowerCaseStr, forKey: "tariffType")
        
        return sendRequest(serviceName, parameters: params as [String : AnyObject], isPostRequest: true, headers: headers, completionBlock: completionBlock)
    }
    
    // MARK: - Roaming API Requests
    
    func getRoamingCountries(_ completionBlock: @escaping MBAPIClientCompletionHandler) -> Request {
        
        let serviceName = "generalservices/getroaming"
        
        let params = ["brandName":MBUserSession.shared.userInfo?.brandName ?? ""]
        
        var headers : [String : String] = kRequestHeaders
        headers.updateValue(MBLanguageManager.userSelectedLanguage().rawValue, forKey: "lang")
        headers.updateValue(MBUserSession.shared.token, forKey: "token")
        headers.updateValue(MBUserSession.shared.msisdn, forKey: "msisdn")
        headers.updateValue(MBUserSession.shared.subscriberTypeLowerCaseStr, forKey: "subscriberType")
        headers.updateValue(MBUserSession.shared.brandNameLowerCaseStr, forKey: "tariffType")
        
        return sendRequest(serviceName, parameters: params as [String : AnyObject], isPostRequest: true, headers: headers, completionBlock: completionBlock)
    }
    
    // MARK: - enable/disable Roaming API Requests
    
    func enableDisableRoaminng(OfferingId offeringId: String, ActionType actionType: String,  _ completionBlock: @escaping MBAPIClientCompletionHandler) -> Request {
        
        let serviceName = "roaming/process"
        
        let params = ["offeringId":offeringId,  "actionType": actionType]
        
        var headers : [String : String] = kRequestHeaders
        headers.updateValue(MBLanguageManager.userSelectedLanguage().rawValue, forKey: "lang")
        headers.updateValue(MBUserSession.shared.token, forKey: "token")
        headers.updateValue(MBUserSession.shared.msisdn, forKey: "msisdn")
        headers.updateValue(MBUserSession.shared.subscriberTypeLowerCaseStr, forKey: "subscriberType")
        headers.updateValue(MBUserSession.shared.brandNameLowerCaseStr, forKey: "tariffType")
        
        return sendRequest(serviceName, parameters: params as [String : AnyObject], isPostRequest: true, headers: headers, completionBlock: completionBlock)
    }
    
    func userRoamingStatus(OfferingId offeringId: String,  _ completionBlock: @escaping MBAPIClientCompletionHandler) -> Request {
        
        let serviceName = "roaming/status"
        
        let params = ["offeringId":offeringId]
        
        var headers : [String : String] = kRequestHeaders
        headers.updateValue(MBLanguageManager.userSelectedLanguage().rawValue, forKey: "lang")
        headers.updateValue(MBUserSession.shared.token, forKey: "token")
        headers.updateValue(MBUserSession.shared.msisdn, forKey: "msisdn")
        headers.updateValue(MBUserSession.shared.subscriberTypeLowerCaseStr, forKey: "subscriberType")
        headers.updateValue(MBUserSession.shared.brandNameLowerCaseStr, forKey: "tariffType")
        
        return sendRequest(serviceName, parameters: params as [String : AnyObject], isPostRequest: true, headers: headers, completionBlock: completionBlock)
    }
    
    // MARK: - RateUS API Requests
    
    func getRateUs(_ completionBlock: @escaping MBAPIClientCompletionHandler) -> Request {
        
        let serviceName = "rateV2/getrateus"
        
       let params = ["entityId":MBUserSession.shared.userInfo?.entityId ?? ""]
        
        var headers : [String : String] = kRequestHeaders
        headers.updateValue(MBLanguageManager.userSelectedLanguage().rawValue, forKey: "lang")
        headers.updateValue(MBUserSession.shared.token, forKey: "token")
        headers.updateValue(MBUserSession.shared.msisdn, forKey: "msisdn")
        headers.updateValue(MBUserSession.shared.subscriberTypeLowerCaseStr, forKey: "subscriberType")
        headers.updateValue(MBUserSession.shared.brandNameLowerCaseStr, forKey: "tariffType")
        
        return sendRequest(serviceName, parameters: params as [String : AnyObject], isPostRequest: true, headers: headers, completionBlock: completionBlock)
    }
    
    
    
    
    
    //MARK: Get Survey
    
    func getSurveys(_ completionBlock: @escaping MBAPIClientCompletionHandler) -> Request {
        
        let serviceName = "survey/getsurveys"
        
//       let params = ["entityId":MBUserSession.shared.userInfo?.entityId ?? ""]
        
        var headers : [String : String] = kRequestHeaders
        headers.updateValue(MBLanguageManager.userSelectedLanguage().rawValue, forKey: "lang")
        headers.updateValue(MBUserSession.shared.token, forKey: "token")
        headers.updateValue(MBUserSession.shared.msisdn, forKey: "msisdn")
        headers.updateValue(MBUserSession.shared.subscriberTypeLowerCaseStr, forKey: "subscriberType")
        headers.updateValue(MBUserSession.shared.brandNameLowerCaseStr, forKey: "tariffType")
        
        return sendRequest(serviceName, parameters: nil, isPostRequest: true, headers: headers, completionBlock: completionBlock)
    }
    
    
    
    //MARK: Save Survey
    
    func saveSurvey(comment: String, answerId: String, questionId: String, offeringIdSurvey: String, offeringTypeSurvey: String, surveyId: String, _ completionBlock: @escaping MBAPIClientCompletionHandler) -> Request {
        
        let serviceName = "survey/savesurvey"
        
       let params = [
            "comment": comment,
            "answerId": answerId,
            "questionId": questionId,
            "offeringIdSurvey": offeringIdSurvey,
            "offeringTypeSurvey": offeringTypeSurvey,
            "surveyId": surveyId,
       ]
        
        var headers : [String : String] = kRequestHeaders
        headers.updateValue(MBLanguageManager.userSelectedLanguage().rawValue, forKey: "lang")
        headers.updateValue(MBUserSession.shared.token, forKey: "token")
        headers.updateValue(MBUserSession.shared.msisdn, forKey: "msisdn")
        headers.updateValue(MBUserSession.shared.subscriberTypeLowerCaseStr, forKey: "subscriberType")
        headers.updateValue(MBUserSession.shared.brandNameLowerCaseStr, forKey: "tariffType")
        
        return sendRequest(serviceName, parameters: params as [String : AnyObject], isPostRequest: true, headers: headers, completionBlock: completionBlock)
    }
}


