//
//  NotificationsVC.swift
//  Bakcell
//
//  Created by Saad Riaz on 6/12/17.
//  Copyright © 2017 evampsaanga. All rights reserved.
//

import UIKit
import ObjectMapper

class NotificationsVC: BaseVC {

    // MARK: - Properties
    var notificationsData : [NotificationsList]?
    var filterdNotificationsData : [NotificationsList]?
    var showSearch : Bool = true
    
  // MARK: - IBOutlet
    @IBOutlet var title_lbl: UILabel!
    @IBOutlet var search_txt: UITextField!
    @IBOutlet var searchView: UIView!
    @IBOutlet var search_btn: UIButton!
    @IBOutlet var tableView: UITableView!

    //MARK: - View controller Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        search_btn.isHidden = false
        search_txt.delegate = self
        searchView.isHidden = true
        search_txt.attributedPlaceholder = NSAttributedString(string: Localized("PlaceHolder_Search"),
                                                              attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        
        tableView.separatorColor = UIColor.clear
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 180.0
        tableView.allowsSelection = false
        tableView.delegate = self
        tableView.dataSource = self
        tableView.isScrollEnabled = true

        // Get Notification data
        getNotifications()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.interactivePop(true)
        
        // Setting ViewController localized text
        layoutViewController()
        
        /* Reset Notification bage to 0 */
        UIApplication.shared.applicationIconBadgeNumber = 0
    }

    //MARK: - IBActions

    @IBAction func searchPressed(_ sender: UIButton) {

        if showSearch == false {
            search_btn.setImage(UIImage (imageLiteralResourceName: "actionbar_search") , for: UIControl.State.normal)
            searchView.isHidden = true
            title_lbl.isHidden = false
            showSearch = true

            search_txt.text = ""
            search_txt.resignFirstResponder()

//            searchOffersByName(searchString: "")

        } else {
            search_btn.setImage(UIImage (imageLiteralResourceName: "Cross") , for: UIControl.State.normal)
            title_lbl.isHidden = true
            searchView.isHidden = false
            showSearch = false

            search_txt.becomeFirstResponder()

        }
    }

     @IBAction func redirectionBtnPressed(_ sender: UIButton) {

        let selectedNotificationData = filterdNotificationsData?[sender.tag]

        if (selectedNotificationData?.actionType ?? "").isEqualToStringIgnoreCase(otherString: "tariff") {

            redirectUserToTariffController(actionID: selectedNotificationData?.actionID)

        } else if (selectedNotificationData?.actionType ?? "").isEqualToStringIgnoreCase(otherString: "supplementary") {

            redirectUserToSupplementaryController(actionID: selectedNotificationData?.actionID)

        }
        print(selectedNotificationData?.actionType ?? "")
    }
    
    //MARK: - Functions
    func layoutViewController() {
        title_lbl.text  = Localized("Title_Notifications")
    }

    func reloadNotificationTableView()  {

        if self.filterdNotificationsData?.count == 0 {
            self.tableView.showDescriptionViewWithImage(description: Localized("Message_NoDataAvalible"))

        } else {
            self.tableView.hideDescriptionView()
        }
        tableView.reloadData()
    }

    func redirectUserToSupplementaryController(actionID : String?) {

        let supplementoryOfferVC = self.myStoryBoard.instantiateViewController(withIdentifier: "SupplementaryVC") as! SupplementaryVC
        supplementoryOfferVC.offeringIdFromNotification = actionID ?? ""
        supplementoryOfferVC.redirectedFromNotification = true
        self.navigationController?.pushViewController(supplementoryOfferVC, animated: true)

    }
    func redirectUserToTariffController(actionID : String?) {

        let aViewController : TariffsMainVC = self.myStoryBoard.instantiateViewController(withIdentifier: "TariffsMainVC") as! TariffsMainVC
        aViewController.redirectedFromNotification = true
        aViewController.offeringIdFromNotification = actionID ?? ""
        self.navigationController?.pushViewController(aViewController, animated: true)

    }


    //MARK: - API Calls
    /// Call 'getNotifications' API .
    ///
    /// - returns: Void
    func getNotifications() {

        /*Loading initial data from UserDefaults*/
        if let notificationsHandler :Notifications = Notifications.loadFromUserDefaults(key: APIsType.notificationData.selectedLocalizedAPIKey()) {
            self.notificationsData = notificationsHandler.notificationsList
            self.filterdNotificationsData = notificationsHandler.notificationsList
            self.reloadNotificationTableView()
            
        } else {
            activityIndicator.showActivityIndicator()
        }

        /*API call to get data*/
        _ = MBAPIClient.sharedClient.getNotifications({ (response, resultData, error, isCancelled, status, resultCode, resultDesc) in
            self.activityIndicator.hideActivityIndicator()
            if error != nil {
                error?.showServerErrorInViewController(self)

            } else {
                // handling data from API response.
                if resultCode == Constants.MBAPIStatusCode.succes.rawValue {

                    // Parssing response data
                    if let notificationsHandler = Mapper<Notifications>().map(JSONObject: resultData){

                        /*Save data into user defaults*/
                        notificationsHandler.saveInUserDefaults(key: APIsType.notificationData.selectedLocalizedAPIKey())
                        
                        self.notificationsData = notificationsHandler.notificationsList
                        self.filterdNotificationsData = notificationsHandler.notificationsList
                        self.reloadNotificationTableView()

                    } else {

                        self.showErrorAlertWithMessage(message: resultDesc)
                    }
                } else {
                    
                    self.showErrorAlertWithMessage(message: resultDesc)
                }
            }
        })
    }
}

 //MARK: - TableView Delegate
extension NotificationsVC : UITableViewDelegate, UITableViewDataSource {

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return filterdNotificationsData?.count ?? 0
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let cell = tableView.dequeueReusableCell(withIdentifier: "cellID") as! NotificationsCell

        // setting side line for last notification data
        if (filterdNotificationsData?.count ?? 0) - 1 == indexPath.row {
            cell.lineHeightConstraint.isActive = true
            cell.lineHeightConstraint.constant = 16
            cell.lineBottomConstraint.isActive = false
        } else {
            cell.lineHeightConstraint.isActive = false
            cell.lineBottomConstraint.isActive = true

        }

        // Loading data
        if let aNotificationData = filterdNotificationsData?[indexPath.row] {

            //FIXME: - icon mapping is not added
            cell.imgView?.image = UIImage (named:"icon1" )
            if aNotificationData.btnTxt?.isBlank ?? true {
                cell.reNewBtnWidth.constant = 0
                cell.reNewBtnWidth.isActive = true
                cell.renew_btn.isHidden = true
            
            } else {
                
                cell.reNewBtnWidth.constant = 90
                //  cell.reNewBtnWidth.isActive = false
                
                cell.renew_btn.isHidden = false
                cell.renew_btn.roundAllCorners(radius: CGFloat(Constants.kButtonCornerRadius))
                cell.renew_btn.setTitle(aNotificationData.btnTxt ?? "", for: UIControl.State.normal)
                cell.renew_btn.addTarget(self, action: #selector(redirectionBtnPressed(_:)), for: UIControl.Event.touchUpInside)
                cell.renew_btn.tag = indexPath.row
            }
            cell.details_lbl.text = aNotificationData.message

            let times = "\(aNotificationData.datetime ?? "")".offSetFirstValue()
            //Adding '.' for "ч"
            if times .range(of: "ч") != nil{
                cell.time_lbl.text = "\(times)."
            }
            else{
                cell.time_lbl.text = "\(times)"
            }

        }
        cell.layoutIfNeeded()
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }

}

extension NotificationsVC : UITextFieldDelegate {

    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let text = textField.text else { return false }

        let newString = (text as NSString).replacingCharacters(in: range, with: string)
        filterdNotificationsData = filterNotificationBySearchString(searchString: newString, notificationsArray: notificationsData)
        reloadNotificationTableView()

        return true
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return false
    }


    func filterNotificationBySearchString(searchString : String? , notificationsArray : [NotificationsList]? ) -> [NotificationsList] {

        // return complete array if string is empty
        if let searchString = searchString {

            if searchString == "" {

                if let offers = notificationsArray {
                    return offers
                }
            }
        }

        // Filtering offers array
        var filteredNotification : [NotificationsList] = []
        if let aNotification = notificationsArray {

            filteredNotification = aNotification.filter() {

                if let offerName = ($0 as NotificationsList).message {

                    return offerName.containsSubString(subString: searchString)

                } else {
                    return false
                }
            }
        }

        return filteredNotification
    }
}
