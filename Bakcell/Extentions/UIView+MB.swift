//
//  UIView+MB.swift
//  Bakcell
//
//  Created by AbdulRehman Warraich on 9/12/17.
//  Copyright © 2017 evampsaanga. All rights reserved.
//

import Foundation
import UIKit

extension UIView {
    
    func roundCorners(corners:UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: self.bounds,
                                byRoundingCorners: corners,
                                cornerRadii: CGSize(width: radius, height: radius))
        let maskLayer = CAShapeLayer()
        maskLayer.frame = self.bounds
        maskLayer.path = path.cgPath
        self.layer.mask = maskLayer
    }
    
    func roundTopCorners(radius: CGFloat) {
        //        DispatchQueue.main.async {
        //            self.roundCorners(corners:[.topLeft, .topRight], radius: radius)
        //        }
        self.layer.cornerRadius = radius
    }
    
    func roundAllCorners(radius: CGFloat) {
        DispatchQueue.main.async {
            self.roundCorners(corners:[.topLeft, .topRight, .bottomLeft, .bottomRight], radius: radius)
        }
    }
    
    func removeAllSubViews() {
        self.subviews.forEach { (aView) in
            aView.removeFromSuperview()
        }
    }
    
    public class func fromNib() -> Self {
        return fromNib(nibName: nil)
    }
    public class func fromNib(nibName: String?) -> Self {
        func fromNibHelper<T>(nibName: String?) -> T where T : UIView {
            let bundle = Bundle(for: T.self)
            let name = nibName ?? String(describing: T.self)
            return bundle.loadNibNamed(name, owner: nil, options: nil)?.first as? T ?? T()
        }
        return fromNibHelper(nibName: nibName)
    }
}

//MARK:- UIView extention for DescriptionView
extension UIView {
    func showDescriptionViewWithImage(_ imageName:String = "step-3-info", description: String?) {
        
        if let myDescriptionView = self.viewWithTag(998877) as? DescriptionView {
            
            myDescriptionView.setDescriptionViewWithImage(imageName, description: description)
            
        } else {
            
            let myDescriptionView: DescriptionView = DescriptionView.fromNib()
            myDescriptionView.setDescriptionViewWithImage(imageName, description: description)
            myDescriptionView.translatesAutoresizingMaskIntoConstraints = false
            
            self.addSubview(myDescriptionView)
            self.bringSubviewToFront(myDescriptionView)
            
            let leadingConstraint = NSLayoutConstraint(item: myDescriptionView, attribute: .leading, relatedBy: .equal, toItem: self, attribute: NSLayoutConstraint.Attribute.leading, multiplier: 1, constant: 0)
            
            let trailingConstraint = NSLayoutConstraint(item: myDescriptionView, attribute: .trailing, relatedBy: .equal, toItem: self, attribute: .trailing, multiplier: 1, constant: 0)
            
            let verticalCenterConstraint = NSLayoutConstraint(item: myDescriptionView, attribute: .centerY, relatedBy: .equal, toItem: self, attribute: .centerY, multiplier: 1, constant: 0)
            
            let horizantalCenterConstraint = NSLayoutConstraint(item: myDescriptionView, attribute: .centerX, relatedBy: .equal, toItem: self, attribute: .centerX, multiplier: 1, constant: 0)
            
            self.addConstraints([leadingConstraint, trailingConstraint, verticalCenterConstraint, horizantalCenterConstraint])
            
        }
    }
    
    func hideDescriptionView() {
        if let myDescriptionView = self.viewWithTag(998877) as? DescriptionView {
            
            myDescriptionView.isHidden = true
            myDescriptionView.removeFromSuperview()
        }
    }
}
