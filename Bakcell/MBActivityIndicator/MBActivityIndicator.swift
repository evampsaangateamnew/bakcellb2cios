//
//  MBActivityIndicator.swift
//  Bakcell
//
//  Created by AbdulRehman Warraich on 4/13/18.
//  Copyright © 2018 evampsaanga. All rights reserved.
//

import Foundation

//MARK:- MBActivityIndicator Class
class MBActivityIndicator {
    
    var container: UIView = UIView()
    var loadingView: UIView = UIView()
    var activityIndicator: UIActivityIndicatorView = UIActivityIndicatorView()
    
    /*
     Show customized activity indicator,
     actually add activity indicator to passing view
     */
    func showActivityIndicator() {
        container.tag = 92119211
        
        if let window = UIApplication.shared.keyWindow {
            container.frame = window.frame
            container.center = window.center
            
            //  if window.viewWithTag(92119211) != nil {
            //  container.backgroundColor = UIColor.clear
            //  } else {
            //  container.backgroundColor = UIColor.black.withAlphaComponent(0.5)
            //  }
            
            container.backgroundColor = UIColor.black.withAlphaComponent(0.5)
            
            loadingView.frame = CGRect(x: 0, y: 0, width: 80, height: 80)
            loadingView.center = container.center
            loadingView.backgroundColor = UIColor.black
            loadingView.clipsToBounds = true
            loadingView.layer.cornerRadius = 10
            
            activityIndicator.frame = CGRect(x: 0, y: 0, width: 40, height: 40);
            activityIndicator.style = UIActivityIndicatorView.Style.whiteLarge
            activityIndicator.center = CGPoint(x:loadingView.frame.size.width / 2,y: loadingView.frame.size.height / 2);
            
            loadingView.addSubview(activityIndicator)
            container.addSubview(loadingView)
            
            window.addSubview(container)
            window.bringSubviewToFront(container)
            
            activityIndicator.startAnimating()
        }
    }
    
    /*
     Hide activity indicator
     Actually remove activity indicator from its superView
     */
    func hideActivityIndicator() {
        activityIndicator.stopAnimating()
        container.removeFromSuperview()
    }
    
    /*
     Hide all activity indicator
     Actually remove activity indicator from its superView
     */
    func removeAllActivityIndicator() {
        activityIndicator.stopAnimating()
        container.removeFromSuperview()
        
        MBActivityIndicator.removeAllActivityIndicator()
    }
    
    class func removeAllActivityIndicator() {
        UIApplication.shared.keyWindow?.subviews.forEach({ (aView) in
            
            // If found in window subview then remove
            if aView.tag == 92119211 {
                
                if let activityIndicator = (aView.subviews.first ?? UIView()).subviews.first as? UIActivityIndicatorView {
                    activityIndicator.stopAnimating()
                }
                aView.removeFromSuperview()
            }
        })
    }
}



