//
//  AutoPaymentCell.swift
//  Bakcell
//
//  Created by Touseef Sarwar on 04/03/2021.
//  Copyright © 2021 evampsaanga. All rights reserved.
//

import UIKit
import MarqueeLabel

class AutoPaymentCell: UITableViewCell {
    //cell Identifier
    static let ID = "AutoPaymentCell"

    //MARK: _ IBOutlets
    
    @IBOutlet weak var cardImage : UIImageView!
    @IBOutlet weak var cardNumber : UILabel!
    @IBOutlet weak var cardAmount : UILabel!
    @IBOutlet weak var cardDay : MarqueeLabel!
    
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
//        self.cardDay.setupMarqueeAnimation()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    func  setUp(withData data : AutoPayments?){
        if let infoo = data{
            self.cardNumber.text = infoo.msisdn
            self.cardDay.text = infoo.recurrentDay
            self.cardAmount.text = infoo.amount + " ₼";
            //self.cardDay.setupMarqueeAnimation()
            self.cardImage.image = infoo.cardType == "master" ? #imageLiteral(resourceName: "masterCard") :  #imageLiteral(resourceName: "visaCard")
            
            
        }else{return}
    }
}
