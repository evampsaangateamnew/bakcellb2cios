//
//  MoneyTransferVC.swift
//  Bakcell
//
//  Created by Saad Riaz on 6/12/17.
//  Copyright © 2017 evampsaanga. All rights reserved.
//

import UIKit
import ObjectMapper

class MoneyTransferVC: BaseVC, UITableViewDelegate, UITableViewDataSource {
    
    // MARK: - Properties
    var reciever = ""
    var selectedValue : String?
    let moneyTransfer : MoneyTransfer = MBUserSession.shared.predefineData?.topup?.moneyTransfer ?? MoneyTransfer()
    
    // MARK: - IBOutlet
    @IBOutlet var dropDownImage: UIImageView!
    @IBOutlet var tableView: UITableView!
    @IBOutlet var recieverNumber_txt: UITextField!
    @IBOutlet var dropDown: UIDropDown!
    @IBOutlet var moneyTransferTitle_lbl: UILabel!
    @IBOutlet var intro_lbl: UILabel!
    @IBOutlet var amount_lbl: UILabel!
    
    @IBOutlet var transfer_btn: UIButton!
    
    // MARK: - View Controller Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        recieverNumber_txt.delegate = self
        
        tableView.allowsSelection = false
        tableView.delegate = self
        tableView.dataSource = self
        tableView.estimatedRowHeight = 44
        tableView.rowHeight = UITableView.automaticDimension
        
        dropDown.textAlignment = NSTextAlignment .center
        dropDown.optionsTextAlignment = NSTextAlignment.center
        dropDown.placeholder = Localized("DropDown_SelectAmount")
        self.dropDown.rowBackgroundColor = UIColor.MBDimLightGrayColor
        dropDown.setFont = UIFont.MBArial(fontSize: 12)
        
        var optionsArray : [String] = []
        
        //Check for Brand to popuate dropDown
        if MBUserSession.shared.brandName() == MBBrandName.Klass{
            
            if let selectedAmount = moneyTransfer.selectAmount?.klass {
                if selectedAmount.count == 1{
                    dropDown.placeholder = selectedAmount[0].amount
                    dropDown.tableHeight = 0
                    self.selectedValue = selectedAmount[0].amount
                    //self.selectedValue = self.selectedValue?.substring(to: (self.selectedValue?.index(before: (self.selectedValue?.endIndex)!))!)
                    
                    
                    selectedAmount.forEach({ (aSelectAmount) in
                        
                        if let aAmount = aSelectAmount.amount {
                            
                            optionsArray.append("\(aAmount)₼")
                        }
                    })
                    
                } else {
                    selectedAmount.forEach({ (aSelectAmount) in
                        
                        if let aAmount = aSelectAmount.amount {
                            
                            optionsArray.append("\(aAmount)₼")
                        }
                    })
                }
            }
            
        } else {
            
            if let selectedAmount = moneyTransfer.selectAmount?.cin {
                if selectedAmount.count == 1{
                    dropDown.placeholder = selectedAmount[0].amount
                    dropDown.tableHeight = 0
                    self.selectedValue = selectedAmount[0].amount
                    
                    selectedAmount.forEach({ (aSelectAmount) in
                        
                        if let aAmount = aSelectAmount.amount {
                            
                            optionsArray.append("\(aAmount)₼")
                        }
                    })
                    
                } else {
                    selectedAmount.forEach({ (aSelectAmount) in
                        
                        if let aAmount = aSelectAmount.amount {
                            
                            optionsArray.append("\(aAmount)₼")
                        }
                    })
                }
            }
        }
        
        dropDown.options = optionsArray
        
        self.dropDown.tableWillAppear {
            self.dropDownImage.image = UIImage (named: "Drop-down-arrow-state2")
        }
        
        self.dropDown.tableWillDisappear {
            self.dropDownImage.image = UIImage (named: "Drop-down-arrow-state1")
        }
        
        dropDown.didSelect { (option, index) in
            // self.dropDownLbl.text = " \(option)"
            print("You just select: \(option) at index: \(index)")
            self.selectedValue = option
            //self.selectedValue = self.selectedValue?.substring(to: (self.selectedValue?.index(before: (self.selectedValue?.endIndex)!))!)
            print(self.selectedValue ?? "")
            
        }
        
        //setting number of views of this screen
        UserDefaults.standard.set(UserDefaults.standard.integer(forKey: "BalanceShareScreenViews")+1, forKey: "BalanceShareScreenViews")
        UserDefaults.standard.synchronize()
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.interactivePop(true)
        
        loadViewLayout()
       /*
         if let verifyOTPVC = self.myStoryBoard.instantiateViewController(withIdentifier: "VerifyOTPVC") as? VerifyOTPVC {
         verifyOTPVC.setStatusCompletionBlocks(sucessBlock: {
         
         }, failureBlock: {
         self.navigationController?.popViewController(animated: true)
         })
         self.presentPOPUP(verifyOTPVC, animated: true, completion: nil)
         }
         */
    }
    
    //MARK: IBACTIONS
    @IBAction func transferPressed(_ sender: Any) {
        
        reciever = recieverNumber_txt.text ?? ""
        
        if selectedValue == nil && reciever == "" {
            
            self.showErrorAlertWithMessage(message: Localized("Msg_PleaseEnterMobileNumber"))
            
        }
        else if selectedValue != nil && reciever == "" {
            
            self.showErrorAlertWithMessage(message: Localized("Msg_PleaseEnterMobileNumber"))
        }
        else if selectedValue == nil && recieverNumber_txt.text != nil {
            
            self.showErrorAlertWithMessage(message: Localized("Msg_PleaseSelectAmount"))
        }
        else if  recieverNumber_txt.text != nil && recieverNumber_txt.isValidMSISDN() == false {
            
            self.showErrorAlertWithMessage(message: Localized("Message_EnterValidNumber"))
        }
            
        else if (selectedValue != nil && recieverNumber_txt.text != nil ) {
            
            let alert = self.myStoryBoard.instantiateViewController(withIdentifier: "AlertVC") as! AlertVC
            
            self.presentPOPUP(alert, animated: true, completion: nil)
            alert.setConfirmationAlertLayoutMoneyTransfer(title: Localized("Title_Confirmation"), amount: self.selectedValue ?? "", reciver: self.reciever)
            
            alert.setYesButtonCompletionHandler { (aString) in
                
                if let verifyOTPVC = self.myStoryBoard.instantiateViewController(withIdentifier: "VerifyOTPVC") as? VerifyOTPVC {
                    
                    verifyOTPVC.setStatusCompletionBlocks(sucessBlock: {
                        
                        self.moneyTransferCall()
                    })
                    
                    self.presentPOPUP(verifyOTPVC, animated: true, completion: nil)
                }
            }
        }
    }
    
    //MARK: - FUNCTIONS
    func loadViewLayout(){
        
        transfer_btn.titleLabel?.font = UIFont.MBArial(fontSize: 16)
        moneyTransferTitle_lbl.font = UIFont.MBArial(fontSize: 20)
        intro_lbl.font = UIFont.MBArial(fontSize: 14)
        amount_lbl.font = UIFont.MBArial(fontSize: 14)
        
        moneyTransferTitle_lbl.text = Localized("Title_MoneyTransfer")
        intro_lbl.text = Localized("Info_Description")
        amount_lbl.text = Localized("Title_Amount")
        recieverNumber_txt.placeholder = Localized("placeHolder_RecieverNumberForMoneyTransfer")
        
        transfer_btn.setTitle(Localized("BtnTitle_TRANSFER"), for: UIControl.State.normal)
        
        
    }
    
    
    //MARK: -  in-App-Survery
    func checkLoanSurvey(title:String, amount:String, receiver:String, showBalanceOfUser:Bool) {
        
        var attributedBalanceTitle = NSMutableAttributedString()
        if  showBalanceOfUser == false {
            attributedBalanceTitle = NSMutableAttributedString()

        } else {
            attributedBalanceTitle = self.setBalanceText(amount: MBUserSession.shared.balance?.amount ?? "0.0", isBalanceColorBlack: false)
        }

        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.alignment = .left

        let successMessage = String(format: Localized("Message_MoneyTransfer_SuccessTransfer"), amount , receiver)

        let message = NSMutableAttributedString(string: successMessage, attributes: [NSAttributedString.Key.foregroundColor: UIColor.MBTextGrayColor, NSAttributedString.Key.paragraphStyle:paragraphStyle, NSAttributedString.Key.font: UIFont.MBArial(fontSize: 14)])

        var amountTextRange = (successMessage as NSString).range(of: amount)
        amountTextRange.length = amountTextRange.length + 4 // to include AZN

        message.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.black, range: amountTextRange)
        
        if let survey  = MBUserSession.shared.appSurvays?.surveys?.first(where: { $0.screenName  == "transfer_money" }) {
            if survey.surveyCount?.toInt ?? 0 < survey.surveyLimit?.toInt ?? 0,
               survey.visitLimit != "-1",
               survey.visitLimit?.toInt ?? 0 <= UserDefaults.standard.integer(forKey: "BalanceShareScreenViews") {
                if let inAppSurveyVC : InAppSurveyVC = InAppSurveyVC.fromNib() {
                    inAppSurveyVC.currentSurvay = survey
                    inAppSurveyVC.showTopUpStackView = true
                    inAppSurveyVC.hideBalanceStackView = false
                    inAppSurveyVC.attributedBalance = attributedBalanceTitle
                    inAppSurveyVC.toptupTitleText = title
                    inAppSurveyVC.attributedMessage = message
                    self.presentPOPUP(inAppSurveyVC, animated: true, completion: nil)
                }
            } else {
                let alert = self.myStoryBoard.instantiateViewController(withIdentifier: "AlertVC") as! AlertVC
                
                alert.setSuccessAlertForMoneyTransfer(title: title, amount: amount, receiver: receiver,  showBalnce: showBalanceOfUser)
                
                self.presentPOPUP(alert, animated: true, completion: nil)
            }
        }
    }
    
    
    func setBalanceText(amount: String, isBalanceColorBlack: Bool = true) -> NSMutableAttributedString {

        let balanceText = NSMutableAttributedString(string: Localized("Balance"), attributes: [NSAttributedString.Key.foregroundColor: UIColor.MBTextGrayColor, NSAttributedString.Key.font: UIFont.MBArial(fontSize: 14)])

        let amountAttributedText = NSMutableAttributedString(string: "\(amount) ₼" ,attributes: [ NSAttributedString.Key.font: UIFont.MBArial(fontSize: 14)])

        if isBalanceColorBlack {

            amountAttributedText.addAttribute(NSAttributedString.Key.foregroundColor,
                                              value: UIColor.black,
                                              range: NSRange(location: 0 , length: amountAttributedText.length))
        } else {

            amountAttributedText.addAttribute(NSAttributedString.Key.foregroundColor,
                                              value: UIColor.MBRedColor,
                                              range: NSRange(location: 0 , length: amountAttributedText.length))
        }

        balanceText.append(amountAttributedText)

        balanceText.addAttribute(NSAttributedString.Key.font,
                                 value: UIFont.systemFont(ofSize: 12),
                                 range: NSRange(location: (balanceText.length - 1), length: 1))

        balanceText.addAttribute(NSAttributedString.Key.baselineOffset,
                                 value:2.00,
                                 range: NSRange(location:(balanceText.length - 1),length:1))

        return balanceText
    }
    
    //MARK: - API CAll
    /// Call 'MoneyTransfer' API.
    ///
    /// - returns: Void
    
    func moneyTransferCall (){
        activityIndicator.showActivityIndicator()
        
        _ = MBAPIClient.sharedClient.moneyTransfer(Transfer: reciever, Amount: selectedValue ?? "",  { ( response, resultData, error, isCancelled, status, resultCode, resultDesc) in
            
            self.activityIndicator.hideActivityIndicator()
            if error != nil {
                //EventLog
                MBAnalyticsManager.logEvent(screenName: "Top up Screen", contentType:"Money transfer Screen" , status:"Money transfer Failure" )
                
                error?.showServerErrorInViewController(self)
                
            } else {
                // handling data from API response.
                if resultCode == Constants.MBAPIStatusCode.succes.rawValue {
                    
                    //EventLog
                    MBAnalyticsManager.logEvent(screenName: "Top up Screen", contentType:"Money transfer Screen" , status:"Money transfer Success" )
                    
                    // Set true to reload Home page data
                    MBUserSession.shared.shouldReloadDashboardData = true
                    
                    // Parssing response data
                    if let MoneyResponse = Mapper<TopUp>().map(JSONObject:resultData) {
                        
                        print(MoneyResponse.newBalance)
                        
                        var showBalanceOfUser:Bool = true
                        if  MBUserSession.shared.subscriberType() == MBSubscriberType.PostPaid {
                            showBalanceOfUser = false
                        } else {
                            MBUserSession.shared.balance?.amount = MoneyResponse.newBalance
                            
                        }
                        
                        self.checkLoanSurvey(title: Localized("Title_successful"), amount: self.selectedValue ?? "",
                                             receiver: self.reciever,
                                             showBalanceOfUser: showBalanceOfUser)
                        /*let alert = self.myStoryBoard.instantiateViewController(withIdentifier: "AlertVC") as! AlertVC
                        
                        alert.setSuccessAlertForMoneyTransfer(title: Localized("Title_successful"), amount: self.selectedValue ?? "", receiver: self.reciever,  showBalnce: showBalanceOfUser)
                        
                        self.presentPOPUP(alert, animated: true, completion: nil)*/
                    }
                    
                } else {
                    //EventLog
                    MBAnalyticsManager.logEvent(screenName: "Top up Screen", contentType:"Money transfer Screen" , status:"Money transfer Failure" )
                    
                    // Show error alert to user
                    self.showErrorAlertWithMessage(message: resultDesc)
                }
            }
        })
    }
    
    // MARK: - TABLEVIEW DELEGATE METHODS
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        
        if MBUserSession.shared.brandName() == MBBrandName.Klass{
            return (moneyTransfer.termsAndCondition?.klass?.count ?? 0)
        }
        else{
            return (moneyTransfer.termsAndCondition?.cin?.count ?? 0)
            
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "TermsConditionsCell", for: indexPath) as! TermsConditionsCell
        
        //Check for the brand type
        if MBUserSession.shared.brandName() == MBBrandName.Klass{
            cell.terms_lbl.text = moneyTransfer.termsAndCondition?.klass?[indexPath.row].text ?? ""
            cell.amount_lbl.text = moneyTransfer.termsAndCondition?.klass?[indexPath.row].amount ?? ""
        }
            
        else{
            cell.terms_lbl.text = moneyTransfer.termsAndCondition?.cin?[indexPath.row].text ?? ""
            cell.amount_lbl.text = moneyTransfer.termsAndCondition?.cin?[indexPath.row].amount ?? ""
            
        }
        
        return cell
        
    }
    
}

//MARK: Textfield delagates
extension MoneyTransferVC : UITextFieldDelegate {
    
    //Set max Limit for textfields
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        guard let text = textField.text else { return false }
        let newLength = text.count + string.count - range.length
        
        let allowedCharacters = CharacterSet(charactersIn: Constants.allowedNumbers)
        let characterSet = CharacterSet(charactersIn: string)
        if textField == recieverNumber_txt {
            if newLength <= 9 && allowedCharacters.isSuperset(of: characterSet) {
                return  true
            } else {
                return  false
            }
            
        }  else {
            return false // Bool
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == recieverNumber_txt {
            transferPressed(UIButton())
        }
        return true
    }
}

