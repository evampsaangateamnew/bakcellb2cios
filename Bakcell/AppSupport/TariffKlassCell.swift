//
//  TariffKlassCell.swift
//  Bakcell
//
//  Created by Saad Riaz on 7/19/17.
//  Copyright © 2017 evampsaanga. All rights reserved.
//

import UIKit


class TariffKlassCell: UICollectionViewCell {


    static let identifier = "CarouselCollectionViewCell"

    @IBOutlet var tableView: MBAccordionTableView!
    
    
    //Stars outlets....
    @IBOutlet weak var btnsStackView: UIStackView!
    @IBOutlet weak var btn1: UIButton!
    @IBOutlet weak var btn2: UIButton!
    @IBOutlet weak var btn3: UIButton!
    @IBOutlet weak var btn4: UIButton!
    @IBOutlet weak var btn5: UIButton!
    
    
    var delegate : SupplementaryOffersCellDelegate? = nil
    var offeringId : String?

    override func awakeFromNib() {
        super.awakeFromNib()
        tableView.initialOpenSections = [0]
    }
    
    
    
    func setRating(offeringId: String){
        
        if  let  userSurvey = MBUserSession.shared.appSurvays?.userSurveys?.first(where: { $0.offeringId == offeringId}){
            if let survey  = MBUserSession.shared.appSurvays?.surveys?.first(where: { $0.survayId  == userSurvey.surveryId }){
                if let  question =  survey.questions?.first(where: { $0.questionId == userSurvey.questionId }){
                    if let answer = question.answers?.firstIndex(where: { $0.answerId ==  userSurvey.answerId }) {
                        switch answer {
                        case 0:
                            print(answer)
                            btn1.setImage(UIImage.init(named: "RedFilledStar"), for: .normal)
                            btn2.setImage(UIImage.init(named: "RedLineStar"), for: .normal)
                            btn3.setImage(UIImage.init(named: "RedLineStar"), for: .normal)
                            btn4.setImage(UIImage.init(named: "RedLineStar"), for: .normal)
                            btn5.setImage(UIImage.init(named: "RedLineStar"), for: .normal)
                        case 1:
                            print(answer)
                            btn1.setImage(UIImage.init(named: "RedFilledStar"), for: .normal)
                            btn2.setImage(UIImage.init(named: "RedFilledStar"), for: .normal)
                            btn3.setImage(UIImage.init(named: "RedLineStar"), for: .normal)
                            btn4.setImage(UIImage.init(named: "RedLineStar"), for: .normal)
                            btn5.setImage(UIImage.init(named: "RedLineStar"), for: .normal)
                        case 2:
                            print(answer)
                            btn1.setImage(UIImage.init(named: "RedFilledStar"), for: .normal)
                            btn2.setImage(UIImage.init(named: "RedFilledStar"), for: .normal)
                            btn3.setImage(UIImage.init(named: "RedFilledStar"), for: .normal)
                            btn4.setImage(UIImage.init(named: "RedLineStar"), for: .normal)
                            btn5.setImage(UIImage.init(named: "RedLineStar"), for: .normal)
                        case 3:
                            print(answer)
                            btn1.setImage(UIImage.init(named: "RedFilledStar"), for: .normal)
                            btn2.setImage(UIImage.init(named: "RedFilledStar"), for: .normal)
                            btn3.setImage(UIImage.init(named: "RedFilledStar"), for: .normal)
                            btn4.setImage(UIImage.init(named: "RedFilledStar"), for: .normal)
                            btn5.setImage(UIImage.init(named: "RedLineStar"), for: .normal)
                        case 4:
                            print(answer)
                            btn1.setImage(UIImage.init(named: "RedFilledStar"), for: .normal)
                            btn2.setImage(UIImage.init(named: "RedFilledStar"), for: .normal)
                            btn3.setImage(UIImage.init(named: "RedFilledStar"), for: .normal)
                            btn4.setImage(UIImage.init(named: "RedFilledStar"), for: .normal)
                            btn5.setImage(UIImage.init(named: "RedFilledStar"), for: .normal)
                        default:
                            break
                        }
                        
                    }
                }
            }
        }else{
            btn1.setImage(UIImage.init(named: "RedLineStar"), for: .normal)
            btn2.setImage(UIImage.init(named: "RedLineStar"), for: .normal)
            btn3.setImage(UIImage.init(named: "RedLineStar"), for: .normal)
            btn4.setImage(UIImage.init(named: "RedLineStar"), for: .normal)
            btn5.setImage(UIImage.init(named: "RedLineStar"), for: .normal)
        }
    }
    
    @IBAction func tabStar(_ sender: UIButton){
        
        self.delegate?.starTapped(offerId: offeringId ?? "")
    }
    
}
