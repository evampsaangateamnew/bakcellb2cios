//
//  Homepage.swift
//  Bakcell
//
//  Created by Saad Riaz on 8/30/17.
//  Copyright © 2017 evampsaanga. All rights reserved.
//

import Foundation
import ObjectMapper

struct Homepage: Mappable {
    
    var notificationUnreadCount : String = ""
    var balance : Balance?
    var mrc: Mrc?
    var credit: Credit?
    var freeResources: FreeResources?
    var installments: Installments?
    
    init?(map: Map) {
    }
    
    init() {
    }
    
    mutating func mapping(map: Map) {
        
        notificationUnreadCount     <- map["notificationUnreadCount"]
        balance                     <- map["balance"]
        mrc                         <- map["mrc"]
        credit                      <- map["credit"]
        freeResources               <- map["freeResources"]
        installments                <- map["installments"]
        
    }
    
}

struct Balance: Mappable {
    
    var prepaid: Prepaid?
    var postpaid: Postpaid?
    var minAmountTeBePaid :String?
    var minAmountLabel :String?
    
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        prepaid             <- map["prepaid"]
        postpaid            <- map["postpaid"]
        minAmountTeBePaid   <- map["minAmountTeBePaid"]
        minAmountLabel      <- map["minAmountLabel"]
    }
    
}

struct MainWallet: Mappable {
    
    var balanceTypeName: String = ""
    var currency: String = ""
    var amount: String = ""
    var effectiveTime: String = ""
    var expireTime: String = ""
    var lowerLimit: String = ""
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        balanceTypeName       <- map["balanceTypeName"]
        currency              <- map["currency"]
        amount                <- map["amount"]
        effectiveTime         <- map["effectiveTime"]
        expireTime            <- map["expireTime"]
        lowerLimit            <- map["lowerLimit"]
        
    }
    
}

struct CountryWideWallet: Mappable {
    
    var balanceTypeName: String = ""
    var currency: String = ""
    var amount: String = ""
    var effectiveTime: String = ""
    var expireTime: String = ""
    var lowerLimit: String = ""
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        balanceTypeName         <- map["balanceTypeName"]
        currency                <- map["currency"]
        amount                  <- map["amount"]
        effectiveTime           <- map["effectiveTime"]
        expireTime              <- map["expireTime"]
        lowerLimit              <- map["lowerLimit"]
        
    }
    
}

struct BounusWallet: Mappable {
    
    var balanceTypeName: String = ""
    var currency: String = ""
    var amount: String = ""
    var effectiveTime: String = ""
    var expireTime: String = ""
    var lowerLimit: String = ""
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        balanceTypeName     <- map["balanceTypeName"]
        currency            <- map["currency"]
        amount              <- map["amount"]
        effectiveTime       <- map["effectiveTime"]
        expireTime          <- map["expireTime"]
        lowerLimit          <- map["lowerLimit"]
        
    }
    
}

struct Prepaid: Mappable {
    
    var mainWallet: MainWallet?
    var countryWideWallet: CountryWideWallet?
    var bounusWallet: BounusWallet?
    
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        mainWallet          <- map["mainWallet"]
        countryWideWallet   <- map["countryWideWallet"]
        bounusWallet        <- map["bounusWallet"]
        
        
    }
    
}

struct Postpaid: Mappable {
    
    var availableBalanceCorporateValue: String = ""
    var availableBalanceIndividualValue: String = ""
    var availableCreditLabel: String = ""
    var balanceCorporateValue: String = ""
    var balanceIndividualValue: String = ""
    var balanceLabel: String = ""
    var corporateLabel: String = ""
    var currentCreditCorporateValue: String = ""
    var currentCreditIndividualValue: String = ""
    var currentCreditLabel: String = ""
    var individualLabel: String = ""
    var outstandingIndividualDebt: String = ""
    var outstandingIndividualDebtLabel: String = ""
    
    var template: String = ""
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        
        availableBalanceCorporateValue          <- map["availableBalanceCorporateValue"]
        availableBalanceIndividualValue         <- map["availableBalanceIndividualValue"]
        availableCreditLabel                    <- map["availableCreditLabel"]
        balanceCorporateValue                   <- map["balanceCorporateValue"]
        balanceIndividualValue                  <- map["balanceIndividualValue"]
        balanceLabel                            <- map["balanceLabel"]
        corporateLabel                          <- map["corporateLabel"]
        currentCreditCorporateValue             <- map["currentCreditCorporateValue"]
        currentCreditIndividualValue            <- map["currentCreditIndividualValue"]
        currentCreditLabel                      <- map["currentCreditLabel"]
        individualLabel                         <- map["individualLabel"]
        outstandingIndividualDebt               <- map["outstandingIndividualDebt"]
        outstandingIndividualDebtLabel          <- map["outstandingIndividualDebtLabel"]
        template                                <- map["template"]
        
    }
    
}

struct Mrc: Mappable {
    
    var mrcCurrency: String = ""
    var mrcDate: String = ""
    var mrcDateLabel: String = ""
    var mrcInitialDate: String = ""
    var mrcLimit: String = ""
    var mrcTitleLabel: String = ""
    var mrcTitleValue: String = ""
    var mrcType: String = ""
    var mrcStatus: String = ""
    
    init?(map: Map) {
    }
    
    mutating func mapping(map: Map) {
        
        mrcCurrency         <- map["mrcCurrency"]
        mrcDate             <- map["mrcDate"]
        mrcDateLabel        <- map["mrcDateLabel"]
        mrcInitialDate      <- map["mrcInitialDate"]
        mrcLimit            <- map["mrcLimit"]
        mrcTitleLabel       <- map["mrcTitleLabel"]
        mrcTitleValue       <- map["mrcTitleValue"]
        mrcType             <- map["mrcType"]
        mrcStatus           <- map["mrcStatus"]
        
    }
    
}

struct Credit: Mappable {
    
    var creditCurrency: String = ""
    var creditDate: String = ""
    var creditDateLabel: String = ""
    var creditInitialDate: String = ""
    var creditLimit: String = ""
    var creditTitleLabel: String = ""
    var creditTitleValue: String = ""
    var creditDays: String?
    var progressDays: String?
    var remainingCreditDays: String = ""
    
    init?(map: Map) {
    }
    
    mutating func mapping(map: Map) {
        
        creditCurrency          <- map["creditCurrency"]
        creditDate              <- map["creditDate"]
        creditDateLabel         <- map["creditDateLabel"]
        creditInitialDate       <- map["creditInitialDate"]
        creditLimit             <- map["creditLimit"]
        creditTitleLabel        <- map["creditTitleLabel"]
        creditTitleValue        <- map["creditTitleValue"]
        creditDays              <- map["creditDays"]
        progressDays            <- map["progressDays"]
        remainingCreditDays     <- map["remainingCreditDays"]
    }
    
}

struct FreeResources: Mappable {
    
    
    var freeResources: [FreeResourceAndRoaming]?
    var freeResourcesRoaming: [FreeResourceAndRoaming]?
    
    init?(map: Map) {
    }
    
    mutating func mapping(map: Map) {
        
        freeResources                   <- map["freeResources"]
        freeResourcesRoaming            <- map["freeResourcesRoaming"]
        
    }
    
}

struct FreeResourceAndRoaming: Mappable {
    
    
    var resourcesTitleLabel: String = ""
    var resourceType: String = ""
    var resourceInitialUnits: String = ""
    var resourceRemainingUnits: String = ""
    var resourceUnitName: String = ""
    var resourceDiscountedText : String = ""
    var remainingFormatted : String = ""
    
    init() {
    }
    
    init?(map: Map) {
    }
    
    mutating func mapping(map: Map) {
        
        resourcesTitleLabel             <- map["resourcesTitleLabel"]
        resourceType                    <- map["resourceType"]
        resourceInitialUnits            <- map["resourceInitialUnits"]
        resourceRemainingUnits          <- map["resourceRemainingUnits"]
        resourceUnitName                <- map["resourceUnitName"]
        resourceDiscountedText          <- map["resourceDiscountedText"]
        remainingFormatted              <- map["remainingFormatted"]
        
    }
    
}

struct Installments: Mappable{
    var installmentDescription: String = ""
    var installmentTitle: String = ""
    var installments: [Installment]?
    
    
    init?(map: Map) {
    }
    
    mutating func mapping(map: Map) {
        
        installmentDescription          <- map["installmentDescription"]
        installmentTitle                <- map["installmentTitle"]
        installments                    <- map["installments"]
    }
}

struct Installment:Mappable {
    
    
    var amountLabel: String = ""
    var amountValue: String = ""
    var currency: String = ""
    var installmentFeeLimit: String = ""
    var installmentStatus: String = ""
    var name: String = ""
    var nextPaymentInitialDate: String = ""
    var nextPaymentLabel: String = ""
    var nextPaymentValue: String = ""
    var purchaseDateLabel: String = ""
    var purchaseDateValue: String = ""
    var remainingAmountCurrentValue: String = ""
    var remainingAmountLabel: String = ""
    var remainingAmountTotalValue: String = ""
    var remainingCurrentPeriod: String = ""
    var remainingPeriodBeginDateLabel: String = ""
    var remainingPeriodBeginDateValue: String = ""
    var remainingPeriodEndDateLabel: String = ""
    var remainingPeriodEndDateValue: String = ""
    var remainingPeriodLabel: String = ""
    var remainingTotalPeriod: String = ""
    
    init?(map: Map) {
    }
    
    mutating func mapping(map: Map) {
        
        amountLabel                         <- map["amountLabel"]
        amountValue                         <- map["amountValue"]
        currency                            <- map["currency"]
        installmentFeeLimit                 <- map["installmentFeeLimit"]
        installmentStatus                   <- map["installmentStatus"]
        name                                <- map["name"]
        nextPaymentInitialDate              <- map["nextPaymentInitialDate"]
        nextPaymentLabel                    <- map["nextPaymentLabel"]
        nextPaymentValue                    <- map["nextPaymentValue"]
        purchaseDateLabel                   <- map["purchaseDateLabel"]
        purchaseDateValue                   <- map["purchaseDateValue"]
        remainingAmountCurrentValue         <- map["remainingAmountCurrentValue"]
        remainingAmountLabel                <- map["remainingAmountLabel"]
        remainingAmountTotalValue           <- map["remainingAmountTotalValue"]
        remainingCurrentPeriod              <- map["remainingCurrentPeriod"]
        remainingPeriodBeginDateLabel       <- map["remainingPeriodBeginDateLabel"]
        remainingPeriodBeginDateValue       <- map["remainingPeriodBeginDateValue"]
        remainingPeriodEndDateLabel         <- map["remainingPeriodEndDateLabel"]
        remainingPeriodEndDateValue         <- map["remainingPeriodEndDateValue"]
        remainingPeriodLabel                <- map["remainingPeriodLabel"]
        remainingTotalPeriod                <- map["remainingTotalPeriod"]
        
    }
    
}
