//
//  UsageHistoryExpandCell.swift
//  Bakcell
//
//  Created by Saad Riaz on 11/6/17.
//  Copyright © 2017 evampsaanga. All rights reserved.
//

import UIKit

class UsageHistoryExpandCell: UITableViewCell {

    @IBOutlet var number_lbl: UILabel!
    @IBOutlet var to_lbl: UILabel!
    @IBOutlet var destDetail_lbl: UILabel!
    @IBOutlet var period_lbl: UILabel!
    @IBOutlet var type_lbl: UILabel!
    @IBOutlet var destination_lbl: UILabel!
    @IBOutlet var zone_lbl: UILabel!
    @IBOutlet var peak_lbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
