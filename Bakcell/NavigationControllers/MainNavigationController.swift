//
//  MainNavigationController.swift
//  Bakcell
//
//  Created by AbdulRehman Warraich on 10/23/17.
//  Copyright © 2017 evampsaanga. All rights reserved.
//

import UIKit

class MainNavigationController: UINavigationController {
    
    //  var isPopGestureEnabled : Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        
        self.interactivePopGestureRecognizer?.isEnabled = true
        self.interactivePopGestureRecognizer?.delegate = self
    }
}

extension MainNavigationController: UIGestureRecognizerDelegate {
    
    func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        
        if let aVC = self.visibleViewController as? BaseVC {
            print("gestureRecognizerShouldBegin: \(aVC.isPopGestureEnabled)")
            return aVC.isPopGestureEnabled
        }
        return false
    }
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldBeRequiredToFailBy otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        
        if let aVC = self.visibleViewController as? BaseVC {
            return aVC.isPopGestureEnabled
        }
        return false
    }
}

