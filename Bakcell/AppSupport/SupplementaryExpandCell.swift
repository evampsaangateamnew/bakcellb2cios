//
//  SupplementaryExpandCell.swift
//  Bakcell
//
//  Created by Saad Riaz on 7/5/17.
//  Copyright © 2017 evampsaanga. All rights reserved.
//

import UIKit
import MarqueeLabel

class SupplementaryExpandCell: UITableViewCell {

    static let identifier = "SupplementaryExpandCell"

    @IBOutlet var myContentView: UIView!
    
    // ALL main view outlet for difftrent types
    @IBOutlet var OfferGroupView: UIView!
    @IBOutlet var ValidityView: UIView!


    // OfferGroup view iner outlets
    @IBOutlet var groupName_lbl: MarqueeLabel!
    @IBOutlet var mobile_img: UIImageView!
    @IBOutlet var tablet_img: UIImageView!
    @IBOutlet var desktop_lbl: UIImageView!

    // Validity view iner outlets
    @IBOutlet var validity_lbl: MarqueeLabel!
    @IBOutlet var priceView: UIView!
    @IBOutlet var price_lbl: UILabel!
    @IBOutlet var manatSign_lbl: UILabel!
    @IBOutlet var manatSignWidth: NSLayoutConstraint!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.separatorInset = UIEdgeInsets.zero
        // Setting background color
        self.myContentView.backgroundColor = UIColor.MBDimLightGrayColor
        self.OfferGroupView.backgroundColor = UIColor.MBDimLightGrayColor
        self.ValidityView.backgroundColor = UIColor.MBDimLightGrayColor

        // Lable textColor
        self.groupName_lbl.textColor = UIColor.MBDarkGrayColor
        self.price_lbl.textColor = UIColor.MBDarkGrayColor
        self.manatSign_lbl.textColor = UIColor.MBDarkGrayColor

        groupName_lbl.setupMarqueeAnimation()
        validity_lbl.setupMarqueeAnimation()

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func setOfferGroup(offerGroup : OfferGroup?) {

        //"Desktop"
        //"Mobile"
        // Hiding all views first
        hideAllViews()

        if let offerGroup = offerGroup {

            OfferGroupView.isHidden = false

            groupName_lbl.text = offerGroup.groupName ?? ""

            mobile_img.image = UIImage(named: "device1b")
            tablet_img.image = UIImage(named: "device2b")
            desktop_lbl.image = UIImage(named: "device3b")

            if let groupValue = offerGroup.groupValue {

                if groupValue.isEqualToStringIgnoreCase(otherString: "Mobile") {

                    mobile_img.image = UIImage(named: "device1a")

                } else if groupValue.isEqualToStringIgnoreCase(otherString: "Desktop") {

                    desktop_lbl.image = UIImage(named: "device3a")

                } else if groupValue.isEqualToStringIgnoreCase(otherString: "tab") {
                    tablet_img.image = UIImage(named: "device2a")
                }
            }
            
        } else {
            OfferGroupView.isHidden = true
        }
    }

    func setOfferValidity(header : Header?) {

        // Hiding all views first
        hideAllViews()

        ValidityView.isHidden = false
        priceView.isHidden = false
        validity_lbl.textColor = UIColor.MBTextGrayColor

        if let header = header{
            var validityTitleString : String = ""

            if let validityTitle = header.validityTitle {

                if !validityTitle.isBlank {
                    validityTitleString = validityTitle + ": "
                }
            }

            if let validityValue = header.validityValue {
                validity_lbl.text = validityTitleString + validityValue
            }

            if let price = header.price {
                price_lbl.text = price
            }

        }
    }

    func setOfferStatus(header : SubscriptionHeader?) {

        // Hiding all views first
        hideAllViews()

        ValidityView.isHidden = false

        if let header = header {
            priceView.isHidden = false
            validity_lbl.textColor = UIColor.MBGreen

            if header.isFreeResource?.isEqualToStringIgnoreCase(otherString: "true") == true {

                validity_lbl.text = header.status ?? ""
                price_lbl.text = Localized("Header_FREE")
                manatSignWidth.constant = 0

            } else {
                validity_lbl.text = header.status ?? ""
                price_lbl.text = header.price ?? ""

                if header.price?.isBlank == true {
                    manatSignWidth.constant = 0
                } else {
                    manatSignWidth.constant = 8
                }
            }

        } else {
            validity_lbl.text = ""
            priceView.isHidden = true
        }


    }

    func setOfferType(header : Header?) {

        // Hiding all views first
        hideAllViews()

        OfferGroupView.isHidden = false

        if let header = header {

            if let type = header.type {

                //                Internet --  "Интернет" --  "İnternet"
                //                Call   --  "Звонки" --  "Zənglər"
                //                Campaign  --  "Кампании" --  "Kampaniyalar"

                mobile_img.image = UIImage(named: "roamingcallb")
                tablet_img.image = UIImage(named: "roaminginternetb")
                desktop_lbl.image = UIImage(named: "romaingcampb")

                if type.isEqualToStringIgnoreCase(otherString:"Call") ||
                    type.isEqualToStringIgnoreCase(otherString:"Звонки") ||
                    type.isEqualToStringIgnoreCase(otherString:"Zənglər") {

                    mobile_img.image = UIImage(named: "roamingcalla")
                    groupName_lbl.text = Localized("Call")

                } else if type.isEqualToStringIgnoreCase(otherString: "Internet") ||
                    type.isEqualToStringIgnoreCase(otherString: "Кампании") ||
                    type.isEqualToStringIgnoreCase(otherString: "Kampaniyalar") {

                    tablet_img.image = UIImage(named: "roaminginterneta")
                    groupName_lbl.text = Localized("Internet")

                } else if type.isEqualToStringIgnoreCase(otherString: "Campaign") ||
                    type.isEqualToStringIgnoreCase(otherString: "Кампании") ||
                    type.isEqualToStringIgnoreCase(otherString: "Kampaniyalar") {

                    desktop_lbl.image = UIImage(named: "romaingcampa")
                    groupName_lbl.text = Localized("Campaign")
                }
            }
        }
    }

    func setOfferType(header : SubscriptionHeader?) {

        // Hiding all views first
        hideAllViews()

        OfferGroupView.isHidden = false

        if let header = header {

            if let type = header.type {

                mobile_img.image = UIImage(named: "roamingcallb")
                tablet_img.image = UIImage(named: "roaminginternetb")
                desktop_lbl.image = UIImage(named: "romaingcampb")

                if type.isEqualToStringIgnoreCase(otherString:"Call") ||
                    type.isEqualToStringIgnoreCase(otherString:"Звонки") ||
                    type.isEqualToStringIgnoreCase(otherString:"Zənglər") {

                    mobile_img.image = UIImage(named: "roamingcalla")

//                    // Using DropDown_Voice for call text insted of calls
//                    groupName_lbl.text = Localized("DropDown_Voice")

                    groupName_lbl.text = Localized("Call")
                } else if type.isEqualToStringIgnoreCase(otherString: "Internet") ||
                    type.isEqualToStringIgnoreCase(otherString: "Кампании") ||
                    type.isEqualToStringIgnoreCase(otherString: "Kampaniyalar") {

                    tablet_img.image = UIImage(named: "roaminginterneta")
                    groupName_lbl.text = Localized("Internet")

                } else if type.isEqualToStringIgnoreCase(otherString: "Campaign") ||
                    type.isEqualToStringIgnoreCase(otherString: "Кампании") ||
                    type.isEqualToStringIgnoreCase(otherString: "Kampaniyalar") {
                    
                    desktop_lbl.image = UIImage(named: "romaingcampa")
                    groupName_lbl.text = Localized("Campaign")
                }
            }
        }
    }

    func hideAllViews() {
        OfferGroupView.isHidden = true
        ValidityView.isHidden = true
    }
    
}
