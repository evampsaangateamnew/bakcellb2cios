//
//  RateUsModel.swift
//  Bakcell
//
//  Created by AbdulRehman Warraich on 1/31/19.
//  Copyright © 2019 evampsaanga. All rights reserved.
//

import ObjectMapper

struct RateUsModel : Mappable {
    var rateus_android : String?
    var rateus_ios : String?
    var pic_tnc : String?
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        
        rateus_android <- map["rateus_android"]
        rateus_ios <- map["rateus_ios"]
        pic_tnc <- map["pic_tnc"]
    }
    
}
