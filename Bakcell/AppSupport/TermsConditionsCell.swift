//
//  TermsConditionsCell.swift
//  Bakcell
//
//  Created by Saad Riaz on 9/21/17.
//  Copyright © 2017 evampsaanga. All rights reserved.
//

import UIKit

class TermsConditionsCell: UITableViewCell {

    @IBOutlet var amount_lbl: UILabel!
    @IBOutlet var terms_lbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
