//
//  RequestHistoryVC.swift
//  Bakcell
//
//  Created by Saad Riaz on 6/9/17.
//  Copyright © 2017 evampsaanga. All rights reserved.
//

import UIKit

import ObjectMapper

class RequestHistoryVC: BaseVC {
    
    var loanHistryResponse : LoanHistryListHandler?
    
    @IBOutlet var dateDropDown: UIDropDown!
    @IBOutlet var pageCount: UILabel!
    @IBOutlet var tableView: MBAccordionTableView!
    
    @IBOutlet var remaining_lbl: UILabel!
    @IBOutlet var paid_lbl: UILabel!
    @IBOutlet var status_lbl: UILabel!
    @IBOutlet var date_lbl: UILabel!
    
    //MARK:- ViewController Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.allowMultipleSectionsOpen = true
        tableView.register(UINib(nibName: "RequestHistoryExpandCell", bundle: nil), forCellReuseIdentifier: "expandID")
        tableView.register(UINib(nibName: "RequestHistoryHeaderView", bundle: nil), forHeaderFooterViewReuseIdentifier: RequestHistoryHeaderView.identifier)
        tableView.delegate = self
        tableView.dataSource = self
        
        self.reloadTableViewData()
        
        
        // Get data of current days by default
        
        let todayDate = MBUtilities.todayDateString(dateFormate: Constants.kAPIFormat)
        self.getLoanHistryList(startDate: todayDate, endDate: todayDate)
        
        //Drop Down
        dateDropDown.optionsTextAlignment = NSTextAlignment.center
        dateDropDown.textAlignment = NSTextAlignment.center
        
        dateDropDown.placeholder = Localized("DropDown_CurrentDay")
        self.dateDropDown.rowBackgroundColor = UIColor.clear
        self.dateDropDown.borderWidth = 0
        self.dateDropDown.tableHeight = 140
        dateDropDown.fontSize = 12
        
        dateDropDown.options = [Localized("DropDown_CurrentDay"),Localized("DropDown_Last7Days"), Localized("DropDown_Last30Days"), Localized("DropDown_Last90Days")]
        dateDropDown.didSelect { (option, index) in
            
            MBUserSession.shared.dateFormatter.dateFormat = Constants.kAPIFormat
            
            let todayDate = MBUtilities.todayDateString(dateFormate: Constants.kAPIFormat)
            
            switch index{
                
            case 0:
                
                // Get data of Today
                self.getLoanHistryList(startDate: todayDate, endDate: todayDate)
                
            case 1:
                // Get data of previous seven days
                
                let sevenDaysAgo = MBUtilities.todayDateString(withAdditionalValue: -7)
                self.getLoanHistryList(startDate: sevenDaysAgo, endDate: todayDate)
                
                
            case 2:
                // Get data of previous thirty days
                
                let thirtyDaysAgo = MBUtilities.todayDateString(withAdditionalValue: -30)
                self.getLoanHistryList(startDate: thirtyDaysAgo, endDate: todayDate)
                
                
            case 3:
                // Get data of previous ninety days
                
                let ninetyDaysAgo = MBUtilities.todayDateString(withAdditionalValue: -90)
                self.getLoanHistryList(startDate: ninetyDaysAgo, endDate: todayDate)
                
            default:
                print("destructive")
                
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        loadViewLayout()
    }
    
    //MARK: - FUNCTIONS
    
    func loadViewLayout(){
        date_lbl.font = UIFont.MBArial(fontSize: 14)
        status_lbl.font = UIFont.MBArial(fontSize: 14)
        paid_lbl.font = UIFont.MBArial(fontSize: 14)
        remaining_lbl.font = UIFont.MBArial(fontSize: 14)
        
        date_lbl.text = Localized("RQ_Date")
        status_lbl.text = Localized("RQ_Status")
        paid_lbl.text = Localized("RQ_Paid")
        remaining_lbl.text = Localized("RQ_Remaining")
    }
    
    func reloadTableViewData(){
        
        if (loanHistryResponse?.loanHistory?.count ?? 0) <= 0 {
            self.tableView.showDescriptionViewWithImage(description: Localized("Message_NoDataAvalible"))
            
        } else {
            self.tableView.hideDescriptionView()
        }
        self.tableView.reloadData()
        showPageNumber()
    }
}

// MARK: - <UITableViewDataSource> / <UITableViewDelegate> -

extension RequestHistoryVC : UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return (loanHistryResponse?.loanHistory?.count) ?? 0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100;
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 52.0
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "expandID", for: indexPath) as? RequestHistoryExpandCell {
            
            cell.label1.text = Localized("Expand_CreditID")
            cell.label2.text = loanHistryResponse?.loanHistory?[indexPath.section].loanID
            cell.amount_lbl.text = Localized("RQ_Amount")
            cell.amountValue_lbl.attributedText = MBUtilities.createAttributedTextWithManatSign(loanHistryResponse?.loanHistory?[indexPath.section].amount ?? "", textColor: .black)
            
            return cell
        } else {
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: RequestHistoryHeaderView.identifier) as? RequestHistoryHeaderView {
            
            headerView.dateTimeLb.font = UIFont.MBArial(fontSize: 14)
            headerView.statusLb.font = UIFont.MBArial(fontSize: 14)
            headerView.paidLb.font = UIFont.MBArial(fontSize: 14)
            headerView.remainingLb.font = UIFont.MBArial(fontSize: 14)
            
            
            headerView.dateTimeLb.text = loanHistryResponse?.loanHistory?[section].dateTime
            headerView.statusLb.text = loanHistryResponse?.loanHistory?[section].status
            headerView.paidLb.text = loanHistryResponse?.loanHistory?[section].paid
            headerView.remainingLb.attributedText = MBUtilities.createAttributedTextWithManatSign(loanHistryResponse?.loanHistory?[section].remaining ?? "", textColor: .black)
            
            return headerView
        } else {
            return UIView()
        }
    }
    
    //MARK: - API Calls
    /// Call 'getLoanHistryList' API .
    ///
    /// - returns: LoanHistryList
    func getLoanHistryList(startDate:String?, endDate:String?) {
        
        activityIndicator.showActivityIndicator()
        
        _ = MBAPIClient.sharedClient.getLoanHistory(StartDate: startDate ?? "", EndDate: endDate ?? "", { (response, resultData, error, isCancelled, status, resultCode, resultDesc) in
            
            self.activityIndicator.hideActivityIndicator()
            if error != nil {
                //EventLog
                MBAnalyticsManager.logEvent(screenName: "Loan Screen", contentType:"Loan Request History Screen" , status:"Failure" )
                
                error?.showServerErrorInViewController(self)
                
            } else {
                // handling data from API response.
                if resultCode == Constants.MBAPIStatusCode.succes.rawValue {
                    //EventLog
                    MBAnalyticsManager.logEvent(screenName: "Loan Screen", contentType:"Loan Request History Screen" , status:"Success" )
                    
                    
                    // Parssing response data
                    if let loanHistryList = Mapper<LoanHistryListHandler>().map(JSONObject: resultData){
                        
                        self.loanHistryResponse = loanHistryList
                        
                    }
                } else {
                    //EventLog
                    MBAnalyticsManager.logEvent(screenName: "Loan Screen", contentType:"Loan Request History Screen" , status:"Failure" )
                    
                    // Show error alert to user
                    self.showErrorAlertWithMessage(message: resultDesc)
                }
            }
            
            // Reload tableView data after API response
            self.reloadTableViewData()
        })
    }
}

// MARK: - <MBAccordionTableViewDelegate> 

extension RequestHistoryVC : MBAccordionTableViewDelegate {
    
    
    func tableView(_ tableView: MBAccordionTableView, didOpenSection section: Int, withHeader header: UITableViewHeaderFooterView?) {
        
        showPageNumber()
    }
    
    func tableView(_ tableView: MBAccordionTableView, didCloseSection section: Int, withHeader header: UITableViewHeaderFooterView?) {
        
        showPageNumber()
    }
    
    func tableView(_ tableView: MBAccordionTableView, canInteractWithHeaderAtSection section: Int) -> Bool {
        return true
    } 
}

// MARK: UIScrollViewDelegate
extension RequestHistoryVC:UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        showPageNumber()
    }
    
    func showPageNumber() {
        
        pageCount.text = "\(tableView.lastVisibleSection())/\(loanHistryResponse?.loanHistory?.count ?? 0)"
        
    }
}
