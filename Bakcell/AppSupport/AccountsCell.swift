//
//  AccountsCell.swift
//  Bakcell
//
//  Created by Saad Riaz on 6/15/17.
//  Copyright © 2017 evampsaanga. All rights reserved.
//

import UIKit

class AccountsCell: UITableViewCell {

    
    @IBOutlet var img: UIImageView!
    @IBOutlet var title_lbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
