//
//  FFCell.swift
//  Bakcell
//
//  Created by Saad Riaz on 6/21/17.
//  Copyright © 2017 evampsaanga. All rights reserved.
//

import UIKit

class FFCell: UITableViewCell {
    
    @IBOutlet var number_lbl: UILabel!
    @IBOutlet var date_lbl: UILabel!
    @IBOutlet var delete_btn: UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
