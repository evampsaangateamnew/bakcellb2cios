//
//  SupplementaryOffersHeaderView.swift
//  Bakcell
//
//  Created by AbdulRehman Warraich on 9/12/17.
//  Copyright © 2017 evampsaanga. All rights reserved.
//

import UIKit
import MarqueeLabel

class SupplementaryOffersHeaderView: MBAccordionTableViewHeaderView {
    
    //MARK:- Properties
    static let identifier : String = "SupplementaryOffersHeaderView"
    
    var viewType : MBSectionType = MBSectionType.UnSpecified
    
    //MARK: - IBOutlet
    @IBOutlet var cView: UIView!
    @IBOutlet var containorViewHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet var stickerView: UIView!
    @IBOutlet var sticker_lbl: UILabel!
    @IBOutlet var stickerWidthConstraint: NSLayoutConstraint!
    @IBOutlet var stickerHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet var detailView: UIView!
    @IBOutlet var offer_lbl: MarqueeLabel!
    
    @IBOutlet var detailSign_btn: UIButton!
    
    //MARK: - View Methods
    override func awakeFromNib() {
        super.awakeFromNib()
        offer_lbl.setupMarqueeAnimation()
        
    }
    
    //MARK: - Functions
    class func instanceFromNib() -> UIView {
        return UINib(nibName: "SupplementaryOffersHeaderView", bundle: Bundle.main).instantiate(withOwner: nil, options: nil)[0] as! SupplementaryOffersHeaderView
    }
    
    
    func setViewForOfferNameWithSickerValues(offerName : String?, stickerTitle : String?,stickerColorCode : String? ) {
        
        //View Type
        viewType = MBSectionType.Header
        
        //Hiding subscribe button
        detailSign_btn.isHidden = true
        
        
        detailView.isHidden = false
        offer_lbl.isHidden = false
        
        // Setting top sticker
        if let stickerTitle = stickerTitle {
            
            stickerHeightConstraint.constant = 20
            //            topSpaceConstraint.constant = 16
            stickerView.isHidden = false
            stickerView.roundTopCorners(radius: 8)
            
            sticker_lbl.text = stickerTitle
            
            if  let stickerColorCode = stickerColorCode {
                
                // If sticker text is "" empty string
                if !stickerColorCode.isEmpty {
                    stickerView.backgroundColor = UIColor(hexString: stickerColorCode)
                } else {
                    // Set default value
                    stickerView.backgroundColor = UIColor.MBGreen
                }
                
            } else {
                // Set default value
                stickerView.backgroundColor = UIColor.MBGreen
            }
            
        } else {
            stickerHeightConstraint.constant = 0
            //            topSpaceConstraint.constant = 0
            stickerView.isHidden = true
        }
        
        //Seting Detailview
        detailView.roundTopCorners(radius: 8)
        detailView.backgroundColor = UIColor.MBDimLightGrayColor
        
        
        // Setting offer name
        if let offerName = offerName {
            offer_lbl.text = offerName
            offer_lbl.font = UIFont.MBArial(fontSize: 14)
        }
    }
    
    func setViewWithTitle(title : String?, isSectionSelected: Bool, headerType:MBSectionType ) {
        
        //View Type
        viewType = headerType
        
        if isSectionSelected {
            detailSign_btn.setImage(#imageLiteral(resourceName: "minu-sign"), for: UIControl.State.normal)
        } else {
            detailSign_btn.setImage(#imageLiteral(resourceName: "Plus-Sign"), for: UIControl.State.normal)
        }
        
        //Hiding subscribe button
        stickerView.isHidden = true
        
        detailView.isHidden = false
        offer_lbl.isHidden = false
        detailSign_btn.isHidden = false
        
        stickerHeightConstraint.constant = 0
        //topSpaceConstraint.constant = 0
        
        //Setting Detailview
        self.layoutIfNeeded()
        detailView.roundTopCorners(radius: 0.0)
        detailView.backgroundColor = UIColor.MBDimLightGrayColor
        
        // Setting offer name
        if let title = title {
            offer_lbl.text = title
            offer_lbl.font = UIFont.MBArial(fontSize: 14.0)
        } else {
            offer_lbl.text = ""
        }
    }
    
    func setSectionSelectedIcon(_ isSelected : Bool) {
        if isSelected {
            detailSign_btn.setImage(#imageLiteral(resourceName: "minu-sign"), for: UIControl.State.normal)
        } else {
            detailSign_btn.setImage(#imageLiteral(resourceName: "Plus-Sign"), for: UIControl.State.normal)
        }
    }
}
