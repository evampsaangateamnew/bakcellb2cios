//
//  CoreServicesVC.swift
//  Bakcell
//
//  Created by Saad Riaz on 6/6/17.
//  Copyright © 2017 evampsaanga. All rights reserved.
//

import UIKit
import KYDrawerController
import ObjectMapper

class CoreServicesVC: BaseVC {
    
    enum MBCoreServiceType: String {
        case internet       = "Internet"
        case suspend        = "suspend"
        case coreService    = "coreService"
        case UnSpecified        = ""
    }
    
    var coreServicesData :  [CoreService]?
    
    //MARK:- IBOutlet
    @IBOutlet var title_lbl: UILabel!
    @IBOutlet var tableView: UITableView!
    
    //MARK:- ViewController Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.separatorColor = UIColor.clear
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 180.0
        tableView.allowsMultipleSelection = false
        tableView.delegate = self
        tableView.dataSource = self
        
        // Loading Core survice information
        getCoreServices()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.interactivePop(true)
        
        // Layout with localized string
        layoutViewController()
    }
    
    //MARK:- .isEnabled = isEnabled
    @IBAction func settingsBtn(_ sender: Any) {
        
        if let aCoreServiceSection = coreServicesData?[0] {
            
            if aCoreServiceSection.coreServicesList?.indices.contains(0) ?? false {
                
                if let aCoreServiceItem = aCoreServiceSection.coreServicesList?[0] {
                    let ForwardNumber = self.myStoryBoard.instantiateViewController(withIdentifier: "ForwardNumberVC") as! ForwardNumberVC
                    ForwardNumber.selectedCoreServiceListItem = aCoreServiceItem
                    ForwardNumber.setCompletionHandler(completionBlock: { (newNumber) in
                        self.updateCoreServiceForwordNumber(section: 0, row: 0, newForwardNumber: newNumber)
                    })
                    self.navigationController?.pushViewController(ForwardNumber, animated: true)
                }
            }
        }
        
    }
    @IBAction func suspendBtn(_ sender: Any) {
        // User logout
        let alert = self.myStoryBoard.instantiateViewController(withIdentifier: "AlertVC") as! AlertVC
        self.presentPOPUP(alert, animated: true, completion: nil)
        
        alert.setAlertForSuspendNumber()
        
        // confirmation button completion handler
        alert.setYesButtonCompletionHandler { (aString) in
            self.reportLostSim()
        }
    }
    
    @IBAction func sendPressed(_ sender: Any) {
        // calling API to send internet setting
        sendInternetSettings()
    }
    
    //MARK: - Functions
    func layoutViewController() {
        title_lbl.text = Localized("Title_CoreServices")
    }
    
    func reloadTableView() {
        tableView.isHidden = false
        tableView.reloadData()
    }
    
    
    func toActiveOrInActiveString(string: String?, isActive : Bool = false) -> String {
        
        if string?.isBlank  ?? true {
            return Localized("Info_Inactive")
        }
        
        if isActive {
            return Localized("Info_Active")
        } else {
            return Localized("Info_Inactive")
        }
        
    }
    
    //MARK: - API Calls
    /// Call 'getCoreServices' API .
    ///
    /// - returns: Void
    func getCoreServices() {
        
        /*Loading initial data from UserDefaults*/
        if let coreServicesResponse :[CoreService] = CoreService.loadArrayFromUserDefaults(key: APIsType.CoreServices.selectedLocalizedAPIKey()) {
            
            // Setting Data in ViewController
            self.coreServicesData = coreServicesResponse
            self.reloadTableView()
            
        } else {
            activityIndicator.showActivityIndicator()
        }
        
        _ = MBAPIClient.sharedClient.getCoreServices(accountType: MBUserSession.shared.customerType, groupType: MBUserSession.shared.groupType, { (response, resultData, error, isCancelled, status, resultCode, resultDesc) in
            
            self.activityIndicator.hideActivityIndicator()
            
            if error != nil {
                error?.showServerErrorInViewController(self)
                
            } else {
                // handling data from API response.
                if resultCode == Constants.MBAPIStatusCode.succes.rawValue {
                    
                    if let coreServicesResponseHandler = Mapper<CoreServices>().map(JSONObject: resultData) {
                        // Setting Data in ViewController
                        self.coreServicesData = coreServicesResponseHandler.coreServices
                        
                        // Save updated core services information
                        MBUserInfoUtilities.saveJSONStringInToUserDefaults(jsonString: coreServicesResponseHandler.coreServices?.toJSONString(), key: APIsType.CoreServices.selectedLocalizedAPIKey())
                    }
                } else {
                    // Show error alert to user
                    self.showErrorAlertWithMessage(message: resultDesc)
                }
            }
            self.reloadTableView()
        })
    }
    
    /// Call 'sendInternetSettings' API .
    ///
    /// - returns: Void
    func sendInternetSettings() {
        activityIndicator.showActivityIndicator()
        _ = MBAPIClient.sharedClient.sendInternetSettings({ (response, resultData, error, isCancelled, status, resultCode, resultDesc) in
            self.activityIndicator.hideActivityIndicator()
            
            if error != nil {
                error?.showServerErrorInViewController(self)
                
            } else {
                // handling data from API response.
                if resultCode == Constants.MBAPIStatusCode.succes.rawValue {
                    
                    if let responseHandler = Mapper<MessageResponse>().map(JSONObject: resultData) {
                        
                        // Success Alert
                        self.showSuccessAlertWithMessage(message: responseHandler.message)
                        
                    } else {
                        self.showErrorAlertWithMessage(message: resultDesc)
                    }
                }
            }
        })
    }
    
    /// Call 'reportLostSim' API .
    ///
    /// - returns: Void
    func reportLostSim() {
        
        activityIndicator.showActivityIndicator()
        _ = MBAPIClient.sharedClient.reportLostSim({ (response, resultData, error, isCancelled, status, resultCode, resultDesc) in
            
            self.activityIndicator.hideActivityIndicator()
            
            if error != nil {
                error?.showServerErrorInViewController(self)
                
            } else {
                // handling data from API response.
                if resultCode == Constants.MBAPIStatusCode.succes.rawValue {
                    
                    if let responseHandler = Mapper<MessageResponse>().map(JSONObject: resultData) {
                        // Success Alert
                        self.showSuccessAlertWithMessage(message: responseHandler.message)
                        
                    } else {
                        self.showErrorAlertWithMessage(message: resultDesc ?? "")
                    }
                } else {
                    self.showErrorAlertWithMessage(message: resultDesc)
                }
            }
        })
    }
    
    @objc func switchChanged(mySwitch: MBSwitch) {
        
        //        let alert = self.myStoryBoard.instantiateViewController(withIdentifier: "AlertVC") as! AlertVC
        //        self.presentPOPUP(alert, animated: true, completion: nil)
        //
        //        alert.setAlertForActivateSuspend(forActive: mySwitch.isOn)
        //
        //        // confirmation button completion handler
        //        alert.setYesButtonCompletionHandler(completionBlock: { (aString) in
        
        self.activityIndicator.showActivityIndicator()
        
        let section = mySwitch.sectionTag
        let row = mySwitch.rowTag
        
        if self.coreServicesData?.indices.contains(section) ?? false {
            
            let aCatagoryType = self.coreServicesData?[section]
            
            if aCatagoryType?.coreServicesList?.indices.contains(row) ?? false {
                
                if let aCoreService = aCatagoryType?.coreServicesList?[row] {
                    
                    if aCoreService.offeringId.isEqualToStringIgnoreCase(otherString: Constants.K_CallDivert_Forward) &&
                        mySwitch.isOn == false {
                        
                        _ = MBAPIClient.sharedClient.changeSupplementaryOffering(actionType: false, offeringId: aCoreService.offeringId, offerName: aCatagoryType?.coreServiceCategory ?? "", { (response, resultData, error, isCancelled, status, resultCode, resultDesc) in
                            
                            self.activityIndicator.hideActivityIndicator()
                            
                            if error != nil {
                                if mySwitch.isOn{
                                    //EventLog
                                    MBAnalyticsManager.logEvent(screenName: "Services Screen", contentType:"Services Core Services Screen" , status:"Services Enable Failure")
                                } else{
                                    MBAnalyticsManager.logEvent(screenName: "Services Screen", contentType:"Services Core Services Screen" , status:"Services Disable Failure")
                                }
                                
                                error?.showServerErrorInViewController(self)
                                // changing back to first value
                                mySwitch.setOn(!mySwitch.isOn, animated: true)
                            } else {
                                
                                // handling data from API response.
                                if resultCode == Constants.MBAPIStatusCode.succes.rawValue {
                                    if mySwitch.isOn{
                                        //EventLog
                                        MBAnalyticsManager.logEvent(screenName: "Services Screen", contentType:"Services Core Services Screen" , status:"Services Enable Success")
                                    }
                                    else{
                                        MBAnalyticsManager.logEvent(screenName: "Services Screen", contentType:"Services Core Services Screen" , status:"Services Disable Success")
                                    }
                                    
                                    // Update current status in local storage
                                    self.updateCoreServiceStatus(section: section, row: row, isActive: mySwitch.isOn)
                                    
                                    // Success Alert // No Result Message
                                    //self.showSuccessAlertWithMessage(message: resultDesc)
                                    
                                } else {
                                    
                                    if mySwitch.isOn{
                                        //EventLog
                                        MBAnalyticsManager.logEvent(screenName: "Services Screen", contentType:"Services Core Services Screen" , status:"Services Enable Failure")
                                    } else{
                                        MBAnalyticsManager.logEvent(screenName: "Services Screen", contentType:"Services Core Services Screen" , status:"Services Disable Failure")
                                    }
                                    
                                    // Error Alert
                                    self.showErrorAlertWithMessage(message: resultDesc)
                                    
                                    // changing back to first value
                                    mySwitch.setOn(!mySwitch.isOn, animated: true)
                                }
                            }
                        })
                        
                    } else {
                        
                        // Number will be empty in this case
                        _ = MBAPIClient.sharedClient.processCoreServices(actionType: mySwitch.isOn, offeringId: aCoreService.offeringId, number: "", accountType: MBUserSession.shared.customerType, groupType: MBUserSession.shared.groupType, { (response, resultData, error, isCancelled, status, resultCode, resultDesc) in
                            
                            self.activityIndicator.hideActivityIndicator()
                            
                            if error != nil {
                                if mySwitch.isOn{
                                    //EventLog
                                    MBAnalyticsManager.logEvent(screenName: "Services Screen", contentType:"Services Core Services Screen" , status:"Services Enable Failure")
                                } else{
                                    MBAnalyticsManager.logEvent(screenName: "Services Screen", contentType:"Services Core Services Screen" , status:"Services Disable Failure")
                                }
                                
                                error?.showServerErrorInViewController(self)
                                // changing back to first value
                                mySwitch.setOn(!mySwitch.isOn, animated: true)
                            } else {
                                
                                // handling data from API response.
                                if resultCode == Constants.MBAPIStatusCode.succes.rawValue {
                                    if mySwitch.isOn{
                                        //EventLog
                                        MBAnalyticsManager.logEvent(screenName: "Services Screen", contentType:"Services Core Services Screen" , status:"Services Enable Success")
                                    }
                                    else{
                                        MBAnalyticsManager.logEvent(screenName: "Services Screen", contentType:"Services Core Services Screen" , status:"Services Disable Success")
                                    }
                                    
                                    // Update current status in local storage
                                    self.updateCoreServiceStatus(section: section, row: row, isActive: mySwitch.isOn)
                                    
                                    // Success Alert // No message
                                    //  self.showSuccessAlertWithMessage(message: resultDesc)
                                    self.showSuccessAlertWithMessage(message: resultDesc)
                                    
                                } else {
                                    
                                    if mySwitch.isOn {
                                        //EventLog
                                        MBAnalyticsManager.logEvent(screenName: "Services Screen", contentType:"Services Core Services Screen" , status:"Services Enable Failure")
                                    } else{
                                        MBAnalyticsManager.logEvent(screenName: "Services Screen", contentType:"Services Core Services Screen" , status:"Services Disable Failure")
                                    }
                                    
                                    // Error Alert
                                    if let coreServicesResponseHandler = Mapper<CoreServicesResponse>().map(JSONObject: resultData) {
                                        self.showSuccessAlertWithMessage(message: coreServicesResponseHandler.serverMessage)
                                    } else {
                                        self.showErrorAlertWithMessage(message: resultDesc)
                                    }
                                    
                                    // changing back to first value
                                    mySwitch.setOn(!mySwitch.isOn, animated: true)
                                }
                                
                            }
                        })
                    }
                }
            }
        }
        //        })
        //
        //        // Calncel botton
        //        alert.setNoButtonCompletionHandler { (aString) in
        //            // changing back to first value
        //            mySwitch.setOn(!mySwitch.isOn, animated: true)
        //        }
        
        
    }
    
    //MARK: - Functions
    /* func createTagForSwitch(section: Int, row: Int) -> Int {
     let tagString = "\(section+1)909\(row)"
     return tagString.toInt
     }
     func getSectionAndRowForTag(tag: Int) -> (Int,Int) {
     let tagString = "\(tag)"
     let tagArray = tagString.components(separatedBy: "909")
     return ((tagArray.first?.toInt ?? 0) - 1, tagArray.last?.toInt ?? 0)
     }*/
    
    func updateCoreServiceStatus(section : Int, row : Int, isActive : Bool = false) {
        
        if coreServicesData?.indices.contains(section) ?? false {
            
            let aCatagoryType = coreServicesData?[section]
            
            if aCatagoryType?.coreServicesList?.indices.contains(row) ?? false {
                
                if let aCoreService = aCatagoryType?.coreServicesList?[row] {
                    aCoreService.status = self.toActiveOrInActiveString(string: aCoreService.status, isActive: isActive)
                    
                    aCatagoryType?.coreServicesList?[row] = aCoreService
                    
                    coreServicesData?[section] = aCatagoryType!
                    
                    // Save updated core services information
                    MBUserInfoUtilities.saveJSONStringInToUserDefaults(jsonString: coreServicesData?.toJSONString(), key: APIsType.CoreServices.selectedLocalizedAPIKey())
                }
            }
        }
        
        // Reload TableView Data
        //self.tableView.reloadData()
    }
    
    func updateCoreServiceForwordNumber(section : Int, row : Int, newForwardNumber : String = "") {
        
        if coreServicesData?.indices.contains(section) ?? false {
            
            let aCatagoryType = coreServicesData?[section]
            
            if aCatagoryType?.coreServicesList?.indices.contains(row) ?? false {
                
                if let aCoreService = aCatagoryType?.coreServicesList?[row] {
                    aCoreService.forwardNumber = newForwardNumber
                    aCoreService.status = self.toActiveOrInActiveString(string: aCoreService.status, isActive: true)
                    
                    aCatagoryType?.coreServicesList?[row] = aCoreService
                    
                    coreServicesData?[section] = aCatagoryType!
                    
                    // Save updated core services information
                    
                    MBUserInfoUtilities.saveJSONStringInToUserDefaults(jsonString: coreServicesData?.toJSONString(), key: APIsType.CoreServices.selectedLocalizedAPIKey())
                }
            }
        }
        // Reload TableView Data
        self.tableView.reloadData()
    }
}

//MARK:- TABLE VIEW DELEGATE METHODS
extension CoreServicesVC : UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        // Adding one additional section for internet setting.
        
        return self.typeOfDataInCoreService(allCoreServices: coreServicesData).count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        let allTypeOfData = self.typeOfDataInCoreService(allCoreServices: coreServicesData)
        
        if allTypeOfData[section] == .internet || allTypeOfData[section] == .suspend { // Check if section is second last
            return 1
        } else {
            return coreServicesData?[section].coreServicesList?.count ?? 0
        }
        
    }
    
    func typeOfDataInCoreService(allCoreServices: [CoreService]?) -> [MBCoreServiceType] {
        
        // Atleast one because there is one with offer name
        
        var dataTypes : [MBCoreServiceType] = []
        
        allCoreServices?.forEach({ (aCoreService) in
            dataTypes.append(.coreService)
        })
        
        //  Add internet offer
        dataTypes.append(.internet)
        
        //  Add block number
        /* Suspend number will not be shown to user of Corporate and those hows grouptype is 'PAYBYSUBS'*/
        if MBUserSession.shared.userCustomerType() != MBCustomerType.Corporate /*||
            (MBUserSession.shared.userInfo?.groupType.isGroupPayBySUBs() ?? false) == true*/ {
            dataTypes.append(.suspend)
        }
        
        return dataTypes
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let allTypeOfData = self.typeOfDataInCoreService(allCoreServices: coreServicesData)
        
        // tableView.allowsSelection = false
        let cell  = tableView.dequeueReusableCell(withIdentifier: "cellID", for: indexPath) as! CoreServicesCell
        cell.selectionStyle = UITableViewCell.SelectionStyle.none
        cell.switchControl.sectionTag = indexPath.section
        cell.switchControl.rowTag = indexPath.row
        cell.setting_btn.removeTarget(nil, action: nil, for: .allEvents)
        
        switch allTypeOfData[indexPath.section] {
        case .internet:
            
            cell.setLayoutForInternetSettings(title: Localized("Info_InternetSettingTitle"), detail: Localized("Info_InternetSettingDetail"))
            cell.setting_btn.addTarget(self, action: #selector(sendPressed(_:)), for: UIControl.Event.touchUpInside)
            break
        case .suspend:
            cell.setLayoutForSuspend(title: Localized("Info_AccountSuspendTitle"), detail: Localized("Info_AccountSuspendDetail"))
            cell.setting_btn.addTarget(self, action: #selector(suspendBtn(_:)), for: UIControl.Event.touchUpInside)
            break
        case .coreService:
            
            if let aCoreService = coreServicesData?[indexPath.section].coreServicesList?[indexPath.row] {
                
                if aCoreService.offeringId.isEqualToStringIgnoreCase(otherString: Constants.K_CallDivert_Forward) {
                    
                    let aCoreService = coreServicesData?[indexPath.section].coreServicesList?[indexPath.row]
                    cell.setLayoutForForwordSettings(title: aCoreService?.forwardNumber)
                    
                    cell.switchControl.setOn((aCoreService?.status ?? "").isTextActive() , animated: true)
                    
                    cell.switchControl.addTarget(self, action: #selector(switchChanged), for: UIControl.Event.valueChanged)
                    
                    if (aCoreService?.status ?? "").isTextActive() == false {
                        cell.selectionStyle = UITableViewCell.SelectionStyle.default
                    }
                    
                } else if aCoreService.offeringId.isEqualToStringIgnoreCase(otherString: Constants.K_IAmBack){
                    
                    if self.isICalledYou_TurnedOff_Suspended() {
                        cell.setLayoutForSwitchSettings(aCoreService, isEnabled: false)
                    } else {
                        cell.setLayoutForSwitchSettings(aCoreService, isEnabled: self.isICalledYou_TurnedOff_Active())
                    }
                    
                    cell.switchControl.setOn(aCoreService.status.isTextActive(), animated: true)
                    
                    cell.switchControl.addTarget(self, action: #selector(switchChanged), for: UIControl.Event.valueChanged)
                    
                } else if aCoreService.offeringId.isEqualToStringIgnoreCase(otherString: Constants.K_ICalledYou_Busy) {
                    
                    cell.setLayoutForSwitchSettings(aCoreService,
                                                    isEnabled: self.isICalledYou_TurnedOff_Suspended() ? false : true)
                    
                    cell.switchControl.setOn(aCoreService.status.isTextActive(), animated: true)
                    
                    cell.switchControl.addTarget(self, action: #selector(switchChanged), for: UIControl.Event.valueChanged)
                    
                } else {
                    var isOfferActive : Bool = false
                    
                    if aCoreService.offeringId.isEqualToStringIgnoreCase(otherString: Constants.K_ICalledYou_TurnedOff) &&
                    aCoreService.status.isSuspended(){
                        
                        isOfferActive = true
                    } else {
                        isOfferActive = aCoreService.status.isTextActive()
                    }
                    
                    cell.setLayoutForSwitchSettings(aCoreService)
                    cell.switchControl.setOn(isOfferActive, animated: true)
                    cell.switchControl.addTarget(self, action: #selector(switchChanged), for: UIControl.Event.valueChanged)
                }
            } else {
                cell.setLayoutForSwitchSettings(nil, isEnabled: false)
                cell.switchControl.setOn(false, animated: true)
            }
            break
            
        default:
            cell.setLayoutForSwitchSettings(nil, isEnabled: false)
            cell.switchControl.setOn(false, animated: true)
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true)
        
        if (coreServicesData?.indices.contains(indexPath.section) ?? false) == true {
            
            if coreServicesData?[indexPath.section].coreServicesList?.indices.contains(indexPath.row) ?? false {
                
                if let aCoreService = coreServicesData?[indexPath.section].coreServicesList?[indexPath.row] {
                    
                    if aCoreService.offeringId.isEqualToStringIgnoreCase(otherString: Constants.K_CallDivert_Forward) &&
                        aCoreService.status.isTextActive() == false {
                        
                        settingsBtn(self)
                    }
                }
            }
        }
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        
        // Internet section setting
        if section == coreServicesData?.count  {
            title = " " + Localized("Info_LineManagement")
        } else if section == ((coreServicesData?.count ?? 0) + 1) {
            title = " " + Localized("Info_Account")
        } else {
            title = " " + (coreServicesData?[section].coreServiceCategory ?? "")
        }
        
        return title
    }
    
    func isICalledYou_TurnedOff_Active() -> Bool {
        
        var isActive = false
        
        coreServicesData?.forEach({ (aCoreServiceCatagory) in
            
            if aCoreServiceCatagory.coreServiceCategory.isEqualToStringIgnoreCase(otherString: Localized("Info_ICalledYou").trimmWhiteSpace) {
                
                aCoreServiceCatagory.coreServicesList?.forEach({ (aCoreService) in
                    
                    if aCoreService.offeringId.isEqualToStringIgnoreCase(otherString: Constants.K_ICalledYou_TurnedOff) {
                        
                        isActive = aCoreService.status.isTextActive()
                    }
                })
            }
        })
        
        return isActive
    }
    
    func isICalledYou_TurnedOff_Suspended() -> Bool {
        
        var isActive = false
        
        coreServicesData?.forEach({ (aCoreServiceCatagory) in
            
            if aCoreServiceCatagory.coreServiceCategory.isEqualToStringIgnoreCase(otherString: Localized("Info_ICalledYou").trimmWhiteSpace) {
                
                aCoreServiceCatagory.coreServicesList?.forEach({ (aCoreService) in
                    
                    if aCoreService.offeringId.isEqualToStringIgnoreCase(otherString: Constants.K_ICalledYou_TurnedOff) {
                        
                        isActive = aCoreService.status.isSuspended()
                    }
                })
            }
        })
        
        return isActive
    }
    
    //    is offering id of  "I Am Back" -> title : "I Am Back"
    //    and "I Called You" -> title : "Unavailable"
    //    are always same
    //    "I Am Back" and  "I Called You" both are objects and have array of “coreServicesList” in it
    //    do we have to look into for a specific offeringID
    //    like now below offeringId is of  "I Called You" section
    //    offeringId : "984855324"
    //    and this offeringId : "102341307" is in : "I Am Back" section
    //    so I will check if “984855324” is Active then this “102341307” will be clickable else not clickable
    //    and same will be done when user enable or disable offeringId : "984855324" of "I Called You" section
}
