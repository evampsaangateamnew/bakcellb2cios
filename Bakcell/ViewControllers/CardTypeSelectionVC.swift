//
//  CardTypeSelectionVC.swift
//  Bakcell
//
//  Created by Muhammad Irfan Awan on 04/03/2021.
//  Copyright © 2021 evampsaanga. All rights reserved.
//

import UIKit
import ObjectMapper

class CardTypeSelectionVC: BaseVC {
    
    static let identifier = "CardTypeSelectionVC"
    
    @IBOutlet var topUpTitle_lbl: UILabel!
    
    @IBOutlet var message_lbl: UILabel!
    
    @IBOutlet weak var cardType_lbl: UILabel!
    @IBOutlet weak var cardType_Img: UIImageView!
    @IBOutlet weak var carTypeDropDown: UIDropDown!
    
    
    @IBOutlet weak var saveCardForPayment_btn : UIButton!
    @IBOutlet weak var saveCardForPaymentDescription_lbl : UILabel!
    
    @IBOutlet var btnContinue: UIButton!
    
    
    
    //MARK: - Properties..
    
    var cardTypes : [String] = ["Visa/Visa Electron","Master/Master  Electron"]
    var selectedCardType : String = ""
    var selectedAmount : String?
    var selectedMsisdn : String?
    var isSaveCard : Bool = true
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if MBUserSession.shared.predefineData?.plasticCards?.count ?? 0 > 0 {
            cardTypes.removeAll()
            for cardObject in MBUserSession.shared.predefineData?.plasticCards ?? [PlasticCard]() {
                cardTypes.append(cardObject.cardVale ?? "")
            }
        }
        
        layoutViews()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.interactivePop(true)
    }
    
    func layoutViews(){
        self.topUpTitle_lbl.text = Localized("Lbl_Cardtype")
        self.btnContinue.setTitle(Localized("BtnTitle_Continue"), for: .normal)
        self.message_lbl.text = Localized("TopUp_Description")
        saveCardForPayment_btn.setImage(UIImage(named: "checkedbox"), for: .normal)
        saveCardForPaymentDescription_lbl.text = Localized("Lbl_SavePaymentForFuture")
        //setting up dropdown.
        setUpDropdown()
    }
    
    
    @IBAction func topUpPressed(_ sender: UIButton) {
        if selectedCardType.isEmpty || selectedCardType == "" {
            self.showErrorAlertWithMessage(message: Localized("Lbl_SelectCard"))
        } else {
//            let alert = self.myStoryBoard.instantiateViewController(withIdentifier: "AlertVC") as! AlertVC
//
//            self.presentPOPUP(alert, animated: true, completion: nil)
//
//            alert.setConfirmationAlertForTopUp(title: Localized("Title_Confirmation"), reciver: Localized("Message_ConfirmationSaveCard"), showBalnce: false)
//
//            alert.setYesButtonCompletionHandler { (aString) in
//                self.isSaveCard = true
//                self.initiatePayment()
//            }
//            alert.setNoButtonCompletionHandler { (aString) in
//                self.initiatePayment()
//            }
            showPaymentPrompt()
            
        }
    }
    
    
    
    
    func showPaymentPrompt() {
        let alert = self.myStoryBoard.instantiateViewController(withIdentifier: "AlertVC") as! AlertVC
        
        self.presentPOPUP(alert, animated: true, completion: nil)
        let messageString = String(format: Localized("Message_LoadConfirmation"), selectedAmount ?? "", selectedMsisdn ?? "")
        
        alert.showtopUpConfirmationAlert(title: Localized("Title_Confirmation"), message: messageString)
        
        alert.setYesButtonCompletionHandler { (aString) in
            self.initiatePayment()
        }
    }
    
    
    
    //Checkbox tapped...
    @IBAction  func checkBoxTapped(_ sender: UIButton){
        if sender.currentImage == UIImage(named: "checkedbox") {
            sender.setImage(UIImage(named: "Checkbox-state-2"), for: .normal)
            self.isSaveCard = false
        }else{
            sender.setImage(UIImage(named: "checkedbox"), for: .normal)
            self.isSaveCard = true
        }
    }
    
    
    
}

//MARK: - Dropdown Functionality..
extension CardTypeSelectionVC{
    
    func setUpDropdown(){
        
        carTypeDropDown.optionsTextAlignment = NSTextAlignment.center
        carTypeDropDown.textAlignment = NSTextAlignment .center
        carTypeDropDown.placeholder = Localized("DropDown_SelectCard")
        self.carTypeDropDown.rowBackgroundColor = UIColor.MBDimLightGrayColor
        carTypeDropDown.setFont = UIFont.MBArial(fontSize: 12)
        carTypeDropDown.tableHeight = 70
        
        carTypeDropDown.options = cardTypes
        self.carTypeDropDown.tableWillAppear {
            self.cardType_Img.image = UIImage (named: "Drop-down-arrow-state2")
        }
        
        self.carTypeDropDown.tableWillDisappear {
            self.cardType_Img.image = UIImage (named: "Drop-down-arrow-state1")
        }
        
        carTypeDropDown.didSelect { (option, index) in
            print("\(option) : ===> \(index)")
            self.selectedCardType = String(MBUserSession.shared.predefineData?.plasticCards?[index].cardKey ?? 0)
            
        }
    }
    
}

//MARK: _ API calls


extension CardTypeSelectionVC {
    
    func  initiatePayment(){
        activityIndicator.showActivityIndicator()
        _ = MBAPIClient.sharedClient.initiatePayment(CardType: self.selectedCardType, Amount: selectedAmount ?? "", IsSaved: isSaveCard, TopUpNumber: selectedMsisdn ?? ""){ ( response, resultData, error, isCancelled, status, resultCode, resultDesc) in
            
            self.activityIndicator.hideActivityIndicator()
            if error != nil {
                //EventLog
//                MBAnalyticsManager.logEvent(screenName: "Top up Screen", contentType:"Top up Scratch Card Failure" , status:"Failure" )
                error?.showServerErrorInViewController(self)
                
            } else {
                // handling data from API response.
                if resultCode == Constants.MBAPIStatusCode.succes.rawValue {
                    
//                    //EventLog
//                    MBAnalyticsManager.logEvent(screenName: "Top up Screen", contentType:"Top up Scratch Card Success" , status:"Success" )
//
//                    // Set true to reload Home page data
//                    MBUserSession.shared.shouldReloadDashboardData = true
//
                    //parsing data...
                    
                    if let resp =  Mapper<SavedCards>().map(JSONObject: resultData){
                        if resp.paymentKey.isEmpty || resp.paymentKey == "" {
                            //do nothing
                        } else {
                            if let plasticCardVC = self.myStoryBoard.instantiateViewController(withIdentifier: "PlasticCardViewController") as? PlasticCardViewController {
                                plasticCardVC.topUpToMSISDN = self.selectedMsisdn ?? ""
                                plasticCardVC.redirectionURL = resp.paymentKey
                                self.navigationController?.pushViewController(plasticCardVC, animated: true)
                            }
                        }
                    }
                    
                    
                } else {
                    //EventLog
//                    MBAnalyticsManager.logEvent(screenName: "Top up Screen", contentType:"Top up Scratch Card Failure" , status:"Failure" )
                    
                    // Show error alert to user
                    self.showErrorAlertWithMessage(message: resultDesc)
                }
            }
        }
    }
}
