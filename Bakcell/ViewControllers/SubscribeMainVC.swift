//
//  SubscribeMainVC.swift
//  Bakcell
//
//  Created by Saad Riaz on 9/8/17.
//  Copyright © 2017 evampsaanga. All rights reserved.
//

import UIKit

import ObjectMapper
import UPCarouselFlowLayout

class SubscribeMainVC: BaseVC  {
    
    //MARK:- Properties
    var offers : [SubscriptionOffers] = []
    var selectedTabType : MBOfferTabType = MBOfferTabType.Internet
    var selectedUsageType : MBOfferTabType?
    
    
    //MARK:- IBOutlet
    @IBOutlet var collectionView: UICollectionView!
    @IBOutlet var offerNumber_lbl: UILabel!
    
    @IBOutlet var noOfferFoundView: UIView!
    @IBOutlet var offerTitleView: UIView!
    @IBOutlet var offerTitle_lbl: UILabel!
    @IBOutlet var noOfferDiscription: UITextView!
    @IBOutlet var offers_btn: UIButton!
    
    //MARK: - ViewController Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if offers.count == 0 {
            collectionView.isHidden = true
            noOfferFoundView.isHidden = false
            
            offerNumber_lbl.text = "0/0"
            offerTitleView.roundTopCorners(radius: 8)
            
        } else {
            collectionView.isHidden = false
            noOfferFoundView.isHidden = true
            
            offerNumber_lbl.text = "1/\(offers.count)"
        }
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        
        LoadViewLayout()
        /* Collection view layout */
        let layout = UPCarouselFlowLayout()
        layout.itemSize = CGSize(width: (UIScreen.main.bounds.width - 40) , height: (UIScreen.main.bounds.height * 0.60))
        layout.sideItemScale = 0.9
        layout.scrollDirection = .vertical
        collectionView.setCollectionViewLayout(layout, animated: true)
    }
    
    
    //MARK: - FUNCTIONS
    func LoadViewLayout(){
        offerTitle_lbl.text = Localized("NoSubscriptions_text")
        noOfferDiscription.text = Localized("Description_NoSubscriptions")
        offers_btn .setTitle(Localized("OFFERS_BTNTITLE"), for: UIControl.State.normal)
    }
    
    //MARK: - IBACTIONS
    @IBAction func reNewButtonPressed(_ sender: UIButton) {
        let selectedOffer = offers[sender.tag]
        
        let confirmationAlert = self.myStoryBoard.instantiateViewController(withIdentifier: "ActivationConfirmationVC") as! ActivationConfirmationVC
        
        confirmationAlert.setConfirmationAlertLayout(message: "\(Localized("Message_RenewOffer"))\(selectedOffer.header?.offerName ?? "")")
        
        
        confirmationAlert.setYesButtonCompletionHandler { (aString) in
            self.changeSupplementaryOffering(selectedOffer: selectedOffer, actionType: true)
        }
        
        self.presentPOPUP(confirmationAlert, animated: true, completion: nil)
        
    }
    
    @IBAction func deActivateButtonPressed(_ sender: UIButton) {
        let selectedOffer = offers[sender.tag]
        
        let confirmationAlert = self.myStoryBoard.instantiateViewController(withIdentifier: "ActivationConfirmationVC") as! ActivationConfirmationVC
        
        self.presentPOPUP(confirmationAlert, animated: true, completion: nil)
        
        confirmationAlert.setConfirmationAlertLayout( message: "\(Localized("Message_DeactivateOffer"))\(selectedOffer.header?.offerName ?? "")" )
        
        confirmationAlert.setYesButtonCompletionHandler { (aString) in
            self.changeSupplementaryOffering(selectedOffer: selectedOffer, actionType: false)
        }
    }
    
    @IBAction func offersButtonPressed(_ sender: UIButton) {
        
        let supplementoryOfferVC = self.myStoryBoard.instantiateViewController(withIdentifier: "SupplementaryVC") as! SupplementaryVC
        
        // Always one call session 
        // supplementoryOfferVC.selectedTabType = .Call
        supplementoryOfferVC.selectedTabType = selectedTabType
        self.navigationController?.pushViewController(supplementoryOfferVC, animated: true)
        
    }
    
    // MARK: - API Calls
    
    /// Call 'changeSupplementaryOffering' API .
    ///
    /// - returns: Void
    func changeSupplementaryOffering(selectedOffer: SubscriptionOffers, actionType:Bool) {
        
        activityIndicator.showActivityIndicator()
        
        _ = MBAPIClient.sharedClient.changeSupplementaryOffering(actionType: actionType, offeringId: selectedOffer.header?.offeringId ?? "", offerName: selectedOffer.header?.offerName ?? "", { (response, resultData, error, isCancelled, status, resultCode, resultDesc)  in
            
            self.activityIndicator.hideActivityIndicator()
            if error != nil {
                error?.showServerErrorInViewController(self)
            } else {
                // handling data from API response.
                if resultCode == Constants.MBAPIStatusCode.succes.rawValue {
                    
                    // Parssing response data
                    if let mySubscriptionOffersHandler = Mapper<MySubscription>().map(JSONObject: resultData){
                        
                        // set data in user session
                        MBUserSession.shared.mySubscriptionOffers = mySubscriptionOffersHandler
                        
                        // Show succes alert
                        self.showSuccessAlertWithMessage(message: mySubscriptionOffersHandler.message)
                        
                    } else {
                        // Show Error alert
                        self.showErrorAlertWithMessage(message: resultDesc )
                        
                    }
                } else {
                    // Show Error alert
                    self.showErrorAlertWithMessage(message: resultDesc)
                }
            }
        })
    }
}


//MARK: SupplementaryOffersCellDelegate
extension SubscribeMainVC: SupplementaryOffersCellDelegate {
    func starTapped(offerId: String) {
        
        if let inAppSurveyVC : InAppSurveyVC = InAppSurveyVC.fromNib() {
            if  let  userSurvey = MBUserSession.shared.appSurvays?.userSurveys?.first(where: { $0.offeringId == offerId}){
                if let survey  = MBUserSession.shared.appSurvays?.surveys?.first(where: { $0.survayId  == userSurvey.surveryId }){
                    inAppSurveyVC.currentSurvay = survey
                    if let  question =  survey.questions?.first(where: { $0.questionId == userSurvey.questionId }){
                        inAppSurveyVC.survayQuestion = question
                        if let answer = question.answers?.firstIndex(where: { $0.answerId ==  userSurvey.answerId }) {
                            inAppSurveyVC.selectedAnswer = question.answers?[answer]
                        }
                    }
                }
                inAppSurveyVC.survayComment = userSurvey.comments
                inAppSurveyVC.offeringId = offerId
                inAppSurveyVC.offeringType = "2"
            } else {
                if let survey = MBUserSession.shared.appSurvays?.surveys?.first(where: { $0.screenName ?? "" == SurveyScreenName.bundle.rawValue}) {
                    inAppSurveyVC.currentSurvay = survey
                    inAppSurveyVC.offeringId = offerId
                    inAppSurveyVC.offeringType = "2"
                }
            }
            inAppSurveyVC.callback = {
                self.collectionView.reloadData()
            }
            self.presentPOPUP(inAppSurveyVC, animated: true, completion: nil)
        }
        
    }
    
    
}

//MARK: - Collection View Delegate
extension SubscribeMainVC : UICollectionViewDelegate,UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return offers.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SubscriptionCollectionCell", for: indexPath) as! SubscriptionCollectionCell
        
        //        cell.layer.borderColor = UIColor.MBBorderGrayColor.cgColor
        //        cell.layer.borderWidth = 1
        //        cell.layer.cornerRadius = 8
        
        //inAppSurvey
        cell.btnsStackView.isHidden = false
        cell.btnsStackView.snp.updateConstraints { const in
            const.height.equalTo(20)
        }
        cell.setRating(with: offers[indexPath.item])
        cell.delegate =  self
        
        
        cell.tableView.tag = indexPath.item
        
        cell.tableView.delegate = self
        cell.tableView.dataSource = self
        cell.tableView.allowMultipleSectionsOpen = false
        cell.tableView.keepOneSectionOpen = true;
        
        cell.tableView.reloadData()
        
        return cell
    }
    
    // USED to toggle
    func openSelectedSection() {
        
        //        SubscriptionCollectionCell.initialSection = [5 as NSNumber]
        
        let visbleIndexPath = collectionView.indexPathsForVisibleItems
        
        visbleIndexPath.forEach { (aIndexPath) in
            
            if let cell = collectionView.cellForItem(at: aIndexPath) as? SubscriptionCollectionCell {
                
                let type = typeOfDataInAOffer(aOffer: offers[cell.tableView.tag])
                
                if offers[cell.tableView.tag].openedViewSection == MBSectionType.Header && type.contains(MBSectionType.Header) {
                    
                    openSectionAt(index: 0, tableView: cell.tableView)
                    
                } else if offers[cell.tableView.tag].openedViewSection == MBSectionType.Detail && type.contains(MBSectionType.Detail) {
                    
                    let index = type.firstIndex(of:MBSectionType.Detail)
                    
                    openSectionAt(index: index ?? 0, tableView: cell.tableView)
                    
                } else {
                    openSectionAt(index: 0, tableView: cell.tableView)
                }
            }
        }
    }
    
    func openSectionAt(index : Int , tableView : MBAccordionTableView) {
        
        if tableView.isSectionOpen(index) == false {
            
            // tableView.scrollToRow(at: IndexPath(row: 0, section: index), at: .top, animated: true)
            
            tableView.toggleSection(withOutCallingDelegates: index)
            
        }
    }
    
}

// MARK: - <UITableViewDataSource> / <UITableViewDelegate> -

extension SubscribeMainVC : UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return numberOfSectionAtOfferIndex(index: tableView.tag);
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        let types = typeOfDataInAOffer(aOffer: offers[tableView.tag])
        
        switch types[section] {
            
        case MBSectionType.Header:
            return numberOfRowsInHeaderSection(aHeader: offers[tableView.tag].header)
            
        case MBSectionType.Detail:
            return numberOfRowsInDetailSection(aDetails: offers[tableView.tag].details)
            
        case MBSectionType.Subscription:
            return 0
            
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        return 45
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        let types = typeOfDataInAOffer(aOffer: offers[tableView.tag])
        
        switch types[indexPath.section] {
            
        case MBSectionType.Header:
            
            let headerDataType = typeOfDataInHeaderSection(aHeader: offers[tableView.tag].header)
            
            if let aHeader = offers[tableView.tag].header {
                
                switch headerDataType[indexPath.row] {
                    
                case MBOfferType.TypeTitle:
                    
                    let cell : SupplementaryExpandCell = tableView.dequeueReusableCell(withIdentifier: SupplementaryExpandCell.identifier) as! SupplementaryExpandCell
                    
                    cell.setOfferType(header: aHeader)
                    
                    return cell
                    
                case MBOfferType.OfferGroup:
                    
                    let cell : SupplementaryExpandCell = tableView.dequeueReusableCell(withIdentifier: SupplementaryExpandCell.identifier) as! SupplementaryExpandCell
                    cell.setOfferGroup(offerGroup: aHeader.offerGroup)
                    
                    return cell
                    
                case MBOfferType.OfferActive:
                    let cell : SupplementaryExpandCell = tableView.dequeueReusableCell(withIdentifier: SupplementaryExpandCell.identifier) as! SupplementaryExpandCell
                    cell.setOfferStatus(header: aHeader)
                    return cell
                    
                case MBOfferType.AttributeList:
                    let cell = tableView.dequeueReusableCell(withIdentifier: AttributeListCell.identifier) as! AttributeListCell
                    cell.setAttributeListOfMySubscription(attributeList: aHeader.attributeList?[0])
                    return cell
                    
                case MBOfferType.Usage:
                    
                    let remaingData = headerDataType.filter {$0 != MBOfferType.Usage}
                    let index = indexPath.row - remaingData.count
                    
                    let cell = tableView.dequeueReusableCell(withIdentifier: SubscriptionUsageCell.identifier) as! SubscriptionUsageCell
                    cell.setProgressLayout(aUsage: aHeader.usage?[index], isRenewButtonEnabled: aHeader.btnRenew?.isButtonEnabled() ?? false, selectedUsageType: self.selectedUsageType)
                    return cell
                    
                default:
                    break
                }
            }
            
        case MBSectionType.Detail :
            
            let aOfferDetails = offers[tableView.tag].details
            
            let containingDataTypes = typeOfDataInDetailSection(aDetails: aOfferDetails)
            
            switch containingDataTypes[indexPath.row] {
                
            case MBOfferType.Price:
                
                let cell = tableView.dequeueReusableCell(withIdentifier: PriceAndRoundingCell.identifier) as! PriceAndRoundingCell
                cell.setPriceLayoutValues(price: aOfferDetails?.price, showIcon: false)
                return cell
                
            case MBOfferType.Rounding:
                
                let cell = tableView.dequeueReusableCell(withIdentifier: PriceAndRoundingCell.identifier) as! PriceAndRoundingCell
                cell.setRoundingLayoutValues(rounding: aOfferDetails?.rounding, showIcon: false)
                return cell
                
            case MBOfferType.TextWithTitle:
                
                let cell = tableView.dequeueReusableCell(withIdentifier: TextDateTimeCell.identifier) as! TextDateTimeCell
                cell.setTextWithTitleLayoutValues(textWithTitle: aOfferDetails?.textWithTitle)
                return cell
                
            case MBOfferType.TextWithOutTitle:
                
                let cell = tableView.dequeueReusableCell(withIdentifier: TextDateTimeCell.identifier) as! TextDateTimeCell
                cell.setTextWithOutTitleLayoutValues(textWithOutTitle: aOfferDetails?.textWithOutTitle)
                return cell
                
            case MBOfferType.TextWithPoints:
                
                let cell = tableView.dequeueReusableCell(withIdentifier: TextDateTimeCell.identifier) as! TextDateTimeCell
                cell.setTextWithPointsLayoutValues(textWithPoint: aOfferDetails?.textWithPoints)
                return cell
                
            case MBOfferType.Date:
                
                let cell = tableView.dequeueReusableCell(withIdentifier: TextDateTimeCell.identifier) as! TextDateTimeCell
                cell.setDateAndTimeLayoutValues(dateAndTime: aOfferDetails?.date)
                return cell
                
            case MBOfferType.Time:
                
                let cell = tableView.dequeueReusableCell(withIdentifier: TextDateTimeCell.identifier) as! TextDateTimeCell
                cell.setDateAndTimeLayoutValues(dateAndTime: aOfferDetails?.time)
                return cell
                
            case MBOfferType.RoamingDetails:
                
                let cell = tableView.dequeueReusableCell(withIdentifier: RoamingDetailsCell.identifier) as! RoamingDetailsCell
                cell.setRoamingDetailsLayout(roamingDetails: aOfferDetails?.roamingDetails)
                return cell
                
            case MBOfferType.FreeResourceValidity:
                
                let cell = tableView.dequeueReusableCell(withIdentifier: FreeResourceValidityCell.identifier) as! FreeResourceValidityCell
                cell.setFreeResourceValidityValues(freeResourceValidity: aOfferDetails?.freeResourceValidity)
                return cell
                
            case MBOfferType.TitleSubTitleValueAndDesc:
                
                let cell = tableView.dequeueReusableCell(withIdentifier: TitleSubTitleValueAndDescCell.identifier) as! TitleSubTitleValueAndDescCell
                cell.setTitleSubTitleValueAndDescLayoutValues(titleSubTitleValueAndDesc: aOfferDetails?.titleSubTitleValueAndDesc)
                return cell
                
            default:
                break
            }
        default:
            return UITableViewCell()
        }
        
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let type = typeOfDataInAOffer(aOffer: offers[tableView.tag])
        
        switch type[section] {
            
        case MBSectionType.Header:
            
            let offerName :String = offers[tableView.tag].header?.offerName ?? ""
            let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: SupplementaryOffersHeaderView.identifier) as! SupplementaryOffersHeaderView
            headerView.setViewForOfferNameWithSickerValues(offerName: offerName, stickerTitle: nil, stickerColorCode: nil)
            return headerView
            
        case MBSectionType.Detail:
            let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: SupplementaryOffersHeaderView.identifier) as! SupplementaryOffersHeaderView
            let title :String = Localized("Title_Details")
            if offers[tableView.tag].openedViewSection == MBSectionType.Detail {
                headerView.setViewWithTitle(title: title, isSectionSelected: true, headerType: MBSectionType.Detail)
            } else {
                headerView.setViewWithTitle(title: title, isSectionSelected: false, headerType: MBSectionType.Detail)
            }
            return headerView
            
        case MBSectionType.Subscription:
            
            let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: SubscribeButtonView.identifier) as! SubscribeButtonView
            
            let aOffer = offers[tableView.tag]
            headerView.setRenewDeActivateButton(tag: tableView.tag, renewEnable: aOffer.header?.btnRenew?.isButtonEnabled() ?? false, deactivateEnable: aOffer.header?.btnDeactivate?.isButtonEnabled() ?? false)
            
            headerView.activateRenew_btn.addTarget(self, action: #selector(reNewButtonPressed(_:)), for: UIControl.Event.touchUpInside)
            headerView.subscribed_btn.addTarget(self, action: #selector(deActivateButtonPressed(_:)), for: UIControl.Event.touchUpInside)
            return headerView
            
        default:
            return UIView()
        }
    }
    
    //MARK: - Helper fuction for data loading
    func numberOfSectionAtOfferIndex(index: Int) -> Int {
        
        var numberOfSections : Int = 2
        
        
        if offers[index].details != nil {
            numberOfSections += 1
        }
        
        return numberOfSections
    }
    
    func numberOfRowsInHeaderSection(aHeader : SubscriptionHeader?) -> Int {
        
        // Atleast one because there is one with offer name
        var numberOfRows : Int = 1
        
        if let aHeader =  aHeader {
            
            if checkOfferContainsOfferGroup(aHeader: aHeader) {
                numberOfRows = numberOfRows + 1
            }
            
            if checkOfferContainsHeaderType(aHeader: aHeader) {
                numberOfRows = numberOfRows + 1
            }
            
            if aHeader.attributeList?.indices.contains(0) ?? false {
                numberOfRows = numberOfRows + 1
            }
            
            if let usage = aHeader.usage {
                numberOfRows = numberOfRows + usage.count
            }
        }
        return numberOfRows
    }
    
    func checkOfferContainsOfferGroup(aHeader : SubscriptionHeader?) -> Bool {
        
        // Atleast one because there is one with offer name
        var isContainsOfferGroup : Bool = false
        
        if selectedTabType == MBOfferTabType.Internet ||
            selectedTabType == MBOfferTabType.Roaming ||
            selectedUsageType == MBOfferTabType.Internet {
            
            if let aHeader =  aHeader {
                
                if aHeader.offerGroup != nil {
                    isContainsOfferGroup = true
                }
            }
        }
        
        return isContainsOfferGroup
    }
    
    func checkOfferContainsHeaderType(aHeader : SubscriptionHeader?) -> Bool {
        
        // Atleast one because there is one with offer name
        var isContainsOfferGroup : Bool = false
        
        if selectedTabType == MBOfferTabType.Roaming {
            
            if let aHeader =  aHeader {
                
                if aHeader.type != nil {
                    isContainsOfferGroup = true
                }
            }
        }
        
        return isContainsOfferGroup
    }
    
    func numberOfRowsInDetailSection(aDetails : DetailsAndDescription?) -> Int {
        
        var numberOfRows : Int = 0
        
        if let aOfferDetails =  aDetails {
            
            // 1
            if aOfferDetails.price != nil {
                numberOfRows = numberOfRows + 1
            }
            
            // 2
            if aOfferDetails.rounding != nil {
                numberOfRows = numberOfRows + 1
            }
            // 3
            if aOfferDetails.textWithTitle != nil {
                numberOfRows = numberOfRows + 1
            }
            // 4
            if aOfferDetails.textWithOutTitle != nil {
                numberOfRows = numberOfRows + 1
            }
            // 5
            if aOfferDetails.textWithPoints != nil {
                numberOfRows = numberOfRows + 1
            }
            // 6
            if aOfferDetails.date != nil {
                numberOfRows = numberOfRows + 1
            }
            // 7
            if aOfferDetails.time != nil {
                numberOfRows = numberOfRows + 1
            }
            // 8
            if aOfferDetails.roamingDetails != nil {
                numberOfRows = numberOfRows + 1
            }
            // 9
            if aOfferDetails.freeResourceValidity != nil {
                numberOfRows = numberOfRows + 1
            }
            // 10
            if aOfferDetails.titleSubTitleValueAndDesc != nil {
                numberOfRows = numberOfRows + 1
            }
            
        }
        return numberOfRows
    }
    
    func typeOfDataInAOffer(aOffer : SubscriptionOffers?) -> [MBSectionType] {
        
        // Atleast one because there is one with offer name
        
        var dataTypes : [MBSectionType] = []
        
        if let aOffer =  aOffer {
            // 1
            if aOffer.header != nil {
                
                dataTypes.append(MBSectionType.Header)
            }
            // 2
            if aOffer.details != nil  {
                
                dataTypes.append(MBSectionType.Detail)
            }
            
            dataTypes.append(MBSectionType.Subscription)
        }
        return dataTypes
    }
    
    func typeOfDataInHeaderSection(aHeader : SubscriptionHeader?) -> [MBOfferType] {
        
        var dataTypeArray : [MBOfferType] = []
        if let aHeader =  aHeader {
            
            if selectedTabType == MBOfferTabType.Internet ||
                selectedTabType == MBOfferTabType.Roaming ||
                selectedUsageType == MBOfferTabType.Internet {
                
                if checkOfferContainsHeaderType(aHeader: aHeader) {
                    dataTypeArray.append(MBOfferType.TypeTitle)
                }
                
                if checkOfferContainsOfferGroup(aHeader: aHeader) {
                    dataTypeArray.append(MBOfferType.OfferGroup)
                }
            }
            
            dataTypeArray.append(MBOfferType.OfferActive)
            
            if aHeader.attributeList?.indices.contains(0) ?? false {
                dataTypeArray.append(MBOfferType.AttributeList)
            }
            
            if aHeader.usage != nil {
                aHeader.usage?.forEach({ (aUsage) in
                    
                    dataTypeArray.append(MBOfferType.Usage)
                })
            }
            
        }
        return dataTypeArray
    }
    
    func typeOfDataInDetailSection(aDetails : DetailsAndDescription?) -> [MBOfferType] {
        
        // Atleast one because there is one with offer name
        
        var dataTypes : [MBOfferType] = []
        
        if let aDetailsData =  aDetails {
            // 1
            if aDetailsData.price != nil {
                
                dataTypes.append(MBOfferType.Price)
            }
            
            // 2
            if aDetailsData.rounding != nil  {
                
                dataTypes.append(MBOfferType.Rounding)
            }
            // 3
            if aDetailsData.textWithTitle != nil {
                
                dataTypes.append(MBOfferType.TextWithTitle)
            }
            // 4
            if aDetailsData.textWithOutTitle != nil {
                
                dataTypes.append(MBOfferType.TextWithOutTitle)
            }
            // 5
            if aDetailsData.textWithPoints != nil {
                
                dataTypes.append(MBOfferType.TextWithPoints)
            }
            // 6
            if aDetailsData.titleSubTitleValueAndDesc != nil {
                
                dataTypes.append(MBOfferType.TitleSubTitleValueAndDesc)
            }
            
            // 7
            if aDetailsData.date != nil {
                
                dataTypes.append(MBOfferType.Date)
            }
            // 8
            if aDetailsData.time != nil {
                
                dataTypes.append(MBOfferType.Time)
            }
            // 9
            if aDetailsData.roamingDetails != nil {
                
                dataTypes.append(MBOfferType.RoamingDetails)
            }
            // 10
            if aDetailsData.freeResourceValidity != nil {
                
                dataTypes.append(MBOfferType.FreeResourceValidity)
            }
        }
        return dataTypes
    }
}


// MARK: - <MBAccordionTableViewDelegate> -

extension SubscribeMainVC : MBAccordionTableViewDelegate {
    
    func tableView(_ tableView: MBAccordionTableView, willOpenSection section: Int, withHeader header: UITableViewHeaderFooterView?) {
        
        if let headerView = header as? SupplementaryOffersHeaderView {
            headerView.detailSign_btn .setImage(#imageLiteral(resourceName: "minu-sign"), for: UIControl.State.normal)
        }
    }
    
    func tableView(_ tableView: MBAccordionTableView, didOpenSection section: Int, withHeader header: UITableViewHeaderFooterView?) {
        
        if let headerView = header as? SupplementaryOffersHeaderView {
            
            if offers[tableView.tag].openedViewSection != headerView.viewType {
                
                offers[tableView.tag].openedViewSection = headerView.viewType;
            }
        }
        
    }
    
    func tableView(_ tableView: MBAccordionTableView, willCloseSection section: Int, withHeader header: UITableViewHeaderFooterView?) {
        
        if let headerView = header as? SupplementaryOffersHeaderView {
            headerView.detailSign_btn .setImage(#imageLiteral(resourceName: "Plus-Sign"), for: UIControl.State.normal)
        }
    }
    
    func tableView(_ tableView: MBAccordionTableView, didCloseSection section: Int, withHeader header: UITableViewHeaderFooterView?) {
        
        if let headerView = header as? SupplementaryOffersHeaderView {
            
            if offers[tableView.tag].openedViewSection == headerView.viewType {
                offers[tableView.tag].openedViewSection = MBSectionType.Header;
                tableView.toggleSection(0)
            }
        }
    }
    
    func tableView(_ tableView: MBAccordionTableView, canInteractWithHeaderAtSection section: Int) -> Bool {
        
        if let headerView = tableView.headerView(forSection: section) as? SubscribeButtonView {
            
            if headerView.viewType == MBSectionType.Subscription {
                
                return false
                
            } else {
                
                return true
            }
        } else {
            
            return true
        }
    }
}

// MARK: - ScrollView Delegate
extension SubscribeMainVC : UIScrollViewDelegate {
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        
        if scrollView == collectionView {
            openSelectedSection()
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        let visibleRect = CGRect(origin: collectionView.contentOffset, size: collectionView.bounds.size)
        let visiblePoint = CGPoint(x: visibleRect.midX, y: visibleRect.midY)
        let indexPath = collectionView.indexPathForItem(at: visiblePoint)
        
        if offers.count > 0 {
            
            let index : Int = indexPath?.item ?? 0
            offerNumber_lbl.text = "\(index + 1 )/\(offers.count)"
            
        } else {
            offerNumber_lbl.text = "0/\(offers.count)"
        }
    }
}
