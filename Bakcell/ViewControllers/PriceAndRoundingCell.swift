//
//  PriceAndRoundingCell.swift
//  TestForAddingViewRunTimeINTableViewCell
//
//  Created by AbdulRehman Warraich on 8/30/17.
//  Copyright © 2017 AbdulRehman Warraich. All rights reserved.
//

import UIKit
import MarqueeLabel

class PriceAndRoundingCell: UITableViewCell {

    static let identifier : String = "PriceAndRoundingCell"
    // Main Views
    @IBOutlet var TitleAndValueView: UIView!
    @IBOutlet var AttributeListView: UIView!
    @IBOutlet var DescriptionView: UIView!

    // TitleAndValueView iner outlets
    @IBOutlet var icon_img: UIImageView!
    @IBOutlet var mainTitle_lbl: MarqueeLabel!
    @IBOutlet var mainValue_lbl: MarqueeLabel!
    @IBOutlet var titleLeftValue_lbl: MarqueeLabel!

    @IBOutlet var centerSepratorView: UIView!

    @IBOutlet var iconImageViewWidthConstraint: NSLayoutConstraint!

    @IBOutlet var manatSign_lbl: UILabel!
    @IBOutlet var manatSignLabelWidthConstraint: NSLayoutConstraint!

    // AttributeListView iner outlets
    @IBOutlet var AttributeListViewHightConstraint: NSLayoutConstraint!
    
    // DescriptionView iner outlets
    @IBOutlet var mainDescription_lbl: UILabel!
    @IBOutlet var descriptionViewHightConstraint: NSLayoutConstraint!

    // seprator view
    @IBOutlet var sepratorView: UIView!
    @IBOutlet var sepratorViewLeading: NSLayoutConstraint!
    @IBOutlet var sepratorViewTrialing: NSLayoutConstraint!
    @IBOutlet var titleLeftValueWidthConstraint: NSLayoutConstraint!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        mainTitle_lbl.setupMarqueeAnimation()
        mainValue_lbl.setupMarqueeAnimation()
        titleLeftValue_lbl.setupMarqueeAnimation()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    // Using in supplementery offers
    func setPriceLayoutValues (price : Price?, showIcon: Bool = true) {

        sepratorView.isHidden = false
        sepratorView.backgroundColor = UIColor.MBDimLightGrayColor

        centerSepratorView.isHidden = true
        
        sepratorViewLeading.constant = 8
        sepratorViewTrialing.constant = 8

        titleLeftValue_lbl.text = ""
        titleLeftValueWidthConstraint.constant = 0

        self.contentView.backgroundColor = UIColor.white

        //        offersCurrency : ""
        //        offersCurrency : "AZN
        //        offersCurrency : "manat"

        if let price = price {

            if showIcon {
                icon_img.image = MBUtilities.iconImageFor(key: price.iconName)
                iconImageViewWidthConstraint.constant = 26
                icon_img.isHidden = false

            } else {
                iconImageViewWidthConstraint.constant = 0
                icon_img.isHidden = true
            }

            mainTitle_lbl.text = price.title ?? ""

            if price.value?.isBlank == false {
                if let offersCurrency = price.offersCurrency {

                    if offersCurrency.isEqualToStringIgnoreCase(otherString: "manat") {
                        manatSign_lbl.isHidden = false
                        manatSignLabelWidthConstraint.constant = 8
                        mainValue_lbl.text = "\(price.value ?? "")"

                    } else {
                        manatSign_lbl.isHidden = true
                        manatSignLabelWidthConstraint.constant = 0
                        mainValue_lbl.text = "\(price.value ?? "") \(offersCurrency)"
                    }

                } else {
                    manatSign_lbl.isHidden = true
                    manatSignLabelWidthConstraint.constant = 0
                    mainValue_lbl.text = price.value ?? ""
                }
            } else {
                manatSign_lbl.isHidden = true
                manatSignLabelWidthConstraint.constant = 0
                mainValue_lbl.text = price.value ?? ""
            }

            if let description = price.description {
//                mainDescription_lbl.text = description
                mainDescription_lbl.loadHTMLString(htmlString: description)
                descriptionViewHightConstraint.isActive = false
            } else {
                mainDescription_lbl.text = ""
                descriptionViewHightConstraint.isActive = true
            }


            AttributeListView.subviews.forEach({ (aView) in

                if aView is AttributeDetailView {
                    aView.removeFromSuperview()
                }
            })

            var lastView : UIView?
            price.attributeList?.forEach({ (aAttribute) in

                let attributesView : AttributeDetailView = AttributeDetailView.instanceFromNib() as! AttributeDetailView
                attributesView.translatesAutoresizingMaskIntoConstraints = false

                attributesView.setAttributeValues(showManatSign: true, aAttribute: aAttribute)

                AttributeListView.addSubview(attributesView)

                let leading = NSLayoutConstraint(item: attributesView, attribute: NSLayoutConstraint.Attribute.leading, relatedBy: NSLayoutConstraint.Relation.equal, toItem: AttributeListView, attribute: NSLayoutConstraint.Attribute.leading, multiplier: 1, constant: 0)
                let trailing = NSLayoutConstraint(item: attributesView, attribute: NSLayoutConstraint.Attribute.trailing, relatedBy: NSLayoutConstraint.Relation.equal, toItem: AttributeListView, attribute: NSLayoutConstraint.Attribute.trailing, multiplier: 1, constant: 0)

                var top : NSLayoutConstraint = NSLayoutConstraint()

                if lastView == nil{

                    top = NSLayoutConstraint(item: attributesView, attribute: NSLayoutConstraint.Attribute.top, relatedBy: NSLayoutConstraint.Relation.equal, toItem: AttributeListView, attribute: NSLayoutConstraint.Attribute.top, multiplier: 1, constant: 0)

                } else {
                    top = NSLayoutConstraint(item: attributesView, attribute: NSLayoutConstraint.Attribute.top, relatedBy: NSLayoutConstraint.Relation.equal, toItem: lastView, attribute: NSLayoutConstraint.Attribute.bottom, multiplier: 1, constant: 0)
                }



                let height = NSLayoutConstraint(item: attributesView, attribute: NSLayoutConstraint.Attribute.height, relatedBy: NSLayoutConstraint.Relation.equal, toItem: nil, attribute: NSLayoutConstraint.Attribute.notAnAttribute, multiplier: 1, constant: 22)

                AttributeListView.addConstraints([leading, trailing, top, height])
                
                lastView = attributesView

            })

            if let count = price.attributeList?.count{
                AttributeListViewHightConstraint.constant = CGFloat(count * 22)
            } else {
                AttributeListViewHightConstraint.constant = 0.0
            }
        }
    }

    func setRoundingLayoutValues (rounding : Rounding?, showIcon: Bool = true) {

        sepratorView.isHidden = false
        sepratorView.backgroundColor = UIColor.MBDimLightGrayColor

        centerSepratorView.isHidden = true

        sepratorViewLeading.constant = 8
        sepratorViewTrialing.constant = 8
        manatSignLabelWidthConstraint.constant = 0

        titleLeftValue_lbl.text = ""
        titleLeftValueWidthConstraint.constant = 0

        self.contentView.backgroundColor = UIColor.white

        if let rounding = rounding {

            if showIcon {
                icon_img.image = MBUtilities.iconImageFor(key: rounding.iconName)
                iconImageViewWidthConstraint.constant = 26
                icon_img.isHidden = false

            } else {
                iconImageViewWidthConstraint.constant = 0
                icon_img.isHidden = true
            }

            mainTitle_lbl.text = rounding.title ?? ""
            mainValue_lbl.text = rounding.value ?? ""

            if rounding.value?.isStringAnNumber() == true {
                manatSign_lbl.isHidden = false
                manatSignLabelWidthConstraint.constant = 8
            } else {
                manatSign_lbl.isHidden = true
                manatSignLabelWidthConstraint.constant = 0
            }

            if let description = rounding.description {
//                mainDescription_lbl.text = description
                mainDescription_lbl.loadHTMLString(htmlString: description)
                descriptionViewHightConstraint.isActive = false
            } else {
                mainDescription_lbl.text = ""
                descriptionViewHightConstraint.isActive = true
            }

            AttributeListView.subviews.forEach({ (aView) in

                if aView is AttributeDetailView {
                    aView.removeFromSuperview()
                }
            })
            var lastView : UIView?
            rounding.attributeList?.forEach({ (aAttribute) in

                let attributesView : AttributeDetailView = AttributeDetailView.instanceFromNib() as! AttributeDetailView
                attributesView.translatesAutoresizingMaskIntoConstraints = false

                attributesView.setAttributeValues(showManatSign: false, aAttribute: aAttribute)

                AttributeListView.addSubview(attributesView)

                let leading = NSLayoutConstraint(item: attributesView, attribute: NSLayoutConstraint.Attribute.leading, relatedBy: NSLayoutConstraint.Relation.equal, toItem: AttributeListView, attribute: NSLayoutConstraint.Attribute.leading, multiplier: 1, constant: 0)
                let trailing = NSLayoutConstraint(item: attributesView, attribute: NSLayoutConstraint.Attribute.trailing, relatedBy: NSLayoutConstraint.Relation.equal, toItem: AttributeListView, attribute: NSLayoutConstraint.Attribute.trailing, multiplier: 1, constant: 0)

                var top : NSLayoutConstraint = NSLayoutConstraint()

                if lastView == nil{

                    top = NSLayoutConstraint(item: attributesView, attribute: NSLayoutConstraint.Attribute.top, relatedBy: NSLayoutConstraint.Relation.equal, toItem: AttributeListView, attribute: NSLayoutConstraint.Attribute.top, multiplier: 1, constant: 0)

                } else {
                    top = NSLayoutConstraint(item: attributesView, attribute: NSLayoutConstraint.Attribute.top, relatedBy: NSLayoutConstraint.Relation.equal, toItem: lastView, attribute: NSLayoutConstraint.Attribute.bottom, multiplier: 1, constant: 0)
                }



                let height = NSLayoutConstraint(item: attributesView, attribute: NSLayoutConstraint.Attribute.height, relatedBy: NSLayoutConstraint.Relation.equal, toItem: nil, attribute: NSLayoutConstraint.Attribute.notAnAttribute, multiplier: 1, constant: 22)

                AttributeListView.addConstraints([leading, trailing, top, height])

                lastView = attributesView

            })
            
            if let count = rounding.attributeList?.count{
                AttributeListViewHightConstraint.constant = CGFloat(count * 22)
            } else {
                AttributeListViewHightConstraint.constant = 0.0
            }
        }
    }


    // Using in Tariff screen
    func setInternetLayoutValues (internetHeader : InternetPriceSection?, setBackgroundColorWhite: Bool = false) {
        sepratorView.isHidden = false

        centerSepratorView.isHidden = true

        mainDescription_lbl.text = ""
        descriptionViewHightConstraint.isActive = true

        sepratorViewLeading.constant = 8
        sepratorViewTrialing.constant = 8
        if setBackgroundColorWhite == true {
            self.contentView.backgroundColor = UIColor.white
            sepratorView.backgroundColor = UIColor.MBDimLightGrayColor
        } else {
            self.contentView.backgroundColor = UIColor.MBDimLightGrayColor
            sepratorView.backgroundColor = UIColor.white
        }

        titleLeftValue_lbl.text = ""
        titleLeftValueWidthConstraint.constant = 0

        //        iconName : ""
        //        title : "Internet"
        //        titleValue : "1 MB"
        //        subTitle : "Download & Upload"
        //        subTitleValue : "0.29"

        if let internetHeader = internetHeader {

            mainTitle_lbl.text = internetHeader.title ?? ""
            mainValue_lbl.text = internetHeader.titleValue ?? ""

            if mainValue_lbl.text?.isStringAnNumber() == true {
                manatSign_lbl.isHidden = false
                manatSignLabelWidthConstraint.constant = 8
            } else {
                manatSign_lbl.isHidden = true
                manatSignLabelWidthConstraint.constant = 0
            }

            icon_img.image = MBUtilities.iconImageFor(key: internetHeader.iconName)


            AttributeListView.subviews.forEach({ (aView) in

                if aView is AttributeDetailView {
                    aView.removeFromSuperview()
                }
            })


            let attributesView : AttributeDetailView = AttributeDetailView.instanceFromNib() as! AttributeDetailView
            attributesView.translatesAutoresizingMaskIntoConstraints = false

            attributesView.setAttributeValuesForInternet(subTitle: internetHeader.subTitle, subTitleValue: internetHeader.subTitleValue, isFromKlassPostpaid: setBackgroundColorWhite)

            AttributeListView.addSubview(attributesView)

            let leading = NSLayoutConstraint(item: attributesView, attribute: NSLayoutConstraint.Attribute.leading, relatedBy: NSLayoutConstraint.Relation.equal, toItem: AttributeListView, attribute: NSLayoutConstraint.Attribute.leading, multiplier: 1, constant: 0)
            let trailing = NSLayoutConstraint(item: attributesView, attribute: NSLayoutConstraint.Attribute.trailing, relatedBy: NSLayoutConstraint.Relation.equal, toItem: AttributeListView, attribute: NSLayoutConstraint.Attribute.trailing, multiplier: 1, constant: 0)

            let top : NSLayoutConstraint = NSLayoutConstraint(item: attributesView, attribute: NSLayoutConstraint.Attribute.top, relatedBy: NSLayoutConstraint.Relation.equal, toItem: AttributeListView, attribute: NSLayoutConstraint.Attribute.top, multiplier: 1, constant: 0)

            let height = NSLayoutConstraint(item: attributesView, attribute: NSLayoutConstraint.Attribute.height, relatedBy: NSLayoutConstraint.Relation.equal, toItem: nil, attribute: NSLayoutConstraint.Attribute.notAnAttribute, multiplier: 1, constant: 22)

            AttributeListView.addConstraints([leading, trailing, top, height])
            
            AttributeListViewHightConstraint.constant = 22
        }
    }

    func setCallAndSMSLayoutValues (callHeaderType : CallSMSPriceSection?, cellType : MBDataTypes, setBackgroundColorWhite: Bool = false) {

        sepratorView.isHidden = false
        sepratorView.backgroundColor = UIColor.MBDimLightGrayColor

        centerSepratorView.isHidden = true

        mainDescription_lbl.text = ""
        descriptionViewHightConstraint.isActive = true
        
        sepratorViewLeading.constant = 8
        sepratorViewTrialing.constant = 8

        if cellType == MBDataTypes.Call {
            self.contentView.backgroundColor = UIColor.white
            sepratorView.backgroundColor = UIColor.MBDimLightGrayColor

        } else if cellType == MBDataTypes.SMS {

            if setBackgroundColorWhite == true {
                self.contentView.backgroundColor = UIColor.white
                sepratorView.backgroundColor = UIColor.MBDimLightGrayColor
            } else {
                self.contentView.backgroundColor = UIColor.MBDimLightGrayColor
                sepratorView.backgroundColor = UIColor.white
            }

        }
        if let callHeaderType = callHeaderType {

            mainTitle_lbl.text = callHeaderType.title ?? ""

            if cellType == MBDataTypes.Call {
                mainValue_lbl.text = callHeaderType.titleValue ?? ""

                // Show double titles and values
                if callHeaderType.priceTemplate?.isEqualToStringIgnoreCase(otherString: Constants.kDoubleTitlesKey) ?? false{

                    titleLeftValueWidthConstraint.constant = 80
                    centerSepratorView.isHidden = false

                } else {
                    titleLeftValueWidthConstraint.constant = 0
                    centerSepratorView.isHidden = true
                }

                // id "Double Values" then left title will not be displed
                if callHeaderType.priceTemplate?.isEqualToStringIgnoreCase(otherString: Constants.kDoubleTitlesKey) ?? false {
                    titleLeftValue_lbl.text = callHeaderType.titleValueLeft ?? ""

                } else {

                    titleLeftValue_lbl.text = ""
                }

                mainValue_lbl.text = callHeaderType.titleValueRight ?? ""

            } else {
                mainValue_lbl.text = callHeaderType.titleValue ?? ""
                titleLeftValueWidthConstraint.constant = 0
            }

            if mainValue_lbl.text?.isStringAnNumber() == true {
                manatSign_lbl.isHidden = false
                manatSignLabelWidthConstraint.constant = 8
            } else {
                manatSign_lbl.isHidden = true
                manatSignLabelWidthConstraint.constant = 0
            }

            icon_img.image = MBUtilities.iconImageFor(key: callHeaderType.iconName)


            AttributeListView.subviews.forEach({ (aView) in

                if aView is AttributeDetailView {
                    aView.removeFromSuperview()
                }
            })

            var lastView : UIView?
            callHeaderType.attributes?.forEach({ (aAttribute) in

                let attributesView : AttributeDetailView = AttributeDetailView.instanceFromNib() as! AttributeDetailView
                attributesView.translatesAutoresizingMaskIntoConstraints = false

                attributesView.setAttributeValuesForCall(showManatSign: true, aAttribute: aAttribute, cellType: cellType, priceTemplate: callHeaderType.priceTemplate, isFromKlassPostpaid: setBackgroundColorWhite)

                AttributeListView.addSubview(attributesView)

                let leading = NSLayoutConstraint(item: attributesView, attribute: NSLayoutConstraint.Attribute.leading, relatedBy: NSLayoutConstraint.Relation.equal, toItem: AttributeListView, attribute: NSLayoutConstraint.Attribute.leading, multiplier: 1, constant: 0)
                let trailing = NSLayoutConstraint(item: attributesView, attribute: NSLayoutConstraint.Attribute.trailing, relatedBy: NSLayoutConstraint.Relation.equal, toItem: AttributeListView, attribute: NSLayoutConstraint.Attribute.trailing, multiplier: 1, constant: 0)

                var top : NSLayoutConstraint = NSLayoutConstraint()

                if lastView == nil{

                    top = NSLayoutConstraint(item: attributesView, attribute: NSLayoutConstraint.Attribute.top, relatedBy: NSLayoutConstraint.Relation.equal, toItem: AttributeListView, attribute: NSLayoutConstraint.Attribute.top, multiplier: 1, constant: 0)

                } else {
                    top = NSLayoutConstraint(item: attributesView, attribute: NSLayoutConstraint.Attribute.top, relatedBy: NSLayoutConstraint.Relation.equal, toItem: lastView, attribute: NSLayoutConstraint.Attribute.bottom, multiplier: 1, constant: 0)
                }



                let height = NSLayoutConstraint(item: attributesView, attribute: NSLayoutConstraint.Attribute.height, relatedBy: NSLayoutConstraint.Relation.equal, toItem: nil, attribute: NSLayoutConstraint.Attribute.notAnAttribute, multiplier: 1, constant: 22)

                AttributeListView.addConstraints([leading, trailing, top, height])

                lastView = attributesView
                
            })
            
            if let count = callHeaderType.attributes?.count {
                AttributeListViewHightConstraint.constant = CGFloat(count * 22)
            } else {
                AttributeListViewHightConstraint.constant = 0.0
            }
        }
    }

}

