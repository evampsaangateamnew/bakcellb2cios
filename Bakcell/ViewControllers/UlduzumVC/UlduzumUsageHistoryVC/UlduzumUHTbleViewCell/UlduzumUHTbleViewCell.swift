//
//  UlduzumUSTbleViewCell.swift
//  Bakcell
//
//  Created by Muhammad Waqas on 1/2/19.
//  Copyright © 2019 evampsaanga. All rights reserved.
//

import UIKit

class UlduzumUHTbleViewCell: UITableViewCell {

    //MARK:- IBOutlet
    @IBOutlet var lblMerchantTitle: UILabel!
    @IBOutlet var lblMerchantVal: UILabel!
    @IBOutlet var lblAmountTitle: UILabel!
    @IBOutlet var lblAmountVal: UILabel!
    @IBOutlet var lblDiscountPercentTitle: UILabel!
    @IBOutlet var lblDiscountPercentVal: UILabel!
    @IBOutlet var lblRedemptionDateTitle: UILabel!
    @IBOutlet var lblRedemptionDateVal: UILabel!
    @IBOutlet var lblDiscountAmountTitle: UILabel!
    @IBOutlet var lblDiscountAmountVal: UILabel!
    @IBOutlet var lblPaymentTypeTitle: UILabel!
    @IBOutlet var lblPaymentTypeval: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func setUsageCellInfo(_ info:UlduzumUsageHistory?) {
        lblMerchantTitle.text           = Localized("Title_MerchantName")
        lblAmountTitle.text             = Localized("Title_TotalAmount")
        lblDiscountPercentTitle.text    = Localized("Title_DiscountPercentage")
        lblRedemptionDateTitle.text     = Localized("Title_RedemptionDate")
        lblDiscountAmountTitle.text     = Localized("Title_DiscountAmount")
        lblPaymentTypeTitle.text        = Localized("Title_PaymentType")
        
        if let infoObj: UlduzumUsageHistory = info {
            lblMerchantVal.text = infoObj.merchantName
            lblAmountVal.text = infoObj.amount
            lblDiscountPercentVal.text = infoObj.discountPercent
            lblRedemptionDateVal.text = infoObj.redemptionDate
            lblDiscountAmountVal.text = infoObj.discountAmount
            lblPaymentTypeval.text = infoObj.paymentType
        } else {
            lblMerchantVal.text = ""
            lblAmountVal.text = ""
            lblDiscountPercentVal.text = ""
            lblRedemptionDateVal.text = ""
            lblDiscountAmountVal.text = ""
            lblPaymentTypeval.text = ""
        }
    }
    
}
