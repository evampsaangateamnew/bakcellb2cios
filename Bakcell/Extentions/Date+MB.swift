//
//  Date+MB.swift
//  Bakcell
//
//  Created by AbdulRehman Warraich on 9/26/17.
//  Copyright © 2017 evampsaanga. All rights reserved.
//

import Foundation

extension Date {
    
    //MARK: - Properties
    var millisecondsSince1970:Double {
        return (self.timeIntervalSince1970 * 1000.0).rounded()
    }
    
    init(milliseconds:Double) {
        self = Date(timeIntervalSince1970: TimeInterval(milliseconds / 1000))
    }
    
    /**
     Returns today date String.
     
     - parameter dateFormate: Date format.
     
     - returns: Todaydate(String).
     */
    func todayDateString(dateFormate: String = Constants.kHomeDateFormate ) -> String {
        
        return MBUserSession.shared.dateFormatter.createString(from: self.todayDate(dateFormate: dateFormate), dateFormate: dateFormate)
    }
    
    /**
     Returns today date.
     
     - parameter dateFormate: Date format.
     
     - returns: Todaydate(Date).
     */
    func todayDate(dateFormate: String = Constants.kHomeDateFormate ) -> Date {
        
        let now = Date()
        let currentDateString = MBUserSession.shared.dateFormatter.createString(from: now, dateFormate: dateFormate)
        
        return MBUserSession.shared.dateFormatter.createDate(from: currentDateString, dateFormate: dateFormate) ?? now
    }
    
    /**
     Returns today date with addition of days.
     
     - parameter withAdditionalValue: number of days to add to current date.
     
     - returns: Todaydate(String).
     */
    func todayDateString(withAdditionalValue additionalValue: Int, dateFormate: String = Constants.kAPIFormat) -> String {
        
        // ADD/Subtract value from date
        if let newDate = Calendar.current.date(byAdding: .day, value: additionalValue, to: Date().todayDate()) {
            
            let myDateString  = MBUserSession.shared.dateFormatter.createString(from: newDate, dateFormate: dateFormate)
            
            return myDateString
            
        } else {
            return ""
        }
    }
    
//    /**
//     Returns previous month.
//     
//     - returns: Previous month(Date).
//     */
//    func getPreviousMonth() -> Date? {
//        var currentCalendar = Calendar.current
//        
//        currentCalendar.timeZone = TimeZone.appTimeZone()
//        
//        return currentCalendar.date(byAdding: .month, value: -1, to: self)
//    }
//    
//    /**
//     Returns end of month.
//     
//     - returns: end of month(Date).
//     */
//    func endOfMonth() -> Date {
//        var currentCalendar = Calendar.current
//        
//        currentCalendar.timeZone = TimeZone.appTimeZone()
//        
//        return currentCalendar.date(byAdding: DateComponents(month: 1, day: -1), to: self.startOfMonth())!
//    }
//    
//    /**
//     Returns month wit addition of monts in date.
//     
//     - parameter withAdditionalValue: number of monts to add to current date.
//     
//     - returns: end of month(Date).
//     */
//    func getMonth(WithAdditionalValue additionalValue: Int = 0) -> Date? {
//        var currentCalendar = Calendar.current
//        
//        currentCalendar.timeZone = TimeZone.appTimeZone()
//        
//        return currentCalendar.date(byAdding: .month, value: additionalValue, to: self)
//    }
//    
//    /**
//     Returns Strat of month.
//     
//     - returns: Start of month(Date).
//     */
//    func startOfMonth() -> Date {
//        var currentCalendar = Calendar.current
//        
//        currentCalendar.timeZone = TimeZone.appTimeZone()
//        
//        if let newDate = currentCalendar.date(from: currentCalendar.dateComponents([.year, .month], from: currentCalendar.startOfDay(for: self))) {
//            
//            return newDate
//        } else {
//            return todayDate()
//        }
//        
//    }
    
}
extension Date {
    
    func getDay() -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd"
        let strMonth = dateFormatter.string(from: self)
        return strMonth
    }

    func getMonthName() -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMMM"
        let strMonth = dateFormatter.string(from: self)
        return strMonth
    }

    func getMonthNameShort() -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMM"
        let strMonth = dateFormatter.string(from: self)
        return strMonth
    }

    func getMonthNameNumber() -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM"
        let strMonth = dateFormatter.string(from: self)
        return strMonth
    }

    func getYearNameNumber() -> String {

        MBUserSession.shared.dateFormatter.dateFormat = "YYYY"
        let strMonth = MBUserSession.shared.dateFormatter.string(from: self)
        return strMonth
    }
    
    func getYearNameNumberShort() -> String {
        
        MBUserSession.shared.dateFormatter.dateFormat = "YY"
        let strMonth = MBUserSession.shared.dateFormatter.string(from: self)
        return strMonth
    }
}


extension Date {

    func currentHour() -> Int {
//        var currentCalendar = Calendar.current
//
//        if let timeZone = TimeZone(abbreviation: "UTC") {
//            currentCalendar.timeZone = timeZone
//        } else {
//            return 0
//        }

        let currentHourValue = Calendar.current.dateComponents([.hour], from: self)

        return currentHourValue.hour ?? 0
    }

    func startOfMonth() -> Date {
        var currentCalendar = Calendar.current

        if let timeZone = TimeZone(abbreviation: "UTC") {
            currentCalendar.timeZone = timeZone
        } else {
            return MBUtilities.todayDate()
        }

        if let newDate = currentCalendar.date(from: currentCalendar.dateComponents([.year, .month], from: currentCalendar.startOfDay(for: self))) {

            return newDate
        } else {
            return MBUtilities.todayDate()
        }

    }

    func endOfMonth() -> Date {
        var currentCalendar = Calendar.current

        if let timeZone = TimeZone(abbreviation: "UTC") {
            currentCalendar.timeZone = timeZone
        } else {
            return MBUtilities.todayDate()
        }

        return currentCalendar.date(byAdding: DateComponents(month: 1, day: -1), to: self.startOfMonth())!
    }

    func getNextMonth() -> Date? {
        var currentCalendar = Calendar.current

        if let timeZone = TimeZone(abbreviation: "UTC") {
            currentCalendar.timeZone = timeZone
        } else {
            return MBUtilities.todayDate()
        }

        return currentCalendar.date(byAdding: .month, value: 1, to: self)
    }

    func getPreviousMonth() -> Date? {
        var currentCalendar = Calendar.current

        if let timeZone = TimeZone(abbreviation: "UTC") {
            currentCalendar.timeZone = timeZone
        } else {
            return MBUtilities.todayDate()
        }

        return currentCalendar.date(byAdding: .month, value: -1, to: self)
    }

    func getMonth(WithAdditionalValue additionalValue: Int = 0) -> Date? {
        var currentCalendar = Calendar.current

        if let timeZone = TimeZone(abbreviation: "UTC") {
            currentCalendar.timeZone = timeZone
        } else {
            return MBUtilities.todayDate()
        }

        return currentCalendar.date(byAdding: .month, value: additionalValue, to: self)
    }


    func getDaysInMonth() -> Int {
        let calendar = Calendar.current

        let dateComponents = DateComponents(year: calendar.component(.year, from: self), month: calendar.component(.month, from: self))
        if let date = calendar.date(from: dateComponents) {

            if let range = calendar.range(of: .day, in: .month, for: date) {
                let numDays = range.count
                return numDays

            } else {
                return 0
            }

        } else {
            return 0
        }
    }
}
