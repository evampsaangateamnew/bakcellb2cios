//
//  CarouselCollectionViewswift
//  Bakcell
//
//  Created by Saad Riaz on 5/23/17.
//  Copyright © 2017 evampsaanga. All rights reserved.
//

import UIKit
import CircleProgressView

class CarouselCollectionViewCell: UICollectionViewCell {

    static let identifier = "CarouselCollectionViewCell"
    var viewType : MBFreeResourceType?

    @IBOutlet var progressValueLblCenterAlignment: NSLayoutConstraint!

    @IBOutlet var mediumLbl: UILabel!
    @IBOutlet var progressValueLbl: UILabel!
    @IBOutlet var progressUnitLbl: UILabel!
    @IBOutlet var ProgressView: CircleProgressView!

    @IBOutlet var add_btn: UIButton!
    @IBOutlet var detail_btn: UIButton!


    
    @IBAction func addButton(_ sender: Any) {
        
    }
    
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        // self.layer.cornerRadius = max(self.frame.size.width, self.frame.size.height) / 2
        // self.layer.borderWidth = 10
    }

    func setLayoutValues(freeResourceData : FreeResourceAndRoaming?, type : MBFreeResourceType, isRoaming: Bool) {

        viewType = type
        ProgressView.setProgress(0.0, animated: false)

        if let resourceData = freeResourceData {

            //Both values found Empty
            if resourceData.resourceDiscountedText.isBlank == false {

                // fill circle for Discounted type
                setColorOfCellControlles(isEnable: true, type: type)

                // Aligning value lable to center
                progressValueLblCenterAlignment.constant = 0
                // Assigning values
                ProgressView.setProgress(1.0, animated: true)
                mediumLbl.text =  resourceData.resourcesTitleLabel
                progressValueLbl.text = resourceData.resourceDiscountedText
                progressUnitLbl.text = ""


            } else if resourceData.resourceInitialUnits.isBlank == false &&
                resourceData.resourceRemainingUnits.isBlank == false { // If found value

                setColorOfCellControlles(isEnable: true, type: type)

                //Aligning and Assigning value to unit label
                if resourceData.resourceUnitName.isBlank ||
                    type == .SMS {
                    progressValueLblCenterAlignment.constant = 0
                    progressUnitLbl.text = ""
                } else {
                    progressValueLblCenterAlignment.constant = -4.0
                    progressUnitLbl.text = resourceData.resourceUnitName
                }

                // Assigning values
                let value = resourceData.resourceRemainingUnits.toDouble / resourceData.resourceInitialUnits.toDouble
                ProgressView.setProgress(value, animated: true)

                mediumLbl.text =  resourceData.resourcesTitleLabel
                progressValueLbl.text = resourceData.remainingFormatted


            } else { // Disable layout if data not found

                setColorOfCellControlles(isEnable: false, type: type)
            }

        } else {
            // Disable layout if data not found
            setColorOfCellControlles(isEnable: false, type: type)
        }
    }

    func setColorOfCellControlles(isEnable : Bool, type : MBFreeResourceType) {

        add_btn.removeTarget(nil, action: nil, for: .allEvents)
        detail_btn.removeTarget(nil, action: nil, for: .allEvents)
        ProgressView.roundedCap = false
        if isEnable {
            ProgressView.trackFillColor = UIColor .white
            ProgressView.trackBackgroundColor = UIColor.MBProgerssEnablePathColor
            progressUnitLbl.textColor = UIColor .white
            progressValueLbl.textColor = UIColor .white
            mediumLbl.textColor = UIColor .white

            add_btn.isEnabled = true
            detail_btn.isEnabled = false

        } else {

            ProgressView.trackFillColor = UIColor .MBProgerssDisableTextColor
            ProgressView.trackBackgroundColor = UIColor .MBProgerssEnablePathColor
            
            progressUnitLbl.textColor = UIColor .white
            progressValueLbl.textColor = UIColor .white
            mediumLbl.textColor = UIColor .MBProgerssDisableTextColor

//            add_btn.isEnabled = false
            detail_btn.isEnabled = false

            ProgressView.setProgress(0.0, animated: true)
//            mediumLbl.text =  type.rawValue
            progressValueLbl.text = "0"
            progressUnitLbl.text = ""

            switch type {
            case .Call:
                progressUnitLbl.text = Localized("Call_Unit")
                mediumLbl.text =  Localized("Calls_Titile")
            case .Internet:
                progressUnitLbl.text = Localized("Internet_Unit")
                mediumLbl.text =  Localized("Internet")
            case .SMS:
                progressValueLblCenterAlignment.constant = 0
                progressUnitLbl.text = ""
                //  progressUnitLbl.text = Localized("SMS_Unit")
                mediumLbl.text =  Localized("SMS")
            }
            
        }
    }

}
