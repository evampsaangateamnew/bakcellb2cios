//
//  UINAvigationController+MB.swift
//  Bakcell
//
//  Created by AbdulRehman Warraich on 8/28/17.
//  Copyright © 2017 evampsaanga. All rights reserved.
//

import Foundation
import UIKit

extension UINavigationController {

    func popToLoginViewController() {

        var isFoundLoginController = false

        self.viewControllers.forEach({ (aViewController) in

            if aViewController.isKind(of: LoginVC().classForCoder) {
                isFoundLoginController = true
                self.popToViewController(aViewController, animated: true)
                
            }

        })

        if !isFoundLoginController {

            if let splashVCObj = self.viewControllers.first {
                self.popToViewController(splashVCObj, animated: true)
            } else {
                self.navigationController?.popToRootViewController(animated: false)
            }

        }
    }
    
//    open func presentPOPUP(_ viewControllerToPresent: UIViewController, animated flag: Bool, modalTransitionStyle:UIModalTransitionStyle = .coverVertical, completion: (() -> Swift.Void)? = nil) {
//        
//        self.modalPresentationStyle = .overCurrentContext
//        self.modalTransitionStyle = modalTransitionStyle
//        
//        self.present(viewControllerToPresent, animated: flag, completion: completion)
//        
//    }
}
