//
//  PostpaidSetupView.swift
//  Bakcell
//
//  Created by Saad Riaz on 9/11/17.
//  Copyright © 2017 evampsaanga. All rights reserved.
//

import UIKit
import MarqueeLabel

class PostpaidSetupView: UIView {

    @IBOutlet var corporateView: UIView!
    @IBOutlet var contentView: UIView!
    @IBOutlet var fullPaymentView: UIView!
    @IBOutlet var typeTitleView: UIView!
    @IBOutlet var individualView: UIView!

    @IBOutlet var currentCreditCorporateView: UIView!
    @IBOutlet var currentCreditIndividualView: UIView!
    
    @IBOutlet var individual_lbl: MarqueeLabel!
    @IBOutlet var corporate_lbl: MarqueeLabel!

    @IBOutlet var outstandingPrice_lbl: UILabel!

    @IBOutlet var outstandingDebt_lbl: MarqueeLabel!
    @IBOutlet var balance_lbl: MarqueeLabel!
    @IBOutlet var currentCredit_lbl: MarqueeLabel!
    @IBOutlet var availableCredit_lbl: MarqueeLabel!

    @IBOutlet var currentCredit_img: UIImageView!

    // Balance labels
    @IBOutlet var corporateBalance_lbl: UILabel!
    @IBOutlet var corporateCurrentCredit_lbl: UILabel!
    @IBOutlet var corporateAvailableCredit_lbl: UILabel!
    @IBOutlet var individualBalance_lbl: UILabel!
    @IBOutlet var individualCurrentCredit_lbl: UILabel!
    @IBOutlet var individualAvailableCredit_lbl: UILabel!


    @IBOutlet var fullPaymentViewHeight: NSLayoutConstraint!
    @IBOutlet var typeTileViewHeight: NSLayoutConstraint!
    @IBOutlet var corporateViewWidth: NSLayoutConstraint!
    @IBOutlet var contentViewViewHeight: NSLayoutConstraint!


    @IBOutlet var currentCreditLabelHeight: NSLayoutConstraint!
    @IBOutlet var currentCreditCorporateViewHeight: NSLayoutConstraint!
    @IBOutlet var currentCreditIndividualViewHeight: NSLayoutConstraint!

    
    class func instanceFromNib() -> UIView {
        return UINib(nibName: "PostpaidSetupView", bundle: Bundle.main).instantiate(withOwner: nil, options: nil)[0] as! PostpaidSetupView

    }

    override func awakeFromNib() {
        super.awakeFromNib()

        outstandingDebt_lbl.setupMarqueeAnimation()
        balance_lbl.setupMarqueeAnimation()
        currentCredit_lbl.setupMarqueeAnimation()
        availableCredit_lbl.setupMarqueeAnimation()

        individual_lbl.setupMarqueeAnimation()
        corporate_lbl.setupMarqueeAnimation()
    }

    func setupMiddleViewLayout(postPaidData: Postpaid?, type: MBPostPaidTempletType) -> CGFloat{

                switch type {
                /*case .InstallmentHidden:
//                    fullPaymentView.isHidden = false
//                    individualView.isHidden = false
                    break

                case .FullAndPartialPayment:
//                    fullPaymentView.isHidden = false
//                    individualView.isHidden = false
                    break

                case .OutstandingAndInstallmentHidden:
                    fullPaymentViewHeight.constant = 0
                    fullPaymentView.isHidden = true

                    contentViewViewHeight.constant = contentViewViewHeight.constant - 25
                    break */

                case .IndividualCorporate:
                    typeTileViewHeight.constant = 0
                    typeTitleView.isHidden = true
                    corporateViewWidth.constant = 0
                    corporateView.isHidden = true

                    contentViewViewHeight.constant = contentViewViewHeight.constant - 21
                    break

                case .TempleteTwo:
                    currentCreditLabelHeight.constant = 0
                    currentCreditCorporateViewHeight.constant = 0
                    currentCreditIndividualViewHeight.constant = 0

                    currentCredit_img.isHidden = true
                    currentCreditCorporateView.isHidden = true
                    currentCreditIndividualView.isHidden = true

                    break

                default:
                    break
                }


        outstandingDebt_lbl.text = postPaidData?.outstandingIndividualDebtLabel ?? ""
        outstandingPrice_lbl.text = postPaidData?.outstandingIndividualDebt ?? ""

        corporate_lbl.text = postPaidData?.corporateLabel ?? ""
        individual_lbl.text = postPaidData?.individualLabel ?? ""

        balance_lbl.text = postPaidData?.balanceLabel ?? ""
        currentCredit_lbl.text = postPaidData?.currentCreditLabel ?? ""
        availableCredit_lbl.text = postPaidData?.availableCreditLabel ?? ""

        corporateBalance_lbl.text = postPaidData?.balanceCorporateValue ?? ""
        corporateCurrentCredit_lbl.text = postPaidData?.currentCreditCorporateValue ?? ""
        corporateAvailableCredit_lbl.text = postPaidData?.availableBalanceCorporateValue ?? ""

        individualBalance_lbl.text = postPaidData?.balanceIndividualValue ?? ""
        individualCurrentCredit_lbl.text = postPaidData?.currentCreditIndividualValue ?? ""
        individualAvailableCredit_lbl.text = postPaidData?.availableBalanceIndividualValue ?? ""

        if type == .TempleteTwo {
            contentViewViewHeight.constant = contentViewViewHeight.constant - 25
        }

        return contentViewViewHeight.constant
    }

}
