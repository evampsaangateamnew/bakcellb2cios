//
//  UlduzumUsageHistoryHandler.swift
//  Bakcell
//
//  Created by Muhammad Waqas on 1/3/19.
//  Copyright © 2019 evampsaanga. All rights reserved.
//

import Foundation
import ObjectMapper

class UlduzumDataHandler : Mappable {
    
    var usageHistoryList : [UlduzumUsageHistory]?
    
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        
        usageHistoryList         <- map["usageHistoryList"]
    }
}

class UlduzumUsageHistory : Mappable {
    
    var userID : String = ""
    var merchantID : String = ""
    var merchantName : String = ""
    var categoryID : String = ""
    var categoryName : String = ""
    var identicalCode : String = ""
    var redemptionDate : String = ""
    var redemptionTime : String = ""
    var amount : String = ""
    var discountAmount : String = ""
    var discountedAmount : String = ""
    var discountPercent : String = ""
    var paymentType : String = ""
    
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        
        userID              <- map["id"]
        merchantID          <- map["merchant_id"]
        merchantName        <- map["merchant_name"]
        categoryID          <- map["category_id"]
        categoryName        <- map["category_name"]
        identicalCode       <- map["identical_code"]
        redemptionDate      <- map["redemption_date"]
        redemptionTime      <- map["redemption_time"]
        amount              <- map["amount"]
        discountAmount      <- map["discount_amount"]
        discountedAmount    <- map["discounted_amount"]
        discountPercent     <- map["discount_percent"]
        paymentType         <- map["payment_type"]
    }
}

class UlduzumUsageTotals : Mappable {
    
    var segmentType : String = ""
    var dailyLimitLeft : String = ""
    var dailyLimitTotal : String = ""
    var lastUnusedCode : String = ""
    var totalRedemptions : String = ""
    var totalAmount : String = ""
    var totalDiscounted : String = ""
    var totalDiscount : String = ""
    
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        
        segmentType             <- map["segment_type"]
        dailyLimitLeft          <- (map["daily_limit_left"], StringTransform2())
        dailyLimitTotal         <- (map["daily_limit_total"], StringTransform2())
        lastUnusedCode          <- (map["last_unused_code"], StringTransform2())
        totalRedemptions        <- map["total_redemptions"]
        totalAmount             <- (map["total_amount"], StringTransform2())
        totalDiscounted         <- (map["total_discounted"], StringTransform2())
        totalDiscount           <- (map["total_discount"], StringTransform2())
        
    }
}

class UlduzumUnusedCodesHandler : Mappable {
    
    var unusedCodesList : [UlduzumUnusedCodes]?
    
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        
        unusedCodesList       <- map["unusedcodeslist"]
        
    }
}

class UlduzumNewCode : Mappable {
    
    var code : String = ""
    
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        
         code       <- map["code"]
        
    }
}

class UlduzumUnusedCodes : Mappable {
    
    var identicalCode : String = ""
    var insertDate : String = ""
    
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        
        identicalCode       <- map["identical_code"]
        insertDate          <- map["insert_date"]
        
    }
}

class UlduzumStoreLocator : Mappable, Copyable {
    
    var storesList : [UlduzumStoreLocation]?
    var storesCategories : [UlduzumStoreCategory]?
    
    required init(instance: UlduzumStoreLocator) {
        self.storesList = instance.storesList
        self.storesCategories = instance.storesCategories
    }
    
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        
        storesList            <- map["merchants"]
        storesCategories      <- map["categories"]
    }
}

class UlduzumStoreLocation : Mappable, Copyable {
    
    var id : String = ""
    var parentID : String = ""
    var name : String = ""
    var categoryID : String = ""
    var categoryName : String = ""
    var shortCode : String = ""
    var contactPerson : String = ""
    var telephone : String = ""
    var mobile : String = ""
    var minDiscountAmount : String = ""
    var maxDiscountAmount : String = ""
    var segment_A_Discount : String = ""
    var segment_B_Discount : String = ""
    var segment_C_Discount : String = ""
    var coorLat : String = ""
    var coorLng : String = ""
    var address : String = ""
    var description : String = ""
    var appShareName : String = ""
    var appRedmShareName : String = ""
    var loyalitySegment : String = ""
    var logoURLStr : String = ""
    //var logoSmall : UlduzumStoreLogo!
    var distance : Double = 0.0
    var isspecialdisc: Bool = false
    var discount : String = ""
    var storeBranches : [StoreBranches]?
    
    required init(instance: UlduzumStoreLocation) {
        self.id = instance.id
        self.parentID = instance.parentID
        self.name = instance.name
        self.categoryID = instance.categoryID
        self.categoryName = instance.categoryName
        self.shortCode = instance.shortCode
        self.contactPerson = instance.contactPerson
        self.telephone = instance.telephone
        self.mobile = instance.mobile
        self.minDiscountAmount = instance.minDiscountAmount
        self.maxDiscountAmount = instance.maxDiscountAmount
        self.segment_A_Discount = instance.segment_A_Discount
        self.segment_B_Discount = instance.segment_B_Discount
        self.segment_C_Discount = instance.segment_C_Discount
        self.coorLat = instance.coorLat
        self.coorLng = instance.coorLng
        self.address = instance.address
        self.description = instance.description
        self.appShareName = instance.appShareName
        self.appRedmShareName = instance.appRedmShareName
        self.loyalitySegment = instance.loyalitySegment
        self.logoURLStr = instance.logoURLStr
        //self.logoSmall = instance.logoSmall
        self.distance = instance.distance
        self.isspecialdisc = instance.isspecialdisc
        self.discount = instance.discount
        self.storeBranches = instance.storeBranches
    }
    
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        
        id                          <- (map["id"], StringTransform2())
        parentID                    <- (map["parent_id"], StringTransform2())
        name                        <- map["name"]
        categoryID                  <- (map["category_id"], StringTransform2())
        categoryName                <- map["category_name"]
        shortCode                   <- map["short_code"]
        contactPerson               <- map["contact_person"]
        telephone                   <- map["telephone"]
        mobile                      <- map["mobile"]
        minDiscountAmount           <- map["min_discount_amount"]
        maxDiscountAmount           <- map["max_discount_amount"]
        segment_A_Discount          <- map["segment_a_discount"]
        segment_B_Discount          <- map["segment_b_discount"]
        segment_C_Discount          <- map["segment_c_discount"]
        coorLat                     <- map["coord_lat"]
        coorLng                     <- map["coord_lng"]
        address                     <- map["address"]
        description                 <- map["description"]
        appShareName                <- map["app_sharename"]
        appRedmShareName            <- map["app_redmsharename"]
        loyalitySegment             <- map["discount"]
        logoURLStr                  <- map["logo"]
//        logoSmall                   <- map["logo_small"]
        distance                    <- map["distance"]
        isspecialdisc               <- map["isspecialdisc"]
        discount                    <- map["discount"]
        storeBranches               <- map["branches"]
        
    }
}

class UlduzumStoreCategory : Mappable {
    
    var categoryID : String = ""
    var categoryName : String = ""
    var categoryCode : String = ""
    
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        
        categoryID            <- (map["category_id"], StringTransform2())
        categoryName          <- map["code"]
        categoryName          <- map["category_name"]
        
    }
}

class UlduzumStoreLogo : Mappable {
    
    var type : String = ""
    var width : String = ""
    var height : String = ""
    var url : String = ""
    
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        
        type                 <- map["type"]
        width                <- map["width"]
        height               <- map["height"]
        url                  <- map["url"]
    }
}


class StoreModel: Mappable {
    
    var storeId : String = ""
    var storeName : String = ""
    var categoryId : String = ""
    var category_name : String = ""
    var logo : String = ""
    var coordLat : String = ""
    var coordLng : String = ""
    var isspecialdisc : String = ""
    var discount : String = ""
    var branches : [StoreBranches]?
    
    
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        storeId             <- (map["merchantDetail.id"], StringTransform2())
        storeName           <- map["merchantDetail.name"]
        categoryId          <- (map["merchantDetail.category_id"], StringTransform2())
        category_name       <- map["merchantDetail.category_name"]
        logo                <- map["merchantDetail.logo"]
        coordLat            <- map["merchantDetail.coord_lat"]
        coordLng            <- map["merchantDetail.coord_lng"]
        isspecialdisc       <- (map["merchantDetail.isspecialdisc"], StringTransform())
        discount            <- (map["merchantDetail.discount"], StringTransform2())
        branches            <- map["merchantDetail.branches"]
    }
}

class StoreBranches: Mappable {
    
    var name : String = ""
    var coord_lat : String = ""
    var coord_lng : String = ""
    var address : String = ""
    
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        name            <- map["name"]
        coord_lat       <- map["coord_lat"]
        coord_lng       <- map["coord_lng"]
        address         <- map["address"]
    }
}

struct StringTransform2: TransformType {

  func transformFromJSON(_ value: Any?) -> String? {
    return value.flatMap(String.init(describing:))
  }

  func transformToJSON(_ value: String?) -> Any? {
    return value
  }

}
