//
//  TariffKlassVC.swift
//  Bakcell
//
//  Created by Saad Riaz on 7/19/17.
//  Copyright © 2017 evampsaanga. All rights reserved.
//

import UIKit
import UPCarouselFlowLayout
import ObjectMapper
import MarqueeLabel

class TariffKlassVC: BaseVC {
    
    //MARK: - Properties
    
    var selectedSectionViewType : MBSectionType = MBSectionType.Header
    
    // Getting Properties value from parent Controller
    var selectedScreenType : MBTariffScreenType!
    var isRedirectedFromHome : Bool = false
    var isPaid : Bool = false
    var scrollToIndex = 0
    
    //Data holder properties
    var corporateData : [Corporate]? = []
    var klassData : [Klass]? = []
    
    //MARK: - IBOutlet
    @IBOutlet var collectionView: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        collectionView.delegate = self
        collectionView.dataSource = self
        
        let layout = UPCarouselFlowLayout()
        layout.itemSize = CGSize(width: UIScreen.main.bounds.width * 0.79, height:  UIScreen.main.bounds.height * 0.70)
        collectionView.collectionViewLayout = layout
        layout.scrollDirection = .horizontal
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if selectedScreenType == .Klass {
            //EventLog
            MBAnalyticsManager.logEvent(screenName: "Tariff Screen", contentType:"Tariff Klass Screen" , status:"Tariff Klass Screen" )
            
            if klassData?.count ?? 0 > scrollToIndex &&
                self.collectionView.numberOfItems(inSection: 0) >= scrollToIndex {
                
                self.view.hideDescriptionView()
                
                self.collectionView.scrollToItem(at: IndexPath(item:self.scrollToIndex, section:0), at: .centeredHorizontally, animated: true)
                
                if self.scrollToIndex == 0 {
                    self.openCurrentPaidStatusSection()
                }
            } else if (klassData?.count ?? 0 <= 0) {
                self.view.showDescriptionViewWithImage(description: Localized("Message_NoDataAvalible"))
            }
            
        } else if selectedScreenType == .Corporate {
            //EventLog
            MBAnalyticsManager.logEvent(screenName: "Tariff Screen", contentType:"Tariff Corporate Screen" , status:"Tariff Corporate Screen" )
            
            if corporateData?.count ?? 0 > scrollToIndex  &&
                self.collectionView.numberOfItems(inSection: 0) >= scrollToIndex {
                
                self.view.hideDescriptionView()
                
                collectionView.scrollToItem(at: IndexPath(item:scrollToIndex, section:0), at: .centeredHorizontally, animated: true)
                
                if self.scrollToIndex == 0 {
                    self.openCurrentPaidStatusSection()
                }
            } else if (corporateData?.count ?? 0 <= 0) {
                self.view.showDescriptionViewWithImage(description: Localized("Message_NoDataAvalible"))
            }
        } else {
            self.view.showDescriptionViewWithImage(description: Localized("Message_NoDataAvalible"))
        }
    }
    
    //MARK: - IBAction
    
    @IBAction func subscribeButtonPressed(_ sender: UIButton) {
        
        var selectedOfferingId: String, selectedOfferName: String, selectedOfferSubscribable: String = ""
        
        if selectedScreenType == MBTariffScreenType.Klass {
            
            selectedOfferingId = klassData?[sender.tag].header?.offeringId ?? ""
            selectedOfferName = klassData?[sender.tag].header?.name ?? ""
            selectedOfferSubscribable = klassData?[sender.tag].header?.subscribable ?? ""
            
        } else {
            
            selectedOfferingId = corporateData?[sender.tag].header?.offeringId ?? ""
            selectedOfferName = corporateData?[sender.tag].header?.name ?? ""
            selectedOfferSubscribable = corporateData?[sender.tag].header?.subscribable ?? ""
            
        }
        
        showConfirmationPOPUP(offeringId: selectedOfferingId, offerName: selectedOfferName, subscribableValue: selectedOfferSubscribable)
    }
    
    //MARK: - Functions
    func showConfirmationPOPUP(offeringId: String?, offerName: String?, subscribableValue : String?) {
        
        let confirmationAlert = self.myStoryBoard.instantiateViewController(withIdentifier: "ActivationConfirmationVC") as! ActivationConfirmationVC
        
        
        let migrationMessage = MBUtilities.getTariffMigrationConfirmationMessage(offeringID: offeringId)
        
        confirmationAlert.setConfirmationAlertLayoutForTariffMigration(message: migrationMessage)
        
        confirmationAlert.setYesButtonCompletionHandler { (aString) in
            self.changetTariff(offeringId: offeringId ?? "", offerName: offerName ?? "", subscribableValue: subscribableValue ?? "")
        }
        
        self.presentPOPUP(confirmationAlert, animated: true, completion: nil)
    }
    
    //MARK: - API Calls
    /// Call 'changetTariff' API .
    ///
    /// - parameter offeringId:     offerId of selected offer
    /// - parameter offerName:     offerName of selected offer
    /// - parameter subscribableValue:     subscribable value of selected offer
    ///
    /// - returns: Void
    func changetTariff(offeringId: String, offerName: String, subscribableValue : String) {
        
        
        activityIndicator.showActivityIndicator()
        
        _ = MBAPIClient.sharedClient.changetTariff(offeringId: offeringId, offerName: offerName, subscribableValue: subscribableValue, { (response, resultData, error, isCancelled, status, resultCode, resultDesc) in
            
            self.activityIndicator.hideActivityIndicator()
            if error != nil {
                error?.showServerErrorInViewController(self)
                
            } else {
                // handling data from API response.
                if resultCode == Constants.MBAPIStatusCode.succes.rawValue {
                    
                    // Parssing response data
                    if let changeTariffResponse = Mapper<MessageResponse>().map(JSONObject: resultData){
                        
                        // Show Error alert
                        self.showSuccessAlertWithMessage(message: changeTariffResponse.message)
                    } else {
                        // Show Error alert
                        self.showErrorAlertWithMessage(message: resultDesc )
                    }
                } else {
                    // Show Error alert
                    self.showErrorAlertWithMessage(message: resultDesc)
                }
            }
        })
    }
}


//MARK: - COLLECTION VIEW Delegate and DataSource

extension TariffKlassVC: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if selectedScreenType == MBTariffScreenType.Klass{
            if let klass = self.klassData {
                return klass.count
            } else {
                return 0
            }
        } else {
            if let corporatel = self.corporateData {
                return corporatel.count
            } else {
                return 0
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CarouselCollectionViewCell", for: indexPath) as! TariffKlassCell
        
        //Register expanding cell and headerView for table view
        
        // New Cells
        cell.tableView.register(UINib(nibName: "AttributeListCell", bundle: nil), forCellReuseIdentifier: AttributeListCell.identifier)
        
        cell.tableView.register(UINib(nibName: "PriceAndRoundingCell", bundle: nil), forCellReuseIdentifier: PriceAndRoundingCell.identifier)
        cell.tableView.register(UINib(nibName: "TextDateTimeCell", bundle: nil), forCellReuseIdentifier: TextDateTimeCell.identifier)
        cell.tableView.register(UINib(nibName: "TitleSubTitleValueAndDescCell", bundle: nil), forCellReuseIdentifier: TitleSubTitleValueAndDescCell.identifier)
        cell.tableView.register(UINib(nibName: "TitleDescriptionCell", bundle: nil), forCellReuseIdentifier: TitleDescriptionCell.identifier)
        cell.tableView.register(UINib(nibName: "PriceAndRoundingCell", bundle: nil), forCellReuseIdentifier: PriceAndRoundingCell.identifier)
        cell.tableView.register(UINib(nibName: "TextDateTimeCell", bundle: nil), forCellReuseIdentifier: TextDateTimeCell.identifier)
        cell.tableView.register(UINib(nibName: "TitleSubTitleValueAndDescCell", bundle: nil), forCellReuseIdentifier: TitleSubTitleValueAndDescCell.identifier)
        cell.tableView.register(UINib(nibName: "FreeResourceValidityCell", bundle: nil), forCellReuseIdentifier: FreeResourceValidityCell.identifier)
        cell.tableView.register(UINib(nibName: "RoamingDetailsCell", bundle: nil), forCellReuseIdentifier: RoamingDetailsCell.identifier)
        
        
        
        // New HeaderView
        cell.tableView.register(UINib(nibName: "SubscribeButtonView", bundle: nil), forHeaderFooterViewReuseIdentifier: SubscribeButtonView.identifier)
        
        cell.tableView.register(UINib(nibName: "IndividualTitleView", bundle: nil), forHeaderFooterViewReuseIdentifier: IndividualTitleView.identifier)
        
        cell.tableView.register(UINib(nibName: "SupplementaryOffersHeaderView", bundle: nil), forHeaderFooterViewReuseIdentifier: SupplementaryOffersHeaderView.identifier)
        
        //survery contetns
        var isSubscribed = ""
        var offeringId = ""
        if selectedScreenType == MBTariffScreenType.Klass {
            isSubscribed = klassData?[indexPath.item].header?.subscribable ?? ""
            offeringId = klassData?[indexPath.item].header?.offeringId ?? ""
            
        } else if selectedScreenType == MBTariffScreenType.Corporate {
            
            isSubscribed = corporateData?[indexPath.item].header?.subscribable ?? ""
            offeringId = corporateData?[indexPath.item].header?.offeringId ?? ""
        }
        if isSubscribed == "2" || isSubscribed == "3" {
            cell.btnsStackView.isHidden = false
            cell.btnsStackView.snp.updateConstraints { const in
                const.height.equalTo(20)
            }
        } else {
            cell.btnsStackView.isHidden = true
            cell.btnsStackView.snp.updateConstraints { const in
                const.height.equalTo(0)
            }
        }
        
        cell.offeringId = offeringId
        cell.setRating(offeringId: offeringId)
        cell.delegate = self
        
        cell.tableView.delegate = self
        cell.tableView.dataSource = self
        
        cell.tableView.allowMultipleSectionsOpen = false
        cell.tableView.keepOneSectionOpen = true;
        
        cell.tableView.tag = indexPath.item
        cell.tableView.estimatedRowHeight = 44
        cell.tableView.rowHeight = UITableView.automaticDimension
        cell.tableView.allowsSelection = false
        cell.tableView.reloadData()
        
        return cell
    }
    
    
    // USED to toggle
    func openSelectedSection() {
        
        let visbleIndexPath = collectionView.indexPathsForVisibleItems
        
        visbleIndexPath.forEach { (aIndexPath) in
            
            if let cell = collectionView.cellForItem(at: aIndexPath) as? TariffKlassCell {
                
                let type = typeOfDataInSection(index: cell.tableView.tag)
                
                if selectedSectionViewType == MBSectionType.Header && type.contains(MBSectionType.Header) {
                    
                    openSectionAt(index: 0, tableView: cell.tableView)
                    
                } else if selectedSectionViewType == MBSectionType.Detail && type.contains(MBSectionType.Detail) {
                    
                    let index = type.firstIndex(of:MBSectionType.Detail)
                    
                    openSectionAt(index: index ?? 0, tableView: cell.tableView)
                    
                } else if selectedSectionViewType == MBSectionType.Description && type.contains(MBSectionType.Description) {
                    
                    let index = type.firstIndex(of:MBSectionType.Description)
                    
                    openSectionAt(index: index ?? 0, tableView: cell.tableView)
                    
                } else if selectedSectionViewType == MBSectionType.PackagePrice && type.contains(MBSectionType.PackagePrice) {
                    
                    let index = type.firstIndex(of:MBSectionType.PackagePrice)
                    
                    openSectionAt(index: index ?? 0, tableView: cell.tableView)
                    
                } else if selectedSectionViewType == MBSectionType.PaygPrice && type.contains(MBSectionType.PaygPrice) {
                    
                    let index = type.firstIndex(of:MBSectionType.PaygPrice)
                    
                    openSectionAt(index: index ?? 0, tableView: cell.tableView)
                    
                } else if selectedSectionViewType == MBSectionType.Price && type.contains(MBSectionType.Price) {
                    
                    let index = type.firstIndex(of:MBSectionType.Price)
                    
                    openSectionAt(index: index ?? 0, tableView: cell.tableView)
                    
                } else {
                    openSectionAt(index: 0, tableView: cell.tableView)
                }
            }
        }
    }
    
    func openSectionAt(index : Int , tableView : MBAccordionTableView) {
        
        if tableView.isSectionOpen(index) == false {
            
            // tableView.scrollToRow(at: IndexPath(row: 0, section: index), at: .top, animated: true)
            
            tableView.toggleSection(withOutCallingDelegates: index)
            
        }
    }
    
}

// MARK: - <UITableViewDataSource> / <UITableViewDelegate>

extension TariffKlassVC : UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return  self.numberOfSection(index: tableView.tag)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        
        let types = typeOfDataInSection(index: tableView.tag)
        
        switch types[section] {
            
        case MBSectionType.Header:
            
            return numberOfRowsInHeaderSection(index: tableView.tag)
            
        case MBSectionType.PackagePrice:
            return numberOfRowsInPackagePrice(aPriceObject: klassData?[tableView.tag].packagePrice)
            
        case MBSectionType.PaygPrice:
            return numberOfRowsInPaygPrice(aPriceObject: klassData?[tableView.tag].paygPrice)
            
        case MBSectionType.Price:
            return numberOfRowsInPrice(aPriceObject: corporateData?[tableView.tag].price)
            
        case MBSectionType.Detail:
            return numberOfRowsInDetailSection(index: tableView.tag)
            
        case MBSectionType.Description:
            return numberOfRowsInDescriptionSection(index: tableView.tag)
            
        default:
            return 0
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        if section == 0 {
            return 35
        } else {
            return 45
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        let sectionType = self.typeOfDataInSection(index: tableView.tag)
        
        switch sectionType[indexPath.section] {
            
        case MBSectionType.Header:
            let cell : AttributeListCell = tableView.dequeueReusableCell(withIdentifier: AttributeListCell.identifier) as! AttributeListCell
            
            
            if selectedScreenType == MBTariffScreenType.Klass {
                cell.setAttributeListForTariffPostpaidHeader(attributeList: klassData?[tableView.tag].header?.attributes?[indexPath.row])
            } else if selectedScreenType == MBTariffScreenType.Corporate {
                cell.setAttributeListForTariffPostpaidHeader(attributeList: corporateData?[tableView.tag].header?.attributes?[indexPath.row])
            }
            
            return cell
            
        case MBSectionType.PackagePrice:
            
            if let aPackagePrice = klassData?[tableView.tag].packagePrice {
                
                let cell = tableView.dequeueReusableCell(withIdentifier: PriceAndRoundingCell.identifier) as! PriceAndRoundingCell
                
                let aPackagePriceDataType = typeOfDataInPackagePrice(aPriceObject: aPackagePrice)
                
                switch aPackagePriceDataType[indexPath.row] {
                    
                case MBDataTypes.Call:
                    
                    cell.setCallAndSMSLayoutValues(callHeaderType: aPackagePrice.call, cellType:MBDataTypes.Call, setBackgroundColorWhite: true)
                    
                case MBDataTypes.SMS:
                    
                    cell.setCallAndSMSLayoutValues(callHeaderType: aPackagePrice.sms, cellType:MBDataTypes.SMS, setBackgroundColorWhite: true)
                    
                case MBDataTypes.Internet:
                    
                    cell.setInternetLayoutValues(internetHeader: aPackagePrice.internet, setBackgroundColorWhite: true)
                    
                default:
                    return UITableViewCell(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
                }
                
                return cell
                
            } else {
                return UITableViewCell(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
            }
            
            
        case MBSectionType.PaygPrice:
            
            if let aPaygPrice = klassData?[tableView.tag].paygPrice {
                
                let cell = tableView.dequeueReusableCell(withIdentifier: PriceAndRoundingCell.identifier) as! PriceAndRoundingCell
                
                let aPaygPriceDataType = typeOfDataInPaygPrice(aPriceObject: aPaygPrice)
                
                switch aPaygPriceDataType[indexPath.row] {
                    
                case MBDataTypes.Call:
                    
                    cell.setCallAndSMSLayoutValues(callHeaderType: aPaygPrice.call, cellType:MBDataTypes.Call, setBackgroundColorWhite: true)
                    
                case MBDataTypes.SMS:
                    
                    cell.setCallAndSMSLayoutValues(callHeaderType: aPaygPrice.sms, cellType:MBDataTypes.SMS, setBackgroundColorWhite: true)
                    
                case MBDataTypes.Internet:
                    
                    cell.setInternetLayoutValues(internetHeader: aPaygPrice.internet, setBackgroundColorWhite: true)
                    
                default:
                    return UITableViewCell(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
                }
                
                return cell
                
            } else {
                return UITableViewCell(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
            }
            
        case MBSectionType.Price:
            
            if let aPrice = corporateData?[tableView.tag].price {
                
                let cell = tableView.dequeueReusableCell(withIdentifier: PriceAndRoundingCell.identifier) as! PriceAndRoundingCell
                
                let aPriceDataType = typeOfDataInPrice(aPriceObject: aPrice)
                
                switch aPriceDataType[indexPath.row] {
                    
                case MBDataTypes.Call:
                    
                    cell.setCallAndSMSLayoutValues(callHeaderType: aPrice.call, cellType:MBDataTypes.Call, setBackgroundColorWhite: true)
                    
                case MBDataTypes.SMS:
                    
                    cell.setCallAndSMSLayoutValues(callHeaderType: aPrice.sms, cellType:MBDataTypes.SMS, setBackgroundColorWhite: true)
                    
                case MBDataTypes.Internet:
                    
                    cell.setInternetLayoutValues(internetHeader: aPrice.internet, setBackgroundColorWhite: true)
                    
                default:
                    return UITableViewCell(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
                }
                
                return cell
                
            } else {
                return UITableViewCell(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
            }
            
            
        case MBSectionType.Detail:
            
            var aDetail : DetailsWithPriceArray?
            
            if selectedScreenType == MBTariffScreenType.Klass {
                
                aDetail = klassData?[tableView.tag].details
                
            } else if selectedScreenType == MBTariffScreenType.Corporate {
                
                aDetail = corporateData?[tableView.tag].details
            }
            
            let detailDataType = typeOfDataInDetailSection(aDetails: aDetail)
            
            switch detailDataType[indexPath.row] {
                
            case MBOfferType.Price:
                
                let cell = tableView.dequeueReusableCell(withIdentifier: PriceAndRoundingCell.identifier) as! PriceAndRoundingCell
                cell.setPriceLayoutValues(price: aDetail?.price?[indexPath.row])
                return cell
                
            case MBOfferType.Rounding:
                
                let cell = tableView.dequeueReusableCell(withIdentifier: PriceAndRoundingCell.identifier) as! PriceAndRoundingCell
                cell.setRoundingLayoutValues(rounding: aDetail?.rounding)
                return cell
                
            case MBOfferType.TextWithTitle:
                
                let cell = tableView.dequeueReusableCell(withIdentifier: TextDateTimeCell.identifier) as! TextDateTimeCell
                cell.setTextWithTitleLayoutValues(textWithTitle: aDetail?.textWithTitle)
                return cell
                
            case MBOfferType.TextWithOutTitle:
                
                let cell = tableView.dequeueReusableCell(withIdentifier: TextDateTimeCell.identifier) as! TextDateTimeCell
                cell.setTextWithOutTitleLayoutValues(textWithOutTitle: aDetail?.textWithOutTitle)
                return cell
                
            case MBOfferType.TextWithPoints:
                
                let cell = tableView.dequeueReusableCell(withIdentifier: TextDateTimeCell.identifier) as! TextDateTimeCell
                cell.setTextWithPointsLayoutValues(textWithPoint: aDetail?.textWithPoints)
                return cell
                
            case MBOfferType.Date:
                
                let cell = tableView.dequeueReusableCell(withIdentifier: TextDateTimeCell.identifier) as! TextDateTimeCell
                cell.setDateAndTimeLayoutValues(dateAndTime: aDetail?.date)
                return cell
                
            case MBOfferType.Time:
                
                let cell = tableView.dequeueReusableCell(withIdentifier: TextDateTimeCell.identifier) as! TextDateTimeCell
                cell.setDateAndTimeLayoutValues(dateAndTime: aDetail?.time)
                return cell
                
            case MBOfferType.RoamingDetails:
                
                let cell = tableView.dequeueReusableCell(withIdentifier: RoamingDetailsCell.identifier) as! RoamingDetailsCell
                cell.setRoamingDetailsLayout(roamingDetails: aDetail?.roamingDetails)
                return cell
                
            case MBOfferType.FreeResourceValidity:
                
                let cell = tableView.dequeueReusableCell(withIdentifier: FreeResourceValidityCell.identifier) as! FreeResourceValidityCell
                cell.setFreeResourceValidityValues(freeResourceValidity: aDetail?.freeResourceValidity)
                return cell
                
            case MBOfferType.TitleSubTitleValueAndDesc:
                
                let cell = tableView.dequeueReusableCell(withIdentifier: TitleSubTitleValueAndDescCell.identifier) as! TitleSubTitleValueAndDescCell
                cell.setTitleSubTitleValueAndDescLayoutValues(titleSubTitleValueAndDesc: aDetail?.titleSubTitleValueAndDesc)
                return cell
                
            default:
                break
            }
            
            
        case MBSectionType.Description:
            
            let descriptionDataType = typeOfDataInDescriptionSection(index: tableView.tag)
            
            let cell = tableView.dequeueReusableCell(withIdentifier: TitleDescriptionCell.identifier) as! TitleDescriptionCell
            
            switch descriptionDataType[indexPath.row] {
                
            case MBDataTypes.Advantages:
                
                cell.setTitleAndDescriptionWithOutIcon(title: corporateData?[tableView.tag].description?.advantages?.title,
                                                       description: corporateData?[tableView.tag].description?.advantages?.description)
                
            case MBDataTypes.Classification:
                
                cell.setTitleAndDescriptionWithOutIcon(title: corporateData?[tableView.tag].description?.classification?.title,
                                                       description: corporateData?[tableView.tag].description?.classification?.description)
            default:
                break
            }
            
            return cell
            
            
            
            
        default:
            return UITableViewCell()
        }
        
        return UITableViewCell()
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let types = typeOfDataInSection(index: tableView.tag)
        
        switch types[section] {
            
        case MBSectionType.Header:
            
            let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: IndividualTitleView.identifier) as! IndividualTitleView
            
            var aOfferName = ""
            var aPriceLabel = ""
            var aPriceValue = ""
            
            if selectedScreenType == MBTariffScreenType.Klass {
                aOfferName = klassData?[tableView.tag].header?.name ?? ""
                aPriceLabel = klassData?[tableView.tag].header?.priceLabel ?? ""
                aPriceValue = klassData?[tableView.tag].header?.priceValue ?? ""
                
            } else if selectedScreenType == MBTariffScreenType.Corporate {
                
                aOfferName = corporateData?[tableView.tag].header?.name ?? ""
                aPriceLabel = corporateData?[tableView.tag].header?.priceLabel ?? ""
                aPriceValue = corporateData?[tableView.tag].header?.priceValue ?? ""
                
            }
            
            headerView.setViewWith(offerName: aOfferName, priceLabel: aPriceLabel, priceValue: aPriceValue)
            
            return headerView
            
        case MBSectionType.PackagePrice:
            
            let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: SupplementaryOffersHeaderView.identifier) as! SupplementaryOffersHeaderView
            
            let title :String = klassData?[tableView.tag].packagePrice?.packagePriceLabel ?? ""
            
            if selectedSectionViewType == MBSectionType.PackagePrice {
                headerView.setViewWithTitle(title: title, isSectionSelected: true, headerType: MBSectionType.PackagePrice)
            } else {
                headerView.setViewWithTitle(title: title, isSectionSelected: false, headerType: MBSectionType.PackagePrice)
            }
            
            return headerView
            
            
        case MBSectionType.PaygPrice:
            
            let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: SupplementaryOffersHeaderView.identifier) as! SupplementaryOffersHeaderView
            
            let title :String = klassData?[tableView.tag].paygPrice?.paygPriceLabel ?? ""
            
            if selectedSectionViewType == MBSectionType.PaygPrice {
                headerView.setViewWithTitle(title: title, isSectionSelected: true, headerType: MBSectionType.PaygPrice)
            } else {
                headerView.setViewWithTitle(title: title, isSectionSelected: false, headerType: MBSectionType.PaygPrice)
            }
            
            return headerView
            
        case MBSectionType.Price:
            
            let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: SupplementaryOffersHeaderView.identifier) as! SupplementaryOffersHeaderView
            
            let title :String = corporateData?[tableView.tag].price?.priceLabel ?? ""
            
            if selectedSectionViewType == MBSectionType.Price {
                headerView.setViewWithTitle(title: title, isSectionSelected: true, headerType: MBSectionType.Price)
            } else {
                headerView.setViewWithTitle(title: title, isSectionSelected: false, headerType: MBSectionType.Price)
            }
            
            return headerView
            
        case MBSectionType.Detail:
            let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: SupplementaryOffersHeaderView.identifier) as! SupplementaryOffersHeaderView
            
            var title :String = ""
            if selectedScreenType == MBTariffScreenType.Klass {
                title = klassData?[tableView.tag].details?.detailLabel ?? ""
                
            } else if selectedScreenType == MBTariffScreenType.Corporate {
                
                title = corporateData?[tableView.tag].details?.detailLabel ?? ""
            }
            
            if selectedSectionViewType == MBSectionType.Detail {
                headerView.setViewWithTitle(title: title, isSectionSelected: true, headerType: MBSectionType.Detail)
            } else {
                headerView.setViewWithTitle(title: title, isSectionSelected: false, headerType: MBSectionType.Detail)
            }
            return headerView
            
        case MBSectionType.Description:
            
            let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: SupplementaryOffersHeaderView.identifier) as! SupplementaryOffersHeaderView
            
            let title :String = corporateData?[tableView.tag].description?.descLabel ?? ""
            
            if selectedSectionViewType == MBSectionType.Description {
                headerView.setViewWithTitle(title: title, isSectionSelected: true, headerType: MBSectionType.Description)
            } else {
                headerView.setViewWithTitle(title: title, isSectionSelected: false, headerType: MBSectionType.Description)
            }
            
            return headerView
            
        case MBSectionType.Subscription:
            
            let headerButtonView = tableView.dequeueReusableHeaderFooterView(withIdentifier: SubscribeButtonView.identifier) as! SubscribeButtonView
            headerButtonView.subscribed_btn.tag = tableView.tag
            
            var  isSubscribed : String = "0"
            
            if selectedScreenType == MBTariffScreenType.Klass {
                isSubscribed = klassData?[tableView.tag].header?.subscribable ?? ""
                
            } else if selectedScreenType == MBTariffScreenType.Corporate {
                
                isSubscribed = corporateData?[tableView.tag].header?.subscribable ?? ""
            }
            
            headerButtonView.setViewForSubscribeButtonForTariff(isSubscribed: isSubscribed, tag: tableView.tag)
            
            
            
            headerButtonView.subscribed_btn.addTarget(self, action: #selector(subscribeButtonPressed(_:)), for: UIControl.Event.touchUpInside)
            
            return headerButtonView
            
        default:
            return UIView()
            
        }
    }
    
    //MARK: - Helping functions
    
    func numberOfSection(index:Int) -> Int {
        
        if selectedScreenType == MBTariffScreenType.Corporate {
            
            if let aCorporateOffer = corporateData?[index] {
                
                var headerCount : Int = 2
                
                if aCorporateOffer.price != nil {
                    headerCount += 1
                }
                
                if aCorporateOffer.details != nil {
                    headerCount += 1
                }
                
                if aCorporateOffer.description != nil {
                    headerCount += 1
                }
                
                return headerCount
                
            } else {
                return 0
            }
            
        } else if selectedScreenType == MBTariffScreenType.Klass {
            
            if let aKlassOffer = klassData?[index] {
                
                var headerCount : Int = 2
                
                if aKlassOffer.details != nil {
                    headerCount += 1
                }
                
                if aKlassOffer.packagePrice != nil {
                    headerCount += 1
                }
                
                if aKlassOffer.paygPrice != nil {
                    headerCount += 1
                }
                
                return headerCount
                
            } else {
                return 0
            }
            
        } else {
            return 0
        }
    }
    
    func typeOfDataInSection(index : Int) -> [MBSectionType] {
        
        // Atleast one because there is one with offer name
        
        var dataTypes : [MBSectionType] = []
        
        if selectedScreenType == MBTariffScreenType.Klass {
            
            if let aOffer =  klassData?[index] {
                // 1
                if aOffer.header != nil {
                    
                    dataTypes.append(MBSectionType.Header)
                }
                // 2
                if aOffer.packagePrice != nil  {
                    
                    dataTypes.append(MBSectionType.PackagePrice)
                }
                // 3
                if aOffer.paygPrice != nil {
                    
                    dataTypes.append(MBSectionType.PaygPrice)
                }
                // 4
                if aOffer.details != nil {
                    
                    dataTypes.append(MBSectionType.Detail)
                }
                
                dataTypes.append(MBSectionType.Subscription)
            }
            
            
        } else if selectedScreenType == MBTariffScreenType.Corporate {
            
            if let aOffer =  corporateData?[index] {
                // 1
                if aOffer.header != nil {
                    
                    dataTypes.append(MBSectionType.Header)
                }
                
                // 2
                if aOffer.price != nil  {
                    
                    dataTypes.append(MBSectionType.Price)
                }
                
                // 3
                if aOffer.details != nil  {
                    
                    dataTypes.append(MBSectionType.Detail)
                }
                // 4
                if aOffer.description != nil {
                    
                    dataTypes.append(MBSectionType.Description)
                }
                
                dataTypes.append(MBSectionType.Subscription)
            }
        }
        
        return dataTypes
    }
    
    // Will return number of row in section header accourding to
    func numberOfRowsInHeaderSection(index : Int) -> Int {
        
        if selectedScreenType == MBTariffScreenType.Klass {
            return klassData?[index].header?.attributes?.count ?? 0
            
        } else  if selectedScreenType == MBTariffScreenType.Corporate {
            return corporateData?[index].header?.attributes?.count ?? 0
            
        } else {
            return 0
        }
        
    }
    
    func numberOfRowsInDetailSection(index : Int) -> Int {
        
        if selectedScreenType == MBTariffScreenType.Klass {
            return numberOfRowsInDetailSection(aDetails: klassData?[index].details)
            
        } else  if selectedScreenType == MBTariffScreenType.Corporate {
            return numberOfRowsInDetailSection(aDetails:  corporateData?[index].details)
            
        } else {
            return 0
        }
    }
    
    func numberOfRowsInDetailSection(aDetails : DetailsWithPriceArray?) -> Int {
        
        var numberOfRows : Int = 0
        
        if let aDetail =  aDetails {
            
            // 1
            //            if aDetail.price != nil {
            //                numberOfRows = numberOfRows + 1
            //            }
            if let count = aDetail.price?.count {
                numberOfRows = numberOfRows + count
            }
            // 2
            if aDetail.rounding != nil {
                numberOfRows = numberOfRows + 1
            }
            // 3
            if aDetail.textWithTitle != nil {
                numberOfRows = numberOfRows + 1
            }
            // 4
            if aDetail.textWithOutTitle != nil {
                numberOfRows = numberOfRows + 1
            }
            // 5
            if aDetail.textWithPoints != nil {
                numberOfRows = numberOfRows + 1
            }
            // 6
            if aDetail.date != nil {
                numberOfRows = numberOfRows + 1
            }
            // 7
            if aDetail.time != nil {
                numberOfRows = numberOfRows + 1
            }
            // 8
            if aDetail.roamingDetails != nil {
                numberOfRows = numberOfRows + 1
            }
            // 9
            if aDetail.freeResourceValidity != nil {
                numberOfRows = numberOfRows + 1
            }
            // 10
            if aDetail.titleSubTitleValueAndDesc != nil {
                numberOfRows = numberOfRows + 1
            }
            
        }
        return numberOfRows
    }
    
    func typeOfDataInDetailSection(aDetails : DetailsWithPriceArray?) -> [MBOfferType] {
        
        var dataTypes : [MBOfferType] = []
        
        if let aDetail =  aDetails {
            // 1
            //            if aDetail.price != nil {
            //
            //                dataTypes.append(MBOfferType.Price)
            //            }
            if let count = aDetail.price?.count {
                
                for _ in 0..<count {
                    dataTypes.append(MBOfferType.Price)
                }
            }
            // 2
            if aDetail.rounding != nil  {
                
                dataTypes.append(MBOfferType.Rounding)
            }
            // 3
            if aDetail.textWithTitle != nil {
                
                dataTypes.append(MBOfferType.TextWithTitle)
            }
            // 4
            if aDetail.textWithOutTitle != nil {
                
                dataTypes.append(MBOfferType.TextWithOutTitle)
            }
            // 5
            if aDetail.textWithPoints != nil {
                
                dataTypes.append(MBOfferType.TextWithPoints)
            }
            // 6
            if aDetail.titleSubTitleValueAndDesc != nil {
                
                dataTypes.append(MBOfferType.TitleSubTitleValueAndDesc)
            }
            
            // 7
            if aDetail.date != nil {
                
                dataTypes.append(MBOfferType.Date)
            }
            // 8
            if aDetail.time != nil {
                
                dataTypes.append(MBOfferType.Time)
            }
            // 9
            if aDetail.roamingDetails != nil {
                
                dataTypes.append(MBOfferType.RoamingDetails)
            }
            // 10
            if aDetail.freeResourceValidity != nil {
                
                dataTypes.append(MBOfferType.FreeResourceValidity)
            }
        }
        return dataTypes
    }
    
    func typeOfDataInDescriptionSection(index : Int) -> [MBDataTypes] {
        
        var dataTypes : [MBDataTypes] = []
        
        if let aDescription =  corporateData?[index].description {
            
            // 1
            if aDescription.advantages != nil  {
                
                dataTypes.append(MBDataTypes.Advantages)
            }
            
            // 2
            if aDescription.classification != nil {
                
                dataTypes.append(MBDataTypes.Classification)
            }
            
        }
        
        return dataTypes
    }
    
    func numberOfRowsInDescriptionSection(index : Int) -> Int {
        
        var numberOfRows : Int = 0
        
        if let aDescription = corporateData?[index].description {
            
            if aDescription.advantages != nil {
                numberOfRows += 1
            }
            
            if aDescription.classification != nil {
                numberOfRows += 1
            }
        }
        return numberOfRows
        
    }
    
    
    func numberOfRowsInPackagePrice(aPriceObject: PackagePrice?) -> Int {
        
        var numberOfRows : Int = 0
        
        if let aPrice =  aPriceObject {
            
            if aPrice.call != nil {
                numberOfRows += 1
            }
            
            if aPrice.sms != nil {
                numberOfRows += 1
            }
            
            if aPrice.internet != nil {
                numberOfRows += 1
            }
        }
        return numberOfRows
    }
    
    func numberOfRowsInPaygPrice(aPriceObject: PaygPrice?) -> Int {
        
        var numberOfRows : Int = 0
        
        if let aPrice =  aPriceObject {
            
            if aPrice.call != nil {
                numberOfRows += 1
            }
            
            if aPrice.sms != nil {
                numberOfRows += 1
            }
            
            if aPrice.internet != nil {
                numberOfRows += 1
            }
        }
        return numberOfRows
    }
    
    func numberOfRowsInPrice(aPriceObject: CinPostpaidPrice?) -> Int {
        
        var numberOfRows : Int = 0
        
        if let aPrice =  aPriceObject {
            
            if aPrice.call != nil {
                numberOfRows += 1
            }
            
            if aPrice.sms != nil {
                numberOfRows += 1
            }
            
            if aPrice.internet != nil {
                numberOfRows += 1
            }
        }
        return numberOfRows
    }
    
    func typeOfDataInPackagePrice(aPriceObject: PackagePrice?) -> [MBDataTypes]  {
        
        var dataTypes : [MBDataTypes] = []
        if let aPrice =  aPriceObject {
            
            // 1
            if aPrice.call != nil {
                
                dataTypes.append(MBDataTypes.Call)
            }
            // 2
            if aPrice.sms != nil  {
                
                dataTypes.append(MBDataTypes.SMS)
            }
            // 3
            if aPrice.internet != nil {
                
                dataTypes.append(MBDataTypes.Internet)
            }
        }
        return dataTypes
    }
    func typeOfDataInPaygPrice(aPriceObject: PaygPrice?) -> [MBDataTypes]  {
        
        var dataTypes : [MBDataTypes] = []
        if let aPrice =  aPriceObject {
            
            // 1
            if aPrice.call != nil {
                
                dataTypes.append(MBDataTypes.Call)
            }
            // 2
            if aPrice.sms != nil  {
                
                dataTypes.append(MBDataTypes.SMS)
            }
            // 3
            if aPrice.internet != nil {
                
                dataTypes.append(MBDataTypes.Internet)
            }
        }
        return dataTypes
    }
    
    
    func typeOfDataInPrice(aPriceObject: CinPostpaidPrice?) -> [MBDataTypes]  {
        
        var dataTypes : [MBDataTypes] = []
        if let aPrice =  aPriceObject {
            
            // 1
            if aPrice.call != nil {
                
                dataTypes.append(MBDataTypes.Call)
            }
            // 2
            if aPrice.sms != nil  {
                
                dataTypes.append(MBDataTypes.SMS)
            }
            // 3
            if aPrice.internet != nil {
                
                dataTypes.append(MBDataTypes.Internet)
            }
        }
        return dataTypes
    }
    
    
    
}

// MARK: - <MBAccordionTableViewDelegate>

extension TariffKlassVC : MBAccordionTableViewDelegate {
    
    func tableView(_ tableView: MBAccordionTableView, willOpenSection section: Int, withHeader header: UITableViewHeaderFooterView?) {
        
        if let headerView = header as? SupplementaryOffersHeaderView {
            headerView.detailSign_btn .setImage(#imageLiteral(resourceName: "minu-sign"), for: UIControl.State.normal)
        }
    }
    
    func tableView(_ tableView: MBAccordionTableView, didOpenSection section: Int, withHeader header: UITableViewHeaderFooterView?) {
        
        if let headerView = header as? SupplementaryOffersHeaderView {
            
            if selectedSectionViewType != headerView.viewType {
                
                selectedSectionViewType = headerView.viewType
            }
        } else if (header as? IndividualTitleView) != nil {
            
            if selectedSectionViewType != .Header {
                
                selectedSectionViewType = .Header
            }
        }
    }
    
    func tableView(_ tableView: MBAccordionTableView, willCloseSection section: Int, withHeader header: UITableViewHeaderFooterView?) {
        
        if let headerView = header as? SupplementaryOffersHeaderView {
            headerView.detailSign_btn .setImage(#imageLiteral(resourceName: "Plus-Sign"), for: UIControl.State.normal)
        }
    }
    
    func tableView(_ tableView: MBAccordionTableView, didCloseSection section: Int, withHeader header: UITableViewHeaderFooterView?) {
        
        
        if let headerView = header as? SupplementaryOffersHeaderView {
            
            if selectedSectionViewType == headerView.viewType {
                selectedSectionViewType = MBSectionType.Header
                tableView.toggleSection(0)
            }
        } else if (header as? IndividualTitleView) != nil {
            
            if selectedSectionViewType == .Header {
                selectedSectionViewType = MBSectionType.Header
                tableView.toggleSection(0)
            }
        }
    }
    
    func tableView(_ tableView: MBAccordionTableView, canInteractWithHeaderAtSection section: Int) -> Bool {
        
        if let headerView = tableView.headerView(forSection: section) as? SubscribeButtonView {
            
            if headerView.viewType == MBSectionType.Subscription {
                
                return false
                
            } else {
                
                return true
            }
        } else {
            
            return true
        }
    }
}

extension TariffKlassVC : UIScrollViewDelegate {
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        
        if scrollView == collectionView {
            openSelectedSection()
        }
    }
    
    func scrollViewDidEndScrollingAnimation(_ scrollView: UIScrollView) {
        
        self.openCurrentPaidStatusSection()
    }
    
    func openCurrentPaidStatusSection() {
        
        if self.isRedirectedFromHome == true {
            DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(1)) {
                
                if self.klassData?.count ?? 0 > self.scrollToIndex {
                    
                    let sectionType = self.typeOfDataInSection(index: self.scrollToIndex)
                    
                    if self.isPaid == true {
                        
                        if sectionType.contains(.PackagePrice) {
                            
                            if let aKlassCell = self.collectionView.cellForItem(at: IndexPath(item:self.scrollToIndex, section:0)) as? TariffKlassCell{
                                
                                let sectionIndex = sectionType.firstIndex(of: .PackagePrice) ?? 0
                                
                                if aKlassCell.tableView.isSectionOpen(sectionIndex) == false{
                                    aKlassCell.tableView.toggleSection(sectionIndex)
                                }
                                
                            }
                            
                        }
                    } else { // UnPaid Tariff
                        
                        if sectionType.contains(.PaygPrice) {
                            
                            if let aKlassCell = self.collectionView.cellForItem(at: IndexPath(item:self.scrollToIndex, section:0)) as? TariffKlassCell{
                                
                                let sectionIndex = sectionType.firstIndex(of: .PaygPrice) ?? 0
                                
                                if aKlassCell.tableView.isSectionOpen(sectionIndex) == false{
                                    aKlassCell.tableView.toggleSection(sectionIndex)
                                }
                                
                            }
                        }
                    }
                    
                    //Reset Check
                    self.isPaid = false
                }
            }
            
            //Reset Check
            self.isRedirectedFromHome = false
        }
    }
}



//MARK: SupplementaryOffersCellDelegate
extension TariffKlassVC: SupplementaryOffersCellDelegate {
    func starTapped(offerId: String) {
        
        if let inAppSurveyVC : InAppSurveyVC = InAppSurveyVC.fromNib() {
            if  let  userSurvey = MBUserSession.shared.appSurvays?.userSurveys?.first(where: { $0.offeringId == offerId}){
                if let survey  = MBUserSession.shared.appSurvays?.surveys?.first(where: { $0.survayId  == userSurvey.surveryId }){
                    inAppSurveyVC.currentSurvay = survey
                    if let  question =  survey.questions?.first(where: { $0.questionId == userSurvey.questionId }){
                        inAppSurveyVC.survayQuestion = question
                        if let answer = question.answers?.firstIndex(where: { $0.answerId ==  userSurvey.answerId }) {
                            inAppSurveyVC.selectedAnswer = question.answers?[answer]
                        }
                    }
                }
                inAppSurveyVC.survayComment = userSurvey.comments
                inAppSurveyVC.offeringId = offerId
                inAppSurveyVC.offeringType = "1"
            } else {
                if let survey = MBUserSession.shared.appSurvays?.surveys?.first(where: { $0.screenName ?? "" == SurveyScreenName.tariff.rawValue}) {
                    inAppSurveyVC.currentSurvay = survey
                    inAppSurveyVC.offeringId = offerId
                    inAppSurveyVC.offeringType = "1"
                }
            }
            inAppSurveyVC.callback =  {
                self.collectionView.reloadData()
            }
            self.presentPOPUP(inAppSurveyVC, animated: true, completion: nil)
        }
        
    }
    
    
}
