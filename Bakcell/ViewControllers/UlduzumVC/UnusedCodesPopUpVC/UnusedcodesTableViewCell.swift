//
//  UnusedcodesTableViewCell.swift
//  Bakcell
//
//  Created by Muhammad Waqas on 1/4/19.
//  Copyright © 2019 evampsaanga. All rights reserved.
//

import UIKit

class UnusedcodesTableViewCell: UITableViewCell {

    //MARK:- IBOutlet
    @IBOutlet var lblUnusedCode: UILabel!
    @IBOutlet var lblInsertDate: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
