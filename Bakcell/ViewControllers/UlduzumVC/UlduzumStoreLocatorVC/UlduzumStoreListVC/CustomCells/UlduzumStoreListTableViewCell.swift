//
//  UlduzumStoreListTableViewCell.swift
//  Bakcell
//
//  Created by Muhammad Waqas on 1/7/19.
//  Copyright © 2019 evampsaanga. All rights reserved.
//

import UIKit

class UlduzumStoreListTableViewCell: UITableViewCell {

    //MARK:- IBOutlet
    @IBOutlet var lblStoreName : UILabel!
    @IBOutlet var lblStoredesc : UILabel!
    @IBOutlet var lblStorePercentage : UILabel!
    
    @IBOutlet var ivIcon : UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
