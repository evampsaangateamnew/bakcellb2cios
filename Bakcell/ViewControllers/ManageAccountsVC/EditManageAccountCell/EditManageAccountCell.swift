//
//  EditManageAccountCell.swift
//  Azerfon
//
//  Created by Muhammad Waqas on 3/22/19.
//  Copyright © 2019 Evamp&Saanga. All rights reserved.
//

import UIKit
import MarqueeLabel

class EditManageAccountCell: UITableViewCell {

    //MARK: - IBOutlet
    @IBOutlet var userNameLabel             : MarqueeLabel!
    @IBOutlet var userPhoneNumberLabel      : UILabel!
    @IBOutlet var deletButton               : UIButton!
    @IBOutlet var dotImageView              : UIImageView!
    @IBOutlet var sepratorView                  : UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        userNameLabel.setupMarqueeAnimation()
    }

    func setLayout(name :String?, msisdn :String?, isSelected :Bool) {
        userNameLabel.text = name
        userPhoneNumberLabel.text = msisdn
        dotImageView.isHidden = !isSelected
    }
}
